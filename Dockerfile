FROM debian:bookworm

RUN apt-get update && \
    apt-get -y install \
        git-restore-mtime \
        jq \
        make \
        nodejs \
        npm \
        po4a \
        rsync \
        sudo

CMD ["/bin/bash"]
