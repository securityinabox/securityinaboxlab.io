# vim:noet:ai:sw=8

# Translation parameters.
KEEP_THRES      ?= 80
FMT              = text
YFM_KEYS        ?= author,download_links,license,logo,subtopic,$\
                   system_requirements,teaser,teaser_image,title,subtitle,$\
                   website
FMT_OPTS         = -o markdown -o yfm_keys=$(YFM_KEYS)
CHARSET         ?= UTF-8

# Source language.
SRC_LANG        := en
# Languages for which there is a translation done or in-progress.
LINGUAS         ?= ar bo es fa fr hi id my ps pt ru th tr vi zh zh_hk
# Languages which must not be included in the website build.
HIDE_LINGUAS    ?= bo hi th zh_hk
# Languages to include in the website build, including the source language.
PUB_LINGUAS     := $(SRC_LANG) $(filter-out $(HIDE_LINGUAS),$(LINGUAS))

# Build parameters.
NODE_ENV        ?= development
BASEURL         ?= https://securityinabox.org
PREFIX          ?=

# Directory configuration.
PO_DIR          ?= locales/po
SOURCE_DIR      ?= src

LOCALES_DIR     ?= locales
MS_SRC_DIRS     ?= layouts sass static
MEDIA_DIR       ?= media

TRANS_DIR       ?= translated
DIST_DIR        ?= dist

# Program locations and options.
JQ              ?= jq
NODE            ?= node
NPM             ?= npm
METALSMITH      ?= node_modules/.bin/metalsmith

PO4A_TRANS      ?= po4a-translate
PO4A_UPDPO      ?= po4a-updatepo
MSGCAT          ?= msgcat
MSGMERGE        ?= msgmerge
MSGINIT         ?= msginit

PO4A_UPDPO_OPTS ?= --no-deprecation -M $(CHARSET) -f $(FMT) $(FMT_OPTS) \
                   --wrap-po=newlines
PO4A_TRANS_OPTS ?= --no-deprecation -M $(CHARSET) -f $(FMT) $(FMT_OPTS) \
                   -k $(KEEP_THRES)
MSGCAT_OPTS     ?= --use-first
MSGMERGE_OPTS   ?= --update --backup=none --previous --no-wrap
MSGINIT_OPTS    ?= --no-translator --no-wrap

# Enumerate source and target files.
MS_CFG_JQ       ?= utility/set_vars.jq
MS_CFG_SRC      ?= metalsmith.tmpl.json
MS_CFG          ?= metalsmith.json

MD_FILES        := $(shell find $(SOURCE_DIR) -type f -name '*.md')
POT_FILES       := $(patsubst $(SOURCE_DIR)/%.md,$(PO_DIR)/%.pot,$(MD_FILES))
PO_FILES        := $(foreach lang,$(LINGUAS), \
                   $(patsubst %.pot,%.$(lang).po,$(POT_FILES)))
COMPENDIA       := $(foreach lang,$(LINGUAS),$(PO_DIR)/compendium.$(lang).po)
TR_MD_FILES     := $(foreach lang,$(PUB_LINGUAS), \
                   $(patsubst $(SOURCE_DIR)/%.md,$(TRANS_DIR)/$(lang)/%.md, \
                   $(MD_FILES)))
MS_FILES        := $(wildcard $(LOCALES_DIR)/*.json) \
                   $(shell find $(MS_SRC_DIRS) -type f)

all: build

#
# Targets for string extraction.
#

# `pot_updated` target: test if POT files are up-to-date.
# This target uses Git commit times to detect outdated files, and requires
# non-shallow clones.
pot_updated: restore-mtime
	@if ! $(MAKE) --question update_pot; then \
	  echo "ERROR: POT files are outdated, run 'make update_pot'" >&2; \
	  exit 1; \
	fi

# `update_pot` target: run after updating master documents.
update_pot: $(POT_FILES)

# `clean_pot` target: Remove POT files without source.
clean_pot:
	rm -f $(filter-out $(POT_FILES), \
	      $(shell find $(PO_DIR) -type f -name '*.pot'))

#
# Targets for maintaining translation files.
#

# `po_updated` target: test if PO files are up-to-date.
# This target uses Git commit times to detect outdated files, and requires
# non-shallow clones.
po_updated: restore-mtime
	@if ! $(MAKE) --question update_po; then \
	  echo "ERROR: PO files are outdated, run 'make update_po'" >&2; \
	  exit 1; \
	fi

# `update_po` target: Update PO files with changes made to master documents.
update_po: $(PO_FILES) compendia

# `compendia` target: Update PO files cointaining all existing translations for
# a specific language, to avoid losing translations when content is moved to a
# different file.
compendia: $(COMPENDIA)

# `clean_po` target: Remove PO files without a corresponding POT file.
clean_po:
	rm -f $(foreach lang, $(LINGUAS), \
	        $(filter-out $(PO_FILES_$(lang)), $(CUR_PO_$(lang))))

#
# Targets to build the website.
#

# `translate` target: run before generating website.
# This target uses the dynamically generated per-language targets.
translate: translate_stamp
translate_stamp: $(TR_MD_FILES)
	touch $@

# `build` target: build the whole website.
build: build_stamp
build_stamp: translate_stamp $(METALSMITH) $(MS_CFG) $(MS_FILES)
	rm -rf $(DIST_DIR)
	mkdir -p $(DIST_DIR)
	$(METALSMITH) -c $(MS_CFG) \
	  --env DEBUG="$(DEBUG)" --env NODE_ENV="$(NODE_ENV)"
	touch $@

#
# Pattern rules.
#

# Regenerate POT files for each source.
$(PO_DIR)/%.pot: $(SOURCE_DIR)/%.md
	mkdir -p `dirname $@`
	@# Remove existing POT file if it exists, so po4a only does extraction.
	rm -f $@ && \
	$(PO4A_UPDPO) $(PO4A_UPDPO_OPTS) -m $< -p $@

# `gen_translate_tgt` macro: hack to dynamically generate per-language targets.
define gen_translate_tgt
PO_FILES_$(1)   := $$(patsubst %.pot,%.$(1).po,$$(POT_FILES))
COMPENDIUM_$(1) := $$(PO_DIR)/compendium.$(1).po
CUR_PO_$(1)      = $$(filter-out $$(COMPENDIUM_$(1)), \
                   $$(shell find $$(PO_DIR) -type f -name '*.$(1).po' | sort))

# Create compendium of all existing translations, to use when initialising a
# new PO file.
$$(COMPENDIUM_$(1)): $$(CUR_PO_$(1))
	$$(MSGCAT) $$(MSGCAT_OPTS) -o $$@ $$^

# Create PO files, or update them when master documents change.
%.$(1).po: %.pot
	test -f $$@ || $(MSGINIT) $(MSGINIT_OPTS) --locale=$(1) -o $$@ -i $$<
	$(MSGMERGE) $(MSGMERGE_OPTS) --lang=$(1) -C $$(COMPENDIUM_$(1)) \
	    $$@ $$< && touch $$@

# Create translated MD file.
$(TRANS_DIR)/$(1)/%.md: $(SOURCE_DIR)/%.md $(PO_DIR)/%.$(1).po
	mkdir -p `dirname $$@`
	$(PO4A_TRANS) $(PO4A_TRANS_OPTS) \
	  -m $$< -p $(PO_DIR)/$$*.$(1).po -l $$@
	@# If the translation was discarded because it is below the treshold,
	@# copy the original file instead.
	test -f $$@ || cp -v $$< $$@
endef

# Run the `gen_translate_tgt` macro for each defined language.
$(foreach lang,$(LINGUAS), \
	$(eval $(call gen_translate_tgt,$(lang))))

# Special-cased translation target pattern to just copy the master document.
$(TRANS_DIR)/$(SRC_LANG)/%.md: $(SOURCE_DIR)/%.md
	mkdir -p `dirname $@`
	cp $< $@

#
# Utility targets.
#

# Adjust metalsmith configuration to match environment vars.
$(MS_CFG): $(MS_CFG_SRC) $(MS_CFG_JQ)
	$(JQ) -f $(MS_CFG_JQ) \
	  --arg source_dir '$(TRANS_DIR)' --arg dest_dir '$(DIST_DIR)' \
	  --arg prefix '$(PREFIX)' --arg site_url '$(BASEURL)' \
	  --arg linguas '$(PUB_LINGUAS)' --arg env '$(NODE_ENV)' \
	  $< > $@

# `restore-mtime` target: attempt to restore mtimes based on commit times.
restore-mtime:
	git restore-mtime --commit-time --merge --no-directories --quiet

# `install_merge_helper` target: adjust git config to use the PO files merge
# helper that Weblate uses.
install_merge_helper:
	git config merge.merge-po-files.driver \
	  './utility/git-merge-gettext-po %O %A %B %P'
	echo '*.po merge=merge-po-files' > $(CURDIR)/.git/info/attributes

# `npm_deps` target: install needed NPM packages for metalsmith.
npm_deps: $(METALSMITH)
$(METALSMITH): package-lock.json
	$(NODE) --version
	$(NPM) --version
	$(NPM) ci
	touch $@

# `serve` target: serve the built website with a local webserver.
serve: build
	$(NPM) exec http-serve -- -c-1 $(DIST_DIR)

#
# Clean-up targets.
#

# `cleanup-trans` target: remove translated files without source.
cleanup-trans:
	for lang in $(PUB_LINGUAS); do \
	  content_dir=$(TRANS_DIR)/$$lang; \
	  if [ -d $$content_dir ]; then \
	    find $$content_dir -type f -name \*.md | \
	      while read file; do \
	        srcfile=`realpath --relative-to=$$content_dir $$file`; \
	        test -f $(SOURCE_DIR)/$$srcfile || rm -v $$file; \
	      done; \
	  fi; \
	done

# `clean` target: remove all build files.
clean:
	rm -rf $(DIST_DIR) $(MS_CFG) build_stamp

# `distclean` target: remove all generated files.
distclean: clean
	rm -rf $(TRANS_DIR) translate_stamp

.PHONY: pot_updated update_pot clean_pot
.PHONY: po_updated compendia update_po clean_po translate build all
.PHONY: restore-mtime install_merge_helper npm_deps serve
.PHONY: cleanup-trans clean distclean
