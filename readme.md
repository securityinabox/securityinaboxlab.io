# Security in a Box

Security in a Box (or SiaB for short) is a free and open-source tool. If you would like to give us feedback, suggest improvements, contribute content or translate Siab to a new language, you will find all the instructions in this README file.

## How to give feedback

Security in a Box is an open-source resource and welcomes any feedback. You can suggest edits, changes or improvements by [sending us an email](#send_us_an_email), [opening a Gitlab issue](#open_a_gitlab_issue), or even [creating a merge request](#create_a_merge_request).

It's worth noting that we will probably not reply to your feedback immediately, but you can rest assured that we will read it carefully and, whenever appropriate, add it to our regular updates plans.

You can check out our [Gitlab issues](https://gitlab.com/securityinabox/securityinabox.gitlab.io/-/issues) to see our current editing and update plans and how we coordinate all suggestions to improve SiaB.

### Send us an email

If you would like to send feedback by email, you can write to siab @ frontlinedefenders . org.

To encrypt your message, you can use our [GnuPG public key](https://securityinabox.org/en/key).

### Open a Gitlab issue

Creating a Gitlab issue to give us feedback is the method we recommend as it makes it is easier to coordinate and plan changes and to track work.

To open an issue, you can write an email to [contact-project+securityinabox-securityinabox-gitlab-io-23208664-issue-@incoming.gitlab.com](mailto:contact-project+securityinabox-securityinabox-gitlab-io-23208664-issue-@incoming.gitlab.com) - this email will create a confidential issue that will only be visible to the members of the Security in a Box project.

Before you start, we suggest you check out the [Gitlab issues](https://gitlab.com/securityinabox/securityinabox.gitlab.io/-/issues) that have already been created in the SiaB project to make sure that your suggestion hasn't already been made by someone else. You can also use the search box to look for keywords connected to your feedback.

Once you've made sure similar feedback hasn't been already submitted to us, you can proceed to sending your email to our Service Desk. As you write, please take time to explain your point clearly so we can understand it and implement effectively the changes you suggest.

In particular, we recommend to follow the best practices regarding the main components of an issue:

- **Title** - The title will be your email subject. Write a short one-sentence title that matches the content of your feedback. Possibly include keywords so it's easier to find your feedback in the list of issues.

- **Description** - The description will be the body of your email. Provide a clear description of the change or improvement you would like to suggest. Describe both the issue you have noticed and the way you would correct it. Also please provide links to the pages where the issue occurs.

Finally, remember to *only describe one problem at a time*: if you have more than one suggestion, please open a separate issue for each one.


### Create a merge request

To create a merge request, follow the instructions in [the Step-by-Step Editing Guide for External Contributors](https://gitlab.com/securityinabox/securityinabox.gitlab.io/-/wikis/Editing-Guide-for-External-Contributors) in [the Security-in-a-Box wiki](https://gitlab.com/securityinabox/securityinabox.gitlab.io/-/wikis/home).

## Translating Security-in-a-Box content

If you would like to translate Security in a Box into a new language, see [our **Translation** guide](https://gitlab.com/securityinabox/securityinabox.gitlab.io/-/wikis/Translation) in [the Security-in-a-Box wiki](https://gitlab.com/securityinabox/securityinabox.gitlab.io/-/wikis/home).


## Maintainers guide

Security in a Box maintainers should be familiar with [the Edit and Maintenance Instructions](https://gitlab.com/securityinabox/securityinabox.gitlab.io/-/wikis/Maintainers-guide) in [the Security-in-a-Box wiki](https://gitlab.com/securityinabox/securityinabox.gitlab.io/-/wikis/home).
