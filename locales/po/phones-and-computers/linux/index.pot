# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-02-20 14:04+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Yaml Front Matter Hash Value: title
#: src/phones-and-computers/linux/index.md:1
#, no-wrap
msgid "Protect your Linux computer"
msgstr ""

#. type: Plain text
#: src/phones-and-computers/linux/index.md:9
#, markdown-text
msgid "If you use Linux, you may have heard the myth that it is more secure than Windows. This is not necessarily true. Security depends on a combination of how we use our devices and their own software, which can be found to have vulnerabilities at any time. While most malware does target more wide-spread operating systems like Windows or Android, you can still get malware as a Linux user if, for example, you click on an infected link or open a malicious attachment. [Read our guide on how to protect yourself against malware](../malware/)."
msgstr ""

#. type: Plain text
#: src/phones-and-computers/linux/index.md:11
#, markdown-text
msgid "Follow the steps in this guide to make your device more secure. Get in the habit of checking these settings from time to time, to make sure nothing has changed."
msgstr ""

#. type: Plain text
#: src/phones-and-computers/linux/index.md:13
#, markdown-text
msgid "This guide specifically covers [Ubuntu Linux](https://ubuntu.com/desktop). If you are using Ubuntu, this guide should be of use. If you are using another variety of Linux, this guide may be of use but you may want to look up comparable topics in the help or support section for the Linux distribution you are using."
msgstr ""

#. type: Title #
#: src/phones-and-computers/linux/index.md:14
#, markdown-text, no-wrap
msgid "If you have just decided to switch to Linux"
msgstr ""

#. type: Bullet: '- '
#: src/phones-and-computers/linux/index.md:19
#, markdown-text
msgid "[Follow the steps in the official Ubuntu installation guide](https://ubuntu.com/tutorials/install-ubuntu-desktop#1-overview)."
msgstr ""

#. type: Bullet: '- '
#: src/phones-and-computers/linux/index.md:19
#, markdown-text
msgid "Possibly encrypt your whole device by choosing the [Erase disk and install Ubuntu option and checking \"Use LVM and encryption\" in the Advanced features during Disk setup](https://ubuntu.com/tutorials/install-ubuntu-desktop#6-type-of-installation). When prompted, choose a [strong disk passphrase](../../passwords/passwords)."
msgstr ""

#. type: Bullet: '- '
#: src/phones-and-computers/linux/index.md:19
#, markdown-text
msgid "When [creating your login details](https://ubuntu.com/tutorials/install-ubuntu-desktop#7-create-your-login-details) make sure to set a [strong passphrase](../../passwords/passwords) and keep “Require my password to log in” enabled."
msgstr ""

#. type: Plain text
#: src/phones-and-computers/linux/index.md:21
#: src/phones-and-computers/linux/index.md:42
#: src/phones-and-computers/linux/index.md:56
#: src/phones-and-computers/linux/index.md:69
#: src/phones-and-computers/linux/index.md:79
#: src/phones-and-computers/linux/index.md:91
#: src/phones-and-computers/linux/index.md:108
#: src/phones-and-computers/linux/index.md:118
#: src/phones-and-computers/linux/index.md:128
#: src/phones-and-computers/linux/index.md:139
#: src/phones-and-computers/linux/index.md:150
#: src/phones-and-computers/linux/index.md:159
#: src/phones-and-computers/linux/index.md:171
#: src/phones-and-computers/linux/index.md:185
#: src/phones-and-computers/linux/index.md:201
#: src/phones-and-computers/linux/index.md:217
#: src/phones-and-computers/linux/index.md:227
#, markdown-text, no-wrap
msgid "<details><summary>Learn why we recommend this</summary>\n"
msgstr ""

#. type: Plain text
#: src/phones-and-computers/linux/index.md:24
#, markdown-text, no-wrap
msgid ""
"While technical attacks can be particularly worrying, your device may as well be confiscated or stolen, which may allow someone to break into it. For this reason, it is smart to set a strong passphrase to encrypt your computer and another strong passphrase to protect your user account, so that nobody can access your device or your sensitive files just by turning it on and guessing a short password.\n"
"</details>\n"
msgstr ""

#. type: Title #
#: src/phones-and-computers/linux/index.md:26
#, markdown-text, no-wrap
msgid "Use the latest version of your device's operating system (OS)"
msgstr ""

#. type: Bullet: '- '
#: src/phones-and-computers/linux/index.md:40
#, markdown-text
msgid "When updating software, do it from a trusted location and internet connection, like your home or office, not at an internet cafe or coffee shop."
msgstr ""

#. type: Bullet: '- '
#: src/phones-and-computers/linux/index.md:40
#, markdown-text
msgid "Updating to the latest version of your operating system may require you to download software and restart a number of times. You will want to set aside time for this where you do not need to do work on your device."
msgstr ""

#. type: Bullet: '- '
#: src/phones-and-computers/linux/index.md:40
#, markdown-text
msgid "After updating, check again if there are any further updates available until you do not see any additional new updates."
msgstr ""

#. type: Bullet: '- '
#: src/phones-and-computers/linux/index.md:40
#, markdown-text
msgid "Linux systems are generally known for being less demanding in terms of computer hardware, so if you use Linux you can generally use a device for a longer time than with other operating systems. Nevertheless, Ubuntu does have [minimum system requirements](https://help.ubuntu.com/community/Installation/SystemRequirements#Recommended_Minimum_System_Requirements) that are higher than other Linux distributions."
msgstr ""

#. type: Bullet: '    - '
#: src/phones-and-computers/linux/index.md:40
#, markdown-text
msgid "Ubuntu offers [long term support for 5 years for its main LTS versions](https://ubuntu.com/blog/what-is-an-ubuntu-lts-release). To keep receiving security updates on your Ubuntu version for up to 10 years, you can subscribe to [Ubuntu Pro](https://ubuntu.com/pro) (free for individuals)."
msgstr ""

#. type: Bullet: '    - '
#: src/phones-and-computers/linux/index.md:40
#, markdown-text
msgid "If the latest version of Ubuntu will not run on your device and Ubuntu Pro is not an option for you, you can switch to a Linux distribution specially created for older hardware, like [Lubuntu](https://lubuntu.me/)."
msgstr ""

#. type: Bullet: '    - '
#: src/phones-and-computers/linux/index.md:40
#, markdown-text
msgid "If you would like to upgrade to the newest version of Ubuntu, to use all of its functionalities, consider buying a new device."
msgstr ""

#. type: Bullet: '- '
#: src/phones-and-computers/linux/index.md:40
#, markdown-text
msgid "[Find out which is the most updated version of Ubuntu available](https://wiki.ubuntu.com/Releases)."
msgstr ""

#. type: Bullet: '- '
#: src/phones-and-computers/linux/index.md:40
#, markdown-text
msgid "[Compare it to the version your device has installed](https://help.ubuntu.com/community/CheckingYourUbuntuVersion)."
msgstr ""

#. type: Bullet: '- '
#: src/phones-and-computers/linux/index.md:40
#, markdown-text
msgid "Check the [minimum system requirements for the current stable Ubuntu version](https://help.ubuntu.com/community/Installation/SystemRequirements#Recommended_Minimum_System_Requirements) or test Ubuntu on your computer with a [LiveCD](https://help.ubuntu.com/community/LiveCD)."
msgstr ""

#. type: Bullet: '- '
#: src/phones-and-computers/linux/index.md:40
#, markdown-text
msgid "[Upgrade Ubuntu](https://ubuntu.com/tutorials/upgrading-ubuntu-desktop#1-before-you-start)."
msgstr ""

#. type: Bullet: '- '
#: src/phones-and-computers/linux/index.md:40
#, markdown-text
msgid "To make sure an update is fully installed, always restart your computer when prompted to do so after the update process is completed."
msgstr ""

#. type: Plain text
#: src/phones-and-computers/linux/index.md:45
#, markdown-text, no-wrap
msgid ""
"New vulnerabilities in the code that runs in your devices and apps are found every day. The developers who write that code cannot predict where they will be found, because the code is so complex. Malicious attackers may exploit these vulnerabilities to get into your devices. But software developers do regularly release code that fixes those vulnerabilities. That is why it is very important to install updates and use the latest version of the operating system for each device you use. We recommend setting your device to automatically update so you have one less task to remember to do.\n"
"</details>\n"
msgstr ""

#. type: Title #
#: src/phones-and-computers/linux/index.md:46
#, markdown-text, no-wrap
msgid "Regularly update all installed apps"
msgstr ""

#. type: Bullet: '- '
#: src/phones-and-computers/linux/index.md:49
#, markdown-text
msgid "[Make sure that your system updates all packages automatically](https://help.gnome.org/users/gnome-packagekit/stable/prefs.html.en)."
msgstr ""

#. type: Title #
#: src/phones-and-computers/linux/index.md:51
#, markdown-text, no-wrap
msgid "Use apps from trusted sources"
msgstr ""

#. type: Bullet: '- '
#: src/phones-and-computers/linux/index.md:54
#, markdown-text
msgid "[Install software using Ubuntu Software](https://help.ubuntu.com/lts/ubuntu-help/addremove-install.html.en)."
msgstr ""

#. type: Plain text
#: src/phones-and-computers/linux/index.md:58
#, markdown-text
msgid "Ubuntu Software is the program to use for installing applications and programs in Ubuntu. Install apps only from software repositories managed by these Linux distribution developers."
msgstr ""

#. type: Plain text
#: src/phones-and-computers/linux/index.md:60
#, markdown-text
msgid "If you decide that the benefit of a particular app outweighs the risk, take additional steps to protect yourself, like planning to keep sensitive or personal information off that device. Even in this case, only install apps from the websites of the developers themselves. \"Mirror\" download sites may be untrustworthy unless you know and trust the people who provide those services."
msgstr ""

#. type: Plain text
#: src/phones-and-computers/linux/index.md:63
#, markdown-text, no-wrap
msgid ""
"To learn how to decide whether you should use a certain app, see [how Security in a Box chooses the tools and services we recommend](../../about/how/#how-we-choose-the-tools-and-services-we-recommend).\n"
"</details>\n"
msgstr ""

#. type: Title #
#: src/phones-and-computers/linux/index.md:64
#, markdown-text, no-wrap
msgid "Remove apps that you do not need and do not use"
msgstr ""

#. type: Bullet: '- '
#: src/phones-and-computers/linux/index.md:67
#, markdown-text
msgid "[Learn how to remove applications](https://help.ubuntu.com/stable/ubuntu-help/addremove-remove)."
msgstr ""

#. type: Plain text
#: src/phones-and-computers/linux/index.md:72
#, markdown-text, no-wrap
msgid ""
"New vulnerabilities in the code that runs in your devices and apps are found every day. The developers who write that code cannot predict where they will be found, because the code is so complex. Malicious attackers may exploit these vulnerabilities to get into your devices. Removing apps you do not use helps limit the number of apps that might be vulnerable. Apps you do not use may also transmit information about you that you may not want to share with others, like your location.\n"
"</details>\n"
msgstr ""

#. type: Title #
#: src/phones-and-computers/linux/index.md:73
#, markdown-text, no-wrap
msgid "Check your app permissions and connected accounts"
msgstr ""

#. type: Bullet: '- '
#: src/phones-and-computers/linux/index.md:77
#, markdown-text
msgid "Review permissions and access to information of installed apps by going to Settings > Applications and clicking on each application in the left-hand menu."
msgstr ""

#. type: Bullet: '- '
#: src/phones-and-computers/linux/index.md:77
#, markdown-text
msgid "[Review online accounts you may have connected to your device (like Google, Facebook, and NextCloud)](https://help.ubuntu.com/stable/ubuntu-help/accounts.html) and check which [services](https://help.ubuntu.com/stable/ubuntu-help/accounts-which-application.html) they can [access](https://help.ubuntu.com/stable/ubuntu-help/accounts-disable-service.html). [Remove accounts](https://help.ubuntu.com/stable/ubuntu-help/accounts-remove.html) you do not use."
msgstr ""

#. type: Plain text
#: src/phones-and-computers/linux/index.md:82
#, markdown-text, no-wrap
msgid ""
"Apps that access sensitive digital details or services — like your location, microphone, camera or device settings — can also leak that information or be exploited by attackers. So if you do not need an app to use a particular service, turn that permission off.\n"
"</details>\n"
msgstr ""

#. type: Title #
#: src/phones-and-computers/linux/index.md:83
#, markdown-text, no-wrap
msgid "Turn off location and wipe history  "
msgstr ""

#. type: Bullet: '- '
#: src/phones-and-computers/linux/index.md:89
#, markdown-text
msgid "Get in the habit of turning off location services overall, or when you are not using them, for your whole device as well as for individual apps."
msgstr ""

#. type: Bullet: '- '
#: src/phones-and-computers/linux/index.md:89
#, markdown-text
msgid "Regularly check and clear your location history if you have it turned on."
msgstr ""

#. type: Bullet: '- '
#: src/phones-and-computers/linux/index.md:89
#, markdown-text
msgid "[Turn location services off](https://help.ubuntu.com/stable/ubuntu-help/privacy-location.html)."
msgstr ""

#. type: Bullet: '- '
#: src/phones-and-computers/linux/index.md:89
#, markdown-text
msgid "Regularly check and clear your location history in Google Maps if you use it. To delete past location history and set it so Google Maps does not save your location activity, follow the instructions [for your Google Maps Timeline](https://support.google.com/accounts/answer/3118687?#delete) and [your Maps activity](https://support.google.com/maps/answer/3137804)."
msgstr ""

#. type: Plain text
#: src/phones-and-computers/linux/index.md:94
#, markdown-text, no-wrap
msgid ""
"Many of our devices keep track of where we are, using GPS, cell phone towers and the wifi networks we connect to. If your device is keeping a record of your physical location, it makes it possible for someone to find you or use that record to prove that you have been in certain places or associated with specific people who were somewhere at the same time as you.\n"
"</details>\n"
msgstr ""

#. type: Title #
#: src/phones-and-computers/linux/index.md:95
#, markdown-text, no-wrap
msgid "Create separate user accounts on your devices"
msgstr ""

#. type: Bullet: '- '
#: src/phones-and-computers/linux/index.md:106
#, markdown-text
msgid "Create more than one user account on your device, with one having \"admin\" (administrative) privileges and the others \"standard\" (non-admin) privileges."
msgstr ""

#. type: Bullet: '    - '
#: src/phones-and-computers/linux/index.md:106
#, markdown-text
msgid "Only you should have access to the admin account."
msgstr ""

#. type: Bullet: '    - '
#: src/phones-and-computers/linux/index.md:106
#, markdown-text
msgid "Standard users should not be allowed to access every app, file or setting on your device."
msgstr ""

#. type: Bullet: '- '
#: src/phones-and-computers/linux/index.md:106
#, markdown-text
msgid "Consider using a standard user for your day-to-day work:"
msgstr ""

#. type: Bullet: '    - '
#: src/phones-and-computers/linux/index.md:106
#, markdown-text
msgid "Use the admin user only when you need to make changes that affect your device security, like installing software."
msgstr ""

#. type: Bullet: '    - '
#: src/phones-and-computers/linux/index.md:106
#, markdown-text
msgid "Using a standard user daily can limit how much your device is exposed to security threats from malware."
msgstr ""

#. type: Bullet: '    - '
#: src/phones-and-computers/linux/index.md:106
#, markdown-text
msgid "When you cross borders, having a \"travel\" user open could help hide your more sensitive files. Use your judgment: will the border authorities confiscate your device for a thorough search, or will they force you to log in to your account so they can take a quick look? If you expect they won't look too deeply into your device, being logged in as a standard user for work that is not sensitive provides you some plausible deniability."
msgstr ""

#. type: Bullet: '- '
#: src/phones-and-computers/linux/index.md:106
#, markdown-text
msgid "[Manage user accounts](https://help.ubuntu.com/stable/ubuntu-help/user-accounts.html)."
msgstr ""

#. type: Bullet: '- '
#: src/phones-and-computers/linux/index.md:106
#: src/phones-and-computers/linux/index.md:116
#, markdown-text
msgid "[Manage who has administrative privileges to change sensitive settings](https://help.ubuntu.com/stable/ubuntu-help/user-admin-change.html)."
msgstr ""

#. type: Plain text
#: src/phones-and-computers/linux/index.md:111
#, markdown-text, no-wrap
msgid ""
"We strongly recommend not sharing devices you use for sensitive work with anyone else. However, if you must share your devices with co-workers or family, you can better protect your device and sensitive information by setting up separate users on your devices in order to keep your administrative permissions and sensitive files protected from other people.\n"
"</details>\n"
msgstr ""

#. type: Title #
#: src/phones-and-computers/linux/index.md:112
#, markdown-text, no-wrap
msgid "Remove unneeded users associated with your device"
msgstr ""

#. type: Bullet: '- '
#: src/phones-and-computers/linux/index.md:116
#, markdown-text
msgid "[Remove unneeded accounts](https://help.ubuntu.com/stable/ubuntu-help/user-delete.html)."
msgstr ""

#. type: Plain text
#: src/phones-and-computers/linux/index.md:121
#, markdown-text, no-wrap
msgid ""
"If you don't intend for someone else to access your device, it is better to not leave that additional \"door\" open on your machine (this is called \"reducing your attack surface\"). Additionally, checking what users can access your device could reveal accounts that have been created on your device without your knowledge.\n"
"</details>\n"
msgstr ""

#. type: Title #
#: src/phones-and-computers/linux/index.md:122
#, markdown-text, no-wrap
msgid "Protect your user account with a strong passphrase"
msgstr ""

#. type: Bullet: '- '
#: src/phones-and-computers/linux/index.md:126
#, markdown-text
msgid "Use a [long passphrase](../../passwords/passwords) (longer than 16 characters), not a short password."
msgstr ""

#. type: Bullet: '- '
#: src/phones-and-computers/linux/index.md:126
#, markdown-text
msgid "To learn how to change your passphrase, see [the Ubuntu guide on changing passwords](https://help.ubuntu.com/stable/ubuntu-help/user-changepassword.html)."
msgstr ""

#. type: Plain text
#: src/phones-and-computers/linux/index.md:131
#, markdown-text, no-wrap
msgid ""
"While technical attacks can be particularly worrying, your device may as well be confiscated or stolen, which may allow someone to break into it. For this reason, it is smart to set a strong passphrase to protect your user account, so that nobody can access your device just by turning it on and guessing a short password.\n"
"</details>\n"
msgstr ""

#. type: Title #
#: src/phones-and-computers/linux/index.md:132
#, markdown-text, no-wrap
msgid "Set your screen to sleep and lock  "
msgstr ""

#. type: Bullet: '- '
#: src/phones-and-computers/linux/index.md:137
#, markdown-text
msgid "[Set your screen to lock a short time after you stop using it (try setting it to 1 minute or 5 minutes and see which works for you)](https://help.ubuntu.com/stable/ubuntu-help/privacy-screen-lock.html). To unlock your screen, you will need your user passphrase."
msgstr ""

#. type: Bullet: '- '
#: src/phones-and-computers/linux/index.md:137
#, markdown-text
msgid "Making it possible to use your fingerprint or face to unlock your computer can be used against you by force; do not use these options unless you have a disability which makes typing impossible."
msgstr ""

#. type: Bullet: '    - '
#: src/phones-and-computers/linux/index.md:137
#, markdown-text
msgid "If you have already entered your fingerprints in your device, you can remove them: go to Settings > Users, then click \"Enabled\" beside \"Fingerprint Login\" and select \"Delete Fingerprints\"."
msgstr ""

#. type: Plain text
#: src/phones-and-computers/linux/index.md:141
#, markdown-text
msgid "We do not recommend screen lock options other than passphrases. If you are arrested, detained or searched, you might easily be forced to unlock your device with your face or fingerprint. Someone who has your device in their possession may use software to guess short passwords. Someone who has dusted for your fingerprints can make a fake version of your finger to unlock your device if you set a fingerprint lock and similar hacks have been demonstrated for face unlock."
msgstr ""

#. type: Plain text
#: src/phones-and-computers/linux/index.md:144
#, markdown-text, no-wrap
msgid ""
"For these reasons, a long passphrase is the safest way to protect your user account.\n"
"</details>\n"
msgstr ""

#. type: Title #
#: src/phones-and-computers/linux/index.md:145
#, markdown-text, no-wrap
msgid "Control what can be seen when your device is locked"
msgstr ""

#. type: Bullet: '- '
#: src/phones-and-computers/linux/index.md:148
#, markdown-text
msgid "See [\"Hiding lock screen notifications\" in the Ubuntu documentation on notifications](https://help.ubuntu.com/stable/ubuntu-help/shell-notifications.html#lock-screen-notifications) to learn how to configure the lock screen to hide notifications."
msgstr ""

#. type: Plain text
#: src/phones-and-computers/linux/index.md:153
#, markdown-text, no-wrap
msgid ""
"A strong screen lock will give you some protection if your device is stolen or seized — but if you don't turn off notifications that show up on your lock screen, whoever has your device can see information that might leak when your contacts send you messages or you get new email.\n"
"</details>\n"
msgstr ""

#. type: Title #
#: src/phones-and-computers/linux/index.md:154
#, markdown-text, no-wrap
msgid "Use a physical privacy filter that prevents others from seeing your screen"
msgstr ""

#. type: Bullet: '- '
#: src/phones-and-computers/linux/index.md:157
#, markdown-text
msgid "For more information on this topic, see [the Security Planner guide on privacy filters](https://securityplanner.consumerreports.org/tool/use-a-privacy-filter-screen)."
msgstr ""

#. type: Plain text
#: src/phones-and-computers/linux/index.md:162
#, markdown-text, no-wrap
msgid ""
"While we often think of attacks on our digital security as highly technical, you might be surprised to learn that some human rights defenders have had their information stolen or their accounts compromised when someone looked over their shoulder at their screen or used a security camera to do so. A privacy filter makes this kind of attack, often called shoulder surfing, less likely to succeed. You should be able to find privacy filters in the same shops where you find other accessories for your devices.\n"
"</details>\n"
msgstr ""

#. type: Title #
#: src/phones-and-computers/linux/index.md:163
#, markdown-text, no-wrap
msgid "Use a camera cover"
msgstr ""

#. type: Bullet: '- '
#: src/phones-and-computers/linux/index.md:169
#, markdown-text
msgid "First of all, figure out whether and where your device has cameras. Your computer might have more than one if you use a plug-in camera as well as one built into your device."
msgstr ""

#. type: Bullet: '- '
#: src/phones-and-computers/linux/index.md:169
#, markdown-text
msgid "You can create a low-tech camera cover: apply a small adhesive bandage on your camera and peel it off when you need to use the camera. A bandage works better than a sticker because the middle part has no adhesive, so your lens won't get sticky."
msgstr ""

#. type: Bullet: '- '
#: src/phones-and-computers/linux/index.md:169
#, markdown-text
msgid "Alternatively, search your preferred store for the model of your computer and \"webcam privacy cover thin slide\" to find the most suitable sliding cover for your device."
msgstr ""

#. type: Bullet: '- '
#: src/phones-and-computers/linux/index.md:169
#, markdown-text
msgid "Advanced Linux users could also consider [disabling altogether camera access on their device](https://askubuntu.com/questions/166809/how-can-i-disable-my-webcam)."
msgstr ""

#. type: Plain text
#: src/phones-and-computers/linux/index.md:174
#, markdown-text, no-wrap
msgid ""
"Malicious software may turn on the camera on your device in order to spy on you and the people around you, or to find out where you are, without you knowing it.\n"
"</details>\n"
msgstr ""

#. type: Title #
#: src/phones-and-computers/linux/index.md:175
#, markdown-text, no-wrap
msgid "Turn off connectivity you're not using"
msgstr ""

#. type: Bullet: '- '
#: src/phones-and-computers/linux/index.md:183
#, markdown-text
msgid "Completely power off your devices at night."
msgstr ""

#. type: Bullet: '- '
#: src/phones-and-computers/linux/index.md:183
#, markdown-text
msgid "Get into habit of keeping wifi, Bluetooth and/or network sharing off and only enable them when you need to use them."
msgstr ""

#. type: Bullet: '- '
#: src/phones-and-computers/linux/index.md:183
#, markdown-text
msgid "Airplane mode can be a quick way to turn off connectivity on your computer. Learn how to enable Airplane mode to turn off all wireless connections, including wifi, 3G and Bluetooth connections in [the Ubuntu guide on how to turn off wireless ](https://help.ubuntu.com/stable/ubuntu-help/net-wireless-airplane.html)."
msgstr ""

#. type: Bullet: '- '
#: src/phones-and-computers/linux/index.md:183
#, markdown-text
msgid "[Make sure Bluetooth is off](https://help.ubuntu.com/stable/ubuntu-help/bluetooth-turn-on-off.html) unless you need to use it."
msgstr ""

#. type: Bullet: '- '
#: src/phones-and-computers/linux/index.md:183
#, markdown-text
msgid "Make sure your device is not providing an internet connection to someone else. Learn how to turn this setting off in the [Ubuntu guide on creating a wireless hotspot](https://help.ubuntu.com/stable/ubuntu-help/net-wireless-adhoc.html.en)."
msgstr ""

#. type: Bullet: '- '
#: src/phones-and-computers/linux/index.md:183
#, markdown-text
msgid "To turn off wifi, go to Settings > Wi-Fi and turn off the wifi switch in the top right corner."
msgstr ""

#. type: Plain text
#: src/phones-and-computers/linux/index.md:187
#, markdown-text
msgid "All wireless communication channels (like wifi, NFC or Bluetooth) could be abused by attackers around us who may try to get to our devices and sensitive information by exploiting weak spots in these networks."
msgstr ""

#. type: Plain text
#: src/phones-and-computers/linux/index.md:189
#, markdown-text
msgid "When you turn Bluetooth or wifi connectivity on, your device tries to look for any Bluetooth device or wifi network it remembers you have connected to before. Essentially, it \"shouts\" the names of every device or network on its list to see if they are available to connect to. Someone snooping nearby can use this \"shout\" to identify your device, because your list of devices or networks is usually unique. This fingerprint-like identification makes it easy for someone snooping close to you to target your device."
msgstr ""

#. type: Plain text
#: src/phones-and-computers/linux/index.md:192
#, markdown-text, no-wrap
msgid ""
"For these reasons, it is a good idea to turn off these connections when you are not using them, particularly wifi and Bluetooth. This limits the time an attacker might have to access your valuables without you noticing that something strange is happening on your device.\n"
"</details>\n"
msgstr ""

#. type: Title #
#: src/phones-and-computers/linux/index.md:193
#, markdown-text, no-wrap
msgid "Clear your saved wifi networks"
msgstr ""

#. type: Bullet: '- '
#: src/phones-and-computers/linux/index.md:199
#, markdown-text
msgid "Save network names and passwords in your [password manager](../../passwords/tools/#keepassxc) instead of in your device's list of networks."
msgstr ""

#. type: Bullet: '- '
#: src/phones-and-computers/linux/index.md:199
#, markdown-text
msgid "If you do save network names and passwords in your list of saved wifi networks, make sure to:"
msgstr ""

#. type: Bullet: '    - '
#: src/phones-and-computers/linux/index.md:199
#, markdown-text
msgid "uncheck the \"Connect automatically\" and \"Make available to other users\" options in the network configuration window: go to Settings > Wi-Fi, click the options button in the top right corner and select \"Known Wi-Fi Networks\". Then select the networks you want to configure."
msgstr ""

#. type: Bullet: '    - '
#: src/phones-and-computers/linux/index.md:199
#, markdown-text
msgid "get in the habit of regularly erasing saved wifi networks when you aren't using them anymore. To delete your saved wifi networks, go to Settings > Wi-Fi, click the options button in the top right corner and select \"Known Wi-Fi Networks\". Then select the networks you want to delete and click the \"Forget\" button."
msgstr ""

#. type: Plain text
#: src/phones-and-computers/linux/index.md:203
#, markdown-text
msgid "When you turn wifi connectivity on, your device tries to look for any wifi network it remembers you have connected to before. Essentially, it \"shouts\" the names of every network on its list to see if they are available to connect to. Someone snooping nearby can use this \"shout\" to identify your device, because your list is usually unique: you have probably at least connected to your home network and your office network, not to mention networks at friends' houses, favorite cafes, etc. This fingerprint-like identification makes it easy for someone snooping in your area to target your device or identify where you have been."
msgstr ""

#. type: Plain text
#: src/phones-and-computers/linux/index.md:206
#, markdown-text, no-wrap
msgid ""
"To protect yourself from this identification, erase wifi networks your device has saved and tell your device not to look for networks all the time. This will make it harder to connect quickly, but saving that information in your password manager instead will keep it available to you when you need it.\n"
"</details>\n"
msgstr ""

#. type: Title #
#: src/phones-and-computers/linux/index.md:207
#, markdown-text, no-wrap
msgid "Turn off sharing you're not using"
msgstr ""

#. type: Plain text
#: src/phones-and-computers/linux/index.md:210
#, markdown-text
msgid "Ubuntu may not automatically make it possible to share files, media or your desktop. If you know sharing is available on your device, check and see if you need to turn the following settings off. If you cannot find the settings using the instructions described here, you probably do not have these sharing options installed."
msgstr ""

#. type: Bullet: '- '
#: src/phones-and-computers/linux/index.md:215
#, markdown-text
msgid "[Sharing files](https://help.ubuntu.com/stable/ubuntu-help/sharing-personal.html)"
msgstr ""

#. type: Bullet: '- '
#: src/phones-and-computers/linux/index.md:215
#, markdown-text
msgid "[Sharing media](https://help.ubuntu.com/stable/ubuntu-help/sharing-media.html)"
msgstr ""

#. type: Bullet: '- '
#: src/phones-and-computers/linux/index.md:215
#, markdown-text
msgid "[Sharing through Bluetooth](https://help.ubuntu.com/stable/ubuntu-help/sharing-bluetooth.html)"
msgstr ""

#. type: Bullet: '- '
#: src/phones-and-computers/linux/index.md:215
#, markdown-text
msgid "[Sharing your desktop](https://help.ubuntu.com/stable/ubuntu-help/sharing-desktop.html)"
msgstr ""

#. type: Plain text
#: src/phones-and-computers/linux/index.md:220
#, markdown-text, no-wrap
msgid ""
"Many devices give us the option to easily share files or services with others around us — a useful feature. However, if you leave this feature on when you are not using it, malicious people may exploit it to get at files on your device.\n"
"</details>\n"
msgstr ""

#. type: Title #
#: src/phones-and-computers/linux/index.md:221
#, markdown-text, no-wrap
msgid "Use a firewall"
msgstr ""

#. type: Bullet: '- '
#: src/phones-and-computers/linux/index.md:225
#, markdown-text
msgid "[Use Gufw](https://help.ubuntu.com/community/Gufw)."
msgstr ""

#. type: Bullet: '   - '
#: src/phones-and-computers/linux/index.md:225
#, markdown-text
msgid "As a default, set \"Incoming\" to \"Deny\" and \"Outgoing\" to \"Allow.\""
msgstr ""

#. type: Plain text
#: src/phones-and-computers/linux/index.md:229
#, markdown-text
msgid "Firewalls are a security option that stops unwanted connections to your device. Like a security guard posted at the door of a building to decide who can enter and who can leave, a firewall receives, inspects and makes decisions about communications going in and out of your device. We recommend turning yours on to prevent malicious code from trying to access your device. The default firewall configuration should be enough protection for most people."
msgstr ""

#. type: Plain text
#: src/phones-and-computers/linux/index.md:231
#, markdown-text
msgid "Not all devices come with a firewall turned on. This does not mean these systems are wide open to all network connections. It just means they trust their software not to be listening when it shouldn't. They are like building owners that do not bother with guards and cameras because they are confident about which doors are unlocked, which are barricaded and which will open only for people with certain keys."
msgstr ""

#. type: Plain text
#: src/phones-and-computers/linux/index.md:234
#, markdown-text, no-wrap
msgid ""
"Firewalls help us protect our devices in situations where a piece of software starts listening to information we weren't expecting it to — where a door is left open, in other words, either by accident or by a malicious person within the building. Firewalls that monitor outgoing connections are sometimes able to let us know when malicious software is trying to steal data or \"phone home\" for instructions. If you install a firewall that is specifically designed to limit outgoing connections, or if you configure your built-in firewall to work this way, you should be prepared to spend some time \"training\" it so that it only alerts you when it observes something unusual.\n"
"</details>\n"
msgstr ""

#. type: Title #
#: src/phones-and-computers/linux/index.md:235
#, markdown-text, no-wrap
msgid "See also"
msgstr ""

#. type: Bullet: '- '
#: src/phones-and-computers/linux/index.md:238
#, markdown-text
msgid "[Security in a Box malware guide](../malware)"
msgstr ""

#. type: Bullet: '- '
#: src/phones-and-computers/linux/index.md:238
#, markdown-text
msgid "[Linux privacy tips from Spread Privacy](https://spreadprivacy.com/linux-privacy-tips/)"
msgstr ""
