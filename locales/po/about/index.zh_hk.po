# Chinese translations for PACKAGE package
# Copyright (C) 2024 Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# Automatically generated, 2024.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-12-12 21:02+0000\n"
"PO-Revision-Date: 2024-03-04 20:41+0000\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: zh_hk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Yaml Front Matter Hash Value: title
#: src/about/index.md:1
#, no-wrap
msgid "About"
msgstr ""
