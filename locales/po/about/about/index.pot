# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-03-05 10:55+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Yaml Front Matter Hash Value: title
#: src/about/about/index.md:1
#, no-wrap
msgid "About Security-in-a-Box"
msgstr ""

#. type: Plain text
#: src/about/about/index.md:7
#, markdown-text
msgid "Security in a Box (SiaB) is a project of [Front Line Defenders](http://www.frontlinedefenders.org)."
msgstr ""

#. type: Plain text
#: src/about/about/index.md:9
#, markdown-text
msgid "Created in 2005 (as [NGO in a Box - Security edition](https://web.archive.org/web/20060209174038/http://security.ngoinabox.org/)) in collaboration with [Tactical Technology Collective](http://www.tacticaltech.org) and then [renamed to its current title in 2009](https://web.archive.org/web/20090326230528/http://security.ngoinabox.org), it was significantly overhauled by Front Line Defenders in 2021 and is undergoing a continuous update process."
msgstr ""

#. type: Plain text
#: src/about/about/index.md:11
#, markdown-text
msgid "Security in a Box primarily aims to help a global community of human rights defenders whose work puts them at risk. It has been recognized worldwide as a foundational resource for helping people at risk protect their digital security and privacy."
msgstr ""

#. type: Plain text
#: src/about/about/index.md:13
#, markdown-text
msgid "Security in a Box is a free and open-source tool hosted on [Gitlab.com](https://gitlab.com/securityinabox/securityinabox.gitlab.io)."
msgstr ""

#. type: Plain text
#: src/about/about/index.md:15
#, markdown-text
msgid "If you would like to give us feedback or contribute to Security in a Box, please follow [the instructions in the Readme file of the SiaB project on Gitlab](https://gitlab.com/securityinabox/securityinabox.gitlab.io/-/blob/main/readme.md)."
msgstr ""

#. type: Title ##
#: src/about/about/index.md:16
#, markdown-text, no-wrap
msgid "Access Security in a Box anonymously"
msgstr ""

#. type: Plain text
#: src/about/about/index.md:19
#, markdown-text
msgid "To access Security in a Box anonymously using the Tor Browser, you can visit the [*onion service*](https://community.torproject.org/onion-services/overview/) below:"
msgstr ""

#. type: Plain text
#: src/about/about/index.md:21
#, markdown-text
msgid "[http://lxjacvxrozjlxd7pqced7dyefnbityrwqjosuuaqponlg3v7esifrzad.onion/en/](http://lxjacvxrozjlxd7pqced7dyefnbityrwqjosuuaqponlg3v7esifrzad.onion/en/)"
msgstr ""

#. type: Title ##
#: src/about/about/index.md:22
#, markdown-text, no-wrap
msgid "About Front Line Defenders"
msgstr ""

#. type: Plain text
#: src/about/about/index.md:25
#, markdown-text
msgid "Front Line Defenders was founded with the specific aim of protecting human rights defenders at risk, people who work, non-violently, for any or all of the rights enshrined in the Universal Declaration of Human Rights (UDHR). Front Line Defenders aims to address some of the needs identified by defenders themselves, including protection, networking, training and access to international bodies that can take action on their behalf."
msgstr ""

#. type: Title ##
#: src/about/about/index.md:26
#, markdown-text, no-wrap
msgid "Funders"
msgstr ""

#. type: Plain text
#: src/about/about/index.md:29
#, markdown-text
msgid "The development of Security in a Box has been supported by [Hivos](https://hivos.org/), [Internews](http://www.internews.eu/), [Sida](http://www.sida.se/English/), [Oak Foundation](http://www.oakfnd.org/), [Sigrid Rausing Fund](https://www.sigrid-rausing-trust.org/), [AJWS](http://ajws.org/), [Open Society Foundations](http://www.opensocietyfoundations.org/), [Ford Foundation](http://www.fordfoundation.org/), and [EIDHR](http://ec.europa.eu/europeaid/index_en.htm)."
msgstr ""

#. type: Plain text
#: src/about/about/index.md:31
#, markdown-text
msgid "This website has been produced with the assistance of the European Union. Nevertheless, the contents of this website are the responsibility of Front Line Defenders and can in no way be taken to reflect the views of the European Union."
msgstr ""

#. type: Title ##
#: src/about/about/index.md:32
#, markdown-text, no-wrap
msgid "License"
msgstr ""

#. type: Plain text
#: src/about/about/index.md:34
#, markdown-text
msgid "This work is licensed under a [Creative Commons Attribution-Share Alike 4.0 Unported License](https://creativecommons.org/licenses/by-sa/4.0/). We strongly encourage the re-use of the material in Security in a Box."
msgstr ""
