# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-12-12 21:02+0000\n"
"PO-Revision-Date: 2024-12-28 22:11+0000\n"
"Last-Translator: Sergey Smirnov <cj75300@gmail.com>\n"
"Language-Team: Russian <https://weblate.securityinabox.org/projects/info/about-how/ru/>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Weblate 4.12.2\n"

#. type: Title #
#: src/about/how/index.md:1 src/about/how/index.md:6
#, no-wrap
msgid "How does Security in a Box work?"
msgstr "Принципы \"Безопасности-в-коробке\""

#. type: Plain text
#: src/about/how/index.md:9
msgid "Security in a Box is a free and open-source resource hosted on [Gitlab.com](https://gitlab.com/securityinabox/securityinabox.gitlab.io)."
msgstr "\"Безопасность-в-коробке\" — бесплатный ресурс, и его открытый код размещен на [Gitlab.com](https://gitlab.com/securityinabox/securityinabox.gitlab.io)."

#. type: Plain text
#: src/about/how/index.md:11
msgid "Security in a Box is constantly updated thanks to the inputs of Front Line Defenders' Digital Protection Team as well as external contributions. All updates and changes are coordinated by Front Line Defenders' Digital Protection Editor."
msgstr "\"Безопасность-в-коробке\" постоянно обновляется стараниями экспертов по цифровой безопасности Front Line Defenders и других специалистов. Все изменения и дополнения осуществляются редактором материалов по цифровой безопасности Front Line Defenders."

#. type: Plain text
#: src/about/how/index.md:13
msgid "All content included in Security in a Box is accurately selected based on the following criteria."
msgstr "Далее мы описываем критерии, по которым готовим материалы \"Безопасности-в-коробке\"."

#. type: Title ##
#: src/about/how/index.md:14
#, no-wrap
msgid "How we develop the protection strategies we recommend"
msgstr "Откуда взялись именно эти рекомендуемые стратегии защиты"

#. type: Plain text
#: src/about/how/index.md:17
msgid "Security in a Box is a resource that includes many digital protection strategies. Browsing this website, you will find recommendations on how to create and manage strong passwords and on how to protect your accounts, communications, devices, internet connection, and sensitive information."
msgstr "\"Безопасность-в-коробке\" включает много рекомендаций по цифровой безопасности. На страницах нашего сайта вы обнаружите советы о том, как создавать и хранить надежные пароли, как защищать аккаунты, коммуникации, устройства, интернет-подключение и ваши самые важные данные."

#. type: Plain text
#: src/about/how/index.md:19
msgid "All these strategies are focused on the protection of digital assets, but they are developed in a holistic way: even if what we want to help you protect is digital, we know that protection always ranges across different spheres, including the legal and psycho-social context as well as a need to look at the physical level. For more information on what a holistic approach to protection implies, we recommend reading about the [holistic approach](https://holistic-security.tacticaltech.org/chapters/prepare/chapter-1-1-what-is-holistic-security.html) in the [Holistic Security Manual](https://holistic-security.tacticaltech.org)."
msgstr "Хотя все эти стратегии относятся к цифровым ценностям, мы использовали комплексный подход. Даже если мы хотим защитить некую цифровую ценность, наши действия затронут несколько разных областей. Мы коснемся юридических, психо-социальных, физических аспектов. Подробнее о комплексном подходе к безопасности читайте [в этой части](https://holistic-security.tacticaltech.org/chapters/prepare/chapter-1-1-what-is-holistic-security.html) [Руководства по комплексной безопасности](https://holistic-security.tacticaltech.org)."

#. type: Plain text
#: src/about/how/index.md:21
msgid "Security in a Box primarily aims to help a global community of human rights defenders whose work puts them at risk. In general, we consider this resource aimed at helping not only human rights defenders but all those who are at risk of digital attacks, for example activists, journalists, and other members of civil society, as well as women and LGBTQIA+ persons who are at risk of gender-based online violence."
msgstr "Главная задача \"Безопасности-в-коробке\" — помощь правозащитникам \"из группы риска\" по всему миру. Если смотреть шире, \"Безопасность-в-коробке\" может пригодиться не только правозащитникам, но и другим людям с высоким уровнем цифровых угроз: активистам, журналистам и другим членам гражданского общества, а также женщинам и ЛГБТ-людям, которые подвергаются онлайновым угрозам в связи с их гендером."

#. type: Plain text
#: src/about/how/index.md:23
msgid "A central part of our approach is enabling our target users to assess the risks that they face and define their own needs. We aim to do no harm to human rights defenders, their families and communities, as well as to other readers of SiaB. We also recognise that listening with empathy to human rights defenders and other users at risk of digital violence and understanding the situations they are in is key to providing effective protection support. So the strategies described in SiaB are developed with an approach that aims at empowering our readers and are based on our users' actual needs and goals, in keeping with Front Line Defenders' [Vision, Mission & Core Values](https://www.frontlinedefenders.org/en/vision-mission-core-values)."
msgstr "Сущность нашего подхода в том, что целевая аудитория должна иметь возможность взвесить свои риски и определить потребности. Мы стараемся не допустить ущерба для правозащитников, членов их семей и сообщества, как и для всех читателей SiaB. По нашему мнению, эффективная помощь зависит от того, можем ли мы с эмпатией выслушивать правозащитников и вообще людей с высоким уровнем цифровых рисков, способны ли мы понять их ситуацию. Стратегии, описанные в SiaB, разрабатывались ради укрепления безопасности наших читателей. Эти рекомендации основаны на их реальных нуждах и задачах в соответствии с разделом [\"Видение, миссия и основные ценности\"](https://www.frontlinedefenders.org/ru/vision-mission-core-values) на сайте Front Line Defenders."

#. type: Title ##
#: src/about/how/index.md:24
#, no-wrap
msgid "How we choose the tools and services we recommend"
msgstr "Почему мы рекомендуем именно эти инструменты и сервисы"

#. type: Plain text
#: src/about/how/index.md:27
msgid "Software is complex, and it is not all created equal when it comes to its security- and privacy-protecting properties. Different tools and services can be considered more or less effective depending on the jurisdiction you are in, its laws, and the adversaries you may face."
msgstr "Компьютерные программы сложны. В них по-разному обеспечиваются безопасность и конфиденциальность. Эффективность инструментов и сервисов зависит от вашей юрисдикции, действующих законов, а также противников, с которыми вы можете столкнуться."

#. type: Plain text
#: src/about/how/index.md:29
msgid "We take a number of factors into account when selecting the tools and services that we recommend in Security in a Box. Each factor is important. Because different areas have different legal requirements for technology and face different threats, it is difficult to rank the importance of each factor globally."
msgstr "При отборе инструментов и сервисов для \"Безопасности-в-коробке\" мы учитываем целый ряд факторов. Каждый из них важен. В разных регионах существуют разные законодательные требования к технологиям, разные угрозы. Поэтому трудно оценить важность отдельного фактора в глобальном масштабе."

#. type: Plain text
#: src/about/how/index.md:31
msgid "Below, we list what we consider the most important questions we ask when we consider which tools and services we recommend. You can also make use of these criteria when you need to assess the relative safety of tools that are not listed in Security in a Box."
msgstr "Далее следует список самых важных, на наш взгляд, вопросов, которые мы задаем при выборе рекомендуемых инструментов и сервисов. Можете использовать эти критерии также для тех инструментов, которых нет в \"Безопасности-в-коробке\"."

#. type: Title ###
#: src/about/how/index.md:33
#, no-wrap
msgid "Can the people who develop this tool or operate this service be trusted?"
msgstr "Можно ли доверять разработчикам инструмента и тем, кто предоставляет сервис?"

#. type: Bullet: '- '
#: src/about/how/index.md:38
msgid "What is the history of the development and ownership of the tool or service? Has it been created by activists or by a company? Does this group have a public face, and what's its background?"
msgstr "Какова история разработки и владения инструментом или сервисом? Кто его создавал — активисты или коммерческая компания? Есть ли у создателя свое публичное представительство? Какова его история?"

#. type: Bullet: '- '
#: src/about/how/index.md:38
msgid "What are the mission and business model of the entity that develops or operates this tool or service?"
msgstr "Каковы миссия и бизнес-модель того, кто разработал и/или распоряжается инструментом (сервисом)?"

#. type: Bullet: '- '
#: src/about/how/index.md:38
msgid "Have there been any security challenges, for example data breaches or requests for information by state authorities? How have the service provider/tool developers reacted to those challenges? Have they openly addressed problems, or have they tried to cover them up?"
msgstr "Случались ли инциденты безопасности (например, утечки данных или эпизоды, когда правительство требовало какую-либо информацию)? Как провайдер сервиса (разработчик инструмента) реагировал на такое? Решали проблему, сохраняя прозрачность, или пытались замять историю?"

#. type: Title ###
#: src/about/how/index.md:39
#, no-wrap
msgid "Does the tool encrypt data? With what kind of encryption technology?"
msgstr "Используется ли шифрование? Если да, то какое?"

#. type: Bullet: '- '
#: src/about/how/index.md:45
msgid "Does the tool encrypt the connection between the user and the people they are communicating with (end-to-end encryption), making it impossible for service providers to access their information?"
msgstr "Поддерживается ли сквозное шифрование для безопасности отправителя и получателя? Позволяет ли инструмент скрыть данные от провайдера сервиса?"

#. type: Bullet: '- '
#: src/about/how/index.md:45
msgid "If end-to-end encryption is not available, we prioritize tools that encrypt the data between the user's device and the service infrastructure (to-server encryption). In such cases we always remind our readers that if data is not encrypted in the servers, they have to trust the people managing this service, as they have potential free access to the information they have stored there."
msgstr "Если сквозное шифрование недоступно, мы обращаем внимание, чтобы шифровались данные между устройством и сервисом. В этом случае мы всегда напоминаем читателям, что данные не зашифрованы на серверах, и что им придется доверять операторам сервиса — ведь оператор в принципе может получить свободный доступ к хранимой там информации."

#. type: Bullet: '- '
#: src/about/how/index.md:45
msgid "Do the default settings protect users' privacy and security?"
msgstr "Позволяют ли настройки по умолчанию эффективно защищать приватность и обеспечивать безопасность?"

#. type: Bullet: '- '
#: src/about/how/index.md:45
msgid "If no end-to-end encryption is available, does the technology allow users to host the service in their own infrastructure, so they can control who can access their data?"
msgstr "Если сквозное шифрование недоступно, может ли пользователь установить сервис на собственный сервер, чтобы самому контролировать доступ к данным?"

#. type: Title ###
#: src/about/how/index.md:47
#, no-wrap
msgid "Is the code available to inspect?"
msgstr "Доступен ли программный код для анализа?"

#. type: Bullet: '- '
#: src/about/how/index.md:54
msgid "In other words, is it open-source?"
msgstr "Иными словами, открыт ли код?"

#. type: Bullet: '- '
#: src/about/how/index.md:54
msgid "Has the tool or service been audited by security experts who are independent of the software development project or of the service provider company?"
msgstr "Проводился ли аудит этого инструмента? (С привлечением экспертов по безопасности, которые не зависят от разработчика инструмента или провайдера сервиса)"

#. type: Bullet: '- '
#: src/about/how/index.md:54
msgid "When was the last audit? Are regular audits planned?"
msgstr "Когда проводился последний аудит? Планируются ли регулярные аудиты в будущем?"

#. type: Bullet: '- '
#: src/about/how/index.md:54
msgid "Has the audit included all parts of the tool or service, or just some of its parts? If it is a service, has the server infrastructure been audited, or just the user interface?"
msgstr "Аудит касался всех компонентов инструмента/сервиса или только некоторых? Если речь о сервисе, проводился ли аудит инфраструктуры сервиса, или изучался только пользовательский интерфейс?"

#. type: Bullet: '- '
#: src/about/how/index.md:54
msgid "What do independent experts say about the tool or service?"
msgstr "Что говорят об этом инструменте/сервисе независимые эксперты?"

#. type: Title ###
#: src/about/how/index.md:55
#, no-wrap
msgid "Is the technology mature?"
msgstr "Насколько признана сама технология?"

#. type: Bullet: '- '
#: src/about/how/index.md:60
msgid "How long has this technology been in operation? Has it passed the test of time?"
msgstr "Как давно существует технология? Прошла ли она испытание временем?"

#. type: Bullet: '- '
#: src/about/how/index.md:60
msgid "Does it have a large community of developers who are still actively working on its development?"
msgstr "Есть ли у проекта большое и активное сообщество разработчиков, которые трудятся над улучшениями?"

#. type: Bullet: '- '
#: src/about/how/index.md:60
msgid "How many active users does it have?"
msgstr "Сколько у него активных пользователей?"

#. type: Title ###
#: src/about/how/index.md:61
#, no-wrap
msgid "Where are the servers located?"
msgstr "Где находятся серверы?"

#. type: Bullet: '- '
#: src/about/how/index.md:66
msgid "This can be a difficult question to answer, with more and more services in the cloud, but trustworthy developers and service providers tend to publish this information in their websites or to make it available through trusted intermediaries."
msgstr "По мере роста популярности облачных решений на этот вопрос становится ответить все сложнее. Однако надежные разработчики и провайдеры сервисов сами публикуют такую информацию на своих сайтах или делают ее доступной через своих доверенных лиц."

#. type: Bullet: '- '
#: src/about/how/index.md:66
msgid "Also drawing conclusions from this piece of information can be tricky for us, as the servers might be located in a country that would protect HRDs and other civil society members in some states but not in others."
msgstr "Делать однозначный вывод в этом случае может быть непросто. Серверы порой находятся в странах, где защита правозащитников и других членов гражданского общества варьируется от одного региона/штата к другому."

#. type: Bullet: '- '
#: src/about/how/index.md:66
msgid "In general we ask ourselves whether the servers are located in a country that would comply with a request by authorities from countries where there is no rule of law, and whether that country enforces human rights and consumer protection. In a nutshell, the question is: do authorities in non-democratic countries have the legal right to seize data or access information or shut down these services because of where the servers are located?"
msgstr "Обычно мы пробуем представить, что произойдет, если за информацией к владельцу сервиса обратится правительство страны, где нет верховенства закона. Пойдет ли провайдер навстречу? Есть ли в стране провайдера работающие механизмы защиты прав человека и прав потребителя? Смогут ли власти какой-нибудь недемократической страны на законных основаниях получить доступ к данным или прекратить работу серверов?"

#. type: Title ###
#: src/about/how/index.md:67
#, no-wrap
msgid "What personal information does it require from users? What does the owner/operator have access to?"
msgstr "Какие персональные данные они от вас хотят? К чему (каким данным) имеет доступ владелец сервиса/оператор?"

#. type: Bullet: '- '
#: src/about/how/index.md:74
msgid "Does the tool or service ask users to provide their phone number, email, nickname, or other personally identifiable information?"
msgstr "Спрашивает ли инструмент (сервис) у пользователей их телефонные номера, адреса email, никнеймы, прочую идентифицирующую информацию?"

#. type: Bullet: '- '
#: src/about/how/index.md:74
msgid "Do they require to install a dedicated app/program that might potentially track users?"
msgstr "Нужно ли устанавливать ту или иную программу/приложение, которое потенциально может следить за пользователями?"

#. type: Bullet: '- '
#: src/about/how/index.md:74
msgid "What does their privacy policy declare? Are they protecting users' privacy and complying with privacy laws like the EU General Data Protection Regulation?"
msgstr "Что написано в политике приватности? Собираются ли ее авторы защищать приватность пользователей? Соответствует ли политика основным документам в области приватности, таким как GDPR (Европейский союз)?"

#. type: Bullet: '- '
#: src/about/how/index.md:74
msgid "What is stored on the servers? Do the company's terms and conditions give the owner the right to access users' data? For what purposes?"
msgstr "Что хранится на серверах? Дают ли корпоративные условия обработки данных владельцу право доступа к информации пользователей? С какими целями?"

#. type: Bullet: '- '
#: src/about/how/index.md:74
msgid "What will this app/program have access to on a device: address book, location, microphone, camera, etc.? Can these permissions be deactivated, or will the app/program stop working in such case?"
msgstr "К чему на устройстве имеет доступ это приложение/программа? Адресная книга, местонахождение, микрофон, камера, что-то еще? Можно ли отключить эти разрешения, или программа/приложение из-за этого перестанет работать?"

#. type: Title ###
#: src/about/how/index.md:75
#, no-wrap
msgid "What's the price? Is it affordable?"
msgstr "Насколько дорогой этот инструмент?"

#. type: Bullet: '- '
#: src/about/how/index.md:80
msgid "When considering a service or tool, we consider its cost. Is it affordable? What is its developers' or provider's business model?"
msgstr "Когда мы рассматриваем сервис или инструмент, его стоимость имеет значение. По карману ли он пользователю? Какова бизнес-модель разработчика (провайдера сервиса)?"

#. type: Bullet: '- '
#: src/about/how/index.md:80
msgid "In addition to up-front payment, we consider hosting costs, potential subscription fees, the cost of learning how to use and manage the tool or service, the need for possible IT support and/or additional equipment, etc."
msgstr "Помимо собственно платы за программу следует учесть расходы на хостинг, подписку (если распространяется по подписке), стоимость обучения тому, как настраивать и использовать программу (сервис). Сюда нужно добавить возможные расходы на техподдержку и/или дополнительное оборудование, и т.д."

#. type: Bullet: '- '
#: src/about/how/index.md:80
msgid "If the tool or service is provided for free, we ask ourselves why. Who is paying for the project if the tool/service is not paid? Is it free because it is based on volunteer work or because it is funded by governments or companies, or is the company actually making money by selling its users' data to third parties?"
msgstr "Если инструмент (сервис) предлагаются бесплатно, мы спрашиваем себя \"почему\". Кто финансирует проект, если не потребитель? Задействован ли здесь труд волонтеров? Деньги приходят от государства? Коммерческих компаний? А может, разработчик (владелец) получает прибыль, продавая пользовательские данные третьим лицам?"

#. type: Title ###
#: src/about/how/index.md:81
#, no-wrap
msgid "Is it available on multiple operating systems and devices?"
msgstr "Поддерживаются ли разные операционные системы и устройства?"

#. type: Bullet: '- '
#: src/about/how/index.md:84
msgid "Can the tool or service be used both on computers and mobile devices? Is it only available on proprietary operating systems like macOS and iOS or also on Linux? If it is not available on all operating systems, is there a reliable alternative for other devices where this tool/service cannot be used?"
msgstr "Можно ли использовать инструмент (сервис) и на компьютерах, и на мобильных устройствах? Он доступен только для проприетарных операционных систем вроде macOS (iOS) или также будет работать в Linux? Если он не работает на всех операционных системах, существует ли надежная альтернатива для тех устройств, где этот инструмент/сервис не работает?"

#. type: Title ###
#: src/about/how/index.md:85
#, no-wrap
msgid "Is it user-friendly? Will it work for people at risk?"
msgstr "Он дружелюбный к пользователю? Как его воспримут люди из \"группы риска\"?"

#. type: Bullet: '- '
#: src/about/how/index.md:90
msgid "Is it confusing or frustrating to use this tool safely, or have the developers/provider made an effort to make it user-friendly?"
msgstr "Этот инструмент запутанный и неудобный? Или разработчик/провайдер постарался сделать его дружелюбным?"

#. type: Bullet: '- '
#: src/about/how/index.md:90
msgid "Do human rights defenders and other people at risk keep using this tool or service, or do they tend to abandon it?"
msgstr "Используют ли этот инструмент/сервис правозащитники и другие люди? Или они стараются избегать его?"

#. type: Bullet: '- '
#: src/about/how/index.md:90
msgid "Is the user interface intuitive? Or, alternatively, has documentation been developed to clearly explain how to use the tool or service?"
msgstr "Можно ли сказать, что пользовательский интерфейс интуитивный? А может быть, есть документация, которая четко объясняет, как использовать инструмент/сервис?"

#. type: Title ###
#: src/about/how/index.md:91
#, no-wrap
msgid "Is it translated into several languages?"
msgstr "Он переведен на разные языки?"

#. type: Bullet: '- '
#: src/about/how/index.md:96
msgid "Is the tool or service localized? Into how many languages?"
msgstr "Локализован ли этот инструмент/сервис? На сколько языков?"

#. type: Bullet: '- '
#: src/about/how/index.md:96
msgid "Is the localization continuously updated?"
msgstr "Обновляется ли локализация?"

#. type: Bullet: '- '
#: src/about/how/index.md:96
msgid "What is the quality of the localization?"
msgstr "Насколько хорош перевод?"

#. type: Bullet: '- '
#: src/about/how/index.md:96
msgid "Is multi-lingual documentation also available to help users understand how to safely use it?"
msgstr "Доступна ли документация на разных языках, которая помогает разобраться с безопасным использованием инструмента/сервиса?"

#, no-wrap
#~ msgid "About Security-in-a-Box"
#~ msgstr "О \"Безопасности-в-коробке\""

#~ msgid "Security in a Box is a project of [Front Line Defenders](http://www.frontlinedefenders.org) It was created in 2007 in collaboration with [Tactical Technology Collective](http://www.tacticaltech.org), and significantly overhauled by Front Line Defenders in 2021. Security in a Box primarily aims to help a global community of human rights defenders whose work puts them at risk. It has been recognized worldwide as a foundational resource for helping people at risk protect their digital security and privacy."
#~ msgstr "\"Безопасность-в-коробке\" (Security in a Box) — проект организации [Front Line Defenders](https://www.frontlinedefenders.org). Он появился в 2007 году в сотрудничестве с [Tactical Technology Collective](https://www.tacticaltech.org) и был существенно переработан Front Line Defenders в 2021 году. \"Безопасность-в-коробке\" в первую очередь нацелена на помощь глобальному сообществу правозащитников из \"группы риска\". Этот проект признан во всем мире как основа помощи людям, подверженным риску, по вопросам цифровой безопасности и конфиденциальности."

#~ msgid "To access Security in a Box anonymously using the Tor Browser, you can visit the [*onion service*](https://en.wikipedia.org/wiki/.onion) below:"
#~ msgstr "Для анонимного доступа к \"Безопасности-в-коробке\" можно использовать Tor Browser. Наш [*onion-сайт*](https://ru.wikipedia.org/wiki/.onion):"

#~ msgid "[http://lxjacvxrozjlxd7pqced7dyefnbityrwqjosuuaqponlg3v7esifrzad.onion/en/](http://lxjacvxrozjlxd7pqced7dyefnbityrwqjosuuaqponlg3v7esifrzad.onion/en/)"
#~ msgstr "[http://lxjacvxrozjlxd7pqced7dyefnbityrwqjosuuaqponlg3v7esifrzad.onion/en/](http://lxjacvxrozjlxd7pqced7dyefnbityrwqjosuuaqponlg3v7esifrzad.onion/en/)"

#, no-wrap
#~ msgid "Can you trust the people who operate this tool?"
#~ msgstr "Можно ли доверять разработчику?"

#~ msgid "If end-to-end encryption is not possible, prioritize tools that encrypt the data between your device and the service you are using (to-server encryption)."
#~ msgstr "Если сквозного шифрования нет, особое внимание стоит уделить инструментам шифрования данных между вашим устройством и сервисом."

#~ msgid "Where are servers located? This can be a difficult question to answer, with more and more services in the cloud. Consut trusted partners in the digital human rights community to learn more. To start, you may want to refer to the work of [https://rankingdigitalrights.org/](Ranking Digital Rights), for comparisons of companies' or governments' support of your digital rights. You can also search the [Lumen Database](https://www.lumendatabase.org/) for the names of individual tools or services to understand what information your local jurisdiction may be asking services to take down, and how those services are responding."
#~ msgstr "Где расположены серверы? На этот вопрос бывает трудно ответить, потому что все чаще работа происходит в облаке. Если необходимо, обратитесь за ответом к надежным экспертам по защите цифровых прав. Для начала можно открыть сайт [https://rankingdigitalrights.org/] (Ranking Digital Rights) и посмотреть, как бизнес и правительства относятся к вашим цифровым правам. Также можно поискать в базе [Lumen Database](https://www.lumendatabase.org/) по названиям отдельных инструментов или сервисов. Это подскажет, какую информацию ваше государство может попросить удалить и как на это обычно реагируют владельцы сервисов."

#~ msgid "Do your adversaries have the legal right to seize data or shut down these services because of where the servers are located?"
#~ msgstr "Есть ли у ваших оппонентов, учитывая место расположения серверов, юридическое право получить контроль над вашими материалами или отключить сервис?"

#, no-wrap
#~ msgid "Is it legal to use this in your area?"
#~ msgstr "Законно ли использовать инструмент в вашем регионе?"

#~ msgid "In some legal jurisdictions, encryption itself is banned, and use of other tools, like VPNs or social media, may be restricted."
#~ msgstr "В некоторых юрисдикциях запрещено шифрование. Кое-где введены ограничения на другие сервисы, например, VPN и социальные сети."

#~ msgid "How do you connect with others? Do you need to provide phone number, email or nickname?"
#~ msgstr "Как вы связываетесь с другими пользователями? Нужно ли предоставлять номер телефона? Email? Никнейм?"

#, no-wrap
#~ msgid "What does the owner/operator have access to?"
#~ msgstr "К чему имеет доступ владелец/оператор сервиса?"

#, no-wrap
#~ msgid "Does the service have the features you need for your specific tasks?"
#~ msgstr "Обладает ли инструмент функциями, которые вам нужны для решения ваших конкретных задач?"

#~ msgid "Does the service allow you to host your own server, if you need that protection for your data?"
#~ msgstr "Предусмотрена ли возможность установки инструмента на собственный сервер (если вы хотите большей безопасности для своих данных)?"

#, no-wrap
#~ msgid "Is it user-friendly?"
#~ msgstr "Им удобно пользоваться?"

#~ msgid "Do you see people around you continuing to use the tool, or do they abandon it?"
#~ msgstr "Ваши знакомые используют этот инструмент? Или пробуют и бросают?"

#, no-wrap
#~ msgid "About Front Line Defenders"
#~ msgstr "О Front Line Defenders"

#~ msgid "Front Line Defenders was founded with the specific aim of protecting human rights defenders at risk, people who work, non-violently, for any or all of the rights enshrined in the Universal Declaration of Human Rights (UDHR). Front Line Defenders aims to address some of the needs identified by defenders themselves, including protection, networking, training and access to international bodies that can take action on their behalf."
#~ msgstr "Организация Front Line Defenders была создана с целью защиты правозащитников, подвергающихся риску. Мы помогаем людям, которые ненасильственным путем отстаивают права, закрепленные во Всеобщей декларации прав человека. Front Line Defenders стремится отвечать на запросы самих правозащитников. В частности, мы занимаемся обеспечением безопасности, укреплением связей между активистами, тренингами, помощью в доступе к международным органам, которые могут оказывать активистам поддержку."

#, no-wrap
#~ msgid "Funders"
#~ msgstr "Финансирование"

#~ msgid "The development of Security in a Box has been supported by [Hivos](https://hivos.org/), [Internews](http://www.internews.eu/), [Sida](http://www.sida.se/English/), [Oak Foundation](http://www.oakfnd.org/), [Sigrid Rausing Fund](https://www.sigrid-rausing-trust.org/), [AJWS](http://ajws.org/), [Open Society Foundations](http://www.opensocietyfoundations.org/), [Ford Foundation](http://www.fordfoundation.org/), and [EIDHR](http://ec.europa.eu/europeaid/index_en.htm).[**¹**](#notes)"
#~ msgstr "Проект \"Безопасность-в-коробке\" в разное время получил поддержку [Hivos](https://hivos.org/), [Internews](http://www.internews.eu/), [Sida](http://www.sida.se/English/), [Oak Foundation](http://www.oakfnd.org/), [Sigrid Rausing Fund](https://www.sigrid-rausing-trust.org/), [AJWS](http://ajws.org/), [Open Society Foundations](http://www.opensocietyfoundations.org/), [Ford Foundation](http://www.fordfoundation.org/) и [EIDHR](http://ec.europa.eu/europeaid/index_en.htm).[**¹**](#notes)"

#, no-wrap
#~ msgid "License"
#~ msgstr "Лицензия"

#~ msgid "This work is licensed under a [Creative Commons Attribution-Share Alike 3.0 Unported License](http://creativecommons.org/licenses/by-sa/3.0/). We strongly encourage the re-use of the material in Security in a Box."
#~ msgstr "Эти материалы лицензированы в соответствии с [Creative Commons Attribution-Share Alike 3.0 Unported License](https://creativecommons.org/licenses/by-sa/3.0/). Мы всячески поощряем их распространение."

#, no-wrap
#~ msgid "Notes"
#~ msgstr "Примечание"

#, no-wrap
#~ msgid "**¹** This website has been produced with the assistance of the European Union. The contents of this website are the responsibility of Front Line Defenders and can in no way be taken to reflect the views of the European Union.\n"
#~ msgstr "**¹** Этот сайт создан при поддержке Европейского союза. Содержание сайта является ответственностью Front Line Defenders и никоим образом не отражает позицию Европейского союза.\n"
