# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-12-12 21:02+0000\n"
"PO-Revision-Date: 2023-07-08 06:08+0000\n"
"Last-Translator: Amin Kamrani <kamrani.ma@googlemail.com>\n"
"Language-Team: Persian <https://weblate.securityinabox.org/projects/info/key/fa/>\n"
"Language: fa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 4.12.2\n"

#. type: Yaml Front Matter Hash Value: title
#: src/key/index.md:1
#, no-wrap
msgid "Security-in-a-Box public GPG key"
msgstr "کلید عمومی گنو پرایوسی گارد (GPG) «جعبه‌ی امنیت دیجیتال»"

#. type: Plain text
#: src/key/index.md:6
msgid "You can send us encrypted email at `siab@frontlinedefenders.org` using the GnuPG public key below. The *fingerprint* of this key is: **2EE5 55C1 9B45 2386 2908 981B FB70 8B4B 4F2F 3F53**."
msgstr "شما می‌توانید به ما ایمیل رمزگذاری شده به نشانی`siab@frontlinedefenders.org` با استفاده از کلید عمومی گنو پرایوسی گارد زیر بفرستید.*فینگرپرینت* این کلید این است: **2EE5 55C1 9B45 2386 2908 981B FB70 8B4B 4F2F 3F53**."

#. type: Fenced code block
#: src/key/index.md:7
#, no-wrap
msgid ""
"-----BEGIN PGP PUBLIC KEY BLOCK-----\n"
"\n"
"mQINBF+FUq4BEAC4SVGLVKrLYrNDI+qH0XLpQmeVLcNHe+RUteDpdseReUq0wj0f\n"
"cjdubPrEVRwRCWgbgEgU7LIOo1Nox6AFpZBtHsiLOMAPEl9q6TPSxBY3f1aEIC6W\n"
"w3yugxF7ZlFXXPr9qUMfcdltpxLenG+YSgLvftz3M74twfz7sQI20dzbar22wekv\n"
"aAl3v+6zKDpIAciqjgcVgsxbU2nPhBqfZlSjwW18E5OC9+H9oA6sAChBARMOxyjY\n"
"5dyS0r90hm4O7tYFjzSMJqfNntrrk4GmuYpRSjTXfCkdiBerPik36wyKngLuBXNJ\n"
"7bxgxVAK1K4fI4KCQW1Rsdxvctds/8X7z8RwunyyMDYidDsxOfeSb7esDR+9TNC0\n"
"H61uL7dTijmeHck/NZJf8XRyRo5RA0ICKGeW70Z2mGXWXv2WrvA2GA+/BGOcmT/w\n"
"8xj/EcNH/ZMRLJoOLlPu+/LVlvetvfDV1bbyu4zdFHExS7aKnOP/erYC6hW3NlnJ\n"
"ZsOHZMhIM3Idk8NdbLMOnVoZ9etO6bKM+rwdsP/OFi3wKTvcOPr5B4G27BI3bAff\n"
"eT1M1r8wsIwVHrMwm4JVST2hEemCGrl29c1Elr+2OdsNvSYrtqAx1A3Nu5gsKe1S\n"
"3qwRxdGrIjR52lxPMcflUsquMeynVsyhPtKtj5+whr7rXJKmLLJBHBMSoQARAQAB\n"
"tDBTZWN1cml0eSBpbiBhIGJveCAgPHNpYWJAZnJvbnRsaW5lZGVmZW5kZXJzLm9y\n"
"Zz6JAjUEEAEIAB8FAl+FUq4GCwkHCAMCBBUICgIDFgIBAhkBAhsDAh4BAAoJEPtw\n"
"i0tPLz9TJ2EP/2dvqIjJKLUBaUc1AUyynqkDeXJ8XUqbXoWS7RN9ZHzEeZlw/DHi\n"
"9+SGbdNqf5Gj2NjyRJMUJWMhG+p2wNeGZ0Ll29zZBGEpjD71iSJLcLBwICCEJOYY\n"
"mVurhIgJjBoNahtsIbXs5EA1ff1Kk5+07yC5p1OnUhjPLmaScSwYvkabTx+GdKLh\n"
"HSro8VOveH3rzXS/REBe4MwR/oOfzB1DFDa1WDzPC4AajQ0djJtiUBrTrmv9XVBz\n"
"sz/z+wf6dq31CewQT4itoRYtXXT7wUGpipAbREE0+vlDuXxdWjAHefBDuv6NLGcT\n"
"o1a84sQdD1eejAnCSiIX+/bKLvg0rkniQ+aRuYwsOlFGxlPi1Aa5zz40ZpB0yetV\n"
"/96OwgPQvE3YX9AJvpxFF7iFVZ5IkYc5jg0Lnx169dcdHxpNqV8+ZsZ64HTdH0k4\n"
"9VRtZkU8SaBpk1+9FMX2syqYSXBT4LsXYZx2BukoES7VrSGiVEccj/rldfwi7yyw\n"
"z+HaX3aSfl9gtOJ/ZjeXdIdQwbkA/r2lASsDAGVG2oAY5FXH+egw2oUSltjAL34G\n"
"9TPrjDMPoocnLe4DM7ABHjZv0wJ+ctBYhx6ft2qX113o7enAE67Djqg6Zleg3XiM\n"
"iIpXRNftl/3i/cqUEI2LXorQh9+sV1WxSV4PdgqcPa2AVlGgEksTjvmKuQINBF+F\n"
"Uq4BEADBkC2WH9NH8EsIX12XYSBFCzP53qlfc2aBOMo5VSYRFgTBl6sDHaf8vmb0\n"
"WWFf8TaVRiRkktIiIkQWrP0jsS3piqxILwbzdc6IbMyi1BzPdWZz47cUjDdiOTKI\n"
"fyiO80e843e67v89re7EWH9hiU4aFzzohgyDuTp11S42YJPqeu9pG9LC8FWdyhjF\n"
"m8FDiABu9PSPVkUNjelhzPMpmCEOEUtqeRwAEJVGUyXYp03+GdK0GA/xLSzca3vh\n"
"cpAoIYlcM9nLGXxaFjB5m1GXVXJgRDPRcaDPl7gDg8+KcdJ1ZkUs4K2RwCBGova4\n"
"U1XbaMT+W5i1hBlw8ge1AXW6ULJWlsDHAMsKZWfKEy/v17lD0mJ6YGw80Kdm8UxE\n"
"rdVBZZvfIticnFshPsj9rZy1bHfYAR7KsyhBQtm7LiaWFTET6feblFX1n+edfHbe\n"
"ZyKeJqu4Fim89yZXauB94sb2t9rETJAqHt0fnxkfjFZejWi104CvydgQpHnu8IE5\n"
"9HbbArAHaZ0fkzio5l/2w1bfJ0njuywXlFS0D2kOTWa5gCE6x4QsvNqijC14/8FG\n"
"5/bg/KkRgAYYQhUKY22gm5ySVTOh4KNEUFRucVZjfFo1SWLr43jsb+vwdRrVaxVH\n"
"9SSYkSEr1pIIYgKDdfpo71G8mv/V256HL+aaoegdI5EX1CSthQARAQABiQIfBBgB\n"
"CAAJBQJfhVKuAhsMAAoJEPtwi0tPLz9TUbgP/1PtNheREiEvOVdml25Pw3ZWJdKh\n"
"xL5Gr2wl5HMF5K0to1Ti+SGmIWsgyZ1/2iXF63RLHlGL9pLLOZe92tmbos5u9r18\n"
"1NkOfhteqyQlW4TDZICvA9hHNGBmAKN7VFjfaI2IaFi5fn+u8gyOfKsRiPbUqfHc\n"
"q4sg9LRinKz6LYMCI3SJs0njj/a5snIPDFQIBM+s2l8W68xnddTEOhdS/ZuzXRA5\n"
"YIduUNjzBriwrN4GNIQvlSdH9PtDdCEpdwi+vc91fdl/kN/JPOGMRqi81KL48F8N\n"
"5p1YmrzS32uTFTUiitcL3iRaUYpbNERFQhMOfbYhzZQGm8fDM4xvn/hsK+JxtKXS\n"
"eDJhOHtDvvs7taG674hW3j/pCLzn/5pcq0L0Ln5zxiOTRi+R4LQnqlEbn/WADhvG\n"
"OkIfgRwXnW2amETtA3zqaBQ+j/1HmxL6OUoM/YW7/4wGHu6dPl5qTc8p2l31pUFA\n"
"5cwA88przCEDklxZvRfFdavCftzn1MH5ZaC90HC7LjTeZN9XGfBkVWbh+D+ljfIp\n"
"xFFHspBx0T0QJbZery4fqRSAX8BQWORTjLpEvMIA7dYfa1YpTfChws0ZcWB34QMv\n"
"jgjnAieeFq9KalwlLN8y3Jk6kpYoUB5wyCbcn9rhg3+DqIG7iuxWcFizmk1IcYal\n"
"BHlRdGUL8uSQOkdi\n"
"=9skJ\n"
"-----END PGP PUBLIC KEY BLOCK-----\n"
msgstr ""
"-----شروع بلاک کلید عمومی پی‌جی‌پی-----\n"
"\n"
"mQINBF+FUq4BEAC4SVGLVKrLYrNDI+qH0XLpQmeVLcNHe+RUteDpdseReUq0wj0f\n"
"cjdubPrEVRwRCWgbgEgU7LIOo1Nox6AFpZBtHsiLOMAPEl9q6TPSxBY3f1aEIC6W\n"
"w3yugxF7ZlFXXPr9qUMfcdltpxLenG+YSgLvftz3M74twfz7sQI20dzbar22wekv\n"
"aAl3v+6zKDpIAciqjgcVgsxbU2nPhBqfZlSjwW18E5OC9+H9oA6sAChBARMOxyjY\n"
"5dyS0r90hm4O7tYFjzSMJqfNntrrk4GmuYpRSjTXfCkdiBerPik36wyKngLuBXNJ\n"
"7bxgxVAK1K4fI4KCQW1Rsdxvctds/8X7z8RwunyyMDYidDsxOfeSb7esDR+9TNC0\n"
"H61uL7dTijmeHck/NZJf8XRyRo5RA0ICKGeW70Z2mGXWXv2WrvA2GA+/BGOcmT/w\n"
"8xj/EcNH/ZMRLJoOLlPu+/LVlvetvfDV1bbyu4zdFHExS7aKnOP/erYC6hW3NlnJ\n"
"ZsOHZMhIM3Idk8NdbLMOnVoZ9etO6bKM+rwdsP/OFi3wKTvcOPr5B4G27BI3bAff\n"
"eT1M1r8wsIwVHrMwm4JVST2hEemCGrl29c1Elr+2OdsNvSYrtqAx1A3Nu5gsKe1S\n"
"3qwRxdGrIjR52lxPMcflUsquMeynVsyhPtKtj5+whr7rXJKmLLJBHBMSoQARAQAB\n"
"tDBTZWN1cml0eSBpbiBhIGJveCAgPHNpYWJAZnJvbnRsaW5lZGVmZW5kZXJzLm9y\n"
"Zz6JAjUEEAEIAB8FAl+FUq4GCwkHCAMCBBUICgIDFgIBAhkBAhsDAh4BAAoJEPtw\n"
"i0tPLz9TJ2EP/2dvqIjJKLUBaUc1AUyynqkDeXJ8XUqbXoWS7RN9ZHzEeZlw/DHi\n"
"9+SGbdNqf5Gj2NjyRJMUJWMhG+p2wNeGZ0Ll29zZBGEpjD71iSJLcLBwICCEJOYY\n"
"mVurhIgJjBoNahtsIbXs5EA1ff1Kk5+07yC5p1OnUhjPLmaScSwYvkabTx+GdKLh\n"
"HSro8VOveH3rzXS/REBe4MwR/oOfzB1DFDa1WDzPC4AajQ0djJtiUBrTrmv9XVBz\n"
"sz/z+wf6dq31CewQT4itoRYtXXT7wUGpipAbREE0+vlDuXxdWjAHefBDuv6NLGcT\n"
"o1a84sQdD1eejAnCSiIX+/bKLvg0rkniQ+aRuYwsOlFGxlPi1Aa5zz40ZpB0yetV\n"
"/96OwgPQvE3YX9AJvpxFF7iFVZ5IkYc5jg0Lnx169dcdHxpNqV8+ZsZ64HTdH0k4\n"
"9VRtZkU8SaBpk1+9FMX2syqYSXBT4LsXYZx2BukoES7VrSGiVEccj/rldfwi7yyw\n"
"z+HaX3aSfl9gtOJ/ZjeXdIdQwbkA/r2lASsDAGVG2oAY5FXH+egw2oUSltjAL34G\n"
"9TPrjDMPoocnLe4DM7ABHjZv0wJ+ctBYhx6ft2qX113o7enAE67Djqg6Zleg3XiM\n"
"iIpXRNftl/3i/cqUEI2LXorQh9+sV1WxSV4PdgqcPa2AVlGgEksTjvmKuQINBF+F\n"
"Uq4BEADBkC2WH9NH8EsIX12XYSBFCzP53qlfc2aBOMo5VSYRFgTBl6sDHaf8vmb0\n"
"WWFf8TaVRiRkktIiIkQWrP0jsS3piqxILwbzdc6IbMyi1BzPdWZz47cUjDdiOTKI\n"
"fyiO80e843e67v89re7EWH9hiU4aFzzohgyDuTp11S42YJPqeu9pG9LC8FWdyhjF\n"
"m8FDiABu9PSPVkUNjelhzPMpmCEOEUtqeRwAEJVGUyXYp03+GdK0GA/xLSzca3vh\n"
"cpAoIYlcM9nLGXxaFjB5m1GXVXJgRDPRcaDPl7gDg8+KcdJ1ZkUs4K2RwCBGova4\n"
"U1XbaMT+W5i1hBlw8ge1AXW6ULJWlsDHAMsKZWfKEy/v17lD0mJ6YGw80Kdm8UxE\n"
"rdVBZZvfIticnFshPsj9rZy1bHfYAR7KsyhBQtm7LiaWFTET6feblFX1n+edfHbe\n"
"ZyKeJqu4Fim89yZXauB94sb2t9rETJAqHt0fnxkfjFZejWi104CvydgQpHnu8IE5\n"
"9HbbArAHaZ0fkzio5l/2w1bfJ0njuywXlFS0D2kOTWa5gCE6x4QsvNqijC14/8FG\n"
"5/bg/KkRgAYYQhUKY22gm5ySVTOh4KNEUFRucVZjfFo1SWLr43jsb+vwdRrVaxVH\n"
"9SSYkSEr1pIIYgKDdfpo71G8mv/V256HL+aaoegdI5EX1CSthQARAQABiQIfBBgB\n"
"CAAJBQJfhVKuAhsMAAoJEPtwi0tPLz9TUbgP/1PtNheREiEvOVdml25Pw3ZWJdKh\n"
"xL5Gr2wl5HMF5K0to1Ti+SGmIWsgyZ1/2iXF63RLHlGL9pLLOZe92tmbos5u9r18\n"
"1NkOfhteqyQlW4TDZICvA9hHNGBmAKN7VFjfaI2IaFi5fn+u8gyOfKsRiPbUqfHc\n"
"q4sg9LRinKz6LYMCI3SJs0njj/a5snIPDFQIBM+s2l8W68xnddTEOhdS/ZuzXRA5\n"
"YIduUNjzBriwrN4GNIQvlSdH9PtDdCEpdwi+vc91fdl/kN/JPOGMRqi81KL48F8N\n"
"5p1YmrzS32uTFTUiitcL3iRaUYpbNERFQhMOfbYhzZQGm8fDM4xvn/hsK+JxtKXS\n"
"eDJhOHtDvvs7taG674hW3j/pCLzn/5pcq0L0Ln5zxiOTRi+R4LQnqlEbn/WADhvG\n"
"OkIfgRwXnW2amETtA3zqaBQ+j/1HmxL6OUoM/YW7/4wGHu6dPl5qTc8p2l31pUFA\n"
"5cwA88przCEDklxZvRfFdavCftzn1MH5ZaC90HC7LjTeZN9XGfBkVWbh+D+ljfIp\n"
"xFFHspBx0T0QJbZery4fqRSAX8BQWORTjLpEvMIA7dYfa1YpTfChws0ZcWB34QMv\n"
"jgjnAieeFq9KalwlLN8y3Jk6kpYoUB5wyCbcn9rhg3+DqIG7iuxWcFizmk1IcYal\n"
"BHlRdGUL8uSQOkdi\n"
"=9skJ\n"
"-----پایان بلاک کلید عمومی پی‌جی‌پی-----\n"
