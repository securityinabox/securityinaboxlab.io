# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-12-12 21:02+0000\n"
"PO-Revision-Date: 2024-06-09 00:10+0000\n"
"Last-Translator: Sergey Smirnov <cj75300@gmail.com>\n"
"Language-Team: Russian <https://weblate.securityinabox.org/projects/info/404/ru/>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Weblate 4.12.2\n"

#. type: Yaml Front Matter Hash Value: title
#: src/404/index.md:1
#, no-wrap
msgid "Error 404 - Page not found"
msgstr "Ошибка 404 — страница не найдена"

#. type: Plain text
#: src/404/index.md:6
msgid "Sorry, the page you are looking for does not exist."
msgstr "Извините, но страницы, которую вы искали, нет на сайте."
