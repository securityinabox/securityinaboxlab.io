# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-12-12 21:02+0000\n"
"PO-Revision-Date: 2023-08-30 22:08+0000\n"
"Last-Translator: Amelia Hg <amelia.mth@protonmail.com>\n"
"Language-Team: Vietnamese <https://weblate.securityinabox.org/projects/info/disclaimer/vi/>\n"
"Language: vi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 4.12.2\n"

#. type: Yaml Front Matter Hash Value: title
#: src/disclaimer/index.md:1
#, no-wrap
msgid "Disclaimer"
msgstr "Miễn trừ trách nhiệm"

#. type: Plain text
#: src/disclaimer/index.md:5
#, no-wrap
msgid "*Software and documentation in this collection of Security in a Box is provided as is and we exclude and expressly disclaim all express and implied warranties or conditions of any kind, including, but not limited to, the implied warranties of merchantability and fitness for a particular purpose so far as such disclaimer is permitted. In no event shall Front Line Defenders, Tactical Technology Collective or any agent or representatives thereof be liable for any direct, indirect, incidental, special, exemplary, or consequential damages (including, but not limited to, procurement of substitute goods or services; loss of use, data, or profits; or business interruption), however caused under any theory of liability, arising in any way out of the use of or inability to make use of this software, even if advised of the possibility of such damage. Nothing in this disclaimer affects your statutory rights.*\n"
msgstr "*Các phần mềm và tài liệu của bộ tổng hợp trong Security in a Box được cung cấp đúng như nguyên trạng thực tế và chúng tôi không chịu mọi trách nhiệm trực tiếp hay gián tiếp về bảo hành cũng như tình trạng sản phầm trong bất kỳ trường hợp nào, bao gồm, nhưng không giới hạn về, các nghĩa vụ bảo hành sản phẩm hay sự phù hợp cho một mục đích sử dụng riêng, chừng nào việc từ chối này được pháp luật cho phép. Trong bất kỳ trường hợp nào, Front Line Defenders, Tactical Technology Collective hay bất kỳ nhân tố hay đại điện nào đều không chịu trách nhiệm pháp lý cho bất kỳ thiệt hại trực tiếp, gián tiếp, ngẫu nhiên, đặc biệt, có dẫn chứng hoặc có nguyên nhân (bao gồm, nhưng không giới hạn, việc mua bán các hàng hóa hoặc dịch vụ giả mạo, việc mất quyền sử dụng, dữ liệu hoặc lợi nhuận; hoặc gián đoạn công việc), cho dù được gây ra bởi bất kỳ nguyên nhân về mặt lý thuyết có tính ràng buộc pháp lý, hay bởi việc ngừng hoặc không thể sử dụng phần mềm này, ngay cả khi có khuyến nghị về nguy cơ xảy ra các thiệt hại này. Sự miễn trừ trách nhiệm này không ảnh hưởng tới các quyền hợp pháp của bạn.*\n"
