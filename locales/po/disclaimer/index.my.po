# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-12-12 21:02+0000\n"
"PO-Revision-Date: 2023-11-04 04:09+0000\n"
"Last-Translator: gtheflash <gtheflash@protonmail.com>\n"
"Language-Team: Burmese <https://weblate.securityinabox.org/projects/info/disclaimer/my/>\n"
"Language: my\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 4.12.2\n"

#. type: Yaml Front Matter Hash Value: title
#: src/disclaimer/index.md:1
#, no-wrap
msgid "Disclaimer"
msgstr "ရှင်းလင်းချက်"

#. type: Plain text
#: src/disclaimer/index.md:5
#, no-wrap
msgid "*Software and documentation in this collection of Security in a Box is provided as is and we exclude and expressly disclaim all express and implied warranties or conditions of any kind, including, but not limited to, the implied warranties of merchantability and fitness for a particular purpose so far as such disclaimer is permitted. In no event shall Front Line Defenders, Tactical Technology Collective or any agent or representatives thereof be liable for any direct, indirect, incidental, special, exemplary, or consequential damages (including, but not limited to, procurement of substitute goods or services; loss of use, data, or profits; or business interruption), however caused under any theory of liability, arising in any way out of the use of or inability to make use of this software, even if advised of the possibility of such damage. Nothing in this disclaimer affects your statutory rights.*\n"
msgstr "* Security in a box တွင်စုစည်းထားသော ဆော့ဖ်ဝဲလ်နှင့် စာရွက်စာတမ်းများကို မူရင်းအတိုင်း ပံ့ပိုးပေးထားပြီး သီးခြားသတ်မှတ်ထားသော ကြံ့ခိုင်မှုဆိုင်ရာနှင့် ရောင်းဝယ်နိုင်မှု အာမခံချက်များအပါအဝင် မည်သည့်ဖော်ပြချက်နှင့်မဆို အဓိပ္ပာယ်သက်ရောက်သော အာမခံများ သို့မဟုတ် အခြေအနေများအားလုံးကို ဖယ်ထုတ်ပြီး အတိအလင်း ငြင်းဆိုထားသည်။ Front Line Defenders၊ Tactical Technology Collective သို့မဟုတ် ယင်း၏အေးဂျင့် သို့မဟုတ် တိုက်ရိုက်သော်လည်းကောင်း၊ သွယ်ဝိုက်၍လည်းကောင်း၊ မတော်တဆသော်လည်းကောင်း၊ အထူး၊ စံနမူနာပြု သို့မဟုတ် နောက်ဆက်တွဲ ပျက်စီးဆုံးရှုံးမှုများ(သို့သော် အကန့်အသတ်မရှိ၊ အစားထိုး ကုန်စည် သို့မဟုတ် ဝန်ဆောင်မှုများ ဝယ်ယူမှု၊ ဆုံးရှုံးမှုအပါအဝင်၊ ဆုံးရှုံးမှု၊ အသုံးပြုမှု၊ ဒေတာ သို့မဟုတ် အမြတ်များ သို့မဟုတ် လုပ်ငန်းဆိုင်ရာ အနှောင့်အယှက်ဖြစ်စေခြင်း) အတွက် ကိုယ်စားလှယ်တစ်ဦးဦးသည် တာဝန်ရှိသော်လည်းတာဝန်ယူမှု၏သီအိုရီအရဖြစ်စေ ဤဆော့ဖ်ဝဲကို အသုံးပြုခြင်း သို့မဟုတ် အသုံးမပြုနိုင်ခြင်းတို့ကြောင့် ဖြစ်ပေါ်လာသော ပျက်စီးဆုံးရှုံးမှုဖြစ်နိုင်ခြေကို အကြံပြုထားသည်။ ဤရှင်းလင်းချက်တွင် မည်သည့်အရာကမှ သင်၏ ရပိုင်ခွင့်များကို မထိခိုက်စေပါ။*\n"
