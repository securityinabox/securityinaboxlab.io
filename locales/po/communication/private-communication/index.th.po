# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-02-20 14:04+0000\n"
"PO-Revision-Date: 2023-07-15 17:08+0000\n"
"Last-Translator: Arthit Suriyawongkul <arthit@gmail.com>\n"
"Language-Team: Thai <https://weblate.securityinabox.org/projects/communication/private-communication/th/>\n"
"Language: th\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 4.12.2\n"

#. type: Yaml Front Matter Hash Value: title
#: src/communication/private-communication/index.md:1
#, no-wrap
msgid "Protect the privacy of your online communication"
msgstr "ปกป้องความเป็นส่วนตัวของการสื่อสารออนไลน์ของคุณ"

#. type: Plain text
#: src/communication/private-communication/index.md:9
msgid "Each communication method, digital or otherwise, comes with advantages and disadvantages in terms of convenience, popularity, cost, and security, among other considerations. It's up to each of us to weigh the benefits and risks of the methods we use to communicate. When our risks are higher, it is wise to choose our communication tools more deliberately."
msgstr "การสื่อสารแต่ละวิธี ไม่ว่าจะทางดิจิทัลหรือทางอื่น ล้วนมีข้อเด่นข้อด้อยให้พิจารณา เช่น ในแง่ความสะดวก ความนิยม ค่าใช้จ่าย และความมั่นคงปลอดภัย มันขึ้นกับพวกเราแต่ละคนจะชั่งน้ำหนักประโยชน์และความเสี่ยงของวิธีต่าง ๆ ที่เราใช้สื่อสาร ในสถานการณ์ที่ความเสี่ยงของเรานั้นสูง มันก็เป็นการดีกว่าที่จะเลือกเครื่องมือสื่อสารของเราอย่างตั้งใจ"

#. type: Title #
#: src/communication/private-communication/index.md:11
#, no-wrap
msgid "Make a communication plan"
msgstr "วางแผนการสื่อสาร"

#. type: Plain text
#: src/communication/private-communication/index.md:14
msgid "Establish and rehearse a communication plan for yourself and your community, so you are able to maintain communication and take care of each other in moments of stress or crisis."
msgstr "จัดให้มีแผนการสื่อสารสำหรับตัวคุณเองและชุมชนของคุณ และซักซ้อมตามแผนดังกล่าว เพื่อคุณจะได้สามารถคงการสื่อสารเอาไว้และยังสามารถดูแลกันและกันได้ในช่วงเวลาตึงเครียดหรือยามวิกฤต"

#. type: Bullet: ' - '
#: src/communication/private-communication/index.md:30
msgid "Talk with your contacts. Propose, discuss and agree upon a specific plan for how you are (and are not) going to communicate. Include:"
msgstr "พูดคุยกับผู้ติดต่อของคุณ เสนอ ถกประเด็น และตกลงแผนที่เจาะจงว่าคุณจะทำการสื่อสาร (และไม่สื่อสาร) อย่างไร รวมถึง:"

#. type: Bullet: '     - '
#: src/communication/private-communication/index.md:30
msgid "known risks"
msgstr "ความเสี่ยงที่รู้อยู่แล้วว่ามี"

#. type: Bullet: '     - '
#: src/communication/private-communication/index.md:30
msgid "apps and services you will and will not use"
msgstr "แอปและบริการที่คุณจะใช้และจะไม่ใช้"

#. type: Bullet: '     - '
#: src/communication/private-communication/index.md:30
msgid "steps you will take if something goes wrong"
msgstr "ขั้นตอนที่คุณจะปฏิบัติ หากเกิดอะไรผิดพลาด"

#. type: Bullet: ' - '
#: src/communication/private-communication/index.md:30
msgid "Consider informal steps you can take, including:"
msgstr "พิจารณาขั้นตอนลำลองที่คุณทำได้ รวมถึง:"

#. type: Bullet: '     - '
#: src/communication/private-communication/index.md:30
msgid "creating and exchanging alternative backup email addresses"
msgstr "สร้างและแลกเปลี่ยนที่อยู่อีเมลสำรอง"

#. type: Bullet: '     - '
#: src/communication/private-communication/index.md:30
msgid "exchanging phone numbers"
msgstr "แลกเปลี่ยนหมายเลขโทรศัพท์"

#. type: Bullet: '     - '
#: src/communication/private-communication/index.md:30
msgid "sharing emergency contacts"
msgstr "แบ่งปันที่อยู่ติดต่อเมื่อเกิดเหตุฉุกเฉิน"

#. type: Bullet: ' - '
#: src/communication/private-communication/index.md:30
msgid "Consider more formal steps you can take:"
msgstr "พิจารณาขั้นตอนที่เป็นทางการมากขึ้นที่คุณทำได้:"

#. type: Bullet: '     - '
#: src/communication/private-communication/index.md:30
msgid "Specify supported communication methods in your organisational security-, data-storage- or document-retention policy."
msgstr "ระบุวิธีการสื่อสารที่สนับสนุน ลงในนโยบายความมั่นคงปลอดภัย นโยบายการจัดเก็บข้อมูล หรือนโยบายการเก็บรักษาเอกสาร ขององค์กรของคุณ"

#. type: Bullet: ' - '
#: src/communication/private-communication/index.md:30
msgid "Consider what you say and where"
msgstr "พิจารณาสิ่งที่คุณพูดและสถานที่ที่พูด"

#. type: Bullet: '     - '
#: src/communication/private-communication/index.md:30
msgid "The easiest way to keep others from learning sensitive information is to not send or say it."
msgstr "วิธีที่ง่ายที่สุดในการไม่ให้คนอื่นทราบถึงข้อมูลอ่อนไหวคือการไม่ส่งหรือไม่พูดมันออกมา"

#. type: Bullet: '     - '
#: src/communication/private-communication/index.md:30
msgid "For example, plan not to send text messages when someone is travelling through border crossings, or in detention."
msgstr "ตัวอย่างเช่น วางแผนไม่ส่งข้อความระหว่างที่มีคนกำลังเดินทางข้ามพรมแดนหรือกำลังถูกควบคุมตัว"

#. type: Bullet: '     - '
#: src/communication/private-communication/index.md:30
msgid "Consider developing code language you can use to avoid openly mentioning names, addresses, specific actions, etc."
msgstr "ลองพิจารณาสร้างรหัสสื่อสารที่คุณสามารถใช้เพื่อเลี่ยงการเอ่ยถึงชื่อ ที่อยู่ การกระทำที่เจาะจง ฯลฯ"

#. type: Bullet: ' - '
#: src/communication/private-communication/index.md:30
msgid "Rehearse your communication plan"
msgstr "ซักซ้อมแผนการสื่อสารของคุณ"

#. type: Plain text
#: src/communication/private-communication/index.md:32
#: src/communication/private-communication/index.md:43
#: src/communication/private-communication/index.md:56
#: src/communication/private-communication/index.md:74
#: src/communication/private-communication/index.md:95
#: src/communication/private-communication/index.md:119
#: src/communication/private-communication/index.md:143
#: src/communication/private-communication/index.md:152
#: src/communication/private-communication/index.md:167
#: src/communication/private-communication/index.md:177
#: src/communication/private-communication/index.md:191
#: src/communication/private-communication/index.md:201
#, no-wrap
msgid "<details><summary>Learn why we recommend this</summary>\n"
msgstr "<details><summary>เรียนรู้ว่าทำไมเราจึงแนะนำเช่นนี้</summary>\n"

#. type: Plain text
#: src/communication/private-communication/index.md:35
#, no-wrap
msgid ""
"In a crisis, we do not think as clearly, and we may have to act fast. We may make decisions that endanger us. Planning an emergency communications strategy in advance can help us stay safe.\n"
"</details>\n"
msgstr ""
"ในภาวะวิกฤต เราไม่สามารถคิดโดยกระจ่างได้ และอาจต้องทำสิ่งต่าง ๆ อย่างรวดเร็ว เราอาจตัดสินใจไปในทางที่ทำให้ตัวเองตกอยู่ในอันตราย การวางแผนการสื่อสารไว้ล่วงหน้าโดยละเอียด จะช่วยให้เราปลอดภัยได้\n"
"</details>\n"

#. type: Title #
#: src/communication/private-communication/index.md:36
#, no-wrap
msgid "Create multiple disposable accounts"
msgstr "สร้างบัญชีใช้แล้วทิ้งหลายๆ อัน"

#. type: Bullet: '- '
#: src/communication/private-communication/index.md:41
#, fuzzy
#| msgid "Many popular email providers allow you to create email aliases for receiving emails to your main email address without revealing your main email address."
msgid "Many popular email providers allow you to create email aliases for receiving emails to your main mailbox without revealing your main email address."
msgstr "ผู้ให้บริการอีเมลที่เป็นที่นิยมจำนวนมาก อนุญาตให้คุณสร้างชื่อเรียกอีเมลเพื่อรับอีเมลทางที่อยู่อีเมลหลักของคุณได้ โดยไม่จำเป็นต้องเปิดเผยที่อยู่อีเมลหลักของคุณ"

#. type: Bullet: '- '
#: src/communication/private-communication/index.md:41
msgid "Use a [Firefox Relay email address](https://relay.firefox.com/)."
msgstr "ใช้ [ที่อยู่อีเมลของ Firefox Relay](https://relay.firefox.com/)"

#. type: Bullet: '- '
#: src/communication/private-communication/index.md:41
#, fuzzy
#| msgid "If you know it does not matter if you lose access to an account, but you need a viable email address to sign up for an online service, try [Guerrilla Mail](https://www.guerrillamail.com/about)."
msgid "If you know it does not matter if you lose access to a mailbox, but you need a viable email address to sign up for an online service, try [Guerrilla Mail](https://www.guerrillamail.com/about)."
msgstr "ถ้าคุณรู้อยู่แล้วว่าบัญชีนี้ไม่ได้สำคัญ คุณสามารถเสียมันไปได้ แต่คุณต้องการที่อยู่อีเมลที่ใช้การได้จริงเพื่อจะสมัครบริการออนไลน์ได้ ให้ลองใช้ [Guerrilla Mail](https://www.guerrillamail.com/about)"

#. type: Plain text
#: src/communication/private-communication/index.md:45
msgid "Encryption tools which scramble your messages so attackers can't read them prevent the content of your messages from being exposed. But it is harder to hide who is sending or receiving a message, the dates and times when messages are sent and received, how much information was sent, and information about the devices used. This kind of \"information about information\" is often called \"metadata.\" It isn't hidden because computers, routers, servers, and other devices between you and the recipient need it to get your message to the right person. Metadata can reveal a lot about who you know and about your communication patterns, especially if someone can analyse a large volume of it."
msgstr ""

#. type: Plain text
#: src/communication/private-communication/index.md:48
#, no-wrap
msgid ""
"Using a number of accounts you can abandon, without significant disruption to your communication, is one strategy for disguising who you are.\n"
"</details>\n"
msgstr ""
"การมีบัญชีที่คุณสามารถโยนทิ้งได้เอาไว้ใช้จำนวนหนึ่ง โดยเมื่อโยนทิ้งไปแล้วจะไม่ขัดขวางการสื่อสารของคุณมากนัก ก็เป็นหนึ่งในวิธีที่ช่วยปกปิดว่าตัวคุณเป็นใคร\n"
"</details>\n"

#. type: Title #
#: src/communication/private-communication/index.md:49
#, no-wrap
msgid "Use Tor Browser, Tails, or a trusted VPN"
msgstr "ใช้ Tor Browser, Tails หรือ VPN ที่เชื่อใจได้"

#. type: Bullet: '- '
#: src/communication/private-communication/index.md:54
msgid "See our guide on [visiting blocked websites](../../internet-connection/anonymity-and-circumvention) for more on Tor, VPNs, and other tools that protect your online activity from people who want to see what you are doing."
msgstr "ดูคู่มือแนะนำของเราเกี่ยวกับ[การเข้าชมเว็บไซต์ที่ถูกปิดกั้น](../../internet-connection/anonymity-and-circumvention) สำหรับข้อมูลเพิ่มเติมเรื่อง Tor, VPN, และเครื่องมืออื่น ๆ จะปกป้องกิจกรรมออนไลน์ของคุณจากบุคคลที่ต้องการดูว่าคุณกำลังทำอะไรอยู่"

#. type: Bullet: '- '
#: src/communication/private-communication/index.md:54
msgid "Learn more about [Tails](https://tails.boum.org/about/index.en.html)."
msgstr "เรียนรู้เพิ่มเติมเกี่ยวกับ [Tails](https://tails.boum.org/about/index.en.html)"

#. type: Bullet: '- '
#: src/communication/private-communication/index.md:54
msgid "Check is it safe and legal for you to use those tools in your context."
msgstr "ตรวจสอบว่ามันปลอดภัยและถูกกฎหมายที่คุณจะใช้เครื่องมือเหล่านั้นในบริบทของคุณ"

#. type: Plain text
#: src/communication/private-communication/index.md:58
#, fuzzy
#| msgid "Using the Tor Browser, a VPN, or Tails allows you to visit websites without disclosing to the website who you are or where you are from. If you log into a website or service while doing so, you will still be sharing your account information (and potentially personal information) with the website."
msgid "Using the Tor Browser, a VPN, or Tails allows you to visit websites without disclosing to the website who you are or where you are from. If you log in to a website or service while doing so, you will still be sharing your account information (and potentially personal information) with the website."
msgstr "การใช้ Tor Browser, VPN, หรือ Tails ช่วยให้คุณเข้าชมเว็บไซต์ได้ โดยไม่เปิดเผยข้อมูลกับเว็บไซต์ว่าคุณเป็นใคร หรือคุณมาจากไหน ถ้าคุณลงชื่อเข้าเว็บไซต์หรือบริการในขณะที่ใช้โปรแกรมเหล่านั้น คุณจะยังแบ่งปันข้อมูลบัญชีของคุณ (และมีโอกาสจะมีข้อมูลส่วนบุคคลของคุณ) ให้กับเว็บไซต์ดังกล่าว"

#. type: Plain text
#: src/communication/private-communication/index.md:61
#, no-wrap
msgid ""
"Tails is an operating system that you use instead of your computer's usual operating system (Windows, Mac, or Linux). It protects all of your internet connections by using Tor at all times. It runs off a USB drive plugged into your machine. Tails erases your history when you shut it down, making it less likely that your computer can be \"fingerprinted\" by sites you have visited, wifi you have used, and apps you have installed.\n"
"</details>\n"
msgstr ""
"Tails เป็นระบบปฏิบัติการที่คุณใช้แทนที่ระบบปฏิบัติการปกติ (วินโดวส์ แมค หรือลีนุกซ์) ที่คุณใช้กับคอมพิวเตอร์ของคุณ มันปกป้องการเชื่อมต่ออินเทอร์เน็ตของคุณทั้งหมดด้วยการใช้ Tor ตลอดเวลา มันทำงานจากไดรฟ์ยูเอสบีที่เสียบกับเครื่องคอมพิวเตอร์ของคุณ Tails จะลบประวัติของคุณเมื่อคุณปิดเครื่อง ทำให้มีโอกาสน้อยลงที่คอมพิวเตอร์ของคุณจะถูก \"ประทับลายนิ้วมือ\" โดยเว็บไซต์ที่คุณเข้าชม เครือข่าย wifi ที่คุณใช้ หรือแอปที่คุณได้ติดตั้ง\n"
"</details>\n"

#. type: Title #
#: src/communication/private-communication/index.md:62
#, no-wrap
msgid "Use email more safely"
msgstr "ใช้อีเมลให้ปลอดภัยขึ้น"

#. type: Title ##
#: src/communication/private-communication/index.md:64
#, no-wrap
msgid "Encrypt your email messages"
msgstr "เข้ารหัสลับข้อความอีเมลของคุณ"

#. type: Bullet: ' - '
#: src/communication/private-communication/index.md:72
msgid "Encrypt email services you already use with"
msgstr "เข้ารหัสลับบริการอีเมลที่คุณกำลังใช้อยู่แล้ว"

#. type: Bullet: '     - '
#: src/communication/private-communication/index.md:72
msgid "[Mailvelope](https://www.mailvelope.com/)"
msgstr "[Mailvelope](https://www.mailvelope.com/)"

#. type: Bullet: '     - '
#: src/communication/private-communication/index.md:72
msgid "[Thunderbird](https://www.thunderbird.net/)"
msgstr "[Thunderbird](https://www.thunderbird.net/)"

#. type: Bullet: ' - '
#: src/communication/private-communication/index.md:72
msgid "Try email services that support encryption"
msgstr "ลองบริการอีเมลที่รองรับการเข้ารหัสลับ"

#. type: Bullet: '     - '
#: src/communication/private-communication/index.md:72
msgid "[Proton Mail](https://proton.me/)"
msgstr "[Proton Mail](https://proton.me/)"

#. type: Bullet: '         - '
#: src/communication/private-communication/index.md:72
msgid "NOTE: Proton Mail does not automatically encrypt mail to someone who does not use the same service. To encrypt to someone who does not use Proton Mail, you must [set a password the recipient can use to open the message](https://proton.me/support/password-protected-emails) or you may need to [exchange encryption public keys](https://proton.me/support/how-to-use-pgp)."
msgstr ""

#. type: Plain text
#: src/communication/private-communication/index.md:76
msgid "Email encryption uses complex mathematics to scramble the content of your files or messages so they can only be read by someone who has the \"key\" to unscramble it. Technical experts trust encryption because at this point in history, no computer is powerful enough to do the math to unscramble it."
msgstr ""

#. type: Plain text
#: src/communication/private-communication/index.md:78
msgid "Using encryption with email is important. Your email passes through a number of other devices on its way to the person you are writing to. The wifi hub or hotspot you are connected to is among them. Messages also pass through wires and wireless connections (like wifi and Bluetooth)."
msgstr ""

#. type: Plain text
#: src/communication/private-communication/index.md:80
msgid "Your email is also saved on devices called servers, and may be saved on more than one of your recipient's devices, like their mobile and their computer. An attacker could theoretically find your messages in any one of these places. Encryption stops attackers from being able to read what they find, making your messages look to attackers as if they are paragraphs of nonsense letters and numbers."
msgstr ""

#. type: Plain text
#: src/communication/private-communication/index.md:82
msgid "Email was not built with encryption included. It was developed in the 1970s, when it was used by a more limited number of people. Today, most email providers (like Gmail, Outlook, Yandex, etc.) protect your messages with encryption, using for example HTTPS, as they go from your device, through your local wifi router, and to your email providers' servers (this is sometimes called to-server encryption). However, your messages remain unencrypted when they are stored on your device and on your email provider's servers as well as on the recipient's device and email provider's servers. This means someone with access to the servers or devices can read your messages."
msgstr ""

#. type: Plain text
#: src/communication/private-communication/index.md:84
msgid "Because of this, we recommend email clients or services that are end-to-end encrypted (e2ee). For example those which use or enable PGP encryption (like Thunderbird and Mailvelope or Proton Mail). This means that your messages are protected with encryption when they are saved on your email provider's servers, as well as on your and recipients devices."
msgstr ""

#. type: Plain text
#: src/communication/private-communication/index.md:87
#, no-wrap
msgid ""
"However, email encryption has a few drawbacks. Most email encryption does not hide who is sending mail to whom, or when. An attacker could use that information to demonstrate that people are talking to each other. Additionally, managing encryption requires extra effort and mindfulness.\n"
"</details>\n"
msgstr ""

#. type: Title ##
#: src/communication/private-communication/index.md:88
#, no-wrap
msgid "Avoid retaining sensitive information you no longer need"
msgstr ""

#. type: Bullet: ' - '
#: src/communication/private-communication/index.md:92
msgid "Set up disappearing messages on messaging applications where possible."
msgstr ""

#. type: Bullet: ' - '
#: src/communication/private-communication/index.md:92
msgid "Delete messages when possible."
msgstr "ลบข้อความเมื่อเป็นไปได้"

#. type: Plain text
#: src/communication/private-communication/index.md:98
#, no-wrap
msgid ""
"Consider which messages someone might find if they were able to get access to your account on your email server or device. What would those messages reveal about your work, your views, or your contacts? Weigh that risk against the benefit of having access to your email on multiple devices. Backing up your email to an encrypted device is one way to retain and save your emails safely.\n"
"</details>\n"
msgstr ""

#. type: Title #
#: src/communication/private-communication/index.md:99
#, no-wrap
msgid "Video and audio chat more safely"
msgstr "คุยแชตวิดีโอและเสียงอย่างปลอดภัยขึ้น"

#. type: Title ##
#: src/communication/private-communication/index.md:101
#, no-wrap
msgid "Use end-to-end encrypted video, voice and text services"
msgstr "ใช้บริการวิดีโอ เสียง และข้อความเข้ารหัสลับต้นทางถึงปลายทาง"

#. type: Plain text
#: src/communication/private-communication/index.md:104
msgid "If use end-to-end encryption is not possible, make sure to use recommended services, which are hosted on trusted servers by trusted providers."
msgstr "ถ้าเป็นไปไม่ได้ที่จะใช้การเข้ารหัสลับแบบต้นทางถึงปลายทาง ให้แน่ใจว่าได้ใช้บริการที่แนะนำ ซึ่งตั้งอยู่บนเซิร์ฟเวอร์ที่น่าเชื่อถือโดยผู้ให้บริการที่น่าเชื่อถือ"

#. type: Title ##
#: src/communication/private-communication/index.md:105
#, no-wrap
msgid "Control who is connected to video calls and chats"
msgstr "ควบคุมว่าใครจะเชื่อมต่อกับการคุยวิดีโอและแชต"

#. type: Bullet: ' - '
#: src/communication/private-communication/index.md:117
msgid "Know who you are sending a call or chat invite to."
msgstr "รู้ว่าคุณกำลังจะส่งคำเชิญการโทรหรือแชตไปให้ใคร"

#. type: Bullet: '     - '
#: src/communication/private-communication/index.md:117
msgid "Confirm the phone numbers or email addresses you are sending to."
msgstr "ยืนยันหมายเลขโทรศัพท์หรือที่อยู่อีเมลที่คุณกำลังจะส่งไป"

#. type: Bullet: '     - '
#: src/communication/private-communication/index.md:117
msgid "Do not share invitations on public social media."
msgstr "อย่าเผยแพร่คำเชิญบนสื่อสังคมสาธารณะ"

#. type: Bullet: ' - '
#: src/communication/private-communication/index.md:117
msgid "Before you begin a call, familiarize yourself with the administrator or moderator tools that let you mute, block, or eject unwanted participants. Set it up so only moderators have the ability to mute and block on video calls."
msgstr ""

#. type: Bullet: ' - '
#: src/communication/private-communication/index.md:117
msgid "When a call or chat begins, do not assume you know who is connected only by reading assigned names. It is not difficult for someone to enter the name of someone you know as their user name, and pretend to be them."
msgstr ""

#. type: Bullet: '     - '
#: src/communication/private-communication/index.md:117
msgid "Check the identities of everyone on the call by asking them to speak or to switch on their camera."
msgstr ""

#. type: Bullet: '     - '
#: src/communication/private-communication/index.md:117
msgid "Confirm in another channel (like secure chat or email) that the person in a call is who their screen name says they are."
msgstr ""

#. type: Bullet: '     - '
#: src/communication/private-communication/index.md:117
msgid "Take screenshots or record unwanted participants to collect evidence for later analysis and legal actions."
msgstr ""

#. type: Bullet: '     - '
#: src/communication/private-communication/index.md:117
msgid "Eject anyone who you do not wish to have in the call or conversation."
msgstr ""

#. type: Bullet: ' - '
#: src/communication/private-communication/index.md:117
msgid "If ejecting or blocking someone does not work, start an entirely new conversation. Double-check the list of phone numbers or email addresses you are sending to, to make sure they are correct for the people you want in the conversation. Contact each person through a different channel to confirm (for example, using a phone call if you think their email address is wrong)."
msgstr ""

#. type: Plain text
#: src/communication/private-communication/index.md:121
msgid "Online services do not prevent people from signing in to calls or chats using someone else's name. To be sure everyone is who they say they are, verify their identity before you discuss sensitive details."
msgstr ""

#. type: Plain text
#: src/communication/private-communication/index.md:124
#, no-wrap
msgid ""
"During the COVID pandemic, many people learned the hard way that people who wanted to harass them could find their way into their organizing video calls. When moderators have the ability to mute or remove participants from calls, it ensures video calls can proceed without interruption.\n"
"</details>\n"
msgstr ""

#. type: Title ##
#: src/communication/private-communication/index.md:125
#, no-wrap
msgid "Change video calls to mic and camera off by default"
msgstr ""

#. type: Bullet: '- '
#: src/communication/private-communication/index.md:130
msgid "Assume that when you connect, your camera and microphone may be turned on by default. Check this setting immediately, and be careful what you show and say until you know these are off."
msgstr ""

#. type: Bullet: '- '
#: src/communication/private-communication/index.md:130
msgid "Consider covering your camera with a sticker or adhesive bandage, and only remove it when you use the camera."
msgstr ""

#. type: Bullet: '- '
#: src/communication/private-communication/index.md:130
msgid "Consider disabling your microphone and camera in the device settings, if you cannot turn them off it in the service before connecting."
msgstr ""

#. type: Title ##
#: src/communication/private-communication/index.md:131
#, no-wrap
msgid "Set rules about capturing and participating in video calls"
msgstr "ตั้งกฎเกี่ยวกับการจับภาพหน้าจอและการมีส่วนร่วมในการคุยวิดีโอ"

#. type: Bullet: ' - '
#: src/communication/private-communication/index.md:141
msgid "Agree on ground rules before a meeting starts. These could address:"
msgstr "ตกลงกฎพื้นฐานก่อนจะเริ่มการประชุม โดยมันอาจพูดถึง:"

#. type: Bullet: '     - '
#: src/communication/private-communication/index.md:141
msgid "whether participants will use real names or a nick names"
msgstr "ว่าผู้เข้าร่วมจะใช้ชื่อจริง ชื่อเล่น หรือชื่อเรียกอื่น ๆ"

#. type: Bullet: '     - '
#: src/communication/private-communication/index.md:141
msgid "whether you will keep your cameras on or off"
msgstr "ว่าคุณจะเปิดหรือจะปิดกล้อง"

#. type: Bullet: '     - '
#: src/communication/private-communication/index.md:141
msgid "whether participants will keep their microphones on or off when not speaking"
msgstr "ว่าผู้เข้าร่วมจะเปิดหรือปิดไมโครโฟนในเวลาที่ตัวเองไม่ได้พูด"

#. type: Bullet: '     - '
#: src/communication/private-communication/index.md:141
msgid "how participants will indicate that they would like to speak"
msgstr "ผู้เข้าร่วมจะส่งสัญญาณได้อย่างไรเมื่อเขาต้องการจะพูด"

#. type: Bullet: '     - '
#: src/communication/private-communication/index.md:141
msgid "who will lead the meeting"
msgstr "ใครจะเป็นคนนำประชุม"

#. type: Bullet: '     - '
#: src/communication/private-communication/index.md:141
msgid "who will take notes, and where, whether, and how will those notes be written and distributed"
msgstr "ใครจะบันทึกการประชุม และบันทึกดังกล่าวจะถูกเขียนและเผยแพร่ต่ออย่างไร ที่ไหน และอย่างไร"

#. type: Bullet: '     - '
#: src/communication/private-communication/index.md:141
msgid "whether it is acceptable to take screenshots of or record a video call, etc."
msgstr "ว่าสามารถจับภาพหน้าจอ บันทึกวิดีโอได้ หรือบันทึกหลักฐานการประชุมในรูปแบบอื่น ๆ ได้หรือไม่"

#. type: Plain text
#: src/communication/private-communication/index.md:145
msgid "If your participants are concerned for their safety, a video record of their participation on the call could put them at risk of their location and their affiliation with your group being identified. Set ground rules in advance about cameras and microphones if this is a risk. Remember, ground rules alone will not stop someone from recording the call."
msgstr ""

#. type: Plain text
#: src/communication/private-communication/index.md:148
#, no-wrap
msgid ""
"Rules about keeping microphones off when you are not speaking, and how you will take turns, can also ensure a smoother, less stressful meeting.\n"
"</details>\n"
msgstr ""

#. type: Title ##
#: src/communication/private-communication/index.md:149
#, no-wrap
msgid "Use a headset"
msgstr "ใช้ชุดหูฟัง"

#. type: Plain text
#: src/communication/private-communication/index.md:155
#, no-wrap
msgid ""
"Using headphones or earbuds ensures that someone near you who is listening to your conversation cannot overhear what others on the call are saying (though of course they would be able to hear you).\n"
"</details>\n"
msgstr ""

#. type: Title ##
#: src/communication/private-communication/index.md:157
#, no-wrap
msgid "Check what can be seen and heard"
msgstr "ตรวจสอบว่าอะไรที่สามารถเห็นและได้ยินได้"

#. type: Bullet: ' - '
#: src/communication/private-communication/index.md:165
msgid "Be mindful of what is visible in the background of your video, both who and what is in the frame."
msgstr ""

#. type: Bullet: ' - '
#: src/communication/private-communication/index.md:165
msgid "You may not want to give away too much information on your house, family pictures, notes on the walls or boards, the natural environment or the city outside your window, etc."
msgstr ""

#. type: Bullet: ' - '
#: src/communication/private-communication/index.md:165
msgid "Choose a location with a blank wall behind you if possible, or clear away clutter."
msgstr ""

#. type: Bullet: ' - '
#: src/communication/private-communication/index.md:165
msgid "Test what is in the background _before_ a call by, for example, opening your camera app or meet.jit.si and starting an empty meeting with your camera switched on so you can see what is in the picture."
msgstr ""

#. type: Bullet: ' - '
#: src/communication/private-communication/index.md:165
msgid "Be mindful who and what can be heard in the background."
msgstr "ให้ระมัดระวังว่ามีเสียงใครหรืออะไรที่สามารถได้ยินจากรอบ ๆ"

#. type: Bullet: ' - '
#: src/communication/private-communication/index.md:165
msgid "Close the door and windows, or alert those sharing your space about your meeting."
msgstr "ปิดประตูและหน้าต่าง หรือบอกกับคนที่อยู่ในพื้นที่เดียวกันกับคุณให้รู้ถึงการประชุม"

#. type: Plain text
#: src/communication/private-communication/index.md:170
#, no-wrap
msgid ""
"Human rights defenders have been found by attackers based on what can be seen and heard in the background when they are on calls, both audio and video. Keep track of the following to stay safe.\n"
"</details>\n"
msgstr ""

#. type: Title #
#: src/communication/private-communication/index.md:171
#, no-wrap
msgid "Use secure chat"
msgstr "ใช้แชตที่ปลอดภัย"

#. type: Bullet: ' - '
#: src/communication/private-communication/index.md:175
msgid "See our [recommendations for chat tools](../tools#more-secure-text-voice-and-video-chat-applications) in the Related Tools section."
msgstr ""

#. type: Bullet: ' - '
#: src/communication/private-communication/index.md:175
msgid "Also see our recommendations about [Anonymity while browsing the Web](../../internet-connection/anonymity-and-circumvention/#anonymity-while-browsing-the-web)."
msgstr ""

#. type: Plain text
#: src/communication/private-communication/index.md:180
#, no-wrap
msgid ""
"Chat services may access and collect information on your location, activity, content, and whom you are speaking to. Picking the right one is important to protecting your safety.\n"
"</details>\n"
msgstr ""

#. type: Title ##
#: src/communication/private-communication/index.md:181
#, no-wrap
msgid "Delete chat history"
msgstr "ลบประวัติแชต"

#. type: Bullet: '- '
#: src/communication/private-communication/index.md:189
msgid "Find the option in an app to delete all conversations, or specific ones."
msgstr ""

#. type: Bullet: '    - '
#: src/communication/private-communication/index.md:189
msgid "If you are deleting a chat app, delete messages first before you remove the app, for extra assurance the messages will be removed."
msgstr ""

#. type: Bullet: '    - '
#: src/communication/private-communication/index.md:189
msgid "If there is no option to automatically make messages disappear, learn how to remove messages manually. Ask your contacts to do the same."
msgstr ""

#. type: Bullet: '- '
#: src/communication/private-communication/index.md:189
msgid "You may also be able to set messages to \"expire,\" or be deleted from your phone and the recipient's phone after a certain amount of time."
msgstr ""

#. type: Bullet: '    - '
#: src/communication/private-communication/index.md:189
msgid "Consider how long you want the messages to be visible before they expire. You may have the option to make them expire in minutes, hours, or days."
msgstr ""

#. type: Bullet: '    - '
#: src/communication/private-communication/index.md:189
msgid "Disappearing messages do not guarantee that your messages will never be found! It is possible that someone could take a screenshot of the messages or that someone could take a photo of the screen using another camera."
msgstr ""

#. type: Plain text
#: src/communication/private-communication/index.md:194
#, no-wrap
msgid ""
"Chat apps keep a record of everything you and your contacts have said or shared, by default. Use the disappearing messages option to limit the amount of information an app stores on your phone.\n"
"</details>\n"
msgstr ""

#. type: Title ##
#: src/communication/private-communication/index.md:195
#, no-wrap
msgid "Take identifying information out of your photos and other files"
msgstr ""

#. type: Bullet: '- '
#: src/communication/private-communication/index.md:199
msgid "Use [Obscuracam](../../files/tools#obscuracam), [Scrambled Exif](../../files/tools#scrambled-exif), [MetaX](../../files/tools#metax), or [Exifcleaner](../../files/tools#exifcleaner)."
msgstr ""

#. type: Bullet: '- '
#: src/communication/private-communication/index.md:199
msgid "See [this article on removing information from photos](https://freedom.press/training/redacting-photos-on-the-go/) for additional techniques."
msgstr ""

#. type: Plain text
#: src/communication/private-communication/index.md:203
msgid "It may seem like simply using a face blur feature or covering sensitive details will protect the people or places in your images. However, there are some ways of saving edited photos which do not stop someone who has the file from seeing what you were trying to hide."
msgstr ""

#. type: Plain text
#: src/communication/private-communication/index.md:205
msgid "All files have a small amount of information saved about where and how they were created. This information is called metadata. You can usually get a look at some of a file's metadata on a computer by right-clicking on the file and selecting \"Properties\" or \"Get info.\""
msgstr "แฟ้มทุกแฟ้มจะมีข้อมูลเล็ก ๆ บันทึกเอาไว้ว่ามันถูกสร้างขึ้นที่ไหนและอย่างไร ข้อมูลนี้เรียกว่า เมทาดาทา ปกติแล้วคุณจะสามารถดูเมทาดาทาของแฟ้มบนคอมพิวเตอร์ได้ โดยการคลิกขวาบนแฟ้ม และเลือก \"คุณสมบัติ\" หรือ \"ขอรายละเอียด\""

#. type: Plain text
#: src/communication/private-communication/index.md:207
#, no-wrap
msgid ""
"Some metadata may include your location or the device the file was made with: information that someone looking at the file could use to identify you. [See this article for more information on metadata](https://freedom.press/training/everything-you-wanted-know-about-media-metadata-were-afraid-ask/ ), and how you can clean it from your files.\n"
"</details>\n"
msgstr ""

#~ msgid "[Tutanota](https://tutanota.com/)"
#~ msgstr "[Tutanota](https://tutanota.com/)"
