# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-02-20 14:04+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Yaml Front Matter Hash Value: title
#: src/communication/account-compromise/index.md:1
#, no-wrap
msgid "Recover from possible account compromise"
msgstr ""

#. type: Plain text
#: src/communication/account-compromise/index.md:9
#, markdown-text
msgid "While it is rarely possible to know for sure if your account has been compromised, below are clues that someone might have tampered with your account."
msgstr ""

#. type: Title #
#: src/communication/account-compromise/index.md:10
#, markdown-text, no-wrap
msgid "Look for signs that your account may have been tampered with"
msgstr ""

#. type: Bullet: '- '
#: src/communication/account-compromise/index.md:23
#, markdown-text
msgid "Changes to content, or to your account settings, that you did not make"
msgstr ""

#. type: Bullet: '- '
#: src/communication/account-compromise/index.md:23
#, markdown-text
msgid "A contact received a message that you did not send"
msgstr ""

#. type: Bullet: '    - '
#: src/communication/account-compromise/index.md:23
#, markdown-text
msgid "Be sure the message was sent from your account, and that its headers were not \"spoofed\""
msgstr ""

#. type: Bullet: '- '
#: src/communication/account-compromise/index.md:23
#, markdown-text
msgid "You cannot login to your account even though the user name and password you entered is correct"
msgstr ""

#. type: Bullet: '- '
#: src/communication/account-compromise/index.md:23
#, markdown-text
msgid "You don't receive messages that contacts are certain they sent"
msgstr ""

#. type: Bullet: '    - '
#: src/communication/account-compromise/index.md:23
#, markdown-text
msgid "Make sure they were not put in your spam folder"
msgstr ""

#. type: Bullet: '- '
#: src/communication/account-compromise/index.md:23
#, markdown-text
msgid "A third party has information exchanged exclusively through this account, even though neither the sender nor the receiver(s) shared it with anyone else"
msgstr ""

#. type: Bullet: '- '
#: src/communication/account-compromise/index.md:23
#, markdown-text
msgid "You receive notification of password change requests that you did not make, whether they were successful or not"
msgstr ""

#. type: Bullet: '- '
#: src/communication/account-compromise/index.md:23
#, markdown-text
msgid "You lost a mobile device, or had one stolen or confiscated, while it was signed into the account"
msgstr ""

#. type: Bullet: '- '
#: src/communication/account-compromise/index.md:23
#, markdown-text
msgid "If your service provider offers a log of recent account activity, you notice connections at a time, or from a location, that you could not have made yourself"
msgstr ""

#. type: Bullet: '- '
#: src/communication/account-compromise/index.md:23
#, markdown-text
msgid "You received a message from the service provider of someone trying to log in to your account"
msgstr ""

#. type: Plain text
#: src/communication/account-compromise/index.md:25
#, markdown-text
msgid "If you notice one or more of these indicators — and other explanations seem unlikely — consider taking some or all of the following steps:"
msgstr ""

#. type: Title #
#: src/communication/account-compromise/index.md:26
#, markdown-text, no-wrap
msgid "Stop using this account to exchange sensitive information"
msgstr ""

#. type: Bullet: '- '
#: src/communication/account-compromise/index.md:29
#, markdown-text
msgid "Wait to use it again until you have a better understanding of what is going on."
msgstr ""

#. type: Title #
#: src/communication/account-compromise/index.md:30
#, markdown-text, no-wrap
msgid "Change your password immediately"
msgstr ""

#. type: Bullet: '- '
#: src/communication/account-compromise/index.md:33
#, markdown-text
msgid "(If you can.) See our guide on how to [create and maintain strong passwords](../../passwords/passwords)."
msgstr ""

#. type: Title #
#: src/communication/account-compromise/index.md:34
#, markdown-text, no-wrap
msgid "Create new, unique passwords for other accounts"
msgstr ""

#. type: Bullet: '- '
#: src/communication/account-compromise/index.md:37
#, markdown-text
msgid "This is crucial if your other accounts have the same or similar passwords."
msgstr ""

#. type: Title #
#: src/communication/account-compromise/index.md:38
#, markdown-text, no-wrap
msgid "If you are not able to log in"
msgstr ""

#. type: Bullet: '- '
#: src/communication/account-compromise/index.md:44
#, markdown-text
msgid "You may be able to contact the service provider to reclaim your account. Not all social media and email providers have procedures in place for this situation, but some do."
msgstr ""

#. type: Bullet: '- '
#: src/communication/account-compromise/index.md:44
#, markdown-text
msgid "Consider getting expert help."
msgstr ""

#. type: Bullet: '    - '
#: src/communication/account-compromise/index.md:44
#, markdown-text
msgid "Technologists or security researchers in your community may be able to provide assistance."
msgstr ""

#. type: Bullet: '    - '
#: src/communication/account-compromise/index.md:44
#, markdown-text
msgid "[Access Now Digital Security Helpline](https://www.accessnow.org/help/) offers emergency technical assistance and advice for activists, journalists, human rights defenders and other members of civil society."
msgstr ""

#. type: Title #
#: src/communication/account-compromise/index.md:45
#, markdown-text, no-wrap
msgid "Review the security of devices connected to this account"
msgstr ""

#. type: Bullet: '- '
#: src/communication/account-compromise/index.md:49
#, markdown-text
msgid "Review the security of other devices that have accessed this account or on which you have stored the password to this account."
msgstr ""

#. type: Bullet: '- '
#: src/communication/account-compromise/index.md:49
#, markdown-text
msgid "See our guides on how to [protect your device from malware and phishing attacks](../../phones-and-computers/malware/) and how to [protect your information from physical threats](../../phones-and-computers/physical-security)."
msgstr ""

#. type: Title #
#: src/communication/account-compromise/index.md:50
#, markdown-text, no-wrap
msgid "Check services that you have \"linked\" to this account"
msgstr ""

#. type: Bullet: '- '
#: src/communication/account-compromise/index.md:56
#, markdown-text
msgid "Many third party services allow you to log in with your Facebook, Google or Microsoft account."
msgstr ""

#. type: Bullet: '- '
#: src/communication/account-compromise/index.md:56
#, markdown-text
msgid "Make a list of linked accounts (search your inbox for \"verify your email\" or \"account confirmation\" to find a lot of these quickly)."
msgstr ""

#. type: Bullet: '- '
#: src/communication/account-compromise/index.md:56
#, markdown-text
msgid "Try to \"unlink\" them if you can."
msgstr ""

#. type: Bullet: '- '
#: src/communication/account-compromise/index.md:56
#, markdown-text
msgid "Prioritize financial and other more sensitive communications accounts."
msgstr ""

#. type: Title #
#: src/communication/account-compromise/index.md:57
#, markdown-text, no-wrap
msgid "Log out of all sessions"
msgstr ""

#. type: Bullet: '- '
#: src/communication/account-compromise/index.md:62
#, markdown-text
msgid "Email and other communications providers often give you the ability to see all the places where your account is logged in. Use this to log out locations and connections you do not recognize."
msgstr ""

#. type: Bullet: '- '
#: src/communication/account-compromise/index.md:62
#, markdown-text
msgid "You may need to log back in on devices you want to use to access the account."
msgstr ""

#. type: Bullet: '- '
#: src/communication/account-compromise/index.md:62
#, markdown-text
msgid "See our basic security guides for your phones and computers for more information."
msgstr ""

#. type: Title #
#: src/communication/account-compromise/index.md:63
#, markdown-text, no-wrap
msgid "Take steps to protect your accounts"
msgstr ""

#. type: Bullet: '- '
#: src/communication/account-compromise/index.md:65
#, markdown-text
msgid "Follow the instructions in our [social media guides](../social-media/) on each service provider under \"If your account has been lost, stolen, or hacked\" or \"Account protection.\""
msgstr ""
