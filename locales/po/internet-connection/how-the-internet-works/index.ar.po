# Arabic translations for PACKAGE package
# Copyright (C) 2024 Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# Automatically generated, 2024.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-02-20 14:04+0000\n"
"PO-Revision-Date: 2025-02-14 21:28+0000\n"
"Last-Translator: Ahmad Gharbeia <ahmad@gharbeia.org>\n"
"Language-Team: Arabic <https://weblate.securityinabox.org/projects/internet-connection/how-the-internet-works/ar/>\n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 ? 4 : 5;\n"
"X-Generator: Weblate 5.8.3\n"

#. type: Yaml Front Matter Hash Value: title
#: src/internet-connection/how-the-internet-works/index.md:1
#, no-wrap
msgid "How the internet works and how it can be censored"
msgstr "كيف تعمل الإنترنت و&nbsp;كيف يمكن حجبها"

#. type: Title #
#: src/internet-connection/how-the-internet-works/index.md:8
#, no-wrap
msgid "How the internet works, and how some countries and other entities block or censor websites and online services"
msgstr "كيف تعمل الإنترنت و&nbsp;كيف تحجب حكومات بعض الدول أو مؤسسات غيرها المواقع و&nbsp;الخدمات"

#. type: Plain text
#: src/internet-connection/how-the-internet-works/index.md:11
msgid "The internet is an international public resource, made up of computers, phones, servers, routers and other devices connected to each other. It was designed to continue providing service even if part of the network was destroyed."
msgstr "الإنترنت شبكة عالمية ما بين شبكات، تتألف من حواسيب و&nbsp;نبائط حاسوبية و&nbsp;شبكية في أنحاء العالم، هي عقد الشبكة، متّصلة ببعضها البعض بشتّى الوسائل، من كوابل و&nbsp;اتصالات لاسلكية، و&nbsp;تدير تلك المكونات و&nbsp;الصِّلات جهات متباينة، لكن يجمعها بروتوكول قياسي مفتوح هو بروتوكول الإنترنت المعروف اختصارًا باسم IPK و&nbsp;وظيفتها الوحيدة تمرير الرسائل عبرها من طرق إلى طرف. و&nbsp;هذه الشبكة صُمِّمت في الأصل لامركزية بغض أن تكون قادرة على العمل حتّى إن تعطّلت بعض أجزائها."

#. type: Plain text
#: src/internet-connection/how-the-internet-works/index.md:15
#, no-wrap
msgid ""
"> No one person, company, organization or government runs the internet.\n"
">\n"
"> [*Wikipedia page on internet governance*](https://en.wikipedia.org/wiki/Internet_governance)\n"
msgstr "الإنترنت مشاع عالمي مشترك لا تملكها جهة بعينها و&nbsp;لا تتحكم فيها كلّها حكومة و&nbsp;لا منظّمة و&nbsp;لا شركة، و&nbsp;توجد صيرورات معقّدة لحوكمة جوانبها المختلفة. مقالة ويكيبيديا الإنگليزية المعنونة [Internet governance](https://en.wikipedia.org/wiki/Internet_governance) مدخل جيّد إلى موضوع حوكمة الإنترنت.\n"

#. type: Plain text
#: src/internet-connection/how-the-internet-works/index.md:17
msgid "However, national and international bodies have some control over the infrastructure of the internet in their jurisdiction. Many countries prevent internet users within their borders from accessing certain websites or online services. Businesses, schools, libraries and other institutions may rely on similar filters to \"protect\" their employees, students and customers from material they consider harmful or distracting."
msgstr "و بالرغم من ذلك فإنّ جهات محليّة و&nbsp;دولية تتحكّم في أجزاء من البنية التحتية للإنترنت في نطاق سيادتها. فبعض البلاد تمنع الناس في نطاق حدودها من النفاذ إلى مواقع بعينها أو خدمات. كما أن مؤسسات الأعمال و&nbsp;المدارس و&nbsp;غيرها من منظمّات قد تضع مُرشِّحات بغرض حماية التلاميذ من المحتوى غير المناسب، أو لترشيد استخدام الإنترنت وفق احتياجات العمل، أو منع الموظفين من الانشغال بغير مهامهم الوظيفية."

#. type: Plain text
#: src/internet-connection/how-the-internet-works/index.md:19
msgid "Internet filtering can be based on different technologies. Some filters block sites based on their IP addresses (identifying numbers that may be shaped like `172.105.249.143` or `2a01:7e01::f03c:92ff:fecd:7e45` and are assigned to every phone, computer, router and website to deliver internet content to those who have requested it). Others block particular domain names (the web addresses you may be more familiar with, like `google.com` or this website, `securityinabox.org`). Some filters block all addresses except an official list of allowed sites defined by the authority that have devised those filters. Other filters search through unencrypted internet traffic and cause the internet infrastructure to ignore requests that include specific keywords (for example, searches that include \"human rights violations\" or names of opposition leaders)."
msgstr "تعمل مُرشِّحات الإنترنت وفق مبادئ تقنية متباينة. فبعضها يسمح بالنفاذ أو يمنعه حسب عنوان آيپي (و هو عنوان فريد على النِّسَقِ`172.105.249.143` أو `2a01:7e01::f03c:92ff:fecd:7e45`يُعيّن لكل نبيطة متّسلة بالشبكة فيمكن عنونة الرسائل إليها). أما بعضها الآخر فيحظر أسماء نطاقات بعينها من قائمة سوداء (و هي الأسماء التي تميّز مواقع الوِب التي تزورون، مثل هذا الموقع `securityinabox.org`). كما يمكن للقائمبن على الحظر تفعيل طور القائمة البيضاء لحظر كل العناوين و&nbsp;النطاقات باستثناء ما في قائمة من المواقع المسموح بها. و&nbsp;لبعض المُرشِّحات القدرة على فحص محتوى التدفقات الشبكية المارّة عبرها لحظر ما يحتوي منها على كلمات مفتاحية معينة، مثلا أسماء قادة المعارضة السياسية أو الكلمات المتربطة بموضوعات حقوق الإنسان."

#. type: Plain text
#: src/internet-connection/how-the-internet-works/index.md:21
msgid "You can often bypass these filters by installing software that uses intermediary servers, located in other countries, to pass content between the blocked content or service you are trying to reach and your device. Learn more about these tools in [the guide on how to circumvent internet blockages and monitoring](../circumvention)."
msgstr "يمكن تجاوز تلك المُرشِّحات عادةً باستعمال برمجيات تُمَرِّر اتصالاتك الشبكية عبر نبائط وسيطة موجودة في بلاد أخرى ليست فيها رقابة. المزيد عن استخدام تلك الوسائل مشروح في [قسم هذا الدليل في كيفية تجاوز حجب الإنترنت و&nbsp;مراقبتها](../circumvention)."

#. type: Title #
#: src/internet-connection/how-the-internet-works/index.md:22
#, no-wrap
msgid "Understanding how websites and online services can be blocked"
msgstr "حجب مواقع الوِب و&nbsp;الخدمات على الإنترنت"

#. type: Plain text
#: src/internet-connection/how-the-internet-works/index.md:25
msgid "Research carried out by organisations like the [Open Observatory of Network Interference (OONI)](https://ooni.org/) and [Reporters Without Borders (RSF)](https://rsf.org/en/recherche?text=censorship) indicates that many countries filter a wide variety of social, political and \"national security\" content, but rarely publish lists of what they block. Governments that wish to control their citizens' access to the internet also block VPNs and proxies they are aware of, as well as websites that offer tools and instructions to help people get around filters."
msgstr "الأبحاث التي تجريها جهات مثل مرصد التدخُّلات في الإنترنت المفتوح ([OONI](https://ooni.org/)) و&nbsp;[مراسلون بلا حدود (RSF)](https://rsf.org/en/recherche?text=censorship) تظهر أنّ كثيرًا من الدول تحجب محتوى متنوّعًا بدوافع اجتماعية و&nbsp;سياسية و&nbsp;متعلّقة \"بالأمن القومي\" إلا أنّ قليلا منها تُعلن عمّا تحجبه. الحكومات الراغبة في التحكّم في نفاذ مواطنيها إلى الإنترنت تحجب كذلك الوسطاء الذين تعرفهم، من مقدّمي خدمات VPN و&nbsp;خواديم وسيطة، و&nbsp;كذلك المواقع التي تتيح أدوات و&nbsp;إرشادات إلى كيفية تجاوز الحجب و&nbsp;المراقبة."

#. type: Plain text
#: src/internet-connection/how-the-internet-works/index.md:27
msgid "Article 19 of the Universal Declaration of Human Rights guarantees free access to information. Despite this, the number of countries censoring the internet continues to increase. As this practice spreads throughout the world, however, so does access to anti-blocking tools that have been created, deployed and publicised by activists, programmers and volunteers, with funding from the United States, Canada, the European Union, NGOs and other bodies concerned with free speech."
msgstr "تكفل المادة 19 من الإعلان العالمي لحقوق الإنسان الحق في استقاء المعلومات بحريّة، و&nbsp;بالرغم من ذلك فإنَّ عدد الدول التي تمارس حجب الإنترنت يتزايد باستمرار، و&nbsp;باستفحال هذه الممارسة تنتشر كذلك المعرفة بوسائل تجاوز الحجب و&nbsp;الأدوات و&nbsp;البنية التحتية المعينة على ذلك، و&nbsp;منها ما يطوّرها و&nbsp;يشغّلها نشطاء و&nbsp;تقنيون و&nbsp;متطوعون، أحيانا بدعم مالي من حكومات الولايات المتّحدة و&nbsp;كندا و&nbsp;الاتحاد الأوربي و&nbsp;منظّمات أهلية و&nbsp;غيرها من المؤسسات المعنية بحرية الرأي و&nbsp;التعبير."

#. type: Title #
#: src/internet-connection/how-the-internet-works/index.md:28
#, no-wrap
msgid "Your internet connection"
msgstr "الاتصال بالإنترنت"

#. type: Plain text
#: src/internet-connection/how-the-internet-works/index.md:31
msgid "![](../../../media/en/anonymity-and-circumvention/internet_connection.png)"
msgstr "![](../../../media/en/anonymity-and-circumvention/internet_connection.png)"

#. type: Plain text
#: src/internet-connection/how-the-internet-works/index.md:33
msgid "When you ask your computer to access a website, or your phone to use an app, your request and the content you see in response both go through a number of network devices. Your phone or computer first uses its wired, wireless or mobile data connection to reach your internet service provider (ISP). If you are home and using your own wifi, this ISP may be the company you pay for internet service. If you are using mobile data, the ISP you are connecting to is probably your mobile service provider. If you are working from an office, school, internet cafe or some other public space, it may be difficult to determine what ISP you are connecting to."
msgstr "عندما تأمر حاسوبك بالاتّصال بخادوم ما على الإنترنت، بطريق طلب موقع في المتصفذح أو باستخدام تطبيق على الهاتف فإنّ التطبيق يُرسل إلى الخادوم طلبًا يمرّ عب العديد من عُقد الشبكة قبل الوصول إلى وجهته، و&nbsp;كذلك الرّد الراجع من الخادوم إلى حاسوبك. فالحاسوب يتّصل سلكيًّا أو لاسلكيًّا بمُقدّم خدمة الاتّصال بالإنترنت إليك (ISP). فإذا كان الاتّصال يجري في المنزل فقد يعني هذا أن مُقدّم الخدمة هي الشركة التي تدفع لها مقابل الخدمة، و&nbsp;إذا كنت تستخدم الهاتف عبر شبكة المحمول فمقدّم الخدمة في هذه الحالة شركةُ المحمول، أما إن كنت تتصل من المكتب أو من الجامعة أو مقهى إنترنت أو مكان عام غيرها فقد يكون مُقدّم الخدمة طرفًا آخر."

#. type: Plain text
#: src/internet-connection/how-the-internet-works/index.md:35
msgid "Your ISP relies on the network infrastructure in your country to connect its users with the rest of the world. The ISP assigns an IP address to the local network or device you are using to connect, whether it is your home wifi, your mobile phone or the network of the internet cafe, school or hotspot you are using. Your ISP will use this address to send content to your device. On the other end of your connection, the website or internet service you are accessing will have received its own IP address from an ISP in its own country."
msgstr "يعتمد مُقدِّم خدمة الاتّصال بالإنترنت على البنية التحتية الشبكية القائمة في منطقتك لتوصيلك بالإنترنت. فيُعيِّن عنوان آيپي فريد للنبيطة التي تتّصل بها بالشبكة، التي قد تكون جهاز الاتّصال الشبكي المنزلي أو جهاز الهاتف، فتصير نبيطتك بدورها عقدة في الشبكة يمكنها إرسال و&nbsp;تلقّي الرسائل، و&nbsp;على الطرف الآخر يكون للخادوم الذي تتواصل معه عنوان آيپي فريد عيّنه له مُقدِّم الخدمة في النطاق الواقع فيه."

#. type: Plain text
#: src/internet-connection/how-the-internet-works/index.md:37
msgid "Online services can use this address to send you the web pages you are trying to view and other data you request. (Your device has its own IP address, which is how your router sends everybody on your network their own traffic. It is only used on your local network.)"
msgstr "عناوين آيپي ليست سريّة و&nbsp;لا يمكن إخفاؤها عن كُلِّ الأطراف في الشبكة. الخدمات على الإنترنت و&nbsp;مواقع الوِب تتواصل فيما بينها بالاعتماد على عناوينها الفريدة لتمرير الطلبات و&nbsp;الردود، بغضّ النظر عن فحواها. هذا جوهر عمل الإنترنت و&nbsp;وظيفتها الوحيدة."

#. type: Plain text
#: src/internet-connection/how-the-internet-works/index.md:39
msgid "Anyone who knows your IP address can figure out what city or region you are in. Certain entities can determine your precise location even more precisely:"
msgstr "و لأن توزيع عناوين آيپي في العالم يُنظم تراتبيًا و&nbsp;جعرافيًا لتسهيل إدارة العناوين، فإنّ مَن يعرف عنوان آيپي المعيّن لنبيطتك، تمكنه معرفه الموضع الذي تتصّل منه، بدرجات متباينة من الدقّة:"

#. type: Bullet: '- '
#: src/internet-connection/how-the-internet-works/index.md:44
msgid "**Your mobile provider** knows the precise physical location of your phone while your device is on, by triangulating your location among its cell towers."
msgstr "**مقدِّم خدمة الاتّصال المحمول** تمكنه معرفة موقع الهاتف بطريق حساب المثلثات استنادًا إلى معرفة أبراج الاتّصال الخلوي المتّصلة بالهاتف، و&nbsp;هذا يختلف كليّة عن تحديد الموقع بمعرفة عنوان آيپي و&nbsp;لا يتوقّف عليه."

#. type: Bullet: '- '
#: src/internet-connection/how-the-internet-works/index.md:44
msgid "**Your ISP** will likely know what building you are in."
msgstr "**الجهة مُقدِّمة خدمة الاتّصال بالإنترنت** تمكنها معرفة المبنى الذي تتواجد فيه."

#. type: Bullet: '- '
#: src/internet-connection/how-the-internet-works/index.md:44
msgid "**An internet cafe, library or business** where you are accessing the internet will know which of their computers you were using at any given time. And if you are using your own device, they will know which local IP address was assigned to your device on their local network, thus associating all your activities to your device."
msgstr "**في مقهى الإنترنت أو مكتبة الجامعة أو مكتبك في مقر عملك** تمكن لمديري الشبكة دومًا معرفة الحاسوب الذي استخدمته، و&nbsp;إذا استخدمت حاسوبك الخاص فإنهم يعرفون عنوان الآپي المعيّن للحاسوب في شبكتهم، و&nbsp;بذلك يعرفون أنشطتك عبر الإنترنت."

#. type: Bullet: '- '
#: src/internet-connection/how-the-internet-works/index.md:44
msgid "**Government agencies** may know all of the above. And even if they do not, they can often use their influence to find out."
msgstr "**الجهات الحكومية** تمكنها معرفة كل ما سبق، بطريق إجبار مقدّمي الخدمات على الإفصاح عن البيانات، بطرق قانونية أو غير قانونية، أو بطريق التحكم المباشر في البنية التحتية أو بكون الحكومة نفسها مقدّمة الخدمة."

#. type: Plain text
#: src/internet-connection/how-the-internet-works/index.md:46
msgid "Internet communication is somewhat more complex than the description above, but even this simplified model can help you figure out what risks you face when connecting to the internet and what institution or entity could be blocking a website you're trying to connect to."
msgstr "و بالرغم من كون الاتّصالات الشبكية عبر الإنترنت أكثر تعقيدًا مما في هذا الشرح المبسّط، فإنّ هذا النموذج المبسّط يبيّن المخاطر المحتملة و&nbsp;أي الفاعلين لديهم القدرة على حجب الاتّصال أو مراقبته."

#. type: Title #
#: src/internet-connection/how-the-internet-works/index.md:47
#, no-wrap
msgid "How websites are blocked"
msgstr "كيفية حجب مواقع الوِب"

#. type: Plain text
#: src/internet-connection/how-the-internet-works/index.md:50
msgid "When you view a web page, your device connects to a domain name service (DNS) to look up the IP address associated with the website's domain name — for example, it could ask what's the IP address of `securityinabox.org` and receive the answer `172.105.249.143`. It then asks your ISP to send a request to the ISP in charge of `172.105.249.143` to ask the web server at `172.105.249.143` for the contents of securityinabox.org."
msgstr "لكي تطالع صفحة وِب فإنّ حاسوبك يتّصل ابتداءً بحاسوب متخصّص يُعرف باسم خادوم أسماء النطاقات (DNS) مستعلمًا عن عنوان آيپي المقابل لاسم نطاق الموقع. على سبيل المثال لمطالعة صفحات `securityinabox.org` يحصل على العنوان `172.105.249.143`. فيُرسِل المتصفحُ (المتصفّح) طلبًا إلى خادوم الوِب العامل في ذلك العنوان طالبا صفحة منه. يمرُّ الطلب من حاسوبك إلى مقدّم خدمة الاتّصال إليك، الذي يُمرّره بدوره عبر الشبكة، من عُقدة لى عُقدة، وصولا إلى مقدّم خدمة الاتصال إلى خادوم الوِب، وجهتهُ النهائية. و&nbsp;يرجع الردّ على نفس المنوال."

#. type: Plain text
#: src/internet-connection/how-the-internet-works/index.md:52
msgid "If you are in a country that censors securityinabox.org, your request will not succeed at some point of this process: when your device tries to look up the IP address, when it requests the content, or as the content is being sent to your device. In some countries, ISPs are required to consult a national blacklist of sites they must not show you. These blocklists can contain domain names, IP addresses, keywords or a mix of all of these. Keyword filters scan both unencrypted requests and the results a website or service returns to you."
msgstr "بناء على ما سبق وصفه، فإنْ كنت تتصل من شبكة يحجب مديرها securityinabox.org فإن الاتصال سيُحبَط في خطوة ما على ناحيتك من الاتّصال: عندما يستعلم حاسوبك عن عنوان آيپي الخادوم، أو عندما يطلب الصفحة، أو عندما يُرجِعُ الخادوم ردّه. في بعض الدول تُلزم الحكومة مقدّمي الخدمة بالاحتكام إلى قائمة سوداء مركزية للحجب، قد تحوي عناوين أو أسماء نطاقات أو كلمات مفتاحية أو أي تنويعة منها. يُمكن لمُرشِّحات الكلمات المفتاحية فحص الطلبات و&nbsp;الردود غير المُعمّاة."

#. type: Plain text
#: src/internet-connection/how-the-internet-works/index.md:54
msgid "You might not always know when you have requested a blocked webpage, content or service. Sometimes you may receive a response that explains why a particular content or service has been censored. But most of the times you will see a misleading error message, saying for example that the page or service could not be found or the address may be misspelled."
msgstr "حسب أسلوب تطبيق الحجب فقد لا يظهر لك على الدوام كون الاتّصال محجوبًا. ففي بعض الدول التي تطبّق الحجب صراحةَ تردُّ آليّات الحجب برسالة تخطرُ المستخدِمَ بحدوث الحجب و&nbsp;بسببه، لكن في أغلب الحالات يُحبَط الاتّصال في صمت على نحو يبدو للتطبيقات المتّصلة أنّه عطل في الاتّصال، و&nbsp;من ثمّ تُظهر التطبيقات رسائل تفيد تعذّر إتمام الاتّصال أو عدم استجابة الخادوم، و&nbsp;ما إلى ذلك من أعطال تقنية متعلّقة بعمل الشبكة."

#. type: Plain text
#: src/internet-connection/how-the-internet-works/index.md:56
msgid "Each blocking technique has strengths and weaknesses. When attempting to get around internet blocks, it is easier to assume the worst than to figure out what techniques are being used in your country. You might as well assume:"
msgstr "لكل أسلوب حجب نقاط قوّة و&nbsp;مثالب. عند السعي إلى تجاوز الحجب على الإنترنت فمن الأسهل افتراض الأسوأ، بدل محاولة فهم أسلوب الحجب المطبّق بالتحديد. فمن الأفضل افتراض التالي:"

#. type: Bullet: '- '
#: src/internet-connection/how-the-internet-works/index.md:62
msgid "that blocking is implemented nationally, at the ISP level _and_ on your local network,"
msgstr "أنّ الحجب مُحدثٌ عمدًا، بواسطة مُقدِّم خدمة الاتّصال (ISP) في ناحيتك من الشبكة،"

#. type: Bullet: '- '
#: src/internet-connection/how-the-internet-works/index.md:62
msgid "that DNS lookups _and_ content requests are blocked,"
msgstr "و الحجب مُطبّق على مستوى الاستعلام عن أسماء النطاقات (DNS) _و كذلك_ على مستوى المحتوى،"

#. type: Bullet: '- '
#: src/internet-connection/how-the-internet-works/index.md:62
msgid "that blocklists are maintained for both domain names and IP addresses,"
msgstr "و توجد قوائم حجب تضمُّ عناوين آيپي و&nbsp;أسماء نطاقات،"

#. type: Bullet: '- '
#: src/internet-connection/how-the-internet-works/index.md:62
msgid "that your unencrypted internet traffic is monitored for keywords, and"
msgstr "و الاتّصالات غير المُعمّاة يجري فحص فحواها،"

#. type: Bullet: '- '
#: src/internet-connection/how-the-internet-works/index.md:62
msgid "that no indication or reason will be given for the blocking."
msgstr ""

#. type: Plain text
#: src/internet-connection/how-the-internet-works/index.md:64
msgid "The safest and most effective anti-blocking tools should work regardless of the type of block you are facing."
msgstr "أسلم و&nbsp;أنجع وسيلة لتجاوز الحجب ينبغي أن تعمل بغضّ النظر عن أسلوب تطبيق الحجب."

#. type: Title #
#: src/internet-connection/how-the-internet-works/index.md:65
#, no-wrap
msgid "How tools get around blocks"
msgstr "كيفية تجاوز الأدوات الحجب"

#. type: Plain text
#: src/internet-connection/how-the-internet-works/index.md:68
msgid "![](../../../media/en/anonymity-and-circumvention/proxies.png)"
msgstr "![](../../../media/en/anonymity-and-circumvention/proxies.png)"

#. type: Plain text
#: src/internet-connection/how-the-internet-works/index.md:70
msgid "Circumvention tools like Tor and VPNs apply encryption to your traffic, thus protecting the secrecy of the requested data, including the IP address of the requested service. They hide the address until your request arrives at a proxy server in another country. The proxy then decrypts that address, sends your request to see the content, accepts the response from the page or service, encrypts it again and sends it back to your device. We sometimes use \"tunnels\" as a metaphor for this: your traffic is still passing through infrastructure controlled by the ISP, government or other institutions that wants to block your access, but their filters are unable to read the content of your request or determine exactly where you're trying to go when you leave the tunnel. All they know is that you are interacting with a proxy and that encryption is being used to prevent them from seeing the information you're requesting."
msgstr "وسائل تجاوز الحجب مثل تور و&nbsp;VPN تعمّي الاتّصالات فتحفظ سريّة الطلب، فحواه و&nbsp;عنوان آيپي المطلوب، و&nbsp;ذلك أثناء مرور التدفقات الشبكية عبر مُقدّم خدمة الاتّصال، وصولا إلى خادوم وسيط (بروكسي) خارج نطاق إنفاذ الحجب، يُظهِّرُ الاتّصال و&nbsp;يعيد إصدار الطلب إلى وجهته الفعلية، و&nbsp;يتلقّى الردّ، ثم يُعمّيه قبل تمريره رجوعًا إلى الطالب. أحيانًا يُشار إلى هذه الآلية باسم \"النفق\"، كناية عن مرور التدفقات الشبكية عبر تجهيزات مُقدّم خدمة الاتّصال أو مُرشّحات الحكومة في أنبوب مُعتِم يمنعها من الاطلاع على ما يمرّ فيه لمعرفة وجهة الاتّصال أو فحواه أو المحتوى المطلوب. ما يظهر لهم هو أنّ حاسوبك يُجري اتّصالا مُعمّى بحاسوب ما (الخادوم الوسيط)، و&nbsp;هذا ما لا يُمكن إخفاؤه."

#. type: Bullet: '- '
#: src/internet-connection/how-the-internet-works/index.md:72
msgid "[Learn how to choose the censorship circumvention tool that is right for you](../circumvention)."
msgstr "يوجد [في هذا الدليل المزيد عن كيفية اختيار وسيلة مناسبة لتجاوز الحجب](../circumvention)."

#. type: Title ##
#: src/internet-connection/how-the-internet-works/index.md:73
#, no-wrap
msgid "Blocking resistance"
msgstr "حجب المقاومة"

#. type: Plain text
#: src/internet-connection/how-the-internet-works/index.md:76
msgid "Of course, the government agency in charge of internet filters — or the company that provides your government with blocking software — might eventually identify that unknown computer as a proxy, and add it to their blocklist. This is why VPNs and other tools sometimes stop working."
msgstr "يمكن في الواقع للجهة المضطلعة بالحجب — أو الشركات التي تمدّها بوسائل الحجب — إدراك كون الحواسيب التي يتّصل بها حاسوبك خواديمَ وسيطةً و&nbsp;من ثمّ تضمين عناوينها في قوائم الحجب، أو حجب أنماط معيّنة من الاتّصالات تميّز الاتّال بالخواديم الوسيطة، و&nbsp;هذا سبب تعطّل VPN و&nbsp;تور و&nbsp;ما شابهها في بعض الحالات."

#. type: Plain text
#: src/internet-connection/how-the-internet-works/index.md:78
msgid "However, it usually takes time for those blocking the internet to discover proxies. Tools for visiting blocked websites use one or more of the following techniques:"
msgstr "إلا أن التعرّف على الخواديم الوسيطة (البروكسيّات) يستغرق بعض الوقت، لذا فأدوات تجاوز الحجب قد تطبّق الوسائل التالية:"

#. type: Bullet: '- '
#: src/internet-connection/how-the-internet-works/index.md:84
msgid "**Hidden proxies** can be distributed to users in a way that prevents censors from finding them all at once."
msgstr "**البروكسيّات الخفيّة** يمكن إعطاء بياناتها إلى المستخدمين بحيث يتعذّر حجبها كلذها مرّة واحدة."

#. type: Bullet: '- '
#: src/internet-connection/how-the-internet-works/index.md:84
msgid "**Private proxies** limit the number of people who know about and can access them, making it harder for authorities to find and block them. You can create a private proxy using for example [Outline](../tools/#outline-vpn) or [Algo](../tools/#algo-vpn)."
msgstr "**البروكسيّات الخاصة** أي غير العمومية، تسمح لعدد قليل من المتسخدمين المعلومين لمشغّليها باستخدامها، فيكون اكتشافها أصعب لعدم شيوع خبرها، توجد أدوات تساعد شخصًا في موضع لا رقابة فيها على تشغيل بروكسي لمساعدة معارفه، منها [Outline](../tools/#outline-vpn) و&nbsp;[Algo](../tools/#algo-vpn)."

#. type: Bullet: '- '
#: src/internet-connection/how-the-internet-works/index.md:84
msgid "**Disposable proxies** can be replaced more quickly than they can be blocked."
msgstr "**بروكسيّات استعمال المرّة الواحدة** يمكن استبدالها بوتيرة أسرع عندما تُحجب."

#. type: Bullet: '- '
#: src/internet-connection/how-the-internet-works/index.md:84
msgid "**Obfuscation** disguises proxy traffic as normal internet traffic thus keeping censors from identifying unknown proxies."
msgstr "**التمويه** يخفي تدفقات الاتّصال الشبكي بالبروكسي في هيئة اتّصال عادي مما يُصعب على آليات الحجب تحسّسه."

#. type: Bullet: '- '
#: src/internet-connection/how-the-internet-works/index.md:84
msgid "**Domain fronting** makes it harder to block a web address as this blockage would also block access to other useful, popular services (such as Google)."
msgstr "**تصدير أسماء النطاقات** وسيلة لتجاوز حجب مواقع الوِب بطريق تمرير طلبات صفحات الوِب من موقع محجوب ضمن اتّصال يبدو ظاهريًّا أن وجته موقع آخر غير محجوب، عادة ما يكون شهيرًا (مثل مواقع گوگل أو الخدمات السحابية الشهيرة)."

#. type: Title #
#: src/internet-connection/how-the-internet-works/index.md:85
#, no-wrap
msgid "Further Reading"
msgstr "مصادر للاستزادة"

#. type: Bullet: '- '
#: src/internet-connection/how-the-internet-works/index.md:88
msgid "Surveillance Self-Defense, [How to: Understand and Circumvent Network Censorship](https://ssd.eff.org/module/understanding-and-circumventing-network-censorship)"
msgstr "دليل الدفاع عن النفس ضد المراقبة [فهم المراقبة على الشبكة و&nbsp;تجاوزها](https://ssd.eff.org/ar/module/understanding-and-circumventing-network-censorship)"

#. type: Bullet: '- '
#: src/internet-connection/how-the-internet-works/index.md:88
msgid "[Security in a box guide on accessing blocked websites](../circumvention)."
msgstr "[دليل عدّة الأمان إلى النفاذ إلى المواقع المحجوبة](../circumvention)."

#~ msgid "that you will be given a misleading reason when a blocked site fails to load."
#~ msgstr "و أنّ الحجب بن يُصرّح به و&nbsp;ما من سبب سيُعطى له."
