# Indonesian translations for PACKAGE package
# Copyright (C) 2024 Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# Automatically generated, 2024.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-02-20 14:04+0000\n"
"PO-Revision-Date: 2024-12-12 21:56+0000\n"
"Last-Translator: Ical <ical@riseup.net>\n"
"Language-Team: Indonesian <https://weblate.securityinabox.org/projects/internet-connection/how-the-internet-works/id/>\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 4.12.2\n"

#. type: Yaml Front Matter Hash Value: title
#: src/internet-connection/how-the-internet-works/index.md:1
#, no-wrap
msgid "How the internet works and how it can be censored"
msgstr "Bagaimana internet bekerja dan bagaimana internet bisa disensor"

#. type: Title #
#: src/internet-connection/how-the-internet-works/index.md:8
#, no-wrap
msgid "How the internet works, and how some countries and other entities block or censor websites and online services"
msgstr "Bagaimana internet bekerja, dan bagaimana beberapa negara dan entitas lain memblokir atau menyensor situsweb dan layanan daring"

#. type: Plain text
#: src/internet-connection/how-the-internet-works/index.md:11
msgid "The internet is an international public resource, made up of computers, phones, servers, routers and other devices connected to each other. It was designed to continue providing service even if part of the network was destroyed."
msgstr "Internet adalah sumber daya publik internasional, terdiri dari komputer, ponsel, peladen, router, dan perangkat lain yang terhubung satu sama lain. Internet dirancang untuk terus menyediakan layanan bahkan jika sebagian dari jaringannya rusak."

#. type: Plain text
#: src/internet-connection/how-the-internet-works/index.md:15
#, no-wrap
msgid ""
"> No one person, company, organization or government runs the internet.\n"
">\n"
"> [*Wikipedia page on internet governance*](https://en.wikipedia.org/wiki/Internet_governance)\n"
msgstr ""
"> Tidak ada satu orangpun, perusahaan, organisasi, atau pemerintah yang menjalankan internet.\n"
">\n"
"> [*Halaman Wikipedia tentang tata kelola internet*](https://id.wikipedia.org/wiki/Tata_kelola_Internet)\n"

#. type: Plain text
#: src/internet-connection/how-the-internet-works/index.md:17
msgid "However, national and international bodies have some control over the infrastructure of the internet in their jurisdiction. Many countries prevent internet users within their borders from accessing certain websites or online services. Businesses, schools, libraries and other institutions may rely on similar filters to \"protect\" their employees, students and customers from material they consider harmful or distracting."
msgstr "Namun, badan nasional dan internasional memiliki kontrol atas infrastruktur internet dalam"

#. type: Plain text
#: src/internet-connection/how-the-internet-works/index.md:19
msgid "Internet filtering can be based on different technologies. Some filters block sites based on their IP addresses (identifying numbers that may be shaped like `172.105.249.143` or `2a01:7e01::f03c:92ff:fecd:7e45` and are assigned to every phone, computer, router and website to deliver internet content to those who have requested it). Others block particular domain names (the web addresses you may be more familiar with, like `google.com` or this website, `securityinabox.org`). Some filters block all addresses except an official list of allowed sites defined by the authority that have devised those filters. Other filters search through unencrypted internet traffic and cause the internet infrastructure to ignore requests that include specific keywords (for example, searches that include \"human rights violations\" or names of opposition leaders)."
msgstr "Penyaringan internet bisa dilakukan dengan teknologi yang berbeda. Beberapa penyaring memblokir situs berdasarkan alamat IP mereka (nomor yang mengidentifikasi, yang berbentuk seperti `72.105.249.143`atau `2a01:7e01::f03c:92ff:fecd:7e45` dan ditetapkan ke setiap ponsel, komputer, router dan situsweb untuk menyediakan konten internet ke orang-orang yang ingin mengaksesnya). Yang lain memblokir nama domain tertentu (alamat web yang mungkin lebih familiar untukmu, seperti `google.com` atau situsweb ni, `securityinabox.org`). Beberapa penyaring memblokir semua alamat kecuali daftar resmi yang diizinkan oleh otoritas yang merancang penyaring tersebut. Penyaring lain mencari melalui lalu lintas internet yang tidak dienkripsi dan menyebabkan infrastruktur internet untuk menolak permintaan yang berisi kata kunci spesifik (contohnya, pencarian yang berisi \"pelanggaran hak asasi manusia\" atau nama dari pemimpin oposisi)."

#. type: Plain text
#: src/internet-connection/how-the-internet-works/index.md:21
msgid "You can often bypass these filters by installing software that uses intermediary servers, located in other countries, to pass content between the blocked content or service you are trying to reach and your device. Learn more about these tools in [the guide on how to circumvent internet blockages and monitoring](../circumvention)."
msgstr "Kamu sering kali dapat melewati penyaring ini dengan memasang perangkat lunak yang menggunakan peladen perantara, yang berlokasi di negara lain, untuk melewatkan konten di antara konten atau layanan yang diblokir yang ingin kamu jangkau dan perangkatmu. Pelajari lebih lanjut tentang alat-alat ini di [panduan tentang cara menghindari pemblokiran dan pemantauan internet](../circumvention)."

#. type: Title #
#: src/internet-connection/how-the-internet-works/index.md:22
#, no-wrap
msgid "Understanding how websites and online services can be blocked"
msgstr "Memahami bagaimana situs web dan layanan daring bisa diblokir"

#. type: Plain text
#: src/internet-connection/how-the-internet-works/index.md:25
msgid "Research carried out by organisations like the [Open Observatory of Network Interference (OONI)](https://ooni.org/) and [Reporters Without Borders (RSF)](https://rsf.org/en/recherche?text=censorship) indicates that many countries filter a wide variety of social, political and \"national security\" content, but rarely publish lists of what they block. Governments that wish to control their citizens' access to the internet also block VPNs and proxies they are aware of, as well as websites that offer tools and instructions to help people get around filters."
msgstr "Penelitian yang dilakukan oleh organisasi seperti [Open Observatory of Network Interference (OONI)](https://ooni.org/) dan [Reporters Without Borders (RSF)](https://rsf.org/en/recherche?text=censorship) mengindikasikan bahwa banyak negara menyaring berbagai macam konten sosial, politik, dan \"keamanan nasional\", namun jarang mempublikasikan daftar dari apa yang mereka blokir. Pemerintah yang ingin mengontrol akses internet warganya juga memblokir VPN dan proksi yang mereka ketahui,juga situs web yang menyediakan alat-alat dan panduan untuk membantu orang-orang melewati penyaringan tersebut."

#. type: Plain text
#: src/internet-connection/how-the-internet-works/index.md:27
msgid "Article 19 of the Universal Declaration of Human Rights guarantees free access to information. Despite this, the number of countries censoring the internet continues to increase. As this practice spreads throughout the world, however, so does access to anti-blocking tools that have been created, deployed and publicised by activists, programmers and volunteers, with funding from the United States, Canada, the European Union, NGOs and other bodies concerned with free speech."
msgstr "Pasal 19 dari Deklarasi Universal Hak Asasi Manusia memastikan akses bebas atas informasi. Meskipun demikian, jumlah negara yang menyensor internet terus meningkat. Namun, seiring dengan menyebarnya praktik ini ke seluruh dunia, begitu pula akses ke perangkat anti-pemblokiran yang telah dibuat, disebarkan, dan dipublikasikan oleh para aktivis, pemrogram, dan sukarelawan, dengan pendanaan dari Amerika Serikat, Kanada, Uni Eropa, LSM, dan lembaga-lembaga lain yang peduli terhadap kebebasan berpendapat."

#. type: Title #
#: src/internet-connection/how-the-internet-works/index.md:28
#, no-wrap
msgid "Your internet connection"
msgstr "Koneksi internetmu"

#. type: Plain text
#: src/internet-connection/how-the-internet-works/index.md:31
msgid "![](../../../media/en/anonymity-and-circumvention/internet_connection.png)"
msgstr "![](../../../media/en/anonymity-and-circumvention/internet_connection.png)"

#. type: Plain text
#: src/internet-connection/how-the-internet-works/index.md:33
msgid "When you ask your computer to access a website, or your phone to use an app, your request and the content you see in response both go through a number of network devices. Your phone or computer first uses its wired, wireless or mobile data connection to reach your internet service provider (ISP). If you are home and using your own wifi, this ISP may be the company you pay for internet service. If you are using mobile data, the ISP you are connecting to is probably your mobile service provider. If you are working from an office, school, internet cafe or some other public space, it may be difficult to determine what ISP you are connecting to."
msgstr "Saat kamu memerintahkan komputermu mengakses situsweb, atau ponselmu menjalankan aplikasi, perintah dan konten yang kamu lihat melewati beberapa perangkat jaringan. Awalnya, ponsel atau komputermu menggunakan koneksi kabel, nirkabel, atau data seluler untuk menuju penyedia jasa internetmu (PJI). Jika kamu berada di rumah dan menggunakan wifi-mu sendiri, PJI ini adalah perusahaan yang kamu bayar untuk layanan internetmu. Jika kamu menggunakan data seluler, PJI yang tersambung adalah penyedia jaringan seluler. Jika kamu bekerja dari kantor, sekolah, warnet, atau ruang publik lainnya, akan sulit mengetahui PJI yang tersambung denganmu."

#. type: Plain text
#: src/internet-connection/how-the-internet-works/index.md:35
msgid "Your ISP relies on the network infrastructure in your country to connect its users with the rest of the world. The ISP assigns an IP address to the local network or device you are using to connect, whether it is your home wifi, your mobile phone or the network of the internet cafe, school or hotspot you are using. Your ISP will use this address to send content to your device. On the other end of your connection, the website or internet service you are accessing will have received its own IP address from an ISP in its own country."
msgstr "PJI-mu mengandalkan infrastruktur jaringan di negaramu untuk menghubungkan penggunanya ke seluruh dunia. PJI menetapkan sebuah alamat IP ke jaringan lokal atau perangkat yang kamu gunakan untuk tersambung, apakah wifi rumahmu, ponselmu atau jaringan warnet, sekolah, atau hotspot yang kamu gunakan. PJI yang kamu gunakan menggunakan alamat ini untuk mengirimkan konten ke perangkatmu. Di ujung lain koneksimu, situsweb atau layanan intenet yang kamu akses akan menerima alamat IP-nya sendiri dari PJI di negaranya sendiri."

#. type: Plain text
#: src/internet-connection/how-the-internet-works/index.md:37
msgid "Online services can use this address to send you the web pages you are trying to view and other data you request. (Your device has its own IP address, which is how your router sends everybody on your network their own traffic. It is only used on your local network.)"
msgstr "Layanan daring bisa menggunakan alamat ini untuk mengirimkanmu halaman web yang coba kamu lihat dan data yang kamu minta. (Perangkatmu memiliki alamat IP-nya sendiri, yang merupakan cara routermu mengirimkan lalu lintas siapapun yang berada di jaringanmu. Hal tersebut hanya digunakan pada jaringan lokal.)"

#. type: Plain text
#: src/internet-connection/how-the-internet-works/index.md:39
msgid "Anyone who knows your IP address can figure out what city or region you are in. Certain entities can determine your precise location even more precisely:"
msgstr "Siapa saja yang mengetahui alamat IP-mu bisa mencari tahu di kota atau daerah mana posisimu. Beberapa entitas bisa mengetahui lokasimu dengan lebih tepat:"

#. type: Bullet: '- '
#: src/internet-connection/how-the-internet-works/index.md:44
msgid "**Your mobile provider** knows the precise physical location of your phone while your device is on, by triangulating your location among its cell towers."
msgstr "**Penyedia layanan selulermu** mengetahui lokasi fisik yang tepat dari ponselnya saat perangkatmu dalam keadaan nyala, dengan melakukan triangulasi lokasimu di antara menara seluler mereka."

#. type: Bullet: '- '
#: src/internet-connection/how-the-internet-works/index.md:44
msgid "**Your ISP** will likely know what building you are in."
msgstr "**PJI mu** kemungkinan besar akan mengetahui di gedung mana kamu berada."

#. type: Bullet: '- '
#: src/internet-connection/how-the-internet-works/index.md:44
msgid "**An internet cafe, library or business** where you are accessing the internet will know which of their computers you were using at any given time. And if you are using your own device, they will know which local IP address was assigned to your device on their local network, thus associating all your activities to your device."
msgstr "**Sebuah warnet, perpustakaan, atau bisnis** di mana kamu mengakses internet akan mengetahui komputer mana yang kamu gunakan pada waktu tertentu. Dan jika kamu menggunakan perangkatmu sendiri, mereka akan mengetahui alamat IP lokal yang ditetapkan ke perangkatmu dalam jaringan lokal mereka, sehingga bisa mengaitkan semua aktivitas yang kamu lakukan ke perangkatmu."

#. type: Bullet: '- '
#: src/internet-connection/how-the-internet-works/index.md:44
msgid "**Government agencies** may know all of the above. And even if they do not, they can often use their influence to find out."
msgstr "**Instansi Pemerintahan** bisa mengetahui hal di atas. Bahkan jika mereka tidak, mereka seringkali bisa menggunakan pengaruhnya untuk mencari tahu."

#. type: Plain text
#: src/internet-connection/how-the-internet-works/index.md:46
msgid "Internet communication is somewhat more complex than the description above, but even this simplified model can help you figure out what risks you face when connecting to the internet and what institution or entity could be blocking a website you're trying to connect to."
msgstr "Komunikasi internet bersifat agak lebih rumit dari deskripsi di atas, namun bahkan model yang disederhanakan ini bisa membantumu mencari tahu risiko apa yang kamu hadapi saat tersambung ke internet dan institusi atau entitas apa yang bisa memblokir situsweb yang kamu coba kunjungi."

#. type: Title #
#: src/internet-connection/how-the-internet-works/index.md:47
#, no-wrap
msgid "How websites are blocked"
msgstr "Bagaimana situsweb bisa diblokir"

#. type: Plain text
#: src/internet-connection/how-the-internet-works/index.md:50
msgid "When you view a web page, your device connects to a domain name service (DNS) to look up the IP address associated with the website's domain name — for example, it could ask what's the IP address of `securityinabox.org` and receive the answer `172.105.249.143`. It then asks your ISP to send a request to the ISP in charge of `172.105.249.143` to ask the web server at `172.105.249.143` for the contents of securityinabox.org."
msgstr "Saat kamu melihat halaman web, perangkatmu tersambung ke layanan nama domain/domain name service (DNS) untuk melihat alamat IP yang terkait dengan nama domain sebuah situsweb — contohnya, bisa meminta alamat IP dari `securityinabox.org` dan mendapatkan hasil `172.105.249.143` kemudian meminta PJI yang kamu gunakan untuk mengirimkan permintaan ke PJI yang bertanggungjawab atas `172.105.249.143` untuk meminta konten securityinabox.org dari peladen web `172.105.249.143`."

#. type: Plain text
#: src/internet-connection/how-the-internet-works/index.md:52
msgid "If you are in a country that censors securityinabox.org, your request will not succeed at some point of this process: when your device tries to look up the IP address, when it requests the content, or as the content is being sent to your device. In some countries, ISPs are required to consult a national blacklist of sites they must not show you. These blocklists can contain domain names, IP addresses, keywords or a mix of all of these. Keyword filters scan both unencrypted requests and the results a website or service returns to you."
msgstr "Jika kamu berada di negara yang menyensor securityinabox.org, permintaanmu tidak akan berhasil pada suatu saat dalam proses ini: saat perangkatmu mencoba mencari tahu alamat IP, saat meminta kontennya, atau saat konten tersebut dikirimkan ke perangkatmu. Di beberapa negara, PJI diwajibkan untuk membaca daftar hitam nasional situs-situs yang tidak boleh mereka tampilkan kepadamu. Daftar hitam ini bisa berisi nama domain, alamat IP, kata kunci atau campuran dari semuanya. Penyaringan kata kunci memindai permintaan tanpa enkripsi dan hasil yang diberikan oleh situsweb atau layanan daring."

#. type: Plain text
#: src/internet-connection/how-the-internet-works/index.md:54
msgid "You might not always know when you have requested a blocked webpage, content or service. Sometimes you may receive a response that explains why a particular content or service has been censored. But most of the times you will see a misleading error message, saying for example that the page or service could not be found or the address may be misspelled."
msgstr "Kamu mungkin tidak selalu mengetahui kapan kamu mengunjungi situsweb, konten, atau layanan yang diblokir. Kadang-kadang kamu mungkin menerima respon yang menjelaskan kenapa konten atau layanan tertentu disensor. Namun, seringnya kamu akan melihat pesan galat yang menyesatkan, mengatakan halaman atau layanan tidak bisa ditemukan atau alamatnya salah ketik, misalnya."

#. type: Plain text
#: src/internet-connection/how-the-internet-works/index.md:56
msgid "Each blocking technique has strengths and weaknesses. When attempting to get around internet blocks, it is easier to assume the worst than to figure out what techniques are being used in your country. You might as well assume:"
msgstr "Setiap teknik pemblokiran memiliki kekuatan dan kekurangan. Saat mencoba menghindari pemblokiran internet, lebih mudah untuk berasumsi yang terburuk dari pada mencari tahu teknik apa yang digunakan di negaramu. Kamu juga bisa berasumsi:"

#. type: Bullet: '- '
#: src/internet-connection/how-the-internet-works/index.md:62
msgid "that blocking is implemented nationally, at the ISP level _and_ on your local network,"
msgstr "pemblokiran tersebut diimplementasikan secara nasional, pada tingkatan PJI _dan_ pada jaringan lokalmu,"

#. type: Bullet: '- '
#: src/internet-connection/how-the-internet-works/index.md:62
msgid "that DNS lookups _and_ content requests are blocked,"
msgstr "bahwa pencarian DNS _dan_ permintaan konten diblokir,"

#. type: Bullet: '- '
#: src/internet-connection/how-the-internet-works/index.md:62
msgid "that blocklists are maintained for both domain names and IP addresses,"
msgstr "bahwa daftar blokir dipertahankan untuk nama domain dan alamat IP,"

#. type: Bullet: '- '
#: src/internet-connection/how-the-internet-works/index.md:62
msgid "that your unencrypted internet traffic is monitored for keywords, and"
msgstr "bahwa lalu-lintasmu yang tidak terenkripsi dipantau untuk mencari kata kunci, dan"

#. type: Bullet: '- '
#: src/internet-connection/how-the-internet-works/index.md:62
msgid "that no indication or reason will be given for the blocking."
msgstr ""

#. type: Plain text
#: src/internet-connection/how-the-internet-works/index.md:64
msgid "The safest and most effective anti-blocking tools should work regardless of the type of block you are facing."
msgstr "Alat yang paling aman dan efektif seharusnya berfungsi tidak memedulikan tipe pemblokiran yang kamu hadapi."

#. type: Title #
#: src/internet-connection/how-the-internet-works/index.md:65
#, no-wrap
msgid "How tools get around blocks"
msgstr "Bagaimana alat-alat menghindari pemblokiran"

#. type: Plain text
#: src/internet-connection/how-the-internet-works/index.md:68
msgid "![](../../../media/en/anonymity-and-circumvention/proxies.png)"
msgstr "![](../../../media/en/anonymity-and-circumvention/proxies.png)"

#. type: Plain text
#: src/internet-connection/how-the-internet-works/index.md:70
msgid "Circumvention tools like Tor and VPNs apply encryption to your traffic, thus protecting the secrecy of the requested data, including the IP address of the requested service. They hide the address until your request arrives at a proxy server in another country. The proxy then decrypts that address, sends your request to see the content, accepts the response from the page or service, encrypts it again and sends it back to your device. We sometimes use \"tunnels\" as a metaphor for this: your traffic is still passing through infrastructure controlled by the ISP, government or other institutions that wants to block your access, but their filters are unable to read the content of your request or determine exactly where you're trying to go when you leave the tunnel. All they know is that you are interacting with a proxy and that encryption is being used to prevent them from seeing the information you're requesting."
msgstr "Peralatan pengalih seperti Tor dan VPN menerapkan enkripsi ke lalu lintas internetmu, sehingga melindungi kerahasiaan dari data yang diminta, termasuk alamat IP dari layanan yang diminta. Peralatan tersebut menyembunyikan alamat tersebut sampai permintaanmu sampai ke sebuah peladen proksi di negara lain. Proksi itu kemudian mendekripsi alamatnya, mengirimkan permintaanmu untuk melihat kontennya, menerima respon dari halaman atau layanan tersebut, mengenkripsinya lagi dan mengirimkannya kembali ke perangkatmu. Kami kadang menggunakan \"lorong\" sebagai metafora dari proses tersebut: lalu lintasmu tetap melewati infrastruktur yang dikendalikan oleh PJI, negara, atau institusi lain yang ingin memblokir aksesmu, namun saringan mereka tidak bisa membaca konten/isi dari permintaanmu atau memastikan secara tepat apa yang ingin kamu tuju saat meninggalkan lorong tersebut. Yang mereka ketahui hanya kamu sedang berinteraksi dengan proksi dan enkripsi tersebut digunakan untuk mencegah meeka melihat informasi yang kamu minta."

#. type: Bullet: '- '
#: src/internet-connection/how-the-internet-works/index.md:72
msgid "[Learn how to choose the censorship circumvention tool that is right for you](../circumvention)."
msgstr "[Pelajari bagaimana memilih alat penghindaran sensor yang tepat untukmu](../circumvention)."

#. type: Title ##
#: src/internet-connection/how-the-internet-works/index.md:73
#, no-wrap
msgid "Blocking resistance"
msgstr "Pertahanan terhadap pemblokiran"

#. type: Plain text
#: src/internet-connection/how-the-internet-works/index.md:76
msgid "Of course, the government agency in charge of internet filters — or the company that provides your government with blocking software — might eventually identify that unknown computer as a proxy, and add it to their blocklist. This is why VPNs and other tools sometimes stop working."
msgstr "Tentu saja, instansi pemerintah yang bertanggung jawab penyaring internet — atau perusahaan yang menyediakan perangkat lunak pemblokir untuk pemerintahmu — pada akhirnya akan mengidentifikasi komputer yang tidak dikenal itu sebagai proksi, dan menambahkannya ke daftar blokir mereka. Inilah sebabnya mengapa VPN dan alat lainnya terkadang berhenti bekerja."

#. type: Plain text
#: src/internet-connection/how-the-internet-works/index.md:78
msgid "However, it usually takes time for those blocking the internet to discover proxies. Tools for visiting blocked websites use one or more of the following techniques:"
msgstr "Bagaimanapun, biasanya memakan waktu bagi mereka yang memblokir internet untuk menemukan proksi. Peralatan untuk mengunjungi situswebyang diblokir menggunakan satu atau lebih dari teknik berikut ini:"

#. type: Bullet: '- '
#: src/internet-connection/how-the-internet-works/index.md:84
msgid "**Hidden proxies** can be distributed to users in a way that prevents censors from finding them all at once."
msgstr "**Proksi tersembunyi** bisa didistribusikan ke pengguna dengan cara mencegah sensor menemukannya saat bersamaan."

#. type: Bullet: '- '
#: src/internet-connection/how-the-internet-works/index.md:84
msgid "**Private proxies** limit the number of people who know about and can access them, making it harder for authorities to find and block them. You can create a private proxy using for example [Outline](../tools/#outline-vpn) or [Algo](../tools/#algo-vpn)."
msgstr ""

#. type: Bullet: '- '
#: src/internet-connection/how-the-internet-works/index.md:84
msgid "**Disposable proxies** can be replaced more quickly than they can be blocked."
msgstr "**Proksi sekali pakai** bisa diganti dengan waktu lebih cepat daripada waktu pemblokiran."

#. type: Bullet: '- '
#: src/internet-connection/how-the-internet-works/index.md:84
msgid "**Obfuscation** disguises proxy traffic as normal internet traffic thus keeping censors from identifying unknown proxies."
msgstr "**Pengaburan** menyamarkan lalu lintas proksi menjadi lalu lintas internet biasa, dengan demikian menjaga sensor mengidentifikasi proksi yang tidak dikenal."

#. type: Bullet: '- '
#: src/internet-connection/how-the-internet-works/index.md:84
msgid "**Domain fronting** makes it harder to block a web address as this blockage would also block access to other useful, popular services (such as Google)."
msgstr "**Domain fronting**menyulitkan untuk memblokir sebuah alamat web karena pemblokiran juga akan memblokir akses ke layanan lain yang berguna dan populer (seperti Google)."

#. type: Title #
#: src/internet-connection/how-the-internet-works/index.md:85
#, no-wrap
msgid "Further Reading"
msgstr "Bacaan Lebih Lanjut"

#. type: Bullet: '- '
#: src/internet-connection/how-the-internet-works/index.md:88
msgid "Surveillance Self-Defense, [How to: Understand and Circumvent Network Censorship](https://ssd.eff.org/module/understanding-and-circumventing-network-censorship)"
msgstr "Surveillance Self-Defense, [Bagaimana cara: Mengerti dan Menghindari Sensor Jaringan](https://ssd.eff.org/module/understanding-and-circumventing-network-censorship)"

#. type: Bullet: '- '
#: src/internet-connection/how-the-internet-works/index.md:88
msgid "[Security in a box guide on accessing blocked websites](../circumvention)."
msgstr "[Panduan security in a box tentang mengakses situsweb yang diblokir](../circumvention)."

#~ msgid "that you will be given a misleading reason when a blocked site fails to load."
#~ msgstr "bahwa kamu akan diberikan alasan yang menyesatkan saat situs yang diblokir gagal dimuat."
