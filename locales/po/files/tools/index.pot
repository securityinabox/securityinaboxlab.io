# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-03-10 22:01+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Yaml Front Matter Hash Value: subtitle
#: src/files/tools/index.md:1
#, no-wrap
msgid "Files"
msgstr ""

#. type: Yaml Front Matter Hash Value: title
#: src/files/tools/index.md:1
#, no-wrap
msgid "Related Tools"
msgstr ""

#. type: Plain text
#: src/files/tools/index.md:10
#, markdown-text
msgid "Use the tools listed in this section to protect, destroy, or recover important information. In this section, you can also learn more about how to [protect your sensitive information](../secure-file-storage), [destroy sensitive information](../destroy-sensitive-information), [destroy identifying information](../destroy-identifying-information) or [recover from information loss](../backup)."
msgstr ""

#. type: Title #
#: src/files/tools/index.md:11
#, markdown-text, no-wrap
msgid "Security in a Box Tool Guides"
msgstr ""

#. type: Plain text
#: src/files/tools/index.md:15
#, markdown-text
msgid "[![](../../../media/en/logos/veracrypt-logo.png)](#veracrypt)  [Veracrypt](../../tools/veracrypt)"
msgstr ""

#. type: Title #
#: src/files/tools/index.md:16
#, markdown-text, no-wrap
msgid "Protect and encrypt your files"
msgstr ""

#. type: Title ##
#: src/files/tools/index.md:18
#, markdown-text, no-wrap
msgid "[Veracrypt](https://www.veracrypt.fr)"
msgstr ""

#. type: Plain text
#: src/files/tools/index.md:22
#, markdown-text
msgid "[![](../../../media/en/logos/veracrypt-logo.png)](https://www.veracrypt.fr)  (Linux, macOS, Windows)"
msgstr ""

#. type: Plain text
#: src/files/tools/index.md:24
#, markdown-text
msgid "Free and open-source application that can encrypt some or all of the files on your computer."
msgstr ""

#. type: Plain text
#: src/files/tools/index.md:26
#, markdown-text
msgid "[Download](https://www.veracrypt.fr/en/Downloads.html) | See [our guide](../../tools/veracrypt) and [their documentation](https://www.veracrypt.fr/en/Documentation.html)"
msgstr ""

#. type: Title ##
#: src/files/tools/index.md:27
#, markdown-text, no-wrap
msgid "[Cryptomator](https://cryptomator.org)"
msgstr ""

#. type: Plain text
#: src/files/tools/index.md:31
#, markdown-text
msgid "[![](../../../media/en/logos/cryptomator-logo.png)](https://cryptomator.org)  (Android, iOS, Linux, macOS, Windows)"
msgstr ""

#. type: Plain text
#: src/files/tools/index.md:33
#, markdown-text
msgid "Free and open-source application that uses independent encryption to protect files you upload to online storage services like Google Drive, OneDrive and Dropbox."
msgstr ""

#. type: Plain text
#: src/files/tools/index.md:35
#, markdown-text
msgid "[Download](https://cryptomator.org/downloads/) | See [their documentation](https://docs.cryptomator.org/en/latest/)"
msgstr ""

#. type: Title ##
#: src/files/tools/index.md:36
#, markdown-text, no-wrap
msgid "[Tella](https://tella-app.org)"
msgstr ""

#. type: Plain text
#: src/files/tools/index.md:40
#, markdown-text
msgid "[![](../../../media/en/logos/tella-logo.png)](https://tella-app.org)  (Android and iOS)"
msgstr ""

#. type: Plain text
#: src/files/tools/index.md:42
#, markdown-text
msgid "A free and open-source app designed for documenting human rights violations that securely hides photos, videos, audio recordings and files on your device."
msgstr ""

#. type: Plain text
#: src/files/tools/index.md:44
#, markdown-text
msgid "Download for [Android](https://play.google.com/store/apps/details?id=org.hzontal.tella) and [iOS](https://apps.apple.com/us/app/tella-document-protect/id1598152580) | See [their documentation](https://docs.tella-app.org/)"
msgstr ""

#. type: Title #
#: src/files/tools/index.md:45
#, markdown-text, no-wrap
msgid "Photos and videos"
msgstr ""

#. type: Title ##
#: src/files/tools/index.md:47
#, markdown-text, no-wrap
msgid "[ObscuraCam](https://guardianproject.info/apps/obscuracam/)"
msgstr ""

#. type: Plain text
#: src/files/tools/index.md:51
#, markdown-text
msgid "[![](../../../media/en/logos/obscuracam-logo.png)](https://guardianproject.info/apps/obscuracam/)  (Android)"
msgstr ""

#. type: Plain text
#: src/files/tools/index.md:53
#, markdown-text
msgid "A free and open-source camera app that removes identifying information from photographs and from the metadata associated with them."
msgstr ""

#. type: Plain text
#: src/files/tools/index.md:55
#, markdown-text
msgid "Download from [Google Play](https://play.google.com/store/apps/details?id=org.witness.sscphase1) or [F-Droid](https://f-droid.org/en/packages/org.witness.sscphase1/)"
msgstr ""

#. type: Title ##
#: src/files/tools/index.md:56
#, markdown-text, no-wrap
msgid "[ProofMode](https://proofmode.org/)"
msgstr ""

#. type: Plain text
#: src/files/tools/index.md:60
#, markdown-text
msgid "[![](../../../media/en/logos/proofmode-logo.png)](https://proofmode.org/)  (Android, iOS and Apple Silicon)"
msgstr ""

#. type: Plain text
#: src/files/tools/index.md:62
#, markdown-text
msgid "A free and open-source camera app designed for documenting human rights violations, Proof Mode gives you the option to gather additional information, sign it cryptographically, and store it securely on your device when you take photographs or record video."
msgstr ""

#. type: Plain text
#: src/files/tools/index.md:64
#, markdown-text
msgid "Download for [Android](https://play.google.com/store/apps/details?id=org.witness.proofmode), [iOS and Apple Silicon (Mac M1, M2)](https://apps.apple.com/us/app/proofmode/id1526270484)"
msgstr ""

#. type: Title ##
#: src/files/tools/index.md:65
#, markdown-text, no-wrap
msgid "[PutMask](https://putmask.com/)"
msgstr ""

#. type: Plain text
#: src/files/tools/index.md:68
#, markdown-text
msgid "[![](../../../media/en/logos/putmask.png)](https://putmask.com/) (Android)"
msgstr ""

#. type: Plain text
#: src/files/tools/index.md:70
#, markdown-text
msgid "A privacy-friendly Android app to blur faces and other sensitive details in videos."
msgstr ""

#. type: Plain text
#: src/files/tools/index.md:72
#, markdown-text
msgid "Download from [Google Play](https://play.google.com/store/apps/details?id=com.putmask.facefilter) | See [their tutorials](https://putmask.com/tutorials)"
msgstr ""

#. type: Title #
#: src/files/tools/index.md:74
#, markdown-text, no-wrap
msgid "Remove metadata from pictures, videos and other files"
msgstr ""

#. type: Title ##
#: src/files/tools/index.md:76
#, markdown-text, no-wrap
msgid "[ExifCleaner](https://exifcleaner.com)"
msgstr ""

#. type: Plain text
#: src/files/tools/index.md:80
#, markdown-text
msgid "[![](../../../media/en/logos/exifcleaner-logo.png)](https://exifcleaner.com)  (Linux, macOS, Windows)"
msgstr ""

#. type: Plain text
#: src/files/tools/index.md:82
#, markdown-text
msgid "A free and open-source application that removes identifying information from the metadata in photos, videos and PDF files."
msgstr ""

#. type: Plain text
#: src/files/tools/index.md:84
#, markdown-text
msgid "[Download](https://github.com/szTheory/exifcleaner/releases)"
msgstr ""

#. type: Title ##
#: src/files/tools/index.md:85
#, markdown-text, no-wrap
msgid "[Scrambled EXIF](https://gitlab.com/juanitobananas/scrambled-exif)"
msgstr ""

#. type: Plain text
#: src/files/tools/index.md:89
#, markdown-text
msgid "[![](../../../media/en/logos/scrambledexif-logo.png)](https://gitlab.com/juanitobananas/scrambled-exif)  (Android)"
msgstr ""

#. type: Plain text
#: src/files/tools/index.md:91 src/files/tools/index.md:100
#, markdown-text
msgid "A free and open-source app that removes identifying information from the metadata in photos."
msgstr ""

#. type: Plain text
#: src/files/tools/index.md:93
#, markdown-text
msgid "Download from [Google Play](https://play.google.com/store/apps/details?id=com.jarsilio.android.scrambledeggsif) or [F-Droid](https://f-droid.org/en/packages/com.jarsilio.android.scrambledeggsif/)"
msgstr ""

#. type: Title ##
#: src/files/tools/index.md:94
#, markdown-text, no-wrap
msgid "[MetaX](https://github.com/Ckitakishi/MetaX)"
msgstr ""

#. type: Plain text
#: src/files/tools/index.md:98
#, markdown-text
msgid "[![](../../../media/en/logos/metax-logo.png)](https://github.com/Ckitakishi/MetaX)  (iOS, macOS)"
msgstr ""

#. type: Plain text
#: src/files/tools/index.md:102
#, markdown-text
msgid "Download from [the App Store](https://apps.apple.com/us/app/metax/id1376589355)"
msgstr ""

#. type: Title #
#: src/files/tools/index.md:103
#, markdown-text, no-wrap
msgid "Destroy sensitive files"
msgstr ""

#. type: Title ##
#: src/files/tools/index.md:105
#, markdown-text, no-wrap
msgid "[BleachBit](https://www.bleachbit.org/)"
msgstr ""

#. type: Plain text
#: src/files/tools/index.md:109
#, markdown-text
msgid "[![](../../../media/en/logos/bleachbit-logo.png)](https://www.bleachbit.org/)  (Linux, macOS, Windows)"
msgstr ""

#. type: Plain text
#: src/files/tools/index.md:111
#, markdown-text
msgid "A free and open-source application that removes traces of your work from various programs (see [cleaners repository](https://github.com/bleachbit/cleanerml)), clears browser trackers, and securely destroys files you have deleted."
msgstr ""

#. type: Plain text
#: src/files/tools/index.md:113
#, markdown-text
msgid "[Download](https://www.bleachbit.org/download) | See [their guide](https://docs.bleachbit.org/)"
msgstr ""

#. type: Title ##
#: src/files/tools/index.md:114
#, markdown-text, no-wrap
msgid "[CCleaner](https://www.ccleaner.com)"
msgstr ""

#. type: Plain text
#: src/files/tools/index.md:118
#, markdown-text
msgid "[![](../../../media/en/logos/ccleaner-logo.png)](https://www.ccleaner.com)  (Windows, macOS, Android, iOS)"
msgstr ""

#. type: Plain text
#: src/files/tools/index.md:120
#, markdown-text
msgid "Removes traces of your work from various programs, clears browser trackers, and securely destroys files you have deleted."
msgstr ""

#. type: Plain text
#: src/files/tools/index.md:122
#, markdown-text
msgid "Download for [Windows](https://www.ccleaner.com/ccleaner/download/standard), [macOS](https://www.ccleaner.com/ccleaner-mac/download/standard), [iOS from the App Store](https://play.google.com/store/apps/details?id=com.piriform.ccleaner) or [Android from Google Play](https://play.google.com/store/apps/details?id=com.piriform.ccleaner) | See [their documentation](https://support.piriform.com)"
msgstr ""

#. type: Title ##
#: src/files/tools/index.md:123
#, markdown-text, no-wrap
msgid "[Eraser](https://eraser.heidi.ie/)"
msgstr ""

#. type: Plain text
#: src/files/tools/index.md:127
#, markdown-text
msgid "[![](../../../media/en/logos/eraser-logo.png)](https://eraser.heidi.ie/)  (Windows)"
msgstr ""

#. type: Plain text
#: src/files/tools/index.md:129
#, markdown-text
msgid "Eraser is a free and open-source secure data removal tool for Windows. It completely removes sensitive data from your hard drive by overwriting it several times with carefully selected patterns."
msgstr ""

#. type: Plain text
#: src/files/tools/index.md:131
#, markdown-text
msgid "[Download](https://eraser.heidi.ie/download/) | See [their video guides](https://eraser.heidi.ie/eraser-video/)"
msgstr ""

#. type: Title #
#: src/files/tools/index.md:132
#, markdown-text, no-wrap
msgid "Recover your files"
msgstr ""

#. type: Title ##
#: src/files/tools/index.md:134
#, markdown-text, no-wrap
msgid "[Photorec](https://www.cgsecurity.org/wiki/PhotoRec)"
msgstr ""

#. type: Plain text
#: src/files/tools/index.md:138
#, markdown-text
msgid "[![](../../../media/en/logos/photorec-logo.png)](https://www.cgsecurity.org/wiki/PhotoRec)  (Linux, macOS, Windows)"
msgstr ""

#. type: Plain text
#: src/files/tools/index.md:140
#, markdown-text
msgid "Recover lost or deleted files on your computer or external storage devices."
msgstr ""

#. type: Plain text
#: src/files/tools/index.md:142
#, markdown-text
msgid "[Download](https://www.cgsecurity.org/wiki/TestDisk_Download) | See [their guide](https://www.cgsecurity.org/wiki/PhotoRec_Step_By_Step)"
msgstr ""

#. type: Title ##
#: src/files/tools/index.md:143
#, markdown-text, no-wrap
msgid "[Recuva](https://www.ccleaner.com/recuva)"
msgstr ""

#. type: Plain text
#: src/files/tools/index.md:147
#, markdown-text
msgid "[![](../../../media/en/logos/recuva-logo.png)](https://www.ccleaner.com/recuva)  (Windows)"
msgstr ""

#. type: Plain text
#: src/files/tools/index.md:149
#, markdown-text
msgid "Recover lost or deleted files on your Windows PC or SD card."
msgstr ""

#. type: Plain text
#: src/files/tools/index.md:150
#, markdown-text
msgid "[Download](https://www.ccleaner.com/recuva/download/standard) | See [their documentation](https://support.ccleaner.com/s/articleresults?language=en_US&searched=recuva)"
msgstr ""
