# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-12-12 21:02+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Yaml Front Matter Hash Value: title
#: src/files/backup/index.md:1
#, no-wrap
msgid "Back up and recover from information loss"
msgstr ""

#. type: Plain text
#: src/files/backup/index.md:9
#, markdown-text
msgid "There is a common saying among digital protection professionals: \"the question is not _if_ you will lose your data, but _when_ you will lose it.\" A backup plan can minimize what you lose, so you can still access important information if any of your devices are stolen, seized, or damaged. Take or consider the following steps to plan ahead."
msgstr ""

#. type: Title #
#: src/files/backup/index.md:10
#, markdown-text, no-wrap
msgid "Recover deleted or lost information"
msgstr ""

#. type: Plain text
#: src/files/backup/index.md:13
#, markdown-text
msgid "To reflect on what to do if you have lost your data, you can start troubleshooting with the help of the Digital First Aid Kit's guide [I lost my data](https://digitalfirstaid.org/topics/lost-data/)."
msgstr ""

#. type: Plain text
#: src/files/backup/index.md:15
#, markdown-text
msgid "You can use the tools listed in this section to recover deleted or lost data that was stored in a computer or external device."
msgstr ""

#. type: Plain text
#: src/files/backup/index.md:17
#, markdown-text
msgid "Be aware that these tools do not work if your device has written new data over your deleted information. If possible, stop using your device until you have tried to recover the files (or had someone do that for you). Doing additional work on your device could write over the files you lost and make them impossible to retrieve. The longer you use your computer before attempting to restore the file, the less likely it is that the file will be retrievable."
msgstr ""

#. type: Plain text
#: src/files/backup/index.md:19
#, markdown-text, no-wrap
msgid "**Linux, macOS, and Windows**\n"
msgstr ""

#. type: Bullet: '- '
#: src/files/backup/index.md:21
#, markdown-text
msgid "Try [TestDisk & PhotoRec](https://www.cgsecurity.org/wiki/PhotoRec). See [the official guide](https://www.cgsecurity.org/testdisk.pdf) for instructions on how to use it."
msgstr ""

#. type: Plain text
#: src/files/backup/index.md:23 src/files/backup/index.md:119
#: src/files/backup/index.md:195
#, markdown-text, no-wrap
msgid "**Windows**\n"
msgstr ""

#. type: Bullet: '- '
#: src/files/backup/index.md:25
#, markdown-text
msgid "Try [Recuva](https://www.ccleaner.com/recuva/). See [their guide](https://www.ccleaner.com/knowledge/recuva-tip-search-for-files)."
msgstr ""

#. type: Plain text
#: src/files/backup/index.md:28 src/files/backup/index.md:145
#: src/files/backup/index.md:157
#, markdown-text, no-wrap
msgid "<details><summary>Learn why we recommend this</summary>\n"
msgstr ""

#. type: Plain text
#: src/files/backup/index.md:30
#, markdown-text
msgid "When you delete a file, it disappears from view, but it remains on your device. Even after you empty the Recycle Bin or Trash, files you deleted can usually be found on the hard drive. See [How to destroy sensitive information](../destroy-sensitive-information) to learn how this can put your security at risk."
msgstr ""

#. type: Plain text
#: src/files/backup/index.md:33
#, markdown-text, no-wrap
msgid ""
"But if you accidentally delete an important file, this might work to your advantage. Some programs, like the ones listed in this section, can restore access to recently deleted files.\n"
"</details>\n"
msgstr ""

#. type: Title #
#: src/files/backup/index.md:34
#, markdown-text, no-wrap
msgid "Create a backup plan"
msgstr ""

#. type: Plain text
#: src/files/backup/index.md:37
#, markdown-text
msgid "To create a backup plan, take the following steps:"
msgstr ""

#. type: Title ##
#: src/files/backup/index.md:38
#, markdown-text, no-wrap
msgid "Organize your information"
msgstr ""

#. type: Plain text
#: src/files/backup/index.md:41
#, markdown-text
msgid "Before you make your backup plan, try to move all of the folders containing the files you intend to back up into a single location, such as inside the \"Documents\" or \"My Documents\" folder."
msgstr ""

#. type: Title ##
#: src/files/backup/index.md:42
#, markdown-text, no-wrap
msgid "Identify where and what your information is"
msgstr ""

#. type: Plain text
#: src/files/backup/index.md:45
#, markdown-text
msgid "The first step to making a backup plan is to picture where your personal and work information is currently located. Your email, for example, may be stored on your email provider's server, on your own computer, or in both places at once. And, of course, you might have several email accounts."
msgstr ""

#. type: Plain text
#: src/files/backup/index.md:47
#, markdown-text
msgid "Then there are important documents on the computers you use in the office or at home: word processing documents, presentations, PDFs and spreadsheets, among other examples. Your computer and mobile devices also store your contact lists, chat histories and personal program settings, all of which can be considered sensitive."
msgstr ""

#. type: Plain text
#: src/files/backup/index.md:49
#, markdown-text
msgid "You may also have stored some information on removable media like USB memory sticks, portable hard drives and other storage media.  If you have a website, it may contain a large collection of articles built up over years of work. Finally, don't forget your non-digital information, such as paper notebooks, diaries and letters."
msgstr ""

#. type: Plain text
#: src/files/backup/index.md:51
#, markdown-text
msgid "You can learn more on how to map your information in the [Holistic security manual section on understanding and cataloguing information](https://holistic-security.tacticaltech.org/chapters/explore/2-4-understanding-and-cataloguing-our-information.html)."
msgstr ""

#. type: Title ##
#: src/files/backup/index.md:52
#, markdown-text, no-wrap
msgid "Define which are primary copies and which are duplicates"
msgstr ""

#. type: Plain text
#: src/files/backup/index.md:55
#, markdown-text
msgid "When you are backing up files, it is sometimes suggested you use the 3-2-1 rule: have at least 3 copies of any piece of information, in at least 2 places, with at least 1 copy in a different place from the original."
msgstr ""

#. type: Plain text
#: src/files/backup/index.md:57
#, markdown-text
msgid "Before you start making backups, decide which of the files you have mapped are \"primary copies\" and which are duplicates. The primary copy should be the most up-to-date version of a particular file or collection of files; it should be the copy you would actually edit if you needed to update the content. Obviously, this does not apply to files of which you have only one copy, but it is extremely important for certain types of information."
msgstr ""

#. type: Plain text
#: src/files/backup/index.md:59
#, markdown-text
msgid "One common disaster scenario occurs when only duplicates of an important document are backed up and the primary copy itself gets lost or destroyed before those duplicates can be updated. For example, say you have been travelling for a week, updating a copy of an important spreadsheet stored on a USB memory stick. You should begin thinking of that copy as your primary copy, because it is more up-to-date than backup copies you may have made at the office."
msgstr ""

#. type: Plain text
#: src/files/backup/index.md:61
#, markdown-text
msgid "Write down the physical location of all primary and duplicate copies of the information you have identified. This will help you clarify your needs and begin to define an appropriate backup policy. The following table is a very basic example. Your list may be much longer, and contain more than one data type stored on multiple storage devices."
msgstr ""

#. type: Plain text
#: src/files/backup/index.md:72
#, markdown-text, no-wrap
msgid ""
"| Data type               | Primary / Duplicate | Storage device      | Location |\n"
"|-------------------------|---------------------|---------------------|----------|\n"
"| Research files          | Primary             | Computer hard drive | Office   |\n"
"| Human rights violations testimonies | Duplicate | USB memory stick  | With me  |\n"
"| Program databases (photos, address book, calendar, etc.) | Primary  | Computer hard drive | Office   |\n"
"| Shared documents        | Duplicate           | Office server       | Office   |\n"
"| Videos and pictures     | Duplicate           | External hard disk          | Home     |\n"
"| Email & email contacts  | Primary             | Email account   | Email server |\n"
"| Text messages & phone contacts | Primary      | Mobile phone        | With me  |\n"
"| Printed documents (contracts, invoices, etc.) | Primary | Desk drawer | Office |\n"
msgstr ""

#. type: Plain text
#: src/files/backup/index.md:74
#, markdown-text
msgid "In the table above, you can see that:"
msgstr ""

#. type: Bullet: '- '
#: src/files/backup/index.md:79
#, markdown-text
msgid "The only documents that will survive if your office computer's hard drive crashes are duplicates on your USB memory stick and other external storage media as well as shared documents on the server."
msgstr ""

#. type: Bullet: '- '
#: src/files/backup/index.md:79
#, markdown-text
msgid "You have no offline copy of your email messages or your address book, so if you forget your email password (or if someone manages to change it maliciously), you will lose access to them."
msgstr ""

#. type: Bullet: '- '
#: src/files/backup/index.md:79
#, markdown-text
msgid "You have no copies of any data from your mobile phone."
msgstr ""

#. type: Bullet: '- '
#: src/files/backup/index.md:79
#, markdown-text
msgid "You have no duplicate copies, digital or physical, of printed documents such as contracts and invoices."
msgstr ""

#. type: Plain text
#: src/files/backup/index.md:81
#, markdown-text
msgid "After following the checklist in this section, you should have rearranged your storage devices, data types and backups in a way that makes your information more resistant to disaster. For example:"
msgstr ""

#. type: Plain text
#: src/files/backup/index.md:98
#, markdown-text, no-wrap
msgid ""
"| Data Type       | Primary/Duplicate | Storage Device                 | Location |\n"
"|-----------------|-------------------|--------------------------------|----------|\n"
"| Human rights violations testimonies  | Primary | Computer hard drive | Office   |\n"
"| Human rights violations testimonies  | Duplicate | USB memory stick  | Home     |\n"
"| Research files  | Primary           | Computer hard drive            | Office   |\n"
"| Research files  | Duplicate         | USB memory stick               | With me  |\n"
"| Program databases | Primary         | Computer hard drive            | Office   |\n"
"| Program databases | Duplicate       | External drive                 | Home     |\n"
"| Email & email contacts | Primary    | Email account              | Email server |\n"
"| Email & email contacts | Duplicate  | Thunderbird on office computer | Office   |\n"
"| Text messages & mobile phone contacts | Primary | Mobile phone       | With me  |\n"
"| Text messages & mobile phone contacts | Duplicate | Computer hard drive | Office   |\n"
"| Text messages & mobile phone contacts | Duplicate | Backup SD card       | Home |\n"
"| Printed documents  | Primary        | Desk drawer                    | Office   |\n"
"| Scanned documents  | Duplicate      | External drive                 | Home     |\n"
"| Backup of all documents | Duplicate | Office server                  | Office   |\n"
msgstr ""

#. type: Plain text
#: src/files/backup/index.md:100
#, markdown-text
msgid "In the new table you will have 3 copies of information: in the computer, in the office server and at home, in 2 places, and at least one copy outside the office. 3-2-1 rule :)"
msgstr ""

#. type: Plain text
#: src/files/backup/index.md:102
#, markdown-text
msgid "Once you have completed your checklist, it's time to make the backup copies."
msgstr ""

#. type: Title #
#: src/files/backup/index.md:103
#, markdown-text, no-wrap
msgid "Back up the files in your computer to a local device"
msgstr ""

#. type: Plain text
#: src/files/backup/index.md:106
#, markdown-text
msgid "Store your backup on portable storage media so that you can take it to a safer location. External hard drives or USB memory sticks are possible choices."
msgstr ""

#. type: Plain text
#: src/files/backup/index.md:108
#, markdown-text
msgid "Because the files you decide to back up often contain the most sensitive information, it is important that you protect them using encryption. You can learn how to do this in [our guide on how to protect information](../secure-file-storage)."
msgstr ""

#. type: Plain text
#: src/files/backup/index.md:110
#, markdown-text, no-wrap
msgid "**Linux**\n"
msgstr ""

#. type: Bullet: '- '
#: src/files/backup/index.md:112
#, markdown-text
msgid "Most Linux distributions include a backup tool. Ubuntu has a built-in tool called Déjà Dup which allows you to back up and encrypt your files. [See this guide to Déjà Dup](https://www.techtarget.com/searchdatabackup/tutorial/Tutorial-How-to-use-Linux-Deja-Dup-to-back-up-and-restore-files) to learn how to use it."
msgstr ""

#. type: Plain text
#: src/files/backup/index.md:114 src/files/backup/index.md:190
#, markdown-text, no-wrap
msgid "**macOS**\n"
msgstr ""

#. type: Bullet: '- '
#: src/files/backup/index.md:116
#, markdown-text
msgid "[Back up to an external drive using Time Machine](https://support.apple.com/en-us/HT201250)."
msgstr ""

#. type: Bullet: '- '
#: src/files/backup/index.md:121
#, markdown-text
msgid "Follow the instructions in the section on how to back up to an external drive in [How to back up your files in Windows](https://www.microsoft.com/en-us/windows/learning-center/back-up-files)."
msgstr ""

#. type: Title #
#: src/files/backup/index.md:123
#, markdown-text, no-wrap
msgid "Back up your phone to a local device"
msgstr ""

#. type: Plain text
#: src/files/backup/index.md:126
#, markdown-text
msgid "If you are backing up your mobile device to your computer, your next step should be storing that backup to an external storage media device. Set your phone to back up automatically."
msgstr ""

#. type: Plain text
#: src/files/backup/index.md:128
#, markdown-text
msgid "To back up the contacts, text messages, settings and other data on your mobile phone, you may be able to connect it to your computer with a USB cable. You may also need to install software from the website of the company that manufactured your phone."
msgstr ""

#. type: Plain text
#: src/files/backup/index.md:130 src/files/backup/index.md:180
#, markdown-text, no-wrap
msgid "**Android**\n"
msgstr ""

#. type: Bullet: '- '
#: src/files/backup/index.md:133
#, markdown-text
msgid "[Move files from your Android device to your PC](https://support.google.com/android/answer/9064445)."
msgstr ""

#. type: Bullet: '- '
#: src/files/backup/index.md:133
#, markdown-text
msgid "If you find it difficult to back up all different type of information from the Android phone to a computer you can back up to [Google's cloud services using the tools built into your device](https://support.google.com/android/answer/2819582). Be aware that in such case your information will be saved on servers owned by Google."
msgstr ""

#. type: Plain text
#: src/files/backup/index.md:135 src/files/backup/index.md:185
#, markdown-text, no-wrap
msgid "**iOS**\n"
msgstr ""

#. type: Bullet: '- '
#: src/files/backup/index.md:139
#, markdown-text
msgid "[Back up to your macOS computer](https://support.apple.com/en-us/HT211229)"
msgstr ""

#. type: Bullet: '- '
#: src/files/backup/index.md:139
#, markdown-text
msgid "[Back up to your Windows computer](https://support.apple.com/en-us/108967)"
msgstr ""

#. type: Bullet: '- '
#: src/files/backup/index.md:139
#, markdown-text
msgid "Make sure to select the Encrypt local backup checkbox and to create a strong password."
msgstr ""

#. type: Title #
#: src/files/backup/index.md:140
#, markdown-text, no-wrap
msgid "Consider whether or not you should back up to \"cloud\" services"
msgstr ""

#. type: Plain text
#: src/files/backup/index.md:143
#, markdown-text
msgid "Consider whether you would prefer to use a service that encrypts your data for you as part of its service (called \"end-to-end encryption\" or \"zero-knowledge\" file storage services) or encrypt your files yourself and then back them up to the cloud."
msgstr ""

#. type: Plain text
#: src/files/backup/index.md:147
#, markdown-text
msgid "When you hear someone call a computer service \"the cloud,\" think \"other people's computers.\" Online file storage services like Google Drive, iCloud or Dropbox store your backups and other data on servers (computers) owned by the service providers, which are often companies. This means that your adversary could have a lot of time to try and access your information without you noticing (unlike devices in your possession, where you would be more likely to notice suspicious activity). So it is likely better to make a local copy of your valuable data and store it somewhere safe."
msgstr ""

#. type: Plain text
#: src/files/backup/index.md:150
#, markdown-text, no-wrap
msgid ""
"However, if there is a strong likelihood that your devices or workspace might be destroyed, or your backup may be found and seized, it could make sense to store your encrypted data in trusted file storage services.\n"
"</details>\n"
msgstr ""

#. type: Title ##
#: src/files/backup/index.md:151
#, markdown-text, no-wrap
msgid "Protect your files before you store them on cloud services"
msgstr ""

#. type: Bullet: '- '
#: src/files/backup/index.md:155
#, markdown-text
msgid "Get [Cryptomator](https://cryptomator.org/) and [use it to protect files](https://docs.cryptomator.org) you want to store on the cloud service."
msgstr ""

#. type: Bullet: '- '
#: src/files/backup/index.md:155
#, markdown-text
msgid "Alternatively, use [VeraCrypt](../../tools/veracrypt/) to create an encrypted volume, then copy it to the cloud."
msgstr ""

#. type: Plain text
#: src/files/backup/index.md:160
#, markdown-text, no-wrap
msgid ""
"If you are worried about someone (like hackers, or the owner of the service) accessing files you have stored online, you can protect them using encryption.\n"
"</details>\n"
msgstr ""

#. type: Title ##
#: src/files/backup/index.md:161
#, markdown-text, no-wrap
msgid "Encrypted cloud services"
msgstr ""

#. type: Plain text
#: src/files/backup/index.md:164
#, markdown-text
msgid "If you decide to keep your files in the cloud, consider using one of the following zero-knowledge, end-to-end encrypted options:"
msgstr ""

#. type: Bullet: '- '
#: src/files/backup/index.md:175
#, markdown-text
msgid "[Proton Drive](https://proton.me/drive)\t(5 GB free; then paid)"
msgstr ""

#. type: Bullet: '- '
#: src/files/backup/index.md:175
#, markdown-text
msgid "[Mega.io](https://mega.io/) (20 GB free; then paid)"
msgstr ""

#. type: Bullet: '- '
#: src/files/backup/index.md:175
#, markdown-text
msgid "[Sync](https://www.sync.com/) (5 GB free; then paid)"
msgstr ""

#. type: Bullet: '- '
#: src/files/backup/index.md:175
#, markdown-text
msgid "[Internxt](https://internxt.com/drive) (10 GB free; then paid)"
msgstr ""

#. type: Bullet: '- '
#: src/files/backup/index.md:175
#, markdown-text
msgid "[Skiff Drive](https://skiff.com/drive)\t(10 GB free; then paid)"
msgstr ""

#. type: Bullet: '- '
#: src/files/backup/index.md:175
#, markdown-text
msgid "[Filen](https://filen.io/) (10 GB free; then paid)"
msgstr ""

#. type: Bullet: '- '
#: src/files/backup/index.md:175
#, markdown-text
msgid "[PCloud](https://www.pcloud.com/) (paid plans, specifically pCloud Encryption)"
msgstr ""

#. type: Bullet: '- '
#: src/files/backup/index.md:175
#, markdown-text
msgid "[SpiderOak](https://spideroak.com/one/) (paid plans)"
msgstr ""

#. type: Bullet: '- '
#: src/files/backup/index.md:175
#, markdown-text
msgid "[Tresorit](https://tresorit.com/) (paid plans)"
msgstr ""

#. type: Bullet: '- '
#: src/files/backup/index.md:175
#, markdown-text
msgid "[Nextcloud](https://nextcloud.com/) (NextCloud can be self-hosted if you have your own server, or else you can choose [a hosting provider](https://nextcloud.com/providers/) offering at least 2 GB free.)"
msgstr ""

#. type: Title ##
#: src/files/backup/index.md:177
#, markdown-text, no-wrap
msgid "Back up to cloud services using your device's built-in tools"
msgstr ""

#. type: Bullet: '- '
#: src/files/backup/index.md:183
#, markdown-text
msgid "[Back up to your Google account](https://support.google.com/android/answer/2819582)."
msgstr ""

#. type: Bullet: '- '
#: src/files/backup/index.md:183
#, markdown-text
msgid "Backups are uploaded to Google servers and are encrypted with your Google Account password. For some data, your screen lock PIN, pattern, or password is also used to encrypt your data."
msgstr ""

#. type: Bullet: '- '
#: src/files/backup/index.md:188 src/files/backup/index.md:193
#, markdown-text
msgid "[Use iCloud](https://support.apple.com/en-us/HT204025)."
msgstr ""

#. type: Bullet: '- '
#: src/files/backup/index.md:188
#, markdown-text
msgid "[Encrypt your iOS backups](https://support.apple.com/en-us/108756)."
msgstr ""

#. type: Bullet: '- '
#: src/files/backup/index.md:193
#, markdown-text
msgid "[Turn on Advanced Data Protection for iCloud](https://support.apple.com/en-us/108756)."
msgstr ""

#. type: Bullet: '- '
#: src/files/backup/index.md:197
#, markdown-text
msgid "[Use OneDrive](https://support.microsoft.com/office/back-up-your-documents-pictures-and-desktop-folders-with-onedrive-d61a7930-a6fb-4b95-b28a-6552e77c3057)."
msgstr ""

#. type: Title #
#: src/files/backup/index.md:199
#, markdown-text, no-wrap
msgid "Back up your email"
msgstr ""

#. type: Bullet: '- '
#: src/files/backup/index.md:202
#, markdown-text
msgid "You can use an email client program (like Thunderbird) to regularly view and back up your email on your device. You can find explanations on the difference between downloading email with POP3 (to delete your email from the server) or IMAP (to keep it on the server) in [the official Thunderbird documentation](https://support.mozilla.org/en-US/kb/difference-between-imap-and-pop3). Most email services provide instructions on how to set up an email client to receive your mail with POP3 or IMAP."
msgstr ""

#. type: Title #
#: src/files/backup/index.md:203
#, markdown-text, no-wrap
msgid "Scan and back up printed documents"
msgstr ""

#. type: Bullet: '- '
#: src/files/backup/index.md:206
#, markdown-text
msgid "When possible, scan (or photograph) all of your important papers. Back up the scans or photos along with your other electronic documents, as discussed above."
msgstr ""

#. type: Title #
#: src/files/backup/index.md:207
#, markdown-text, no-wrap
msgid "Set a backup schedule"
msgstr ""

#. type: Bullet: '- '
#: src/files/backup/index.md:210
#, markdown-text
msgid "To back up all of the data types listed above, you will need a combination of software and processes. Make sure that each data type is stored in at least two separate locations."
msgstr ""

#. type: Title #
#: src/files/backup/index.md:211
#, markdown-text, no-wrap
msgid "Practice recovery"
msgstr ""

#. type: Bullet: '- '
#: src/files/backup/index.md:214
#, markdown-text
msgid "Once you have made your backups, test to make sure that you know how to open files and use them again. Remember that, in the end, it is the restore procedure, not the backup procedure, that you really care about!"
msgstr ""

#. type: Title #
#: src/files/backup/index.md:215
#, markdown-text, no-wrap
msgid "Establish procedures for coworkers"
msgstr ""

#. type: Bullet: '- '
#: src/files/backup/index.md:218
#, markdown-text
msgid "If you are working with a team, write up and share procedures for all staff to reliably and securely back up files. Communicate the risks that losing your data could have to your ability to do your work. It may help to work with your colleagues to fill out a grid like the one above to identify all data you work with as a team."
msgstr ""

#. type: Title #
#: src/files/backup/index.md:219
#, markdown-text, no-wrap
msgid "Other considerations"
msgstr ""

#. type: Plain text
#: src/files/backup/index.md:222
#, markdown-text
msgid "When you are making a backup plan, think of it in a larger perspective, asking yourself: \"How can I recover from a disaster and keep working?\""
msgstr ""

#. type: Plain text
#: src/files/backup/index.md:224
#, markdown-text
msgid "Your plan should not just be about your files, but also about:"
msgstr ""

#. type: Bullet: '- '
#: src/files/backup/index.md:228
#, markdown-text
msgid "Software you use, and the licenses for it,"
msgstr ""

#. type: Bullet: '- '
#: src/files/backup/index.md:228
#, markdown-text
msgid "How you can replace equipment if it is lost, destroyed, or confiscated,"
msgstr ""

#. type: Bullet: '- '
#: src/files/backup/index.md:228
#, markdown-text
msgid "Having a place you can go to to continue working in a crisis."
msgstr ""

#. type: Plain text
#: src/files/backup/index.md:229
#, markdown-text
msgid "Planning for this may mean setting aside money to recover from a loss. For example, you might consider writing this amount into a grant."
msgstr ""
