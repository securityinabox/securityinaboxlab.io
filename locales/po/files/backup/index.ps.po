# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-12-12 21:02+0000\n"
"PO-Revision-Date: 2024-12-30 00:11+0000\n"
"Last-Translator: Nangyalai <alainangy6@gmail.com>\n"
"Language-Team: Pashto <https://weblate.securityinabox.org/projects/files/backup/ps/>\n"
"Language: ps\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.12.2\n"

#. type: Yaml Front Matter Hash Value: title
#: src/files/backup/index.md:1
#, no-wrap
msgid "Back up and recover from information loss"
msgstr "د معلوماتو زېرمه کول (بېکپ-اپ) او له لاسه تللیو مالوماتو (ډېټا) ریکوري"

#. type: Plain text
#: src/files/backup/index.md:9
msgid "There is a common saying among digital protection professionals: \"the question is not _if_ you will lose your data, but _when_ you will lose it.\" A backup plan can minimize what you lose, so you can still access important information if any of your devices are stolen, seized, or damaged. Take or consider the following steps to plan ahead."
msgstr "په ډیجیټلي خوندیتوب کې یو عامه مقوله ده چې وایي: \"پوښتنه دا نه ده چې ایا تاسې خپل معلومات له لاسه ورکوئ او که نه، بلکې پوښتنه دا ده چې کله به یې له لاسه ورکوئ.\" د بېک-اپ یوه مناسبه تګلاره کولای شي رسېدونکی زیان راکم کړي، او ډاډ درکړي چې که ستاسې ټیلفون، کمپیوټر او یا بله الکترونیکي وسیله غلا شي، ونیول شي، یا زیانمنه شي، نو مهم معلومات به لا هم تاسو سره خوندي وي. د مخنیوي تدبیر په توګه لاندې ګامونه واخلئ یا یې په پام کې ونیسئ."

#. type: Title #
#: src/files/backup/index.md:10
#, no-wrap
msgid "Recover deleted or lost information"
msgstr "د حذف شوو یا له لاسه تللیو معلوماتو بېرته ترلاسه کول (ریکوري)"

#. type: Plain text
#: src/files/backup/index.md:13
msgid "To reflect on what to do if you have lost your data, you can start troubleshooting with the help of the Digital First Aid Kit's guide [I lost my data](https://digitalfirstaid.org/topics/lost-data/)."
msgstr "که تاسې خپل معلومات له لاسه ورکړي وي، د دې د حل لپاره د \"ډیجیټلي لومړنۍ مرستې کېټ\" لارښود چې د[ما خپل معلومات له لاسه ورکړي](https://digitalfirstaid.org/topics/lost-data/) په نوم یادیږي، وکاروئ."

#. type: Plain text
#: src/files/backup/index.md:15
msgid "You can use the tools listed in this section to recover deleted or lost data that was stored in a computer or external device."
msgstr "تاسې کولای شئ د دې برخې په کارولو سره هغه حذف شوي یا له لاسه تللي معلومات بېرته ترلاسه کړئ چې په کمپیوټر یا بهرنۍ وسیله کې ساتل شوي وو."

#. type: Plain text
#: src/files/backup/index.md:17
msgid "Be aware that these tools do not work if your device has written new data over your deleted information. If possible, stop using your device until you have tried to recover the files (or had someone do that for you). Doing additional work on your device could write over the files you lost and make them impossible to retrieve. The longer you use your computer before attempting to restore the file, the less likely it is that the file will be retrievable."
msgstr "پام مو وي، که چېرې له یوې وسېلې څخه ستاسو مالومات حذفیږي او تاسو پر هغه وسېله نوي مالومات زېرمه کوي، نو په داسې حال کې د ډیجېتالي لومړنۍ مرستې کېټ په کارولو سره حذف شوي مالومات بېرته ترلاسه کېدای نه شي.. که امکان وي، خپله الکترونیکي وسیله بنده کړئ تر هغو چې هڅه وکړئ خپل فایلونه بېرته ترلاسه کړئ (یا دا کار بل چا ته وسپارئ). د وسیلې په دوامداره کارولو سره کېدای شي حذف شوي فایلونه مو په بشپړ ډول له لاسه ورکړي او د بېرته ترلاسه کولو چانس یې صفر کړي. هر څومره چې د ډېټا ترلاسه کولو مخکې خپل کمپیوټر وکاروئ، هغومره د ورک شویو ډېټا بېرته ترلاسه کول، ناشوني کیږي."

#. type: Plain text
#: src/files/backup/index.md:19
#, no-wrap
msgid "**Linux, macOS, and Windows**\n"
msgstr "لینکس Linux، مک-اوس macOS او وینډوز Windows\n"

#. type: Bullet: '- '
#: src/files/backup/index.md:21
msgid "Try [TestDisk & PhotoRec](https://www.cgsecurity.org/wiki/PhotoRec). See [the official guide](https://www.cgsecurity.org/testdisk.pdf) for instructions on how to use it."
msgstr "[TestDisk & PhotoRec](https://www.cgsecurity.org/wiki/PhotoRec) وکاروئ. د کارولو څرنګوالي لپاره د دوی [رسمي لارښود](https://www.cgsecurity.org/testdisk.pdf) وګورئ."

#. type: Plain text
#: src/files/backup/index.md:23 src/files/backup/index.md:119
#: src/files/backup/index.md:195
#, no-wrap
msgid "**Windows**\n"
msgstr "وینډوز Windows\n"

#. type: Bullet: '- '
#: src/files/backup/index.md:25
msgid "Try [Recuva](https://www.ccleaner.com/recuva/). See [their guide](https://www.ccleaner.com/knowledge/recuva-tip-search-for-files)."
msgstr "[Recuva](https://www.ccleaner.com/recuva/) وکاروئ. [د دوی لارښود](https://www.ccleaner.com/knowledge/recuva-tip-search-for-files) وګورئ."

#. type: Plain text
#: src/files/backup/index.md:28 src/files/backup/index.md:145
#: src/files/backup/index.md:157
#, no-wrap
msgid "<details><summary>Learn why we recommend this</summary>\n"
msgstr "ولې دا سپارښتنه درته کوو؟\n"

#. type: Plain text
#: src/files/backup/index.md:30
msgid "When you delete a file, it disappears from view, but it remains on your device. Even after you empty the Recycle Bin or Trash, files you deleted can usually be found on the hard drive. See [How to destroy sensitive information](../destroy-sensitive-information) to learn how this can put your security at risk."
msgstr "کله چې تاسې یو فایل حذف کړئ، دا له نظره ورکېږي، خو په الکترونیکي وسیله کې پاتې کېږي. حتی که تاسې د ټرش یا ریسایکل بین منځپانګه هم خالي کړئ، حذف شوي فایلونه اکثر په هارډ ډرایو کې پیدا کېدلای شي. وګورئ چې [حساس معلومات څنګه له منځه یوسئ](../destroy-sensitive-information)ترڅو پوه شئ چې دا څرنګه کولای شي چې ستاسې امنیت له خطر سره مخ کړي."

#. type: Plain text
#: src/files/backup/index.md:33
#, no-wrap
msgid ""
"But if you accidentally delete an important file, this might work to your advantage. Some programs, like the ones listed in this section, can restore access to recently deleted files.\n"
"</details>\n"
msgstr "خو که تاسې سهواً یو مهم فایل حذف کړئ، دا حالت ستاسې په ګټه کارېدلی شي. ځینې پروګرامونه، لکه په دې برخه کې یاد شوي وسایل، کولای شي وروستي حذف شوي فایلونه بېرته ترلاسه کړي. </details>\n"

#. type: Title #
#: src/files/backup/index.md:34
#, no-wrap
msgid "Create a backup plan"
msgstr "د بېک اپ پلان جوړول"

#. type: Plain text
#: src/files/backup/index.md:37
msgid "To create a backup plan, take the following steps:"
msgstr "د بېک-اپ یوه مؤثره تګلاره جوړولو لپاره لاندې ګامونه تعقیب کړئ:"

#. type: Title ##
#: src/files/backup/index.md:38
#, no-wrap
msgid "Organize your information"
msgstr "خپله معلومات منظم کړئ"

#. type: Plain text
#: src/files/backup/index.md:41
msgid "Before you make your backup plan, try to move all of the folders containing the files you intend to back up into a single location, such as inside the \"Documents\" or \"My Documents\" folder."
msgstr "د بېک اپ پلان جوړولو دمخه، هڅه وکړئ ټول هغه فولډرونه چې بېک اپ ته یې اړتیا لري، په یوه ځای کې ، لکه د \"Documents\" یا \"My Documents\" فولډر دننه راټول کړئ."

#. type: Title ##
#: src/files/backup/index.md:42
#, no-wrap
msgid "Identify where and what your information is"
msgstr "ستاسې معلومات چېرته او په څه ډول خوندي دي"

#. type: Plain text
#: src/files/backup/index.md:45
msgid "The first step to making a backup plan is to picture where your personal and work information is currently located. Your email, for example, may be stored on your email provider's server, on your own computer, or in both places at once. And, of course, you might have several email accounts."
msgstr "د بېک-اپ پلان جوړولو لومړنی ګام دا دی چې معلوم کړئ ستاسې شخصي او کاري معلومات اوس مهال چېرته ساتل کېږي. د بېلګې په توګه، ستاسې ایمیل ممکن ستاسې د انترنټ خدمتونو چمتو کونکي کمپنۍ په سرور کې، ستاسې خپل کمپیوټر کې یا په دواړو ځایونو کې په یو وخت کې ساتل کېږي. همدارنګه، کېدای شي چې تاسې څو ایمیل حسابونه ولرئ."

#. type: Plain text
#: src/files/backup/index.md:47
msgid "Then there are important documents on the computers you use in the office or at home: word processing documents, presentations, PDFs and spreadsheets, among other examples. Your computer and mobile devices also store your contact lists, chat histories and personal program settings, all of which can be considered sensitive."
msgstr "بیا هغه مهم اسناد په نظر کې ونیسئ چې تاسې په دفتر یا کور کې په کمپیوټرونو کې لرئ: لکه د متن پروسسولو اسناد، پرزېنټیشنونه، PDFs او جدولونه. ستاسې کمپیوټر او موبایل او نور وسایل ستاسې د اړیکو لیستونه، د چټ تاریخچه، او شخصي پروګرام تنظیمات هم ساتي، چې دا ټول حساس معلومات ګڼل کېدای شي."

#. type: Plain text
#: src/files/backup/index.md:49
msgid "You may also have stored some information on removable media like USB memory sticks, portable hard drives and other storage media.  If you have a website, it may contain a large collection of articles built up over years of work. Finally, don't forget your non-digital information, such as paper notebooks, diaries and letters."
msgstr "کېدای شي تاسې ځینې معلومات په لېږدېدونکو ذخیره‌يي وسیلو لکه USB ، هارډهارډ ډرایو او نورو ذخیره‌يي وسیلو کې خوندي کړي وي. که تاسې یوه وېبپاڼه لرئ، نو کېدای شي چې په کې د کلونو کار مقالې زېرمه شوي وي. په پای کې، خپل غیر ډیجیټلي معلومات لکه کاغذي کتابچې، ډایرۍ او لیکونه مه هېروئ."

#. type: Plain text
#: src/files/backup/index.md:51
msgid "You can learn more on how to map your information in the [Holistic security manual section on understanding and cataloguing information](https://holistic-security.tacticaltech.org/chapters/explore/2-4-understanding-and-cataloguing-our-information.html)."
msgstr "خپلو مالوماتو پېژندنې او طبقه بندی په اړه نور مالومات، د [ټولییز خوندیتوب لارښود برخه کې د «معلوماتو کتلاګول او د کتلاګولو طریقه»](https://holistic-security.tacticaltech.org/chapters/explore/2-4-understanding-and-cataloguing-our-information.html) لاندې عنوان کې وګورئ."

#. type: Title ##
#: src/files/backup/index.md:52
#, no-wrap
msgid "Define which are primary copies and which are duplicates"
msgstr "د فایل لومړنۍ نسخې او نقلونه مشخص کړئ"

#. type: Plain text
#: src/files/backup/index.md:55
msgid "When you are backing up files, it is sometimes suggested you use the 3-2-1 rule: have at least 3 copies of any piece of information, in at least 2 places, with at least 1 copy in a different place from the original."
msgstr "کله چې د فایلونو بېک-اپ کوئ، په ځینو مواردو کې سپارښتنه کېږي چې د ۳-۲-۱ قاعده وکاروئ: د هرې برخې معلوماتو لپاره لږ تر لږه درې نسخې ولرئ، په لږ تر لږه دوه بېلابېلو ځایونو کې، او لږ تر لږه یوه نسخه د اصلي نسخې له ځایه جلا وساتئ."

#. type: Plain text
#: src/files/backup/index.md:57
msgid "Before you start making backups, decide which of the files you have mapped are \"primary copies\" and which are duplicates. The primary copy should be the most up-to-date version of a particular file or collection of files; it should be the copy you would actually edit if you needed to update the content. Obviously, this does not apply to files of which you have only one copy, but it is extremely important for certain types of information."
msgstr "د بېک-اپ جوړولو دمخه، پرېکړه وکړئ چې په نښه شویو فایلونو کې کومې نسخې \"لومړنۍ\" دي او کومې یې نقلونه دي. لومړنۍ نسخه باید د فایل یا فایلونو ټولګې تر ټولو وروستۍ تازه شوې نسخه وي؛ دا باید هغه نسخه وي چې تاسې د اېډېټ کولو لپاره استفاده ترې کوئ. البته، دا د هغو فایلونو لپاره نه تطبیقېږي چې یوازې یوه نسخه یې شتون لري، خو د ځینو ډولونو معلوماتو لپاره خورا مهم دی."

#. type: Plain text
#: src/files/backup/index.md:59
msgid "One common disaster scenario occurs when only duplicates of an important document are backed up and the primary copy itself gets lost or destroyed before those duplicates can be updated. For example, say you have been travelling for a week, updating a copy of an important spreadsheet stored on a USB memory stick. You should begin thinking of that copy as your primary copy, because it is more up-to-date than backup copies you may have made at the office."
msgstr "یو عامه ناورین هغه وخت پېښېږي کله چې د یوه مهم سند یوازې نقلونه بېک-اپ شي او خپله لومړنۍ نسخه مخکې له دې چې اېډېټ شي له لاسه ولاړه شي یا له منځه ولاړه شي. د بېلګې په توګه، که تاسې د یوې اوونۍ لپاره په سفر کې یاست او په یوه مهم جدول باندې کار کوئ چې په USB حافظه کې ذخیره شوی وي، نو باید د دې نسخې په اړه د لومړنۍ نسخې په توګه فکر وکړئ، ځکه دا د دفتر په بېک-اپ کې له ذخیره شویو نسخو څخه نوي ده."

#. type: Plain text
#: src/files/backup/index.md:61
msgid "Write down the physical location of all primary and duplicate copies of the information you have identified. This will help you clarify your needs and begin to define an appropriate backup policy. The following table is a very basic example. Your list may be much longer, and contain more than one data type stored on multiple storage devices."
msgstr "د هغو معلوماتو د لومړنیو او نقل نسخو فزیکي ځای ولیکئ چې تاسې مشخص کړي دي. دا کار به تاسې سره مرسته وکړي ترڅو خپلې اړتیاوې روښانه کړئ او د یوې مناسبه بېک-اپ تګلارې لپاره چوکاټ تعریف کړئ. لاندې جدول یو ساده مثال دی. ستاسې لیست ممکن ډېر اوږد وي او په ډېرو ذخیره‌يي وسیلو کې ګڼ ډوله معلومات ولري."

#. type: Plain text
#: src/files/backup/index.md:72
#, no-wrap
msgid ""
"| Data type               | Primary / Duplicate | Storage device      | Location |\n"
"|-------------------------|---------------------|---------------------|----------|\n"
"| Research files          | Primary             | Computer hard drive | Office   |\n"
"| Human rights violations testimonies | Duplicate | USB memory stick  | With me  |\n"
"| Program databases (photos, address book, calendar, etc.) | Primary  | Computer hard drive | Office   |\n"
"| Shared documents        | Duplicate           | Office server       | Office   |\n"
"| Videos and pictures     | Duplicate           | External hard disk          | Home     |\n"
"| Email & email contacts  | Primary             | Email account   | Email server |\n"
"| Text messages & phone contacts | Primary      | Mobile phone        | With me  |\n"
"| Printed documents (contracts, invoices, etc.) | Primary | Desk drawer | Office |\n"
msgstr ""
"د معلوماتو ډول..... لومړنۍ / نقل ..... ذخیره‌يي وسیله.....ځای\n"
"\n"
"د څېړنې فایلونه\tلومړنۍ\tد کمپیوټر هارډ ډرایو\tدفتر\n"
"د بشري حقونو د سرغړونو شاهدۍ\tنقل\tUSB حافظه\tله ما سره\n"
"د پروګرام ډیټابیسونه (عکسونه، اړیکې، کلیزه)\tلومړنۍ\tد کمپیوټر هارډ ډرایو\tدفتر\n"
"شریک اسناد\tنقل\tد دفتر سرور\tدفتر\n"
"ویډیوګانې او عکسونه\tنقل\tبهرنۍ هارډ ډرایو\tکور\n"
"ایمیلونه او د ایمیل اړیکې\tلومړنۍ\tد ایمیل حساب\tد ایمیل سرور\n"
"د متن پیغامونه او د ټیلیفون اړیکې\tلومړنۍ\tموبایل ټیلیفون\tله ما سره\n"
"چاپي اسناد (قراردادونه، بیلونه)\tلومړنۍ\tد مېز دراز\tدفتر\n"

#. type: Plain text
#: src/files/backup/index.md:74
msgid "In the table above, you can see that:"
msgstr "په پورتني جدول کې ښودل شوې ده چې:"

#. type: Bullet: '- '
#: src/files/backup/index.md:79
msgid "The only documents that will survive if your office computer's hard drive crashes are duplicates on your USB memory stick and other external storage media as well as shared documents on the server."
msgstr "که د دفتر د کمپیوټر هارډویر (هارډ ډرایو) خراب شي، یوازې هغه کاپي اسناد به پاتې شي چې په USB حافظه، بهرنۍ زیرمه ییزه وسیله یا د سرور په شریکو اسنادو کې وي."

#. type: Bullet: '- '
#: src/files/backup/index.md:79
msgid "You have no offline copy of your email messages or your address book, so if you forget your email password (or if someone manages to change it maliciously), you will lose access to them."
msgstr "ستاسې د برېښنالیک پیغامونو یا د اړیکو کتاب د آف‌لاین نسخه نشته، نو که تاسې خپل ایمیل پاسورډ هېر کړئ (یا بل څوک په کینه دا بدل کړي)، نو ورته لاسرسی به له لاسه ورکړئ."

#. type: Bullet: '- '
#: src/files/backup/index.md:79
msgid "You have no copies of any data from your mobile phone."
msgstr "د خپل موبایل ټیلیفون هېڅ ډېټا کاپي نه لرئ."

#. type: Bullet: '- '
#: src/files/backup/index.md:79
msgid "You have no duplicate copies, digital or physical, of printed documents such as contracts and invoices."
msgstr "ستاسې د چاپي اسنادو لکه قراردادونو او بیلونو هېڅ ډیجیټلي یا فزیکي کاپي ګانې نشته."

#. type: Plain text
#: src/files/backup/index.md:81
msgid "After following the checklist in this section, you should have rearranged your storage devices, data types and backups in a way that makes your information more resistant to disaster. For example:"
msgstr "د دې چک‌لیست له تعقیبولو وروسته مو، باید خپلې ذخیروي (زېرمه ييزې) وسیلې، د معلوماتو ډولونه او بېک-اپونه په داسې ډول بیا تنظیم کړي وي چې ستاسې معلومات د ناورینونو پر وړاندې زیات مقاومت ولري. د بېلګې په توګه:"

#. type: Plain text
#: src/files/backup/index.md:98
#, no-wrap
msgid ""
"| Data Type       | Primary/Duplicate | Storage Device                 | Location |\n"
"|-----------------|-------------------|--------------------------------|----------|\n"
"| Human rights violations testimonies  | Primary | Computer hard drive | Office   |\n"
"| Human rights violations testimonies  | Duplicate | USB memory stick  | Home     |\n"
"| Research files  | Primary           | Computer hard drive            | Office   |\n"
"| Research files  | Duplicate         | USB memory stick               | With me  |\n"
"| Program databases | Primary         | Computer hard drive            | Office   |\n"
"| Program databases | Duplicate       | External drive                 | Home     |\n"
"| Email & email contacts | Primary    | Email account              | Email server |\n"
"| Email & email contacts | Duplicate  | Thunderbird on office computer | Office   |\n"
"| Text messages & mobile phone contacts | Primary | Mobile phone       | With me  |\n"
"| Text messages & mobile phone contacts | Duplicate | Computer hard drive | Office   |\n"
"| Text messages & mobile phone contacts | Duplicate | Backup SD card       | Home |\n"
"| Printed documents  | Primary        | Desk drawer                    | Office   |\n"
"| Scanned documents  | Duplicate      | External drive                 | Home     |\n"
"| Backup of all documents | Duplicate | Office server                  | Office   |\n"
msgstr ""
"د معلوماتو ډول\tلومړنۍ/نقل\tذخیره‌يي وسیله\tځای\n"
"\n"
"د بشري حقونو د سرغړونو اسناد او مدارک\tلومړنۍ\tد کمپیوټر هارډویر (هارډ ډرایو)\tدفتر\n"
"د بشري حقونو د سرغړونو اسناد او مدارک\tکاپي\tUSB حافظه\tکور\n"
"د څېړنې فایلونه \tلومړنۍ\tد کمپیوټر هارډویر (هارډ ډرایو)\tدفتر\n"
"د څېړنې فایلونه\tکاپي\tUSB حافظه\tله ما سره\n"
"د پروګرام ډیټابیسونه\tلومړنۍ\tد کمپیوټر هارډویر (هارډ ډرایو)\tدفتر\n"
"د پروګرام ډیټابیسونه\tکاپي\tبهرنۍ هارډ ډرایو\tکور\n"
"ایمیلونه او د ایمیل اړیکې\tلومړنۍ\tد ایمیل حساب\tد ایمیل سرور\n"
"ایمیلونه او د ایمیل اړیکې\tکاپي\tد دفتر په کمپیوټر کې Thunderbird\tدفتر\n"
"د موبایل ټیلیفون اړیکې او پیغامونه\tلومړنۍ\tموبایل ټیلیفون\tله ما سره\n"
"د موبایل ټیلیفون اړیکې او پیغامونه\tکاپي\tد کمپیوټر هارډویر (هارډ ډرایو)\tدفتر\n"
"د موبایل ټیلیفون اړیکې او پیغامونه\tکاپي\tد بېک-اپ SD کارت\tکور\n"
"چاپي اسناد (قراردادونه، بیلونه)\tلومړنۍ\tد مېز دراز\tدفتر\n"
"سکن شوي اسناد\tکاپي\tبهرنی هارډویر (هارډ ډرایو)\tکور\n"
"د ټولو اسنادو بېک-اپ\tکاپي\tد دفتر سرور\tدفتر\n"

#. type: Plain text
#: src/files/backup/index.md:100
msgid "In the new table you will have 3 copies of information: in the computer, in the office server and at home, in 2 places, and at least one copy outside the office. 3-2-1 rule :)"
msgstr "په دې جدول کې ښکاري چې تاسې د ۳-۲-۱ قاعده په بریالیتوب سره پلې کړې ده: معلومات په درېیو نسخو کې خوندي دي، په دوه بېلابېلو ځایونو کې، او لږ تر لږه یوه نسخه یې له دفتر څخه بهر ده. د ۳-۲-۱ قاعده:)"

#. type: Plain text
#: src/files/backup/index.md:102
msgid "Once you have completed your checklist, it's time to make the backup copies."
msgstr "کله چې د چک-لیست کار پای ته ورسېد، بیا نو د بېک-اپ اخېستلو وخت دی."

#. type: Title #
#: src/files/backup/index.md:103
#, no-wrap
msgid "Back up the files in your computer to a local device"
msgstr "په کمپیوټر کې فایلونه په ځایي وسیله بېک-اپ کړئ"

#. type: Plain text
#: src/files/backup/index.md:106
msgid "Store your backup on portable storage media so that you can take it to a safer location. External hard drives or USB memory sticks are possible choices."
msgstr "خپل بېک-اپ د لېږد وړ ذخیره‌يي وسیلو لکه هارډ ډرایو یا USB حافظې ته واچوئ ترڅو یې خوندي ځای ته انتقال کړئ."

#. type: Plain text
#: src/files/backup/index.md:108
msgid "Because the files you decide to back up often contain the most sensitive information, it is important that you protect them using encryption. You can learn how to do this in [our guide on how to protect information](../secure-file-storage)."
msgstr "د دې لپاره چې هغه فایلونه چې تاسې یې بېک-اپ کوئ اکثراً حساس معلومات لري، مهمه ده چې دا فایلونه د کوډ کولو له لارې خوندي کړئ. د کوډ کولو څرنګوالي لپاره زموږ د [د معلوماتو خوندي کولو لارښود](../secure-file-storage) ته مراجعه وکړئ."

#. type: Plain text
#: src/files/backup/index.md:110
#, no-wrap
msgid "**Linux**\n"
msgstr "لینکس Linux\n"

#. type: Bullet: '- '
#: src/files/backup/index.md:112
msgid "Most Linux distributions include a backup tool. Ubuntu has a built-in tool called Déjà Dup which allows you to back up and encrypt your files. [See this guide to Déjà Dup](https://www.techtarget.com/searchdatabackup/tutorial/Tutorial-How-to-use-Linux-Deja-Dup-to-back-up-and-restore-files) to learn how to use it."
msgstr "ډېری لینکس وېشونه د بېک-اپ لپاره یو ځانګړی وسیله لري. د بېلګې په توګه، په Ubuntu کې یو جوړ شوی وسیله \"Déjà Dup\" شتون لري چې تاسې ته د فایلونو بېک-اپ او کوډ کولو اجازه درکوي. د دې وسیلې کارولو د زده‌کړې لپاره [د Déjà Dup لارښود](https://www.techtarget.com/searchdatabackup/tutorial/Tutorial-How-to-use-Linux-Deja-Dup-to-back-up-and-restore-files) وګورئ."

#. type: Plain text
#: src/files/backup/index.md:114 src/files/backup/index.md:190
#, no-wrap
msgid "**macOS**\n"
msgstr "مک-او-اس-macOS\n"

#. type: Bullet: '- '
#: src/files/backup/index.md:116
msgid "[Back up to an external drive using Time Machine](https://support.apple.com/en-us/HT201250)."
msgstr "[د «ټایم ماشین» په مرسته د بهرنۍ ډرایو ته بېک-اپ](https://support.apple.com/en-us/HT201250) ترسره کړئ."

#. type: Bullet: '- '
#: src/files/backup/index.md:121
msgid "Follow the instructions in the section on how to back up to an external drive in [How to back up your files in Windows](https://www.microsoft.com/en-us/windows/learning-center/back-up-files)."
msgstr "په وینډوز کې د فایلونو بېک-اپ کولو برخه کې [ په Windows کې څنګه فایلونه بېک آپ کړو](https://www.microsoft.com/en-us/windows/learning-center/back-up-files) لارښوونې تعقیب کړئ."

#. type: Title #
#: src/files/backup/index.md:123
#, no-wrap
msgid "Back up your phone to a local device"
msgstr "په موبایل کې فایلونه په ځایي وسیله بېک-اپ کړئ"

#. type: Plain text
#: src/files/backup/index.md:126
msgid "If you are backing up your mobile device to your computer, your next step should be storing that backup to an external storage media device. Set your phone to back up automatically."
msgstr "که تاسې خپل موبایل وسیله په کمپیوټر کې بېک-اپ کوئ، نو راتلونکی ګام باید دا وي چې دغه بېک-اپ په یوه بهرنۍ ذخیره‌يي وسیله کې وساتئ. خپل ټیلیفون په داسې ډول تنظیم کړئ چې په اوتومات ډول بېک-اپ وکړي."

#. type: Plain text
#: src/files/backup/index.md:128
msgid "To back up the contacts, text messages, settings and other data on your mobile phone, you may be able to connect it to your computer with a USB cable. You may also need to install software from the website of the company that manufactured your phone."
msgstr "د خپل موبایل ټیلیفون د اړیکو، پیغامونو، تنظیماتو او نورو معلوماتو بېک-اپ کولو لپاره، کېدای شي تاسې خپل ټیلیفون د USB کیبل په مرسته کمپیوټر سره وصل کړئ. کېدای شي د ټیلیفون د جوړونکي کمپنۍ وېبپاڼې څخه ځانګړی سافټویر هم ډاونلوډ او نصب کړئ."

#. type: Plain text
#: src/files/backup/index.md:130 src/files/backup/index.md:180
#, no-wrap
msgid "**Android**\n"
msgstr "اندروید Android\n"

#. type: Bullet: '- '
#: src/files/backup/index.md:133
msgid "[Move files from your Android device to your PC](https://support.google.com/android/answer/9064445)."
msgstr "[خپل Android وسیله څخه فایلونه کمپیوټر ته انتقال کړئ](https://support.google.com/android/answer/9064445)."

#. type: Bullet: '- '
#: src/files/backup/index.md:133
msgid "If you find it difficult to back up all different type of information from the Android phone to a computer you can back up to [Google's cloud services using the tools built into your device](https://support.google.com/android/answer/2819582). Be aware that in such case your information will be saved on servers owned by Google."
msgstr "که تاسې د Android ټیلیفون څخه کمپیوټر ته د ټولو مختلفو ډولونو معلوماتو بېک-اپ کول ستونزمن ګڼئ، نو [د ګوګل کلاوډ خدمتونو په مرسته په خپل ټیلیفون کې جوړ شوي وسیلې وکاروئ](https://support.google.com/android/answer/2819582). په یاد ولرئ چې په دې حالت کې به ستاسې معلومات د ګوګل ملکیت سرورونو کې زېرمه شي."

#. type: Plain text
#: src/files/backup/index.md:135 src/files/backup/index.md:185
#, no-wrap
msgid "**iOS**\n"
msgstr "آی-او-اس iOS\n"

#. type: Bullet: '- '
#: src/files/backup/index.md:139
msgid "[Back up to your macOS computer](https://support.apple.com/en-us/HT211229)"
msgstr "[خپل وسیله د macOS کمپیوټر سره بېک-اپ کړئ](https://support.apple.com/en-us/HT211229)"

#. type: Bullet: '- '
#: src/files/backup/index.md:139
msgid "[Back up to your Windows computer](https://support.apple.com/en-us/108967)"
msgstr "[خپل وسیله د Windows کمپیوټر سره بېک-اپ کړئ](https://support.apple.com/en-us/108967)"

#. type: Bullet: '- '
#: src/files/backup/index.md:139
msgid "Make sure to select the Encrypt local backup checkbox and to create a strong password."
msgstr "ډاډ ترلاسه کړئ چې د \"Encrypt local backup\" چک‌باکس ټاکلی او یو قوي پاسورډ جوړ کړی وي."

#. type: Title #
#: src/files/backup/index.md:140
#, no-wrap
msgid "Consider whether or not you should back up to \"cloud\" services"
msgstr "پر کلاوډ خدمتونو د بېک-اپ کولو په اړه غور وکړئ"

#. type: Plain text
#: src/files/backup/index.md:143
msgid "Consider whether you would prefer to use a service that encrypts your data for you as part of its service (called \"end-to-end encryption\" or \"zero-knowledge\" file storage services) or encrypt your files yourself and then back them up to the cloud."
msgstr "وګورئ چې آیا تاسې داسې خدمت غوره کوئ چې ستاسې معلومات د خدمت په برخه کې کوډ کړي (د \"پای-تر-پایه کوډ کول\" یا \"تخنیکي مهارت نلرو\" فایل ذخیره خدمتونه) یا خپل فایلونه پخپله کوډ کړئ او بیا یې کلاوډ ته بېک-اپ کړئ."

#. type: Plain text
#: src/files/backup/index.md:147
msgid "When you hear someone call a computer service \"the cloud,\" think \"other people's computers.\" Online file storage services like Google Drive, iCloud or Dropbox store your backups and other data on servers (computers) owned by the service providers, which are often companies. This means that your adversary could have a lot of time to try and access your information without you noticing (unlike devices in your possession, where you would be more likely to notice suspicious activity). So it is likely better to make a local copy of your valuable data and store it somewhere safe."
msgstr "کله چې څوک یو کمپیوټر خدمت ته \"کلاوډ\" وایي، فکر وکړئ چې دا د \"د نورو خلکو کمپیوټرونه\" دي. آنلاین فایل ذخیره خدمتونه لکه ګوګل ډرایو، iCloud یا ډراپ باکس ستاسې بېک-اپونه او نور معلومات د انترنیتي خدمت وړاندې کونکو (اکثره شرکتونو) په سرورونو کې ذخیره کوي. دا معنی لري چې ستاسې مخالفینو ممکن ډېر وخت ولري ترڅو ستاسې معلوماتو ته د لاسرسي هڅه وکړي، پرته له دې چې تاسې پرې خبر شئ (برخلاف د هغو وسیلو چې تاسې سره وي، او د شکمن فعالیت په اړه زر پوهېدلای شئ). له همدې امله، دا غوره ده چې د خپلو ارزښتمنو معلوماتو یوه ځایي نسخه جوړ کړئ او په خوندي ځای کې یې ذخیره کړئ."

#. type: Plain text
#: src/files/backup/index.md:150
#, no-wrap
msgid ""
"However, if there is a strong likelihood that your devices or workspace might be destroyed, or your backup may be found and seized, it could make sense to store your encrypted data in trusted file storage services.\n"
"</details>\n"
msgstr "خو، که ډېره قوي شونتیا وي چې ستاسې وسیلې یا د کار ځای له منځه یوړل شي، یا ستاسې بېک-اپ کشف او ونیول شي، نو په باور لرونکو فایل زېرمه خدمتونو کې د خپلو کوډ شویو معلوماتو زېرمه کول یو منطقي ګام دی.</details>\n"

#. type: Title ##
#: src/files/backup/index.md:151
#, no-wrap
msgid "Protect your files before you store them on cloud services"
msgstr "مخکې له دې چې فایلونه په کلاوډ خدمتونو کې زېرمه کړئ، خوندي یې کړئ"

#. type: Bullet: '- '
#: src/files/backup/index.md:155
msgid "Get [Cryptomator](https://cryptomator.org/) and [use it to protect files](https://docs.cryptomator.org) you want to store on the cloud service."
msgstr "[Cryptomator](https://cryptomator.org/) ترلاسه کړئ او د هغو [فایلونو خوندیتوب لپاره یې وکاروئ](https://docs.cryptomator.org) چې تاسو یې پر کلاوډ خدمتونو زېرمه کول غواړئ."

#. type: Bullet: '- '
#: src/files/backup/index.md:155
msgid "Alternatively, use [VeraCrypt](../../tools/veracrypt/) to create an encrypted volume, then copy it to the cloud."
msgstr "همدارنګه تاسو کولای شئ چې یا د [VeraCrypt](../../tools/veracrypt/) په مرسته یو کوډ شوی حجم جوړ کړئ او بیا یې کلاوډ ته کاپي کړئ."

#. type: Plain text
#: src/files/backup/index.md:160
#, no-wrap
msgid ""
"If you are worried about someone (like hackers, or the owner of the service) accessing files you have stored online, you can protect them using encryption.\n"
"</details>\n"
msgstr "که تاسې اندېښمن یاست چې څوک (لکه هکران یا د شرکت مالک) هغو فایلونو ته لاسرسی ومومي چې تاسې آنلاین زېرمه کړي، تاسې یې د کوډ کولو په مرسته خوندي کولای شئ. </details>\n"

#. type: Title ##
#: src/files/backup/index.md:161
#, no-wrap
msgid "Encrypted cloud services"
msgstr "انکرېپټ شوي کلاوډ خدمتونه"

#. type: Plain text
#: src/files/backup/index.md:164
msgid "If you decide to keep your files in the cloud, consider using one of the following zero-knowledge, end-to-end encrypted options:"
msgstr "که پرېکړه مو دا وي چې خپل فایلونه په کلاوډ کې وساتئ، نو لاندې ، پای-تر-پایه(end to end) انکریپشن ډولونه چې په ساده توګه لیکل شوي، تعقیب کړئ:"

#. type: Bullet: '- '
#: src/files/backup/index.md:175
msgid "[Proton Drive](https://proton.me/drive)\t(5 GB free; then paid)"
msgstr "[Proton Drive](https://proton.me/drive) چې ۵ ګیګابایټ وړیا او تر هغې زیات یې په پیسو رانیول کیږي"

#. type: Bullet: '- '
#: src/files/backup/index.md:175
msgid "[Mega.io](https://mega.io/) (20 GB free; then paid)"
msgstr "[Mega.io](https://mega.io/) چې ۲۰ ګیګابایټ وړیا او تر هغې زیات یې په پیسو رانیول کیږي"

#. type: Bullet: '- '
#: src/files/backup/index.md:175
msgid "[Sync](https://www.sync.com/) (5 GB free; then paid)"
msgstr "[Sync](https://www.sync.com/) چې ۵ ګیګابایټ وړیا او تر هغې زیات یې په پیسو رانیول کیږي"

#. type: Bullet: '- '
#: src/files/backup/index.md:175
msgid "[Internxt](https://internxt.com/drive) (10 GB free; then paid)"
msgstr "[Internxt](https://internxt.com/drive) چې ۱۰ ګیګابایټ وړیا او تر هغې زیات یې په پیسو رانیول کیږي"

#. type: Bullet: '- '
#: src/files/backup/index.md:175
msgid "[Skiff Drive](https://skiff.com/drive)\t(10 GB free; then paid)"
msgstr "[Skiff Drive](https://skiff.com/drive) چې ۱۰ ګیګابایټ وړیا او تر هغې زیات یې په پیسو رانیول کیږي"

#. type: Bullet: '- '
#: src/files/backup/index.md:175
msgid "[Filen](https://filen.io/) (10 GB free; then paid)"
msgstr "[Filen](https://filen.io/) چې ۱۰ ګیګابایټ وړیا او تر هغې زیات یې په پیسو رانیول کیږي"

#. type: Bullet: '- '
#: src/files/backup/index.md:175
msgid "[PCloud](https://www.pcloud.com/) (paid plans, specifically pCloud Encryption)"
msgstr "[PCloud](https://www.pcloud.com/)، خصوصاً د PCloud Encryption چې په پلانې ډول په پېسو رانیول کیږي"

#. type: Bullet: '- '
#: src/files/backup/index.md:175
msgid "[SpiderOak](https://spideroak.com/one/) (paid plans)"
msgstr "[SpiderOak](https://spideroak.com/one/) چې په پلانې ډول په پیسو رانیول کیږي"

#. type: Bullet: '- '
#: src/files/backup/index.md:175
msgid "[Tresorit](https://tresorit.com/) (paid plans)"
msgstr "[Tresorit](https://tresorit.com/) چې په پیسو رانیول کیږي"

#. type: Bullet: '- '
#: src/files/backup/index.md:175
msgid "[Nextcloud](https://nextcloud.com/) (NextCloud can be self-hosted if you have your own server, or else you can choose [a hosting provider](https://nextcloud.com/providers/) offering at least 2 GB free.)"
msgstr "[Nextcloud](https://nextcloud.com/)؛ که خپل سرور لرئ نو کولای شئ چې Nextcloud پکښې کوربه کړئ که نه نو [یو hosting provider](https://nextcloud.com/providers/)، پیدا کړئ چې لږ تر لږه ۲ ګیګابایټ پورې وړیا خدمت وړاندې کړي."

#. type: Title ##
#: src/files/backup/index.md:177
#, no-wrap
msgid "Back up to cloud services using your device's built-in tools"
msgstr "د الکترونیکې وسیلي کې د موجودو اسبابو په مرسته کلاوډ خدمتونو ته بېک اپ کول"

#. type: Bullet: '- '
#: src/files/backup/index.md:183
msgid "[Back up to your Google account](https://support.google.com/android/answer/2819582)."
msgstr "[پل معلومات د ګوګل اکاونټ ته بېک-اپ کړئ](https://support.google.com/android/answer/2819582)."

#. type: Bullet: '- '
#: src/files/backup/index.md:183
msgid "Backups are uploaded to Google servers and are encrypted with your Google Account password. For some data, your screen lock PIN, pattern, or password is also used to encrypt your data."
msgstr "بېک-اپونه د ګوګل سرورونو ته اپلوډ کېږي او ستاسې د ګوګل حساب پاسورډ په مرسته کوډ کېږي. د ځینو معلوماتو لپاره، ستاسې د پردې د قفل PIN، بڼه، یا پاسورډ هم د کوډ کولو لپاره کارول کېږي."

#. type: Bullet: '- '
#: src/files/backup/index.md:188 src/files/backup/index.md:193
msgid "[Use iCloud](https://support.apple.com/en-us/HT204025)."
msgstr "[iCloud وکاروئ](https://support.apple.com/en-us/HT204025)."

#. type: Bullet: '- '
#: src/files/backup/index.md:188
msgid "[Encrypt your iOS backups](https://support.apple.com/en-us/108756)."
msgstr "[خپل iOS backups انکرېپټ کړئ](https://support.apple.com/en-us/108756)."

#. type: Bullet: '- '
#: src/files/backup/index.md:193
msgid "[Turn on Advanced Data Protection for iCloud](https://support.apple.com/en-us/108756)."
msgstr "[iCloud لپاره د ډېټا د لوړې کچې خوندیتوب Advanced Data Protection آپشن فعال کړئ](https://support.apple.com/en-us/108756)."

#. type: Bullet: '- '
#: src/files/backup/index.md:197
msgid "[Use OneDrive](https://support.microsoft.com/office/back-up-your-documents-pictures-and-desktop-folders-with-onedrive-d61a7930-a6fb-4b95-b28a-6552e77c3057)."
msgstr "[OneDrive وکاروئ](https://support.microsoft.com/office/back-up-your-documents-pictures-and-desktop-folders-with-onedrive-d61a7930-a6fb-4b95-b28a-6552e77c3057)."

#. type: Title #
#: src/files/backup/index.md:199
#, no-wrap
msgid "Back up your email"
msgstr "برېښنالیک (ایمیل) بېک-اپ کړئ"

#. type: Bullet: '- '
#: src/files/backup/index.md:202
msgid "You can use an email client program (like Thunderbird) to regularly view and back up your email on your device. You can find explanations on the difference between downloading email with POP3 (to delete your email from the server) or IMAP (to keep it on the server) in [the official Thunderbird documentation](https://support.mozilla.org/en-US/kb/difference-between-imap-and-pop3). Most email services provide instructions on how to set up an email client to receive your mail with POP3 or IMAP."
msgstr "تاسې کولای شئ د Thunderbird په څېر د ایمیل پیرودونکي پروګرامونه وکاروئ ترڅو خپل ایمیل په منظمه توګه وګورئ او په خپله الکترونیکي وسیله کې یې بېک-اپ کړئ. د POP3 او IMAP ترمنځ توپیر په اړه د توضیحاتو لپاره د Thunderbird [رسمي لارښود](https://support.mozilla.org/en-US/kb/difference-between-imap-and-pop3)وګورئ. اکثره ایمیل خدمتونه د POP3 یا IMAP په مرسته د ایمیل ترلاسه کولو د تنظیم کولو څرنګوالي لارښوونې چمتو کوي."

#. type: Title #
#: src/files/backup/index.md:203
#, no-wrap
msgid "Scan and back up printed documents"
msgstr "چاپي اسناد لومړۍ سکن او بیا بېک-اپ کړئ"

#. type: Bullet: '- '
#: src/files/backup/index.md:206
msgid "When possible, scan (or photograph) all of your important papers. Back up the scans or photos along with your other electronic documents, as discussed above."
msgstr "کله چې ممکن وي، خپل ټول مهم اسناد سکین کړئ یا عکسونه یې واخلئ. د سکینونو یا عکسونو بېک-اپونه د خپلو نورو الکترونیکي اسنادو سره، لکه څنګه چې پورته تشریح شوي، وساتئ."

#. type: Title #
#: src/files/backup/index.md:207
#, no-wrap
msgid "Set a backup schedule"
msgstr "د بېک-اپ مهالویش جوړ کړئ"

#. type: Bullet: '- '
#: src/files/backup/index.md:210
msgid "To back up all of the data types listed above, you will need a combination of software and processes. Make sure that each data type is stored in at least two separate locations."
msgstr "د ډېټا ټول پورته ذکر شوي ډولونو د بېک-اپ کولو لپاره، تاسې د سافټویرونو او پروسو یو ترکیب ته اړتیا لرئ. ډاډ ترلاسه کړئ چې هر ډوله معلومات یا ډېټا لږ تر لږه په دوه بېلابېلو ځایونو کې ذخیره شوي دي."

#. type: Title #
#: src/files/backup/index.md:211
#, no-wrap
msgid "Practice recovery"
msgstr "د بېرته ترلاسه کولو (د فایلونو ریکوري) تمرین وکړئ"

#. type: Bullet: '- '
#: src/files/backup/index.md:214
msgid "Once you have made your backups, test to make sure that you know how to open files and use them again. Remember that, in the end, it is the restore procedure, not the backup procedure, that you really care about!"
msgstr "کله چې مو بېک-اپونه جوړ کړل، بیا ځان سره د حذف شوي ډېټا بېرته ترلاسه کولو تمرین وکړئ ترڅو د فایل خلاصولو او کارولو د مهارت څخه ډاډمن شئ . په یاد ولرئ چې یوازې د بېک-اپ جوړولو پروسه مهمه نه ده بلکه د حذف شوي فایلونو بېرته ترلاسه کول هم اړینه ده!"

#. type: Title #
#: src/files/backup/index.md:215
#, no-wrap
msgid "Establish procedures for coworkers"
msgstr "د همکارانو لپاره پروسې تنظیم کړئ"

#. type: Bullet: '- '
#: src/files/backup/index.md:218
msgid "If you are working with a team, write up and share procedures for all staff to reliably and securely back up files. Communicate the risks that losing your data could have to your ability to do your work. It may help to work with your colleagues to fill out a grid like the one above to identify all data you work with as a team."
msgstr "که له یو ټیم سره کار کوئ، د ټولو کارکوونکو لپاره پروسې ولیکئ او شریکې یې کړئ ترڅو په باوري او خوندي ډول فایلونه بېک-اپ کړي. د معلوماتو له لاسه ورکولو خطرات ورسره شریک کړئ چې د کار کولو وړتیا مو اغېزمنوي. ښایي د خپلو همکارانو سره په ګډه کار وکړئ ترڅو د پورته جدول په څېر یو جال ډک کړئ او ټول هغه معلومات مشخص کړئ چې تاسې یې د یوې ډلې په توګه کاروئ."

#. type: Title #
#: src/files/backup/index.md:219
#, no-wrap
msgid "Other considerations"
msgstr "نورې غورونې"

#. type: Plain text
#: src/files/backup/index.md:222
msgid "When you are making a backup plan, think of it in a larger perspective, asking yourself: \"How can I recover from a disaster and keep working?\""
msgstr "کله چې د بېک-اپ پلان جوړوئ، نو له یوې لوړې زاویې ورته وګورئ او له ځانه وپوښتئ: \"څنګه کولای شم له یوه ناورین وروسته بېرته کار پیل کړم او دوام ورکړم؟\""

#. type: Plain text
#: src/files/backup/index.md:224
msgid "Your plan should not just be about your files, but also about:"
msgstr "ستاسې پلان باید یوازې د فایلونو په اړه نه وي، بلکې همدارنګه:"

#. type: Bullet: '- '
#: src/files/backup/index.md:228
msgid "Software you use, and the licenses for it,"
msgstr "هغه سافټویرونه چې تاسې یې کاروئ، او د هغوی لایسنسونه،"

#. type: Bullet: '- '
#: src/files/backup/index.md:228
msgid "How you can replace equipment if it is lost, destroyed, or confiscated,"
msgstr "که الکترونیکي وسایل له لاسه ورکړئ، ویجاړ شي یا ونیول شي، څنګه یې بدیل موندلی شئ،"

#. type: Bullet: '- '
#: src/files/backup/index.md:228
msgid "Having a place you can go to to continue working in a crisis."
msgstr "د یوې داسې ځای درلودل چې په ناورین کې کار ته دوام ورکړئ."

#. type: Plain text
#: src/files/backup/index.md:229
msgid "Planning for this may mean setting aside money to recover from a loss. For example, you might consider writing this amount into a grant."
msgstr "دا ډول پلان جوړول په دې مانا ده چې تاسو باید د زیان جبرانولو او له خنډ څخه د وتلو لپاره یو مقدار پیسې ځانګړي کړی.. د بېلګې په توګه، تاسې کولای شې د زیان جبرانولو لپاره د مالي ملاتړ یو مقدار په ګرڼټ کې ذکر کړئ.."
