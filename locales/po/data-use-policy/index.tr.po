# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-05-17 04:09+0000\n"
"PO-Revision-Date: 2023-09-16 11:09+0000\n"
"Last-Translator: Levent Pişkin <leventpiskin@protonmail.com>\n"
"Language-Team: Turkish <https://weblate.securityinabox.org/projects/info/data-use-policy/tr/>\n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.12.2\n"

#. type: Yaml Front Matter Hash Value: title
#: src/data-use-policy/index.md:1
#, no-wrap
msgid "Data use policy"
msgstr "Veri Kullanım Politikası"

#. type: Title ##
#: src/data-use-policy/index.md:5
#, no-wrap, markdown-text
msgid "Why isn't this a \"Privacy Policy\"?"
msgstr "Neden \"Gizlilik Politikası\" değil?"

#. type: Plain text
#: src/data-use-policy/index.md:8
#, markdown-text
msgid "Many websites link to their “privacy policies” from their homepages, but what they actually tell you is how they plan to use your data. We believe that it is important to be transparent that this policy is a promise about how we will use the data you leave when you visit this site, but neither this promise nor the promise of any other site gives you full privacy."
msgstr "Pek çok web sitesi “gizlilik politikalarına” ana sayfalarından bağlantı verir ve aslında size söyledikleri sizin verilerinizi nasıl kullanmayı planladıklarıdır. Bu politikanın siteyi ziyaret ettiğinizde verilerinizi nasıl kullanacağımıza ilişkin şeffaflık sağlayan bir söz olarak bulunması önemlidir. Ancak ne bu söz ne de diğer sitelerinki size tam bir gizlilik sağlamaz."

#. type: Plain text
#: src/data-use-policy/index.md:10
#, markdown-text
msgid "The privacy policy is a compulsory legal disclosure of how the website operator collects, retains, and shares personally identifiable information. In other words, it’s often a list of ways your personal data are not private and under their control. There are legitimate reasons that people and organisations running websites need to keep data about your visits. We limit the data that we keep to only the data that we need for maintaining the site and satisfying legal and funding-related reporting requirements."
msgstr "Gizlilik politikası, web sitesi işleticisinin kişisel verilerinizi nasıl topladığı, sakladığı ve paylaştığına ilişkin yasal olarak zorunlu bir açıklamadır. Bir başka deyişle, genellikle kişisel verilerinizin gizli olmamasının ve onların kontrolünde olmasının yollarını açıklayan bir listedir. Web sitelerini işleten kişi ve kurumların web sitesini ziyarete dair verileri saklamasının meşru nedenleri vardır. Biz tuttuğumuz verileri yalnızca sitenin bakımı ve yasal ve finansal raporlama için ihtiyaç duyduğumuz verilerle sınırlandırıyoruz."

#. type: Title ##
#: src/data-use-policy/index.md:11
#, no-wrap, markdown-text
msgid "Visiting Security-in-a-Box"
msgstr "Security-in-a-Box'ı ziyaret etmek"

#. type: Plain text
#: src/data-use-policy/index.md:14
#, markdown-text
msgid "Securityinabox.org is operated by [Front Line Defenders](https://www.frontlinedefenders.org/), a non-profit operating from Dublin, Ireland and is governed by EU Data law."
msgstr "Securityinabox.org, kar amacı gütmeyen bir kuruluş olan ve İrlanda'nın Dublin şehrinde faaliyet gösteren [Front Line Defenders](https://www.frontlinedefenders.org/) tarafından işletilmektedir ve AB Veri yasalarına tâbidir."

#. type: Plain text
#: src/data-use-policy/index.md:16
#, markdown-text
msgid "When you visit this site, we collect information about your visit of the sort that web browsers and servers typically make available. If you don’t have Do not Track enabled, we may still collect the following anonymous/anonymised information when you visit our website:"
msgstr "Siteyi ziyaret ettiğinizde, ziyaretiniz hakkında web tarayıcıların ve sunucuların kullanıma sunduğu türden bilgileri topluyoruz. Takip Etme özelliğini etkinleştirmeseniz bile, siteyi ziyaret ettiğinizde aşağıdaki anonim/anonimleştirilmiş bilgileri toplayabiliriz:"

#. type: Bullet: '* '
#: src/data-use-policy/index.md:37
#, markdown-text
msgid "the time and date you visited the site"
msgstr "siteyi ziyaret ettiğiniz zaman ve tarih"

#. type: Bullet: '* '
#: src/data-use-policy/index.md:37
#, markdown-text
msgid "the first page you visit"
msgstr "ilk ziyaret ettiğiniz sayfa"

#. type: Bullet: '* '
#: src/data-use-policy/index.md:37
#, markdown-text
msgid "how many times you click on links"
msgstr "linklere kaç kere tıkladığınız"

#. type: Bullet: '* '
#: src/data-use-policy/index.md:37
#, markdown-text
msgid "how long you stayed on the site"
msgstr "ne kadar süre web sitesinde kaldığınız"

#. type: Bullet: '* '
#: src/data-use-policy/index.md:37
#, markdown-text
msgid "the last page you visit before leaving"
msgstr "ayrılmadan önce ziyaret ettiğiniz son sayfa"

#. type: Bullet: '* '
#: src/data-use-policy/index.md:37
#, fuzzy, markdown-text
#| msgid "what site brought you to the page (if you followed a link form another page, like on a search engine or social media)"
msgid "what site brought you to the page (if you followed a link from another page, like on a search engine or social media)"
msgstr "sizi sayfaya hangi sitenin yönlendirdiği (bir arama motoru veya sosyal medya gibi başka bir web sayfasındaki bağlantıya tıklayarak ziyaret ettiyseniz)"

#. type: Bullet: '* '
#: src/data-use-policy/index.md:37
#, markdown-text
msgid "what sites you visited through links on our site"
msgstr "sitemizdeki bağlantılar aracılığıyla hangi siteleri ziyaret ettiğiniz"

#. type: Bullet: '* '
#: src/data-use-policy/index.md:37
#, markdown-text
msgid "what you download from our site"
msgstr "sitemizden ne indirdiğiniz"

#. type: Bullet: '* '
#: src/data-use-policy/index.md:37
#, markdown-text
msgid "what keyword or keywords you used if you found the site through a search engine"
msgstr "siteyi bir arama motoru aracılığıyla bulduysanız hangi anahtar kelimeyi veya anahtar kelimeleri kullandığınız"

#. type: Bullet: '* '
#: src/data-use-policy/index.md:37
#, markdown-text
msgid "what browser you use"
msgstr "hangi web tarayıcıyı kullandığınız"

#. type: Bullet: '* '
#: src/data-use-policy/index.md:37
#, markdown-text
msgid "what operating system you use"
msgstr "hangi işletim sistemini kullandığınız"

#. type: Bullet: '* '
#: src/data-use-policy/index.md:37
#, markdown-text
msgid "what kind of device you use (computer vs smartphone vs tablet)"
msgstr "ne tür bir cihaz kullandığınız (bilgisayar veya akıllı telefon veya tablet)"

#. type: Bullet: '* '
#: src/data-use-policy/index.md:37
#, markdown-text
msgid "what brand device you use"
msgstr "hangi marka cihazı kullandığınız"

#. type: Bullet: '* '
#: src/data-use-policy/index.md:37
#, markdown-text
msgid "what model device you use"
msgstr "hangi model cihazı kullandığınız"

#. type: Bullet: '* '
#: src/data-use-policy/index.md:37
#, markdown-text
msgid "your country"
msgstr "bulunduğunuz ülke"

#. type: Bullet: '* '
#: src/data-use-policy/index.md:37
#, markdown-text
msgid "your IP address (We anonymise Visitors' IP addresses in our stats: we use this information to protect our servers from misuse. We do not use this information to identify Security-in-a-Box visitors.)"
msgstr "IP adresiniz (İstatistiklerimizde Ziyaretçilerin IP adreslerini anonimleştiririz: bu bilgi sunucularımızı kötüye kullanıma karşı korumak için kullanılır Security in a Box ziyaretçilerini tanımlamak için değil.)"

#. type: Bullet: '* '
#: src/data-use-policy/index.md:37
#, markdown-text
msgid "your screen resolution"
msgstr "ekran çözünürlüğünüz"

#. type: Bullet: '* '
#: src/data-use-policy/index.md:37
#, markdown-text
msgid "language of your browser"
msgstr "web tarayıcınızı hangi dilde kullandığınız"

#. type: Bullet: '* '
#: src/data-use-policy/index.md:37
#, markdown-text
msgid "number of times you have visited the site"
msgstr "siteyi kaç kere ziyaret ettiğiniz"

#. type: Bullet: '* '
#: src/data-use-policy/index.md:37
#, markdown-text
msgid "days since the last time you visited the site"
msgstr "son ziyaretinizden sonra geçen gün sayısı"

#. type: Plain text
#: src/data-use-policy/index.md:39
#, markdown-text
msgid "We do this to better understand how people use the site and so that we can fix problems with the site when they arise. From time to time, we also release reports that include very general information about who visits this site, like the total number of visitors or which countries or other websites comprise the most visits."
msgstr "İnsanların siteyi nasıl kullandığını daha iyi anlamak ve siteyle ilgili sorunlar ortaya çıktığında kullanmak için bunu yapıyoruz. Zaman zaman toplam ziyaretçi sayısı, en çok ziyaretin hangi ülkelerden veya hangi web sitelerinden geldiği gibi sitenin ziyaretçilerine ilişkin çok genel bilgileri içeren raporlar yayınlıyoruz."

#. type: Plain text
#: src/data-use-policy/index.md:41
#, markdown-text
msgid "We may collect statistics about the behaviour of visitors to the Security-in-a-Box. For instance, Security-in-a-Box may reveal how many visitors navigate from one page to another within the toolkit. We do this so that we can figure out how we can improve the toolkit and develop content that better suits your needs."
msgstr "Security-in-a-Box ziyaretçilerinin davranışları hakkında istatistik topluyoruz. Mesela Security-in-a-Box, araç takımı toolkit içinde kaç ziyaretçinin bir sayfadan diğerine geçtiğini bulabilir. Toolkit'i ve ihtiyaçlarınıza daha uygun içeriği nasıl geliştirebileceğimizi bulmak için yapıyoruz."

#. type: Plain text
#: src/data-use-policy/index.md:43
#, fuzzy, markdown-text
#| msgid "We collect this information using [open source analytics software](https://matomo.org/) that we host ourselves. This means that even the people who wrote the software do not see any of your information. This is done through a combination of server logs, javascript, and cookies. We need the server logs to protect this site and our servers, but you can block javascript using [NoScript](https://noscript.net/) and turn off or delete cookies and the site will continue to work correctly. We do not partner with third parties to collect this information, nor do we share information with third parties, with the exception of the release of non-personally-identifying information in formats described above."
msgid "We collect this information using [open-source analytics software](https://matomo.org/) that we host ourselves. This means that even the people who wrote the software do not see any of your information. This is done through a combination of server logs, javascript, and cookies. We need the server logs to protect this site and our servers, but you can block javascript using [NoScript](https://noscript.net/) and turn off or delete cookies and the site will continue to work correctly. We do not partner with third parties to collect this information, nor do we share information with third parties, with the exception of the release of non-personally-identifying information in formats described above."
msgstr "Bu bilgileri bizim sürücü hizmeti sağladığımız [açık kaynak analitik yazılımını](https://matomo.org/) kullanarak topluyoruz. Bu, yazılımı kodlayan kişilerin bile hiçbir bilginizi görmemesi anlamına gelir. Bu log kaydı, javascript ve çerezlerin bir kombinasyonu aracılığıyla yapılır. Los kayıtlarına web sitesini ve sunucularımızı koruma için ihtiyaç duyarız, ancak [NoScript](https://noscript.net/) kullanarak javascript'i engelleyebilir ve çerezleri silebilir veya kapatabilirsiniz ve bu durumda da site düzgün çalışmaya devam edecektir. Yukarıda açıklandığı şekliyle kişisel olarak tanımlayıcı olmayan bilgilerin yayınlanması dışında, bu bilgileri toplamak için üçüncü taraflarla çalışmıyoruz ve bu bilgileri üçüncü taraflarla paylaşmıyoruz."

#. type: Title ###
#: src/data-use-policy/index.md:45
#, no-wrap, markdown-text
msgid "I am using a VPN"
msgstr "VPN kullanıyorum"

#. type: Plain text
#: src/data-use-policy/index.md:48
#, markdown-text
msgid "VPNs can obscure your IP address and your country location, but some data may still be visible. If you use a VPN while visiting our website, and you haven’t enabled DNT, we would see all of the above information, save your IP address and country location, which would show the IP address and country of the exit point of your VPN."
msgstr "VPN Ip adresinizi ve ülke konumunuzu gizlese de bazı veriler hala görünür olabilir. Web sitemizi VPN kullanarak ziyaret ediyorsanız ancak DNT'yi etkinleştirmediyseniz, yukarıdaki tüm bilgileri görebiliriz, IP adresinizi ve VPN'in çıkış noktasındaki IP adresini ve bağlandığı ülkeyi gösteren ülke konumunuzu kaydedebiliriz."

#. type: Title ###
#: src/data-use-policy/index.md:49
#, no-wrap, markdown-text
msgid "I am actually using TOR"
msgstr "Aslında TOR kullanıyorum"

#. type: Plain text
#: src/data-use-policy/index.md:52
#, markdown-text
msgid "Just as when you are using a VPN, we can still access the above information, though the changing exit node IP addresses does add a complication. Furthermore, if we wanted to, we could go back through the exit node IP Addresses which are routinely published by Tor and determine if you are using TOR. But, that is something that we have never done and would not do. We will also see similar information if you visit this site through our Onion service ."
msgstr "VPN kullanırken olduğu gibi, yukarıdaki bilgilere hala erişebiliriz, ancak çıkış düğümü IP adreslerinin değişmesi işi biraz daha karmaşıklaştırır. Ayrıca Tor tarafından düzenli olarak yayınlanan çıkış düğümü IP adreslerine bakıp Tor kullanıp kullanmadığınızı anlayabiliriz. Ancak böyle bir şeyi asla yapmadık ve yapmayız. Keza siteyi Onion servisimiz aracılığıyla ziyaret ettiğinizde de benzer bilgileri görürüz."

#. type: Title ##
#: src/data-use-policy/index.md:54
#, no-wrap, markdown-text
msgid "Using the toolkit and Related Services"
msgstr "Toolkit'i ve İlgili Servisleri Kullanmak"

#. type: Plain text
#: src/data-use-policy/index.md:57
#, markdown-text
msgid "Occasionally, we may link to projects from Front Line Defenders, Tactical Tech, or other partners from this site."
msgstr "Bu siteden zaman zaman Front Line Defenders, Tactical Tech ve diğer ortakların projelerine bağlantı veriyoruz."

#. type: Plain text
#: src/data-use-policy/index.md:59
#, markdown-text
msgid "In the case of other projects hosted by Front Line Defenders, we may offers the opportunity to engaging in a way that requires collecting personally identifying information, like providing an email address so that we can send you updates that you have decided that you want to receive. In that case, Front Line Defenders collects such information only as is necessary or appropriate to fulfil the purpose of the visitor’s interaction. You can always refuse to supply your unique information or withdraw your consent at any time. This may prevent you from fully engaging in certain activities, but we try hard to make as much of our content available without any kind of barrier. Any way that you chose to engage with a Front Line Defenders site is voluntary and will not effect your use of Security-in-a-Box. Through your use of Security-in-a-Box itself, you will not be asked to provide personally identifying information."
msgstr "Front Line Defenders'ın yürüttüğü diğer projelerle alakalı olarak güncelleme almak istediğinizde, size bunları gönderebilmemiz için e-posta adresi gibi tanımlayıcı bilgilerin toplanmasını gerektiren durumlar söz konusu olabilir. Bu tür durumlada Front Line Defenders bu bilgileri ziyaretçinin etkileşim kurmak istediği amaçla sınırlı olarak ve gerekli veya uygun olduğu ölçüde toplar. Bu tür bilgileri vermeyi her daim reddedebilir veya onayınızı istediğiniz zaman geri alabilirsiniz. Bu belirli faaliyetlere katılmanızı engelleyebilse de site içeriğini mümkün olan en geniş ölçüde herhangi bir engel olmaksızın kullanmanız için çalışıyoruz. Front Line Defenders'ın web sitesi ile etkileşim kurmak için seçtiğiniz herhangi bir yöntem tamamen isteğe bağlı olup bu Security-in-a-Box'u kullanmanızı etkilemez. Security-in-a-Box kullandığınız süre boyunca sizden kişisel olarak herhangi bir tanımlayıcı bilgi vermeniz istenmeyecektir."

#. type: Plain text
#: src/data-use-policy/index.md:61
#, markdown-text
msgid "Projects and sites hosted by Front Line Defenders and other parters have their own data use policies and you should check the applicable policies if you have question about your data and those sites."
msgstr "Front Line Defenders ve diğer ortaklar tarafından yürütülen diğer proje ve web sitelerinin kendi veri kullanım politikaları vardır. Şayet verileriniz veya bu web siteleri hakkında sorularınız varsa, ilgili politika belgelerine bakmalısınız."

#. type: Title ##
#: src/data-use-policy/index.md:62
#, no-wrap, markdown-text
msgid "Changes to this Policy"
msgstr "Politika Belgesinde Değişiklik yapmak"

#. type: Plain text
#: src/data-use-policy/index.md:65
#, markdown-text
msgid "Although most changes are likely to be minor, we may change the Security-in-a-Box Privacy Policy from time to time, in response to legal and technical changes and at our discretion. We encourage visitors to frequently check this page for any changes to this Data Use Policy. Your continued use of this site after any change in this Data Use Policy constitutes your acceptance of these change."
msgstr "Değişikliklerin genelde önemsiz küçük ayrıntılara ilişkin olması kuvvetle muhtemel olsa da, yasal ve teknik değişiklikler ve kendi takdirimize bağlı olarak zaman zaman değişiklik yapılabilir. Ziyaretçilerin bu Veri Kullanım Politikası'nda herhangi bir değişiklik olup olmadığını görmeleri için sayfayı sıklıkla kontrol etmelerini öneririz zira Veri Kullanım Politikası'nda herhangi bir değişiklik yapıldıktan sonra siteyi kullanmaya devam etmeniz, yapılan değişiklikleri kabul ettiğiniz anlamına gelir."

#. type: Title ##
#: src/data-use-policy/index.md:66
#, no-wrap, markdown-text
msgid "Contacts"
msgstr "İletişim"

#. type: Plain text
#: src/data-use-policy/index.md:69
#, markdown-text
msgid "All questions related to the data use policies of Security-in-a-Box should be directed in writing to the [Front Line Defenders](mailto:info@frontlinedefenders.org)."
msgstr "Security-in-a-Box'ın veri kullanım politikalarıyla ilgili tüm soruları yazılı olarak [Front Line Defenders'a](mailto:info@frontlinedefenders.org) gönderilmelidir."

#. type: Title ##
#: src/data-use-policy/index.md:70
#, no-wrap, markdown-text
msgid "Reusing this Data Use Policy"
msgstr "Veri Kullanımı Politikasının Yeniden Kullanımı"

#. type: Plain text
#: src/data-use-policy/index.md:73
#, markdown-text
msgid "You are welcome to reuse and be inspired by this Data Use Policy after making sure it complies with the way your website tracks, uses and discloses users information."
msgstr "Web sitenizin buradaki kullanıcı bilgilerini izleme, kullanma ve açıklama politikalarına uygun olduğuna emin olduktan sonra Veri Kullanım Politikası belgesinden esinlenebilir ve kullanabilirsiniz."

#. type: Plain text
#: src/data-use-policy/index.md:74
#, markdown-text
msgid "_This document was last updated 13 October 2020._"
msgstr "_Bu belge en son 13 Ekim 2020'de güncellendi._"
