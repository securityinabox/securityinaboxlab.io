# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-12-12 21:02+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Yaml Front Matter Hash Value: license
#: src/tools/obscuracam/index.md:1
#, no-wrap
msgid "Free and Open Source Software"
msgstr ""

#. type: Yaml Front Matter Hash Value: logo
#: src/tools/obscuracam/index.md:1
#, no-wrap
msgid "../../../media/en/logos/obscuracam-logo.png"
msgstr ""

#. type: Yaml Front Matter Array Element: system_requirements
#: src/tools/obscuracam/index.md:1
#, no-wrap
msgid "Android 2.3.3 or higher"
msgstr ""

#. type: Yaml Front Matter Hash Value: title
#: src/tools/obscuracam/index.md:1
#, no-wrap
msgid "ObscuraCam for Android"
msgstr ""

#. type: Yaml Front Matter Hash Value: website
#: src/tools/obscuracam/index.md:1
#, no-wrap
msgid "https://guardianproject.info/apps/obscuracam/"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:20
#, markdown-text, no-wrap
msgid "**ObscuraCam** is a free camera application for Android devices, created by the [Guardian Project](https://guardianproject.info/), that has the ability to recognize and hide faces. It allows you to blur or delete the faces of those you photograph in order to protect their identities. \n"
msgstr ""

#. type: Title #
#: src/tools/obscuracam/index.md:21
#, markdown-text, no-wrap
msgid "Required reading"
msgstr ""

#. type: Bullet: '- '
#: src/tools/obscuracam/index.md:25
#, markdown-text
msgid "[Use mobile phones as securely as possible](../../mobile-phones)"
msgstr ""

#. type: Bullet: '- '
#: src/tools/obscuracam/index.md:25
#, markdown-text
msgid "[Use smartphones as securely as possible](../../smartphones)"
msgstr ""

#. type: Title #
#: src/tools/obscuracam/index.md:26
#, markdown-text, no-wrap
msgid "What you will get from this guide"
msgstr ""

#. type: Bullet: '- '
#: src/tools/obscuracam/index.md:30
#, markdown-text
msgid "The ability to **hide faces in photos taken with your Android device**."
msgstr ""

#. type: Bullet: '- '
#: src/tools/obscuracam/index.md:30
#, markdown-text
msgid "An easy way to **share or save these \"obscured\" photos**."
msgstr ""

#. type: Title #
#: src/tools/obscuracam/index.md:31
#, markdown-text, no-wrap
msgid "1. Introduction to ObscuraCam"
msgstr ""

#. type: Bullet: '- '
#: src/tools/obscuracam/index.md:37
#, markdown-text
msgid "**ObscuraCam**'s automatic face-recognition does not always work, but you can easily select and obscure faces manually."
msgstr ""

#. type: Bullet: '- '
#: src/tools/obscuracam/index.md:37
#, markdown-text
msgid "On some versions of the Android operating system, the option to *delete the original media file* does not work. If you rely on this option, please check to make sure that you do not have **ObscuraCam** photos (with visible faces!) somewhere on your device."
msgstr ""

#. type: Bullet: '- '
#: src/tools/obscuracam/index.md:37
#, markdown-text
msgid "If you use **ObscuraCam** to send photos to yourself or to someone else, the tool **does not** provide additional protection (such as end-to-end encryption) for the photo in transit."
msgstr ""

#. type: Bullet: '- '
#: src/tools/obscuracam/index.md:37
#, markdown-text
msgid "A **tag** is an area in the photo which has been obscured by one of several methods **ObscuraCam** can use."
msgstr ""

#. type: Title ##
#: src/tools/obscuracam/index.md:40
#, markdown-text, no-wrap
msgid "1.0 Other tools like ObscuraCam"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:43
#, markdown-text
msgid "Alternative software that lets you pixelate, blur or redact faces from images:"
msgstr ""

#. type: Bullet: '* '
#: src/tools/obscuracam/index.md:45
#, markdown-text
msgid "Windows: [Gimp](http://www.gimp.org/), [Pinta](http://pinta-project.com/)"
msgstr ""

#. type: Bullet: '* '
#: src/tools/obscuracam/index.md:47
#, markdown-text
msgid "OS X: [Gimp](http://www.gimp.org/), [Pinta](http://pinta-project.com/)"
msgstr ""

#. type: Bullet: '* '
#: src/tools/obscuracam/index.md:49
#, markdown-text
msgid "Linux: [Gimp](http://www.gimp.org/), [Pinta](http://pinta-project.com/)"
msgstr ""

#. type: Title #
#: src/tools/obscuracam/index.md:51
#, markdown-text, no-wrap
msgid "2. Install and configure ObscuraCam"
msgstr ""

#. type: Title ##
#: src/tools/obscuracam/index.md:53
#, markdown-text, no-wrap
msgid "2.1 Install ObscuraCam"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:56
#, markdown-text, no-wrap
msgid "**Step 1.** On your Android device, **download** and **install** the app from the [Google Play](https://play.google.com/store/apps/details?id=org.thoughtcrime.securesms) store by tapping ![](../../../media/en/obscuracam/obscuracam_001.png).\n"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:58
#, markdown-text
msgid "![](../../../media/en/obscuracam/obscuracam_002.png)"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:60
#, markdown-text, no-wrap
msgid "*Figure 1: K-9 on the Google Play Store*\n"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:62
#, markdown-text, no-wrap
msgid "**Step 2:**. Before the installation process begins, you will be prompted to review the access that the application will have on your phone. Review this carefully. Once your are happy with the permissions that will be granted, press ![](../../../media/en/obscuracam/obscuracam_003.png) and the installation will complete.  If you do not agree with the permissions that will be granted, press the back button and the installation will be cancelled.\n"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:64
#, markdown-text
msgid "![](../../../media/en/obscuracam/obscuracam_004.png)"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:67
#, markdown-text, no-wrap
msgid ""
"*Figure 2: Permissions required*\n"
"**Step 3.** **Tap** *Open* to run the app for the first time\n"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:69
#, markdown-text, no-wrap
msgid "**Step 4:** Review the *Terms of use* and if you are happy with them press ![](../../../media/en/obscuracam/obscuracam_005.png) to be brought to the main screen of **ObscuraCam**. Otherwise press ![](../../../media/en/obscuracam/obscuracam_006.png) to stop using app.\n"
msgstr ""

#. type: Title #
#: src/tools/obscuracam/index.md:71
#, markdown-text, no-wrap
msgid "3. Using ObscuraCam to obscure photos"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:74
#, markdown-text
msgid "You can use **ObscuraCam** to hide some or all of the faces that appear in your photos. It works on pictures that you take with the **ObscuraCam** app itself, but you can also blur faces in other photos, as long as you have a way to copy or move the image files onto your Android device."
msgstr ""

#. type: Title ##
#: src/tools/obscuracam/index.md:75
#, markdown-text, no-wrap
msgid "3.1 Taking And Obscuring New Photos"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:78
#, markdown-text
msgid "To take a photo on your Android device without showing your subjects' faces, perform the following steps:"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:80
#, markdown-text, no-wrap
msgid "**Step 1:** From the main screen of **ObscuraCam** *tap* ![](../../../media/en/obscuracam/obscuracam_007.png).\n"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:82
#, markdown-text
msgid "![](../../../media/en/obscuracam/obscuracam_008.png)"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:84
#, markdown-text, no-wrap
msgid "*Figure 3: ObscuraCam home screen*\n"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:86
#, markdown-text, no-wrap
msgid "**Step 2:** Your phone's camera app will open, take a photo as you normally would.\n"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:88
#, markdown-text, no-wrap
msgid "**Step 3:** A screen with a preview of the photo you took will be displayed. **Tap** ![](../../../media/en/obscuracam/obscuracam_009.png) to proceed with obscuring the photo, otherwise **tap** ![](../../../media/en/obscuracam/obscuracam_010.png) to discard the current photo and take another.\n"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:90
#, markdown-text, no-wrap
msgid "**Step 4:**ObscuraCam** will attempt to identify faces automatically. \n"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:92
#, markdown-text
msgid "![](../../../media/en/obscuracam/obscuracam_011.png)"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:94
#, markdown-text, no-wrap
msgid "*Figure 4: ObscuraCam Detecting Faces* \n"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:97
#, markdown-text, no-wrap
msgid "**Step 5:** If **ObscuraCam** can detect faces in the photo it will add an image \"*tag*\" (a rectangle used to select content that should be hidden) over the face and pixelate it. If **ObscuraCam** did not detect any faces or you need to obscure additional areas on the photo, tap on the area to add additional tags.\n"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:99
#, markdown-text, no-wrap
msgid "**Note:** To learn how to modify or add additional blurring or effects see  section [4. Modifying the \"blur\" mode]( #modifying-the-blur-mode) below.\n"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:101 src/tools/obscuracam/index.md:131
#, markdown-text
msgid "![](../../../media/en/obscuracam/obscuracam_012.png)"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:103
#, markdown-text, no-wrap
msgid "*Figure 5: ObscuraCam - blurred face* \n"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:105
#, markdown-text, no-wrap
msgid "**Step 6:** To see a preview of the photo you can tap ![](../../../media/en/obscuracam/obscuracam_013.png), to return to the edit screen tap anywhere on the preview image.\n"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:107
#, markdown-text, no-wrap
msgid "**Step 7:** To share the obscured photo via an existing application on your phone such as [K-9 Mail](../../k9/android) or [TextSecure](../../textsecure/android) tap ![](../../../media/en/obscuracam/obscuracam_014.png).\n"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:109
#, markdown-text, no-wrap
msgid "**Step 8:** To save the blurred photo to your phone tap ![](../../../media/en/obscuracam/obscuracam_015.png).  You will be asked whether you want to delete the original, unblurred, image from your phone, if you no longer need the original photo tap ![](../../../media/en/obscuracam/obscuracam_016.png) otherwise tap  ![](../../../media/en/obscuracam/obscuracam_017.png) to save a copy of both the obscured and unobscured photo.\n"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:111
#, markdown-text
msgid "![](../../../media/en/obscuracam/obscuracam_018.png)"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:113
#, markdown-text, no-wrap
msgid "*Figure 6: Confirm deletion of original image* \n"
msgstr ""

#. type: Title ##
#: src/tools/obscuracam/index.md:115
#, markdown-text, no-wrap
msgid "3.2 Obscuring Existing Photos"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:118
#, markdown-text
msgid "As well as obscuring photos as you take them, **ObscuraCam** can also obscure any old photos on your phone or that you copy to your phone."
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:120
#, markdown-text, no-wrap
msgid "**Step 1:** From the main screen of **ObscuraCam** *tap* ![](../../../media/en/obscuracam/obscuracam_019.png).\n"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:122
#, markdown-text, no-wrap
msgid "**Step 2:** Browse to the file on your phone that you want to obscure.\n"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:124
#, markdown-text, no-wrap
msgid "**Step 3:** **ObscuraCam** will attempt to detect any faces in the image (See Figure 6) and pixelate them.\n"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:126
#, markdown-text, no-wrap
msgid "**Step 4:** If **ObscuraCam** did not detect any faces or you want to obscure additional faces or items in the photo tap the relevant area to add additional pixelation. \n"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:129
#, markdown-text, no-wrap
msgid "**Note:** To learn how to modify or add additional blurring or effects see  section [Modifying the \"blur\" mode]( #modifying-the-blur-mode) below.\n"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:133
#, markdown-text, no-wrap
msgid "*Figure 7: ObscuraCam - blurred face* \n"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:135
#, markdown-text, no-wrap
msgid "**Step 5:** To see a preview of the photo you can tap ![](../../../media/en/obscuracam/obscuracam_013.png), to return to the edit screen tap anywhere on the preview image.\n"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:137
#, markdown-text, no-wrap
msgid "**Step 6:** To share the obscured photo via an existing application on your phone such as [K-9 Mail](../../k9/android) or [TextSecure](../../textsecure/android) tap ![](../../../media/en/obscuracam/obscuracam_014.png).\n"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:139
#, markdown-text, no-wrap
msgid "**Step 7:** To save the blurred photo to your phone tap ![](../../../media/en/obscuracam/obscuracam_015.png).  You will be asked whether you want to delete the original, unblurred, image from your phone, if you no longer need the original photo tap ![](../../../media/en/obscuracam/obscuracam_016.png) otherwise tap  ![](../../../media/en/obscuracam/obscuracam_017.png) to save a copy of both the obscured and unobscured photo.\n"
msgstr ""

#. type: Title #
#: src/tools/obscuracam/index.md:141
#, markdown-text, no-wrap
msgid "4. Modifying the blur mode"
msgstr ""

#. type: Title ##
#: src/tools/obscuracam/index.md:147
#, markdown-text, no-wrap
msgid "4.1 Resize or move the tag"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:150
#, markdown-text
msgid "While editing a photo in **ObscuraCam**, if you need to resize the tag, touch one of the four corners of the white outline box so that it turns green and drag it to make it larger or smaller."
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:152
#, markdown-text
msgid "If you need to move the tag so that it is in a better position on the photo, touch the centre of the pixelated area so that the surrounding white box turns green, then drag the box to the location you want."
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:154
#, markdown-text
msgid "![](../../../media/en/obscuracam/obscuracam_020.png)"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:156
#, markdown-text, no-wrap
msgid "*Figure 8: Pixelated tag ready for resizing or moving* \n"
msgstr ""

#. type: Title ##
#: src/tools/obscuracam/index.md:157
#, markdown-text, no-wrap
msgid "4.2 Change the obscure type"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:160
#, markdown-text
msgid "ObscuraCam gives you a few options on how to obscure a photo.  From the edit screen tap on an existing obscured area to bring up the obscuring options:"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:162
#, markdown-text
msgid "![](../../../media/en/obscuracam/obscuracam_021.png)"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:164
#, markdown-text, no-wrap
msgid "*Figure 9: Obscuring options* \n"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:167
#, markdown-text, no-wrap
msgid "**Pixelate** will obscure any part of the photo inside the tag by pixelating it.\n"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:169
#, markdown-text
msgid "![](../../../media/en/obscuracam/obscuracam_022.png)"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:171
#, markdown-text, no-wrap
msgid "*Figure 10: Pixelated area* \n"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:173
#, markdown-text, no-wrap
msgid "**Redact** will obscure any part of the photo inside the tag by placing a black box over it.\n"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:175
#, markdown-text
msgid "![](../../../media/en/obscuracam/obscuracam_023.png)"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:177
#, markdown-text, no-wrap
msgid "*Figure 11: Redacted area*\n"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:179
#, markdown-text, no-wrap
msgid "**Mask** will obscure any part of the photo inside the tag by placing a funny mask over the person.\n"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:181
#, markdown-text
msgid "![](../../../media/en/obscuracam/obscuracam_024.png)"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:183
#, markdown-text, no-wrap
msgid "*Figure 12: Mask* \n"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:185
#, markdown-text, no-wrap
msgid "**Crowd Pixel** will obscure any part of the photo *outside* the tag by pixelating the it.\n"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:187
#, markdown-text
msgid "![](../../../media/en/obscuracam/obscuracam_025.png)"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:189
#, markdown-text, no-wrap
msgid "*Figure 13: Crowd Pixel* \n"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:191
#, markdown-text, no-wrap
msgid "**Clear Tag** will remove the selected tag from the photo.\n"
msgstr ""

#. type: Title #
#: src/tools/obscuracam/index.md:193
#, markdown-text, no-wrap
msgid "5. Obscuring Videos"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:196
#, markdown-text, no-wrap
msgid "**Obscuracam** can also obscure faces or objects in videos, however unlike with photos it can only obscure existing photos that you took with your phone or copied to your phone.\n"
msgstr ""

#. type: Title ##
#: src/tools/obscuracam/index.md:197
#, markdown-text, no-wrap
msgid "5.1 Automatic Obscuring of Videos"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:200 src/tools/obscuracam/index.md:232
#, markdown-text, no-wrap
msgid "**Step 1:** From the main screen (*See figure 3*) of **Obscuracam** tap ![](../../../media/en/obscuracam/obscuracam_026.png).\n"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:202 src/tools/obscuracam/index.md:234
#, markdown-text, no-wrap
msgid "**Step 2:** Select the video you with to obscure from your phones storage.\n"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:204
#, markdown-text, no-wrap
msgid "**Step 3** Tap ![](../../../media/en/obscuracam/obscuracam_027.png) when asked *Would you like to detect faces in this video*.\n"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:206
#, markdown-text, no-wrap
msgid "**Step 3:** The video will begin to play and **ObscuraCam** will attempt to detect faces.\n"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:208
#, markdown-text, no-wrap
msgid "**Note:** The process for detecting faces takes approximately *4 seconds* for every *1 second* of the video.\n"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:210
#, markdown-text
msgid "![](../../../media/en/obscuracam/obscuracam_028.png)"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:212
#, markdown-text, no-wrap
msgid "*Figure 14: Detecting faces* \n"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:214
#, markdown-text, no-wrap
msgid "**Step 4:** Once the face detection has completed, you can tap ![](../../../media/en/obscuracam/obscuracam_029.png) to review the video. This will show you the pixelated tags over the faces as well as a green trail showing the path the tag takes through the video as the face moves.\n"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:216
#, markdown-text
msgid "![](../../../media/en/obscuracam/obscuracam_030.png)"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:218
#, markdown-text, no-wrap
msgid "*Figure 15: Detected faces*\n"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:220
#, markdown-text, no-wrap
msgid "**Step 5:** After reviewing the video and you are happy that all faces are obscured, tap ![](../../../media/en/obscuracam/obscuracam_031.png) at the top of the screen to save the video to your phones storage.\n"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:222
#, markdown-text
msgid "![](../../../media/en/obscuracam/obscuracam_032.png)"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:224
#, markdown-text, no-wrap
msgid "*Figure 16: Saving your video*\n"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:226
#, markdown-text, no-wrap
msgid "**Note:** If **ObscuraCam** did not properly obscure faces or failed to detect some, you can see how to manually obscure them in section [Manual Obscuring of Videos](#manual-obscuring-of-videos).\n"
msgstr ""

#. type: Title ##
#: src/tools/obscuracam/index.md:227
#, markdown-text, no-wrap
msgid "5.2 Manual Obscuring of Videos"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:230
#, markdown-text
msgid "If **ObscuraCam** was not able to detect faces properly during the automatic process or you would like more process over the obscuring process you can manually obscure video."
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:236
#, markdown-text, no-wrap
msgid "**Step 3:** Tap ![](../../../media/en/obscuracam/obscuracam_033.png) when asked *Would you like to detect faces in this video*.\n"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:238
#, markdown-text, no-wrap
msgid "**Step 4:** Tap ![](../../../media/en/obscuracam/obscuracam_029.png) to start playing the video.\n"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:240
#, markdown-text, no-wrap
msgid "**Step 5:**  While the video plays, touch and hold your finger on a face in the video to add a tag over it,  drag the tag around the screen with your finger following the movement of the face. Lift your finger off the screen to stop obscuring the video.\n"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:241
#, markdown-text, no-wrap
msgid "**Note:** as you follow the face on the screen you will see a green trail being left behind, this is the path the pixelated tag will take once the video is saved.  \n"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:244
#, markdown-text, no-wrap
msgid "**Step 6:** If there are additional faces in the video that you wish to obscure, touch and hold ![](../../../media/en/obscuracam/obscuracam_034.png) and move the slider back to the time point where the next face appears on video and repeat *step 5* above.\n"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:246
#, markdown-text, no-wrap
msgid "**Step 7:** Once you have finished manually obscuring faces, you can tap ![](../../../media/en/obscuracam/obscuracam_029.png) to review the video. This will show you the pixelated tags over the faces as well as a green trail  showing the path the tag takes through the video as the face moves.\n"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:248
#, markdown-text, no-wrap
msgid "**Step 8:** After reviewing the video and you are happy that all faces are obscured, tap ![](../../../media/en/obscuracam/obscuracam_031.png) at the top of the screen to save the video to your phones storage.\n"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:250
#, markdown-text
msgid "![](../../../media/en/obscuracam/obscuracam_035.png)"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:252
#, markdown-text, no-wrap
msgid "*Figure 17: Obscured section of video*\n"
msgstr ""

#. type: Title ##
#: src/tools/obscuracam/index.md:253
#, markdown-text, no-wrap
msgid "5.3 Changing Output Video Quality"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:255
#, markdown-text, no-wrap
msgid "**ObscuraCam** chooses a default video quality when saving that make the video small enough to easily share via email or other methods but does reduce resolution and quality of the video.  \n"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:258
#, markdown-text
msgid "To keep the obscured video in a higher resolution:"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:260
#, markdown-text, no-wrap
msgid "**Step 1:** After either [automatically](#automatic-obscuring-of-videos) or [manually](#manual-obscuring-of-videos) detecting faces and before saving the video, tap ![](../../../media/en/obscuracam/obscuracam_036.png) to bring up the menu and select ![](../../../media/en/obscuracam/obscuracam_037.png).\n"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:262
#, markdown-text, no-wrap
msgid "**Step 2:** Review and change the settings you want, where possibly **ObscuraCam** will give you the range by which each setting can be changed.\n"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:264
#, markdown-text
msgid "![](../../../media/en/obscuracam/obscuracam_038.png)"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:266
#, markdown-text, no-wrap
msgid "*Figure 18: Video output/saving preferences*\n"
msgstr ""

#. type: Plain text
#: src/tools/obscuracam/index.md:267
#, markdown-text, no-wrap
msgid "**Step 3:** Once you have made the desired output changes, tap the back button on your phone and save the video.\n"
msgstr ""
