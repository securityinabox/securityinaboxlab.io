# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-03-06 00:24+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: hi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Yaml Front Matter Hash Value: license
#: src/tools/k9/index.md:1
#, no-wrap
msgid "Free and Open Source Software"
msgstr ""

#. type: Yaml Front Matter Hash Value: logo
#: src/tools/k9/index.md:1
#, no-wrap
msgid "../../../media/en/logos/k9-logo.png"
msgstr ""

#. type: Yaml Front Matter Array Element: system_requirements
#: src/tools/k9/index.md:1
#, no-wrap
msgid "Android 1.5 or higher"
msgstr ""

#. type: Yaml Front Matter Array Element: system_requirements
#: src/tools/k9/index.md:1
#, no-wrap
msgid "APG must be installed before installing K-9"
msgstr ""

#. type: Yaml Front Matter Hash Value: title
#: src/tools/k9/index.md:1
#, no-wrap
msgid "K9 with APG for Android"
msgstr ""

#. type: Yaml Front Matter Hash Value: website
#: src/tools/k9/index.md:1
#, no-wrap
msgid "https://code.google.com/p/k9mail"
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:21
#, no-wrap, markdown-text
msgid "**K-9 Mail** is a free and open source email client for Android devices, that integrates seamlessly with **Android Privacy Guard**. \n"
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:23
#, no-wrap, markdown-text
msgid "**Android Privacy Guard (APG)** is a free and open source application that lets you encrypt, decrypt and sign files, messages or emails using Public Key Encryption (like OpenPGP) or encrypt/decrypt files or messages with symmetric encryption, securing them with a password.\n"
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:25
#, markdown-text
msgid "The use of these two tools allows for easy encrypting and decrypting of email messages."
msgstr ""

#. type: Title #
#: src/tools/k9/index.md:26
#, no-wrap, markdown-text
msgid "Required reading"
msgstr ""

#. type: Bullet: '- '
#: src/tools/k9/index.md:31
#, markdown-text
msgid "[Keep your online communication private](../../../communication/private-communication/)"
msgstr ""

#. type: Bullet: '- '
#: src/tools/k9/index.md:31
#, markdown-text
msgid "[Use mobile phones as securely as possible](../../mobile-phones)"
msgstr ""

#. type: Bullet: '- '
#: src/tools/k9/index.md:31
#, markdown-text
msgid "[Use smartphones as securely as possible](../../smartphones)"
msgstr ""

#. type: Title #
#: src/tools/k9/index.md:32
#, no-wrap, markdown-text
msgid "What you will get from this guide"
msgstr ""

#. type: Bullet: '- '
#: src/tools/k9/index.md:35
#, markdown-text
msgid "The ability to **use encrypted email on your Android device**"
msgstr ""

#. type: Title #
#: src/tools/k9/index.md:36
#, no-wrap, markdown-text
msgid "1. Introduction to K9"
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:39
#, no-wrap, markdown-text
msgid "**K-9 Mail** is a comprehensive email client that will allow you to send and receive email from one or more email accounts, as well as send and receive them securely when used with [**APG**](../../apg/android) to encrypt the contents.\n"
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:41
#, no-wrap, markdown-text
msgid "**Note:** Even when you are using encryption for your emails, it is important to remember that the *recipients* and the *subject* can not be hidden from anyone monitoring your email.\n"
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:43
#, markdown-text
msgid "Before you start using **K-9 Mail** you will need:"
msgstr ""

#. type: Bullet: '- '
#: src/tools/k9/index.md:48
#, markdown-text
msgid "to have an internet connection on your phone."
msgstr ""

#. type: Bullet: '- '
#: src/tools/k9/index.md:48
#, markdown-text
msgid "to have an email account that supports either secure POP3 or IMAP connections."
msgstr ""

#. type: Bullet: '- '
#: src/tools/k9/index.md:48
#, markdown-text
msgid "to have an OpenPGP key-pair and public keys of the people with whom you wish to communicate with. See the [**APG**](../../apg/android) guide, for setting up your key pair."
msgstr ""

#. type: Bullet: '- '
#: src/tools/k9/index.md:48
#, markdown-text
msgid "to be familiar with the concept of public/private key encryption. See the [***Using public key encryption in email***](../../communication/private-communication/#using-public-key-encryption-in-email) section of [***Keep your online communication private***](../../communication/private-communication/) and the [***Thunderbird with Enigmail and GPG***](../../thunderbird/windows) guide."
msgstr ""

#. type: Title ##
#: src/tools/k9/index.md:49
#, no-wrap, markdown-text
msgid "1.0 Other tools like K9 Mail"
msgstr ""

#. type: Bullet: '- '
#: src/tools/k9/index.md:57
#, markdown-text
msgid "[**Thunderbird**](../../thunderbird/windows) with Enigmail and GPG Microsoft Windows, Mac OS and GNU Linux."
msgstr ""

#. type: Bullet: '- '
#: src/tools/k9/index.md:57
#, markdown-text
msgid "[**Mail.app**](https://www.apple.com/osx/apps/#mail) and [GPG Tools](https://gpgtools.org/) on Mac OS."
msgstr ""

#. type: Bullet: '- '
#: src/tools/k9/index.md:57
#, markdown-text
msgid "[**Claws Mail**](http://www.claws-mail.org/) on Windows and Linux."
msgstr ""

#. type: Bullet: '- '
#: src/tools/k9/index.md:57
#, markdown-text
msgid "[**gpg4usb**](../../gpg4usb/windows) for Microsoft Windows and GNU Linux."
msgstr ""

#. type: Bullet: '- '
#: src/tools/k9/index.md:57
#, markdown-text
msgid "[**Mailvelope**](https://www.mailvelope.com/) for Microsoft Windows, Mac OS and GNU Linux."
msgstr ""

#. type: Bullet: '- '
#: src/tools/k9/index.md:57
#, markdown-text
msgid "[**Mailpile**](https://www.mailpile.is/) for Microsoft Windows, Mac OS and GNU Linux."
msgstr ""

#. type: Title #
#: src/tools/k9/index.md:58
#, no-wrap, markdown-text
msgid "2. Install and configure K9 Mail"
msgstr ""

#. type: Title ##
#: src/tools/k9/index.md:60
#, no-wrap, markdown-text
msgid "2.1 Install K9 Mail"
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:63
#, no-wrap, markdown-text
msgid "**Note:** if you want to be able to send encrypted email, you should install [**APG**](../../apg/android) before installing K-9 Mail.\n"
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:65
#, no-wrap, markdown-text
msgid "**Step 1.** On your Android device, **download** and **install** the app from the [Google Play](https://play.google.com/store/apps/details?id=com.fsck.k9) store by tapping ![](../../../media/en/k9/k9-en-and-001.png).\n"
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:67
#, markdown-text
msgid "![](../../../media/en/k9/k9-en-and-002.png)"
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:69
#, no-wrap, markdown-text
msgid "*Figure 1: K-9 Mail on the Google Play Store*\n"
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:71
#, no-wrap, markdown-text
msgid "**Step 2:**. Before the installation process begins, you will be prompted to review the access that the application will have on your phone. Review this carefully. Once your are happy with the permissions that will be granted, tap ![](../../../media/en/k9/k9-en-and-003.png) and the installation will complete.  If you do not agree with the permissions that will be granted, tap the back button and the installation will be cancelled.\n"
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:73
#, markdown-text
msgid "![](../../../media/en/k9/k9-en-and-004.png)"
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:75
#, no-wrap, markdown-text
msgid "*Figure 2: Permissions required*\n"
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:77
#, no-wrap, markdown-text
msgid "**Step 3.** **Tap** *Open* to run the app for the first time.\n"
msgstr ""

#. type: Title ##
#: src/tools/k9/index.md:79
#, no-wrap, markdown-text
msgid "2.2 Configure K9 Mail automatically"
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:82
#, markdown-text
msgid "After installing **K-9 Mail** and running it for the first time you will be presented with a welcome screen describing the features of the mail client.  Press ![](../../../media/en/k9/k9-en-and-005.png) to begin the account setup."
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:84
#, markdown-text
msgid "Where possible, **K-9 Mail** will attempt to configure your account automatically.  If this is not possible, or if you wish to have more control over the account setup process, you can also [**configure it manually**](#configure-k9-mail-manually)."
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:87
#, no-wrap, markdown-text
msgid "**Step 1:** Enter your email address and email password in the fields provided and tap ![](../../../media/en/k9/k9-en-and-005.png) \n"
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:89
#, markdown-text
msgid "![](../../../media/en/k9/k9-en-and-006.png)"
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:91
#, no-wrap, markdown-text
msgid "*Figure 3: Adding email credentials*\n"
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:93
#, no-wrap, markdown-text
msgid "**Step 2:** **K-9 Mail** will connect to the internet and attempt to get your account settings.\n"
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:95
#, no-wrap, markdown-text
msgid "**Step 3**: Once the settings have been retrieved you will be asked to enter your name as you want it to be displayed on all outgoing email and to give the account a name.  The account name will allow you to distinguish between multiple accounts, should you want to add more.  Tap ![](../../../media/en/k9/k9-en-and-007.png) to complete the account setup.\n"
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:97
#, markdown-text
msgid "![](../../../media/en/k9/k9-en-and-008.png)"
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:99
#, no-wrap, markdown-text
msgid "*Figure 4: K-9 Mail account setup*\n"
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:101
#, no-wrap, markdown-text
msgid "**Step 4:** **K-9 Mail** will display changes to the program since the last version, tap ![](../../../media/en/k9/k9-en-and-009.png) to dismiss this window and be brought to your mail account.\n"
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:103
#, markdown-text
msgid "![](../../../media/en/k9/k9-en-and-010.png)"
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:105
#, no-wrap, markdown-text
msgid "*Figure 5: Email accounts*\n"
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:107
#, no-wrap, markdown-text
msgid "**Step 5:** To make sure the account is working in **K-9 Mail**, **send** yourself an email from your computer and download it from the **K-9 Mail** client. \n"
msgstr ""

#. type: Title ##
#: src/tools/k9/index.md:109
#, no-wrap, markdown-text
msgid "2.3 Configure K9 Mail manually"
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:112
#, no-wrap, markdown-text
msgid "**Step 1:** Enter your email address and email password in the fields provided (see *Fig 3*) and tap ![](../../../media/en/k9/k9-en-and-011.png) \n"
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:114
#, no-wrap, markdown-text
msgid "**Step 2:** Select the account type your email is (IMAP/POP/Exchange) and tap the relevant button.\n"
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:116
#, no-wrap, markdown-text
msgid "**Note:** you will need to refer to your email client settings on your computer to know what account type your email server uses.\n"
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:118
#, markdown-text
msgid "![](../../../media/en/k9/k9-en-and-012.png)"
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:120
#, no-wrap, markdown-text
msgid "*Figure 6: Account type selection*\n"
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:122
#, no-wrap, markdown-text
msgid "**Step 3:** Next are the incoming server settings. If unsure, refer to the email client on your computer for settings. Always ensure that the *security type* is set to either *STARTTLS* or *SSL/TLS*. **Never** use the *none* option.\n"
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:124
#, markdown-text
msgid "![](../../../media/en/k9/k9-en-and-013.png)"
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:126
#, no-wrap, markdown-text
msgid "*Figure 7: Incoming Server Settings*\n"
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:128
#, no-wrap, markdown-text
msgid "**Step 4.** **K-9 Mail** will then connect to your mail server to check if your settings are correct. It might display a warning about the certificate of your secured connection. *Do not ignore this!* This is the only time you can verify that the certificate really belongs to your mail server. If you ignore this, you can not be sure if you are not subject to a *Man-in-the-Middle attack*, and your communications could be intercepted. You can see a SHA-1 fingerprint at the very end of the warning. Either **check** on your computer if the installed certificate from your mail server has the same fingerprint, or find a way to check your mail server's certificate directly from your provider. \n"
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:130
#, no-wrap, markdown-text
msgid "**Step 5.** **K-9 Mail** asks you to configure your outgoing server settings. Again, **ensure** that *Security Type* is *STARTTLS* or *SSL/TLS*. For all additional settings, **check** your computer's email client or the settings of your email provider.\n"
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:132
#, markdown-text
msgid "![](../../../media/en/k9/k9-en-and-014.png)"
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:134
#, no-wrap, markdown-text
msgid "*Figure 8: Outgoing server settings*\n"
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:136
#, no-wrap, markdown-text
msgid "**Step 6.** **K-9 Mail** now asks you how often you want it to automatically poll for email. **Set** the option to *never* and uncheck *enable push mail for this account*, if you only want to receive email when you manually check, otherwise leave the settings as they are to automatically receive email as they arrive to your account.\n"
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:138
#, markdown-text
msgid "![](../../../media/en/k9/k9-en-and-015.png)"
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:140
#, no-wrap, markdown-text
msgid "*Figure 9: Poll frequency*\n"
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:142
#, no-wrap, markdown-text
msgid "**Step 7.** The last pieces of information to provide are a nickname for the email account which will be displayed in **K-9 Mail** and to set up the name you wish to be displayed on all outgoing email. (See *Figure 4*)\n"
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:144
#, no-wrap, markdown-text
msgid "**Step 8:** To make sure the account is working in **K-9 Mail**, **send** yourself an email from your computer and download it from the **K-9 Mail** client. \n"
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:147
#, markdown-text
msgid "We recommend that you use **K-9 Mail** only in addition to your computer's email client. Therefore it is important that when you download email with your Android phone, it does not delete the email on the server, since you want to receive the email later with your computer, too. By default, **K-9 Mail** is set up this way, but you may want to learn more about the settings which can be found in *accounts*; this can be reached by long pressing on the account you have just set up and selecting *account settings* from the menu. You may also wish to check the *fetching mail* and *sending mail* account option for settings."
msgstr ""

#. type: Title #
#: src/tools/k9/index.md:149
#, no-wrap, markdown-text
msgid "3. Send and receive encrypted email with K9"
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:152
#, markdown-text
msgid "One of the main benefits of using **K-9 Mail** over other email clients is that it lets you send and receive *GPG* encrypted email.  Before you can start sending and receiving encrypted email, you need to ensure that you have all your OpenPGP keys imported into APG.  To learn more about this, read the [**APG**](../../apg/android) guide."
msgstr ""

#. type: Title ##
#: src/tools/k9/index.md:153
#, no-wrap, markdown-text
msgid "3.1 Send encrypted email with K9"
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:156
#, no-wrap, markdown-text
msgid "**Step 1:** From any screen in **K-9 Mail** tap the ![](../../../media/en/k9/k9-en-and-016.png) icon to start a new email.\n"
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:158
#, no-wrap, markdown-text
msgid "**Step 2:** On the email composition screen add your recipient by either typing in an email address or pressing ![](../../../media/en/k9/k9-en-and-017.png) and selecting one from your address book.\n"
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:160
#, no-wrap, markdown-text
msgid "**Step 3:** Enable encrypted email by checking the box next to *encrypt* (![](../../../media/en/k9/k9-en-and-018.png)).\n"
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:162
#, no-wrap, markdown-text
msgid "**Step 4:** When finished writing your email, press ![](../../../media/en/k9/k9-en-and-019.png) to send.\n"
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:164
#, no-wrap, markdown-text
msgid "**Step 5:** The following screen will ask you to select which GPG keys to encrypt to. Be default the recipient key and your own should already be selected.\n"
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:166
#, no-wrap, markdown-text
msgid "**Note:** You should always ensure that your key is selected so that you can read the encrypted email that you had send.\n"
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:168
#, markdown-text
msgid "![](../../../media/en/k9/k9-en-and-020.png) ![](../../../media/en/k9/k9-en-and-021.png)"
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:170
#, no-wrap, markdown-text
msgid "*Figure 10 & 11: email composition and GPG key selection*\n"
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:172
#, no-wrap, markdown-text
msgid "**Step 6:** Once all recipient keys have been selected, press ![](../../../media/en/k9/k9-en-and-022.png) to send the email.\n"
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:174
#, no-wrap, markdown-text
msgid "**Note:** Currently, **K-9 Mail** can not encrypt attachments, so you will need to [use **APG** to encrypt files you wish to send](../../apg/android#encrypting-and-decrypting-files-with-apg) before you compose your message. To attach the file, tap ![](../../../media/en/k9/k9-en-and-023.png) and select the encrypted file (ending in *.gpg*).\n"
msgstr ""

#. type: Title ##
#: src/tools/k9/index.md:176
#, no-wrap, markdown-text
msgid "3.2 Receive encrypted email with K9"
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:179
#, no-wrap, markdown-text
msgid "**Step 1:** Open your *inbox* and tap the email you wish to read.\n"
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:181
#, no-wrap, markdown-text
msgid "**Step 2:** Tap the ![](../../../media/en/k9/k9-en-and-024.png) button.\n"
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:183
#, markdown-text
msgid "![](../../../media/en/k9/k9-en-and-025.png) ![](../../../media/en/k9/k9-en-and-026.png)"
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:185
#, no-wrap, markdown-text
msgid "*Figure 12 & 13: Encrypted email and GPG passphrase prompt*\n"
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:187
#, no-wrap, markdown-text
msgid "**Step 3:** Enter the passphrase for your GPG key when prompted and press ![](../../../media/en/k9/k9-en-and-027.png) to decrypt the email.\n"
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:189
#, markdown-text
msgid "![](../../../media/en/k9/k9-en-and-028.png)"
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:191
#, no-wrap, markdown-text
msgid "*Figure 14: Successfully Decrypted email*\n"
msgstr ""

#. type: Plain text
#: src/tools/k9/index.md:194
#, no-wrap, markdown-text
msgid "**Note:** as **K-9 Mail** is currently not able to decrypt encrypted attachments, you will need to save the attachments to your phone and [decrypt them with APG](../../apg/android#encrypting-and-decrypting-files-with-apg).\n"
msgstr ""
