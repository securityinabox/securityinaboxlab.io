# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-08-08 16:07+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Yaml Front Matter Hash Value: license
#: src/tools/firefox/index.md:1
#, no-wrap
msgid "Free and Open Source Software"
msgstr ""

#. type: Yaml Front Matter Hash Value: logo
#: src/tools/firefox/index.md:1
#, no-wrap
msgid "../../../media/tool/logo/firefox-logo-hr.png"
msgstr ""

#. type: Yaml Front Matter Hash Value: title
#: src/tools/firefox/index.md:1
#, no-wrap
msgid "Firefox Web Browser and Security Add-Ons"
msgstr ""

#. type: Yaml Front Matter Hash Value: website
#: src/tools/firefox/index.md:1
#, no-wrap
msgid "https://www.mozilla.org/en-US/firefox/"
msgstr ""

#. type: Plain text
#: src/tools/firefox/index.md:12
#, markdown-text
msgid "A web browser is one of the digital tools most of us use daily. It is the main way many people access the internet. Many people are familiar with Internet Explorer, Edge, Safari, and Chrome, browsers that come installed with our devices."
msgstr ""

#. type: Plain text
#: src/tools/firefox/index.md:14
#, markdown-text
msgid "Because we rely so much on browsers, they are often targets for people who want to compromise your privacy or security. Follow the steps below to choose a more secure browser and make its protection stronger."
msgstr ""

#. type: Title ##
#: src/tools/firefox/index.md:15
#, markdown-text, no-wrap
msgid "Look for the guide to your operating system"
msgstr ""

#. type: Plain text
#: src/tools/firefox/index.md:18
#, markdown-text
msgid "Many of the links on this page will take you to Mozilla's instructions for using Firefox. On that page, click the menus under \"Customize this article\" on the upper right hand side to see instructions for your operating system (Linux, Mac, Windows), or search for these articles for [Android](https://support.mozilla.org/en-US/products/mobile) or [iOS](https://support.mozilla.org/en-US/products/ios)."
msgstr ""

#. type: Title #
#: src/tools/firefox/index.md:19
#, markdown-text, no-wrap
msgid "Change your device’s default browser"
msgstr ""

#. type: Bullet: '- '
#: src/tools/firefox/index.md:29
#, markdown-text
msgid "**Do NOT use:**"
msgstr ""

#. type: Bullet: '    - '
#: src/tools/firefox/index.md:29
#, markdown-text
msgid "Safari"
msgstr ""

#. type: Bullet: '    - '
#: src/tools/firefox/index.md:29
#, markdown-text
msgid "Edge"
msgstr ""

#. type: Bullet: '    - '
#: src/tools/firefox/index.md:29
#, markdown-text
msgid "Internet Explorer"
msgstr ""

#. type: Bullet: '- '
#: src/tools/firefox/index.md:29
#, markdown-text
msgid "**DO use:**"
msgstr ""

#. type: Bullet: '    - '
#: src/tools/firefox/index.md:29
#, markdown-text
msgid "We strongly recommend the [Firefox](https://www.mozilla.org/en-US/firefox/new/) web browser, made by Mozilla. It has better built-in security than others. Firefox is free and open source software (FOSS)."
msgstr ""

#. type: Bullet: '    - '
#: src/tools/firefox/index.md:29
#, markdown-text
msgid "[Google Chrome](https://www.google.com/chrome/) also has high quality security, and would be another option. However, because it is a Google product, consider whether Chrome might send more data about you to Google than you are comfortable with. It will be able to connect your browser history with your email accounts and other personal information."
msgstr ""

#. type: Bullet: '    - '
#: src/tools/firefox/index.md:29
#, markdown-text
msgid "Learn [how to set your default browser](https://support.mozilla.org/en-US/kb/make-firefox-your-default-browser). Use the drop-down menus under \"Customize this article\" to find the right instructions for your device. Or, try the links below."
msgstr ""

#. type: Plain text
#: src/tools/firefox/index.md:31
#, markdown-text, no-wrap
msgid "**Android**\n"
msgstr ""

#. type: Bullet: '- '
#: src/tools/firefox/index.md:33
#, markdown-text
msgid "Android devices differ, but look in your device's settings for something like Apps or Applications > (possibly Advanced) > Default App(lications > Browser (app)"
msgstr ""

#. type: Plain text
#: src/tools/firefox/index.md:35
#, markdown-text, no-wrap
msgid "**iOS**\n"
msgstr ""

#. type: Bullet: '- '
#: src/tools/firefox/index.md:37
#, markdown-text
msgid "[Set Firefox as your default browser](https://support.apple.com/HT211336)"
msgstr ""

#. type: Plain text
#: src/tools/firefox/index.md:39
#, markdown-text, no-wrap
msgid "**Linux**\n"
msgstr ""

#. type: Bullet: '- '
#: src/tools/firefox/index.md:41
#, markdown-text
msgid "[Set Firefox as your default browser](https://help.ubuntu.com/stable/ubuntu-help/net-default-browser.html)"
msgstr ""

#. type: Plain text
#: src/tools/firefox/index.md:43
#, markdown-text, no-wrap
msgid "**Mac**\n"
msgstr ""

#. type: Bullet: '- '
#: src/tools/firefox/index.md:45
#, markdown-text
msgid "[Set Firefox as your default browser](https://support.apple.com/HT201607)"
msgstr ""

#. type: Plain text
#: src/tools/firefox/index.md:47
#, markdown-text, no-wrap
msgid "**Windows**\n"
msgstr ""

#. type: Bullet: '- '
#: src/tools/firefox/index.md:51
#, markdown-text
msgid "[Stop the Edge browser from automatically starting when you start Windows](https://support.microsoft.com/microsoft-edge/stop-microsoft-edge-from-starting-automatically-c341c879-799a-dccd-d6be-bc51ecdd5804)"
msgstr ""

#. type: Bullet: '- '
#: src/tools/firefox/index.md:51
#, markdown-text
msgid "[Change your default browser on Windows 10](https://support.microsoft.com/windows/change-your-default-browser-in-windows-10-020c58c6-7d77-797a-b74e-8f07946c5db6)"
msgstr ""

#. type: Bullet: '- '
#: src/tools/firefox/index.md:51
#, markdown-text
msgid "[Change your default browser on earlier versions of Windows](https://support.microsoft.com/windows/change-which-programs-windows-7-uses-by-default-62fd162f-8c82-0436-806f-c60d69dcf495)"
msgstr ""

#. type: Plain text
#: src/tools/firefox/index.md:53 src/tools/firefox/index.md:66
#: src/tools/firefox/index.md:75 src/tools/firefox/index.md:97
#: src/tools/firefox/index.md:106 src/tools/firefox/index.md:130
#: src/tools/firefox/index.md:159 src/tools/firefox/index.md:172
#: src/tools/firefox/index.md:195 src/tools/firefox/index.md:206
#: src/tools/firefox/index.md:216 src/tools/firefox/index.md:234
#, markdown-text, no-wrap
msgid "<details><summary>Learn why we recommend this</summary>\n"
msgstr ""

#. type: Plain text
#: src/tools/firefox/index.md:56
#, markdown-text, no-wrap
msgid ""
"Not all web browsers are created equal. Some protect your privacy and security more effectively than others.\n"
"</details>\n"
msgstr ""

#. type: Title #
#: src/tools/firefox/index.md:57
#, markdown-text, no-wrap
msgid "Make sure your browser is up to date"
msgstr ""

#. type: Bullet: '- '
#: src/tools/firefox/index.md:60
#, markdown-text
msgid "Firefox should update automatically, but you can [check whether you have the latest version of the browser](https://support.mozilla.org/kb/update-firefox-latest-release)"
msgstr ""

#. type: Title #
#: src/tools/firefox/index.md:61
#, markdown-text, no-wrap
msgid "Turn off the browser's built-in password manager"
msgstr ""

#. type: Bullet: '- '
#: src/tools/firefox/index.md:64
#, markdown-text
msgid "[Remove all saved logins and disable the password manager](https://support.mozilla.org/kb/password-manager-remember-delete-edit-logins#w_disabling-the-password-manager)"
msgstr ""

#. type: Plain text
#: src/tools/firefox/index.md:69
#, markdown-text, no-wrap
msgid ""
"Firefox can save and encrypt passwords for you. However, we recommend you turn this feature off and [use separate password manager like KeePassXC](../../passwords/) instead. Browser-based password managers put you at greater risk of an attacker tricking your browser into giving up your passwords.\n"
"</details>\n"
msgstr ""

#. type: Title #
#: src/tools/firefox/index.md:70
#, markdown-text, no-wrap
msgid "Review the camera, microphone, and other site permissions"
msgstr ""

#. type: Bullet: '- '
#: src/tools/firefox/index.md:73
#, markdown-text
msgid "[Manage permissions you may have granted to different websites](https://support.mozilla.org/kb/how-manage-your-camera-and-microphone-permissions)"
msgstr ""

#. type: Plain text
#: src/tools/firefox/index.md:78
#, markdown-text, no-wrap
msgid ""
"Permissions can be like a door or window you left open in your house: if one website can get in, others may be able to as well. Make sure only websites you use and trust have permission to use sensitive features like your camera or microphone. Malware might use those permissions to let someone see or hear where you are.\n"
"</details>\n"
msgstr ""

#. type: Title #
#: src/tools/firefox/index.md:79
#, markdown-text, no-wrap
msgid "Disable in all browsers: Flash and Java"
msgstr ""

#. type: Plain text
#: src/tools/firefox/index.md:82
#, markdown-text, no-wrap
msgid "**Disable in your browsers (Firefox, Chrome, Internet Explorer, Safari):**\n"
msgstr ""

#. type: Plain text
#: src/tools/firefox/index.md:86
#, markdown-text
msgid "Check to ensure that the following are disabled in your browser: - [Java](https://www.java.com/download/help/disable_browser.html)  - [Flash](https://www.howtogeek.com/222275/how-to-uninstall-and-disable-flash-in-every-web-browser/)"
msgstr ""

#. type: Plain text
#: src/tools/firefox/index.md:88
#, markdown-text, no-wrap
msgid "**Disable in email**\n"
msgstr ""

#. type: Bullet: '- '
#: src/tools/firefox/index.md:95
#, markdown-text
msgid "Mac mail:"
msgstr ""

#. type: Bullet: '    - '
#: src/tools/firefox/index.md:95
#, markdown-text
msgid "[Follow these instructions](https://osxdaily.com/2014/02/05/uninstall-mail-app-plugin-mac-os-x/); look for Java or Flash add-ons and turn them off or set them to \"ask to run\""
msgstr ""

#. type: Bullet: '- '
#: src/tools/firefox/index.md:95
#, markdown-text
msgid "Outlook:"
msgstr ""

#. type: Bullet: '    - '
#: src/tools/firefox/index.md:95
#, markdown-text
msgid "[Follow these instructions](https://support.microsoft.com/en-us/office/turn-an-add-in-off-for-outlook-for-windows-96737da4-ab7c-464e-9d2a-cf15db47c4cf); look for Java or Flash add-ons and turn them off or set them to \"ask to run\""
msgstr ""

#. type: Bullet: '- '
#: src/tools/firefox/index.md:95
#, markdown-text
msgid "Thunderbird:"
msgstr ""

#. type: Bullet: '    - '
#: src/tools/firefox/index.md:95
#, markdown-text
msgid "[Follow these instructions](https://support.mozilla.org/kb/thunderbird-add-ons-frequently-asked-questions#w_how-do-i-disable-or-uninstall-an-add-on); [look for Java or Flash add-ons and turn them off or set them to \"ask to run\""
msgstr ""

#. type: Plain text
#: src/tools/firefox/index.md:100
#, markdown-text, no-wrap
msgid ""
"Flash and Java are software packages that make it easy for someone to run malicious code on your device without your permission.\n"
"</details>\n"
msgstr ""

#. type: Title #
#: src/tools/firefox/index.md:101
#, markdown-text, no-wrap
msgid "Check Enhanced Tracking Protections settings"
msgstr ""

#. type: Bullet: '- '
#: src/tools/firefox/index.md:104
#, markdown-text
msgid "Check your settings; at least [set Enhanced Tracking Protection to Standard](https://support.mozilla.org/kb/enhanced-tracking-protection-firefox-desktop#w_adjust-your-global-enhanced-tracking-protection-settings); consider whether you want to set it to Strict (more sites will break)"
msgstr ""

#. type: Plain text
#: src/tools/firefox/index.md:109
#, markdown-text, no-wrap
msgid ""
"Cookies and other trackers gather details of who you are, where you are, and what you have looked at online. Consider what might happen if these fell into the hands of your adversary, and take these steps to limit tracking.\n"
"</details>\n"
msgstr ""

#. type: Title #
#: src/tools/firefox/index.md:110
#, markdown-text, no-wrap
msgid "Set default search engine"
msgstr ""

#. type: Plain text
#: src/tools/firefox/index.md:113
#, markdown-text, no-wrap
msgid "**All browsers**\n"
msgstr ""

#. type: Bullet: '- '
#: src/tools/firefox/index.md:118
#, markdown-text
msgid "Set your browser to a default search engine that does not track you:"
msgstr ""

#. type: Bullet: '    - '
#: src/tools/firefox/index.md:118
#, markdown-text
msgid "[**DuckDuckGo**](https://duckduckgo.com/about) (our top recommendation)"
msgstr ""

#. type: Bullet: '    - '
#: src/tools/firefox/index.md:118
#, markdown-text
msgid "[**StartPage**](https://www.startpage.com/)"
msgstr ""

#. type: Bullet: '    - '
#: src/tools/firefox/index.md:118
#, markdown-text
msgid "[**Disconnect**](https://disconnect.me/search/)"
msgstr ""

#. type: Plain text
#: src/tools/firefox/index.md:120 src/tools/firefox/index.md:179
#: src/tools/firefox/index.md:225
#, markdown-text, no-wrap
msgid "**Firefox**\n"
msgstr ""

#. type: Bullet: '- '
#: src/tools/firefox/index.md:124
#, markdown-text
msgid "Use the drop-down menus under \"Customize this article\" to find the right instructions for your device on these pages:"
msgstr ""

#. type: Bullet: '    - '
#: src/tools/firefox/index.md:124
#, markdown-text
msgid "[Set your default search engine](https://support.mozilla.org/kb/change-your-default-search-settings-firefox#w_default-search-engine)"
msgstr ""

#. type: Bullet: '    - '
#: src/tools/firefox/index.md:124
#, markdown-text
msgid "[Add or remove search engines](https://support.mozilla.org/kb/change-your-default-search-settings-firefox#w_remove-or-add-search-engines)"
msgstr ""

#. type: Plain text
#: src/tools/firefox/index.md:126 src/tools/firefox/index.md:190
#: src/tools/firefox/index.md:230
#, markdown-text, no-wrap
msgid "**Chrome**\n"
msgstr ""

#. type: Bullet: '- '
#: src/tools/firefox/index.md:128
#, markdown-text
msgid "[Set your default search engine](https://support.google.com/chrome/answer/95426)"
msgstr ""

#. type: Plain text
#: src/tools/firefox/index.md:133
#, markdown-text, no-wrap
msgid ""
"Search engines like Google and Bing build profiles of people who use them, track your device specifically, and share their users' personal information with third parties. Your browser uses one search engine by default when you type in what you want to search for.\n"
"</details>\n"
msgstr ""

#. type: Title #
#: src/tools/firefox/index.md:134
#, markdown-text, no-wrap
msgid "Use protective browser plugins"
msgstr ""

#. type: Plain text
#: src/tools/firefox/index.md:137
#, markdown-text, no-wrap
msgid "**All devices**\n"
msgstr ""

#. type: Bullet: '- '
#: src/tools/firefox/index.md:156
#, markdown-text
msgid "You can choose which add-ons to install and decide how to configure them, depending on your circumstances."
msgstr ""

#. type: Bullet: '- '
#: src/tools/firefox/index.md:156
#, markdown-text
msgid "If you are using a computer that is managed by someone else (at an Internet cafe, for example, or in your place of work), you might have to make these adjustments repeatedly."
msgstr ""

#. type: Bullet: '- '
#: src/tools/firefox/index.md:156
#, markdown-text
msgid "Install and configure:"
msgstr ""

#. type: Bullet: '    - '
#: src/tools/firefox/index.md:156
#, markdown-text
msgid "[HTTPS Everywhere](https://www.eff.org/https-everywhere/)"
msgstr ""

#. type: Bullet: '        - '
#: src/tools/firefox/index.md:156
#, markdown-text
msgid "**Why?** _Makes it so someone snooping on the network cannot see as much of what you are viewing and posting online, by protecting your browsing with encryption._"
msgstr ""

#. type: Bullet: '    - '
#: src/tools/firefox/index.md:156
#, markdown-text
msgid "[Privacy Badger](https://privacybadger.org/)"
msgstr ""

#. type: Bullet: '        - '
#: src/tools/firefox/index.md:156
#, markdown-text
msgid "**Why?** _Blocks trackers that gather data on where you have been online._"
msgstr ""

#. type: Bullet: '    - '
#: src/tools/firefox/index.md:156
#, markdown-text
msgid "[uBlock Origin](https://ublockorigin.com/)"
msgstr ""

#. type: Bullet: '        - '
#: src/tools/firefox/index.md:156
#, markdown-text
msgid "**Why?** _Blocks advertising and trackers, some of which might be malicious._"
msgstr ""

#. type: Bullet: '    - '
#: src/tools/firefox/index.md:156
#, markdown-text
msgid "Cookie Autodelete for [Firefox](https://addons.mozilla.org/firefox/addon/cookie-autodelete/) and [Chrome](https://chrome.google.com/webstore/detail/cookie-autodelete/fhcgjolkccmbidfldomjliifgaodjagh/)"
msgstr ""

#. type: Bullet: '        - '
#: src/tools/firefox/index.md:156
#, markdown-text
msgid "**Why?** _Deletes trackers that gather data on where you have been online._"
msgstr ""

#. type: Bullet: '    - '
#: src/tools/firefox/index.md:156
#, markdown-text
msgid "[Facebook Container](https://addons.mozilla.org/firefox/addon/facebook-container/), if you use Facebook (Firefox only)"
msgstr ""

#. type: Bullet: '        - '
#: src/tools/firefox/index.md:156
#, markdown-text
msgid "**Why?** _Keeps Facebook from gathering data on where you have been online and associating it with your profile._"
msgstr ""

#. type: Bullet: '    - '
#: src/tools/firefox/index.md:156
#, markdown-text
msgid "Zoom Redirector for [Firefox](https://addons.mozilla.org/firefox/addon/zoom-redirector/) and [Chrome](https://chrome.google.com/webstore/detail/zoom-redirector/fmaeeiocbalinknpdkjjfogehkdcbkcd)"
msgstr ""

#. type: Bullet: '        - '
#: src/tools/firefox/index.md:156
#, markdown-text
msgid "**Why?** _By making Zoom links open in your browser, this add-on keeps the call within your browser's protections._"
msgstr ""

#. type: Bullet: '    - '
#: src/tools/firefox/index.md:156
#, markdown-text
msgid "Optional, but recommended: [NoScript](https://noscript.net/)"
msgstr ""

#. type: Bullet: '        - '
#: src/tools/firefox/index.md:156
#, markdown-text
msgid "Note that NoScript will often make it appear that there is nothing on pages you visit, or that they are broken. [Learn how to configure NoScript so this happens less often.](https://noscript.net/features)"
msgstr ""

#. type: Bullet: '        - '
#: src/tools/firefox/index.md:156
#, markdown-text
msgid "**Why?** _It may be possible for an adversary to get to your device using malicious code in a script downloaded along with a webpage you are viewing. NoScript blocks all code from unknown websites, protecting your device from infection._"
msgstr ""

#. type: Plain text
#: src/tools/firefox/index.md:161
#, markdown-text
msgid "When you browse the Web, you come into contact with a great deal of code from unknown sources. This is one reason why the overwhelming majority of malware and spyware infections originate from web pages. Additionally, people who maintain and advertise on websites use \"cookies,\" which are small pieces of information that track you while you browse. And more fundamentally, websites do not always encrypt what they send or receive from you; they do not all use HTTPS."
msgstr ""

#. type: Plain text
#: src/tools/firefox/index.md:164
#, markdown-text, no-wrap
msgid ""
"We recommend installing these browser plugins or add-ons to protect against these security and privacy issues.\n"
"</details>\n"
msgstr ""

#. type: Title #
#: src/tools/firefox/index.md:165
#, markdown-text, no-wrap
msgid "Manage add-ons and pop-ups"
msgstr ""

#. type: Bullet: '- '
#: src/tools/firefox/index.md:170
#, markdown-text
msgid "[Make sure Firefox is set to block pop-ups and warn you when it is about to install an add-on](https://support.mozilla.org/kb/pop-blocker-settings-exceptions-troubleshooting#w_pop-up-blocker-settings)"
msgstr ""

#. type: Bullet: '- '
#: src/tools/firefox/index.md:170
#, markdown-text
msgid "[Update add-ons automatically](https://support.mozilla.org/kb/how-update-add-ons)"
msgstr ""

#. type: Bullet: '- '
#: src/tools/firefox/index.md:170
#, markdown-text
msgid "[Remove unused add-ons](https://support.mozilla.org/kb/disable-or-remove-add-ons)"
msgstr ""

#. type: Plain text
#: src/tools/firefox/index.md:175
#, markdown-text, no-wrap
msgid ""
"Malicious people may try to trick you into installing malware in the form of add-ons to your browser. They may do this using a pop-up window. Make sure your browser is set to protect you from these tricks. Additionally, ensure add-ons you do want are up to date, and remove ones you are not using. Just as old food can spoil, old code can let in bugs and endanger you.\n"
"</details>\n"
msgstr ""

#. type: Title #
#: src/tools/firefox/index.md:176
#, markdown-text, no-wrap
msgid "Delete browsing history"
msgstr ""

#. type: Bullet: '- '
#: src/tools/firefox/index.md:188
#, markdown-text
msgid "[Clear all cookies](https://support.mozilla.org/kb/clear-cookies-and-site-data-firefox)"
msgstr ""

#. type: Bullet: '- '
#: src/tools/firefox/index.md:188
#, markdown-text
msgid "[Disable third-party cookies](https://support.mozilla.org/kb/disable-third-party-cookies)"
msgstr ""

#. type: Bullet: '- '
#: src/tools/firefox/index.md:188
#, markdown-text
msgid "Set up a button that makes it easy to [quickly delete cookies and the history of pages you visited](https://support.mozilla.org/kb/forget-button-quickly-delete-your-browsing-history)"
msgstr ""

#. type: Bullet: '- '
#: src/tools/firefox/index.md:188
#, markdown-text
msgid "Tell Firefox to [\"Never remember history\" or \"use custom history\"](https://support.mozilla.org/kb/delete-browsing-search-download-history-firefox#w_how-do-i-make-firefox-clear-my-history-automatically)"
msgstr ""

#. type: Bullet: '    - '
#: src/tools/firefox/index.md:188
#, markdown-text
msgid "You can also delete your browser history manually: [\"how to clear your history.\"](https://support.mozilla.org/kb/delete-browsing-search-download-history-firefox#w_how-do-i-clear-my-history)"
msgstr ""

#. type: Bullet: '- '
#: src/tools/firefox/index.md:188
#, markdown-text
msgid "Consider whether you want to change what browser suggests when you type in address bar. You can:"
msgstr ""

#. type: Bullet: '    - '
#: src/tools/firefox/index.md:188
#, markdown-text
msgid "[Change your address bar settings so it does not suggest pages from your browsing history or other unwanted results](https://support.mozilla.org/kb/address-bar-autocomplete-firefox#w_how-can-i-control-what-results-the-address-bar-shows-me)"
msgstr ""

#. type: Bullet: '    - '
#: src/tools/firefox/index.md:188
#, markdown-text
msgid "[Remove autocomplete results](https://support.mozilla.org/kb/address-bar-autocomplete-firefox#w_removing-autocomplete-results)"
msgstr ""

#. type: Bullet: '- '
#: src/tools/firefox/index.md:193
#, markdown-text
msgid "[Select \"Block all cookies\" and \"Clear cookies when you quit Chrome\"](https://support.google.com/chrome/answer/95647)"
msgstr ""

#. type: Bullet: '- '
#: src/tools/firefox/index.md:193
#, markdown-text
msgid "[Delete the history of pages you visited](https://support.google.com/chrome/answer/95589?hl=en&co=GENIE.Platform=Desktop)"
msgstr ""

#. type: Plain text
#: src/tools/firefox/index.md:197
#, markdown-text
msgid "Your browsing history is a list of websites you have visited. The default option in Firefox is \"Remember my browsing and download history\", which means that Firefox will remember your browsing, download, form, and search histories. Firefox will also accept cookies (small pieces of information that track your online activity) from the websites you visit. These cookies allow websites to record information on your device that Firefox will send back to them and their advertising partners."
msgstr ""

#. type: Plain text
#: src/tools/firefox/index.md:200
#, markdown-text, no-wrap
msgid ""
"Browser history can be helpful to you: your browser will suggest pages you have visited before, so you don't have to re-type addresses or get sent to sites that are malicious. But there are trade-offs. If someone had access to the history of what you viewed on the internet, there is a lot they could learn about you, the people you work with, and the things you have been reading about.\n"
"</details>\n"
msgstr ""

#. type: Title #
#: src/tools/firefox/index.md:201
#, markdown-text, no-wrap
msgid "Consider not showing what you last viewed on startup"
msgstr ""

#. type: Bullet: '- '
#: src/tools/firefox/index.md:204
#, markdown-text
msgid "[Turn off \"restore previous session\"](https://support.mozilla.org/kb/restore-previous-session)"
msgstr ""

#. type: Plain text
#: src/tools/firefox/index.md:209
#, markdown-text, no-wrap
msgid ""
"If you are worried that your device will be seized or searched, turn off the feature that shows the webpages you had open when you last closed your browser.\n"
"</details>\n"
msgstr ""

#. type: Title #
#: src/tools/firefox/index.md:210
#, markdown-text, no-wrap
msgid "Connect over HTTPS"
msgstr ""

#. type: Bullet: '- '
#: src/tools/firefox/index.md:214
#, markdown-text
msgid "[Set your preferences to HTTPS only](https://support.mozilla.org/kb/https-only-prefs)"
msgstr ""

#. type: Bullet: '- '
#: src/tools/firefox/index.md:214
#, markdown-text
msgid "When you visit websites, look at the address at the top of your browser. Be sure the address begins `httpS://`, not just `http://`."
msgstr ""

#. type: Plain text
#: src/tools/firefox/index.md:219
#, markdown-text, no-wrap
msgid ""
"The S in HTTPS stands for \"secure.\" This is the protocol you should use to access web pages in your browser. HTTPS encrypts and protects what you are looking at as it travels between your device and the server the website is on.\n"
"</details>\n"
msgstr ""

#. type: Title #
#: src/tools/firefox/index.md:220
#, markdown-text, no-wrap
msgid "Use private browsing"
msgstr ""

#. type: Bullet: '- '
#: src/tools/firefox/index.md:223
#, markdown-text
msgid "[Understand more about what private browsing will NOT protect you from](https://support.mozilla.org/kb/common-myths-about-private-browsing), including posts you make on social media, files you download, or malware someone has put on your device."
msgstr ""

#. type: Bullet: '- '
#: src/tools/firefox/index.md:228
#, markdown-text
msgid "[Turn on private browsing for this session](https://support.mozilla.org/kb/private-browsing-use-firefox-without-history)"
msgstr ""

#. type: Bullet: '- '
#: src/tools/firefox/index.md:228
#, markdown-text
msgid "Consider [turning private browsing on at all times](https://support.mozilla.org/kb/private-browsing-use-firefox-without-history#w_can-i-set-firefox-to-always-use-private-browsing)"
msgstr ""

#. type: Bullet: '- '
#: src/tools/firefox/index.md:232
#, markdown-text
msgid "[Turn on private browsing](https://support.google.com/chrome/answer/95464?hl=en&ref_topic=9845306)"
msgstr ""

#. type: Plain text
#: src/tools/firefox/index.md:237
#, markdown-text, no-wrap
msgid ""
"\"Private browsing\" is a mode where the browser does not track cookies or save your browser history. Using it is a quick way to hide some of your activity if you otherwise tell your browser it is ok to keep a record of the pages you have searched. It can be especially useful if there is someone you live with who is threatening you and who has access to your device.\n"
"</details>\n"
msgstr ""

#. type: Title #
#: src/tools/firefox/index.md:238
#, markdown-text, no-wrap
msgid "Frequently Asked Questions"
msgstr ""

#. type: Plain text
#: src/tools/firefox/index.md:241
#, markdown-text, no-wrap
msgid "**Q:** I thought I got the internet through Facebook, Google, or Wikipedia. Do I need a browser?\n"
msgstr ""

#. type: Plain text
#: src/tools/firefox/index.md:243
#, markdown-text, no-wrap
msgid "**A:** If you are in a country where Wikipedia, Google, or Facebook provides your free access to the internet (a service known as zero-rating), you may think of these organizations or companies as the internet. But the internet is a much larger, loose network of computers that your device connects to in order to access pages, videos, files, and other content. The internet includes computers owned by governments, military groups, and universities. The internet also includes Facebook, Google, and Wikipedia--not the other way around.\n"
msgstr ""

#. type: Plain text
#: src/tools/firefox/index.md:245
#, markdown-text
msgid "Using Firefox, instead of an internet browser that is owned by a for-profit company like Google or Facebook, makes it less likely that what you look at online will be shared with a person, company, or government agency who you do not want to know these things."
msgstr ""

#. type: Plain text
#: src/tools/firefox/index.md:247
#, markdown-text, no-wrap
msgid "**Q:** Why would I want so many different add-ons to defend myself against malicious websites? If NoScript protects me from potentially dangerous scripts, for example, why do I also need other add-ons which function in a similar way?\n"
msgstr ""

#. type: Plain text
#: src/tools/firefox/index.md:248
#, markdown-text, no-wrap
msgid "**A:** It is often a good idea to use more than one tool to address the same general security issue (anti-virus programs are an important exception to this rule, since they tend to conflict with one another). These Firefox add-ons use very different techniques to protect your browser from a variety of threats. NoScript, for example, blocks all scripts from unknown websites, but users tend to 'whitelist' the websites they visit frequently, which allows them to load potentially-malicious scripts. NoScript users also tend to allow unknown sites to load scripts, on a temporary basis, if those scripts are necessary for the page to function properly.\n"
msgstr ""
