# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-12-12 21:02+0000\n"
"PO-Revision-Date: 2023-07-11 12:08+0000\n"
"Last-Translator: Ical <ical@riseup.net>\n"
"Language-Team: Indonesian <https://weblate.securityinabox.org/projects/info/landing-page/id/>\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 4.12.2\n"

#. type: Yaml Front Matter Hash Value: title
#: src/index.md:1
#, no-wrap
msgid "What do you need to protect?"
msgstr "Apa yang perlu kamu lindungi?"
