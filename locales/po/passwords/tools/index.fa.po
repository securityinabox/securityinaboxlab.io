# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-02-20 14:04+0000\n"
"PO-Revision-Date: 2024-06-06 19:10+0000\n"
"Last-Translator: Amin Kamrani <kamrani.ma@googlemail.com>\n"
"Language-Team: Persian <https://weblate.securityinabox.org/projects/passwords/tools/fa/>\n"
"Language: fa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 4.12.2\n"

#. type: Yaml Front Matter Hash Value: title
#: src/passwords/tools/index.md:1
#, no-wrap
msgid "Related Tools"
msgstr "ابزارهای مرتبط"

#. type: Plain text
#: src/passwords/tools/index.md:10
#, fuzzy
#| msgid "Use the following tools to create and maintain strong passwords and to protect your devices and accounts. You can also learn more about how to [create and maintain strong passwords](../passwords-and-2fa)."
msgid "Use the following tools to create and maintain strong passwords and to protect your devices and accounts. In this section, you can also learn more about how to [create and maintain strong passwords](../passwords), [manage your passwords safely](../password-managers) and [use 2-factor authentication](../2fa)."
msgstr "از ابزار زیر برای ایجاد و حفظ رمزهای عبور قوی استفاده کنید تا از حساب‌های کاربری و دستگاه‌های‌تان محافظت شود. شما همچنین می‌توانید درباره‌ی چگونگی [ایجاد و حفظ رمزهای عبور قوی](../passwords-and-2fa)، بیشتر بدانید."

#. type: Title #
#: src/passwords/tools/index.md:11
#, no-wrap
msgid "Security in a Box Tool Guides"
msgstr "راهنمای ابزار «جعبه امنیت دیجیتال»"

#. type: Plain text
#: src/passwords/tools/index.md:15
msgid "[![](../../../media/en/logos/keepassxc-logo.png)](#keepassxc)  [KeePassXC](#keepassxc)"
msgstr "[![](../../../media/en/logos/keepassxc-logo.png)](#keepassxc)  [KeePassXC](#keepassxc)"

#. type: Plain text
#: src/passwords/tools/index.md:18
msgid "[![](../../../media/en/logos/keepassdx-logo.png)](#keepassdx)  [KeePassDX](#keepassdx)"
msgstr "[![](../../../media/en/logos/keepassdx-logo.png)](#keepassdx)  [KeePassDX](#keepassdx)"

#. type: Title #
#: src/passwords/tools/index.md:20
#, no-wrap
msgid "Manage your passwords safely"
msgstr "رمزهای عبورتان را به صورت امن مدیریت کنید"

#. type: Title ##
#: src/passwords/tools/index.md:22
#, no-wrap
msgid "[KeePassXC](https://keepassxc.org)"
msgstr "[KeePassXC](https://keepassxc.org)"

#. type: Plain text
#: src/passwords/tools/index.md:26
#, fuzzy
#| msgid "[![](../../../media/en/logos/keepassxc-logo.png)](https://keepassxc.org)  (Linux, Mac, Windows)"
msgid "[![](../../../media/en/logos/keepassxc-logo.png)](https://keepassxc.org)  (Linux, macOS, Windows)"
msgstr "[![](../../../media/en/logos/keepassxc-logo.png)](https://keepassxc.org)  (لینوکس، مک، ویندوز)"

#. type: Plain text
#: src/passwords/tools/index.md:28
msgid "A free and open-source password manager for computers"
msgstr "یک مدیر رمز عبور رایگان و متن‌باز برای کامپیوترها"

#. type: Plain text
#: src/passwords/tools/index.md:30
#, fuzzy
#| msgid "[Download](https://keepassxc.org/download/) | See [our guide](../../tools/keepassxc) and [their guide](https://keepassxc.org/docs/)"
msgid "[Download](https://keepassxc.org/download/) | See [our guide](../../tools/keepassxc) and [their FAQ](https://keepassxc.org/docs/)"
msgstr "[دانلود کنید](https://keepassxc.org/download/) | [راهنمای ما](/en/tools/keepassxc) و [راهنمای آن‌ها](https://keepassxc.org/docs/) را ببینید"

#. type: Title ##
#: src/passwords/tools/index.md:31
#, no-wrap
msgid "[KeePassDX](https://www.keepassdx.com)"
msgstr "[KeePassDX](https://www.keepassdx.com)"

#. type: Plain text
#: src/passwords/tools/index.md:35
msgid "[![](../../../media/en/logos/keepassdx-logo.png)](https://www.keepassdx.com)  (Android)"
msgstr "[![](/media/en/logos/keepassdx-logo.png)](https://www.keepassdx.com)  (اندروید)"

#. type: Plain text
#: src/passwords/tools/index.md:37
msgid "A free and open-source password manager for Android phones and tablets"
msgstr "یک مدیر رمز عبور رایگان و متن‌باز برای تلفن‌های اندروید و تبلت‌ها"

#. type: Plain text
#: src/passwords/tools/index.md:39
#, fuzzy
#| msgid "Download from [Google Play](https://play.google.com/store/apps/details?id=com.kunzisoft.keepass.free) or [F-Droid](https://www.f-droid.org/packages/com.kunzisoft.keepass.libre/) | See [our guide](../../tools/keepassdx) and [their guide](https://github.com/Kunzisoft/KeePassDX/wiki)"
msgid "Download from [Google Play](https://play.google.com/store/apps/details?id=com.kunzisoft.keepass.free) or [F-Droid](https://www.f-droid.org/packages/com.kunzisoft.keepass.libre/) | See [our guide](../../tools/keepassdx) and [their documentation](https://github.com/Kunzisoft/KeePassDX/wiki)"
msgstr "دانلود از [گوگل پلی](https://play.google.com/store/apps/details?id=com.kunzisoft.keepass.free) یا [اف‌-دروید](https://www.f-droid.org/packages/com.kunzisoft.keepass.libre/) | [راهنمای ما](../../tools/keepassdx) و [راهنمای آن‌ها](https://github.com/Kunzisoft/KeePassDX/wiki) را ببینید"

#. type: Title ##
#: src/passwords/tools/index.md:40
#, no-wrap
msgid "[Strongbox](https://strongboxsafe.com)"
msgstr "[«استرانگ‌باکس» Strongbox](https://strongboxsafe.com)"

#. type: Plain text
#: src/passwords/tools/index.md:44
#, fuzzy
#| msgid "[![](../../../media/en/logos/strongbox-logo.png)](https://strongboxsafe.com)  (iOS, Mac)"
msgid "[![](../../../media/en/logos/strongbox-logo.png)](https://strongboxsafe.com)  (iOS, macOS)"
msgstr "[![](../../../media/en/logos/strongbox-logo.png)](https://strongboxsafe.com)  (آی‌او‌اس، مک)"

#. type: Plain text
#: src/passwords/tools/index.md:46
#, fuzzy
#| msgid "A free and open-source password manager for iPhones and Macs"
msgid "A free and open-source password manager for iPhones and macOS computers"
msgstr "یک مدیر رمز عبور رایگان و متن‌باز برای آی‌فون‌ها و مک‌ها"

#. type: Plain text
#: src/passwords/tools/index.md:48
#, fuzzy
#| msgid "Download from [Apple's iOS App Store](https://apps.apple.com/us/app/strongbox-keepass-pwsafe/id897283731) or [Apple's Mac App Store](https://apps.apple.com/us/app/strongbox/id1270075435) | See [their guide](https://strongboxsafe.com/support/)"
msgid "Download from the [iOS App Store](https://apps.apple.com/us/app/strongbox-keepass-pwsafe/id897283731) or the [macOS App Store](https://apps.apple.com/us/app/strongbox/id1270075435) | See [their documentation](https://strongboxsafe.com/getting-started/)"
msgstr "دانلود از [اپ‌استورِ آی‌او‌اسِ اپل](https://apps.apple.com/us/app/strongbox-keepass-pwsafe/id897283731) یا [اپ استورِ مکِ اپل](https://apps.apple.com/us/app/strongbox/id1270075435) | [راهنمای آن‌ها](https://strongboxsafe.com/support/) را ببینید"

#. type: Title ##
#: src/passwords/tools/index.md:49
#, no-wrap
msgid "[Bitwarden](https://bitwarden.com)"
msgstr "[«بیت‌واردن» Bitwarden](https://bitwarden.com)"

#. type: Plain text
#: src/passwords/tools/index.md:53
#, fuzzy
#| msgid "[![](../../../media/en/logos/bitwarden-logo.png)](https://bitwarden.com)  (Android, iOS, Linux, Mac, Windows)"
msgid "[![](../../../media/en/logos/bitwarden-logo.png)](https://bitwarden.com)  (Android, iOS, Linux, macOS, Windows)"
msgstr "[![](../../../media/en/logos/bitwarden-logo.png)](https://bitwarden.com)  (اندروید، آی‌او‌اس، لینوکس، مک، ویندوز)"

#. type: Plain text
#: src/passwords/tools/index.md:55
msgid "An open-source online password manager with free, paid or self-hosted options"
msgstr "یک مدیر رمز عبور آنلاین متن‌باز با گزینه‌های رایگان، پرداختی یا خودمیزبانی"

#. type: Plain text
#: src/passwords/tools/index.md:57
#, fuzzy
#| msgid "Download from [their website](https://bitwarden.com/download/), [Apple's iOS App Store](https://apps.apple.com/app/bitwarden-free-password-manager/id1137397744) or [Google Plaly](https://play.google.com/store/apps/details?id=com.x8bit.bitwarden) | See [their documentation](https://bitwarden.com/help/)"
msgid "Download from [their website](https://bitwarden.com/download/), [Apple's iOS App Store](https://apps.apple.com/app/bitwarden-free-password-manager/id1137397744) or [Google Play](https://play.google.com/store/apps/details?id=com.x8bit.bitwarden) | See [their documentation](https://bitwarden.com/help/)"
msgstr "دانلود از [وبسایت‌شان](https://bitwarden.com/download/)، [اپ‌استورِ آی‌او‌اسِ اپل](https://apps.apple.com/app/bitwarden-free-password-manager/id1137397744) یا [گوگل پلی](https://play.google.com/store/apps/details?id=com.x8bit.bitwarden) | [متونِ آن‌ها](https://bitwarden.com/help/) را ببینید"

#. type: Title #
#: src/passwords/tools/index.md:58
#, no-wrap
msgid "Two-factor authentication (2FA)"
msgstr "احراز هویت دو عاملی (2FA)"

#. type: Title ##
#: src/passwords/tools/index.md:60
#, no-wrap
msgid "[Aegis](https://getaegis.app/)"
msgstr "[«ایجس» Aegis](https://getaegis.app/)"

#. type: Plain text
#: src/passwords/tools/index.md:64
msgid "[![](../../../media/en/logos/aegis-logo.png)](https://getaegis.app/)  (Android)"
msgstr "[![](../../../media/en/logos/aegis-logo.png)](https://getaegis.app/)  (اندروید)"

#. type: Plain text
#: src/passwords/tools/index.md:66 src/passwords/tools/index.md:75
#: src/passwords/tools/index.md:84
msgid "Free and open-source app to manage two factor authentication tokens (MFA, 2FA)"
msgstr "اپلیکیشن رایگان و متن‌باز برای مدیریت توکن‌های احراز هویت دو عاملی (یا چند عاملی)"

#. type: Plain text
#: src/passwords/tools/index.md:68
msgid "Download from [Google Play](https://play.google.com/store/apps/details?id=com.beemdevelopment.aegis) or [F-Droid](https://f-droid.org/en/packages/com.beemdevelopment.aegis) | See [their guide](https://github.com/beemdevelopment/Aegis)"
msgstr "دانلود از [گوگل پلی](https://play.google.com/store/apps/details?id=com.beemdevelopment.aegis) یا [اف-دروید](https://f-droid.org/en/packages/com.beemdevelopment.aegis) | [راهنمای آن‌ها](https://github.com/beemdevelopment/Aegis) را ببینید"

#. type: Title ##
#: src/passwords/tools/index.md:69
#, no-wrap
msgid "[Raivo OTP](https://github.com/raivo-otp/ios-application)"
msgstr "[Raivo OTP](https://github.com/raivo-otp/ios-application)"

#. type: Plain text
#: src/passwords/tools/index.md:73
#, fuzzy
#| msgid "[![](../../../media/en/logos/raivo-otp-logo.png)](https://github.com/raivo-otp/ios-application)  (iOS, Mac)"
msgid "[![](../../../media/en/logos/raivo-otp-logo.png)](https://github.com/raivo-otp/ios-application)  (iOS, macOS)"
msgstr "[![](../../../media/en/logos/raivo-otp-logo.png)](https://github.com/raivo-otp/ios-application)  (آی‌او‌اس، مک)"

#. type: Plain text
#: src/passwords/tools/index.md:77
#, fuzzy
#| msgid "Download from [Apple's iOS App Store](https://apps.apple.com/us/app/raivo-otp/id1459042137) or [Apple's Mac App Store](https://apps.apple.com/us/app/raivo-otp/id1498497896)"
msgid "Download from [Apple's iOS App Store](https://apps.apple.com/us/app/raivo-otp/id1459042137) or [Apple's App Store](https://apps.apple.com/us/app/raivo-otp/id1498497896)"
msgstr "دانلود از [اپ‌استورِ آی‌او‌اسِ اپل](https://apps.apple.com/us/app/raivo-otp/id1459042137) یا [اپ‌استورِ مکِ اپل](https://apps.apple.com/us/app/raivo-otp/id1498497896)"

#. type: Title ###
#: src/passwords/tools/index.md:78
#, fuzzy, no-wrap
#| msgid "[Haven](https://guardianproject.github.io/haven/)"
msgid "[FreeOTP](https://freeotp.github.io/)"
msgstr "[«هیون» Haven](https://guardianproject.github.io/haven/)"

#. type: Plain text
#: src/passwords/tools/index.md:82
#, fuzzy
#| msgid "[![](../../../media/en/logos/haven-logo.png)](https://guardianproject.github.io/haven/)  (Android)"
msgid "[![](../../../media/en/logos/freeotp-logo.png)](https://freeotp.github.io/)  (Android, iOS)"
msgstr "[![](../../../media/en/logos/haven-logo.png)](https://guardianproject.github.io/haven/)  (اندروید)"

#. type: Plain text
#: src/passwords/tools/index.md:85
#, fuzzy
#| msgid "Download from [Google Play](https://play.google.com/store/apps/details?id=com.fsck.k9) or [F-Droid](https://f-droid.org/packages/com.fsck.k9/) | See [their guide](https://k9mail.app/documentation/)"
msgid "Download from [Google Play](https://play.google.com/store/apps/details?id=org.fedorahosted.freeotp) or [F-Droid](https://f-droid.org/packages/org.fedorahosted.freeotp/) (Android) or from the [App Store](https://apps.apple.com/us/app/freeotp-authenticator/id872559395) (iOS)"
msgstr "از [گوگل پلی](https://play.google.com/store/apps/details?id=com.fsck.k9) یا [«اف-دروید» F-Droid](https://f-droid.org/packages/com.fsck.k9/) دانلود کنید | [راهنمای آن‌ها](https://k9mail.app/documentation/) را ببینید"

#, no-wrap
#~ msgid "Create and maintain strong passwords"
#~ msgstr "ایجاد و حفظ رمزهای عبور قوی"

#, fuzzy, no-wrap
#~ msgid "Free and open-source app to manage two factor authentication tokens (MFA, 2FA)\n"
#~ msgstr ""
#~ "Free and open-source app to manage two factor authentication tokens (MFA, 2FA)\n"
#~ " "
