# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-12-12 21:02+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: ps\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Yaml Front Matter Hash Value: author
#: src/blog/observations-recent-india-censorship/index.md:1
#, no-wrap
msgid "Kaustubh Srikanth"
msgstr ""

#. type: Yaml Front Matter Hash Value: teaser
#: src/blog/observations-recent-india-censorship/index.md:1
#, no-wrap
msgid "On 17th December 2014, the Government of India's Ministry of Communications and Information Technology issued an order to all licensed Internet Service Providers (ISPs) in the country to block access to 32 websites, effective immediately."
msgstr ""

#. type: Yaml Front Matter Hash Value: teaser_image
#: src/blog/observations-recent-india-censorship/index.md:1
#, no-wrap
msgid "../../../media/en/blog/cens2.jpg"
msgstr ""

#. type: Yaml Front Matter Hash Value: title
#: src/blog/observations-recent-india-censorship/index.md:1
#, no-wrap
msgid "Observations on recent India censorship"
msgstr ""

#. type: Plain text
#: src/blog/observations-recent-india-censorship/index.md:10
msgid "![](../../../media/en/blog/cens2.jpg)"
msgstr ""

#. type: Plain text
#: src/blog/observations-recent-india-censorship/index.md:12
msgid "On 17th December 2014, the Government of India's Ministry of Communications and Information Technology issued an order to all licensed Internet Service Providers (ISPs) in the country to [block access to 32 websites](http://www.livemint.com/Industry/drJ5ToWFEIyRNEAbn9OcGN/Govt-blocks-32-websites-including-Vimeo-and-Github.html), effective immediately. Not only did the ban affect access to popular cultural sites such as archive.org, vimeo.com, dailymotion.com, but the order also blocked access to sites like github.com, pastebin.com, which are useful for all sorts of people but are especially popular with software developers."
msgstr ""

#. type: Plain text
#: src/blog/observations-recent-india-censorship/index.md:14
msgid "A copy of the order that the MCIT's Department of Telecommunications sent to ISPs by email can be found [here](http://cis-india.org/internet-governance/resources/2014-12-17_DoT-32-URL-Block-Order_compressed.pdf) (356kB, compressed PDF) or [here](http://cis-india.org/internet-governance/resources/2014-12-17_DoT-32-URL-Block-Order.pdf) (2MB)."
msgstr ""

#. type: Plain text
#: src/blog/observations-recent-india-censorship/index.md:16
msgid "![](../../../media/en/blog/2015-01-06-Kaustabh_screenshot1.png)"
msgstr ""

#. type: Plain text
#: src/blog/observations-recent-india-censorship/index.md:18
msgid "The Ministry's order was issued following a request from the Mumbai police's Anti-Terrorism Squad on 15th November 2014. The police request argued that the targeted web services were being used for \"Jihadi Propaganda\" by \"Anti-National groups\", and were encouraging youth in the country to join organisations like the Islamic State (ISIS/ISIL)."
msgstr ""

#. type: Plain text
#: src/blog/observations-recent-india-censorship/index.md:20
msgid "However, many of the blocked sites are large resources for general use by diverse communities which have no links to terrorism. Tools which are important in the daily work of India-based software developers are included in the banned sites, whose work in the IT sector is penalised by broad bans with the excuse of anti-terrorism measures."
msgstr ""

#. type: Plain text
#: src/blog/observations-recent-india-censorship/index.md:22
msgid "As IT professionals in India attempt to continue to do their jobs, there has been a lack of information about the nature of the site bans. We thought it would be a good idea to do some research using free and accessible tools and to look at how censorship has been implemented, as well as the various circumvention techniques people are using."
msgstr ""

#. type: Title ##
#: src/blog/observations-recent-india-censorship/index.md:23
#, no-wrap
msgid "A summary of our key findings"
msgstr ""

#. type: Plain text
#: src/blog/observations-recent-india-censorship/index.md:26
msgid "Between January 1st and 3rd 2015, we conducted censorship measurements from various Internet connections using seven different ISPs in India. These include TATA Communications and the state-run Mahanagar Telecom Nigam Limited (MTNL). The understanding we currently have is preliminary and draws on the browsing experience of several customers of different ISPs around India as well as information gained through the use of the open source censorship measurement toolkit provided by [Open Observatory of Network Interference](https://ooni.torproject.org/) (OONI) and other manual tests we conducted."
msgstr ""

#. type: Plain text
#: src/blog/observations-recent-india-censorship/index.md:28
msgid "The censorship order issued by the Ministry specifies what to block, but not how. Unsurprisingly, this has led to a situation where different ISPs are blocking sites using different techniques. Users of some ISPs may be able to circumvent the censorship by simply changing their DNS settings, while others will need to configure proxies or install circumvention software. In all cases we have observed, censorship can be bypassed using standard circumvention tools such as the [Tor Browser](https://torproject.org/)."
msgstr ""

#. type: Plain text
#: src/blog/observations-recent-india-censorship/index.md:30
msgid "We saw a variety of different block pages across multiple ISPs. Here are screenshots of the six we captured."
msgstr ""

#. type: Bullet: '* '
#: src/blog/observations-recent-india-censorship/index.md:37
msgid "“The page you have requested has been blocked, because the URL is banned”"
msgstr ""

#. type: Bullet: '* '
#: src/blog/observations-recent-india-censorship/index.md:37
msgid "“This site has been blocked as per the instruction of Competent Authority”"
msgstr ""

#. type: Bullet: '* '
#: src/blog/observations-recent-india-censorship/index.md:37
msgid "“<!–This is a comment. Comments are not displayed in the browser–>”"
msgstr ""

#. type: Bullet: '* '
#: src/blog/observations-recent-india-censorship/index.md:37
msgid "“The requested url is blocked, based on the blocking Instruction order received from the Department of Telecommunications, Ministry of Communications & IT, Government of India”"
msgstr ""

#. type: Bullet: '* '
#: src/blog/observations-recent-india-censorship/index.md:37
msgid "“HTTP Error 404 — File or Directory not found”"
msgstr ""

#. type: Bullet: '* '
#: src/blog/observations-recent-india-censorship/index.md:37
msgid "The page you have requested has been blocked, because the URL is banned.”"
msgstr ""

#. type: Plain text
#: src/blog/observations-recent-india-censorship/index.md:39
msgid "Besides finding that different ISPs use different methods of blocking, we also found that the same sites might be blocked with different methods even from the same ISP. The \"not found\" and \"this is a comment\" error pages appeared across multiple ISPs, which could indicate that there are multiple layers of blocking so that if the first one \"fails open\" another layer catches it. Even so, the blocking is unreliable--when requesting the same site many times, it sometimes loads and sometimes yields a censorship message or error page."
msgstr ""

#. type: Plain text
#: src/blog/observations-recent-india-censorship/index.md:41
msgid "TATA appears to be using a proxy server to inspect and modify traffic to certain IP addresses. If the request is for one of the censored sites, a block page is returned instead. We can tell that the filtering is only being applied to certain IP addresses by sending HTTP requests for censored hostnames to the IP addresses of unrelated websites. Using some TATA connections, requests to some IP addresses are blocked based on the content of the request, while requests for those same hostnames sent to other IPs are not blocked. In particular, requests to google.com IPs containing host headers requesting blocked hostnames return the block page for those hostnames, while requests to yahoo.com IP addresses do not."
msgstr ""

#. type: Plain text
#: src/blog/observations-recent-india-censorship/index.md:43
msgid "Instead of [Deep Packet Inspection](https://en.wikipedia.org/wiki/Deep_packet_inspection), MTNL appears to be using a combination of DNS-based and IP-based blocking approaches. Their DNS resolvers gives an incorrect answer (59.185.3.14) for the censored hostnames. It is possible to see the block page that MTNL users experience by browsing to [http://59.185.3.14/](http://59.185.3.14/) from anywhere in the world. Some MTNL customers were still able to connect to github's correct IP, while others were not."
msgstr ""

#. type: Plain text
#: src/blog/observations-recent-india-censorship/index.md:45
msgid "Most of the reports collected using OONI are available [here](https://ooni.torproject.org/reports/0.1/IN/). These reports contain evidence of other sites being blocked in addition to the 32 websites specified in the December 17th order. The other sites are apparently being blocked using the same infrastructure, but we have not been able to determine under what authority their blocking has been ordered."
msgstr ""

#. type: Plain text
#: src/blog/observations-recent-india-censorship/index.md:47
msgid "Other domains which appeared blocked on MTNL during testing included adult websites featuring Indian people although other adult websites listed in the [alexa-top-1000](https://alexa.com/) were not observed to be blocked. Censorship of advertisement, music sharing, and file hosting websites was also observed."
msgstr ""

#. type: Title ##
#: src/blog/observations-recent-india-censorship/index.md:48
#, no-wrap
msgid "How can one circumvent this censorship?"
msgstr ""

#. type: Plain text
#: src/blog/observations-recent-india-censorship/index.md:51
msgid "In some cases, as ISPs are only blocking HTTP connections, while allowing access to sites over HTTPS, one could try to manually access the site using https:// instead of http:// in the URL. Regardless of whether the webpages you access are being censored or not, we recommend using the [HTTPS Everywhere](https://www.eff.org/https-everywhere) plugin in your web browser to automatically use the HTTPS version of many sites."
msgstr ""

#. type: Plain text
#: src/blog/observations-recent-india-censorship/index.md:53
msgid "When the censorship is DNS-based, it can usually be circumvented by changing the DNS configuration on your device to use nameservers hosted outside of India. Two popular public DNS services are offered by [OpenDNS](https://store.opendns.com/setup) and [Google's public DNS](https://developers.google.com/speed/public-dns/docs/using)."
msgstr ""

#. type: Plain text
#: src/blog/observations-recent-india-censorship/index.md:55
msgid "We were also able to access the blocked websites using [Tor Browser](https://www.torproject.org/) at all times."
msgstr ""

#. type: Plain text
#: src/blog/observations-recent-india-censorship/index.md:57
msgid "Another option is to use a Virtual Private Network (VPN) hosted outside the country and a couple of services which offer this are The [RiseUp Collective](https://help.riseup.net/en/vpn) (Free) and [iVPN](https://www.ivpn.net/) (Paid). Mobile users using Android devices can also use [Psiphon](https://psiphon.ca/en/index.html)."
msgstr ""

#. type: Title ##
#: src/blog/observations-recent-india-censorship/index.md:58
#, no-wrap
msgid "Additional resources"
msgstr ""

#. type: Plain text
#: src/blog/observations-recent-india-censorship/index.md:61
msgid "If you would like to understand more about censorship techniques or help collect more data, here are some useful resources that you might want to refer to:"
msgstr ""

#. type: Bullet: '* '
#: src/blog/observations-recent-india-censorship/index.md:63
msgid "[Open Observatory of Network Interference](https://ooni.torproject.org/) provides a set of open source tools that can be used to test and collect technical data about censorship and network tampering. We have made the reports generated from the data we collected using OONI [here](https://ooni.torproject.org/reports/0.1/IN/)."
msgstr ""

#. type: Bullet: '* '
#: src/blog/observations-recent-india-censorship/index.md:65
msgid "URL lists provided by [CitizenLab](http://citizenlab.org/) were also used during [testing](https://github.com/citizenlab/test-lists/archive/master.zip)."
msgstr ""

#. type: Bullet: '* '
#: src/blog/observations-recent-india-censorship/index.md:67
msgid "[Tor Browser](https://www.torproject.org/) is a free and open source software tool, which lets you securely circumvent censorship and surveillance and allows you to access resources on the Internet anonymously."
msgstr ""

#. type: Bullet: '* '
#: src/blog/observations-recent-india-censorship/index.md:69
msgid "For other resources about circumvention tools and tactics, and general digital security advice, please see Tactical Techology Collective's [Security-in-a-box](../../) project."
msgstr ""

#. type: Bullet: '* '
#: src/blog/observations-recent-india-censorship/index.md:71
msgid "The [Center for Internet and Society](https://cis-india.org/) is an organisation based in Bangalore, India, which is actively working on policy for Internet governance, censorship and surveillance in India."
msgstr ""

#. type: Title ##
#: src/blog/observations-recent-india-censorship/index.md:72
#, no-wrap
msgid "Current state of things"
msgstr ""

#. type: Plain text
#: src/blog/observations-recent-india-censorship/index.md:75
msgid "Following a new [order](http://pib.nic.in/newsite/PrintRelease.aspx?relid=114259) issued on 31st December 2014, 4 of the 32 websites have subsequently been unblocked. The unblocked sites are github.com, vimeo.com, dailymotion.com and weebly.com"
msgstr ""

#. type: Plain text
#: src/blog/observations-recent-india-censorship/index.md:77
msgid "We will keep monitoring this censorship and publish any other relevant findings over the next few days. If you are a software developer or an IT professional who wants to help us collect more data from multiple ISPs in India, please contact us at censorship-in@chaoslab.in. Please use this [PGP key](http://chaoslab.in/goiblocks/censorship-in@chaoslab.in.pub.asc) if you would like to send us an encrypted email."
msgstr ""

#. type: Plain text
#: src/blog/observations-recent-india-censorship/index.md:79
#, no-wrap
msgid "*To read the larger dialogue on Twitter about this blocking of websites, please follow the hashtag [#GOIblocks](https://twitter.com/hashtag/GOIblocks).*\n"
msgstr ""

#. type: Plain text
#: src/blog/observations-recent-india-censorship/index.md:80
#, no-wrap
msgid "*This blog post was co-authored by [Kaustubh Srikanth](https://twitter.com/houndbee), [Leif Ryge](https://twitter.com/wiretapped), Aaron Gibson and [Claudio Guarnieri](https://twitter.com/botherder) and originally appeared in [Huffingtonpost.in](http://www.huffingtonpost.in/kaustubh-srikanth/technical-observations-ab_b_6421306.html) on 6th January, 2015*\n"
msgstr ""
