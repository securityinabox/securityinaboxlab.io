# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-03-05 10:55+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Yaml Front Matter Hash Value: author
#: src/blog/2024-security-in-a-box-updates/index.md:1
#, no-wrap
msgid "Security in a Box"
msgstr ""

#. type: Yaml Front Matter Hash Value: teaser
#: src/blog/2024-security-in-a-box-updates/index.md:1
#, no-wrap
msgid "Updates to tool guides on password managers, browser add-ons, and basic Android security."
msgstr ""

#. type: Yaml Front Matter Hash Value: teaser_image
#: src/blog/2024-security-in-a-box-updates/index.md:1
#, no-wrap
msgid "../../../media/en/blog/Cybersecurity_Visuals_Challenge_2019_-_Rebecca_Wang.jpg"
msgstr ""

#. type: Yaml Front Matter Hash Value: title
#: src/blog/2024-security-in-a-box-updates/index.md:1
#, no-wrap
msgid "On the latest Security in a Box updates"
msgstr ""

#. type: Plain text
#: src/blog/2024-security-in-a-box-updates/index.md:11
#, markdown-text
msgid "![](../../../media/en/blog/Cybersecurity_Visuals_Challenge_2019_-_Rebecca_Wang.jpg)"
msgstr ""

#. type: Plain text
#: src/blog/2024-security-in-a-box-updates/index.md:13
#, markdown-text
msgid "Security in a Box was initially created [in 2005 as NGO in a Box - Security edition](https://web.archive.org/web/20060209174038/http://security.ngoinabox.org/), with its title changing to the current one [in 2009](https://web.archive.org/web/20090522065822/http://security.ngoinabox.org:80/). Over these 20 years, it has been developed on different platforms and has repeatedly changed its structure and content, with one fundamental goal in mind: offering useful strategies and tools for human rights defenders, journalists, activists and civil society members to protect themselves against the most widespread digital threats they face in their everyday life."
msgstr ""

#. type: Plain text
#: src/blog/2024-security-in-a-box-updates/index.md:15
#, markdown-text
msgid "At the beginning of each page, immediately after the title, you can check when a guide has been last updated, to make sure that the recommendations you are going to read still apply to the current situation."
msgstr ""

#. type: Plain text
#: src/blog/2024-security-in-a-box-updates/index.md:17
#, markdown-text
msgid "In 2024 most of the guides in Security in a Box have been reviewed and updated and some more have been created from scratch. All this new and updated content has been completely translated into French, Indonesian, Portuguese, Russian, Spanish, Turkish and Vietnamese, and is being translated into Arabic, Burmese, Chinese, Farsi and Pashto (this latter translation has been funded by [UN Women](https://www.unwomen.org/en))."
msgstr ""

#. type: Plain text
#: src/blog/2024-security-in-a-box-updates/index.md:19
#, markdown-text
msgid "Here's a list of what is new and what is updated in the entire website:"
msgstr ""

#. type: Title ##
#: src/blog/2024-security-in-a-box-updates/index.md:20
#, markdown-text, no-wrap
msgid "New guides on Security in a Box"
msgstr ""

#. type: Bullet: '- '
#: src/blog/2024-security-in-a-box-updates/index.md:28
#, markdown-text
msgid "[Create and protect multiple online identities](https://securityinabox.org/en/communication/multiple-identities/)"
msgstr ""

#. type: Bullet: '- '
#: src/blog/2024-security-in-a-box-updates/index.md:28
#, markdown-text
msgid "[Secure your email communications](https://securityinabox.org/en/communication/secure-email/)"
msgstr ""

#. type: Bullet: '- '
#: src/blog/2024-security-in-a-box-updates/index.md:28
#, markdown-text
msgid "[Browse the web more securely](https://securityinabox.org/en/internet-connection/safer-browsing/)"
msgstr ""

#. type: Bullet: '- '
#: src/blog/2024-security-in-a-box-updates/index.md:28
#, markdown-text
msgid "[Anonymize your connections and communications](https://securityinabox.org/en/internet-connection/anonymity/)"
msgstr ""

#. type: Bullet: '- '
#: src/blog/2024-security-in-a-box-updates/index.md:28
#, markdown-text
msgid "[Destroy identifying information](https://securityinabox.org/en/files/destroy-identifying-information/)"
msgstr ""

#. type: Bullet: '    - '
#: src/blog/2024-security-in-a-box-updates/index.md:28
#: src/blog/2024-security-in-a-box-updates/index.md:40
#, markdown-text
msgid "[Protect your data when using YouTube](https://securityinabox.org/en/tools/youtube/)"
msgstr ""

#. type: Title ##
#: src/blog/2024-security-in-a-box-updates/index.md:29
#, markdown-text, no-wrap
msgid "Guides updated in 2024"
msgstr ""

#. type: Bullet: '- '
#: src/blog/2024-security-in-a-box-updates/index.md:40
#, markdown-text
msgid "[The whole section on passwords](https://securityinabox.org/en/passwords/)"
msgstr ""

#. type: Bullet: '- '
#: src/blog/2024-security-in-a-box-updates/index.md:40
#, markdown-text
msgid "[The whole section on phones and computers security](https://securityinabox.org/en/phones-and-computers/)"
msgstr ""

#. type: Bullet: '- '
#: src/blog/2024-security-in-a-box-updates/index.md:40
#, markdown-text
msgid "[The whole section on protecting files and information](https://securityinabox.org/en/files)"
msgstr ""

#. type: Bullet: '- '
#: src/blog/2024-security-in-a-box-updates/index.md:40
#, markdown-text
msgid "The following guides:"
msgstr ""

#. type: Bullet: '    - '
#: src/blog/2024-security-in-a-box-updates/index.md:40
#, markdown-text
msgid "[How the internet works and how it can be censored](https://securityinabox.org/en/internet-connection/how-the-internet-works/)"
msgstr ""

#. type: Bullet: '    - '
#: src/blog/2024-security-in-a-box-updates/index.md:40
#, markdown-text
msgid "[Circumvent internet blockages and monitoring](https://securityinabox.org/en/internet-connection/circumvention/)"
msgstr ""

#. type: Bullet: '    - '
#: src/blog/2024-security-in-a-box-updates/index.md:40
#, markdown-text
msgid "[Protect your data and communications when using Google services](https://securityinabox.org/en/tools/google/)"
msgstr ""

#. type: Bullet: '    - '
#: src/blog/2024-security-in-a-box-updates/index.md:40
#, markdown-text
msgid "[Protect yourself and your data when using Facebook]((https://securityinabox.org/en/tools/facebook/)"
msgstr ""

#. type: Title ##
#: src/blog/2024-security-in-a-box-updates/index.md:41
#, markdown-text, no-wrap
msgid "Future plans"
msgstr ""

#. type: Plain text
#: src/blog/2024-security-in-a-box-updates/index.md:44
#, markdown-text
msgid "In the next few months, we will keep updating and expanding Security in a Box, and in 2025 we are going to restructure it a bit more. If you are curious about what we are planning to do, have a look at [this Gitlab issue in particular](https://gitlab.com/securityinabox/securityinabox.gitlab.io/-/issues/237)."
msgstr ""

#. type: Plain text
#: src/blog/2024-security-in-a-box-updates/index.md:46
#, markdown-text
msgid "Security in a Box is an open-source resource, and we always welcome comments and suggestions. You can find instructions on how to give us feedback in [our Gitlab repository](https://gitlab.com/securityinabox/securityinabox.gitlab.io/-/blob/main/readme.md#how-to-give-feedback). We look forward to hearing from you, and to make Security in a Box always a bit more useful together with you!"
msgstr ""

#. type: Plain text
#: src/blog/2024-security-in-a-box-updates/index.md:47
#, markdown-text
msgid "_Image: [Illustration to promote cybersecurity submitted in the Cybersecurity Visuals Challenge 2019 hosted by OpenIDEO, by Rebecca Wang](https://commons.wikimedia.org/wiki/File:Cybersecurity_Visuals_Challenge_2019_-_Rebecca_Wang.jpg)"
msgstr ""
