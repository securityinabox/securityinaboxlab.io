# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-12-12 21:02+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: zh_hk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Yaml Front Matter Hash Value: author
#: src/blog/lets-encrypt-moving-towards-encrypted-web/index.md:1
#, no-wrap
msgid "Maria Xynou"
msgstr ""

#. type: Yaml Front Matter Hash Value: teaser
#: src/blog/lets-encrypt-moving-towards-encrypted-web/index.md:1
#, no-wrap
msgid "Do you own a website? If so, as of today Let's Encrypt, a new non-profit certificate authority (CA), entered Public Beta and allows you to add HTTPS to your website for free and more easily than ever before."
msgstr ""

#. type: Yaml Front Matter Hash Value: teaser_image
#: src/blog/lets-encrypt-moving-towards-encrypted-web/index.md:1
#, no-wrap
msgid "../../../media/en/blog/howitworks_certificate.png"
msgstr ""

#. type: Yaml Front Matter Hash Value: title
#: src/blog/lets-encrypt-moving-towards-encrypted-web/index.md:1
#, no-wrap
msgid "Lets Encrypt - Moving Towards an Encrypted Web"
msgstr ""

#. type: Plain text
#: src/blog/lets-encrypt-moving-towards-encrypted-web/index.md:10
msgid "![](../../../media/en/blog/howitworks_certificate.png)"
msgstr ""

#. type: Plain text
#: src/blog/lets-encrypt-moving-towards-encrypted-web/index.md:12
msgid "Do you own a website? If so, as of today [Let's Encrypt](https://letsencrypt.org/), a new non-profit certificate authority (CA), [entered Public Beta](https://letsencrypt.org/2015/12/03/entering-public-beta.html) and allows you to add HTTPS to your website for free and more easily than ever before."
msgstr ""

#. type: Plain text
#: src/blog/lets-encrypt-moving-towards-encrypted-web/index.md:14
msgid "If you've ever experienced the hassle of setting up a secure website, you'll be pleased to know that Let's Encrypt automates the entire process and only requires you to manage HTTPS with a few simple [commands](https://letsencrypt.readthedocs.org/en/latest/). This could be a huge step towards an encrypted Web."
msgstr ""

#. type: Title ##
#: src/blog/lets-encrypt-moving-towards-encrypted-web/index.md:15
#, no-wrap
msgid "Why TLS certificates are important"
msgstr ""

#. type: Plain text
#: src/blog/lets-encrypt-moving-towards-encrypted-web/index.md:18
msgid "When we access a website, our device makes an HTTP connection to the server that is hosting the website. But as HTTP connections are unencrypted, anyone can potentially intercept our connection and read our traffic or even re-direct us to a malicious server in order to infect our device with malware. To prevent this, it's important that our connections to websites are encrypted and that the websites we visit are secure."
msgstr ""

#. type: Plain text
#: src/blog/lets-encrypt-moving-towards-encrypted-web/index.md:20
msgid "HTTPS was designed for precisely this purpose. Through the use of Transport Layer Security (TLS), it encrypts HTTP connections to websites that support it. This is where digital certificates play an important role; they authenticate websites that support HTTPS so that we know we're communicating with the right servers. Websites that are TLS certified are more secure because they allow you to connect to them via encryption, limiting the probability of interception and increasing the confidentiality of exchanged data."
msgstr ""

#. type: Title ##
#: src/blog/lets-encrypt-moving-towards-encrypted-web/index.md:21
#, no-wrap
msgid "How Let's Encrypt helps us move towards an encrypted Web"
msgstr ""

#. type: Plain text
#: src/blog/lets-encrypt-moving-towards-encrypted-web/index.md:24
msgid "Up until today, obtaining a TLS certificate has been a difficult and expensive process which has discouraged many people from using HTTPS. Starting from today though, Let's Encrypt addresses this problem."
msgstr ""

#. type: Plain text
#: src/blog/lets-encrypt-moving-towards-encrypted-web/index.md:26
msgid "According to the [EFF](https://www.eff.org/deeplinks/2014/11/certificate-authority-encrypt-entire-web): *“Switching a webserver from HTTP to HTTPS with this CA will be as easy as issuing one command, or clicking one button.”*"
msgstr ""

#. type: Plain text
#: src/blog/lets-encrypt-moving-towards-encrypted-web/index.md:28
msgid "The [Let's Encrypt client](http://letsencrypt.org/howitworks/) handles the certificate request and authentication for you and, by default, renews your certificate automatically. It can also configure servers with your new certificate through a simple [command](https://letsencrypt.org/howitworks/). Detailed information on system requirements and on how to use Let's Encrypt's client to request for certificates can be found in its [documentation](https://letsencrypt.readthedocs.org/en/latest/)."
msgstr ""

#. type: Plain text
#: src/blog/lets-encrypt-moving-towards-encrypted-web/index.md:30
msgid "That said, it's important to bear in mind that Let's Encrypt just entered [Public Beta](https://letsencrypt.org/2015/12/03/entering-public-beta.html), which means that improvements (particularly on the client side) will still need to be made and it might contain some bugs. Yet, this is still an important step towards a more secure Web for everyone. Not only does Let's Encrypt advance TLS security best practices, but it's also [transparent](https://letsencrypt.org/about/) in the sense that all issued or revoked certificates will be publicly recorded and available for anyone to inspect. Furthermore, Let's Encrypt follows [open standards](https://letsencrypt.org/about/) by publishing its automatic issuance and renewal protocol."
msgstr ""

#. type: Plain text
#: src/blog/lets-encrypt-moving-towards-encrypted-web/index.md:31
#, no-wrap
msgid "Help make HTTPS the default across the Internet. Request a TLS certificate and let's encrypt.  \n"
msgstr ""
