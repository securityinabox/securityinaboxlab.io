# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2025-02-20 14:11+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Yaml Front Matter Hash Value: author
#: src/blog/dim-sum-guide/index.md:1
#, no-wrap
msgid "Haven"
msgstr ""

#. type: Yaml Front Matter Hash Value: teaser
#: src/blog/dim-sum-guide/index.md:1
#, no-wrap
msgid "Introducing the Dim Sum guide, a resource that builds on the content of Security in a Box and provides more specific instructions and guidance for Chinese-speaking readers from China"
msgstr ""

#. type: Yaml Front Matter Hash Value: teaser_image
#: src/blog/dim-sum-guide/index.md:1
#, no-wrap
msgid "../../../media/en/blog/dim-sum.png"
msgstr ""

#. type: Yaml Front Matter Hash Value: title
#: src/blog/dim-sum-guide/index.md:1
#, no-wrap
msgid "The Dim Sum Guide - digital security advice for Chinese-speaking readers"
msgstr ""

#. type: Plain text
#: src/blog/dim-sum-guide/index.md:10
#, markdown-text
msgid "![](../../../media/en/blog/dim-sum.png)"
msgstr ""

#. type: Plain text
#: src/blog/dim-sum-guide/index.md:12
#, markdown-text, no-wrap
msgid "*Access the Dim sum Guide on [https://yummydimsum.github.io/](https://yummydimsum.github.io/).*\n"
msgstr ""

#. type: Plain text
#: src/blog/dim-sum-guide/index.md:14
#, markdown-text
msgid "Some countries and regions experience more complex censorship and cyber-attacks against human rights defenders than others, and the advice provided by Security in a Box needs to be combined with guidance that addresses threats specific to those regions. People from China are one of the groups in need of contextualised digital protection. This is why our team, with the support of Front Line Defenders, completed the Dim Sum guide. It builds on the content of Security in a Box and provides more specific instructions and guidance for Chinese-speaking readers from China, focusing on particular aspects as detailed in the following paragraphs. Ideally one should read both guides together."
msgstr ""

#. type: Title ##
#: src/blog/dim-sum-guide/index.md:15
#, markdown-text, no-wrap
msgid "Restrictions and problems with the use of iPhones and iPads in China"
msgstr ""

#. type: Plain text
#: src/blog/dim-sum-guide/index.md:18
#, markdown-text
msgid "The Dim Sum guide devotes a section to the restrictions applied to the usage of iOS devices in China, including the lack of eSIM, FaceTime being only available for video but not for audio calls and heavy censorship of books, music, movies and other content which is available for consumption on these devices elsewhere in the world."
msgstr ""

#. type: Plain text
#: src/blog/dim-sum-guide/index.md:20
#, markdown-text
msgid "During the \"Sitongqiao Protests\" and the \"White Paper Movement\" in 2022, many Chinese people used AirDrop on iPhones to share images and videos related to the protests. But in early 2024 the Beijing authorities were apparently able to trace the mobile phone numbers and email accounts of those who sent these messages. Given the risk of tracing the identity of people who share files through AirDrop, the Dim Sum manual recommends not to use AirDrop at all."
msgstr ""

#. type: Title ##
#: src/blog/dim-sum-guide/index.md:21
#, markdown-text, no-wrap
msgid "Safer Chinese input methods"
msgstr ""

#. type: Plain text
#: src/blog/dim-sum-guide/index.md:24
#, markdown-text
msgid "Chinese input methods are mandatory software for Chinese language readers, and there is a risk of exposing sensitive information if one uses inappropriate and unsafe apps. It has been widely proved that Chinese input methods by some Chinese companies have security issues or even backdoors. Therefore the Dim Sum guide has a section dedicated to more secure Chinese input methods and their usage in different operating systems."
msgstr ""

#. type: Title ##
#: src/blog/dim-sum-guide/index.md:25
#, markdown-text, no-wrap
msgid "Secure VPNs available in China"
msgstr ""

#. type: Plain text
#: src/blog/dim-sum-guide/index.md:28
#, markdown-text
msgid "China has one of the strictest Internet filtering regimes in the world, and using a VPN to \"go over the [Great Fire-]wall\" has become a daily routine for many Chinese people. However, not all VPNs can work stably and are secure enough, and according to some reports the Chinese police force has even developed some VPNs with the specific intent of phishing the data of people who use them. The Dim sum guide recommends some more secure censorship circumvention solutions, and the team behind the manual constantly monitors and updates this section to provide the most current and relevant information on the topic."
msgstr ""

#. type: Title ##
#: src/blog/dim-sum-guide/index.md:29
#, markdown-text, no-wrap
msgid "Confronting the police in case of mobile phone searches"
msgstr ""

#. type: Plain text
#: src/blog/dim-sum-guide/index.md:32
#, markdown-text
msgid "In the wake of the \"White Paper Movement\" at the end of 2022, police in many Chinese cities have been randomly questioning passers-by on the street to check whether they have installed VPNs, Telegram and similar tools on their devices. The Dim Sum guide explains how ordinary people can respond to police checks of their mobile phones, both from a digital security perspective and from a legal perspective."
msgstr ""

#. type: Title ##
#: src/blog/dim-sum-guide/index.md:33
#, markdown-text, no-wrap
msgid "Conclusions"
msgstr ""

#. type: Plain text
#: src/blog/dim-sum-guide/index.md:36
#, markdown-text
msgid "We sincerely hope that the Dim Sum guide will help Chinese readers, especially Chinese human rights defenders, to protect their activities as well as their data and devices. However, we also know that the digital landscape is constantly shifting and that no resource can guarantee 100% security in any given context."
msgstr ""

#. type: Plain text
#: src/blog/dim-sum-guide/index.md:38
#, markdown-text
msgid "We will continue to monitor and update the Dim Sum guide and Security in a Box, to provide the latest and most appropriate digital security recommendations, and we very much welcome comments and suggestions from our readers. Send us feedback by [adding an issue to the Github where we developed the guide](https://github.com/YummyDimsum/yummydimsum.github.io/issues) or by writing an email to secure.resistance @ proton . me."
msgstr ""

#. type: Plain text
#: src/blog/dim-sum-guide/index.md:40
#, markdown-text
msgid "The Dim Sum guide can be accessed on:"
msgstr ""

#. type: Bullet: '- '
#: src/blog/dim-sum-guide/index.md:42
#, markdown-text
msgid "[https://yummydimsum.github.io/](https://yummydimsum.github.io/)"
msgstr ""

#. type: Plain text
#: src/blog/dim-sum-guide/index.md:44
#, markdown-text
msgid "The Dim Sum guide is also frequently shared on social media, so feel free to follow along:"
msgstr ""

#. type: Bullet: '- '
#: src/blog/dim-sum-guide/index.md:46
#, markdown-text
msgid "[Mastodon](https://mstdn.social/@dimsum)"
msgstr ""

#. type: Bullet: '- '
#: src/blog/dim-sum-guide/index.md:46
#, markdown-text
msgid "[Instagram](https://www.instagram.com/yummydimsumcook/)"
msgstr ""
