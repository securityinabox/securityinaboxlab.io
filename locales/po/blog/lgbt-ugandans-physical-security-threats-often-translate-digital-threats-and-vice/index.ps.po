# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-12-12 21:02+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: ps\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Yaml Front Matter Hash Value: author
#: src/blog/lgbt-ugandans-physical-security-threats-often-translate-digital-threats-and-vice/index.md:1
#, no-wrap
msgid "Spyros E. Monastiriotis"
msgstr ""

#. type: Yaml Front Matter Hash Value: teaser
#: src/blog/lgbt-ugandans-physical-security-threats-often-translate-digital-threats-and-vice/index.md:1
#, no-wrap
msgid "Uganda has been in the news headlines around the world since 2009, when it introduced its first Anti-homosexuality Bill and there have been several attempts since then to increase penalties for LGBT people and those who help them."
msgstr ""

#. type: Yaml Front Matter Hash Value: teaser_image
#: src/blog/lgbt-ugandans-physical-security-threats-often-translate-digital-threats-and-vice/index.md:1
#, no-wrap
msgid "../../../media/en/blog/kampala.jpg"
msgstr ""

#. type: Yaml Front Matter Hash Value: title
#: src/blog/lgbt-ugandans-physical-security-threats-often-translate-digital-threats-and-vice/index.md:1
#, no-wrap
msgid "For LGBT Ugandans, physical security threats often translate into digital threats and vice versa"
msgstr ""

#. type: Plain text
#: src/blog/lgbt-ugandans-physical-security-threats-often-translate-digital-threats-and-vice/index.md:10
msgid "![](../../../media/en/blog/kampala.jpg)"
msgstr ""

#. type: Plain text
#: src/blog/lgbt-ugandans-physical-security-threats-often-translate-digital-threats-and-vice/index.md:12
msgid "Uganda has been in the news headlines around the world since 2009, when it introduced its first [Anti-homosexuality Bill](http://www.publiceye.org/publications/globalizing-the-culture-wars/pdf/uganda-bill-september-09.pdf) and there have been [several attempts](http://www.publiceye.org/publications/globalizing-the-culture-wars/pdf/uganda-bill-september-09.pdf) since then to increase penalties for LGBT people and those who help them. Unsurprisingly, criminalization, physical violence and harassment has led many in the LGBT community to socialize, discuss and organize online. However, many LGBT people are now facing extreme digital threats, including abuse on social media and targeted malware attacks. Activists and trainers are working hard to help people protect themselves online, in one of the most hostile to LGBT people climates in Africa."
msgstr ""

#. type: Plain text
#: src/blog/lgbt-ugandans-physical-security-threats-often-translate-digital-threats-and-vice/index.md:14
msgid "Despite the political repression, Uganda has one of the most vibrant and outspoken LGBT movements in Africa, but the repercussions for those choosing to live publicly as LGBT can be severe."
msgstr ""

#. type: Plain text
#: src/blog/lgbt-ugandans-physical-security-threats-often-translate-digital-threats-and-vice/index.md:16
msgid "As one human rights defender says:"
msgstr ""

#. type: Plain text
#: src/blog/lgbt-ugandans-physical-security-threats-often-translate-digital-threats-and-vice/index.md:18
#, no-wrap
msgid "> LGBT persons in Uganda are under threat. Occasionally even their neighbors threaten to report them to the police.\n"
msgstr ""

#. type: Plain text
#: src/blog/lgbt-ugandans-physical-security-threats-often-translate-digital-threats-and-vice/index.md:20
msgid "LGBT people suffer [arbitrary arrests](http://76crimes.com/2015/01/27/uganda-9-gay-men-threatened-by-mob-arrested-by-police/), [physical assaults](http://www.hrw.org/news/2014/05/14/uganda-anti-homosexuality-act-s-heavy-toll) and are frequently [evicted](http://www.hrw.org/news/2014/05/14/uganda-anti-homosexuality-act-s-heavy-toll) from their homes. And in an atmosphere of [concerted political suppression](http://www.hrw.org/world-report/2014/country-chapters/uganda), opportunities for LGBT people to meet and organize are very limited."
msgstr ""

#. type: Plain text
#: src/blog/lgbt-ugandans-physical-security-threats-often-translate-digital-threats-and-vice/index.md:22
msgid "As a result, the internet has become hugely important to the Ugandan LGBT community. LGBT organizations have been launching [online petitions](https://www.change.org/p/international-criminal-court-investigate-and-prosecute-the-top-3-homophobic-ugandans-for-crimes-against-humanity), [running websites](https://sexualminoritiesuganda.com/) and participating in the human rights [debate](http://thelede.blogs.nytimes.com/2014/02/24/reaction-to-uganda-antigay-law/?_r=0) in Uganda, while LGBT individuals use internet platforms - such as dating websites - to network. But this creates further risks. One human rights defender said:"
msgstr ""

#. type: Plain text
#: src/blog/lgbt-ugandans-physical-security-threats-often-translate-digital-threats-and-vice/index.md:24
#, no-wrap
msgid "> There is a lot of hate speech on discussion forums. Often we find comments with threats, including death threats\n"
msgstr ""

#. type: Plain text
#: src/blog/lgbt-ugandans-physical-security-threats-often-translate-digital-threats-and-vice/index.md:26
msgid "[Facebook pages](https://www.facebook.com/IWasAliveWhenUgandaMade50) harassing and expressing hate speech against LGBT individuals have often been shared in Uganda. Newspapers have repeatedly [published](http://www.bbc.com/news/world-africa-26338941) the names and addresses of alleged LGBT individuals and in some cases have even used their Facebook pictures."
msgstr ""

#. type: Plain text
#: src/blog/lgbt-ugandans-physical-security-threats-often-translate-digital-threats-and-vice/index.md:28
msgid "In 2014 several LGBT organizations [received emails](https://freedomhouse.org/report/freedom-net/2014/uganda) affected by [Zeus malware](https://en.wikipedia.org/wiki/Zeus_(malware)), which is used as a “backdoor” to access personal online accounts. In early 2015 an organization had one of their email accounts hacked and information about their donors leaked; they were then asked to send part of a donation they had received days before to an organization based in Kenya."
msgstr ""

#. type: Plain text
#: src/blog/lgbt-ugandans-physical-security-threats-often-translate-digital-threats-and-vice/index.md:30
msgid "Frequent police raids on HIV/AIDS organization [offices](http://www.nature.com/nm/journal/v20/n5/full/nm0514-456.html) and at [events](https://freedomhouse.org/article/uganda-police-raid-lgbti-activists-workshop-kampala-condemned#.VP2mNcvaaPQ%20%7C%20http://www.nature.com/nm/journal/v20/n5/full/nm0514-456.html) also make it extremely risky to store sensitive information on devices. In addition, the 2014 [Anti-Pornography Act](http://www.ug-cert.ug/files/downloads/The-Anti-pornography-act-2014) holds Internet Service Providers (ISP) responsible for allowing pornography downloads through their services: such a provision can give way for surveillance and blocking of LGBT-related content by ISPs."
msgstr ""

#. type: Plain text
#: src/blog/lgbt-ugandans-physical-security-threats-often-translate-digital-threats-and-vice/index.md:32
msgid "There is a clear connection between online and offline threats to LGBT people. Online harassment – especially when real names and pictures are involved – can be reproduced in the offline space. Police raids pose not only physical threats but also threats to information stored on devices. Malware and hacking attempts can result into wealth and information loss and so on."
msgstr ""

#. type: Plain text
#: src/blog/lgbt-ugandans-physical-security-threats-often-translate-digital-threats-and-vice/index.md:34
msgid "One activist says:"
msgstr ""

#. type: Plain text
#: src/blog/lgbt-ugandans-physical-security-threats-often-translate-digital-threats-and-vice/index.md:36
#, no-wrap
msgid "> Data- and email-theft are of the greatest digital security risks LGBT communities face in Uganda. And for that reason, strong passwords, whole-drive encryption, anti-viruses and anti-malware are the main tactics we adopt.\n"
msgstr ""

#. type: Plain text
#: src/blog/lgbt-ugandans-physical-security-threats-often-translate-digital-threats-and-vice/index.md:38
msgid "Activists in the Ugandan LGBT community have been highly pro-active in raising awareness of digital threats and taking steps to counter them. [Digital security trainings](http://ifreedomuganda.net/#) have been taking place for some time, particularly with organizations. In these trainings activists learn how to stay secure online and offline and find out about new tools and techniques. But [limited digital literacy](https://www.unwantedwitness.or.ug/wp-content/uploads/2014/01/internet-they-are-coming-for-it-too.pdf) and [low internet penetration](http://opennetafrica.org/wp-content/uploads/researchandpubs/State%20of%20Internet%20Freedoms%20in%20Uganda%202014.pdf) make effective digital security training highly challenging."
msgstr ""

#. type: Plain text
#: src/blog/lgbt-ugandans-physical-security-threats-often-translate-digital-threats-and-vice/index.md:40
msgid "One trainer in Uganda says:"
msgstr ""

#. type: Plain text
#: src/blog/lgbt-ugandans-physical-security-threats-often-translate-digital-threats-and-vice/index.md:42
#, no-wrap
msgid "> One of the most outstanding challenges I face as a digital security trainer is the fact that most of our people don't have concrete background in the use of ICTs and computers. They just have a basic knowledge of connecting online to specific pages. As such, we have had to first give these people computer literacy trainings and later introduce them to digital security. […] We have very recently embarked on distributing Tactical Tech's Security in-a-box manuals to community organizations and members.\n"
msgstr ""

#. type: Plain text
#: src/blog/lgbt-ugandans-physical-security-threats-often-translate-digital-threats-and-vice/index.md:44
msgid "There is a combination of factors that increases both offline and online risks to LGBT Ugandans. Legal persecution and social prejudice are reproduced in the online space as digital risks. These risks feed then back to the cycle and often have consequences in the offline world. Digital risks are reinforced by an environment characterized by [loopholes in the protection of human rights](http://www.hrw.org/world-report/2014/country-chapters/uganda) and limited digital literacy. As such, there is an urgent need to increase LGBT Ugandans' awareness of digital risks and of the tools and tactics they can use to deal with these risks."
msgstr ""

#. type: Plain text
#: src/blog/lgbt-ugandans-physical-security-threats-often-translate-digital-threats-and-vice/index.md:46
msgid "[Tactical Technology Collective](https://tacticaltech.org/) has produced a series of toolkits tailored to the needs of LGBT communities in sub-Saharan Africa. They present and describe, in user-friendly way, the tools and tactics LGBT communities can undertake to deal with digital risks. If you are a member of Uganda's LGBT communities and you would like to find out more on how to protect your devices and online communications you can access Tactical Tech's guide [here](../../lgbti-africa) and [here](../../../media/en/blog/cf-lgbti-africa.pdf), for free. If you are interested in Tactical Tech's work on digital security in general, you can visit our [website](https://tacticaltech.org/) or the website dedicated to our project [Security in-a-box](../../)."
msgstr ""

#. type: Plain text
#: src/blog/lgbt-ugandans-physical-security-threats-often-translate-digital-threats-and-vice/index.md:48
msgid "You can follow me on [twitter](https://twitter.com/spyrosem)."
msgstr ""

#. type: Plain text
#: src/blog/lgbt-ugandans-physical-security-threats-often-translate-digital-threats-and-vice/index.md:50
msgid "Many thanks to [Geoffrey Wokulira Ssebaggala](http://www.frontlinedefenders.org/GeoffreySsebaggala) from [Unwanted Witness](https://unwantedwitness.or.ug/) and [Kelly Daniel Mukwano](https://twitter.com/KellyMukwano) from [i freedom Uganda](http://ifreedomuganda.net/) for their comments and support."
msgstr ""

#. type: Plain text
#: src/blog/lgbt-ugandans-physical-security-threats-often-translate-digital-threats-and-vice/index.md:51
msgid "The quotes have been picked from research on the digital security needs of African LGBT communities conducted by Tactical Tech staff. For security reasons the quotes have been anonymized."
msgstr ""
