# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-12-12 21:02+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: vi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Yaml Front Matter Hash Value: author
#: src/blog/how-recently-was-security-in-a-box-updated/index.md:1
#, no-wrap
msgid "Gus Andrews"
msgstr ""

#. type: Yaml Front Matter Hash Value: teaser
#: src/blog/how-recently-was-security-in-a-box-updated/index.md:1
#, no-wrap
msgid "Updates to tool guides on password managers, browser add-ons, and basic Android security."
msgstr ""

#. type: Yaml Front Matter Hash Value: teaser_image
#: src/blog/how-recently-was-security-in-a-box-updated/index.md:1
#, no-wrap
msgid "../../../media/en/blog/flickr-mtl_mauricio-oscar-in-a-box.jpg"
msgstr ""

#. type: Yaml Front Matter Hash Value: title
#: src/blog/how-recently-was-security-in-a-box-updated/index.md:1
#, no-wrap
msgid "How recently was Security In A Box updated? New in 2020"
msgstr ""

#. type: Plain text
#: src/blog/how-recently-was-security-in-a-box-updated/index.md:10
msgid "![](../../../media/en/blog/flickr-mtl_mauricio-oscar-in-a-box.jpg)"
msgstr ""

#. type: Plain text
#: src/blog/how-recently-was-security-in-a-box-updated/index.md:12
msgid "It can be hard to keep track of which parts of which digital security guides are up to date. Security in a Box has been incrementally updated over the years. To guide you to our most up-to-date content, we wanted to highlight these sections, which have been updated in English in the last six months:"
msgstr ""

#. type: Bullet: '* '
#: src/blog/how-recently-was-security-in-a-box-updated/index.md:18
msgid "[Create and Maintain Strong Passwords](../../passwords/passwords-and-2fa/)"
msgstr ""

#. type: Bullet: '* '
#: src/blog/how-recently-was-security-in-a-box-updated/index.md:18
msgid "[KeePassXC - Secure Password Manager for Windows](../../guide/keepassxc/windows/)"
msgstr ""

#. type: Bullet: '* '
#: src/blog/how-recently-was-security-in-a-box-updated/index.md:18
msgid "[KeePassDX - Mobile Password Manager for Android](../../guide/keepassdx/android/)"
msgstr ""

#. type: Bullet: '* '
#: src/blog/how-recently-was-security-in-a-box-updated/index.md:18
msgid "[Some sections of Firefox and Security Add-Ons for Windows - Secure Web Browser](../../guide/firefox/)"
msgstr ""

#. type: Bullet: '* '
#: src/blog/how-recently-was-security-in-a-box-updated/index.md:18
msgid "[Some sections of Basic security for Android](../../phones-and-computers/android/)"
msgstr ""

#. type: Plain text
#: src/blog/how-recently-was-security-in-a-box-updated/index.md:20
msgid "Note that translations of these pages into other languages are not necessarily up to date."
msgstr ""

#. type: Plain text
#: src/blog/how-recently-was-security-in-a-box-updated/index.md:22
msgid "We are currently working on dramatic changes to the structure and contents of SiaB. Among other things, we hope to deploy a more effective translation backend to make it easier to keep future versions of the site up to date."
msgstr ""

#. type: Plain text
#: src/blog/how-recently-was-security-in-a-box-updated/index.md:24
msgid "We welcome feedback on Security in a Box! Drop us a note and tell us: Which sections of SiaB do you depend on? What is currently useful to you? What is not so useful?"
msgstr ""

#. type: Plain text
#: src/blog/how-recently-was-security-in-a-box-updated/index.md:25
msgid "_Image: Flickr user mtl_mauricio, \"Oscar in a box\", CC-BY-2.0_"
msgstr ""
