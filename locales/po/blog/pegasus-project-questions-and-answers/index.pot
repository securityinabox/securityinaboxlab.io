# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-12-12 21:02+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Yaml Front Matter Hash Value: author
#: src/blog/pegasus-project-questions-and-answers/index.md:1
#, no-wrap
msgid "Mohammed Al-Maskati"
msgstr ""

#. type: Yaml Front Matter Hash Value: teaser
#: src/blog/pegasus-project-questions-and-answers/index.md:1
#, no-wrap
msgid "Amnesty International and the media platform Forbidden Stories have published an investigation into the use of an Israeli tool to spy on many people in the world. The investigation was called the Pegasus Project. Also available in Arabic, Russian and Chinese."
msgstr ""

#. type: Yaml Front Matter Hash Value: teaser_image
#: src/blog/pegasus-project-questions-and-answers/index.md:1
#, no-wrap
msgid "../../../media/en/blog/black-pegasus-wings.png"
msgstr ""

#. type: Yaml Front Matter Hash Value: title
#: src/blog/pegasus-project-questions-and-answers/index.md:1
#, no-wrap
msgid "Pegasus Project Questions and Answers"
msgstr ""

#. type: Plain text
#: src/blog/pegasus-project-questions-and-answers/index.md:10
#, markdown-text
msgid "![](../../../media/en/blog/black-pegasus-wings.png)"
msgstr ""

#. type: Plain text
#: src/blog/pegasus-project-questions-and-answers/index.md:12
#, markdown-text, no-wrap
msgid "**What happened?**\n"
msgstr ""

#. type: Plain text
#: src/blog/pegasus-project-questions-and-answers/index.md:14
#, markdown-text
msgid "Amnesty International and the media platform Forbidden Stories have published an investigation into the use of an Israeli tool to spy on many people in the world. The investigation was called the [Pegasus Project](https://forbiddenstories.org/case/the-pegasus-project/)."
msgstr ""

#. type: Plain text
#: src/blog/pegasus-project-questions-and-answers/index.md:16
#, markdown-text, no-wrap
msgid "**How was this information obtained?**\n"
msgstr ""

#. type: Plain text
#: src/blog/pegasus-project-questions-and-answers/index.md:18
#, markdown-text
msgid "Forbidden Stories and Amnesty International gained access to a leak of more than 50,000 records of phone numbers that were targeted by NSO clients."
msgstr ""

#. type: Plain text
#: src/blog/pegasus-project-questions-and-answers/index.md:20
#, markdown-text, no-wrap
msgid "**Who is the Israeli company and what is the tool used?**\n"
msgstr ""

#. type: Plain text
#: src/blog/pegasus-project-questions-and-answers/index.md:22
#, markdown-text
msgid "The company is [NSO](https://en.wikipedia.org/wiki/NSO_Group) and the tool that is used is called Pegasus. NSO claims to sell spyware and tools to governments to target criminals around the world."
msgstr ""

#. type: Plain text
#: src/blog/pegasus-project-questions-and-answers/index.md:24
#, markdown-text, no-wrap
msgid "**Who are the company's clients?**\n"
msgstr ""

#. type: Plain text
#: src/blog/pegasus-project-questions-and-answers/index.md:26
#, markdown-text
msgid "According to the documents, the company sold these programs to dozens of countries, but the countries that were exposed recently and used Pegasus against journalists and HRDs are: the UAE, Saudi Arabia, Bahrain, Morocco, Mexico, Azerbaijan, India, Hungary, Togo and Rwanda."
msgstr ""

#. type: Plain text
#: src/blog/pegasus-project-questions-and-answers/index.md:28
#, markdown-text, no-wrap
msgid "**Who are the victims?**\n"
msgstr ""

#. type: Plain text
#: src/blog/pegasus-project-questions-and-answers/index.md:30
#, markdown-text
msgid "According to the [documents](https://cdn.occrp.org/projects/project-p/#/), the victims are: journalists, human rights defenders, political activists, and politicians in countries and international organizations."
msgstr ""

#. type: Plain text
#: src/blog/pegasus-project-questions-and-answers/index.md:32
#, markdown-text, no-wrap
msgid "**If the number is 50,000, is everyone really infected with malware?**\n"
msgstr ""

#. type: Plain text
#: src/blog/pegasus-project-questions-and-answers/index.md:34
#, markdown-text
msgid "It is impossible to be sure that 50,000 people have been hacked, even if an individual's phone number appears in the list. However, the Amnesty International Security Lab, in partnership with Forbidden Stories, was able to perform technical analysis on dozens of phones and the results showed that at least 67 devices were infected with this software."
msgstr ""

#. type: Plain text
#: src/blog/pegasus-project-questions-and-answers/index.md:36
#, markdown-text, no-wrap
msgid "**How did the hack happen?**\n"
msgstr ""

#. type: Plain text
#: src/blog/pegasus-project-questions-and-answers/index.md:38
#, markdown-text
msgid "NSO has exploited some vulnerabilities in operating systems and applications on both IPhones and Android devices (for example in [WhatsApp](https://www.theguardian.com/technology/2020/jul/17/us-judge-whatsapp-lawsuit-against-israeli-spyware-firm-nso-can-proceed)). Malware was distributed by sending links to fake malicious sites (created to look similar to a legitimate site). Clicking on those links would install the malware on the device."
msgstr ""

#. type: Plain text
#: src/blog/pegasus-project-questions-and-answers/index.md:40
#, markdown-text
msgid "NSO has also recently exploited a vulnerability in the iMessage application available on the iOS operating system."
msgstr ""

#. type: Plain text
#: src/blog/pegasus-project-questions-and-answers/index.md:42
#, markdown-text
msgid "The so-called [zero-day vulnerabilities](https://en.wikipedia.org/wiki/Zero-day_(computing)) are used, which means that the vulnerability that a particular application or program suffers from is not known to developers and is known only to attackers because the vulnerability is not known to developers, no remedy has been developed prior to exploitation."
msgstr ""

#. type: Plain text
#: src/blog/pegasus-project-questions-and-answers/index.md:44
#, markdown-text, no-wrap
msgid "**What can Pegasus malware do?**\n"
msgstr ""

#. type: Plain text
#: src/blog/pegasus-project-questions-and-answers/index.md:46
#, markdown-text
msgid "When a device is infected with Pegasus, attackers can see everything a user does on their device, as if an unlocked phone was given to another person to use, with access to all its information and apps."
msgstr ""

#. type: Plain text
#: src/blog/pegasus-project-questions-and-answers/index.md:48
#, markdown-text
msgid "The software can be used to look at victims' phone and email messages, look at photos they took, eavesdrop on their calls, track their location and even take a photo."
msgstr ""

#. type: Plain text
#: src/blog/pegasus-project-questions-and-answers/index.md:50
#, markdown-text
msgid "Pegasus developers are getting better at hiding all traces of the software over time, making it difficult to confirm whether a particular phone has been hacked or not."
msgstr ""

#. type: Plain text
#: src/blog/pegasus-project-questions-and-answers/index.md:52
#, markdown-text, no-wrap
msgid "**Is it possible to check my device to see if I am infected or not?**\n"
msgstr ""

#. type: Plain text
#: src/blog/pegasus-project-questions-and-answers/index.md:54
#, markdown-text
msgid "The Pegasus software is very sophisticated and it is difficult to access accurate information about whether your device is infected with it or not. And it is difficult to detect it using anti-malware programs."
msgstr ""

#. type: Plain text
#: src/blog/pegasus-project-questions-and-answers/index.md:56
#, markdown-text
msgid "If you think you might be a target, it is advised to contact an expert in order to help you try to check your devices, However, the results might not be definitive."
msgstr ""

#. type: Plain text
#: src/blog/pegasus-project-questions-and-answers/index.md:58
#, markdown-text, no-wrap
msgid "**Amnesty International said that it issued a program to check devices?**\n"
msgstr ""

#. type: Plain text
#: src/blog/pegasus-project-questions-and-answers/index.md:60
#, markdown-text
msgid "Indeed, AI has issued [important instructions](https://github.com/mvt-project/mvt) for using a technical method to do this, but this method was intended for technical experts and not the average user."
msgstr ""

#. type: Plain text
#: src/blog/pegasus-project-questions-and-answers/index.md:62
#, markdown-text, no-wrap
msgid "**Are the victims iPhone users only?**\n"
msgstr ""

#. type: Plain text
#: src/blog/pegasus-project-questions-and-answers/index.md:64
#, markdown-text
msgid "No, the victims are users of all devices, Both iPhone or Android."
msgstr ""

#. type: Plain text
#: src/blog/pegasus-project-questions-and-answers/index.md:66
#, markdown-text, no-wrap
msgid "**Has Apple released an update to address these vulnerabilities?**\n"
msgstr ""

#. type: Plain text
#: src/blog/pegasus-project-questions-and-answers/index.md:68
#, markdown-text
msgid "No."
msgstr ""

#. type: Plain text
#: src/blog/pegasus-project-questions-and-answers/index.md:70
#, markdown-text
msgid "Apple released an important update on 19/July/2021 with iOS version [14.7](https://support.apple.com/en-gb/HT212601), and this update addresses serious security vulnerabilities that were previously discovered, However, new security vulnerabilities being exploited by NSO are not covered by this update"
msgstr ""

#. type: Plain text
#: src/blog/pegasus-project-questions-and-answers/index.md:72
#, markdown-text
msgid "Nonetheless we strongly recommend updating your device immediately. See [steps for update](https://support.apple.com/en-gb/guide/iphone/iph3e504502/ios)."
msgstr ""

#. type: Plain text
#: src/blog/pegasus-project-questions-and-answers/index.md:74
#, markdown-text, no-wrap
msgid "**Should I be worried?**\n"
msgstr ""

#. type: Plain text
#: src/blog/pegasus-project-questions-and-answers/index.md:76
#, markdown-text
msgid "We are all worried about the situation, but in the same time we recognise that worrying without taking practical steps to mitigate the risk will not help stop the process of targeting you with malware. So believe that concerns should be turned into motiation to develop digital protection knowledge as well as strengthening the protection of your phones and communications."
msgstr ""

#. type: Plain text
#: src/blog/pegasus-project-questions-and-answers/index.md:78
#, markdown-text, no-wrap
msgid "**What are the practical steps to take?**\n"
msgstr ""

#. type: Plain text
#: src/blog/pegasus-project-questions-and-answers/index.md:80
#, markdown-text
msgid "1- If you believe that you are infected with malware:"
msgstr ""

#. type: Bullet: '* '
#: src/blog/pegasus-project-questions-and-answers/index.md:86
#, markdown-text
msgid "Sign out of all accounts"
msgstr ""

#. type: Bullet: '* '
#: src/blog/pegasus-project-questions-and-answers/index.md:86
#, markdown-text
msgid "Change passwords for all accounts."
msgstr ""

#. type: Bullet: '* '
#: src/blog/pegasus-project-questions-and-answers/index.md:86
#, markdown-text
msgid "Enable two-step verification for accounts"
msgstr ""

#. type: Bullet: '* '
#: src/blog/pegasus-project-questions-and-answers/index.md:86
#, markdown-text
msgid "Stop using the device and disconnect the device from any internet network."
msgstr ""

#. type: Bullet: '* '
#: src/blog/pegasus-project-questions-and-answers/index.md:86
#, markdown-text
msgid "Contact an expert in digital protection to get help."
msgstr ""

#. type: Plain text
#: src/blog/pegasus-project-questions-and-answers/index.md:88
#, markdown-text
msgid "2- To protect your device in general:"
msgstr ""

#. type: Bullet: '* '
#: src/blog/pegasus-project-questions-and-answers/index.md:97
#, markdown-text
msgid "Check Security in-a-Box basic security guides for [Android](../../guide/basic-security/android/) and [iOS](../../guide/basic-security/ios/)"
msgstr ""

#. type: Bullet: '* '
#: src/blog/pegasus-project-questions-and-answers/index.md:97
#, markdown-text
msgid "We recommend to perform a factory reset of your phone from time to time in order to erase what may have been installed in your device. See steps for [Android](https://support.google.com/android/answer/6088915) and [iOS](https://support.apple.com/en-us/HT201252).  Note: Factory reset will remove all information stored on the phone, make sure that you have a recent, working backup of all information stored on the phone before performing factory reset."
msgstr ""

#. type: Bullet: '* '
#: src/blog/pegasus-project-questions-and-answers/index.md:97
#, markdown-text
msgid "Immediately update operating systems and all applications when updates become available."
msgstr ""

#. type: Bullet: '* '
#: src/blog/pegasus-project-questions-and-answers/index.md:97
#, markdown-text
msgid "Do not download applications from outside the official application store."
msgstr ""

#. type: Bullet: '* '
#: src/blog/pegasus-project-questions-and-answers/index.md:97
#, markdown-text
msgid "Use a Virtual Private Network (VPN) on mobile phones and computers."
msgstr ""

#. type: Bullet: '* '
#: src/blog/pegasus-project-questions-and-answers/index.md:97
#, markdown-text
msgid "Check links before opening them. Use [VirusTotal.com](https://www.virustotal.com)"
msgstr ""

#. type: Bullet: '* '
#: src/blog/pegasus-project-questions-and-answers/index.md:97
#, markdown-text
msgid "Use anti-malware software."
msgstr ""

#. type: Bullet: '* '
#: src/blog/pegasus-project-questions-and-answers/index.md:97
#, markdown-text
msgid "Review phone for unneccessary apps and uninstall if possible.  If not possible, try and disable them."
msgstr ""
