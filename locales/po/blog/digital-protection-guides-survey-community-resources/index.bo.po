# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-12-12 21:02+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: bo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Yaml Front Matter Hash Value: author
#: src/blog/digital-protection-guides-survey-community-resources/index.md:1
#, no-wrap
msgid "Gus Andrews"
msgstr ""

#. type: Yaml Front Matter Hash Value: teaser
#: src/blog/digital-protection-guides-survey-community-resources/index.md:1
#, no-wrap
msgid "Introducing an overview of resources on holistic digital, physical, and organizational security, privacy, and wellbeing. Viewable by language, community, and topic."
msgstr ""

#. type: Yaml Front Matter Hash Value: teaser_image
#: src/blog/digital-protection-guides-survey-community-resources/index.md:1
#, no-wrap
msgid "../../../media/en/blog/stanzebla-flickr-gate-lock.jpg"
msgstr ""

#. type: Yaml Front Matter Hash Value: title
#: src/blog/digital-protection-guides-survey-community-resources/index.md:1
#, no-wrap
msgid "Digital protection guides - A survey of community resources"
msgstr ""

#. type: Plain text
#: src/blog/digital-protection-guides-survey-community-resources/index.md:10
msgid "![](../../../media/en/blog/stanzebla-flickr-gate-lock.jpg)"
msgstr ""

#. type: Plain text
#: src/blog/digital-protection-guides-survey-community-resources/index.md:12
msgid "Security in a Box has been respected for many years as one of the most detailed, localized training resources in the internet freedom community. In the past fifteen years, the software our community uses for digital security has changed. SiaB has evolved, and the community has been blessed with many other excellent new resources as well. We are currently preparing to update SiaB, and as we do, we are taking into account the landscape of other guides that is now available."
msgstr ""

#. type: Plain text
#: src/blog/digital-protection-guides-survey-community-resources/index.md:14
msgid "As a community, we have changed our approach to training in the past decade and a half, too. We are more likely to look for tools and strategies that are easy to use. When we are doing our best, we take guidance from the communities who need to stay safe, rather than considering ourselves the experts and dictating what they need to do. We are more aware of gendered experiences of digital security and privacy. And curricula like [Level Up](https://level-up.cc/) have helped us better understand the specific needs of adult learners."
msgstr ""

#. type: Plain text
#: src/blog/digital-protection-guides-survey-community-resources/index.md:16
msgid "When we began to review the existing digital security and privacy guides, we soon realized the list we were compiling could be useful for others. So we are making it available to everyone who would like an overview of the materials the community has developed. We hope this makes it easier for trainers to find the guides that best serve their communities’ needs. We also hope it can help us all better coordinate production of new materials, as needed."
msgstr ""

#. type: Plain text
#: src/blog/digital-protection-guides-survey-community-resources/index.md:18
#, no-wrap
msgid "**[View the guide to digital protection guides here.](https://docs.google.com/spreadsheets/d/1LOc6SOJGWymaN4P1hc8ln3Zp-aGob_eKSr9B6MJ6ReE/edit#gid=1566213339)**\n"
msgstr ""

#. type: Plain text
#: src/blog/digital-protection-guides-survey-community-resources/index.md:20
#, no-wrap
msgid "**Please note this spreadsheet is presented in Google Sheets.** You do not need a Google account to view it. We do not generally endorse the use of Google products by at-risk populations. Google's business model is selling data about your viewing patterns to other companies, where it may end up in the hands of unethical data brokers or governments. However, in this case Google Sheets allowed us to present this data to you in a format we could not get a comparable open source tools to replicate. The \"filter views\" on this sheet allow different users to search this sheet for different kinds of guides (for example, translated in Amharic or for a low-literacy audience) and view different results simultaneously. We expect that if you use recommended tracker-blocker plugins (like uBlock Origin), it will help mitigate the potential tracking harm of viewing a Google-based site.\n"
msgstr ""

#. type: Title ##
#: src/blog/digital-protection-guides-survey-community-resources/index.md:21
#, no-wrap
msgid "For digital security trainers and those learning about digital security"
msgstr ""

#. type: Plain text
#: src/blog/digital-protection-guides-survey-community-resources/index.md:24
msgid "This “guide to the guides” includes materials produced by international digital rights organizations and our colleagues in the civil sphere, but also guides that are produced by individuals outside that sphere but used by trainers we know, as well as materials linked to by other guides. There are digital security and privacy materials in formats that range from short quizzes to tool guides to full lesson plans, meme images to podcasts. They are available in 58 languages, including Amharic, Aymara, Azerbaijani, Igbo, Kiswahili, nasa Yuwe, Pijao, Quechua dialects, Singhalese, Twi, and Yoruba."
msgstr ""

#. type: Plain text
#: src/blog/digital-protection-guides-survey-community-resources/index.md:26
msgid "You will find materials in here that are peripheral to the topics of privacy and security. As the digital rights movement has grown, we have welcomed related concerns into our tent. There is some material in here on fighting disinformation, a topic of rising concern for many of us. (We can add more of this, if there is interest; there has been a proliferation of work on this since 2015.) Because they overlap a great deal, we have included guides on privacy, sexuality, and relationship violence along with guides on gendered online harassment and defamation. There are also a couple of guides in here that are not exactly about security and privacy, but may be useful to those who need more background or need help explaining technologies or approaches to their colleagues. Those are tagged “research library.” They include Internews’s civicspace.tech guides, XYZ’s work on technology and gender, and some background on internet infrastructure by Womensnet ZA. Those guides contain material on mobile networks, blockchain technologies, artificial intelligence, and machine learning."
msgstr ""

#. type: Plain text
#: src/blog/digital-protection-guides-survey-community-resources/index.md:28
msgid "We hope to keep this sheet up to date, so if anything on here looks dangerously outdated let us know. Also, if you know of additional relevant guides that should be included—particularly in languages other than English and Spanish!—please let us know. We expect to also update this with information on secure software’s own documentation before too long."
msgstr ""

#. type: Title ##
#: src/blog/digital-protection-guides-survey-community-resources/index.md:29
#, no-wrap
msgid "For security guide writers, NGOs, and funders"
msgstr ""

#. type: Plain text
#: src/blog/digital-protection-guides-survey-community-resources/index.md:32
msgid "We expect this guide to the guides will be useful to you, too. Here are some observations we made as we put this spreadsheet together."
msgstr ""

#. type: Plain text
#: src/blog/digital-protection-guides-survey-community-resources/index.md:34
#, no-wrap
msgid "**Thinking about writing a new guide?** Consider localizing, curating and updating existing materials instead, with the goal of meeting a particular community’s threat model. There are currently over forty guides each in Spanish and in English. A majority of them are Crective Commons licensed for re-use. Consider running an event with a local community to localise existing security guide so it speaks the local language and supports the abilities and needs of that community. We all should carefully consider why it is that we are planning to write a new guide rather than update an existing guide and having it localized well.\n"
msgstr ""

#. type: Plain text
#: src/blog/digital-protection-guides-survey-community-resources/index.md:36
#, no-wrap
msgid "**Maintaining your existing guide?** To make updating easier, consider separating out:\n"
msgstr ""

#. type: Bullet: '* '
#: src/blog/digital-protection-guides-survey-community-resources/index.md:38
msgid "reasons why particular security advice is given, independent of personal threat model. Example: “don’t re-use passwords because there are lists of passwords floating around on the internet that make it easy for someone to get into more than one of your accounts,”"
msgstr ""

#. type: Bullet: '* '
#: src/blog/digital-protection-guides-survey-community-resources/index.md:40
msgid "strategies for specific threats faced by your readers and trainees and"
msgstr ""

#. type: Bullet: '* '
#: src/blog/digital-protection-guides-survey-community-resources/index.md:42
msgid "step-by-step guides to tools."
msgstr ""

#. type: Plain text
#: src/blog/digital-protection-guides-survey-community-resources/index.md:45
msgid "Why do we suggest this? Separating out different kinds of advice may make localization easier. These different parts of security advice need to be updated on different schedules. Separating them will make it easier to focus on what needs updating and when. The “why” of strategies for security generally does not need to be updated often. The reasoning behind using strong encryption to protect your documents hasn't changed much in the past decade, for example, though the tools we recommend for encrypting have. Specific threat models need periodic updating. Events unfold, like evolving government capabilities, new vulnerability disclosures, or groups of people migrating or organizing. Step-by-step guides to using tools need fast, regular updating as tools change all the time. These go out of date faster than any other part of a guide. Tool producers themselves may produce more updated guides than security trainers. Tails and Thunderbird have done an exemplary job with this. If developers aren’t producing usable guides or documentation, how can the community help make that happen?"
msgstr ""

#. type: Plain text
#: src/blog/digital-protection-guides-survey-community-resources/index.md:47
#, no-wrap
msgid "**We should all clearly list when our guides were last updated.** Many guides out there may include outdated material. Outdated material may put users at risk. Also consider when to take down or put a warning on outdated material.\n"
msgstr ""

#. type: Plain text
#: src/blog/digital-protection-guides-survey-community-resources/index.md:49
#, no-wrap
msgid "**Explain how your guide was localized if versions exist in multiple languages.** There can be a big difference in quality between co-produced guides or formally localized guides and the ones where a volunteer helped with translation. Co-produced guides will be sensitive to local conditions in which technology is used.\n"
msgstr ""

#. type: Plain text
#: src/blog/digital-protection-guides-survey-community-resources/index.md:51
#, no-wrap
msgid "**Consider using Creative Commons licensing.** We think it does lead to a certain amount of sustainability. Different projects are definitely using each other’s material, or at least linking to it. However, there isn’t much coordination of updates, or awareness of who is using updated material or who is not.\n"
msgstr ""

#. type: Plain text
#: src/blog/digital-protection-guides-survey-community-resources/index.md:53
#, no-wrap
msgid "**Consider organizing regular community events to update your materials.** A couple of guides currently have easy-to-use feedback options on individual articles, but still it appears people don’t leave feedback. Our most effective strategy for getting feedback was reaching out to individuals directly, and setting aside a specific time when people could work together on commenting.\n"
msgstr ""

#. type: Plain text
#: src/blog/digital-protection-guides-survey-community-resources/index.md:55
#, no-wrap
msgid "**There is an opportunity to better manage and coordinate security guide production in the internet freedom community.** Because so much material is on GitHub and GitLab, and so much of it is Creative Commons licensed, it would not be difficult for organizations with a stake in training guide production to collaboratively coordinate a repository. This could also include pulling in the documentation produced by tool developers themselves.\n"
msgstr ""

#. type: Plain text
#: src/blog/digital-protection-guides-survey-community-resources/index.md:57
#, no-wrap
msgid "**Planning to localize/translate into more languages should happen early in your guide development.** Think ahead about how you will localize, and how you will build a “workflow” out of the different pieces of software you will use (including collaborative editing software like office suites, version control software like GitHub or GitLab, and translation-specific software like Transifex). Producers of digital security guides could learn more about industry-standard documentation and localization processes that could make this easier for us.\n"
msgstr ""

#. type: Plain text
#: src/blog/digital-protection-guides-survey-community-resources/index.md:58
msgid "_Image: Flickr user stanzebla, \"Gate Lock\", CC-BY-SA-2.0_"
msgstr ""
