# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-12-12 21:02+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: hi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Yaml Front Matter Hash Value: author
#: src/blog/quick-security-recommendations-for-your-devices/index.md:1
#, no-wrap
msgid "Security in a Box"
msgstr ""

#. type: Yaml Front Matter Hash Value: teaser
#: src/blog/quick-security-recommendations-for-your-devices/index.md:1
#, no-wrap
msgid "A few first effective steps you can take to better protect your device"
msgstr ""

#. type: Yaml Front Matter Hash Value: teaser_image
#: src/blog/quick-security-recommendations-for-your-devices/index.md:1
#, no-wrap
msgid "../../../media/en/blog/polygonal-hand-holding-smartphone.png"
msgstr ""

#. type: Title #
#: src/blog/quick-security-recommendations-for-your-devices/index.md:1
#: src/blog/quick-security-recommendations-for-your-devices/index.md:11
#, markdown-text, no-wrap
msgid "Quick security recommendations for your devices"
msgstr ""

#. type: Plain text
#: src/blog/quick-security-recommendations-for-your-devices/index.md:10
#, markdown-text
msgid "![](../../../media/en/blog/polygonal-hand-holding-smartphone.png)"
msgstr ""

#. type: Plain text
#: src/blog/quick-security-recommendations-for-your-devices/index.md:14
#, markdown-text
msgid "People often ask us where they can start, what are minimum steps they should consider taking in an effort to better protect their devices. In this post we share what we are recommending as the first, often most effective and most important items."
msgstr ""

#. type: Title ##
#: src/blog/quick-security-recommendations-for-your-devices/index.md:15
#, markdown-text, no-wrap
msgid "General"
msgstr ""

#. type: Bullet: '1. '
#: src/blog/quick-security-recommendations-for-your-devices/index.md:23
#, markdown-text
msgid "Use [unique and strong passwords](../../passwords/passwords) for each account, using a [password manager](../../passwords/password-managers/) to safely store them."
msgstr ""

#. type: Bullet: '2. '
#: src/blog/quick-security-recommendations-for-your-devices/index.md:23
#, markdown-text
msgid "Use [two-factor authentication (2FA)](../../passwords/2fa) on [supported accounts](https://2fa.directory/). As a first choice, consider using [hardware devices (also called security keys)](#hardware). Otherwise you can also use apps or programs that generate time-based one-time passwords (TOTP). Avoid using SMS for 2FA if you can."
msgstr ""

#. type: Bullet: '3. '
#: src/blog/quick-security-recommendations-for-your-devices/index.md:23
#, markdown-text
msgid "Avoid using biometrics (face ID, fingerprint scan) as an authentication method. [We explain why in the guide on passwords](../../passwords/passwords/#avoid-fingerprint-or-face-unlock-biometrics)."
msgstr ""

#. type: Bullet: '4. '
#: src/blog/quick-security-recommendations-for-your-devices/index.md:23
#, markdown-text
msgid "Delete old files, documents, pictures, screenshots and chat history that you do not need on your device. Securely [back up](../../files/backup/) as necessary before removal."
msgstr ""

#. type: Bullet: '5. '
#: src/blog/quick-security-recommendations-for-your-devices/index.md:23
#, markdown-text
msgid "If you can, avoid installing social media apps: you can use them by accessing their website with your browser instead."
msgstr ""

#. type: Bullet: '6. '
#: src/blog/quick-security-recommendations-for-your-devices/index.md:23
#, markdown-text
msgid "Restart your device frequently. This ensures updates are applied properly and reduces the risk for cases of non-persistent malware."
msgstr ""

#. type: Plain text
#: src/blog/quick-security-recommendations-for-your-devices/index.md:31
#, markdown-text, no-wrap
msgid ""
"<a name=\"hardware\"></a>\n"
"- 2FA hardware token recommendations:\n"
"  * [Yubikey](https://www.yubico.com/products/)\n"
"  * [Nitrokey](https://www.nitrokey.com/products/nitrokeys)\n"
"  * [Solokeys](https://solokeys.com/)\n"
"  * [Thetis Key](https://thetis.io/)\n"
"  * [Google Titan Key](https://cloud.google.com/security/products/titan-security-key)\n"
msgstr ""

#. type: Bullet: '- '
#: src/blog/quick-security-recommendations-for-your-devices/index.md:37
#, markdown-text
msgid "2FA TOTP recommendations:"
msgstr ""

#. type: Bullet: '  * '
#: src/blog/quick-security-recommendations-for-your-devices/index.md:37
#, markdown-text
msgid "Computers: [KeePassXC](https://keepassxc.org)"
msgstr ""

#. type: Bullet: '  * '
#: src/blog/quick-security-recommendations-for-your-devices/index.md:37
#, markdown-text
msgid "Android: [Aegis](https://getaegis.app/)"
msgstr ""

#. type: Bullet: '  * '
#: src/blog/quick-security-recommendations-for-your-devices/index.md:37
#, markdown-text
msgid "iOS/iPhone: [Raivo OTP](https://github.com/raivo-otp/ios-application/blob/master/README.md)"
msgstr ""

#. type: Bullet: '  * '
#: src/blog/quick-security-recommendations-for-your-devices/index.md:37
#, markdown-text
msgid "Android and iOS: [FreeOTP](https://freeotp.github.io/)"
msgstr ""

#. type: Plain text
#: src/blog/quick-security-recommendations-for-your-devices/index.md:44
#, markdown-text, no-wrap
msgid ""
"**Further reading:**\n"
"   * [Creating Strong Passwords](https://ssd.eff.org/module/creating-strong-passwords)\n"
"   * [Privacy Guides - Threat modeling article](https://www.privacyguides.org/en/basics/threat-modeling/)\n"
"   * [Security Planner recommendations](https://securityplanner.consumerreports.org/recommendations)\n"
"   * [Open Brienfing - Digital security guidance](https://openbriefing.gitbook.io/defenders-protocol/digital)\n"
"   * [Surveillance Self-defense - Security plan](https://ssd.eff.org/module/your-security-plan)\n"
msgstr ""

#. type: Title ##
#: src/blog/quick-security-recommendations-for-your-devices/index.md:45
#, markdown-text, no-wrap
msgid "Android"
msgstr ""

#. type: Bullet: '1. '
#: src/blog/quick-security-recommendations-for-your-devices/index.md:54
#, markdown-text
msgid "Check that your [Android is up to date](https://support.google.com/android/answer/7680439) and that both your [version of Android](https://endoflife.date/android) and your device are still supported (check [Samsung](https://security.samsungmobile.com/workScope.smsb), [Google Pixel](https://support.google.com/pixelphone/answer/4457705), [Nokia](https://www.hmd.com/en_int/security-updates) or [Motorola](https://en-us.support.motorola.com/app/software-security-update). For other models, see [C. Scott Brown's article on the phone update policies from every major Android manufacturer](https://www.androidauthority.com/phone-update-policies-1658633/))."
msgstr ""

#. type: Bullet: '2. '
#: src/blog/quick-security-recommendations-for-your-devices/index.md:54
#, markdown-text
msgid "Automatically [update your apps](https://support.google.com/googleplay/answer/113412)."
msgstr ""

#. type: Bullet: '3. '
#: src/blog/quick-security-recommendations-for-your-devices/index.md:54
#, markdown-text
msgid "Enable [Play protect](https://support.google.com/googleplay/answer/2812853)."
msgstr ""

#. type: Bullet: '4. '
#: src/blog/quick-security-recommendations-for-your-devices/index.md:54
#, markdown-text
msgid "[Review the permissions](https://support.google.com/android/answer/9431959) your apps have access to."
msgstr ""

#. type: Bullet: '5. '
#: src/blog/quick-security-recommendations-for-your-devices/index.md:54
#, markdown-text
msgid "Review [installed apps](../../phones-and-computers/android/#remove-apps-that-you-do-not-need-and-do-not-use) and uninstall any unneeded/unknown ones."
msgstr ""

#. type: Bullet: '6. '
#: src/blog/quick-security-recommendations-for-your-devices/index.md:54
#, markdown-text
msgid "Ensure apps can only be installed from [trusted sources](../../phones-and-computers/android/#use-apps-from-trusted-sources)."
msgstr ""

#. type: Bullet: '7. '
#: src/blog/quick-security-recommendations-for-your-devices/index.md:54
#, markdown-text
msgid "Set a longer password (not a PIN or a pattern) to [protect access to your device](https://support.google.com/android/answer/9079129#zippy=%2Cstandard-locks)."
msgstr ""

#. type: Plain text
#: src/blog/quick-security-recommendations-for-your-devices/index.md:57
#, markdown-text, no-wrap
msgid ""
"**Further reading:**\n"
"  * [Protect your Android device](../../phones-and-computers/android/)\n"
msgstr ""

#. type: Title ##
#: src/blog/quick-security-recommendations-for-your-devices/index.md:58
#, markdown-text, no-wrap
msgid "iOS/iPhone"
msgstr ""

#. type: Bullet: '1. '
#: src/blog/quick-security-recommendations-for-your-devices/index.md:66
#, markdown-text
msgid "Check that your [iOS version](https://endoflife.date/ios) and [device](https://endoflife.date/iphone) are still supported and [up-to-date](https://support.apple.com/en-gb/guide/iphone/iph3e504502/ios)."
msgstr ""

#. type: Bullet: '2. '
#: src/blog/quick-security-recommendations-for-your-devices/index.md:66
#, markdown-text
msgid "Automatically [update your apps](https://support.apple.com/en-gb/102629)."
msgstr ""

#. type: Bullet: '3. '
#: src/blog/quick-security-recommendations-for-your-devices/index.md:66
#, markdown-text
msgid "Review the [permissions your apps have access to](https://support.apple.com/en-ie/guide/iphone/iph251e92810/ios)."
msgstr ""

#. type: Bullet: '4. '
#: src/blog/quick-security-recommendations-for-your-devices/index.md:66
#, markdown-text
msgid "Review installed apps and [uninstall any unneeded ones](https://support.apple.com/en-gb/guide/iphone/iph248b543ca/ios)."
msgstr ""

#. type: Bullet: '5. '
#: src/blog/quick-security-recommendations-for-your-devices/index.md:66
#, markdown-text
msgid "Switch on the [lockdown mode](https://support.apple.com/en-us/105120), which will also make it harder to compromise your device."
msgstr ""

#. type: Bullet: '6. '
#: src/blog/quick-security-recommendations-for-your-devices/index.md:66
#, markdown-text
msgid "Set a long [passcode](https://support.apple.com/en-us/119586) to protect access to your device."
msgstr ""

#. type: Plain text
#: src/blog/quick-security-recommendations-for-your-devices/index.md:69
#, markdown-text, no-wrap
msgid ""
"**Further reading:**\n"
"  * [Protect your iOS device](../../phones-and-computers/ios/)\n"
msgstr ""

#. type: Title ##
#: src/blog/quick-security-recommendations-for-your-devices/index.md:70
#, markdown-text, no-wrap
msgid "Windows"
msgstr ""

#. type: Bullet: '1. '
#: src/blog/quick-security-recommendations-for-your-devices/index.md:80
#, markdown-text
msgid "Ensure you are using a [supported version of Windows](https://endoflife.date/windows) with [automatic updates enabled](https://support.microsoft.com/en-au/topic/how-to-change-your-automatic-updates-settings-by-using-windows-security-center-804009cd-7931-fc07-5ada-6b157a854201)."
msgstr ""

#. type: Bullet: '2. '
#: src/blog/quick-security-recommendations-for-your-devices/index.md:80
#, markdown-text
msgid "Make sure that any software installed via the [Microsoft store is set to automatically update](https://support.microsoft.com/en-us/windows/turn-on-automatic-app-updates-70634d32-4657-dc76-632b-66048978e51b)."
msgstr ""

#. type: Bullet: '3. '
#: src/blog/quick-security-recommendations-for-your-devices/index.md:80
#, markdown-text
msgid "Ensure [Windows Defender is turned on](https://support.microsoft.com/en-us/windows/stay-protected-with-windows-security-2ae0363d-0ada-c064-8b56-6a39afb6a963). Activate Microsoft Defender rather than using a third-party antivirus."
msgstr ""

#. type: Bullet: '4. '
#: src/blog/quick-security-recommendations-for-your-devices/index.md:80
#, markdown-text
msgid "Consider using [Hardentools](https://github.com/securitywithoutborders/hardentools#readme) to disable some often abused features."
msgstr ""

#. type: Bullet: '5. '
#: src/blog/quick-security-recommendations-for-your-devices/index.md:80
#, markdown-text
msgid "Consider using [Simplewall to monitor](https://www.henrypp.org/product/simplewall) where you computer is connecting to."
msgstr ""

#. type: Bullet: '6. '
#: src/blog/quick-security-recommendations-for-your-devices/index.md:80
#, markdown-text
msgid "Ensure [Bitlocker - or Device Encryption - is turned on](https://support.microsoft.com/en-us/windows/turn-on-device-encryption-0c453637-bc88-5f74-5105-741561aae838)."
msgstr ""

#. type: Bullet: '7. '
#: src/blog/quick-security-recommendations-for-your-devices/index.md:80
#, markdown-text
msgid "Ensure your computer [requires a strong password](https://support.microsoft.com/en-us/windows/change-or-reset-your-windows-password-8271d17c-9f9e-443f-835a-8318c8f68b9c) to log in."
msgstr ""

#. type: Bullet: '8. '
#: src/blog/quick-security-recommendations-for-your-devices/index.md:80
#, markdown-text
msgid "[Review installed programs](https://support.microsoft.com/en-us/windows/how-to-check-if-an-app-or-program-is-installed-in-windows-10-5af73cea-f875-dfa0-4cd1-72a02aa06436) and [remove any that are no longer needed](../../phones-and-computers/windows/#remove-apps-that-you-do-not-need-and-do-not-use)."
msgstr ""

#. type: Plain text
#: src/blog/quick-security-recommendations-for-your-devices/index.md:84
#, markdown-text, no-wrap
msgid ""
"**Further reading:**\n"
"  * [Protect your Windows computer](../../phones-and-computers/windows/)\n"
"  * [Manage updates in Windows](https://support.microsoft.com/en-us/windows/manage-updates-in-windows-643e9ea7-3cf6-7da6-a25c-95d4f7f099fe)\n"
msgstr ""

#. type: Title ##
#: src/blog/quick-security-recommendations-for-your-devices/index.md:85
#, markdown-text, no-wrap
msgid "macOS"
msgstr ""

#. type: Bullet: '1. '
#: src/blog/quick-security-recommendations-for-your-devices/index.md:92
#, markdown-text
msgid "Ensure macOS [automatic updates are enabled](https://support.apple.com/guide/mac-help/keep-your-mac-up-to-date-mchlpx1065/mac#aria-mchlpa64b4a7) and that you use a [supported version of macOS](https://endoflife.date/macos )."
msgstr ""

#. type: Bullet: '2. '
#: src/blog/quick-security-recommendations-for-your-devices/index.md:92
#, markdown-text
msgid "Consider using [LuLu](https://objective-see.org/products/lulu.html) to monitor where you computer is connecting to."
msgstr ""

#. type: Bullet: '3. '
#: src/blog/quick-security-recommendations-for-your-devices/index.md:92
#, markdown-text
msgid "Make sure [FileVault is enabled](https://support.apple.com/en-vn/guide/mac-help/mh11785/mac)."
msgstr ""

#. type: Bullet: '4. '
#: src/blog/quick-security-recommendations-for-your-devices/index.md:92
#, markdown-text
msgid "Ensure your computer [requires a strong password](https://support.apple.com/en-vn/guide/mac-help/mchlp1550/14.0/mac/14.0) to log in."
msgstr ""

#. type: Bullet: '5. '
#: src/blog/quick-security-recommendations-for-your-devices/index.md:92
#, markdown-text
msgid "Review installed programs and [uninstall any that are not needed](../../phones-and-computers/mac/#remove-apps-that-you-do-not-need-and-do-not-use)."
msgstr ""

#. type: Plain text
#: src/blog/quick-security-recommendations-for-your-devices/index.md:97
#, markdown-text, no-wrap
msgid ""
"**Further reading:**\n"
"  * [Protect your Mac computer](../../phones-and-computers/mac/)\n"
"  * [About background updates in macOS](https://support.apple.com/en-us/101591)\n"
"  * [Set up your Mac to be secure](https://support.apple.com/en-ie/guide/mac-help/flvlt003/mac)\n"
msgstr ""

#. type: Title ##
#: src/blog/quick-security-recommendations-for-your-devices/index.md:98
#, markdown-text, no-wrap
msgid "Ubuntu Linux"
msgstr ""

#. type: Bullet: '1. '
#: src/blog/quick-security-recommendations-for-your-devices/index.md:104
#, markdown-text
msgid "Ensure that your version of Ubuntu is [still supported](https://endoflife.date/ubuntu) and that you are keeping it and the [installed software up-to-date](https://wiki.ubuntu.com/SoftwareUpdates)."
msgstr ""

#. type: Bullet: '2. '
#: src/blog/quick-security-recommendations-for-your-devices/index.md:104
#, markdown-text
msgid "Enable the [Firewall](https://help.ubuntu.com/community/Gufw) or consider using [OpenSnitch to monitor](https://github.com/evilsocket/opensnitch/wiki/Installation) where you computer is connecting to."
msgstr ""

#. type: Bullet: '3. '
#: src/blog/quick-security-recommendations-for-your-devices/index.md:104
#, markdown-text
msgid "Ensure [LUKS encryption is enabled](https://www.tecmint.com/encrypt-ubuntu-24-04-installation/) when you install the operating system."
msgstr ""

#. type: Bullet: '4. '
#: src/blog/quick-security-recommendations-for-your-devices/index.md:104
#, markdown-text
msgid "Ensure automatic login is disabled and [your account is set up with a strong password](https://help.ubuntu.com/stable/ubuntu-help/user-changepassword.html)."
msgstr ""

#. type: Plain text
#: src/blog/quick-security-recommendations-for-your-devices/index.md:106
#, markdown-text, no-wrap
msgid ""
"**Further reading:**\n"
"  * [Protect your Linux Device](../../phones-and-computers/linux/)\n"
msgstr ""
