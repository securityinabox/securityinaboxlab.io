---
title: For LGBT Ugandans, physical security threats often translate into digital threats and vice versa
post_date: 2015.03.13
author: Spyros E. Monastiriotis
teaser_image: ../../../media/en/blog/kampala.jpg
teaser: Uganda has been in the news headlines around the world since 2009, when it introduced its first Anti-homosexuality Bill and there have been several attempts since then to increase penalties for LGBT people and those who help them.
---

![](../../../media/en/blog/kampala.jpg)

Uganda has been in the news headlines around the world since 2009, when it introduced its first [Anti-homosexuality Bill](http://www.publiceye.org/publications/globalizing-the-culture-wars/pdf/uganda-bill-september-09.pdf) and there have been [several attempts](http://www.publiceye.org/publications/globalizing-the-culture-wars/pdf/uganda-bill-september-09.pdf) since then to increase penalties for LGBT people and those who help them. Unsurprisingly, criminalization, physical violence and harassment has led many in the LGBT community to socialize, discuss and organize online. However, many LGBT people are now facing extreme digital threats, including abuse on social media and targeted malware attacks. Activists and trainers are working hard to help people protect themselves online, in one of the most hostile to LGBT people climates in Africa.

Despite the political repression, Uganda has one of the most vibrant and outspoken LGBT movements in Africa, but the repercussions for those choosing to live publicly as LGBT can be severe.

As one human rights defender says:

> LGBT persons in Uganda are under threat. Occasionally even their neighbors threaten to report them to the police.

LGBT people suffer [arbitrary arrests](http://76crimes.com/2015/01/27/uganda-9-gay-men-threatened-by-mob-arrested-by-police/), [physical assaults](http://www.hrw.org/news/2014/05/14/uganda-anti-homosexuality-act-s-heavy-toll) and are frequently [evicted](http://www.hrw.org/news/2014/05/14/uganda-anti-homosexuality-act-s-heavy-toll) from their homes. And in an atmosphere of [concerted political suppression](http://www.hrw.org/world-report/2014/country-chapters/uganda), opportunities for LGBT people to meet and organize are very limited.

As a result, the internet has become hugely important to the Ugandan LGBT community. LGBT organizations have been launching [online petitions](https://www.change.org/p/international-criminal-court-investigate-and-prosecute-the-top-3-homophobic-ugandans-for-crimes-against-humanity), [running websites](https://sexualminoritiesuganda.com/) and participating in the human rights [debate](http://thelede.blogs.nytimes.com/2014/02/24/reaction-to-uganda-antigay-law/?_r=0) in Uganda, while LGBT individuals use internet platforms - such as dating websites - to network. But this creates further risks. One human rights defender said:

> There is a lot of hate speech on discussion forums. Often we find comments with threats, including death threats

[Facebook pages](https://www.facebook.com/IWasAliveWhenUgandaMade50) harassing and expressing hate speech against LGBT individuals have often been shared in Uganda. Newspapers have repeatedly [published](http://www.bbc.com/news/world-africa-26338941) the names and addresses of alleged LGBT individuals and in some cases have even used their Facebook pictures.

In 2014 several LGBT organizations [received emails](https://freedomhouse.org/report/freedom-net/2014/uganda) affected by [Zeus malware](https://en.wikipedia.org/wiki/Zeus_(malware)), which is used as a “backdoor” to access personal online accounts. In early 2015 an organization had one of their email accounts hacked and information about their donors leaked; they were then asked to send part of a donation they had received days before to an organization based in Kenya.

Frequent police raids on HIV/AIDS organization [offices](http://www.nature.com/nm/journal/v20/n5/full/nm0514-456.html) and at [events](https://freedomhouse.org/article/uganda-police-raid-lgbti-activists-workshop-kampala-condemned#.VP2mNcvaaPQ%20%7C%20http://www.nature.com/nm/journal/v20/n5/full/nm0514-456.html) also make it extremely risky to store sensitive information on devices. In addition, the 2014 [Anti-Pornography Act](http://www.ug-cert.ug/files/downloads/The-Anti-pornography-act-2014) holds Internet Service Providers (ISP) responsible for allowing pornography downloads through their services: such a provision can give way for surveillance and blocking of LGBT-related content by ISPs.

There is a clear connection between online and offline threats to LGBT people. Online harassment – especially when real names and pictures are involved – can be reproduced in the offline space. Police raids pose not only physical threats but also threats to information stored on devices. Malware and hacking attempts can result into wealth and information loss and so on.

One activist says:

> Data- and email-theft are of the greatest digital security risks LGBT communities face in Uganda. And for that reason, strong passwords, whole-drive encryption, anti-viruses and anti-malware are the main tactics we adopt.

Activists in the Ugandan LGBT community have been highly pro-active in raising awareness of digital threats and taking steps to counter them. [Digital security trainings](http://ifreedomuganda.net/#) have been taking place for some time, particularly with organizations. In these trainings activists learn how to stay secure online and offline and find out about new tools and techniques. But [limited digital literacy](https://www.unwantedwitness.or.ug/wp-content/uploads/2014/01/internet-they-are-coming-for-it-too.pdf) and [low internet penetration](http://opennetafrica.org/wp-content/uploads/researchandpubs/State%20of%20Internet%20Freedoms%20in%20Uganda%202014.pdf) make effective digital security training highly challenging.

One trainer in Uganda says:

> One of the most outstanding challenges I face as a digital security trainer is the fact that most of our people don't have concrete background in the use of ICTs and computers. They just have a basic knowledge of connecting online to specific pages. As such, we have had to first give these people computer literacy trainings and later introduce them to digital security. […] We have very recently embarked on distributing Tactical Tech's Security in-a-box manuals to community organizations and members.

There is a combination of factors that increases both offline and online risks to LGBT Ugandans. Legal persecution and social prejudice are reproduced in the online space as digital risks. These risks feed then back to the cycle and often have consequences in the offline world. Digital risks are reinforced by an environment characterized by [loopholes in the protection of human rights](http://www.hrw.org/world-report/2014/country-chapters/uganda) and limited digital literacy. As such, there is an urgent need to increase LGBT Ugandans' awareness of digital risks and of the tools and tactics they can use to deal with these risks.

[Tactical Technology Collective](https://tacticaltech.org/) has produced a series of toolkits tailored to the needs of LGBT communities in sub-Saharan Africa. They present and describe, in user-friendly way, the tools and tactics LGBT communities can undertake to deal with digital risks. If you are a member of Uganda's LGBT communities and you would like to find out more on how to protect your devices and online communications you can access Tactical Tech's guide [here](../../lgbti-africa) and [here](../../../media/en/blog/cf-lgbti-africa.pdf), for free. If you are interested in Tactical Tech's work on digital security in general, you can visit our [website](https://tacticaltech.org/) or the website dedicated to our project [Security in-a-box](../../).

You can follow me on [twitter](https://twitter.com/spyrosem).

Many thanks to [Geoffrey Wokulira Ssebaggala](http://www.frontlinedefenders.org/GeoffreySsebaggala) from [Unwanted Witness](https://unwantedwitness.or.ug/) and [Kelly Daniel Mukwano](https://twitter.com/KellyMukwano) from [i freedom Uganda](http://ifreedomuganda.net/) for their comments and support.

The quotes have been picked from research on the digital security needs of African LGBT communities conducted by Tactical Tech staff. For security reasons the quotes have been anonymized.
