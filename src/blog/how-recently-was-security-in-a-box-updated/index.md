---
title: How recently was Security In A Box updated? New in 2020
post_date: 2021.02.01
author: Gus Andrews
type: blog
teaser_image: ../../../media/en/blog/flickr-mtl_mauricio-oscar-in-a-box.jpg
teaser: Updates to tool guides on password managers, browser add-ons, and basic Android security.
---
![](../../../media/en/blog/flickr-mtl_mauricio-oscar-in-a-box.jpg)

It can be hard to keep track of which parts of which digital security guides are up to date. Security in a Box has been incrementally updated over the years. To guide you to our most up-to-date content, we wanted to highlight these sections, which have been updated in English in the last six months:

* [Create and Maintain Strong Passwords](../../passwords/passwords-and-2fa/)
* [KeePassXC - Secure Password Manager for Windows](../../guide/keepassxc/windows/)
* [KeePassDX - Mobile Password Manager for Android](../../guide/keepassdx/android/)
* [Some sections of Firefox and Security Add-Ons for Windows - Secure Web Browser](../../guide/firefox/)
* [Some sections of Basic security for Android](../../phones-and-computers/android/)

Note that translations of these pages into other languages are not necessarily up to date.

We are currently working on dramatic changes to the structure and contents of SiaB. Among other things, we hope to deploy a more effective translation backend to make it easier to keep future versions of the site up to date. 

We welcome feedback on Security in a Box! Drop us a note and tell us: Which sections of SiaB do you depend on? What is currently useful to you? What is not so useful?

_Image: Flickr user mtl_mauricio, "Oscar in a box", CC-BY-2.0_
