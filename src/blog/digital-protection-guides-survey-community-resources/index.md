---
title: Digital protection guides - A survey of community resources
post_date: 2021.03.24
author: Gus Andrews
teaser_image: ../../../media/en/blog/stanzebla-flickr-gate-lock.jpg
teaser: Introducing an overview of resources on holistic digital, physical, and organizational security, privacy, and wellbeing. Viewable by language, community, and topic.
---

![](../../../media/en/blog/stanzebla-flickr-gate-lock.jpg)

Security in a Box has been respected for many years as one of the most detailed, localized training resources in the internet freedom community. In the past fifteen years, the software our community uses for digital security has changed. SiaB has evolved, and the community has been blessed with many other excellent new resources as well. We are currently preparing to update SiaB, and as we do, we are taking into account the landscape of other guides that is now available.

As a community, we have changed our approach to training in the past decade and a half, too. We are more likely to look for tools and strategies that are easy to use. When we are doing our best, we take guidance from the communities who need to stay safe, rather than considering ourselves the experts and dictating what they need to do. We are more aware of gendered experiences of digital security and privacy. And curricula like [Level Up](https://level-up.cc/) have helped us better understand the specific needs of adult learners.

When we began to review the existing digital security and privacy guides, we soon realized the list we were compiling could be useful for others. So we are making it available to everyone who would like an overview of the materials the community has developed. We hope this makes it easier for trainers to find the guides that best serve their communities’ needs. We also hope it can help us all better coordinate production of new materials, as needed.

**[View the guide to digital protection guides here.](https://docs.google.com/spreadsheets/d/1LOc6SOJGWymaN4P1hc8ln3Zp-aGob_eKSr9B6MJ6ReE/edit#gid=1566213339)**

**Please note this spreadsheet is presented in Google Sheets.** You do not need a Google account to view it. We do not generally endorse the use of Google products by at-risk populations. Google's business model is selling data about your viewing patterns to other companies, where it may end up in the hands of unethical data brokers or governments. However, in this case Google Sheets allowed us to present this data to you in a format we could not get a comparable open source tools to replicate. The "filter views" on this sheet allow different users to search this sheet for different kinds of guides (for example, translated in Amharic or for a low-literacy audience) and view different results simultaneously. We expect that if you use recommended tracker-blocker plugins (like uBlock Origin), it will help mitigate the potential tracking harm of viewing a Google-based site.

## For digital security trainers and those learning about digital security

This “guide to the guides” includes materials produced by international digital rights organizations and our colleagues in the civil sphere, but also guides that are produced by individuals outside that sphere but used by trainers we know, as well as materials linked to by other guides. There are digital security and privacy materials in formats that range from short quizzes to tool guides to full lesson plans, meme images to podcasts. They are available in 58 languages, including Amharic, Aymara, Azerbaijani, Igbo, Kiswahili, nasa Yuwe, Pijao, Quechua dialects, Singhalese, Twi, and Yoruba.

You will find materials in here that are peripheral to the topics of privacy and security. As the digital rights movement has grown, we have welcomed related concerns into our tent. There is some material in here on fighting disinformation, a topic of rising concern for many of us. (We can add more of this, if there is interest; there has been a proliferation of work on this since 2015.) Because they overlap a great deal, we have included guides on privacy, sexuality, and relationship violence along with guides on gendered online harassment and defamation. There are also a couple of guides in here that are not exactly about security and privacy, but may be useful to those who need more background or need help explaining technologies or approaches to their colleagues. Those are tagged “research library.” They include Internews’s civicspace.tech guides, XYZ’s work on technology and gender, and some background on internet infrastructure by Womensnet ZA. Those guides contain material on mobile networks, blockchain technologies, artificial intelligence, and machine learning.

We hope to keep this sheet up to date, so if anything on here looks dangerously outdated let us know. Also, if you know of additional relevant guides that should be included—particularly in languages other than English and Spanish!—please let us know. We expect to also update this with information on secure software’s own documentation before too long.

## For security guide writers, NGOs, and funders

We expect this guide to the guides will be useful to you, too. Here are some observations we made as we put this spreadsheet together.

**Thinking about writing a new guide?** Consider localizing, curating and updating existing materials instead, with the goal of meeting a particular community’s threat model. There are currently over forty guides each in Spanish and in English. A majority of them are Crective Commons licensed for re-use. Consider running an event with a local community to localise existing security guide so it speaks the local language and supports the abilities and needs of that community. We all should carefully consider why it is that we are planning to write a new guide rather than update an existing guide and having it localized well.

**Maintaining your existing guide?** To make updating easier, consider separating out:

* reasons why particular security advice is given, independent of personal threat model. Example: “don’t re-use passwords because there are lists of passwords floating around on the internet that make it easy for someone to get into more than one of your accounts,”

* strategies for specific threats faced by your readers and trainees and

* step-by-step guides to tools.


Why do we suggest this? Separating out different kinds of advice may make localization easier. These different parts of security advice need to be updated on different schedules. Separating them will make it easier to focus on what needs updating and when. The “why” of strategies for security generally does not need to be updated often. The reasoning behind using strong encryption to protect your documents hasn't changed much in the past decade, for example, though the tools we recommend for encrypting have. Specific threat models need periodic updating. Events unfold, like evolving government capabilities, new vulnerability disclosures, or groups of people migrating or organizing. Step-by-step guides to using tools need fast, regular updating as tools change all the time. These go out of date faster than any other part of a guide. Tool producers themselves may produce more updated guides than security trainers. Tails and Thunderbird have done an exemplary job with this. If developers aren’t producing usable guides or documentation, how can the community help make that happen?

**We should all clearly list when our guides were last updated.** Many guides out there may include outdated material. Outdated material may put users at risk. Also consider when to take down or put a warning on outdated material.

**Explain how your guide was localized if versions exist in multiple languages.** There can be a big difference in quality between co-produced guides or formally localized guides and the ones where a volunteer helped with translation. Co-produced guides will be sensitive to local conditions in which technology is used.

**Consider using Creative Commons licensing.** We think it does lead to a certain amount of sustainability. Different projects are definitely using each other’s material, or at least linking to it. However, there isn’t much coordination of updates, or awareness of who is using updated material or who is not.

**Consider organizing regular community events to update your materials.** A couple of guides currently have easy-to-use feedback options on individual articles, but still it appears people don’t leave feedback. Our most effective strategy for getting feedback was reaching out to individuals directly, and setting aside a specific time when people could work together on commenting.

**There is an opportunity to better manage and coordinate security guide production in the internet freedom community.** Because so much material is on GitHub and GitLab, and so much of it is Creative Commons licensed, it would not be difficult for organizations with a stake in training guide production to collaboratively coordinate a repository. This could also include pulling in the documentation produced by tool developers themselves.

**Planning to localize/translate into more languages should happen early in your guide development.** Think ahead about how you will localize, and how you will build a “workflow” out of the different pieces of software you will use (including collaborative editing software like office suites, version control software like GitHub or GitLab, and translation-specific software like Transifex). Producers of digital security guides could learn more about industry-standard documentation and localization processes that could make this easier for us.

_Image: Flickr user stanzebla, "Gate Lock", CC-BY-SA-2.0_
