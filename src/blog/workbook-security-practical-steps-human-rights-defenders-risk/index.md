---
title: Workbook on Security - Practical Steps for Human Rights Defenders at Risk
post_date: 2015.03.13
author: Front Line Defenders
teaser_image: ../../../media/en/blog/fld-workbook-on-security1.jpg
teaser: The Workbook on Security is designed to raise awareness about security issues and to help human rights defenders consider how to mitigate threats. The workbook takes human rights defenders through the steps to producing a security plan - for individuals and for organisations.
---

![](../../../media/en/blog/fld-workbook-on-security1.jpg)

The Workbook on Security is designed to raise awareness about security issues and to help human rights defenders consider how to mitigate threats. The workbook takes human rights defenders through the steps to producing a security plan - for individuals and for organisations. It follows a systematic approach for assessing their security situation and developing risk and vulnerability reduction strategies and tactics. The workbook is available in [English](https://www.frontlinedefenders.org/en/file/1097/download?token=HDBq4NXR), [Arabic](https://www.frontlinedefenders.org/ar/file/1534/download?token=wR0PDCS1), [Chinese](https://www.frontlinedefenders.org/zh/file/2343/download?token=qLfz0LPP), [Dari](https://www.frontlinedefenders.org/en/file/3067/download?token=tSE8lBdh), [French](https://www.frontlinedefenders.org/fr/file/1652/download?token=A8lc3mrx), [Portuguese](https://www.frontlinedefenders.org/pt/file/2277/download?token=9hrswjtN), [Russian](https://www.frontlinedefenders.org/ru/file/2183/download?token=0TMpabhU), [Spanish](https://www.frontlinedefenders.org/es/file/1541/download?token=XfyVORpn) and [Urdu](https://www.frontlinedefenders.org/en/file/3068/download?token=orw-Jx_S).
