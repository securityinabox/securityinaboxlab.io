---
title: TrueCrypts Security Flaws - What Now?
post_date: 2015.10.08
author: Maria Xynou
teaser_image: ../../../media/en/blog/truecrypt_0.png
teaser: In the last week, critical security flaws have been reported in TrueCrypt, the open source software for file and disk encryption. As a result, we are reviewing our advice on file and disk encryption and we now recommend users to consider other tools for secure file storage.
---

![](../../../media/en/blog/truecrypt_0.png)

In the last week, [critical security flaws](https://thehackernews.com/2015/09/truecrypt-encryption-software.html) have been reported in [TrueCrypt](../../guide/truecrypt/windows), the open source software for file and disk encryption. As a result, we are reviewing our advice on file and disk encryption and we now recommend users to consider other tools for secure file storage.

## What's wrong with TrueCrypt?  

Serious [vulnerabilities](http://www.pcworld.com/article/2987439/encryption/newly-found-truecrypt-flaw-allows-full-system-compromise.html) were detected by Google zero-day researcher, James Forshaw, if an attacker has physical access to a device with TrueCrypt installed. While these security flaws don't allow an attacker to decrypt data directly, they can be exploited to compromise a machine, install spyware and to record password keystrokes – ultimately enabling an attacker to figure out a TrueCrypt user's decryption key.

Even though security engineers performed an audit of TrueCrypt which covered the driver code, they missed the following, which were [detected by Forshaw](https://thehackernews.com/2015/09/truecrypt-encryption-software.html):

* A vulnerability (CVE-2015-7358) which enables an attacker to can gain access to a running process and to get full administrative privileges
* A vulnerability (CVE-2015-7359) which enables an attacker to impersonate an authenticated user

## Why did we recommend TrueCrypt in the first place?

Up until now, we have recommended TrueCrypt not because it provided perfect security (there's no such thing as “perfect security” anyway), but because it was widely regarded as the best option for file and disk encryption for most users. In particular, [TrueCrypt](../../guide/truecrypt/windows):

* is open source (which enabled its audit from professional engineers around the world)
* implements strong AES encryption
* encrypts whole disks and external disks
* creates encrypted volumes and hidden volumes (which are useful features for many users)

Last year, however, TrueCrypt developers mysteriously [announced](http://www.theregister.co.uk/2014/05/28/truecrypt_hack) an end to the development of this software, quoting “unfixed security issues”. The reasons for this remain unknown and have been subject to some debate and speculation.

We nonetheless continued to recommend TrueCrypt because:

* there is a lack of clear alternatives that offer Windows users the same features
* a recent independent audit found no major security flaws in the source code
* there were at least two tools aimed at resurrecting this code and developing it further (CipherShed and VeraCrypt)

However, given that TrueCrypt is no longer maintained, the recently discovered bugs won't be fixed directly in the program's code (but will be in [VeraCrypt](https://veracrypt.codeplex.com/), see below). Furthermore, the last version of TrueCrypt is now out of date and getting increasingly awkward to install on current operating systems.

## What now?

The closest tool to TrueCrypt currently available is one of its forks, VeraCrypt. Like TrueCrypt, VeraCrypt is * open source * and independently audited. [VeraCrypt 1.15](https://veracrypt.codeplex.com/) was recently released and addresses the TrueCrypt vulnerabilities that were detected by Forshaw. CipherShed is another project that promises to improve on the TrueCrypt source code, but it is still some way off being released.

Another option is to use encryption built in to your device operating system, such as [BitLocker](http://windows.microsoft.com/en-US/windows-vista/BitLocker-Drive-Encryption-Overview) on Windows and [FileVault](http://osxdaily.com/2013/05/22/filevault-disk-encryption-mac/) on Mac OS X. These are very simple to use as they can be activated in the settings menu of your operating system. However, BitLocker and FileVault are proprietary tools, they haven't been audited and there is no guarantee that either company's tool has not been compromised by government agencies.

The table below shows some of the features for some TrueCrypt alternatives:

<div class="table-wrapper">

| **Software** | **License** | **Operating System (OS)** | **Whole disk encryption** | **Partition encryption** | **File encryption** |
|--------------|-------------|---------------------------|---------------------------|--------------------------|---------------------|
| VeraCrypt |Apache License 2.0, TrueCrypt License version 3.0 (legacy code only) | Windows, OS X, Linux | Yes |Yes | Yes |
| BitLocker |Proprietary (Miscrosoft) | Select editions of Windows Vista and later | Yes | Yes  |Yes |
| FileVault ('FileVault2', OS X 10.7+) | Proprietary (Apple Inc.) | OS X | Yes | Yes | No |
| Disk Utility | Proprietary (Apple Inc.) | OS X | No (external media only) |Yes |Yes |

</div>

Depending on their threat model and/or interest in trying out a different operating system, users can also consider switching to **Linux for full disk encryption**. The advantage is that - unlike [BitLocker](http://windows.microsoft.com/en-US/windows-vista/BitLocker-Drive-Encryption-Overview) or [FileVault](http://osxdaily.com/2013/05/22/filevault-disk-encryption-mac/) -  Linux's full disk encryption is open source. And as we learned from the latest report on TrueCrypt's vulnerabilities, it's important that the source code of the tools we use is open for review by engineers and researchers around the world.
