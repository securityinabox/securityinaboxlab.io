---
title: Pegasus Project Questions and Answers
post_date: 2021.07.23
author: Mohammed Al-Maskati
teaser_image: ../../../media/en/blog/black-pegasus-wings.png
teaser: "Amnesty International and the media platform Forbidden Stories have published an investigation into the use of an Israeli tool to spy on many people in the world. The investigation was called the Pegasus Project. Also available in Arabic, Russian and Chinese."
---

![](../../../media/en/blog/black-pegasus-wings.png)

**What happened?**

Amnesty International and the media platform Forbidden Stories have published an investigation into the use of an Israeli tool to spy on many people in the world. The investigation was called the [Pegasus Project](https://forbiddenstories.org/case/the-pegasus-project/).

**How was this information obtained?**

Forbidden Stories and Amnesty International gained access to a leak of more than 50,000 records of phone numbers that were targeted by NSO clients.

**Who is the Israeli company and what is the tool used?**

The company is [NSO](https://en.wikipedia.org/wiki/NSO_Group) and the tool that is used is called Pegasus. NSO claims to sell spyware and tools to governments to target criminals around the world.

**Who are the company's clients?**

According to the documents, the company sold these programs to dozens of countries, but the countries that were exposed recently and used Pegasus against journalists and HRDs are: the UAE, Saudi Arabia, Bahrain, Morocco, Mexico, Azerbaijan, India, Hungary, Togo and Rwanda.

**Who are the victims?**

According to the [documents](https://cdn.occrp.org/projects/project-p/#/), the victims are: journalists, human rights defenders, political activists, and politicians in countries and international organizations.

**If the number is 50,000, is everyone really infected with malware?**

It is impossible to be sure that 50,000 people have been hacked, even if an individual's phone number appears in the list. However, the Amnesty International Security Lab, in partnership with Forbidden Stories, was able to perform technical analysis on dozens of phones and the results showed that at least 67 devices were infected with this software.

**How did the hack happen?**

NSO has exploited some vulnerabilities in operating systems and applications on both IPhones and Android devices (for example in [WhatsApp](https://www.theguardian.com/technology/2020/jul/17/us-judge-whatsapp-lawsuit-against-israeli-spyware-firm-nso-can-proceed)). Malware was distributed by sending links to fake malicious sites (created to look similar to a legitimate site). Clicking on those links would install the malware on the device.

NSO has also recently exploited a vulnerability in the iMessage application available on the iOS operating system.

The so-called [zero-day vulnerabilities](https://en.wikipedia.org/wiki/Zero-day_(computing)) are used, which means that the vulnerability that a particular application or program suffers from is not known to developers and is known only to attackers because the vulnerability is not known to developers, no remedy has been developed prior to exploitation.

**What can Pegasus malware do?**

When a device is infected with Pegasus, attackers can see everything a user does on their device, as if an unlocked phone was given to another person to use, with access to all its information and apps.

The software can be used to look at victims' phone and email messages, look at photos they took, eavesdrop on their calls, track their location and even take a photo.

Pegasus developers are getting better at hiding all traces of the software over time, making it difficult to confirm whether a particular phone has been hacked or not.

**Is it possible to check my device to see if I am infected or not?**

The Pegasus software is very sophisticated and it is difficult to access accurate information about whether your device is infected with it or not. And it is difficult to detect it using anti-malware programs.

If you think you might be a target, it is advised to contact an expert in order to help you try to check your devices, However, the results might not be definitive.

**Amnesty International said that it issued a program to check devices?**

Indeed, AI has issued [important instructions](https://github.com/mvt-project/mvt) for using a technical method to do this, but this method was intended for technical experts and not the average user.

**Are the victims iPhone users only?**

No, the victims are users of all devices, Both iPhone or Android.

**Has Apple released an update to address these vulnerabilities?**

No.

Apple released an important update on 19/July/2021 with iOS version [14.7](https://support.apple.com/en-gb/HT212601), and this update addresses serious security vulnerabilities that were previously discovered, However, new security vulnerabilities being exploited by NSO are not covered by this update

Nonetheless we strongly recommend updating your device immediately. See [steps for update](https://support.apple.com/en-gb/guide/iphone/iph3e504502/ios).

**Should I be worried?**

We are all worried about the situation, but in the same time we recognise that worrying without taking practical steps to mitigate the risk will not help stop the process of targeting you with malware. So believe that concerns should be turned into motiation to develop digital protection knowledge as well as strengthening the protection of your phones and communications.

**What are the practical steps to take?**

1- If you believe that you are infected with malware:

* Sign out of all accounts
* Change passwords for all accounts.
* Enable two-step verification for accounts
* Stop using the device and disconnect the device from any internet network.
* Contact an expert in digital protection to get help.

2- To protect your device in general:

* Check Security in-a-Box basic security guides for [Android](../../guide/basic-security/android/) and [iOS](../../guide/basic-security/ios/)
* We recommend to perform a factory reset of your phone from time to time in order to erase what may have been installed in your device. See steps for [Android](https://support.google.com/android/answer/6088915) and [iOS](https://support.apple.com/en-us/HT201252).
  Note: Factory reset will remove all information stored on the phone, make sure that you have a recent, working backup of all information stored on the phone before performing factory reset.
* Immediately update operating systems and all applications when updates become available.
* Do not download applications from outside the official application store.
* Use a Virtual Private Network (VPN) on mobile phones and computers.
* Check links before opening them. Use [VirusTotal.com](https://www.virustotal.com)
* Use anti-malware software.
* Review phone for unneccessary apps and uninstall if possible.  If not possible, try and disable them.
