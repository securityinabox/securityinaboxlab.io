---
title: On the latest Security in a Box updates
post_date: 2025.02.18
author: Security in a Box
type: blog
teaser_image: ../../../media/en/blog/Cybersecurity_Visuals_Challenge_2019_-_Rebecca_Wang.jpg
teaser: Updates to tool guides on password managers, browser add-ons, and basic Android security.
---

![](../../../media/en/blog/Cybersecurity_Visuals_Challenge_2019_-_Rebecca_Wang.jpg)

Security in a Box was initially created [in 2005 as NGO in a Box - Security edition](https://web.archive.org/web/20060209174038/http://security.ngoinabox.org/), with its title changing to the current one [in 2009](https://web.archive.org/web/20090522065822/http://security.ngoinabox.org:80/). Over these 20 years, it has been developed on different platforms and has repeatedly changed its structure and content, with one fundamental goal in mind: offering useful strategies and tools for human rights defenders, journalists, activists and civil society members to protect themselves against the most widespread digital threats they face in their everyday life.

At the beginning of each page, immediately after the title, you can check when a guide has been last updated, to make sure that the recommendations you are going to read still apply to the current situation.

In 2024 most of the guides in Security in a Box have been reviewed and updated and some more have been created from scratch. All this new and updated content has been completely translated into French, Indonesian, Portuguese, Russian, Spanish, Turkish and Vietnamese, and is being translated into Arabic, Burmese, Chinese, Farsi and Pashto (this latter translation has been funded by [UN Women](https://www.unwomen.org/en)).

Here's a list of what is new and what is updated in the entire website:

## New guides on Security in a Box

- [Create and protect multiple online identities](https://securityinabox.org/en/communication/multiple-identities/)
- [Secure your email communications](https://securityinabox.org/en/communication/secure-email/)
- [Browse the web more securely](https://securityinabox.org/en/internet-connection/safer-browsing/)
- [Anonymize your connections and communications](https://securityinabox.org/en/internet-connection/anonymity/)
- [Destroy identifying information](https://securityinabox.org/en/files/destroy-identifying-information/)
- [Protect your data when using YouTube](https://securityinabox.org/en/tools/youtube/)

## Guides updated in 2024

- [The whole section on passwords](https://securityinabox.org/en/passwords/)
- [The whole section on phones and computers security](https://securityinabox.org/en/phones-and-computers/)
- [The whole section on protecting files and information](https://securityinabox.org/en/files)
- The following guides:
    - [How the internet works and how it can be censored](https://securityinabox.org/en/internet-connection/how-the-internet-works/)
    - [Circumvent internet blockages and monitoring](https://securityinabox.org/en/internet-connection/circumvention/)
    - [Protect your data and communications when using Google services](https://securityinabox.org/en/tools/google/)
    - [Protect your data when using YouTube](https://securityinabox.org/en/tools/youtube/)
    - [Protect yourself and your data when using Facebook]((https://securityinabox.org/en/tools/facebook/)

## Future plans

In the next few months, we will keep updating and expanding Security in a Box, and in 2025 we are going to restructure it a bit more. If you are curious about what we are planning to do, have a look at [this Gitlab issue in particular](https://gitlab.com/securityinabox/securityinabox.gitlab.io/-/issues/237).

Security in a Box is an open-source resource, and we always welcome comments and suggestions. You can find instructions on how to give us feedback in [our Gitlab repository](https://gitlab.com/securityinabox/securityinabox.gitlab.io/-/blob/main/readme.md#how-to-give-feedback). We look forward to hearing from you, and to make Security in a Box always a bit more useful together with you!

_Image: [Illustration to promote cybersecurity submitted in the Cybersecurity Visuals Challenge 2019 hosted by OpenIDEO, by Rebecca Wang](https://commons.wikimedia.org/wiki/File:Cybersecurity_Visuals_Challenge_2019_-_Rebecca_Wang.jpg)
