---
weight: 999
post_date: 19 September 2024
title: Related Tools
subtitle: Internet Connection
topic: internet-connection
---

Use the following tools to browse the web more securely, to access blocked online services and websites, to help hide your online activity, and to remain anonymous online. You can also learn more about [how to browse the web safely](../safer-browsing), [how to visit blocked websites](../circumvention) and [how to anonymize your connections](../anonymity), as well as find out about [tools that can help you keep your online communication private](../../communication/tools/).

# Browse the web more securely

## Browsers

### [Firefox](https://www.mozilla.org/en-US/firefox/)

[![](../../../media/en/logos/firefox-logo-64x64.png)](https://www.mozilla.org/firefox/new/)
(Android, iOS, Linux, macOS, Windows)

To protect your privacy and security, we strongly recommend using the open-source Firefox browser to view web pages, instead of other less privacy-friendly and less secure browsers like Edge, Opera, or Safari.

[Download](https://www.mozilla.org/en-US/firefox/download/thanks/) | See [their guide](https://support.mozilla.org/en-US/products/firefox/privacy-and-security) and [our recommendations on how to browse the web securely](../safer-browsing)

### [Firefox Focus](https://www.mozilla.org/en-US/firefox/browsers/mobile/focus/)

[![](../../../media/en/logos/firefox-focus-logo.png)](https://www.mozilla.org/en-US/firefox/browsers/mobile/focus/)
(Android, iOS)

A lightweight version of Firefox with automatic tracking protection and ad blocking built in.

[Download](https://www.mozilla.org/firefox/browsers/mobile/focus/) | See [their guide](https://support.mozilla.org/en-US/products/firefox/privacy-and-security) and [our recommendations on how to browse the web securely](../safer-browsing)

### [Chromium](https://www.chromium.org/Home/)

[![](../../../media/en/logos/chromium-logo.png)](https://www.chromium.org/Home/)
(Android, Linux)

Chromium is an open-source browser focused on offering a safer, faster, and more stable web browsing experience. On Linux, you can install Chromium through your software installer in many distributions. On Android, you can install it through [FFUpdater](#ffupdater).

On Windows, macOS and iOS, you can use [Google Chrome](https://www.google.com/intl/en_us/chrome/), which uses Chromium's free and open-source code. Since both browsers use the same code, in most cases Chrome's documentation also applies to Chromium.

Since Chromium and Chrome are developed by Google, consider whether they might send more data about you to Google than you are comfortable with and, if possible, avoid logging in to your Google account when using them.

See [Chrome's guide on security](https://support.google.com/chrome?sjid=2999784176296019534-EU#topic=7437824) and [our recommendations on how to browse the web securely](../safer-browsing)


### [FFUpdater](https://github.com/Tobi823/ffupdater)

[![](../../../media/en/logos/ffupdater-logo.png)](https://github.com/Tobi823/ffupdater)
(Android)

A free and open-source installer and updater app that can help you manage browsers like Firefox (and other based on it, like Tor Browser), Chromium (and other based on it, like DuckDuckGo Browser), K-9, Orbot and other. You can get it from [F-Droid](https://f-droid.org/en/packages/de.marmaro.krt.ffupdater/).

[Download](https://f-droid.org/packages/de.marmaro.krt.ffupdater/) | See [their documentation](https://github.com/Tobi823/ffupdater?tab=readme-ov-file#additional-documentation)


### [DuckDuckGo Privacy Browser](https://duckduckgo.com/duckduckgo-help-pages/get-duckduckgo/browser/)

[![](../../../media/en/logos/DuckDuckGo-logo.jpg)](https://duckduckgo.com/duckduckgo-help-pages/get-duckduckgo/browser/)
(Windows, macOS, iOS and Android)

A privacy-oriented browser that automatically blocks web trackers and upgrades insecure HTTP connections to HTTPS when possible.

DuckDuckGo Browser is developed by DuckDuckGo, a company that also maintains the DuckDuckGo search engine and develops the DuckDuckGo privacy extension or add-on for several browsers including Firefox and Chrome/Chromium.

[Install](https://duckduckgo.com/duckduckgo-help-pages/get-duckduckgo/) | See [their documentation](https://duckduckgo.com/duckduckgo-help-pages/)

## Add-ons and extensions for general privacy and security

While we recommend Firefox, many of the following add-ons/extensions are available for other browsers as well.

### [Privacy Badger](https://privacybadger.org/)

[![](../../../media/en/logos/privacy-badger-logo.jpg)](https://privacybadger.org/)
(Firefox, Chrome/Chromium, Firefox on Android, Edge and Opera)

A free and open-source add-on/extension that blocks trackers used to gather your data when you browse the web.

[Download](https://privacybadger.org/) | See [their FAQ](https://privacybadger.org/)

### [uBlock Origin](https://ublockorigin.com/)

[![](../../../media/en/logos/ublock-logo.png)](https://ublockorigin.com/)
(Firefox, Firefox for Android, Edge, Opera)

A free and open-source content blocker that stops pop-up windows and can help protect you from spyware and malware when you browse the web.

[Download](https://ublockorigin.com/) | See [their documentation](https://github.com/gorhill/uBlock/wiki)

### [uBlock Origin Lite](https://ublockorigin.com/)

[![](../../../media/en/logos/uBlock_Origin_lite-logo.png)](https://github.com/uBlockOrigin/uBOL-home)
(Chrome/Chromium)

A free and open-source content blocker that stops pop-up windows and can help protect you from spyware and malware when you browse the web.

[Download](https://chromewebstore.google.com/detail/ublock-origin-lite/ddkjiahejlhfcafbddmgiahcphecmpfh) | See [their documentation](https://github.com/uBlockOrigin/uBOL-home/blob/main/README.md)

### [Facebook Container](https://www.mozilla.org/firefox/facebookcontainer/)

[![](../../../media/en/logos/Facebook-Container-logo.png)](https://www.mozilla.org/firefox/facebookcontainer/)
(Firefox)

A free and open-source add-on that keeps Facebook and Instagram from gathering information on you as you browse the web.

[Download](https://addons.mozilla.org/firefox/addon/facebook-container/) | See [their guide](https://github.com/mozilla/contain-facebook#readme)

## [Disconnect Private Browsing](https://disconnect.me/disconnect)

[![](../../../media/en/logos/disconnect-logo.jpg)](https://disconnect.me/disconnect)
(Firefox, Chrome/Chromium, Opera, Safari)

A browser add-on/extension that blocks most third-parties' tracking requests as you browse the web.

[Download](https://disconnect.me/disconnect) | See [their FAQ](https://disconnect.me/help)

### [StartPage Privacy Protection Extension](https://add.startpage.com/protection/)

[![](../../../media/en/logos/startpage-logo.png)](https://add.startpage.com/protection/) (Firefox, Chrome/Chromium, Opera, Safari)

A browser extension that prevents advertisers and other third parties from tracking your web browsing.

[Install](https://add.startpage.com/protection/) | See [their support center](https://support.startpage.com/hc/en-us/sections/26194864251156-Privacy-Protection-Extension)

## [NoScript](https://noscript.net/)

[![](../../../media/en/logos/NoScript-logo.png)](https://noscript.net/)
(Firefox, Chrome/Chromium and other Chromium-based browsers like Edge, Brave or Vivaldi)

Free and open-source add-on/extension that stops malware on websites from infecting your device.

NoScript can make websites look empty or broken. [Learn how to configure NoScript so this happens less often](https://noscript.net/features).

[Download](https://noscript.net/getit) | See [their guide](https://noscript.net/usage/)

## More private search engines

### [DuckDuckGo](https://duckduckgo.com/)

[![](../../../media/en/logos/duckduckgo-logo.png)](https://duckduckgo.com/)

A search engine that does not store or sell your personal information.

See [their privacy policy](https://duckduckgo.com/privacy) and [their help pages](https://duckduckgo.com/duckduckgo-help-pages/)

### [StartPage](https://www.startpage.com/)

[![](../../../media/en/logos/startpage-logo.png)](https://www.startpage.com/)

A search engine that does not store or sell your personal information.

See [their support center](https://support.startpage.com/hc/en-us)

# Visit blocked websites

## Choose a VPN

### [Proton VPN](https://protonvpn.com/)

[![](../../../media/en/logos/ProtonVPN_logo.png)](https://protonvpn.com/)
(Windows, macOS, Linux, iOS, Android - also available as a multiplatform browser extension for Chrome/Chromium, Firefox, Android TV and Chromebook)

An application that provides access to some blocked websites while helping to hide your online activity from your ISP and from the websites and services you access. Free for one device and limited speed; offers improved performance for a monthly fee.

[Download](https://protonvpn.com/download) | See [their documentation](https://protonvpn.com/support)

### [Riseup VPN](https://riseup.net/vpn)

[![](../../../media/en/logos/riseup-logo.png)](https://riseup.net/vpn)
(Android, Linux, macOS, Windows)

A free, activist-focused application that provides access to some blocked websites while helping to hide your online activity.

[Download](https://riseup.net/en/vpn#download-riseupvpn) | See [their guide](https://riseup.net/vpn)

### [TunnelBear VPN](https://www.tunnelbear.com/)

[![](../../../media/en/logos/tunnelbear-logo-64x64.jpg)](https://www.tunnelbear.com)
(Android, iOS, macOS, Windows - also available as a multiplatform browser extension for Chrome/Chromium, Firefox and Edge)

An application that provides access to some blocked websites while helping to hide your online activity from your ISP and from the websites and services you access. Free for up to 2 GB/month on one device (or up to 10 GB in selected countries). Supports unlimited devices and unlimited data for a fee.

[Download](https://www.tunnelbear.com/pricing) | See [their guide](https://help.tunnelbear.com/hc/en-us/categories/360000518011-Getting-to-know-your-Bear)


## Install your own VPN

### [Algo VPN](https://github.com/trailofbits/algo)

[![](../../../media/en/logos/algo-logo.png)](https://github.com/trailofbits/algo)
(Linux, macOS, Windows, Android)

An open-source application developed by the security company Trail of Bits that makes it easier for individuals and organizations to administer their own secure VPN service. Administrators can then give other users access to blocked websites while helping them hide their online activity.

[Set up a server](https://github.com/trailofbits/algo#deploy-the-algo-server) | [Configure the VPN clients](https://github.com/trailofbits/algo)

### [Outline VPN](https://getoutline.org)

[![](../../../media/en/logos/outline-logo.png)](https://getoutline.org)
(Android, iOS, Linux, macOS, Windows, Chrome/Chromium extension. Management app: Linux, macOS, Windows)

An open-source application developed by Google that makes it easier for individuals and organizations to administer their own secure VPN service. Administrators can then give other users access to blocked websites while helping them hide their online activity.

[Set up a server and download a client](https://getoutline.org/get-started/) | [Learn how it works](https://getoutline.org/how-it-works/)

### [Amnezia VPN](https://amnezia.org/en/self-hosted)

[![](../../../media/en/logos/amnezia-logo.png)](https://amnezia.org/en/self-hosted)
(Android, iOS, Linux, macOS, Windows. Server: Linux, suitable for Ubuntu 22.04 or Debian 11)

An open-source application that makes it easier for individuals and organizations to administer their own secure VPN service.

[Set up a server](https://amnezia.org/en/starter-guide) | [Download a client](https://amnezia.org/en/downloads) | [Learn how it works](https://docs.amnezia.org/documentation/how-amnezia-works)



## Try a dedicated censorship circumvention tool

### [Lantern](https://lantern.io/)

[![](../../../media/en/logos/lantern-logo.png)](https://lantern.io/)
(Android, iOS, Linux, macOS, Windows)

A free application that provides access to blocked websites while helping to hide your online activity. Offers improved access to blocked sites for a fee.

[Download](https://lantern.io/download) | See [their FAQ](https://lantern.io/faq)

### [Psiphon](https://psiphon.ca/)

[![](../../../media/en/logos/Psiphon-logo.png)](https://psiphon.ca/)
(Android, iOS, macOS, Windows)

A free application that provides access to blocked websites while helping to hide your online activity. Provides good user support and offers improved performance for a fee.

[Download](https://psiphon.ca/en/download.html) | See [their guide](https://psiphon.ca/en/psiphon-guide.html)


# Anonymize your connections

## [Tor Browser](https://www.torproject.org)

[![](../../../media/en/logos/tor-logo.png)](https://www.torproject.org/)
(Android, Linux, macOS, Windows)

A free and open-source browser based on Mozilla Firefox that uses a network of volunteer relays to hide your online activity while providing access to some blocked websites. The Tor network might be blocked in some regions, and using it could make your connection look suspicious to anyone who might be monitoring your online activity.

[Download](https://www.torproject.org/download/) | See [their user manual](https://tb-manual.torproject.org/) and [their FAQ](https://support.torproject.org/)

## [Onion Browser](https://onionbrowser.com/)

[![](../../../media/en/logos/onionbrowser-logo.jpg)](https://onionbrowser.com/)
(iOS)

A free and open-source browser for iOS developed by the [Guardian Project](https://guardianproject.info/) that uses a network of volunteer relays to hide your online activity while providing access to some blocked websites. The Tor network might be blocked in some regions, and using it could make your connection look suspicious to anyone who might be monitoring your online activity.

[Download](https://apps.apple.com/us/app/onion-browser/id519296448) | See [their user manual](https://guardianproject.info/apps/org.torproject.torbrowser/) and [their FAQ](https://onionbrowser.com/faqs)

## [Orbot](https://orbot.app/en/)

[![](../../../media/en/logos/orbot-logo.png)](https://orbot.app/en/)
(Android, iOS)

A free and open-source application for mobile devices developed by the [Guardian Project](https://guardianproject.info/) that routes internet traffic through the Tor network and can anonymize connections for all traffico or only for certain apps.

[Download](https://orbot.app/en/download/) | See [their FAQ](https://orbot.app/en/faqs/) and [their user manual](https://github.com/guardianproject/orbot?tab=readme-ov-file#orbot)

## [Tails](https://tails.net)

[![](../../../media/en/logos/tails-logo.png)](https://tails.net)
(bootable on most computers; installation from Linux, macOS or Windows)

Use a separate, secure operating system that you load from a USB drive. Tails communicates over the Tor network to hide what you are looking at online and offers access to some blocked services. Tails does not leave traces of your work in the device where you run it.

[Download](https://tails.net/install/index.en.html) | See [their documentation](https://tails.net/doc/index.en.html)

## [Whonix](https://whonix.org)

[![](../../../media/en/logos/Whonix-logo.jpg)](https://whonix.org)
(Windows, macOS, Linux, Qubes OS)

 An anonymous operating system that runs like an app and routes all internet traffic generated inside its workstation through the Tor network.

[Download](https://www.whonix.org/wiki/Download) | See [their documentation](https://www.whonix.org/wiki/Documentation)

# Access and share content on decentralized networks

## [Ceno Browser](https://censorship.no/)

[![](../../../media/en/logos/ceno-logo.png)](https://censorship.no/)
(Android)

A free and open-source tool to access I2P, a decentralized and resilient network of peers where you can share and access files even when you cannot access the web.

[Download](https://geti2p.net/en/) | See [their FAQ](https://geti2p.net/en/faq) and learn [how to browse I2P](https://geti2p.net/en/about/browser-config)

## [I2P](https://geti2p.net/en/)

[![](../../../media/en/logos/i2plogo.png)](https://geti2p.net/en/)
(Android)

A free and open-source mobile web browser for Android devices for storing and sharing web content on decentralized networks even when connectivity across national borders is interrupted or extremely limited.

[Download](https://censorship.no/en/download.html) | See [their FAQ](https://censorship.no/en/support.html) and [their user manual](https://censorship.no/user-manual/)
