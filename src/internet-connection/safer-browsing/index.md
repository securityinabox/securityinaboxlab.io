---
weight: 030
title: Browse the web more securely
post_date: 12 September 2024
topic: internet-connection
---

A web browser is one of the digital tools most of us use daily. It is the main way many people access the internet. Many people are familiar with Edge, Safari and Chrome – browsers that come installed with our devices.

Because we rely so much on browsers, they are often targets for people who want to compromise your privacy or security. Follow the steps below to choose a more secure browser and make its protection stronger.

# Choose a web browser... or two!

- Install [Firefox for your platform and in your language](https://www.mozilla.org/en-US/firefox/all/) (available for Windows, macOS, Linux, Android and iOS).
    - On Android and iOS, consider installing [Firefox Focus](https://www.mozilla.org/en-US/firefox/browsers/mobile/focus/), a light-weight browser which comes with privacy-oriented settings by default.
- Install Chrome [for Windows, macOS, Linux](https://support.google.com/chrome/answer/95346?hl=en&co=GENIE.Platform%3DDesktop&oco=0), [Android](https://support.google.com/chrome/answer/95346?hl=en&co=GENIE.Platform%3DAndroid&oco=0) or [iOS](https://support.google.com/chrome/answer/95346?hl=en&co=GENIE.Platform%3DiOS&oco=0).
- Consider installing the free and open-source [Chromium](https://www.chromium.org/) instead of Chrome. On Linux, you can install Chromium through your software installer in many distributions. On Android, you can install it through [FFUpdater](https://github.com/Tobi823/ffupdater), a free and open-source installer and updater app that you can get from [F-Droid](https://f-droid.org/en/packages/de.marmaro.krt.ffupdater/).
- Optionally consider installing the [DuckDuckGo Privacy Browser](https://duckduckgo.com/duckduckgo-help-pages/get-duckduckgo/browser/) for Windows, macOS, iOS and Android.
- For advanced privacy and to access websites that are not accessible in the institution or country you connect from, you can consider using the [Tor Browser](https://www.torproject.org/download/) (for Windows, macOS, Linux or Android), but consider this may change your browsing experience dramatically. Read more on this in [our guide on anonymization tools](../anonymity).
- On Android, you can install a selection of secure and privacy-oriented web browsers through [FFUpdater](https://github.com/Tobi823/ffupdater), a free and open-source installer and updater app that you can get from [F-Droid](https://f-droid.org/en/packages/de.marmaro.krt.ffupdater/).

<details><summary>Learn why we recommend this</summary>

Most operating systems come with pre-installed browsers. For example the default macOS and iOS browser is Safari, and the default Windows browser is Edge. These web browsers are not designed with a focus on privacy and security, and we strongly suggest to use more security and privacy-oriented software as your default browsers.

- We recommend the [Firefox](https://www.mozilla.org/en-US/firefox/new/) web browser, made by Mozilla. Firefox is free and open-source software and has better built-in security than others browsers.
    - Firefox Focus is a light-weight, privacy-oriented web browser for mobile devices. Read [Mozilla's blog post on the differences between Firefox Focus and other browsers](https://blog.mozilla.org/en/products/firefox/firefox-focus-or-firefox/).
- [Chrome](https://www.google.com/chrome/) and [Chromium](https://www.chromium.org/Home/), both developed by Google, also have high-quality security. While Chromium is free and open-source, Chrome is proprietary freeware based on open-source components. Since Chrome is developed by Google, consider whether it might send more data about you to Google than you are comfortable with and consider not logging in to your Google account when using it.
    - Read [Google's documentation on privacy in Chrome](https://support.google.com/chrome/answer/14225066?hl=en)
- The [DuckDuckGo Privacy Browser](https://duckduckgo.com/duckduckgo-help-pages/get-duckduckgo/does-duckduckgo-make-a-browser/) is a privacy-oriented browser that automatically blocks web trackers and upgrades insecure HTTP connections to HTTPS when possible.
- The [Tor Browser](https://www.torproject.org/) is a modified version of Mozilla Firefox that allows you to anonymize your connections by browsing the web through the Tor network. Read more on this in [our guide on anonymity tools](../anonymity).

It can be a good idea to install more than one browser, and use one for more sensitive activities and the other for your everyday needs. Read more on this in [our guide on multiple identity management](../../communication/multiple-identities/#different-devices).
</details>

# Change your device's default browser

- Avoid using less secure and less privacy-friendly browsers like Edge or Safari as your primary web browser. Deactivate or uninstall browsers you don't use.
- Learn [how to change your default browser to Firefox](https://support.mozilla.org/en-US/kb/make-firefox-your-default-browser) on Linux, macOS and Windows. Use the "Customize this article" drop-down menu in the right-hand side of the window to find the right instructions for your device. For Android and iOS read the following instructions.
    - [Make Firefox the default browser on Android](https://support.mozilla.org/en-US/kb/make-firefox-default-browser-android).
    - [Set Firefox as the default browser for iOS](https://support.mozilla.org/en-US/kb/set-firefox-default-browser-ios).
- You can also decide to make Chrome your default browser on [Windows, macOS](https://support.google.com/chrome/answer/95417?hl=en&co=GENIE.Platform%3DDesktop) and [iOS](https://support.google.com/chrome/answer/95417?hl=en&co=GENIE.Platform%3DiOS&oco=0). But we strongly recommend not to log in with your Google account.
    - To avoid signing into your Google account on Chrome, go to the browser Settings, select Google services and make sure Allow Chrome sign-in is unchecked.
- [Change which web browser opens websites by default on Ubuntu](https://help.ubuntu.com/stable/ubuntu-help/net-default-browser.html).
- [Stop the Edge browser from automatically starting when you start Windows](https://support.microsoft.com/microsoft-edge/stop-microsoft-edge-from-starting-automatically-c341c879-799a-dccd-d6be-bc51ecdd5804).

<details><summary>Learn why we recommend this</summary>

Not all web browsers are created equal. Some protect your privacy and security more effectively than others.

By setting a browser focused on privacy and security as your default browser, the links you click in other apps will open directly in a more secure browser. In any case, always think twice before you open a link you receive through messages or emails.

Read more on why you should [pause before you click when you receive a link in our guide on malware](../../phones-and-computers/malware/#pause-before-you-click-and-be-cautious-when-you-receive-a-link).
</details>

# Make sure your browser is up to date

- Firefox should update automatically, but it is important to periodically [check whether you have the latest version of the browser](https://support.mozilla.org/kb/update-firefox-latest-release).
- Chrome/Chromium should also update automatically, but it is important to periodically check for updates. Learn how to do this in the [official guide on how to update Chrome](https://www.google.com/chrome/update/).
- [Learn how to set up the Tor Browser to update automatically and how to update it manually](https://tb-manual.torproject.org/updating/).

# Enter web addresses in the address bar

If you know the complete web address of a website (for example `securityinabox.org`), type it in the address bar to avoid going through a search engine to access it.

If you just type the name of the website (Security in a Box) without its extension (`.org`), the browser will open your default search engine with several results, some of which may be malicious.

If you use the address bar to search the web, consider [replacing Google with a more privacy-friendly option as your default search engine](#set_your_default_search_engine).

- **Firefox**
    - Learn more on the Firefox address bar in the [Mozilla guide on how to search with the address bar](https://support.mozilla.org/en-US/kb/search-firefox-address-bar).
    - Consider whether you want to change what Firefox suggests when you type in the address bar.
        - [Change your address bar settings so it does not suggest pages from your browsing history or other unwanted results](https://support.mozilla.org/kb/address-bar-autocomplete-firefox#w_how-can-i-control-what-results-the-address-bar-shows-me).
        - [Remove autocomplete results](https://support.mozilla.org/kb/address-bar-autocomplete-firefox#w_removing-autocomplete-results).
        - [Disable search suggestions](https://support.mozilla.org/en-US/kb/search-suggestions-firefox#w_enabling-or-disabling-search-suggestions).

- **Chrome**/**Chromium**
    - [Manage Google autocomplete predictions and turn off personal results](https://support.google.com/websearch/answer/106230?sjid=17730964962312313254-EU).

<details><summary>Learn why we recommend this</summary>

The address bar is the familiar text field at the top of a web browser’s interface. When you browse the web, the address bar displays the address of the web page you are visiting (in our current case, this is `https://securityinabox.org/en/internet-connection/safer-browsing`). By typing a web address into the address bar, you'll request a website or a specific page of a website.

Address bars also offer additional functionalities, like performing web searches or providing suggestions for websites.
</details>

# Set your default search engine

- Choose a default search engine that does not track you:
    - [**DuckDuckGo**](https://duckduckgo.com/about) (our top recommendation)
    - [**StartPage**](https://www.startpage.com/)

- **Firefox**
    - [Set your default search engine on your computer](https://support.mozilla.org/kb/change-your-default-search-settings-firefox#w_default-search-engine).
    - [Add or remove search engines on your computer](https://support.mozilla.org/kb/change-your-default-search-settings-firefox#w_remove-or-add-search-engines).
    - [Manage your default search engines in Firefox for Android](https://support.mozilla.org/en-US/kb/manage-my-default-search-engines-firefox-android).
    - [Change your default search engine in Firefox for iOS](https://support.mozilla.org/en-US/kb/change-your-default-search-engine-firefox-ios).

- **Chrome**/**Chromium**
    - [Set your default search engine](https://support.google.com/chrome/answer/95426).

<details><summary>Learn why we recommend this</summary>

Search engines like Google and Bing build profiles of people who use them, track your device specifically and share their users' personal information with third parties. Your browser uses one search engine by default when you use the address bar to perform a web search or when you right-click a selected string of text and click Search.
</details>

# Review the camera, microphone and other site permissions

- **Firefox**
    - [Learn how to manage your camera and microphone permissions in Firefox](https://support.mozilla.org/kb/how-manage-your-camera-and-microphone-permissions).
    - [Grant camera access to Firefox for Android](https://support.mozilla.org/en-US/kb/camera-permissions).
    - [Learn about functions and permissions on Firefox Focus for Android and iOS](https://support.mozilla.org/en-US/kb/how-does-firefox-focus-ios-use-permissions-it-requests#w_firefox-focus-functions-and-permissions).
- **Chrome**/**Chromium**
    - [Use your camera and microphone in Chrome](https://support.google.com/chrome/answer/2693767?hl=en).

<details><summary>Learn why we recommend this</summary>

Permissions can be like a door or window you leave open in your house: if one website can get in, others may be able to as well. Make sure only websites you use and trust have permission to use sensitive features like your camera or microphone. Malware might use those permissions to let someone see or hear where you are.
</details>

# Secure your connections

- Activate HTTPS-Only mode.
    - [Set your preferences to HTTPS-Only in Firefox](https://support.mozilla.org/en-US/kb/https-only-prefs#w_enabledisable-https-only-mode).
    - [Turn on Always use secure connections in Chrome/Chromium](https://support.google.com/chrome/answer/10468685?hl=en&co=GENIE.Platform%3DDesktop&oco=0#zippy=%2Cturn-on-always-use-secure-connections).
    - Read the [Security planner guide on how to set up HTTPS-Only mode in other browsers](https://securityplanner.consumerreports.org/tool/install-https-everywhere).
- When you visit websites, look at the address at the top of your browser. Make sure the address begins with `https://`, not just `http://`.

<details><summary>Learn why we recommend this</summary>

The S in HTTPS stands for "secure." This is the protocol you should use to access web pages in your browser. HTTPS encrypts and protects what you are looking at as it travels between your device and the website, making it harder for people trying to spy on you to see, for example, sensitive data you enter or what pages you visit in that site.
</details>

# Enable Tracking Protection settings

- Enable strict privacy protection in your browser to avoid being tracked by third parties when browsing the web.
    - **Firefox**:
        - [Set Enhanced Tracking Protection at least to Standard](https://support.mozilla.org/kb/enhanced-tracking-protection-firefox-desktop#w_adjust-your-global-enhanced-tracking-protection-settings); consider whether you want to set it to Strict (more sites will look like they are broken).
    - **Chrome**/**Chromium**:
        - [Enable Tracking Protection](https://support.google.com/chrome/answer/95647?sjid=10772239645414681785-EU#tracking_protection).
        - [Set your Safe Browsing protection level to Enhanced protection](https://support.google.com/chrome/answer/9890866?hl=en&co=GENIE.Platform%3DDesktop&sjid=10772239645414681785-EU&oco=0#zippy=%2Cenhanced-protection).
- Set your browser to delete data when you end your browsing session.
    - [Delete cookies and site data when Firefox is closed](https://support.mozilla.org/en-US/kb/clear-cookies-and-site-data-firefox#w_clear-cookies-for-any-website).
    - [Learn how to automatically clear browsing data when closing Chrome/Chromium](https://support.google.com/chrome/community-guide/245444314/how-to-automatically-clear-browsing-data-when-closing-google-chrome-window-a-step-by-step-guide?hl=en).
- Sometimes, a website may not work correctly with enhanced tracking protection settings. In such case, you can disable tracking protection temporarily for that particular website.
    - [Temporarily disable tracking protection in Firefox](https://support.mozilla.org/ta/kb/introducing-total-cookie-protection-standard-mode#w_what-do-i-do-if-a-site-seems-broken).
    - [Allow third-party cookies temporarily for a specific site in Chrome/Chromium](https://support.google.com/chrome/answer/95647?hl=en&co=GENIE.Platform%3DDesktop#zippy=%2Callow-third-party-cookies-temporarily-for-a-specific-site).

<details><summary>Learn why we recommend this</summary>

When you browse the web, cookies and trackers gather details of who you are, where you are, and what you have looked at online. Consider what might happen if these fell into the hands of your adversary, and take these steps to limit tracking.
</details>


# Turn off the browser's built-in password manager

- Remove all saved logins.
    - [**Firefox**](https://support.mozilla.org/en-US/kb/password-manager-remember-delete-edit-logins#w_remove-all-saved-logins)
    - [**Chrome**/**Chromium**](https://support.mozilla.org/en-US/kb/password-manager-remember-delete-edit-logins#w_remove-all-saved-logins)
- Disable the password manager.
    - [**Firefox**](https://support.mozilla.org/en-US/kb/password-manager-remember-delete-edit-logins#w_disable-the-firefox-password-management-feature)
    - [**Chrome**/**Chromium**](https://support.google.com/chrome/answer/95606?hl=en&co=GENIE.Platform%3DDesktop#zippy=%2Cstart-or-stop-saving-passwords)

<details><summary>Learn why we recommend this</summary>

Most browsers can generate, save and encrypt passwords for you. However, we recommend you turn this feature off and [use a separate password manager like KeePassXC](../../passwords/password-managers/#use-a-password-manager-installed-in-your-device) instead. Browser-based password managers put you at greater risk of an attacker tricking your browser into giving up your passwords.

If you decide to use a browser-based password manager, avoid using it to store passwords for highly sensitive accounts and [read our recommendations on how to protect the passwords you store in your browser](../../passwords/password-managers/#if-you-decide-to-store-some-passwords-in-your-browser-or-email-client).
</details>

# Use protective browser add-ons/extensions

- You can choose which add-ons/extensions to install and decide how to configure them, depending on your circumstances.
- If you are using a device that is managed by someone else (for example in a public library or at your workplace), you might have to make these adjustments repeatedly.
- Install and configure:
    - [Privacy Badger](https://privacybadger.org/) (for Firefox, Chrome/Chromium and Firefox on Android)
        - **Why?** _Prevents tracking and collection of metadata through your browser._
    - [uBlock Origin](https://ublockorigin.com/) (for Firefox and Firefox on Android) and [uBlock Origin Lite](https://chromewebstore.google.com/detail/ublock-origin-lite/ddkjiahejlhfcafbddmgiahcphecmpfh) for Chrome/Chromium
        - **Why?** _Blocks advertising and trackers, some of which might be malicious._
    - [Facebook Container](https://addons.mozilla.org/firefox/addon/facebook-container/), if you use Facebook or Instagram (Firefox only, only for computers)
        - **Why?** _Keeps Facebook from gathering data on where you have been online and associating it with your profile._
    - [Zoom Redirector for Firefox](https://addons.mozilla.org/firefox/addon/zoom-redirector/) and [Xoom Redirector for Chrome/Chromium](https://chromewebstore.google.com/detail/xoom-redirector/ocabkmapohekeifbkoelpmppmfbcibna) (only for computers)
        - **Why?** _By making Zoom links open in your browser, this add-on keeps the call within your browser's protections._
    - [NoScript](https://noscript.net/) (for Firefox, Chrome/Chromium and other Chromium-based browsers like Edge, Brave or Vivaldi) (optional, but recommended; available for computers and for Firefox on Android)
        - Note that NoScript will often make websites look empty or broken. [Learn how to configure NoScript so this happens less often](https://noscript.net/features).
        - **Why?** _It may be possible for an adversary to get to your device using malicious code in a script downloaded along with a webpage you are viewing. NoScript blocks all code from unknown websites, protecting your device from infection._


<details><summary>Learn why we recommend this</summary>

When you browse the web, you come into contact with a great deal of code from unknown sources. This is one reason why the overwhelming majority of malware and spyware infections originate from web pages.

We recommend installing these browser extensions or add-ons to protect against these security and privacy issues.
</details>

# Remove unwanted add-ons/extensions and manage pop-ups

- Remove all add-ons/extensions that you do not use or recognise.
    - [**Firefox**](https://support.mozilla.org/kb/disable-or-remove-add-ons#w_removing-extensions)
    - [**Chrome**/**Chromium**](https://support.google.com/chromebook/answer/2589434?hl=en)
- Make sure your browser is set to block pop-ups.
    - [Read the Firefox guide on how to block pop-ups](https://support.mozilla.org/kb/pop-blocker-settings-exceptions-troubleshooting#w_pop-up-blocker-settings).
    - [Learn how to block pop-ups and redirects in Chrome/Chromium](https://support.google.com/chrome/answer/95472?hl=en&co=GENIE.Platform%3DDesktop#zippy=%2Cblock-pop-ups-and-redirects-from-a-site).

<details><summary>Learn why we recommend this</summary>

Malicious people may try to trick you into installing malware through your browser. They may do this using a pop-up window. Make sure your browser is set to protect you from these tricks. Additionally, remove add-ons/extensions you are not using to be sure you only use pieces of software that you really need.

Browser add-ons/extensions can be malicious and could be used to spy on you just as any other program or app. It is therefore important to only install add-ons/extensions that you can trust, possibly only from trusted sources, like the [Mozilla add-ons repository for Firefox](https://addons.mozilla.org/en-US/firefox/) or the [Chrome web store for Chrome/Chromium extensions](https://chromewebstore.google.com/category/extensions). If you really need to install an add-on/extension from a different source than the official repositories, read [Mozilla's tips for assessing the safety of an extension](https://support.mozilla.org/en-US/kb/tips-assessing-safety-extension) before you proceed.
</details>

# Delete your browsing history

- **Firefox**
    - Set up a button that makes it easy to [quickly delete your recent cookies and the history of pages you visited](https://support.mozilla.org/kb/forget-button-quickly-delete-your-browsing-history).
    - Tell Firefox to [clear your history automatically](https://support.mozilla.org/kb/delete-browsing-search-download-history-firefox#w_how-do-i-make-firefox-clear-my-history-automatically) or [remove single websites from your history](https://support.mozilla.org/en-US/kb/delete-browsing-search-download-history-firefox#w_remove-a-single-website-from-your-history).
    - You can also [delete your browser history manually](https://support.mozilla.org/kb/delete-browsing-search-download-history-firefox#w_how-do-i-clear-my-history).

- **Chrome**/**Chromium**
    - [Delete the history of pages you visited](https://support.google.com/chrome/answer/95589?hl=en&co=GENIE.Platform=Desktop).
    - If you are logged in with your Google account when you use Chrome or Chromium, [learn how to delete all your Google activity](https://support.google.com/accounts/answer/465).

<details><summary>Learn why we recommend this</summary>

Your browsing history is a list of websites you have visited. The default option in Firefox and Chrome/Chromium is to remember your browsing, download, form and search histories.

Browser history can be helpful to you: your browser will suggest pages you have visited before, so you don't have to re-type addresses or get sent by mistake to sites that are malicious. But there are trade-offs. If someone had access to the history of what you viewed on the internet, there is a lot they could learn about you, the people you work with and the things you have been reading about.
</details>

# Use private browsing or Incognito mode

If you would like to avoid saving your history just for one browsing session, you can use the private browsing, or Incognito mode feature.

- [Understand more about what private browsing (or Incognito mode) will NOT protect you from](https://support.mozilla.org/kb/common-myths-about-private-browsing), including your IP address not changing, leaving traces in the device you are using or being spied upon through malware in your computer.

**Firefox**

- [Turn on private browsing for one session](https://support.mozilla.org/en-US/kb/private-browsing-use-firefox-without-history#w_how-do-i-open-a-new-private-window).
- Consider [turning private browsing on at all times](https://support.mozilla.org/en-US/kb/private-browsing-use-firefox-without-history#w_can-i-set-firefox-to-always-use-private-browsing).

**Chrome**/**Chromium**

- [Browse in Incognito mode](https://support.google.com/chrome/answer/95464).

<details><summary>Learn why we recommend this</summary>

If you open a browser window in "Private browsing" or "Incognito mode", the browser does not track cookies or save your browser history. Using it is a quick way to hide some of your activity if you otherwise tell your browser it is ok to keep a record of the pages you have searched. It can be especially useful if you need to better protect your privacy from somebody with access to your device.

Note that private browsing/Incognito mode will not protect you from malware or  surveillance. Read our guides on [malware](../../phones-and-computers/malware/) and [anonymity](../anonymity) to learn how to address such risks.
</details>

# Consider not showing what you last viewed on startup or your most visited sites

- **Firefox**
    - [Make sure that Firefox is not set to restore the previous session at startup](https://support.mozilla.org/en-US/kb/restore-previous-session#w_when-youve-set-firefox-to-restore-the-previous-session-at-startup).
    - [Configure Firefox to open new tabs as blank pages](https://support.mozilla.org/en-US/kb/customize-your-new-tab-page#w_blank-new-tab-page).
- **Chrome**/**Chromium**
    - [Make sure your startup page is set to open with a new tab, not where you left off](https://support.google.com/chrome/answer/95314?sjid=17730964962312313254-EU#zippy=%2Chave-a-new-tab-open).
    - [Customize your New Tab Page](https://support.google.com/chrome/answer/11032183?sjid=17730964962312313254-EU#zippy=) unchecking My shortcuts and Most visited sites.

<details><summary>Learn why we recommend this</summary>

If you are worried that somebody else will have access to your browser, turn off the feature that shows the webpages you had open when you last closed your browser and stop your browser from suggesting to open your most visited sites.
</details>

# [Advanced] How to recognize and report a malicious website

Most modern browsers recognize malicious and unsafe websites by default and will warn you if you try to access phishing, malware or social engineering sites. If you see such a warning, it's best not to visit that website. If, on the other hand, you open such a website without being warned, consider reporting it to improve spam detection systems that protect Google's search results.

- Learn more about the [built-in Phishing and Malware Protection feature in Firefox](https://support.mozilla.org/en-US/kb/how-does-phishing-and-malware-protection-work).
- Learn more on [warnings about unsafe sites in Chrome/Chromium](https://support.google.com/chrome/answer/99020?hl=en&co=GENIE.Platform%3DDesktop).
- [Report spam, phishing, or malware to the Google Safe Browsing program](https://developers.google.com/search/help/report-quality-issues).
