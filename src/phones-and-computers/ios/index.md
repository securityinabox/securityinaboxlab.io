---
title: Protect your iOS device
weight: 020
post_date: 30 May 2024
topic: phones-and-computers
---

If you use an iPhone or iPad, you may have heard the myth that they are more secure. This is not necessarily true. Security depends on a combination of how we use our devices and their own software, which can be found to have vulnerabilities at any time. Follow the steps in this guide to make your device more secure. Get in the habit of checking these settings from time to time, to make sure nothing has changed.

# Use the latest version of your device's operating system (OS)

- When updating software, do it from a trusted location and internet connection, like your home or office, not at an internet cafe or coffee shop.
- Updating to the latest version of your operating system may require you to download software and restart a number of times. You will want to set aside time for this where you do not need to do work on your device.
- After updating, check again if there are any further updates available until you do not see any additional new updates.
- If the iOS version that runs in your device is unmaintained, it is best to consider buying a new device.
    - [Find out which is the most updated version available](https://support.apple.com/en-us/HT201222).
    - [Compare it to the version your device has installed](https://support.apple.com/en-us/109065).
    - [Update your operating system](https://support.apple.com/guide/iphone/update-ios-iph3e504502/ios).
    - [Set your OS to update on its own](https://support.apple.com/guide/iphone/update-ios-iph3e504502/ios#aria-iphfa2484766).
- To make sure an update is fully installed, always restart your device when prompted to do so after downloading the update.

**Notes**

- The iOS update guide recommends backing up your device through several methods, including iCloud (which means Apple's own servers), before you update your operating system. Consider which is the safest method for you, given the threats you face. In general, we recommend [backing up your phone or tablet to your own computer](https://support.apple.com/en-us/108796).

<details><summary>Learn why we recommend this</summary>

New vulnerabilities in the code that runs in your devices and apps are found every day. The developers who write that code cannot predict where they will be found, because the code is so complex. Malicious attackers may exploit these vulnerabilities to get into your devices. But software developers do regularly release code that fixes those vulnerabilities. That is why it is very important to install updates and use the latest version of the operating system for each device you use. We recommend setting your device to automatically update so you have one less task to remember to do.
</details>

# Turn Lockdown Mode on

If you use iPhone, iPad or another iOS device, we strongly suggest to [turn Lockdown Mode on](https://support.apple.com/en-gb/guide/iphone/iph049680987/ios). Most likely you will not see any or much difference in the way your device works, but you will be much better protected.

<details><summary>Learn why we recommend this</summary>

[Lockdown Mode](https://support.apple.com/en-us/105120) reduces the attack surface for sophisticated spyware like [Pegasus](../../blog/pegasus-project-questions-and-answers/). Features of the Lockdown Mode should be used by everyone and should really be part of a standard operating system.
</details>

# Use apps from trusted sources

- We recommend [not jailbreaking your device](https://support.apple.com/en-gb/guide/iphone/iph9385bb26a/ios), as it puts you at greater risk from malicious code.
- [Install apps from the App Store](https://support.apple.com/en-us/102590) instead.

<details><summary>Learn why we recommend this</summary>

[Apple's App Store](https://www.apple.com/app-store/) is the official app store for iOS devices. This centralized way of distributing apps makes it easier for people to find what they need, and it also makes it easier for Apple to monitor the offered apps for major security violations, as well as generally enforce their policies on listed apps. Only install apps from the App Store.

Some governments have demanded tech companies ban certain apps in their countries. When that happens, your contacts may encourage you to "jailbreak" your device in order to install banned apps. In fact, "jailbreaking" your device is not necessary if you use [techniques to circumvent your country's censorship](../../internet-connection/anonymity-and-circumvention/) when setting up your phone.
</details>

# Remove apps that you do not need and do not use

- Follow [the instructions on how to delete apps from iPhone](https://support.apple.com/en-gb/guide/iphone/iph248b543ca/ios).

<details><summary>Learn why we recommend this</summary>

New vulnerabilities in the code that runs in your devices and apps are found every day. The developers who write that code cannot predict where they will be found, because the code is so complex. Malicious attackers may exploit these vulnerabilities to get into your devices. Removing apps you do not use helps limit the number of apps that might be vulnerable. Apps you do not use may also transmit information about you that you may not want to share with others, like your location.
</details>

# Use privacy-friendly apps

- You can browse the web with [Firefox](../../tools/firefox/).
- You can use [free and open source software for almost everything you need to do on an iOS device](https://github.com/dkhamsing/open-source-ios-apps). To decide whether you should use a certain app, see [how Security in a Box chooses the tools and services we recommend](../../about/how/#how-we-choose-the-tools-and-services-we-recommend).

<details><summary>Learn why we recommend this</summary>

iPhones and iPads come with built-in software, like Safari or Mail, that have a history of [privacy](https://www.theregister.com/2024/04/30/apple_safari_europe_tracking/) and [security issues](https://www.bitdefender.com/blog/hotforsecurity/stop-using-your-ios-mail-app-now-heres-what-you-need-to-know-about-the-scary-flaw-just-discovered-and-how-to-stay-safe/). Instead, you can use more privacy-friendly apps to browse the web, read your email and much more.
</details>

# If possible, avoid using social media apps

- Access social media and other sites by logging in through your browser instead.

<details><summary>Learn why we recommend this</summary>

Apps may share a lot of your data, like the ID of your phone, your phone number and which wifi you connect to. Some social media apps collect more information than they should. This includes apps like Facebook or Instagram. Use those services through the secure browser on your device (like [Firefox](../../tools/firefox/)) to protect your privacy a bit more.
</details>

# Check your app permissions

Review all permissions one by one to make sure they are enabled only for apps you use. The following permissions should be turned off in apps you do not use, and considered suspicious when used by apps you do not recognize:

- Location
- Contacts
- SMS
- Microphone
- Camera
- Call logs
- Phone
- Modify or change system settings
- Allowed to download other apps
- Calendars
- Reminders
- Photos
- Bluetooth
- Local Network
- Nearby Interactions
- Speech Recognition
- Health
- Media
- Files and Folders
- Motion & Fitness

- Learn more on this [in the Security Planner article on how to review your iPhone’s app permissions](https://securityplanner.consumerreports.org/tool/review-iphone-app-permissions).
- [More info on privacy and location services](https://support.apple.com/en-us/102515)
- [More info on other app permissions](https://support.apple.com/guide/iphone/control-access-to-information-in-apps-iph251e92810/ios)

<details><summary>Learn why we recommend this</summary>


Apps that access sensitive digital details or services — like your location, microphone, camera or device settings — can also leak that information or be exploited by attackers. So if you do not need an app to use a particular service, turn that permission off.
</details>

# Turn off location and wipe history

- Get in the habit of turning off location services overall, or when you are not using them, for your whole device as well as for individual apps.
    - [Learn how to turn off location on your device](https://support.apple.com/en-us/102647).
- Regularly check and clear your location history if you keep it turned on.
    - Learn how to [clear your location history in the Maps app on your iPhone](https://support.apple.com/guide/iphone/clear-location-history-iph32b15b22f/ios) or [iPad](https://support.apple.com/guide/ipad/clear-location-history-ipadc8e477d7/ipados).
    - Regularly check and clear your location history in Google Maps if you keep it turned on. To delete past location history and set it so Google Maps does not save your location activity, follow the instructions [for your Google Maps Timeline](https://support.google.com/accounts/answer/3118687?#delete) and [your Maps activity](https://support.google.com/maps/answer/3137804).

<details><summary>Learn why we recommend this</summary>

Many of our devices keep track of where we are using GPS, cell phone towers and the wifi networks we connect to. If your device is keeping a record of your physical location, it makes it possible for someone to find you or use that record to prove that you have been in certain places or associated with specific people who were somewhere at the same time as you.
</details>

# Secure the accounts connected with your device

- Review [Apple's checklist to protect your Apple ID and password](https://support.apple.com/en-us/102614). In particular, make sure to [set a strong password](../../passwords/passwords/), to [enable 2-factor authentication](https://support.apple.com/en-us/102660) and not to share your Apple ID account with anyone.
- [Learn how to check your device list](https://support.apple.com/en-us/102649) to see which devices are connected to your Apple ID account.
- Also see our guide on [social media accounts](../../communication/social-media) to check other accounts connected with your device.
- You may want to take a picture or screenshot of results showing suspicious activity, like devices you have disposed of, don't have control of or don't recognize.
- Follow the [steps suggested by Apple if you think your Apple ID has been compromised](https://support.apple.com/en-us/102560).
- Consider turning on [Advanced Data Protection for iCloud](https://support.apple.com/en-us/108756) to enable end-to-end encryption and make sure only you can access your data through your trusted devices.
    - Note that if you turn on Advanced Data Protection, access to your iCloud data on the web at iCloud.com will be disabled and you will only be able to access your data on your trusted devices. If you turn on web access again, you can use one of your trusted devices to approve temporary access to your data on the web.

<details><summary>Learn why we recommend this</summary>

Apple devices have Apple ID accounts associated with them. More than one device may be logged in at a time (like your phone, laptop and maybe your TV). If someone else has access to your accounts without your authorization, the steps included in this section will help you see and stop this.
</details>


# Remove permissions to access your device, accounts or information

- If you’re running iOS 15 or earlier, use [this checklist to see if anyone else has access to your device or accounts](https://support.apple.com/guide/personal-safety/see-who-has-access-to-your-iphone-or-ipad-ipsb8deced49/web).
- If you’re running iOS 16 or later, you can use Safety Check to:
    - [review and update information you share with people, apps, and devices](https://support.apple.com/en-gb/guide/iphone/iph42f0b2d53/16.0/ios/16.0).
    - [stop sharing information from Apple apps to other people](https://support.apple.com/guide/personal-safety/how-safety-check-works-ips2aad835e1/1.0/web/1.0#ips6b5010ee3).
- In some cases, you may also be sharing information that the the tools above can’t help you detect. Read [the additional considerations when using Safety Check](https://support.apple.com/guide/personal-safety/additional-considerations-safety-check-ipsce4cb8352/web) to decide what steps you can take to reduce the amount of information you’re sharing.
- Learn how you can [manage the information you share with people and apps with iOS' built-in privacy and security protections](https://support.apple.com/guide/iphone/use-built-in-privacy-and-security-protections-iph6e7d349d1/ios#iph27454a3969).

<details><summary>Learn why we recommend this</summary>

If you don't intend for someone else to access your device, accounts or information, it is better to not leave any additional "door" open (this is called "reducing your attack surface"). Additionally,the checks suggested in this section could reveal that someone is accessing your device, account or data without your permission.
</details>

# Set your screen to sleep and lock

- Set your screen to lock a short time after you stop using it (5 minutes is good).
- Use a [longer passphrase](../../passwords/passwords) (minimum 10 characters), not a short password or PIN.
- Making it possible to use your fingerprint, face, eyes or voice to unlock your device can be used against you by force; do not use these options unless you have a disability which makes typing impossible.
    - Remove your fingerprints and face from your device if you have already entered them.
    - Learn how to remove a fingerprint in [Manage your Touch ID settings](https://support.apple.com/en-us/102528#manage).
    - Learn how to turn off Face ID in [the guide to set up Face ID on iPhone](https://support.apple.com/en-sg/guide/iphone/iph6d162927a/ios#aria-iph7be5b34b6).
- Simple "swipe to unlock" options are not secure locks; do not use this option.
- Learn how to set a "passcode" in [Apple's guide on setting passcodes on iPhone, iPad or iPod touch](https://support.apple.com/en-us/119586).

<details><summary>Learn why we recommend this</summary>

While technical attacks can be particularly worrying, your device may as well be confiscated or stolen, which may allow someone to break into it. For this reason, it is smart to set a passphrase screen lock, so that nobody can access your device just by turning it on and guessing a short PIN or password.

We do not recommend screen lock options other than passphrases. If you are arrested, detained or searched, you might easily be forced to unlock your device with your face, voice, eyes or fingerprint. Someone who has your device in their possession may use software to guess short passwords or PINs. And if you set a fingerprint lock, someone who has dusted for your fingerprints can make a fake version of your finger to unlock your device; similar hacks have been demonstrated for face unlock.

For these reasons, the safest lock you can set is a longer passphrase.
</details>

# Control what can be seen when your device is locked

- [In your notification settings, ensure "show previews" is set to "When Unlocked" or "Never"](https://support.apple.com/guide/iphone/change-notification-settings-iph7c3d96bab/ios).
- In Settings, go to Face ID & Passcode (or Touch ID & Passcode) and turn off Accessories under Allow Access When Locked. For more information, see [Allow USB and other accessories to connect to your iPhone, iPad, or iPod touch](https://support.apple.com/en-us/111806).
- Review what else is allowed when your phone is locked in Settings > Touch ID & Passcode (or Face ID & Passcode) > Allow Access When Locked, and turn off options you would not want other people to access when your device is locked. In particular, be sure to disable access to the Notification Center and the Control Center when your device is locked.
    - Learn how to control access to information on the iPhone Lock Screen in [the support page on turning on lock screen features on iPhone](https://support.apple.com/guide/iphone/control-access-information-lock-screen-iph9a2a69136/ios).

<details><summary>Learn why we recommend this</summary>

A strong screen lock will give you some protection if your device is stolen or seized — but if you don't turn off notifications that show up on your lock screen, whoever has your device can see information that might leak when your contacts send you messages or you get new email.
</details>

# Disable voice controls

- [Turn off classic voice control](https://support.apple.com/en-us/119836) and/or [Siri](https://support.apple.com/guide/iphone/change-siri-settings-iphc28624b81/ios).
- Learn how to turn off Siri in the [official guide on changing when Siri responds](https://support.apple.com/en-il/guide/iphone/iphc28624b81abc/ios). In particular, make sure to turn off the "Allow Siri When Locked" option.
- Turn off Dictation: go to Settings > General > Keyboard, then tap to turn off Enable Dictation. By turning off both Ask Siri and Dictation, Apple will delete Siri Data that is associated with your account.
- Read [Ask Siri, Dictation & Privacy](https://www.apple.com/legal/privacy/data/en/ask-siri-dictation) to learn more about how Apple treats the data it gathers when you use Siri.
- If you have decided the benefits to you outweigh the large risks of using voice control, follow the [Security Planner instructions to do so more safely](https://securityplanner.consumerreports.org/tool/secure-your-smart-speaker).

<details><summary>Learn why we recommend this</summary>

If you set up a device so you can speak to it to control it, it becomes possible for someone else to install code on your device that could capture what your device is listening to.

It is also important to consider the risk of voice impersonation: someone could record your voice and use it to control your phone without your permission.

If you have a disability that makes it difficult for you to type or use other manual controls, you may find voice control necessary. This section provides instructions on how to set them up more safely. However, if you do not use voice controls for this reason, it is much safer to turn them off.
</details>

# Use a physical privacy filter that prevents others from seeing your screen

- For more information on this topic, see [the Security Planner guide on privacy filters](https://securityplanner.consumerreports.org/tool/use-a-privacy-filter-screen).

<details><summary>Learn why we recommend this</summary>

While we often think of attacks on our digital security as highly technical, you might be surprised to learn that some human rights defenders have had their information stolen or their accounts compromised when someone looked over their shoulder at their screen or used a security camera to do so. A privacy filter makes this kind of attack, often called shoulder surfing, less likely to succeed. You should be able to find privacy filters in the same shops where you find other accessories for your devices.
</details>

# Use a camera cover

- First of all, figure out whether and where your device has cameras. Your smartphone might have more than one.
- You can create a low-tech camera cover: apply a small adhesive bandage on your camera and peel it off when you need to use the camera. A bandage works better than a sticker because the middle part has no adhesive, so your lens won't get sticky.
- Alternatively, search your preferred store for the model of your device and "webcam privacy cover thin slide" to find the most suitable sliding cover for your phone or tablet.

<details><summary>Learn why we recommend this</summary>

Malicious software may turn on the camera on your device in order to spy on you and the people around you, or to find out where you are, without you knowing it.
</details>

# Turn off connectivity you're not using

- Completely power off your devices at night.
- Get into the habit of keeping wifi, Bluetooth and/or network sharing off and only enable them when you need to use them.
- Airplane mode can be a quick way to turn off connectivity on your mobile. Learn how to selectively turn on wifi and Bluetooth once your device is in Airplane mode, to use only the services you want.
    - Turn Airplane mode on and make sure wifi and Bluetooth are off.
    - To learn how to selectively turn on wifi and Bluetooth while your device is in Airplane mode, see [Apple's guide on how to use Airplane mode](https://support.apple.com/en-us/108785).
- Disable Bluetooth and wifi whenever you aren't using them. To do this, go to Settings and DO NOT use the Control Center (the swipe up from the bottom of the screen). The Control Center only lets you disconnect from the currently connected Bluetooth devices or wifi networks and does not include the option to disable Bluetooth or wifi altogether.
- Turn off Personal Hotspot when you are not using it.
    - Make sure your device is not providing an internet connection to someone else using Personal Hotspot. Go to Settings > Cellular > Personal Hotspot or Settings > Personal Hotspot and make sure that the "Allow Others to Join" option is disabled.
    - Learn how to disconnect devices in the [official guide on how to set up a Personal Hotspot on iOS devices](https://support.apple.com/en-us/111785).

<details><summary>Learn why we recommend this</summary>

All wireless communication channels (like wifi, NFC or Bluetooth) could be abused by attackers around us who may try to get to our devices and sensitive information by exploiting weak spots in these networks.

When you turn Bluetooth or wifi connectivity on, your device tries to look for any Bluetooth device or wifi network it remembers you have connected to before. Essentially, it "shouts" the names of every device or network on its list to see if they are available to connect to. Someone snooping nearby can use this "shout" to identify your device, because your list of devices or networks is usually unique. This fingerprint-like identification makes it easy for someone snooping close to you to target your device.

For these reasons, it is a good idea to turn off these connections when you are not using them, particularly wifi and Bluetooth. This limits the time an attacker might have to access your valuables without you noticing that something strange is happening on your device.
</details>

# Clear your saved wifi networks

- Save network names and passwords in your [password manager](../../passwords/tools/#strongbox) instead of your device's list of networks.
- If you do save network names and passwords in your list of saved wifi networks, get in the habit of regularly erasing them when you aren't using them anymore and turn off "Auto-Join". To learn how to do this, see [Apple's guide on how to forget wifi networks on iOS devices](https://support.apple.com/en-us/102480).
- You can also reset your phone's network settings altogether to remove all saved wifi networks and bluetooth devices. To learn how to reset your network settings, see [Reset iPhone settings to their defaults](https://support.apple.com/guide/iphone/reset-iphone-settings-iphea1c2fe48/ios).

<details><summary>Learn why we recommend this</summary>

When you turn wifi connectivity on, your device tries to look for any wifi network it remembers you have connected to before. Essentially, it "shouts" the names of every network on its list to see if they are available to connect to. Someone snooping nearby can use this "shout" to identify your device, because your list is usually unique: you have probably at least connected to your home network and your office network, not to mention networks at friends' houses, favorite cafes, etc. This fingerprint-like identification makes it easy for someone snooping in your area to target your device or identify where you have been.

To protect yourself from this identification, erase wifi networks your device has saved and tell your device not to remember networks. This will make it harder to connect quickly, but saving that information in your password manager instead will keep it available to you when you need it.
</details>

# Turn off sharing you're not using

- [Turn off AirDrop by checking the "Receiving Off" option and remove from your list of Contacts anyone you don't want to share data with](https://support.apple.com/en-us/119857#setoptions).

<details><summary>Learn why we recommend this</summary>

Many devices give us the option to easily share files or services with others around us — a useful feature. However, if you leave this feature on when you are not using it, malicious people may exploit it to get at files on your device.
</details>

#  Advanced: figure out whether someone has accessed your device without your permission (basic forensics)

Follow the steps on the following checklists:

- [Review iCloud accounts](https://github.com/securitywithoutborders/guide-to-quick-forensics/blob/master/ios/icloud.md).
- [Check for Mobile Device Management profiles](https://github.com/securitywithoutborders/guide-to-quick-forensics/blob/master/ios/mdm.md).
- [Check for jailbreaks](https://github.com/securitywithoutborders/guide-to-quick-forensics/blob/master/ios/jailbreaks.md).
- [Check devices linked to chat applications](https://github.com/securitywithoutborders/guide-to-quick-forensics/blob/master/smartphones/linkeddevices.md).
- [Monitor network traffic](https://github.com/securitywithoutborders/guide-to-quick-forensics/blob/master/smartphones/network.md).
- If you suspect your device may be compromised, follow the steps in the Digital First Aid Kit troubleshooter [My device is acting suspiciously](https://digitalfirstaid.org/topics/device-acting-suspiciously/#ios-intro).

<details><summary>Learn why we recommend this</summary>

It may not always be obvious when someone has accessed your devices, files or communications. These additional checklists may give you more insight into whether your devices have been tampered with.
</details>

# More resources

- [Security in a Box malware guide](../malware)
- [Personal Safety User Guide for Apple devices](https://help.apple.com/pdf/personal-safety/en_US/personal-safety-user-guide.pdf)
