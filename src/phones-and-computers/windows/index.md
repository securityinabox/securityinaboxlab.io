---
title: Protect your Windows computer
weight: 030
post_date: 18 June 2024
topic: phones-and-computers
---

Microsoft Windows is the world's most used operating system for desktop computers and laptops, but also the most common target for malicious software (malware). Follow this checklist to secure your Windows device.

#  Use the latest version of your device's operating system (OS)

- When updating software, do it from a trusted location and internet connection, like your home or office, not at an internet cafe or coffee shop.
- Updating to the latest version of your operating system may require you to download software and restart a number of times. You will want to set aside time for this where you do not need to do work on your device.
- After updating, check again if there are any further updates available until you do not see any additional new updates.
- [Check which version of Windows is running in your computer](https://learn.microsoft.com/en-us/windows/client-management/client-tools/windows-version-search).
- Make sure your version of Windows is still supported by looking for its name in the search bar of the [Microsoft Product and services lifecycle information page](https://learn.microsoft.com/en-us/lifecycle/products/). If your version of Windows is not supported, you should upgrade your computer to the most recent version.
		- Alternatively, you can check whether your Windows version is still maintained on the [endoflife.date page for Windows](https://endoflife.date/windows).
- [See the most updated versions available](https://en.wikipedia.org/wiki/List_of_Microsoft_Windows_versions).
    - Windows 11 is the latest version. [Check to see if your computer is compatible](https://www.microsoft.com/en-us/windows/windows-11#pchealthcheck) or select Start > Settings > Update & Security > Windows Update > Check for updates. Note that time to download and install Windows 11 depends on your internet connection and computer speed. The upgrade process requires your computer to have enough disk space.
    - Windows 10 or 11 should be set to install updates automatically, but you can follow the instructions on how to check for updates manually in the [Windows update FAQ](https://support.microsoft.com/en-us/windows/windows-update-faq-8a903416-6f45-0718-f5c7-375e92dddeb2).
- [Update your operating system](https://support.microsoft.com/en-us/windows/update-windows-3c5ae7fc-9fb6-9af1-1984-b5e0412c556a).
- If the latest version of Windows will not run on your device, it is best to consider buying a new device or, alternatively, replacing Windows with a version of [Linux](../linux) that supports your older hardware, for example [Lubuntu](https://lubuntu.me/).
- To make sure an update is fully installed, always restart your computer when prompted to do so after the update process is completed.

<details><summary>Learn why we recommend this</summary>

New vulnerabilities in the code that runs in your devices and apps are found every day. The developers who write that code cannot predict where they will be found, because the code is so complex. Malicious attackers may exploit these vulnerabilities to get into your devices.

But software developers do regularly release code that fixes those vulnerabilities. That is why it is very important to install updates and use the latest version of the operating system for each device you use. We recommend setting your device to automatically update so you have one less task to remember to do.
</details>

# Regularly update all installed apps

- [Make sure the Microsoft Store updates all installed apps automatically](https://support.microsoft.com/en-us/windows/turn-on-automatic-app-updates-70634d32-4657-dc76-632b-66048978e51b).

# Use programs and apps from trusted sources

- [Check what apps are installed in your computer](https://support.microsoft.com/en-us/windows/see-all-your-apps-in-windows-fde6f576-0fc0-0813-6b0d-d3ec1d244c50).
- Search the [Microsoft Store](https://www.microsoft.com/store/apps/) for apps installed in your computer to gauge whether they are legitimate.
- [Determine whether your Microsoft software is licensed](https://www.microsoft.com/en-us/howtotell/Whattolookfor.aspx).
- Note that when it comes to finding out whether you can trust software, Microsoft does not offer as many protections to users as other platforms do. Carefully evaluate whether it is safe to install a given program or app.
    - To learn how to decide whether you should use a certain app, see [how Security in a Box chooses the tools and services we recommend](../../about/how/#how-we-choose-the-tools-and-services-we-recommend).
    - If you are unsure, ask an expert before you install any app.

<details><summary>Learn why we recommend this</summary>

Only install apps from the Microsoft Store or from the websites of the developers themselves. "Mirror" download sites may be untrustworthy, unless you know and trust the people who provide those services. If you decide that the benefit of a particular app outweighs the risk, take additional steps to protect yourself, like planning to keep sensitive or personal information off that device.
</details>

# Remove apps that you do not need and do not use

- [Learn how to uninstall or remove apps and programs](https://support.microsoft.com/en-us/windows/uninstall-or-remove-apps-and-programs-in-windows-10-4b55f974-2cc6-2d2b-d092-5905080eaf98).
- If you cannot uninstall an app or service, you can try to disable them.
    - Learn how to [disable apps in Windows 11](https://techcommunity.microsoft.com/t5/windows-11/how-to-disable-startup-programs-in-windows-11-3-ways/m-p/3249863).
    - Learn how to [disable startup services in Windows 10](https://answers.microsoft.com/en-us/windows/forum/all/how-to-totally-disable-app-without-uninstalling-it/2af823bb-d301-4fe5-ae49-673fe60032d7).

<details><summary>Learn why we recommend this</summary>

New vulnerabilities in the code that runs in your devices and apps are found every day. The developers who write that code cannot predict where they will be found, because the code is so complex. Malicious attackers may exploit these vulnerabilities to get into your devices.

Removing apps you do not use helps limit the number of apps that might be vulnerable. Apps you do not use may also transmit information about you that you may not want to share with others, like your location. If you cannot remove apps, you may at least try to disable them.
</details>

# Use privacy-friendly apps

- You can browse the web with [Firefox](../../tools/firefox/).
- You can read your email with [Thunderbird](https://www.thunderbird.net).
- You can use [free and open source software for almost everything you need to do on Windows](https://github.com/thechampagne/awesome-windows). To decide whether you should use a certain app, see [how Security in a Box chooses the tools and services we recommend](../../about/how/#how-we-choose-the-tools-and-services-we-recommend).

<details><summary>Learn why we recommend this</summary>

Windows computers come with built-in software, like for example Microsoft Edge or Microsoft Office, that have a history of [privacy](https://www.theverge.com/2023/4/25/23697532/microsoft-edge-browser-url-leak-bing-privacy) and [security issues](https://www.techtarget.com/searchsecurity/news/366549116/Vendors-criticize-Microsoft-for-repeated-security-failings). Instead, you can use more privacy-friendly apps to browse the web, read your email and much more.

Once you have installed and set up these privacy-friendly apps, you can also [uninstall or disable](#remove-apps-that-you-do-not-need-and-do-not-use) the apps that were installed by default in your device and that you aren't planning to use.
</details>

# Check your app permissions

Review all permissions one by one to make sure they are enabled only for apps you use. The following permissions should be turned off in apps you do not use, and considered suspicious when used by apps you do not recognize:

- Location
- Contacts
- Messaging
- Microphone
- Voice or speech recognition
- (Web)camera
- Screen recording
- Call logs or call history
- Phone
- Calendar
- Email
- Pictures library
- Movies or videos, and their libraries
- Fingerprint reader
- Near field communications (NFC)
- Bluetooth
- WiFi
- Wired connections
- Any setting with "disk access," "files," "folders," or "system" in it
- Any setting with "install" in it
- Facial recognition
- Allowed to download other apps

Learn how to find your app settings and what each permission allows an app to do in [the Microsoft guide on app permissions](https://support.microsoft.com/en-us/windows/app-permissions-aea98a7c-b61a-1930-6ed0-47f0ed2ee15c).

- Make sure that only apps you use have access to the following permissions:

- Account Info
- Motion
- Radios
- Tasks

In addition to checking on the permissions mentioned above, turn off permissions for the following if you see them:

- Allow elevation
- Modifiable app
- Packaged services
- Package write redirect compatability shim
- Unvirtualized resources

<details><summary>Learn why we recommend this</summary>

Apps that access sensitive digital details or services — like your location, microphone, camera or device settings — can also leak that information or be exploited by attackers. So if you do not need an app to use a particular service, turn that permission off.
</details>

# Turn off location and wipe history  

- Get in the habit of turning off location services overall, or when you are not using them, for your whole device as well as for individual apps.
- Regularly check and clear your location history if you have it turned on.
- [Learn how location services work and how to turn them off](https://support.microsoft.com/en-us/windows/windows-location-service-and-privacy-3a8eee0a-5b0b-dc07-eede-2a5ca1c49088).
- Regularly check and clear your location history in Google Maps if you use it. To delete past location history and set it so Google Maps does not save your location activity, follow the instructions [for your Google Maps Timeline](https://support.google.com/accounts/answer/3118687?#delete) and [your Maps activity](https://support.google.com/maps/answer/3137804).

<details><summary>Learn why we recommend this</summary>

Many of our devices keep track of where we are, using GPS, cell phone towers and the wifi networks we connect to. If your device is keeping a record of your physical location, it makes it possible for someone to find you or use that record to prove that you have been in certain places or associated with specific people who were somewhere at the same time as you.
</details>


# Create separate user accounts on your devices

- [We recommend using a local account](https://support.microsoft.com/en-us/windows/manage-user-accounts-in-windows-104dc19f-6430-4b49-6a2b-e4dbd1dcdf32) (one that is just on your device) instead of a Microsoft account to reduce the amount of data you share with Microsoft when using your Windows computer. Be aware you will not be able to share data across devices if you do this.
    - To create a local account, go to Settings > Accounts > Other Users. Click Add Account, then click "I don't have this person's sign-in information". Finally select the option "Add a user without a Microsoft account".
- Create more than one account on your device, with one having "admin" (administrative) privileges and the others "standard" (non-admin) privileges.
    - Only you should have access to the admin account.
    - Standard accounts should not be allowed to access every app, file or setting on your device.
- Consider using a standard account for your day-to-day work:
    - Use the admin account only when you need to make changes that affect your device security, like installing software.
    - Using a standard account daily can limit how much your device is exposed to security threats from malware.
    - When you cross borders, having a "travel" account open could help hide your more sensitive files. Use your judgment: will the border authorities confiscate your device for a thorough search, or will they force you to log in to your account so they can take a quick look? If you expect they won't look too deeply into your device, being logged in with a standard account for work that is not sensitive provides you some plausible deniability.
- Learn how to [manage user accounts in Windows](https://support.microsoft.com/en-us/windows/manage-user-accounts-in-windows-104dc19f-6430-4b49-6a2b-e4dbd1dcdf32).

<details><summary>Learn why we recommend this</summary>

We strongly recommend not sharing devices you use for sensitive work with anyone else. However, if you must share your devices with co-workers or family, you can better protect your device and sensitive information by setting up separate accounts on your devices in order to keep your administrative permissions and sensitive files protected from other people.
</details>

# Remove unneeded accounts associated with your device

- Learn how to remove user accounts in the ["Remove a user account" section of the guide on how to manage user accounts in Windows](https://support.microsoft.com/en-us/windows/manage-user-accounts-in-windows-104dc19f-6430-4b49-6a2b-e4dbd1dcdf32).

<details><summary>Learn why we recommend this</summary>

If you don't intend for someone else to access your device, it is better to not leave that additional "door" open on your machine (this is called "reducing your attack surface"). Additionally, checking what accounts are associated with your device could reveal accounts that have been added to  your device without your knowledge.
</details>

#  Secure the accounts connected with your device

- [Learn how to secure Microsoft accounts connected with your device](https://support.microsoft.com/en-us/account-billing/check-the-recent-sign-in-activity-for-your-microsoft-account-5b3cfb8e-70b3-2bd6-9a56-a50177863357).
- [Check the recent activity of your account](https://support.microsoft.com/en-us/account-billing/what-is-the-recent-activity-page-23cf5556-4dbe-70da-82c8-bb3a8d8f8016) to make sure there hasn't been any suspicious activity.
- Also see our guide on [social media accounts](../../communication/social-media) to check other accounts connected with your device.
- Take a picture or [screenshot](https://support.microsoft.com/en-us/windows/keyboard-shortcut-for-print-screen-601210c0-b3a9-7b58-bc40-bae4dcf5f108) of your account activity if you see anything suspicious, like devices you have disposed of, don't have control of or don't recognize.

<details><summary>Learn why we recommend this</summary>

Most devices have accounts associated with them, like Microsoft accounts for your Windows computer. More than one device may be logged in to your Microsoft account or services (like your Windows computer, your Xbox console or Microsoft products like Skype). If someone else has access to your account without your authorization, the checks included in this section will help you see and stop this.
</details>

# Protect your user account and computer with a strong passphrase

- Use a [long passphrase](../../passwords/passwords) (longer than 16 characters), not a short password or PIN.
- To learn how to change your passphrase, see [the Windows guide on sign-in options and account protection](https://support.microsoft.com/windows/windows-10-sign-in-options-and-account-protection-7b34d4cf-794f-f6bd-ddcc-e73cdf1a6fbf).
- Ensure Windows Hello is turned off.
- Making it possible to use your fingerprint, face, eyes or voice to unlock your computer (for example with Windows Hello) can be used against you by force; do not use these options unless you have a disability which makes typing impossible.
     - Remove your fingerprints, face or iris from your device if you have already entered them. Learn how to remove your biometric data by disabling Windows Hello in the ["Windows Hello" section of the guide on Windows sign-in options](https://support.microsoft.com/en-us/windows/windows-sign-in-options-and-account-protection-7b34d4cf-794f-f6bd-ddcc-e73cdf1a6fbf#ID0EFLBBDBBF).
- On Windows 10, turn on "Require sign-in" and set a lock time.
- On Windows 11, next to "If you've been away, when should Windows require you to sign in again?" select "When PC wakes up from sleep".

<details><summary>Learn why we recommend this</summary>

While technical attacks can be particularly worrying, your device may as well be confiscated or stolen, which may allow someone to break into it. For this reason, it is smart to set a strong passphrase to protect your user account, so that nobody can access your device just by turning it on and guessing a short password.

We do not recommend screen lock options other than passphrases. If you are arrested, detained or searched, you might easily be forced to unlock your device with your face, eyes or fingerprint. Someone who has your device in their possession may use software to guess short passwords or PINs. Someone who has dusted for your fingerprints can make a fake version of your finger to unlock your device if you set a fingerprint lock and similar hacks have been demonstrated for face unlock.

For these reasons, a long passphrase is the safest way to protect your user account and your computer.
</details>

# Set your screen to sleep and lock

- [Set your screen to lock a short time after you stop using it (5 minutes is good)](https://support.microsoft.com/en-us/windows/how-to-adjust-power-and-sleep-settings-in-windows-26f623b5-4fcc-4194-863d-b824e5ea7679).
- Get in the habit of pressing Windows Logo Key + L to lock your screen when you step away from your machine.

# Control what can be seen when your device is locked

- [Stop notifications from appearing when your device is locked](https://support.microsoft.com/windows/change-notification-settings-in-windows-10-ddcbbcd4-0a02-f6e4-fe14-6766d850f294).

<details><summary>Learn why we recommend this</summary>

A strong screen lock will give you some protection if your device is stolen or seized — but if you don't turn off notifications that show up on your lock screen, whoever has your device can see information that might leak when your contacts send you messages or you get new email.
</details>

# Make sure voice controls are not enabled

- [Make sure Voice Access is not enabled](https://support.microsoft.com/en-us/topic/set-up-voice-access-9fc44e29-12bf-4d86-bc4e-e9bb69df9a0e).
- [Disable Copilot](https://www.reddit.com/r/WindowsHelp/comments/1c1k1o9/disable_windows_copilot_ai_windows_10_and_11/).
- Learn more on [speech, voice activation, inking, typing and privacy in Windows](https://support.microsoft.com/en-us/windows/speech-voice-activation-inking-typing-and-privacy-149e0e60-7c93-dedd-a0d8-5731b71a4fef).


<details><summary>Learn why we recommend this</summary>

If you set up a device so you can speak to it to control it, for example through Voice Access or Copilot, the system is constantly listening while this feature is on. It also becomes possible for someone else to install code on your device that could capture what your device is listening to.

It is also important to consider the risk of voice impersonation: someone could record your voice and use it to control your computer without your permission.

If you have a disability that makes it difficult for you to type or use other manual controls, you may find voice controls necessary. This section provides instructions on how to set them up more safely. However, if you do not use voice controls for this reason, it is much safer to turn them off.
</details>

# Use a physical privacy filter that prevents others from seeing your screen

- For more information on this topic, see [the Security Planner guide on privacy filters](https://securityplanner.consumerreports.org/tool/use-a-privacy-filter-screen).

<details><summary>Learn why we recommend this</summary>

While we often think of attacks on our digital security as highly technical, you might be surprised to learn that some human rights defenders have had their information stolen or their accounts compromised when someone looked over their shoulder at their screen or used a security camera to do so. A privacy filter makes this kind of attack, often called shoulder surfing, less likely to succeed. You should be able to find privacy filters in the same shops where you find other accessories for your devices.
</details>

# Use a camera cover

- First of all, figure out whether and where your device has cameras. Your computer might have more than one if you use a plug-in camera as well as one built into your device.
- You can create a low-tech camera cover: apply a small adhesive bandage on your camera and peel it off when you need to use the camera. A bandage works better than a sticker because the middle part has no adhesive, so your lens won't get sticky.
- Alternatively, search your preferred store for the model of your computer and "webcam privacy cover thin slide" to find the most suitable sliding cover for your device.
- If you have administrator access, you can consider disabling altogether your cameras.
    - [Learn how to disable your camera in Windows 11](https://support.microsoft.com/en-us/windows/manage-cameras-with-camera-settings-in-windows-11-97997ed5-bb98-47b6-a13d-964106997757#bkmk_enabledisable).
    - Learn how to disable your camera in Windows 10 in the ["Turn off the camera" section of the guide on How to use the Camera app](https://support.microsoft.com/en-us/windows/how-to-use-the-camera-app-ea40b69f-be6a-840e-9c8c-1fd6eea97c22#WindowsVersion=Windows_10).

<details><summary>Learn why we recommend this</summary>

Malicious software may turn on the camera on your device in order to spy on you and the people around you, or to find out where you are, without you knowing it.
</details>

# Turn off connectivity you're not using

- Completely power off your devices at night.
- Get into the habit of keeping wifi, Bluetooth and/or network sharing off and only enable them when you need to use them.
- Airplane mode can be a quick way to turn off connectivity on your computer. Learn how to enable Airplane mode to turn off all wireless connections, including wifi, cellular, Bluetooth and Near Field Communication (NFC), in [the Microsoft guide on how to turn airplane mode on or off](https://support.microsoft.com/en-au/windows/turn-airplane-mode-on-or-off-f2c2e0a1-706f-ff26-c4b2-4a37f9796df1).
- When you connect to a network in a place you trust (your home or office) set it to "private" and set public networks (cafes, co-working spaces, etc.) to "public". Learn more about these settings in [the guide on public and private wifi networks in Windows](https://support.microsoft.com/windows/make-a-wi-fi-network-public-or-private-in-windows-10-0460117d-8d3e-a7ac-f003-7a0da607448d).
- [Make sure Bluetooth is off](https://support.microsoft.com/windows/turn-bluetooth-on-or-off-9e92fddd-4e12-e32b-9132-5e36bdb2f75a).
- Make sure your device is not providing an internet connection to someone else. Learn how to turn this setting off in the [guide on using your Windows PC as a mobile hotspot](https://support.microsoft.com/en-us/windows/use-your-pc-as-a-mobile-hotspot-c89b0fad-72d5-41e8-f7ea-406ad9036b85).
- To turn off wifi:
    - In Windows 11, go to Settings > Network & internet and turn off the wifi switch.
    - In Windows 10, go to Settings > Network & internet > WiFi and turn off the wifi switch.

<details><summary>Learn why we recommend this</summary>

All wireless communication channels (like wifi, NFC or Bluetooth) could be abused by attackers around us who may try to get to our devices and sensitive information by exploiting weak spots in these networks.

When you turn Bluetooth or wifi connectivity on, your device tries to look for any Bluetooth device or wifi network it remembers you have connected to before. Essentially, it "shouts" the names of every device or network on its list to see if they are available to connect to. Someone snooping nearby can use this "shout" to identify your device, because your list of devices or networks is usually unique. This fingerprint-like identification makes it easy for someone snooping close to you to target your device.

For these reasons, it is a good idea to turn off these connections when you are not using them, particularly wifi and Bluetooth. This limits the time an attacker might have to access your valuables without you noticing that something strange is happening on your device.
</details>

# Clear your saved wifi networks

- Save network names and passwords in your [password manager](../../passwords/tools/#keepassxc) instead of in your device's list of networks.
- If you do save network names and passwords in your list of saved wifi networks, make sure to:
    - not connect automatically to the networks you save: in the list of networks you see when clicking the wifi icon in the taskbar, right-click the network you have saved to uncheck the "Connect automatically" option.
    - get in the habit of regularly forgetting saved wifi networks when you aren't using them anymore. [Learn how to delete your saved wifi networks](https://answers.microsoft.com/en-us/windows/forum/all/delete-unwanted-wi-fi-networks-on-my-list/ee6a7dca-aaef-402f-b7a7-b2d7d2c0e61e).

<details><summary>Learn why we recommend this</summary>

When you turn wifi connectivity on, your device tries to look for any wifi network it remembers you have connected to before. Essentially, it "shouts" the names of every network on its list to see if they are available to connect to. Someone snooping nearby can use this "shout" to identify your device, because your list is usually unique: you have probably at least connected to your home network and your office network, not to mention networks at friends' houses, favorite cafes, etc. This fingerprint-like identification makes it easy for someone snooping in your area to target your device or identify where you have been.

To protect yourself from this identification, erase wifi networks your device has saved and tell your device not to look for networks all the time. This will make it harder to connect quickly, but saving that information in your password manager instead will keep it available to you when you need it.
</details>

# Turn off sharing you're not using

- [Stop sharing files on OneDrive](https://support.microsoft.com/office/stop-sharing-onedrive-or-sharepoint-files-or-folders-or-change-permissions-0a36470f-d7fe-40a0-bd74-0ac6c1e13323).
- [Stop sharing files over local networks](https://support.microsoft.com/windows/file-sharing-over-a-network-in-windows-10-b58704b2-f53a-4b82-7bc1-80f9994725bf).
- [Stop sharing files with nearby devices](https://support.microsoft.com/windows/share-things-with-nearby-devices-in-windows-10-0efbfe40-e3e2-581b-13f4-1a0e9936c2d9).

<details><summary>Learn why we recommend this</summary>

Many devices give us the option to easily share files or services with others around us — a useful feature. However, if you leave this feature on when you are not using it, malicious people may exploit it to get at files on your device.
</details>

# Turn off Autoplay

- [Find the Autoplay setting in Windows 10](https://answers.microsoft.com/en-us/windows/forum/all/how-to-change-autoplay-settings-in-windows-10/0ed2fe8d-10d5-434b-83bc-cde1baf28fe6) and turn it off.
- In Windows 11, go to Settings > Bluetooth & Devices > Autoplay and turn off the Autoplay switch.

<details><summary>Learn why we recommend this</summary>

The Autoplay feature in Windows may automatically run malicious code from a drive you insert into your device. Turning this feature off protects you against such a risk.
</details>

# Turn off advertising and other "privacy options"

- Review options in Settings > Privacy (Windows 10) or Settings > Privacy & security > General (Windows 11) and turn off at least the first 3 options.

<details><summary>Learn why we recommend this</summary>

Sharing additional data about your device means that more information about you is floating around in public. Limiting this sharing is an extra way to protect yourself.
</details>

# Protect your device with Windows Security

- Windows Security is built into Windows and includes an antivirus program called Microsoft Defender Antivirus. Activate Microsoft Defender rather than using a third-party antivirus.
- Read [Stay protected with Windows Security](https://support.microsoft.com/en-us/windows/stay-protected-with-windows-security-2ae0363d-0ada-c064-8b56-6a39afb6a963) to get familiar with all the security features offered by Windows Security.
- One option that you may want to turn on is [SmartScreen](https://learn.microsoft.com/en-us/windows/security/operating-system-security/virus-and-threat-protection/microsoft-defender-smartscreen/), which protects against phishing or malware websites and applications, and the downloading of potentially malicious files.
    - To make sure SmartScreen is enabled, open Windows Security, go to App & browser control > Reputation-based protection and turn on the switch for SmartScreen for Microsoft Edge.

# Use a firewall

- [Turn on Microsoft Defender Firewall](https://support.microsoft.com/en-us/windows/turn-microsoft-defender-firewall-on-or-off-ec0844f7-aebd-0583-67fe-601ecf5d774f).

<details><summary>Learn why we recommend this</summary>

Firewalls are a security option that stops unwanted connections to your device. Like a security guard posted at the door of a building to decide who can enter and who can leave, a firewall receives, inspects and makes decisions about communications going in and out of your device. We recommend turning yours on to prevent malicious code from trying to access your device. The default firewall configuration should be enough protection for most people.
</details>

#  Advanced: Stop malicious code from running

- [Install and run Hardentools to keep malicious code from running](https://github.com/securitywithoutborders/hardentools#readme).

# Advanced: figure out whether someone has accessed your device without your permission (basic forensics)

- Review [programs launching at startup](https://github.com/securitywithoutborders/guide-to-quick-forensics/blob/master/windows/autoruns.md), [network connections](https://github.com/securitywithoutborders/guide-to-quick-forensics/blob/master/windows/network.md) and [running processes](https://github.com/securitywithoutborders/guide-to-quick-forensics/blob/master/windows/processes.md).
- If you suspect your device may be compromised, follow the steps in the Digital First Aid Kit troubleshooter [My device is acting suspiciously](https://digitalfirstaid.org/topics/device-acting-suspiciously/#windows-intro).

<details><summary>Learn why we recommend this</summary>

It may not always be obvious when someone has accessed your devices, files or communications. These additional checklists may give you more insight into whether your devices have been tampered with.
</details>
