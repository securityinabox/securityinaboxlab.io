---
title: Protect against physical threats
weight: 002
post_date: 9 April 2024
topic: phones-and-computers
---

We do a lot of digital work to protect sensitive information. But that is only one aspect of information security. The work you do to protect your valuable devices and documents can be undone in an instant if your devices are lost, stolen, tampered with, confiscated or damaged. Planning for physical security is as important as protecting your devices digitally.

Careful risk assessment, maintaining a safe work environment and writing up a security policy can help you avoid physical disasters. Even if you are not working with a formal organization, it is a good idea to write out guidelines and response plans for yourself, your household and the people you work with.

Both criminals and politically motivated attackers may have reasons to target your data. They might be seeking financial information, sensitive data related to your work or personal details they can use to intimidate, blackmail or impersonate you. You could also be targeted due to information received from others, for example contacts lists, interviews, testimonies or identifying information on victims or witnesses. Criminal and political attacks are often difficult to distinguish and attempts at obtaining sensitive data often look like attempts at stealing valuable hardware.

When planning your protection, it is important to always consider the well-being of all the people involved. To learn more on this, see [Front Line Defenders' resources for well-being and stress management](https://www.frontlinedefenders.org/en/resources-wellbeing-stress-management).

While working on your physical protection, you are bound to exchange and store a lot of sensitive information in the process. Make sure to protect both [your sensitive information](../../files/secure-file-storage) and [the privacy of your online communications](../../communication/private-communication/).

Start with the steps below to develop a physical security plan.

# Think about inexpensive, low-tech security measures

- While it is important to budget for physical security, money is not the only way to achieve protection. Your actions, procedures, teamwork, preparation, planning, practice, time and learning are all free of cost.
- Consider creative, low-tech protection solutions: A dog can be as good an alarm as the latest surveillance camera. Legend has it that ancient Japanese castles used intentionally creaky floors to detect intruders.
- Plan for many different security measures. Like in a rope, the more threads of the rope there are, the stronger the rope is.

<details><summary>Learn why we recommend this</summary>

A high-tech solution is not always necessary to be safe. Think of our ancestors − they stayed secure without surveillance cameras, without the latest electronic alarm systems, without reinforced steel doors and even without electricity. Those principles of security can still be applied.

If you can’t buy the latest fire extinguisher, what else could you use to douse a fire? Sand, mud or woollen blankets are all measures people used in the past and can still use today. Rather than saying: "I can't buy the latest fire extinguishers, therefore I can't put out a fire," improvise, be creative and find the logic behind the recommended security steps.

Using many different security measures is called "defense in depth." Each measure may be defeated or fail, but the combination of these measures makes you more protected. The more layers you have, the stronger your security plan is.
</details>

# Create a physical security policy, with colleagues and family

- If you live with other people or share an office with another organization, talk to them about security. Try to determine what behaviors you can expect from one another and from visitors.
- Get to know your neighbors. Depending on the security climate of your workspace, this may provide one of two opportunities:
    - Neighbors could become allies who can help you keep an eye on your home or office.
    - If not, your neighbors will become another entry on the list of potential threats that you need to address.
- Set aside time to work on a policy document.
    - Coordinate with colleagues so they can participate.
    - Coordinate with family members as well to make plans for keeping your home safe.
    - Make sure everyone has time to ask questions about what they are supposed to do and why.
    - Determine what support your colleagues or family members need to be able to act on the policy, and make sure that support is available.
- Set dates to revisit the policy.
- Set dates for regular security and protection capacity building exercises.
- Establish security briefing procedures and expectations about sharing information on incidents.
- Store and back up your policy documents in ways that are quickly accessible.
- Plan how you will present your policy to people joining your organization.
- See Front Line Defenders' [Workbook on Security](https://www.frontlinedefenders.org/en/resource-publication/workbook-security-practical-steps-human-rights-defenders-risk) for practical advice on how to create a security policy.
- You can find security policy templates in [Access Now Digital Security Helpline's shared resources](https://gitlab.com/AccessNowHelpline/helpline_documentation_resources/-/tree/master/templates/Organizational_Security_Policies-Templates).


<details><summary>Learn why we recommend this</summary>

Emergencies often impact our ability to think clearly. For this reason, it is a good idea to write up your plans for security in a policy document and revise it as your situation changes. Work on this document with your colleagues and/or family to make sure everyone knows what to do. This will take some time, but doing it in advance ensures everyone will know how to act on small but important details.
</details>

# Have a communication plan in case of emergency

It is important to have a plan both for your household and your office. Consider including the following information:

- The persons in your network of allies and supporters who can come to your assistance.
- Emergency contacts and medical conditions of staff.
- Whom to contact in the event of a fire, flood or other natural disaster.
- How to respond to a burglary or an office raid.
- What steps to take if a device is lost or stolen.
- Who should be notified if sensitive information is disclosed or misplaced.
- How to recover information from your off-site backup system.
- How to perform certain important emergency repairs.
- How to contact the companies that provide services like electric power, water and internet access.

# What goes in your plan?

Assess the risks and vulnerabilities you face and reply to the following questions:

- How might your information be lost or compromised?
- What would happen if it was?
- What devices do you use?
    - Make an inventory.
    - Include serial numbers and physical descriptions.
    - Include which devices connect to which services (for example, social media accounts, remote calendars, email accounts, online file storage, etc.).
- Where are these items physically located?
    - Think broadly: not just about information stored at the office or at home, but also in someone's luggage, in a recycling bin out back or "somewhere on the internet" (which often means on servers run by online service providers, social media companies or other far away people you do not know).
- What is your policy on people using their own devices for work?
- What communication channels do you use, and how do you use them?
    - Examples might include letters, faxes, mobile devices, landline phones, emails, video calls, social media and secure messaging platforms.
- How do you store important or sensitive information?
    - Computer hard drives, email and web servers, USB memory sticks, external hard drives, mobile phones, printed paper and hand-written notes are all common means of data storage.
        - In each case, include information in your plan about whether or not the data is encrypted, whether and where there is a backup and who has access to the keys or passwords needed to decrypt this information.
    - See [the chapter on protecting your sensitive information](../../files/secure-file-storage/) and [our basic security guides on encrypting your devices and setting screen locks](../) to learn how to stop anyone who gets physical access to your device from getting access to your files.
- How do you destroy sensitive data when you no longer need it?
    - How will you securely dispose of paper rubbish that contains sensitive information?
    - How will you remove sensitive information from devices you are getting rid of?
    - Learn how to securely delete data in [our guide on destroying sensitive information](../../files/destroy-sensitive-information/).
- What is your plan for traveling?
    - How will you, your colleagues and family members interact with immigration and border security personnel in various circumstances?
    - How will you all handle sensitive data or software that might be seen as incriminating?
    - What information do you all need about travel insurance, if any?
    - Would it improve your security to partner with someone who will not be traveling and check in at prearranged times?
    - What will you do if a colleague fails to check in as planned?
- What will you do in different emergencies?
    - Make simple checklists to make it easier to act under stressful conditions.
    - Include information about access to legal support.
    - Find out what legal protections you have against law enforcement personnel, landlords and others who might try to enter your home or office.

<details><summary>Learn why we recommend this</summary>

Think about what would happen if you lost your devices and documents due to theft, disaster or confiscation. Who would be affected? Would you be able to continue your work? Thinking through different kinds of risks can help reduce their potential impact on your work.

Any piece of your information might be vulnerable in more than one way. For example, files you store on a USB memory stick could be vulnerable to malware, but you could also lose the stick or it could be stolen. Include each of these possibilities in your plan. Some steps you take to protect your security, like backing up your files to a device that is not in your office, are helpful against both digital and physical threats.

There is no universally "correct" policy; the policy that will help you depends on your situation. For example, if you are making a decision about what to do when carrying devices out of the office, you may want to be more specific. Do you need to make a policy for someone walking across town or for going through a border? Will somebody else be carrying your bag? Is the risk to the device different if you are going to a protest?
</details>

# Create an office access policy

- Your policy should include rules about key distribution, monitoring systems like cameras, alarm systems and what to do with people delivering packages, doing technical maintenance or cleaning your space.
- Think of your physical security as having the following layers, and plan for the protection of each of them:
    - The walls or fences of your site,
    - Between the walls/fences and the doors and windows of your building,
    - Inside your building,
    - Finally, a safe room inside that building and your evacuation plans if those other layers are breached.
- Decide which parts of your space should be restricted to visitors.
- If possible, arrange rooms for greater privacy and security:
    - Create a reception area where visitors can be met when they enter the office.
    - Create a meeting room that is separate from your normal workspace.
    - If you work out of your home, this might require that you move documents and equipment into a bedroom or some other private space when meeting with visitors.
- Consider purchasing devices from known, trusted vendors like Apple, Google, Samsung, Sony, etc. Avoid buying devices that are no-name brands as they could come preloaded with malware and can lack security updates.
- Your printers, monitors, projectors and other devices likely have USB, ethernet or other ports. Keep them out of reach or outside public areas, so nobody can plug into them a device that can spy on you.

<details><summary>Learn why we recommend this</summary>

We often think of digital threats as only technical. But hackers think of "social engineering," or convincing you to let them access physical spaces near your devices, as part of their toolkit to get to your digital valuables. Having a policy about who can access which parts of your space and why, and thinking through how you will keep it from being violated, will limit the possibility of this happening.
</details>

# When working outside your home or office

- Public wifi and internet cafes should be considered insecure.
    - Use a virtual private network (VPN) or the Tor Browser to prevent attacks when you connect to public wifi. You can learn more about these tools in the guide on how to [browse anonymously](../../internet-connection/anonymity) and [bypass censorship on the internet](../../internet-connection/circumvention).
- Consider carrying your laptop in something that does not look like a laptop bag.
- Keep devices near you at all times. For example, do not leave your device out of your sight when charging.
- Consider traveling with a security cable and practice finding workspaces near objects to which you can attach one. Thieves often exploit meal times and restroom visits to steal unattended equipment from hotel rooms and cafes.
- Remember that hotel safe deposit boxes are accessible to hotel staff who have the master key.

# Consider using surveillance cameras and motion sensors		

- Remember that if what you are looking for is a fast alert, low-tech solutions like bells or alarms which ring when a door opens or a dog that will bark can be as effective as a surveillance camera, if not more so. Closed-circuit TV (CCTV) cameras may require someone to monitor them or review footage.
- Consider whether a camera to monitor your space would put those who work there or nearby at risk if your adversary had access to these cameras. Balance this risk against your need to know if your space has been searched, raided or burglarized.
- Avoid using "internet of things" devices like Amazon's Ring. Many internet of things systems are notoriously vulnerable to spying. Amazon, which offers the Ring system, has been known to share camera footage with law enforcement without users' permission, and may use your data in other ways it does not disclose.
- As an alternative, consider using [Haven](https://guardianproject.info/apps/org.havenapp.main/), which was specially created to help human rights defenders monitor their own spaces with control over their own data.
- When using a surveillance camera, you may want to transmit the video to a location other than the one you are monitoring. This video should be sent encrypted and also remain encrypted wherever it is stored. Consider how you can best protect physical access to these videos, how long you will keep them and how you will securely delete them.

# Protect your local network

## Use ethernet cables, but only in protected areas

- It may be safer to use ethernet cables. A wireless network can be breached without physical access.
- Avoid running ethernet cables outside your building and outside your physically protected areas, as it makes it easier for someone to tamper with them when you are not looking.

## Set a strong passphrase on your wireless network

- Follow our guide on [creating and maintaining strong passwords](../../passwords/passwords).
- [Set your wireless network to use WPA2 or WPA3 security](../malware/#secure-your-router).
- The steps to secure a wireless network will depend on which router you use.

<details><summary>Learn why we recommend this</summary>

It is possible for anyone in range of your wifi signal to spy on your network or communications. If your wifi relies on a weak password − or no password at all − anyone within range is a potential intruder.
</details>

## Install a firewall router

- Consider purchasing and installing a firewall router to protect and separate your office or home network from the router of your internet service provider company. Read more on this in our guide on how to [secure your router](../malware/#secure-your-router).

<details><summary>Learn why we recommend this</summary>

Internet service providers have full access to the router they have provided you with to give you access to the internet. In some cases, they can use this router to access your network, communication and devices. So placing your own router with firewall function between your local network and the ISP's router (or replacing the ISP's router with your own) and connecting to your router rather than to the ISP's router can stop your ISP from seeing what devices connect to your network.
</details>

## Avoid connecting unnecessary devices to your network

- Televisions, cameras, phones, printers, video game consoles and "Internet of Things" (IoT) devices are also computers. They come with many of the same risks. Think twice before connecting new equipment to your home or office network.
- Check what devices are connected to your network and unplug those you are not using.

## Change the name of your wifi network

- Give your wifi a name that does not clearly identify you, your organization or the location of your access point.

## Create a separate wifi account for guests

- Most modern wifi devices have this capability.
- This way, you will not need to give your password to your guests and it will be easier to change the passwords if you need to.
- Ensure the guest network is protected by a strong password. Leaving your router unprotected makes it possible for intruders to tamper with your wifi.

## Lock up networking equipment

- Lock networking equipment like servers, routers, switches and modems inside a secure room or cabinet to make it hard for an intruder to tamper with them.

## Make sure your servers are encrypted

- If your office uses servers, work with the person who manages them to ensure they encrypt their data (see the [section on keeping your digital information private](../../files/secure-file-storage/#consider-encrypting-your-whole-device) for more information on this topic).

<details><summary>Learn why we recommend this</summary>

Encrypting your servers protects your files in the event your servers are seized and confiscated or if they need to be repaired or replaced.</details>

# Prevent accidents and outages

Computers, networking equipment and data storage devices can be quite delicate. The same is true of surveillance cameras, printers, "smart devices" and other hardware. Electric fluctuations like lightning strikes, power surges, cuts to your power, blackouts and brownouts can cause physical damage to digital devices by harming electronic components or destroying data on hard drives. Extreme temperatures, dust and moisture can also damage hardware.

## Use electric sockets and plugs that have ground lines

Having a grounded power connection prolongs the life of your electronic equipment and can reduce the risk of failure due to irregular current or lighting strikes.

## Plug electronics into surge protectors

- Not all power strips contain surge protectors, so check this when buying new ones. A surge protector should specify a maximum voltage and list a rating in joules.
- If your electricity is particularly unstable, you might also need a "power filter" or "line conditioner."
- Put surge protectors, UPSs, power strips and extension cables where they will not be unplugged or powered off if someone bumps them.

## Prevent damage in case of a blackout

- Consider installing Uninterruptible Power Supplies (UPSs). These are somewhat more expensive than surge protectors, but they will stabilize your power supply and provide temporary power in the event of a blackout.
- UPSs are particularly valuable for servers and for desktop computers that do not work if they are not plugged in.
- Consider lighting that does not depend on electricity − strong flashlights, strip lighting powered by batteries, solar charged lights, etc.

## When moving into a new building, test the power supply

- Do this before plugging in important equipment. If the power behaves poorly with lamps, lights and fans, it is likely to damage your digital devices.
- Ask a trusted electrician to review the cabling.

## Get good cables

- When you find yourself with access to high-quality computer cables, surge protectors and power strips, consider picking up a few extras. Sparking power strips that fall out of wall sockets and fail to hold plugs securely can cause people physical harm, as well as damaging your devices and data.

## Ventilation

- If you run your computer inside a cabinet, make sure it has adequate ventilation to prevent it from overheating.
- Computer equipment should not be housed near radiators, heating vents, air conditioners or other ductwork.

# Protect against theft, tampering and confiscation

## Start with locks

- If possible, install high-quality locks on your doors and windows.
    - Keep an up-to-date list of how many keys have been created and to whom they have been distributed.
    - Make plans to collect keys from anyone who no longer needs access (when people leave an organization is an ideal time to take keys back.)
- Consider purchasing a laptop safe or a locking cabinet for sensitive documents and equipment.
- Lock the devices themselves:
    - Most desktop computer cases have a slot where you can attach a padlock to stop someone from getting in and tampering with the hardware.
    - Use locking security cables, where possible, to prevent intruders from stealing desktop and laptop computers.

<details><summary>Learn why we recommend this</summary>

Locks on doors, gates, windows and other entry points act as your outer layers of security. They can help you delay and detect intrusions, not only stop them.
</details>

## Don’t place equipment where it can be easily stolen or tampered with

- Avoid placing important devices, including servers and wifi routers, in easily accessible locations like hallways and reception areas, or next to windows or doors.
- Protect your building's main electric switches with locks.

<details><summary>Learn why we recommend this</summary>

If equipment is located in easy-to-access areas, attackers may steal it or install malware on it. Be aware that attackers may try to cut the electricity to the property before entering, adding to your confusion and making it hard to call for help.
</details>

## Consider the risks of leaving devices behind vs. taking them with you

- You know the level of risk better than anyone else, so consider: is it likely that someone will raid your space or tamper with your devices while you are out? Or is it more likely you will be detained and searched with your devices on you?
- What are you going to do while you're out of the office? Will you travel? Cross borders? Go to a protest where you may likely be arrested? Weigh whether the risk of search or confiscation is higher if the device is on you or if you leave it behind.
- Consider using security tape to cover the USB ports and disk drives of your computer.
- Scatter small objects over your device. Consider taking a picture of it before you go. When you return, compare the photo to the position of the objects, to see if the device has been moved. (One classic tactic is to leave a single thread or hair on the device if there is no breeze in the area; it can be hard for an attacker to re-create the shape of that thread or hair.)
- Consider using a monitoring app like [Haven](https://guardianproject.info/apps/org.havenapp.main/) to watch your things while you are gone.
- Also consider whether a locked box could help secure your devices in case your space is searched.

## Decide whether to register your devices with law enforcement

- If your local law enforcement agency is trustworthy, registering the model and serial number of your devices can help you recover devices if they are stolen.

# Consider what can be seen

- Establish a "clean desk policy": ensure you and your colleagues do not leave sensitive information sitting on your desk, particularly passwords, paper calendars, planners, journals, address books or sticky notes.
- Position computer screens in your home or office so they cannot be seen from outside. Remember to account for windows, open doors and visitor waiting areas.
- Consider ways you can avoid using devices in public, where someone could look over your shoulder.
- If you often work in public, buy privacy screens. These simple plastic covers make it difficult to read a screen unless it is directly in front of you. They are available for laptops, external monitors, tablets and smartphones. Learn more on privacy screens on [Security Planner](https://securityplanner.consumerreports.org/tool/use-a-privacy-filter-screen).
- If you need to hide your location, remember that these clues can be used to figure out where you are: 	
    - Check what is visible on camera in video calls: architecture, signs, trees, geological features like mountains?
    - Consider what can be heard in the background: nearby traffic, sound systems, factories, children playing?
- Cover cameras on your devices when you are not using them, so they cannot be manipulated to spy on you. [Read more on camera covers](../windows/#use-a-camera-cover).

<details><summary>Learn why we recommend this</summary>

Your adversaries may not need to have physical or electronic access to your important devices and data if they can see it by looking at your space or device. They may not need your exact location if they can determine it from things they see or hear in the background of calls or videos. Paper documents are refreshingly immune to malware, but if they are stolen, copied or photographed, they can reveal extremely sensitive information.
</details>

# Decide how you will dispose of sensitive information

- Set a regular schedule to erase files securely in order to ensure sensitive information does not remain on your devices, hard drives, USB memory sticks, removable memory cards (SD cards) from cameras, mobile phones or portable music players and any other device where sensitive data may be stored.
- Learn how to securely delete files, wipe blank space and dispose of an old device in [our guide on destroying sensitive information](../../files/destroy-sensitive-information). Incorporate these instructions for each one of your devices (Android, iOS, Windows, macOS or Linux) into your physical security plan.
- When you are disposing of a computer hard drive, you can make it harder to get data off of it by destroying it with a power drill, a few strong blows from a hammer or nails hammered through it. Do not burn or pour acid on a drive, and do not put it in the microwave.


# More resources

- To learn more about integrating physical, digital, psycho-social and organizational security into your policies and practices, see the [Holistic Security manual](https://holistic-security.tacticaltech.org) ([PDF](https://holistic-security.tacticaltech.org/downloads.html)) and the [Holistic trainers' manual](https://holistic-security.tacticaltech.org/trainers-manual.html) ([PDF](https://holistic-security.tacticaltech.org/ckeditor_assets/attachments/60/holisticsecurity_trainersmanual.pdf)).
- For practical advice on how to create a security policy, see [Front Line Defenders' Workbook on Security](https://www.frontlinedefenders.org/en/workbook-security).
- You can find security policy templates in [Access Now Digital Security Helpline's shared resources](https://gitlab.com/AccessNowHelpline/helpline_documentation_resources/-/tree/master/templates/Organizational_Security_Policies-Templates).
