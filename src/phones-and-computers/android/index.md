---
title: Protect your Android device
weight: 010
post_date: 16 May 2024
topic: phones-and-computers
---

Security starts with setting up your device to protect your information. Follow the steps in this guide to make your Android device more secure. Android devices differ by manufacturer, so you may need to look in a few places to find the settings you are looking for.

# Use the latest version of your device's operating system (OS)

- When updating software, do it from a trusted location and internet connection, like your home or office, not at an internet cafe or coffee shop.
- Updating to the latest version of your operating system may require you to download software and restart a number of times. You will want to set aside time for this where you do not need to do work on your device.
- After updating, check again if there are any further updates available until you do not see any additional new updates.
- If the Android version that runs on your device is unmaintained, it is best to consider buying a new device. Check which Android versions are still maintained in the [Wikipedia Android version history](https://en.wikipedia.org/wiki/Android_version_history) or in the [page on the end of life dates for Android versions](https://endoflife.date/android).
- [Find out which is the most updated version available](https://developer.android.com/about/versions).
- [Compare it to the version your device has installed and update your operating system](https://support.google.com/android/answer/7680439).
- To make sure an update is fully installed, always restart your device when prompted to do so after downloading the update.
- Most system updates and security patches happen automatically. To check if an update is available, follow the instructions in [Check & update your Android version — Get security updates & Google Play system updates](https://support.google.com/android/answer/7680439).
- Additionally, [check the "Android security update" date on your device](https://support.google.com/android/answer/7680439?hl=en). Security updates are released at least once each month.
    - Be aware that some phone manufacturers can be one month to a year behind in implementing the latest security updates on their devices.
    - If you find this date on your device consistently lags behind the [latest security update date](https://source.android.com/docs/security/bulletin/asb-overview), consider buying a phone from a manufacturer which implements security updates faster, for example a recent Google Pixel model. Be aware that this may be rather expensive.
    - Also consider that mobile phone producers only provide security updates for a certain period of time, which varies depending on the model and brand of a device.
        - If you have a Google Pixel phone, you can check until when you will receive security updates in [the official Google documentation](https://support.google.com/pixelphone/answer/4457705).
        - If you have a Samsung mobile device, see [the page on Samsung security updates](https://security.samsungmobile.com/workScope.smsb).
        - If you have a Fairphone, see the [page on end of life dates for Fairphone updates](https://endoflife.date/fairphone).
        - If you have a Motorola device, see the [Motorola support page on security updates](https://en-us.support.motorola.com/app/software-security-update).
        - If you have a Nokia device, see the [Security and Maintenance Release Summary on Nokia's website](https://www.hmd.com/en_int/security-updates).
        - For other models, see [C. Scott Brown's article on the phone update policies from every major Android manufacturer](https://www.androidauthority.com/phone-update-policies-1658633/).
    - If you find your security patches consistently lag behind the latest level, consider buying a newer phone from a manufacturer which implements security updates faster, for example a recent Google Pixel model. Be aware that this may be rather expensive.

<details><summary>Learn why we recommend this</summary>

New vulnerabilities in the code that runs in your devices and apps are found every day. The developers who write that code cannot predict where they will be found, because the code is so complex. Malicious attackers may exploit these vulnerabilities to get into your devices. But software developers do regularly release code that fixes those vulnerabilities. That is why it is very important to install updates and use the latest version of the operating system for each device you use. We recommend setting your device to automatically update so you have one less task to remember to do.
</details>

# Regularly update all installed apps

- Check that the Google Play Store app is updated by following the instructions in [the official guide on how to update the Google Play Store](https://support.google.com/googleplay/answer/113412#update_google_play_store).
- Follow the instructions in [the official documentation](https://support.google.com/googleplay/answer/113412#auto) to learn how to update all Android apps automatically.

# Use apps from trusted sources

- Avoid "[rooting](https://www.howtogeek.com/335642/rooting-android-just-isnt-worth-it-anymore/)" your device.
- Install apps from the [Google Play Store](https://play.google.com/store).
- [Check if your apps were installed from the store and disable installation from unknown sources by using Google Play Protect](https://support.google.com/accounts/answer/2812853).

<details><summary>Learn why we recommend this</summary>

The [Google Play Store](https://play.google.com/store) is the official app store for Android. This centralized way of distributing apps makes it easier for people to find what they need, and it also makes it easier for Google to monitor the offered apps for major security violations, as well as generally enforce their policies on listed apps. Only install apps from the Google Play Store.

If you decide that the benefit of a particular app that cannot be installed through the Google Play Store outweighs the risk, take additional steps to protect yourself, like planning to keep sensitive or personal information off that device. Even in this case, only install apps from trusted alternative app stores like [F-Droid](https://f-droid.org/) or [Aurora Store](https://gitlab.com/AuroraOSS/AuroraStore) or from the websites of the developers themselves. "Mirror" download sites may be untrustworthy unless you know and trust the people who provide those services.

To learn how to decide whether you should use a certain app, see [how Security in a Box chooses the tools and services we recommend](../../about/how/#how-we-choose-the-tools-and-services-we-recommend).

Some governments have demanded tech companies ban certain apps in their countries. When that happens, your contacts may encourage you to "root" your device in order to install banned apps by going to third-party app stores or websites. In fact, "rooting" your device is not necessary if you use [techniques to circumvent your country's censorship](../../internet-connection/anonymity-and-circumvention/) when setting up your phone.

We recommend not rooting your device, as it puts you at greater risk from infection with malicious code.
</details>

# Remove apps that you do not need and do not use

- Learn [how to delete apps](https://support.google.com/googleplay/answer/13627402).
- It may not be possible to uninstall the apps the manufacturer put on your phone, but [see these instructions on how to try to remove or disable them](https://www.howtogeek.com/115533/how-to-disable-or-uninstall-android-bloatware/).

<details><summary>Learn why we recommend this</summary>

New vulnerabilities in the code that runs in your devices and apps are found every day. The developers who write that code cannot predict where they will be found, because the code is so complex. Malicious attackers may exploit these vulnerabilities to get into your devices. Removing apps you do not use helps limit the number of apps that might be vulnerable. Apps you do not use may also transmit information about you that you may not want to share with others, like your location. If you cannot remove apps, you may at least try to disable them.
</details>

# If possible, avoid using social media apps

- Access social media and other sites by logging in through your browser instead.

<details><summary>Learn why we recommend this</summary>

Apps may share a lot of your data, like the ID of your phone, your phone number and which wifi you connect to. Some social media apps collect more information than they should. This includes apps like Facebook or Instagram. Use those services through the secure browser on your device (like [Firefox](../../tools/firefox/)) to protect your privacy a bit more.
</details>

# Use privacy-friendly apps

- You can browse the web with [Firefox](../../tools/firefox/).
- You can read your email with [K-9 Mail](../../communication/tools/#k-9-mail).
- You can use [F-Droid](https://f-droid.org/en/) or [Aurora Store](https://gitlab.com/AuroraOSS/AuroraStore) to find free and open-source apps that offer an alternative to proprietary apps installed in your device.

<details><summary>Learn why we recommend this</summary>

Android devices come with built-in apps that by default ask you to log in to your Google accounts, like for example Chrome or GMail. Instead, you can use more privacy-friendly apps to browse the web, read your email and much more. In many cases you can also choose to install these apps from alternative app stores focused on free and open-source software like [F-Droid](https://f-droid.org/) or [Aurora Store](https://gitlab.com/AuroraOSS/AuroraStore).

Once you have installed and set up these privacy-friendly apps, you can also [uninstall or disable](#remove-apps-that-you-do-not-need-and-do-not-use) the apps that were installed by default in your device and that you aren't planning to use.
</details>

# Check your app permissions

Review all permissions one by one to make sure they are enabled only for apps you use. The following permissions should be turned off in apps you do not use, and considered suspicious when used by apps you do not recognize:

- Location
- Contacts
- SMS
- Microphone
- Voice or speech recognition
- (Web) camera
- Screen recording
- Call logs or call history
- Phone
- Calendar
- Email
- Pictures
- Movies or videos, and their libraries
- Fingerprint reader
- Near field communications (NFC)
- Bluetooth
- Any setting mentioning "disk access," "files," "folders," or "system"
- Any setting mentioning "install"
- Facial recognition
- Physical activity
- Health Connect
- Allowed to download other apps

To learn how to change app permissions on Android, see [Google's support page](https://support.google.com/android/answer/9431959?hl=en), with details on how to change and remove app permissions, what each app permission implies and how to disable camera or microphone access on your device.

<details><summary>Learn why we recommend this</summary>

Apps that access sensitive digital details or services — like your location, microphone, camera or device settings — can also leak that information or be exploited by attackers. So if you do not need an app to use a particular service, turn that permission off.
</details>

# Turn off location and wipe history

- Get in the habit of [turning off location services overall](https://support.google.com/android/answer/3467281), or when you are not using them, for your whole device as well as [for individual apps](https://support.google.com/android/answer/6179507).
- Location settings may be in slightly different places on different Android devices, but are probably somewhere in Settings, Privacy and/or Security as well as in your Google account preferences.
- Regularly check and clear your location history if you keep it turned on. To delete past location history and set it so your devices and Google Maps do not save your location activity, follow the instructions [for your Google Maps Timeline](https://support.google.com/accounts/answer/3118687?#delete) and [your Maps activity](https://support.google.com/maps/answer/3137804).

<details><summary>Learn why we recommend this</summary>

Many of our devices keep track of where we are using GPS, cell phone towers and the wifi networks we connect to. If your device is keeping a record of your physical location, it makes it possible for someone to find you or use that record to prove that you have been in certain places or associated with specific people who were somewhere at the same time as you.
</details>

# Create separate user accounts on your devices

- Create more than one user account on your device, with one having "admin" (administrative) privileges and the others "standard" (non-admin) privileges.
    - Only you should have access to the admin user.
    - Standard users should not be allowed to access every app, file or setting on your device.
- Consider using a standard user for your day-to-day work.
    - Use the admin user only when you need to make changes that affect your device security, like installing software.
    - Using a standard user daily can limit how much your device is exposed to security threats from malware.
    - When you cross borders, having a standard user open could help hide your more sensitive files. Use your judgment: will the border authorities confiscate your device for a thorough search, or will they force you to log in to your account so they can take a quick look? If you expect they won't look too deeply into your device, being logged in as a standard user for work that is not sensitive provides you some plausible deniability.
- Learn how to add users in [Google's support page on adding and deleting users](https://support.google.com/android/answer/2865483#zippy=%2Cadd-user).

<details><summary>Learn why we recommend this</summary>

We strongly recommend not sharing devices you use for sensitive work with anyone else. However, if you must share your devices with co-workers or family, you can better protect your device and sensitive information by setting up separate users on your devices in order to keep your administrative permissions and sensitive files protected from other people.
</details>

# Remove unneeded users associated with your device

- Learn how to remove unwanted users in [Google's support page on how to delete users](https://support.google.com/android/answer/2865483#zippy=%2Cdelete-user).

<details><summary>Learn why we recommend this</summary>

If you don't intend for someone else to access your device, it is better to not leave that additional "door" open (this is called "reducing your attack surface"). Additionally, checking what users can access your device could reveal accounts that have been created on your device without your knowledge.
</details>

# Secure the Google accounts connected with your device

- Log in to each Google account associated with your device.
- [Check which devices use your account](https://support.google.com/accounts/answer/3067630).
- Secure the Google accounts connected with your device following the [steps for account protection in our guide on how to use Google](../../tools/google/#account-protection).
- Also see our guide on [social media accounts](../../communication/social-media) to check other accounts connected with your device.
- Take a picture or [screenshot](https://support.google.com/android/answer/9075928) of your account activity if you see anything suspicious, like devices you have disposed of, don't have control of or don't recognize.
- Also see the section on [suspicious access in the guide on Google](../../tools/google/#look-for-suspicious-access).
- Go to the [security checkup page](https://myaccount.google.com/security-checkup) and check whether there is any warning.
- Complete the following steps:
    - Check the last activity in [your account settings](https://myaccount.google.com/device-activity). To do this, you can also [click on "Details" in the bottom right corner of your GMail inbox](https://mail.google.com/mail/u/0/#inbox).
    - Check if there has been any [recent security-related activity](https://myaccount.google.com/notifications) on your Google account.
    - Check if your Google account [is connected to any third-party apps or services](https://myaccount.google.com/connections).
    - Check if you have enabled any [application passwords](https://myaccount.google.com/apppasswords).
    - Check that you have set a [recovery email](https://myaccount.google.com/recovery/email) and [phone number](https://myaccount.google.com/signinoptions/rescuephone) to recover access in case you are locked out of your account.
    - Check whether your mail is being [automatically forwarded to another address](https://mail.google.com/mail/#settings/fwdandpop).
- Consider enrolling in [Google's Advanced Protection program](https://landing.google.com/advancedprotection/).

<details><summary>Learn why we recommend this</summary>

Most devices have accounts associated with them, like Google accounts for your Android phone, your Chromebook laptop, and Google TV. More than one device may be logged in to your Google account (like your phone, your laptop and maybe your TV). If someone else has access to your account without your authorization, the checks included in this section will help you see and stop this.
</details>

# Set your screen to sleep and lock

- Set a [longer passphrase](../../passwords/passwords) (minimum 10 characters), not a short password or PIN.
    - To learn how to set or change your passphrase, see the [Google support page on how to set a screen lock on an Android device](https://support.google.com/android/answer/9079129).
    - Making it possible to use your fingerprint, face, eyes or voice to unlock your device can be used against you by force; do not use these options unless you have a disability which makes typing impossible.
        - Remove your fingerprints and face from your device if you have already entered them. Android devices differ, so this could be in a few locations on your device, but try following the instructions to [remove a fingerprint](https://support.google.com/googlepixeltablet/answer/13554937#zippy=%2Cremove-a-fingerprint) or [delete Face Unlock](https://support.google.com/pixelphone/answer/9517039?hl=en#zippy=%2Cdisable-or-delete-face-unlock). Alternatively, try looking where you would normally [find your device lock settings](https://support.google.com/android/answer/9079129).
  - Pattern locks can be guessed; do not use this option.
  - Simple "swipe to unlock" options are not secure locks; do not use this option.
  - Disable the "make password visible" option.
- Set your screen to lock a short time after you stop using it (try setting it to 1 minute or 5 minutes and see which works for you). The place to do this will be different on different devices, but look for "Screen timeout" under the "[Display](https://support.google.com/pixelphone/answer/6111557?hl=en)," "System" or "Security" settings.

<details><summary>Learn why we recommend this</summary>

While technical attacks can be particularly worrying, your device may as well be confiscated or stolen, which may allow someone to break into it. For this reason, it is smart to set a passphrase screen lock, so that nobody can access your device just by turning it on and guessing a short PIN or password.

We do not recommend screen lock options other than passphrases. If you are arrested, detained or searched, you might easily be forced to unlock your device with your face, voice, eyes or fingerprint. Someone who has your device in their possession may use software to guess short passwords or PINs. It is also possible to guess "pattern" locks by looking at finger tracks on the screen. Someone who has dusted for your fingerprints can make a fake version of your finger to unlock your device if you set a fingerprint lock and similar hacks have been demonstrated for face unlock.

For these reasons, the safest lock you can set is a longer passphrase.
</details>

# Control what can be seen when your device is locked

- [In the guide on how to control how notifications show on your phone's lock screen](https://support.google.com/android/answer/9079661), follow the instructions to [not show any notification](https://support.google.com/android/answer/9079661#zippy=%2Coption-dont-show-any-notifications).
    - If you really need to be notified about incoming messages, you can choose to [Hide sensitive content from notifications on your lock screen](https://support.google.com/android/answer/9079661#zippy=%2Coption-hide-sensitive-content-from-notifications-on-your-lock-screen), which will show only incoming messages without mentioning anything on the sender or the content.
- If you have enabled multiple users in your device, make sure that the option "Add users from lock screen" is off. To learn how to do this, see [Google's support page on changing guest and user settings](https://support.google.com/pixelphone/answer/2865944?hl=en#zippy=%2Clet-users-add-other-users-from-the-lock-screen). These instructions can vary, and you could also find them in Security > Lock Screen or in System > Advanced > Multiple users.

<details><summary>Learn why we recommend this</summary>

A strong screen lock will give you some protection if your device is stolen or seized — but if you don't turn off notifications that show up on your lock screen, whoever has your device can see information that might leak when your contacts send you messages or you get new email.
</details>

# Disable voice controls

- Disable Google Assistant and/or voice control. Voice and Assistant settings may be in slightly different places depending on your Android device, but are probably somewhere in Settings > Google.

    Review the instructions in [the Google Assistant support page](https://support.google.com/assistant/thread/226517535?hl=en&msgid=226543054).

- If you have decided the benefits to you outweigh the large risks of using voice control, follow the [Security Planner instructions to do so more safely](https://securityplanner.consumerreports.org/tool/secure-your-smart-speaker) and read [Keeping your information private and secure in the official Google Assistant guide](https://developers.google.com/assistant/how-assistant-works/privacy).
- Also consider [disabling altogether microphone access on your device](https://support.google.com/android/answer/13532937?hl=en) and only enabling it when you really need it.

<details><summary>Learn why we recommend this</summary>

If you set up a device so you can speak to it to control it, it becomes possible for someone else to install code on your device that could capture what your device is listening to.

It is also important to consider the risk of voice impersonation: someone could record your voice and use it to control your phone without your permission.

If you have a disability that makes it difficult for you to type or use other manual controls, you may find voice controls necessary. This section provides instructions on how to set them up more safely. However, if you do not use voice controls for this reason, it is much safer to turn them off.
</details>

# Use a physical privacy filter that prevents others from seeing your screen

- For more information on this topic, see [the Security Planner guide on privacy filters](https://securityplanner.consumerreports.org/tool/use-a-privacy-filter-screen).

<details><summary>Learn why we recommend this</summary>

While we often think of attacks on our digital security as highly technical, you might be surprised to learn that some human rights defenders have had their information stolen or their accounts compromised when someone looked over their shoulder at their screen or used a security camera to do so. A privacy filter makes this kind of attack, often called shoulder surfing, less likely to succeed. You should be able to find privacy filters in the same shops where you find other accessories for your devices.
</details>

# Use a camera cover

- First of all, figure out whether and where your device has cameras. Your smartphone might have more than one.
- You can create a low-tech camera cover: apply a small adhesive bandage on your camera and peel it off when you need to use the camera. A bandage works better than a sticker because the middle part has no adhesive, so your lens won't get sticky.
- Alternatively, search your preferred store for the model of your device and "webcam privacy cover thin slide" to find the most suitable sliding cover for your phone or tablet.
- Also consider [disabling altogether camera access on your device](https://support.google.com/android/answer/13532937?hl=en).

<details><summary>Learn why we recommend this</summary>

Malicious software may turn on the camera on your device in order to spy on you and the people around you, or to find out where you are, without you knowing it.
</details>

# Turn off connectivity you're not using

- Completely power off your devices at night.
- Get into the habit of keeping wifi, Bluetooth and/or network sharing off and only enable them when you need to use them.
- Airplane mode can be a quick way to turn off connectivity on your mobile. Learn how to selectively turn on wifi and Bluetooth once your device is in Airplane mode, to use only the services you want.
    - Turn Airplane mode on and make sure wifi and Bluetooth are off.
    - To learn how to selectively turn on wifi and Bluetooth while your phone is in Airplane mode, see [Google's support page on keeping your Android’s wireless connections on in Airplane mode](https://support.google.com/pixelphone/answer/12639358).
- Check the ["Change more Wi-Fi settings" instructions in Google's support page on managing advanced network settings on Android](https://support.google.com/pixelphone/answer/9655181?hl=en#zippy=%2Cchange-more-wi-fi-settings) and make sure "Turn on Wi-Fi automatically" and "Notify for public networks" are turned OFF.
- Turn off the hotspot when you are not using it.
    - Make sure your device is not providing an internet connection to someone else using the hotspot. To learn how to turn off the hotspot, see [the Android support page](https://support.google.com/android/answer/9059108) or [the Pixel support page](https://support.google.com/pixelphone/answer/2812516) on sharing mobile connection by hotspot.

<details><summary>Learn why we recommend this</summary>

All wireless communication channels (like wifi, NFC or Bluetooth) could be abused by attackers around us who may try to get to our devices and sensitive information by exploiting weak spots in these networks.

When you turn Bluetooth or wifi connectivity on, your device tries to look for any Bluetooth device or wifi network it remembers you have connected to before. Essentially, it "shouts" the names of every device or network on its list to see if they are available to connect to. Someone snooping nearby can use this "shout" to identify your device, because your list of devices or networks is usually unique. This fingerprint-like identification makes it easy for someone snooping close to you to target your device.

For these reasons, it is a good idea to turn off these connections when you are not using them, particularly wifi and Bluetooth. This limits the time an attacker might have to access your valuables without you noticing that something strange is happening on your device.
</details>

# Clear your saved wifi networks

- Save network names and passwords in your [password manager](../../passwords/tools/#keepassdx) instead of your device's list of networks.
- If you do save network names and passwords in your list of saved wifi networks, get in the habit of regularly erasing them when you aren't using them anymore. To learn how to do this, see [Google's documentation on how to remove saved networks](https://support.google.com/android/answer/9075847#zippy=%2Cremove-a-saved-network).
- [Make sure "Turn on Wi-Fi automatically" and "Notify for public networks" are turned off in your Network preferences](https://support.google.com/android/answer/9654714?#zippy=%2Cchange-more-wi-fi-settings).

<details><summary>Learn why we recommend this</summary>

When you turn wifi connectivity on, your device tries to look for any wifi network it remembers you have connected to before. Essentially, it "shouts" the names of every network on its list to see if they are available to connect to. Someone snooping nearby can use this "shout" to identify your device, because your list is usually unique: you have probably at least connected to your home network and your office network, not to mention networks at friends' houses, favorite cafes, etc. This fingerprint-like identification makes it easy for someone snooping in your area to target your device or identify where you have been.

To protect yourself from this identification, erase wifi networks your device has saved and tell your device not to look for networks all the time. This will make it harder to connect quickly, but saving that information in your password manager instead will keep it available to you when you need it.
</details>

# Turn off sharing you're not using

- Android devices differ, but look for a "connected devices," "device connections" or similar option in Settings and turn off or remove all devices there.
- [Turn off Quick Share](https://support.google.com/android/answer/9286773) (also called Nearby Share on some devices). Only enable it if you really need to share data with nearby devices that you trust.
    - If you really must share data with someone near you and they are not your Google contact, choose to share with "everyone" but check the "Use everyone mode temporarily" option so that you stop being visible to nearby devices after few minutes. Once you're done sharing, turn off Quick Share again.

<details><summary>Learn why we recommend this</summary>

Many devices give us the option to easily share files or services with others around us — a useful feature. However, if you leave this feature on when you are not using it, malicious people may exploit it to get at files on your device.
</details>

# Advanced: figure out whether someone has accessed your device without your permission (basic forensics)

Follow the steps on the following checklists:

- [Check devices linked to chat applications](https://github.com/securitywithoutborders/guide-to-quick-forensics/blob/master/smartphones/linkeddevices.md).
- [Review installed applications](https://github.com/securitywithoutborders/guide-to-quick-forensics/blob/master/android/applications.md).
- [Check if the phone is rooted](https://github.com/securitywithoutborders/guide-to-quick-forensics/blob/master/android/root.md).
- [Check for indicators of stalkerware installation](https://github.com/securitywithoutborders/guide-to-quick-forensics/blob/master/android/stalkerware.md).
- If you suspect your device may be compromised, follow the steps in the Digital First Aid Kit troubleshooter [My device is acting suspiciously](https://digitalfirstaid.org/topics/device-acting-suspiciously/#android-intro).


<details><summary>Learn why we recommend this</summary>

It may not always be obvious when someone has accessed your devices, files or communications. These additional checklists may give you more insight into whether your devices have been tampered with.
</details>

# Advanced: Use Android without a Google account

If you are concerned with Google tracking your every move, you can remove your Google account from your device by following the steps in the Android documentation on how to [Add or remove an account on Android](https://support.google.com/android/answer/7664951#remove_account). Better yet, the first time you configure your phone you can skip the "Sign in" screen. This way your device will not be tied to any Google account and information regarding location, searches, installed apps and so on will not be added to its profile.

With no Google account connected to your device, you will not be able to use the Google Play Store to install apps. Use alternative app stores like [F-Droid](https://f-droid.org/) and [Aurora Store](https://gitlab.com/AuroraOSS/AuroraStore) instead.

- The F-Droid store offers only free and open-source (FOSS) applications. To install it [download the F-Droid APK from the official website](https://f-droid.org/) and click the downloaded file to proceed with the installation. You may need to temporarily [allow installing unknown apps](https://www.androidauthority.com/how-to-install-apks-31494/#1). Make sure to revoke the installation permissions when the installation is completed!
- In the Aurora Store you will find the same apps that are in the Google Play Store. You can [install Aurora Store from F-Droid](https://f-droid.org/en/packages/com.aurora.store/).
- Regularly update installed apps by opening F-Droid and Aurora Store and manually verifying upgrades. Note that automatic updates may not work and with time you may be using outdated and insecure apps if you don't update them manually.

# Advanced: Change the operating system of your Android device

Android is made by Google so it is loaded with Google apps that track you and gather a lot of information about what you do and where you are. In some cases you can install a more secure and private alternative Android operating system such as security- and privacy-focused [GrapheneOS](https://grapheneos.org/) or [CalyxOS](https://calyxos.org/). This is an advanced solution: if you decide to do this, make sure your device is compatible. There are several steps you must take to install and if something goes wrong you could make your device unusable.

# See also

- [Security in a Box malware guide](../malware)
- [Google's security tips](https://safety.google/security/security-tips/)
