---
title: Protect your Mac computer
weight: 040
post_date: 25 June 2024
topic: phones-and-computers
---

If you use a Mac, you may have heard the myth that Mac computers are more secure. This is not necessarily true. Security depends on a combination of how we use our devices and their own software, which can be found to have vulnerabilities at any time. Follow the steps in this guide to make your device more secure. Get in the habit of checking these settings from time to time, to make sure nothing has changed.

#  Use the latest version of your device's operating system (OS)

- When updating software, do it from a trusted location like your home or office, not at an internet cafe or coffee shop.
- Updating to the latest version of your operating system may require you to download software and restart a number of times. You will want to set aside time for this where you do not need to do work on your device.
- After updating, check again if there are any further updates available until you do not see any additional new updates.
- If the latest version of macOS will not run on your computer, it is best to consider buying a new device.
    - [Find out which is the most updated version available](https://support.apple.com/HT201222).
		    - Alternatively, you can check whether your macOS version is still maintained on the [endoflife.date page for macOS](https://endoflife.date/macos).
    - [Compare it to the version your Mac has installed](https://support.apple.com/en-us/109033).
    - [Update your operating system](https://support.apple.com/en-us/108382).
    - [Set your Mac to update on its own](https://support.apple.com/guide/mac-help/keep-your-mac-up-to-date-mchlpx1065/mac#mchlpa64b4a7).
- To make sure an update is fully installed, always restart your device when prompted to do so after downloading the update.

**Notes**

- Find out [what to do if your computer doesn't have enough storage space to install](https://support.apple.com/en-us/102624).
- [Learn more about updating old versions of macOS](https://support.apple.com/en-us/102662).

<details><summary>Learn why we recommend this</summary>

New vulnerabilities in the code that runs in your devices and apps are found every day. The developers who write that code cannot predict where they will be found, because the code is so complex. Malicious attackers may exploit these vulnerabilities to get into your devices. But software developers do regularly release code that fixes those vulnerabilities. That is why it is very important to install updates and use the latest version of the operating system for each device you use. We recommend setting your device to automatically update so you have one less task to remember to do.
</details>

# Turn Lockdown Mode on

We strongly suggest to [turn Lockdown Mode on](https://support.apple.com/en-us/105120) in your Mac computer. Most likely you will not see any or much difference in the way your device works, but you will be much better protected.

<details><summary>Learn why we recommend this</summary>

[Lockdown Mode](https://support.apple.com/en-us/105120) limits infection strategies used by sophisticated spyware like [Pegasus](../../blog/pegasus-project-questions-and-answers/). Features of the Lockdown Mode should be used by everyone and should really be part of a standard operating system.
</details>

# Use apps from trusted sources

- [Install apps from the App Store](https://support.apple.com/en-us/111105).
- [Only allow apps downloaded from the App Store](https://support.apple.com/en-us/102445#changesettings) unless you know what you are doing.
- Advanced: learn how to [temporarily override your security settings to install an app you trust](https://support.apple.com/en-us/102445#openanyway).

<details><summary>Learn why we recommend this</summary>

[Apple's App Store](https://www.apple.com/app-store/) is the official app store for macOS. This centralized way of distributing apps makes it easier for people to find what they need, and it also makes it easier for Apple to monitor the offered apps for major security violations, as well as generally enforce their policies on listed apps. Only install apps from the App Store.

If you're really sure about what you're doing, you can install certain apps from the websites of the developers themselves. "Mirror" download sites may be untrustworthy, unless you know and trust the people who provide those services. If you decide that the benefit of a particular app outweighs the risk, take additional steps to protect yourself, like planning to keep sensitive or personal information off that device.

To learn how to decide whether you should use a certain app, see [how Security in a Box chooses the tools and services we recommend](../../about/how/#how-we-choose-the-tools-and-services-we-recommend).
</details>

# Remove apps that you do not need and do not use  

- [Learn how to uninstall apps on your Mac](https://support.apple.com/en-us/102610).
- *Note:* It is difficult and risky to uninstall many default apps that your Mac has pre-installed, like Safari or iTunes.

<details><summary>Learn why we recommend this</summary>

New vulnerabilities in the code that runs in your devices and apps are found every day. The developers who write that code cannot predict where they will be found, because the code is so complex. Malicious attackers may exploit these vulnerabilities to get into your devices. Removing apps you do not use helps limit the number of apps that might be vulnerable. Apps you do not use may also transmit information about you that you may not want to share with others, like your location.
</details>

# Use privacy-friendly apps

- You can browse the web with [Firefox](../../tools/firefox/).
- You can read your email with [Thunderbird](https://www.thunderbird.net).
- You can use [free and open source software for almost everything you need to do on a Mac computer](https://github.com/serhii-londar/open-source-mac-os-apps?tab=readme-ov-file). To decide whether you should use a certain app, see [how Security in a Box chooses the tools and services we recommend](../../about/how/#how-we-choose-the-tools-and-services-we-recommend).

<details><summary>Learn why we recommend this</summary>

Mac computers come with built-in software, like Safari or iTunes, that have a history of [privacy](https://www.theregister.com/2024/04/30/apple_safari_europe_tracking/) and [security issues](https://www.techradar.com/news/apple-itunes-has-a-serious-security-flaw-you-really-should-know-about). Instead, you can use more privacy-friendly apps to browse the web, read your email and much more.
</details>

# Check your app permissions

- Review all permissions one by one to make sure they are enabled only for apps you use. The following permissions are specifically suspicious as they are very regularly used by malicious applications: Location, Photos, Contacts, Calendar, Microphone, Camera, usage logs.
- Review the topics linked in the ["Control the personal information you share with apps" section of the macOS guide on how to guard your privacy on Mac](https://support.apple.com/en-gw/guide/mac-help/mh35847/mac#mchlpa3b92d9).

<details><summary>Learn why we recommend this</summary>

Apps that access sensitive digital details or services — like your location, microphone, camera or device settings — can also leak that information or be exploited by attackers. So if you do not need an app to use a particular service, turn that permission off.
</details>

# Turn off location and wipe history   

- Get in the habit of turning off location services overall, or when you are not using them, for your whole device as well as for individual apps.
- Regularly check and clear your location history if you have it turned on.
- Learn how to [turn off location services overall or just for specific apps](https://support.apple.com/en-gw/guide/maps/mpsa33caac1f/mac).
- Regularly check and clear your location history in Google Maps if you use it. To delete past location history and set it so Google Maps does not save your location activity, follow the instructions [for your Google Maps Timeline](https://support.google.com/accounts/answer/3118687?#delete) and [your Maps activity](https://support.google.com/maps/answer/3137804).

<details><summary>Learn why we recommend this</summary>

Many of our devices keep track of where we are, using GPS, cell phone towers and the wifi networks we connect to. If your device is keeping a record of your physical location, it makes it possible for someone to find you or use that record to prove that you have been in certain places or associated with specific people who were somewhere at the same time as you.
</details>

# Create separate user accounts on your devices

- Create more than one user account on your device, with one having "admin" (administrative) privileges and the others with "standard" (non-admin) privileges.
    - Only you should have access to the admin account.
    - Standard accounts should not be allowed to access every app, file or setting on your device.
- Consider using a standard user for your day-to-day work:
    - Use the admin user only when you need to make changes that affect your device security, like installing software.
    - Using a standard user daily can limit how much your device is exposed to security threats from malware.
    - When you cross borders, having a "travel" user account open could help hide your more sensitive files. Use your judgment: will the border authorities confiscate your device for a thorough search, or will they force you to log in to your account so they can take a quick look? If you expect they won't look too deeply into your device, being logged in as a standard user for work that is not sensitive provides you some plausible deniability.
- [Learn how to add a user on Mac](https://support.apple.com/en-gw/guide/mac-help/mchl3e281fc9/mac).

<details><summary>Learn why we recommend this</summary>

We strongly recommend not sharing devices you use for sensitive work with anyone else. However, if you must share your devices with co-workers or family, you can better protect your device and sensitive information by setting up separate users on your devices in order to keep your administrative permissions and sensitive files protected from other people.
</details>

# Remove unneeded accounts associated with your device

- [Learn how to delete unneeded accounts](https://support.apple.com/en-gw/guide/mac-help/mchlp1557/mac).

<details><summary>Learn why we recommend this</summary>

If you don't intend for someone else to access your device, it is better to not leave that additional "door" open on your machine (this is called "reducing your attack surface"). Additionally, checking what user accounts are associated with your device could reveal accounts that have been created on your device without your knowledge.
</details>

# Secure the accounts connected with your device

- Review [Apple's checklist to protect your Apple ID and password](https://support.apple.com/en-us/102614). In particular, make sure to [set a strong password](../../passwords/passwords/), to [enable 2-factor authentication](https://support.apple.com/en-us/102660) and not to share your Apple ID account with anyone.
- [Learn how to check your device list](https://support.apple.com/en-us/102649) to see which devices are connected to your Apple ID account.
- Also see our guide on [social media accounts](../../communication/social-media) to check other accounts connected with your device.
- You may want to take a picture or [screenshot](https://support.apple.com/en-us/102646) of results showing suspicious activity, like devices you have disposed of, don't have control of or don't recognize.
- Follow the [steps suggested by Apple if you think your Apple ID has been compromised](https://support.apple.com/en-us/102560).
- Consider turning on [Advanced Data Protection for iCloud](https://support.apple.com/en-us/108756) to enable end-to-end encryption and make sure only you can access your data through your trusted devices.
    - Note that if you turn on Advanced Data Protection, access to your iCloud data on the web at iCloud.com will be disabled and you will only be able to access your data on your trusted devices. If you turn on web access again, you can use one of your trusted devices to approve temporary access to your data on the web.

<details><summary>Learn why we recommend this</summary>

Most devices have accounts associated with them, like Apple ID accounts for your macOS laptop, Apple Watch, iPad and Apple TV. More than one device may be logged in at a time (like your phone, laptop and maybe your TV). If someone else has access to your accounts without your authorization, the steps included in this section will help you see and stop this.
</details>

# Protect your user account and computer with a strong passphrase

- Use a [long passphrase](../../passwords/passwords) (longer than 16 characters), not a short password.
- Disable [automatic login](https://support.apple.com/en-us/102316).
- [Learn how to change the login password on Mac](https://support.apple.com/en-vn/guide/mac-help/mchlp1550/14.0/mac/14.0).
- Make sure that [FileVault is turned on](https://support.apple.com/en-vn/guide/mac-help/mh11785/mac).
- Making it possible to use your fingerprint to unlock your device can be used against you by force; do not use TouchID to log in to your Mac computer unless you have a disability which makes typing impossible.
    - Remove your fingerprint from your device if you have already entered it.
    - Learn how to delete a fingerprint in [the guide on how to use Touch ID on Mac](https://support.apple.com/en-vn/guide/mac-help/mchl16fbf90a/mac#mchlbeff15b7).

<details><summary>Learn why we recommend this</summary>

While technical attacks can be particularly worrying, your device may as well be confiscated or stolen, which may allow someone to break into it. For this reason, it is smart to set a strong passphrase to protect your computer and user account, so that nobody can access your machine just by turning it on and guessing a short password.

We do not recommend login options other than passphrases. If you are arrested, detained or searched, you might easily be forced to unlock your device with your fingerprint. Someone who has your device in their possession may use software to guess short passwords. And if you set a fingerprint lock, someone who has dusted for your fingerprints can make a fake version of your finger to unlock your device.

For these reasons, the safest protection you can set for logging in to your device is a longer passphrase.
</details>


# Set your screen to sleep and lock  

- [Set your screen to lock a short time after you stop using it (5 minutes is good)](https://support.apple.com/en-in/guide/mac-help/mh11784/mac).
- [Learn how to require a password when the computer wakes from sleep or the screensaver activates](https://support.apple.com/en-in/guide/mac-help/mchlp2270/mac).


# Control what can be seen when your device is locked

Stop notifications from appearing when your device is locked. There are several ways to do this:

- Learn how to pause notifications when the device is sleeping or the screen is locked in the [guide on how to change notifications settings on Mac](https://support.apple.com/en-in/guide/mac-help/mh40583/mac).
- Another quick way of disabling notifications is [using Control Centre to turn on a Focus, for example Do Not Disturb](https://support.apple.com/en-in/guide/mac-help/mchl999b7c1a/14.0/mac/14.0).
- You can also [turn off notifications just for specific apps or websites](https://support.apple.com/en-in/guide/mac-help/mchl39cc046c/mac).

<details><summary>Learn why we recommend this</summary>

A strong screen lock will give you some protection if your device is stolen or seized — but if you don't turn off notifications that show up on your lock screen, whoever has your device can see information that might leak when your contacts send you messages or you get new email.
</details>

# Disable voice controls

- [Learn how to turn off Siri](https://support.apple.com/en-in/guide/mac-help/mchl6b029310/mac#mchl9bf44710).
- [Learn how to delete Siri requests and dictation history on Mac](https://support.apple.com/en-gb/guide/mac-help/mchlf55961c0/mac).
- If you have decided the benefits to you outweigh the large risks of using voice control, follow the [Security Planner instructions to do so more safely](https://securityplanner.consumerreports.org/tool/secure-your-smart-speaker).

<details><summary>Learn why we recommend this</summary>

If you set up a device so you can speak to it to control it, it becomes possible for someone else to install code on your device that could capture what your device is listening to.

It is also important to consider the risk of voice impersonation: someone could record your voice and use it to control your computer without your permission.

If you have a disability that makes it difficult for you to type or use other manual controls, you may find voice control necessary. This section provides instructions on how to set them up more safely. However, if you do not use voice controls for this reason, it is much safer to turn them off.
</details>

# Use a physical privacy filter that prevents others from seeing your screen

- For more information on this topic, see [the Security Planner guide on privacy filters](https://securityplanner.consumerreports.org/tool/use-a-privacy-filter-screen).

<details><summary>Learn why we recommend this</summary>

While we often think of attacks on our digital security as highly technical, you might be surprised to learn that some human rights defenders have had their information stolen or their accounts compromised when someone looked over their shoulder at their screen or used a security camera to do so. A privacy filter makes this kind of attack, often called shoulder surfing, less likely to succeed. You should be able to find privacy filters in the same shops where you find other accessories for your devices.
</details>

# Use a camera cover

- First of all, figure out whether and where your device has cameras. Your computer might have more than one if you use a plug-in camera as well as one built into your device.
- You can create a low-tech camera cover: apply a small adhesive bandage on your camera and peel it off when you need to use the camera. A bandage works better than a sticker because the middle part has no adhesive, so your lens won't get sticky.
- Alternatively, search your preferred store for the model of your computer and "webcam privacy cover thin slide" to find the most suitable sliding cover for your device.
- You can also consider [disabling your camera for specific apps](https://support.apple.com/en-ie/guide/mac-help/mchlf6d108da/mac).
- On Mac computers it is also possible to disable the built-in camera altogether: go to Settings > Screen Time > Content & Privacy, enable the Content & Privacy toggle, click App Restrictions and finally turn off the Allow Camera toggle and click Done. Learn more in the [Apple guide on changing App & Feature Restrictions settings in Screen Time on Mac](https://support.apple.com/en-ie/guide/mac-help/mchl3a19a9e7/14.0/mac/14.0).

<details><summary>Learn why we recommend this</summary>

Malicious software may turn on the camera on your device in order to spy on you and the people around you, or to find out where you are, without you knowing it.
</details>

# Turn off connectivity you're not using

- Completely power off your computer at night.
- Get into the habit of keeping wifi, Bluetooth and/or network sharing off and only enable them when you need to use them.
 - [Make sure Bluetooth is off](https://support.apple.com/guide/mac-help/turn-bluetooth-on-or-off-blth1008/mac).
 - [Make sure wifi is off](https://support.apple.com/guide/mac-help/use-the-wi-fi-status-menu-on-mac-mchlfad426fa/mac#mchl441b5083).
- [View this guide to understanding wifi menu icons on Mac](https://support.apple.com/guide/mac-help/wi-fi-menu-icons-on-mac-mchlcedc581e/mac).
- [Follow the instructions to get to the "Sharing" System Preferences window and ensure the "Internet Sharing" checkbox is un-checked](https://support.apple.com/guide/mac-help/share-internet-connection-mac-network-users-mchlp1540/14.0/mac/14.0).

<details><summary>Learn why we recommend this</summary>

All wireless communication channels (like wifi, NFC or Bluetooth) could be abused by attackers around us who may try to get to our devices and sensitive information by exploiting weak spots in these networks.

When you turn Bluetooth or wifi connectivity on, your device tries to look for any Bluetooth device or wifi network it remembers you have connected to before. Essentially, it "shouts" the names of every device or network on its list to see if they are available to connect to. Someone snooping nearby can use this "shout" to identify your device, because your list of devices or networks is usually unique. This fingerprint-like identification makes it easy for someone snooping close to you to target your device.

For these reasons, it is a good idea to turn off these connections when you are not using them, particularly wifi and Bluetooth. This limits the time an attacker might have to access your valuables without you noticing that something strange is happening on your device.
</details>

# Clear your saved wifi networks

- Save network names and passwords in your [password manager](../../passwords/tools/#keepassxc) instead of in your device's list of networks.
- If you do save network names and passwords in your list of saved wifi networks, make sure to:
    - not connect automatically to the networks you save. In the list of known networks in your [wifi settings](https://support.apple.com/guide/mac-help/change-wi-fi-settings-on-mac-mh11935/mac), move the pointer over the known network, click the options button, then uncheck Auto-Join.
    - get in the habit of regularly removing saved wifi networks when you aren't using them anymore. In the list of known networks in your [wifi settings](https://support.apple.com/guide/mac-help/change-wi-fi-settings-on-mac-mh11935/mac), move the pointer over the known network, click the options button, then choose Forget This Network.

<details><summary>Learn why we recommend this</summary>

When you turn wifi connectivity on, your computer tries to look for any wifi network it remembers you have connected to before. Essentially, it "shouts" the names of every network on its list to see if they are available to connect to. Someone snooping nearby can use this "shout" to identify your device, because your list is usually unique: you have probably at least connected to your home network and your office network, not to mention networks at friends' houses, favorite cafes, etc. This fingerprint-like identification makes it easy for someone snooping in your area to target your device or identify where you have been.

To protect yourself from this identification, erase wifi networks your device has saved and tell your device not to remember networks. This will make it harder to connect quickly, but saving that information in your password manager instead will keep it available to you when you need it.
</details>

# Turn off sharing you're not using

 - [Turn AirDrop off](https://support.apple.com/guide/mac-help/airdrop-mac-send-files-devices-mh35868/mac#mchl3aad4788).
    - If you decide to use AirDrop with your contacts, click the arrow next to AirDrop, then click Contacts Only. Make sure to remove from your list of Contacts anyone you don't want to share data with.
 - [Read the instructions on the Sharing settings on Mac](https://support.apple.com/guide/mac-help/change-sharing-settings-mchl26e04309/mac). Un-check services you are not using, and manage those you are (you might be using screen sharing or printer sharing for work, or media sharing to listen to music).

<details><summary>Learn why we recommend this</summary>

Many devices give us the option to easily share files or services with others around us — a useful feature. However, if you leave this feature on when you are not using it, malicious people may exploit it to get at files on your device.
</details>

# Use a firewall

- [Learn how to block connections to your Mac with a firewall](https://support.apple.com/guide/mac-help/block-connections-to-your-mac-with-a-firewall-mh34041/mac).
- We recommend enabling "stealth" mode to keep your computer more secure. See [macOS official documentation on the stealth mode](https://support.apple.com/en-jo/guide/mac-help/mh17133/mac).

<details><summary>Learn why we recommend this</summary>

Firewalls are a security option that stops unwanted connections to your device. Like a security guard posted at the door of a building to decide who can enter and who can leave, a firewall receives, inspects and makes decisions about communications going in and out of your device. We recommend turning yours on to prevent malicious code from trying to access your computer. The default firewall configuration should be enough protection for most people.
</details>

# Additional protection recommendations

- In System Preferences > Spotlight > Search Results, un-check at least Contacts, Events & Reminders, Mail & Messages and Siri Suggestions.
- In System Preferences > Spotlight > Privacy, you may want to add sensitive folders you do not want Spotlight to communicate to Apple about.
- [Read more recommendations to secure and improve privacy on macOS](https://github.com/drduh/macOS-Security-and-Privacy-Guide).

#  Advanced: figure out whether someone has accessed your device without your permission (basic forensics)

- Review [running processes](https://github.com/securitywithoutborders/guide-to-quick-forensics/blob/master/mac/processes.md), [programs launching at startup](https://github.com/securitywithoutborders/guide-to-quick-forensics/blob/master/mac/autoruns.md), [network connections](https://github.com/securitywithoutborders/guide-to-quick-forensics/blob/master/mac/network.md) and [kernel extensions](https://github.com/securitywithoutborders/guide-to-quick-forensics/blob/master/mac/kernel.md).
- [Consider installing OverSight, BlockBlock and KnockKnock by Objective-See.com](https://objective-see.com/products.html).
    - Their tools LuLu, DoNotDisturb, RansomWhere? and ReiKey might also be useful.
- If you're using a Mac with the Apple T2 Security Chip, check the [Startup Security Utility and make sure Secure Boot is set to Full Security](https://support.apple.com/HT208198).
- If you're using a Mac with Apple silicon, [learn how to change security settings to make sure the Security Policy is set to Full Security](https://support.apple.com/guide/mac-help/change-security-settings-startup-disk-a-mac-mchl768f7291/mac).
- If you suspect your device may be compromised, follow the steps in the Digital First Aid Kit troubleshooter [My device is acting suspiciously](https://digitalfirstaid.org/topics/device-acting-suspiciously/#mac-intro).

<details><summary>Learn why we recommend this</summary>

It may not always be obvious when someone has accessed your devices, files or communications. These additional recommendations and checklists may give you more insight into whether your devices have been tampered with.
</details>
