---
weight: 999
post_date: 26 June 2024
topic: phones-and-computers
title: Related Tools
subtitle: Phones & Computers
---

Use the following tools to protect your devices against malware and physical threats. In this section, you can also learn more about [physical threats](../physical-security) and [malware](../malware) or about [Android](../android), [iOS](../ios), [Windows](../windows), [Mac](../mac) and [Linux](../linux) devices.

# Scan your devices to detect malicious software

## [AVG Antivirus](https://www.avg.com/en-us/homepage)

[![](../../../media/en/logos/AVG-logo.png)](https://www.avg.com/en-us/homepage)
(Android, iOS, macOS, Windows)

An application that protects your phone or computer from malicious software and other threats.

Download ([Android](https://www.avg.com/antivirus-for-android), [iOS](https://www.avg.com/mobile-security-for-iphone-ipad), [macOS](https://www.avg.com/avg-antivirus-for-mac), [Windows](https://www.avg.com/free-antivirus-download)) | See [their support page](https://support.avg.com/?l=en)

## [Avira Antivirus](https://www.avira.com/)

[![](../../../media/en/logos/avira-logo.png)](https://www.avira.com/)
(Android, iOS, macOS, Windows)

An application that scans your phone or computer to detect if malicious software has been installed.

[Download](https://www.avira.com/en/free-antivirus) | See [their support page](https://support.avira.com/)

## [Bitdefender free antivirus](https://www.bitdefender.com)

[![](../../../media/en/logos/bitdefender-logo.png)](https://www.bitdefender.com)
(Android, macOS, Windows)

An application that scans your phone or computer to detect if malicious software has been installed.

Download ([Android](https://www.bitdefender.com/solutions/antivirus-free-for-android.html), [Windows](https://www.bitdefender.com/solutions/free.html), [macOS](https://www.bitdefender.com/solutions/virus-scanner-for-mac.html) | See [their support page](https://www.bitdefender.com/consumer/support/)

## [ClamAV](https://www.clamav.net/)

[![](../../../media/en/logos/ClamAV_logo.png)](https://www.clamav.net/) (Linux, Windows, macOS)

An open-source tool that scans your computer to detect if malicious software has been installed.

[Download](https://www.clamav.net/downloads) | See [their documentation](https://docs.clamav.net/)

## [Malwarebytes](https://www.malwarebytes.com)

[![](../../../media/en/logos/malwarebytes-logo.png)](https://www.malwarebytes.com)
(Android, iOS, macOS, Windows)

An application that scans your phone or computer to detect if malicious software has been installed.

[Download](https://www.malwarebytes.com/mwb-download) | See [their channel on YouTube](https://www.youtube.com/@malwarebytes/videos)

## [Tools from Objective-See.com](https://objective-see.com/)

[![](../../../media/en/logos/objective-see-logo.png)](https://objective-see.com/)
(macOS)

Apps that configure, protect and scan your Mac computer to help detect and disable malicious software. We recommend [LuLu](https://objective-see.com/products/lulu.html), [Do Not Disturb](https://objective-see.com/products/dnd.html), [OverSight](https://objective-see.com/products/oversight.html), [KnockKnock](https://objective-see.com/products/knockknock.html), [BlockBlock](https://objective-see.com/products/blockblock.html), [Task Explorer](https://objective-see.com/products/taskexplorer.html), [Netiquette](https://objective-see.com/products/netiquette.html), [Lockdown](https://objective-see.com/products/lockdown.html) and [RansomWhere?](https://objective-see.com/products/ransomwhere.html).

[Download](https://objective-see.com/products.html) | Their guides are in the links above

# Protect your devices from infection

## [CIRCLean](https://circl.lu/projects/CIRCLean/)

[![](../../../media/en/logos/circlean_logo.png)](https://circl.lu/projects/CIRCLean/)
(RaspberryPi - installation through Linux, macOS, Windows)

A hardware solution to clean documents from untrusted USB sticks. CIRCLean runs on a RaspberryPI and automatically converts untrusted documents into a readable but disarmed format, storing these clean files on a USB stick you trust.

See [their guide](https://circl.lu/projects/CIRCLean/)

## [DangerZone](https://dangerzone.rocks/)

[![](../../../media/en/logos/dangerzone-logo.png)](https://dangerzone.rocks/)
(Linux, macOS, Windows)

A free and open-source app that converts potentially infectious documents and pictures into safe form.

[Download](https://dangerzone.rocks/#downloads) | See [their guide](https://github.com/freedomofpress/dangerzone)

## [Hardentools](https://github.com/securitywithoutborders/hardentools#readme)

[![](../../../media/en/logos/hardentools-logo.png)](https://github.com/securitywithoutborders/hardentools#readme)
(Windows)

An application that can help protect your Windows computer by disabling certain risky features and services that are typically exposed.

[Download](https://github.com/securitywithoutborders/hardentools/releases/latest) | See [their howto](https://github.com/hardentools/hardentools?tab=readme-ov-file#how-to-use-it)

## [Qubes OS](https://www.qubes-os.org)

[![](../../../media/en/logos/qubes-logo.png)](https://www.qubes-os.org)

A secure operating system that provides an alternative to Windows, macOS or Linux. On Qubes OS you can separate your activities using different "compartments" (virtual machines known as _qubes_) to do different things, which helps protect your machine against malicious software.

[Download](https://www.qubes-os.org/downloads/) | See [their documentation](https://www.qubes-os.org/doc/)

## [URLCheck](https://triangularapps.blogspot.com/search/label/UrlChecker)

[![](../../../media/en/logos/URLCheck_logo.png)](https://triangularapps.blogspot.com/search/label/UrlChecker)
(Android)

A free and open-source app to analyze URLs before opening them.

Download on the [Google Play Store](https://play.google.com/store/apps/details?id=com.trianguloy.urlchecker&hl=en_US) or on [F-Droid](https://f-droid.org/en/packages/com.trianguloy.urlchecker/) | See [their documentation](https://github.com/TrianguloY/UrlChecker)

# Firefox add-ons that help protect your device against malicious software

## [NoScript](https://noscript.net/)

[![](../../../media/en/logos/NoScript-logo.png)](https://noscript.net/)
(Firefox, Chrome/Chromium)

An add-on that stops malware on websites from infecting your device. (Until it is properly configured, NoScript makes some websites appear broken.)

[Download](https://noscript.net/getit) | See [their guide](https://noscript.net/usage/)

## [uBlock Origin](https://ublockorigin.com/)

[![](../../../media/en/logos/ublock-logo.png)](https://ublockorigin.com/)
(Firefox, Chrome/Chromium, Edge, Opera, Thunderbird)

An ad blocker that can help protect you from spyware and malware when you browse the web.

[Download](https://ublockorigin.com/) | See [their documentation](https://github.com/gorhill/uBlock/wiki)

# Protect against physical threats

## [Haven](https://guardianproject.github.io/haven/)

[![](../../../media/en/logos/haven-logo.png)](https://guardianproject.github.io/haven/)
(Android)

A free and open-source app that turns your Android device into a surveillance camera

Download from [Google Play](https://play.google.com/store/apps/details?id=org.havenapp.main) | See [their documentation](https://guardianproject.github.io/haven/)
