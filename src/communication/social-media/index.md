---
title: Protect yourself and your data when using social media
weight: 045
post_date: 11 March 2025
topic: communication
---

We use social media platforms to connect with others and to spread messages we find important. At the same time, the companies managing these platforms often have a business model based on using our communications and contacts to make money. They share with third parties information about us, like our buying habits, social connections, location, birthday and other identifying information, and often we don't even realize this. When you use their services, you agree to their terms, which say it is ok for them to share or sell this information, among other things. Sometimes it is not clear how public that information will become.

Furthermore, social media platforms are actively monitored by governments and law enforcement agencies. The authorities collect publicly available data and, when possible, gather "metadata" about that data, which we may not think about them gathering (like when a photo was taken, or who is connected to whom). They may also ask social media companies to turn over private information about individuals of interest, often including human rights defenders or activists.

While we only provide instructions for certain social media platforms in this guide, many companies running social networking platforms will behave in similar ways and offer options like the ones listed here. We recommend looking around your account settings and taking the same steps to protect yourself on each of your social media accounts.

If you use any of the services below, click the links to view checklists that can help you secure your account.

# [→ Facebook](../../tools/facebook)

# [→ Instagram](../../tools/instagram)

# [→ X (Twitter)](../../tools/x-twitter/)

# [→ YouTube](../../tools/youtube)

<!-- # [→ TikTok](../../tools/tiktok) -->

# Alternatives

There are alternatives to commercial social networking platforms that can help you organize and spread messages without leaving your personal information as exposed as it is on mainstream platforms:

- For organizing with a carefully chosen group of people, moving your group to [secure chat apps](../tools/#more-secure-text-voice-and-video-chat-applications) like [Signal](https://signal.org/) or [Delta Chat](https://delta.chat/) can be one of the easiest options.
- For a larger private group, look into setting up a [Mattermost](https://mattermost.com) or a [Rocket.chat](https://www.rocket.chat/) instance.
- A common alternative to social media platforms is Telegram, which is used both to organize, through the creation of small private groups, and to spread messages to a wider audience through public channels. WhatsApp is also used in a similar way, but in both cases communications cannot be considered secure, as the companies managing these tools have a commercial agenda similar to social media platforms. If you decide to use these chat apps to spread news on your activities, we strongly recommend reading our guides on how to use [Telegram](../../tools/telegram) or [WhatsApp](../../tools/whatsapp) more securely.
    - The shortcoming of chat apps is that they can't support general public outreach the way that mainstream social networking platforms can. However, they provide better support for private groups, as long as they are encrypted, and that may be what you need. Make the decision based on what is best for you and the groups you collaborate with. If you decide to use a chat app to organize, read [our guide on how to chat securely](../secure-chat).
- For spreading news on your activities as you would on X, you might want to explore setting up an account on an alternative platform like [Mastodon](https://joinmastodon.org/about) or [Bluesky](https://bsky.app/). Read about all the possible alternatives in [our blog post on leaving X](../../blog/x-odus).
    - If you would like to start a Mastodon account, you can start from [Fedi.Tips](https://fedi.tips/), an unofficial non-technical guide to using Mastodon and the Fediverse.
    - If you would like to set up your own Mastodon instance, read the guides on [how to run a Mastodon server](https://docs.joinmastodon.org/user/run-your-own/) and on how to [run your own social networking platform](https://runyourown.social/).
- If you need a public platform to spread news, images, videos and longer contributions, you might consider creating a blog on a platform like [Noblogs](https://noblogs.org) (run by the autonomous tech collective [Autistici/Inventati](https://autistici.org)), [Blackblogs](https://blackblogs.org/) or [Shelter](https://www.shelter.is/services.html), or even run your own website.
- Alternatively, you could organize through a mailing list, which you can create for example on [Riseup](https://riseup.net/en/lists) or [Autistici/Inventati](https://www.autistici.org/services/lists). And if you want to reach a wide audience to spread your messages, you could consider setting up a newsletter, for example on [Autistici/Inventati](https://www.autistici.org/services/lists)'s servers.

For information on more privacy-friendly alternatives to mainstream social networking platforms, see our [list of communications tools](../tools/).
