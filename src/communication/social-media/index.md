---
title: Protect yourself and your data when using social media
weight: 045
post_date: 31 July 2024
topic: communication
---

We use social media to connect with others and to spread messages we find important. At the same time, social media use our communication and contacts to make money. They share information about us that we may or may not be aware they are sharing, like our buying habits, social connections, location, birthday and other identifying information. When you use their services, you are agreeing to their terms, which say it is ok for them to share or sell this information, among other things. Sometimes it is not clear how public they will make that information.

Social media are actively monitored by governments and law enforcement. The authorities collect publicly available data and gather "metadata" about that data, which we may not think about them gathering (like when a photo was taken, or who is connected to whom). They may also ask social media companies to turn over private information about individuals of interest, often including human rights defenders or activists.

While we only provide instructions for certain social media platforms in this guide, many social networking companies will behave in similar ways and offer options like the ones listed here. We recommend looking around your account settings and taking the same steps to protect yourself on each of your social media accounts.

If you use any of the services below, click the links to view checklists that can help you secure your account.

# [→ Facebook](../../tools/facebook)

# [→ Instagram](../../tools/instagram)

# [→ TikTok](../../tools/tiktok)

# [→ X (Twitter)](../../tools/twitter)

# [→ YouTube](../../tools/youtube)


# Alternatives

There are alternatives to social media that can help you organize and spread messages without leaving your personal information as exposed as it is on mainstream platforms:

- For organizing with a carefully-chosen group of people, moving your group to encrypted chat like [Signal](https://signal.org/) or [Delta Chat](https://delta.chat/) can be one of the easiest options.
- For a larger private group, look into setting up a [Mattermost](https://mattermost.com) or a [Rocket.chat](https://www.rocket.chat/) instance.
- A common alternative to social media platforms is Telegram, which is used both to organize, through the creation of small groups, and to spread messages to a wider audience through public channels. WhatsApp is also used in a similar way, but in both cases communications are not encrypted, and the companies managing these tools have a commercial agenda similar to social media platforms. If you decide to use these chat apps to spread news on your activities, we strongly recommend reading our guides on how to use [Telegram](../../tools/telgram) or [WhatsApp](../../tools/whatsapp) more securely.

The shortcoming of chat apps is that they can't support general public outreach the way that mainstream social media can. They provide better support for private groups, as long as they are encrypted. And that may be what you need. Make the decision based on what is best for your movement. If you decide to use a chat app to organize, read [our guide on how to chat securely](../secure-chat).

For spreading news on your activities as you would on Facebook or Twitter, you might want to explore setting up on [Mastodon](https://joinmastodon.org/) account. Its shortcoming is that it is less well-known, so not as many people are already there ready to spread information (what we call "network effects"). But for a movement that you want to expand, it might be the right choice.

- If you would like to start a Mastodon account, you can start from [Fedi.Tips](https://fedi.tips/), an unofficial non-technical guide to using Mastodon and the Fediverse.
- If you would like to set up a Mastodon instance, read the guides on [how to run a Mastodon server](https://docs.joinmastodon.org/user/run-your-own/) and on how to [run your own social networking platform](https://runyourown.social/).

If you need a public platform to spread news, images, videos and longer contributions, you might consider creating a blog on a platform like [Noblogs](https://noblogs.org) (run by the autonomous tech collective [Autistici/Inventati](https://autistici.org)) or [Shelter](https://www.shelter.is/services.html), or even run your own website.

Alternatively, you could organize through a mailing list, which you can create for example on [Riseup](https://riseup.net/en/lists), [Autistici/Inventati](https://www.autistici.org/services/lists) or another [autonomous server](https://riseup.net/en/security/resources/radical-servers). And if you want to reach a wide audience to spread your messages, you could consider setting up a newsletter, for example on [Autistici/Inventati](https://www.autistici.org/services/lists)'s servers.

For more information on how to use more securely social networking platforms and more privacy-friendly alternatives, see our [list of communications tools](../tools/).
