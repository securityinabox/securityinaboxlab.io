---
title: Recover from possible account compromise
weight: 036
post_date: 09 December 2021
topic: communication
---

While it is rarely possible to know for sure if your account has been compromised, below are clues that someone might have tampered with your account.

# Look for signs that your account may have been tampered with

- Changes to content, or to your account settings, that you did not make
- A contact received a message that you did not send
    - Be sure the message was sent from your account, and that its headers were not "spoofed"
- You cannot login to your account even though the user name and password you entered is correct
- You don't receive messages that contacts are certain they sent
    - Make sure they were not put in your spam folder
- A third party has information exchanged exclusively through this account, even though neither the sender nor the receiver(s) shared it with anyone else
- You receive notification of password change requests that you did not make, whether they were successful or not
- You lost a mobile device, or had one stolen or confiscated, while it was signed into the account
- If your service provider offers a log of recent account activity, you notice connections at a time, or from a location, that you could not have made yourself
- You received a message from the service provider of someone trying to log in to your account

If you notice one or more of these indicators — and other explanations seem unlikely — consider taking some or all of the following steps:

# Stop using this account to exchange sensitive information

- Wait to use it again until you have a better understanding of what is going on.

# Change your password immediately

- (If you can.) See our guide on how to [create and maintain strong passwords](../../passwords/passwords).

# Create new, unique passwords for other accounts

- This is crucial if your other accounts have the same or similar passwords.

# If you are not able to log in

- You may be able to contact the service provider to reclaim your account. Not all social media and email providers have procedures in place for this situation, but some do.
- Consider getting expert help.
    - Technologists or security researchers in your community may be able to provide assistance.
    - [Access Now Digital Security Helpline](https://www.accessnow.org/help/) offers emergency technical assistance and advice for activists, journalists, human rights defenders and other members of civil society.

# Review the security of devices connected to this account

- Review the security of other devices that have accessed this account or on which you have stored the password to this account.
- See our guides on how to [protect your device from malware and phishing attacks](../../phones-and-computers/malware/) and how to [protect your information from physical threats](../../phones-and-computers/physical-security).

# Check services that you have "linked" to this account

- Many third party services allow you to log in with your Facebook, Google or Microsoft account.
- Make a list of linked accounts (search your inbox for "verify your email" or "account confirmation" to find a lot of these quickly).
- Try to "unlink" them if you can.
- Prioritize financial and other more sensitive communications accounts.

# Log out of all sessions

- Email and other communications providers often give you the ability to see all the places where your account is logged in. Use this to log out locations and connections you do not recognize.
- You may need to log back in on devices you want to use to access the account.
- See our basic security guides for your phones and computers for more information.

# Take steps to protect your accounts

- Follow the instructions in our [social media guides](../social-media/) on each service provider under "If your account has been lost, stolen, or hacked" or "Account protection."
