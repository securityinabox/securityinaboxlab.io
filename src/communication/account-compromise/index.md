---
title: Recover from possible account compromise
weight: 036
post_date: 18 March 2025
topic: communication
---

While it is rarely possible to know for sure if your account has been compromised, below are clues that someone might have tampered with your account.

# Look for signs that your account may have been tampered with

- You notice changes to content, or to your account settings, that you did not make.
- A contact received a message that you did not send.
    - Be sure the message was sent from your account.
    - If it's an email, [check whether the headers were "spoofed"](../secure-email/#advanced-beyond-the-body-learn-to-read-an-email-header).
- You cannot log in to your account even though the username and password you entered are correct.
    - To make sure you're entering your correct username and password, try writing them in a text editor and copying them from the editor to paste them into the login form.
- You don't receive messages that contacts are certain they sent.
    - Make sure these messages haven't ended up in your spam folder.
- A third party has information exchanged exclusively through this account, even though neither the sender nor the receiver(s) shared it with anyone else.
- You have received notifications of password change requests that you did not make, whether they were successful or not.
- You lost a mobile device, or had one stolen or confiscated, while it was signed into the account.
    - Remember that even if you had enabled [full-disk encryption (or FDE)](../../files/secure-file-storage/), a device needs to be turned off for FDE to work.
- If your service provider offers a log of recent account activity and you notice connections at a time, or from a location, that you could not have made yourself.
    - Remember that if you use a VPN or Tor, your device might appear to have signed in from a place different from your actual location.
- You received a message from a service provider on attempts at logging in to your account.

If you notice one or more of these indicators — and other explanations seem unlikely — consider taking some or all of the following steps:

## Stop using this account to exchange sensitive information

- Wait to use it again until you have a better understanding of what is going on.

## Change your password immediately

- See our guide on how to [create and maintain strong passwords](../../passwords/passwords).

## Create new, unique passwords for other accounts

- This is crucial if your other accounts have the same or similar passwords.

## Review the security of devices connected to your account

- Review the security of other devices that have accessed this account, on which you have stored the password to this account or where you keep your 2-factor authentication method for this account.
- See our guides on [how to protect your phones and computers from several kinds of attacks](../../phones-and-computers).

## Check services that you have "linked" to this account

- Many third-party services allow you to log in with your Facebook, Google, Apple or Microsoft account.
- Make a list of linked accounts (search your inbox for "verify your email" or "account confirmation" to find a lot of these quickly).
- Try to "unlink" them if you can.
- Prioritize financial and other more sensitive communications accounts.

## Log out of all sessions

- Email and other communications providers often give you the ability to see all the places where your account is logged in. Use this to log out of locations and connections you do not recognize.
- You may need to log back in on devices you want to use to access your account.
- See our [basic security guides for your phones and computers](../../phones-and-computers) for more information on how to do this in each operating system.

## Take steps to protect your accounts

- Follow the instructions in our [social media guides](../social-media/) on each service provider under "If you think your account has been hacked" or "Protect your account."

# If you are not able to log in

- Try the standard recovery password procedure.
    - Not all social media and email providers have procedures in place for this situation, but most do.
- If you cannot recover access to your accont, try following the steps in [the Digital First Aid Kit troubleshooter I lost access to my account](https://digitalfirstaid.org/topics/account-access-issues).
- Consider getting expert help.
    - Technologists or security researchers in your community may be able to provide assistance.
    - [Access Now Digital Security Helpline](https://www.accessnow.org/help/) and [Front Line Defenders](https://www.frontlinedefenders.org/emergency-contact) offer emergency technical assistance and advice for activists, journalists, human rights defenders and other civil society members.
