---
title: Create and protect multiple online identities
weight: 025
post_date: 9 September 2024
topic: communication
---

This guide helps you reflect on the reasons why you may want to create alternative online identities and suggests methods to secure these identities in order to make it as difficult as possible to connect them to your private life or to your other online personas.

# Develop your strategy

Strategies for maintaining separate identities can range from full transparency to full anonymity.

Depending on your risk, situation, needs and goals, you can choose to [use your official name transparently](#real-identity), [create an alternative persona with its own name and story](#persistent-pseudonymity) or [try and hide your identity completely](#anonymity).

In the following sections you can find a description of all these strategies to help you reflect on what works best for you.

## Real identity

Publishing content online under your official name can make sense if your identity is connected to a reputation which gives you credibility and trust.

If you decide to be transparent about who you are, this doesn't mean that you have to expose all your private life to the internet. You could, for example, use the name and surname in your documents to sign your contributions to media outlets and other websites while using private profiles with a [pseudonym](#persistent-pseudonymity) on social media and chat groups to communicate with your close friends and family.

What counts in these cases is that you always [apply basic practices to keep your identities separate](#apply-basic-strategies-to-keep-your-identities-separate).

## Persistent pseudonymity

If you use an alternative name consistently for a certain kind of activities over a longer period of time, you are opting for persistent pseudonymity. This strategy can help mitigate threats like political or economic retribution for what you publish online, while allowing to develop an online reputation connected to your alternative name. This in turn makes it possible for others to decide whether you can be trusted or not − a crucial aspect in trust-based online communities.

It is also possible to maintain multiple pseudonyms (and reputations) for different purposes. For example, you could differentiate between your name as an activist and the nickname you use to publish photos you take as a hobby.

An alternative identity can also be managed collectively. While this option offers the advantages of group collaboration, you will need to develop good coordination practices together with strong security measures if you want to avoid exposing single individuals to risks determined by someone else's choices.  

In the following sections you can find instructions on how to create and protect a pseudonymous identity.

## Anonymity

Total online anonymity can be hard to achieve, but anonymization tools and strategies are helpful in high-risk situations that could compromise your freedom and wellbeing and even endanger your close contacts.

Unlike persistent pseudonymity, complete anonymity consists in using a short-lived identity to achieve a specific goal without leaving any traces. For example, a whistleblower who would like to send documents to a media outlet would use anonymization tools to communicate with journalists and then delete any account they created for this purpose.

If you are thinking of anonymizing online activities, consider that in some countries the authorities could interpret this strategy as a sign that you are doing something wrong. In such cases ask yourself if you could find other ways of reaching your goals, for example by not using the internet at all.

[Learn more about the tools and strategies you can use to anonymize your connections, messages and file sharing](../../internet-connection/anonymity).


## Comparing strategies

The pros and cons of the various identity options:

|                             | Riskier to you | Maintain reputation | Effort needed |
|-----------------------------|----------------|---------------------|---------------|
| **Real identity**           |	+              | ++                  | -             |
| **Persistent pseudonimity** |	-              | +                   | +             |
| **Anonymity**               | --             | -                   | ++            |


- **Real identity**
    - Risk: Using your official identity online means that you are easily identifiable by family members, colleagues and others, so your activities can be linked back to you.
    - Reputation: Others can easily identify you; gaining reputation and trust is easier.
    - Effort: This strategy requires little effort as you don't need to learn how to use anonymization tools.

- **Persistent pseudonymity**
    - Risk: A pseudonym could be linked to your official identity if you don't apply carefully strategies to protect your connections.
    - Reputation: A persistent pseudonym makes it possible to identify you across platforms and is a good way to gain reputation and trust.
    - Effort: Maintaining a pseudonym requires some effort, especially if you are also using your official name elsewhere.

- **Anonymity**
    - Risk: While it can be useful if you want to carry out activities that are sanctioned in your context, anonymity can also be hard to maintain. Choose this option carefully and get informed on [how to use anonymization tools correctly](../internet-connection/anonymity).
    - Reputation: Anonymity offers few opportunities to gain trust and reputation, or to network with others.
    - Effort: Attaining anonymity is hard, and such an effort requires caution to avoid leaking information that could refeal your identity.


# Check what information exists already on you online

Before you start creating alternative identities for your online activities, it's best to research the web to find out what the internet can reveal about you − an activity often called "self-doxing".

- Read [Access Now Helpline's guide on how to explore the web to find the traces you have left online](https://guides.accessnow.org/self-doxing.html).
- Learn more on "self-doxing" in the [New York Times Open Team's guide to finding and removing your personal information from the internet](https://open.nytimes.com/how-to-dox-yourself-on-the-internet-d2892b4c5954).

<details><summary>Learn why we recommend this</summary>

Doxing consists in tracing and gathering information about someone using all available sources in order to publish it. A malicious actor may do this to identify valuable information about their target, for example to find out private details on an online persona. "Self-doxing" lets you find out what information is available on you and, if necessary, delete it in order to prevent this data from being republished and spread.
</details>

# Get rid of unwanted information and old identities

If you find images, videos or other information that could help connect your identities with each other, you can contact the websites and platforms where they have been published and ask them to take them down.

- First of all, make a list of all the pictures, videos and other information you want to delete, following [the instructions in the Digital First Aid Kit guide on how to document online harassment](https://digitalfirstaid.org/documentation/).
- Then proceed to filing a takedown request:
    - Follow the instructions by the [Cyber Civil Rights Initiative's Safety Center](https://cybercivilrights.org/ccri-safety-center/#online-removal) to request image removal.
    - Read the [Digital First Aid Kit troubleshooter on doxing](https://digitalfirstaid.org/topics/doxing/).
- Request Google to [remove results with your personal contact information from search results](https://support.google.com/websearch/answer/12719076?hl=en).
- If you live in the European Union, you can also request Google to remove results with your name from search results.
    - Read [Google's Right to be forgotten Overview](https://support.google.com/legal/answer/10769224?hl=en).

<details><summary>Learn why we recommend this</summary>

Using an identity for an extended period of time − for work and for your private life as well as for other activities − can create a lot of information that could be used to profile or attack you. It can be useful to think of each of your identities, even those that carry your official name, as disposable, and get rid of information, pictures, videos and old accounts when you need to.
</details>

# Map your social domains

Another important step before creating multiple identities consists in mapping out your social domains to figure out where they tend to overlap and which intersections could lead to cross-domain attacks.

Think about your different activities and networks, both online and offline, and consider how sensitive each of them is. Once you have identified your most sensitive domains, reflect on how to keep them separate from the other networks in your life. Based on this reflection, you can proceed to develop the online identities that you need to create to separate your sensitive domains from the rest of your activities.

<details><summary>Learn why we recommend this</summary>

Each person belongs to several social domains − in their workplace and in their activist organization, in their family and in their friends circle, in their sports teams, in their neighborhood and in their videogame crew.

We tend to secure some of our networks and activities more than others. For example, we may tend to protect our communications more for our work or activism and less when interacting with friends on a social media platform. But any intersection between these groups can turn into a threat. For example, if an activist talks about their hobby in a social network, someone who wants to spy on them may try to install a virus in their device by sending them an infected PDF about their hobby and convince them to open it. This is only possible, however, if a person uses the same identity for everything they do online. This is one of the reasons why keeping social domains apart from each other can help secure our most sensitive activities.
</details>

# Create and secure an alternative identity

## Choose a name

Your new identity could have a name and a surname that the social media platform where you are registering an account will find credible as an official name. You can also choose a less plausible name, but consider that some platforms − most notably Facebook − require users to register under "the name they use in everyday life", so it can help to select a credible name when creating an online persona. In any case, if you decide to use a pseudonym on social networking platforms, you should always keep in mind the risk of having your account disabled and think of alternative ways of communicating if you lose access to your social media account.

Once you have decided on a name, a surname and a username for your new persona, it's best to do [thorough research to find out if someone else is already using that name](#check-what-information-exists-already-on-you-online).

## Get a different phone number

There are several ways of getting a different phone number, and depending on where you live you may even be able to register it without connecting it to your official identity.

- In some countries, you can buy a cheap pre-paid SIM card that is already registered without requiring you to show your documents.
- You can also get a phone number from online calling services like [Hushed](https://hushed.com/), [Google Voice](https://voice.google.com/about) or [Twilio](https://www.twilio.com/en-us/phone-numbers).

Remember that it is important to maintain control of your alternative phone number: if you stop paying for it, it might be reassigned to someone else and it might become harder to recover an account you had registered with that number if you lose access to it.

<details><summary>Learn why we recommend this</summary>

When you register a new email address or an account on social media, messaging apps, etc., you are often asked for a phone number, and in most cases your phone number is connected to your documents and official identity. So your phone number might be the weak spot that could make it possible to trace back your alternative identities to you. This is why one of the first steps for creating multiple identities should consist in finding ways of getting a different, permanent phone number for each of your online personas.
</details>

## Hide your IP address

You can choose to use a trusted VPN to hide your IP, but remember that the VPN provider will be able to see what you do online, so you need to trust them. If you want to be sure that nobody can link your official name to your alternative identities, it is best to anonymize your connections with Tor.

- [Learn how to choose a trusted VPN](../../internet-connection/circumvention).
- [Learn how to anonymize your connections with Tor](../../internet-connection/anonymity).

<details><summary>Learn why we recommend this</summary>

To make sure your ISP, email provider and online platforms cannot link your official identity to your alternative identities, it is best to hide your IP address both when you create accounts for your new identity and when you use and manage these accounts.
</details>

## Create a new email account

You can register a new mail account, create an alias starting from your current email account or, if you don't need a permanent address, you could use a disposable email service. Read more on all these options in [our guide on safe email](../secure-email).

<details><summary>Learn why we recommend this</summary>

An important step to creating an alternative online identity consists in creating a new email account. You will need to use this email to register new accounts on social networking platforms and other online services.

If you don't want the email provider to trace back your new account to your official identity, use [a tool to hide your IP address](#hide-your-ip-address) when creating your new email account − and remember always to use the same tool also to check your mail.
</details>

# Keep your identities separate

## Apply basic practices to keep your identities separate

Whatever approach you choose, you should always apply some basic practices that can help you reduce the risk of revealing too much about your identities when using social media, chats and other online platforms.

- When sharing personal details about your private life, use private profiles that can only be accessed by selected contacts.
    - When using private profiles on commercial social media, be aware of the regular changes to the privacy policies of that platform. This can have an impact on how "private" your profiles are. There have been cases where privacy settings have been changed, exposing pictures, content and the conversations of private profiles and groups.
- When writing or posting images about events, always consider if the information you share could be used to identify or attack someone. It is always a good idea to ask for permission to write about individuals. If you decide to post information, do it only after the event is finished.
- Use [strong and unique passwords](../../passwords/passwords) and [two-factor authentication](../../passwords/2fa) for every online account you create.
- [Learn how to reduce the risk of being tracked when browsing the web](../../internet-connection/safer-browsing).


## Remove identifying information from pictures and other files

When uploading images, videos or other files, remember to delete metadata and other information that might disclose your identity, location and more.

- [Learn how to get rid of data that could reveal personal details](../../files/destroy-identifying-information/).

## Use different online accounts, or even different platforms based on your communication goals

Each of your identities should have different accounts on social media and messaging apps, and you should take care to register these accounts [through a different IP](#hide-your-ip-address), [phone number](#get-a-different-phone-number) and [mail address](#create-a-new-email-address) than the ones connected to your main identity.

It could also be a good idea to not use the same social networking platforms and messaging apps for each of your identities: depending on the kind of activities you want to carry out and the character you want to give to your personas, you may decide to use different platforms for each of them − for example Facebook if you need to promote events or TikTok if you would like to address a younger audience.

## Keep your online accounts really separate

If you use different accounts on the same social networking platforms, follow the recommendations listed in this section to reduce the risk of someone linking your accounts to each other:

- When creating an account for a new persona on a social networking platform, use the [device](#different-devices), [browser](#different-browsers) or app you have set up for that persona.
- Make sure to check your account's privacy settings so that you know what you are making public, who can see your posts, who can contact you, who can look you up and what your contacts can do. [Learn  how to protect yourself and your data when using social media](../social-media).
- Be very careful about the profile information you provide, as well as the profile picture and cover photo you use, as these are generally publicly available, regardless of your privacy settings.
- Don't give too many details on your alternative identity's life. If, instead, you would like to talk about your persona, make sure to create a full-fledged character that has a story and psychological attitude different from your own. Learn more on [how to create a credible persona in the Gendersec guide on multiple identities](https://gendersec.tacticaltech.org/wiki/index.php/Step_1#Creating_a_credible_persona).
- Make sure your different identities don't follow each another and try to avoid having the same contacts follow your various identities. If someone is looking to unmask one of your personas, the first thing they will look for is who the account follows, and who follows the account. For the same reason, we should avoid reposting or liking content published by one account with another account.
- Most social networking platforms will display your location where they can. This is generally the case if we interact with the platform using a phone with active GPS, but it can also happen if we are connecting from other devices − for example the network our computer is connected to may also provide location data. It is always a good idea to double-check your settings − particularly on photo and video sharing sites.
- Photos and videos can also reveal a lot of information without you realising it. Many cameras will embed metadata into your pictures, which can include the date, time and location of the photo, camera type, etc. Photo and video sharing platforms may publish this information when we upload content to their sites. [Learn how to remove metadata from the images and videos you publish](../../files/destroy-identifying-information).
- It can be a good idea to publish from your various identities at different times of the day. Some social networking platforms allow users to schedule the publication time of their posts.
    - [Learn how to schedule your posts on Facebook](https://www.facebook.com/help/389849807718635).
    - [Learn how to create and manage scheduled posts and reels on Instagram](https://help.instagram.com/439971288310029).
    - [Learn how to schedule video publish time on Youtube](https://support.google.com/youtube/answer/1270709?hl=en).
    - [Learn how to schedule video publication on TikTok](https://www.tiktok.com/business/en-US/blog/introducing-video-scheduler-now-you-can-plan-tiktoks-in-advance).


<a name="different-devices"></a>
# Compartmentalize each identity using different devices, profiles or virtual environments

You can use several tools to keep your identities separate in your devices. In this section, you can find recommendations on how to do this in a computer and in a phone.

## Computers

### Use different devices

- You can get a different computer for each of your identities. Note that these devices have to be able to receive security updates.
    - Check if a device you're thinking of using for your alternative identity will receive security updates in our guides on [Windows](../../phones-and-computers/windows/#use-the-latest-version-of-your-devices-operating-system-os), [Mac](../../phones-and-computers/mac/#use-the-latest-version-of-your-devices-operating-system-os) and [Linux](../../phones-and-computers/linux/#use-the-latest-version-of-your-devices-operating-system-os).

### Start your computer from a USB disk

- If you need to anonymize all your connections for an alternative identity, a secure tool to do this is [Tails](../../internet-connection/tools/#tails), a free and open-source operating system that runs from a USB drive, connects to the internet over Tor and leaves no trace on your computer. You can use a different Tails stick for each of your most sensitive identities.
    - You can store all your data for your separate identity in the encrypted Tails USB stick by creating a persistent storage. Learn more about this option in the [Tails official documentation](https://tails.net/doc/persistent_storage/index.en.html).
- You can also create an [Ubuntu live USB with persistency](https://wiki.ubuntu.com/LiveUsbPendrivePersistent) and use it to manage a separate identities, but with this solution your USB stick won't be encrypted by default, so you should keep your USB stick in a safe place and make sure nobody can use it without your permission.

### Use virtual machines or sandboxing tools

- If your computer complies with the [system requirements](https://www.qubes-os.org/hcl/), you may consider installing [Qubes OS](../../phones-and-computers/tools/#qubes-os), a free and open-source, security-oriented operating system that lets you create isolated compartments (called qubes) for each of your identities and needs.
    - See [some examples of how different types of users organize their qubes](https://www.qubes-os.org/doc/how-to-organize-your-qubes/) to figure out how you could organize your qubes for each of your identities.
- Another secure way of managing a separate identity and anonymizing all its connections to the internet is [Whonix](https://www.whonix.org), an anonymous virtual machine that runs like an app and routes all internet traffic through Tor.
- You can also isolate your identities by using a virtual machine (or VM) for each of them. You can create VMs with tools like [VirtualBox](https://www.virtualbox.org/), [VMware](https://www.vmware.com/products/desktop-hypervisor/workstation-and-fusion) or [KVM](https://www.redhat.com/en/topics/virtualization/what-is-KVM), but consider that this solution requires a lot of resources from your computer and will not change your IP address, so it's best to not run more than one VM at a time and to run a VPN or Tor before you start a virtual machine.
- On Windows, a similar possibility that requires less resources but also does not change your IP address is [Sandboxie](https://sandboxie-plus.com), a [sandboxing tool](https://www.techtarget.com/searchmobilecomputing/definition/application-sandboxing) to compartmentalize your identities.

### Create different accounts in your devices

- If you don't need to protect your personas against sophisticated attacks or against someone inspecting your devices, you can create a different user account for each of your identities.
    - Learn how to create different user accounts in [Windows](../../phones-and-computers/windows/#create-separate-user-accounts-on-your-devices), [Mac](../../phones-and-computers/mac/#create-separate-user-accounts-on-your-devices) and [Linux](../../phones-and-computers/linux/#create-separate-user-accounts-on-your-devices).

### Use different browsers, browser profiles or extensions

- If you only need a browser to manage your alternative identities, you can use a different browser than the one you usually use for your main identity.
    - [Learn what browsers you can choose in our guide on safer browsing](../../internet-connection/safer-browsing).
    - Consider using [the Tor Browser](https://www.torproject.org/) to manage an alternative identity with a different IP address while using the same device.
- Besides using different browsers for each of your identities, you can consider using different profiles in the same browser.
    - [Learn how to create a new user profile on Firefox](https://support.mozilla.org/en-US/kb/profile-manager-create-remove-switch-firefox-profiles#w_create-remove-or-rename-a-profile).
    - [Learn how to create a new user profile on Chrome/Chromium](https://www.chromium.org/developers/creating-and-using-profiles/#to-create-a-profile).
- To make sure your identities are separated while using the same browser, consider installing an extension that lets you keep parts of your online life separated into different tabs, like [Firefox Multi-Account Containers](https://addons.mozilla.org/en-US/firefox/addon/multi-account-containers/) or [SessionBox for Chrome/Chromium](https://chromewebstore.google.com/detail/sessionbox-multi-login-to/megbklhjamjbcafknkgmokldgolkdfig?hl=en).

## Phones

### Use different devices

- Consider dedicating a separate phone to each of your identities. Note that these devices have to be able to receive security updates.
    - Check if a device you're thinking of using for your alternative identity will receive security updates in our guides on [Android](../../phones-and-computers/android/#use-the-latest-version-of-your-devices-operating-system-os) and [iOS](../../phones-and-computers/ios/#use-the-latest-version-of-your-devices-operating-system-os).

### Create different accounts on Android

- If you don't need to protect your personas against sophisticated attacks or against someone inspecting your devices, you can create a different user account in your tablet or phone for each of your identities.
    - Learn how to create different user accounts in [Android](../../phones-and-computers/android/#create-separate-user-accounts-on-your-devices).

### Sandbox your applications on Android

- In case you prefer to not switch every time from one account to the other, you can also consider using Shelter (only available [on F-Droid](https://f-droid.org/packages/net.typeblog.shelter/)), a [sandboxing tool](https://www.techtarget.com/searchmobilecomputing/definition/application-sandboxing) that lets you compartmentalize an alternative identity in the Android work profile. Note that this won't change your IP address and will not protect you against sophisticated attacks or against someone inspecting your devices.

### Use different browsers

- If you only need a browser to manage your alternative identities, you can use a different browser than the one you usually use for your main identity.
    - [Learn what browsers you can choose in our guide on safer browsing](../../internet-connection/safer-browsing).
		- Consider using [the Tor Browser](https://www.torproject.org/) on Android or [Onion Browser](https://onionbrowser.com/) on iOS to manage an alternative identity with a different IP address while using the same device.

<details><summary>Learn why we recommend this</summary>

Keeping your identities separate from each other by managing each of them in a different device, user profile or sandbox will help you avoid human errors like replying to a message from the wrong account, and will also make it harder to connect your different identities through [device or browser fingerprinting](https://en.wikipedia.org/wiki/Device_fingerprint).

Some of these tools, like Whonix, Tor Browser or Qubes OS, also make it possible to manage more than one identity with different IP addresses while using the same device.

In some cases you may just want to manage each identity in a different browser or app to make sure you don't use one identity instead of the other in your social media accounts, chats, etc.
</details>

# Use payment methods that won't reveal your official identity

If you need to make online purchases for your pseudonymous identity, you may want to use payment methods that are not connected to your official name and bank account.

Besides cryptocurrencies, which may be harder to manage, you can use cash to buy prepaid cards available at supermarkets or you can open an account with a bank that offers virtual credit cards that keep your identity hidden from the sellers.

# Reflect on how to manage your multiple identities in physical spaces

If your alternative identity's activities are not restricted to the online sphere, you will need to reflect on how to show your face in connection with your pseudonym.

Reflect on how to separate your everyday life from the physical activities of your alternative persona, for example when giving a talk or attending an event. You may choose to disguise yourself with different looks or with a mask, or to only show your face in spaces and with people you trust and where taking photos and videos is not allowed. However, also consider that the way to reach that space might be surveilled, for example by security cameras.

You can create a disposable identity that you can use to keep in touch with new acquaintances. This will give you some time to understand if you can trust these new contacts before you include them in a more trusted network.

# [Advanced] Create a website that cannot be connected to your identity

- Check whether the domain name and the registrar you are choosing allow for privacy protection, and try to find a domain name provider that offers this option for free.
- To be sure not even your domain name provider knows who you are, you can register your domain on [Njalla](https://njal.la/), an anonymous registrar which accepts encrypted requests and payments in cryptocurrencies.

<details><summary>Learn why we recommend this</summary>

If you need to create a website for one of your alternative identities, remember that anybody looking up your domain will be able to find its owner unless your domain name registrar offers privacy protection to its clients. This option can be included in the basic package you are buying with your domain name or could have an additional cost, but remember that [for some top level domains this option is not supported at all](https://en.wikipedia.org/wiki/Domain_privacy#Privacy_forbidden).
</details>
