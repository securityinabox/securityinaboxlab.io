---
title: Protect the privacy of your online communication
weight: 035
post_date: 15 September 2021
topic: communication
---

Each communication method, digital or otherwise, comes with advantages and disadvantages in terms of convenience, popularity, cost, and security, among other considerations. It's up to each of us to weigh the benefits and risks of the methods we use to communicate. When our risks are higher, it is wise to choose our communication tools more deliberately.


# Make a communication plan

Establish and rehearse a communication plan for yourself and your community, so you are able to maintain communication and take care of each other in moments of stress or crisis.

 - Talk with your contacts. Propose, discuss and agree upon a specific plan for how you are (and are not) going to communicate. Include:
     - known risks
     - apps and services you will and will not use
     - steps you will take if something goes wrong
 - Consider informal steps you can take, including:
     - creating and exchanging alternative backup email addresses
     - exchanging phone numbers
     - sharing emergency contacts
 - Consider more formal steps you can take:
     - Specify supported communication methods in your organisational security-, data-storage- or document-retention policy.
 - Consider what you say and where
     - The easiest way to keep others from learning sensitive information is to not send or say it.
     - For example, plan not to send text messages when someone is travelling through border crossings, or in detention.
     - Consider developing code language you can use to avoid openly mentioning names, addresses, specific actions, etc.
 - Rehearse your communication plan

<details><summary>Learn why we recommend this</summary>

In a crisis, we do not think as clearly, and we may have to act fast. We may make decisions that endanger us. Planning an emergency communications strategy in advance can help us stay safe.
</details>

# Create multiple disposable accounts

- Many popular email providers allow you to create email aliases for receiving emails to your main mailbox without revealing your main email address.
- Use a [Firefox Relay email address](https://relay.firefox.com/).
- If you know it does not matter if you lose access to a mailbox, but you need a viable email address to sign up for an online service, try [Guerrilla Mail](https://www.guerrillamail.com/about).

<details><summary>Learn why we recommend this</summary>

Encryption tools which scramble your messages so attackers can't read them prevent the content of your messages from being exposed. But it is harder to hide who is sending or receiving a message, the dates and times when messages are sent and received, how much information was sent, and information about the devices used. This kind of "information about information" is often called "metadata." It isn't hidden because computers, routers, servers, and other devices between you and the recipient need it to get your message to the right person. Metadata can reveal a lot about who you know and about your communication patterns, especially if someone can analyse a large volume of it.

Using a number of accounts you can abandon, without significant disruption to your communication, is one strategy for disguising who you are.
</details>

# Use Tor Browser, Tails, or a trusted VPN

- See our guide on [visiting blocked websites](../../internet-connection/anonymity-and-circumvention) for more on Tor, VPNs, and other tools that protect your online activity from people who want to see what you are doing.
- Learn more about [Tails](https://tails.boum.org/about/index.en.html).
- Check is it safe and legal for you to use those tools in your context.

<details><summary>Learn why we recommend this</summary>

Using the Tor Browser, a VPN, or Tails allows you to visit websites without disclosing to the website who you are or where you are from. If you log in to a website or service while doing so, you will still be sharing your account information (and potentially personal information) with the website.

Tails is an operating system that you use instead of your computer's usual operating system (Windows, Mac, or Linux). It protects all of your internet connections by using Tor at all times. It runs off a USB drive plugged into your machine. Tails erases your history when you shut it down, making it less likely that your computer can be "fingerprinted" by sites you have visited, wifi you have used, and apps you have installed.
</details>

# Use email more safely

## Encrypt your email messages

 - Encrypt email services you already use with
     - [Mailvelope](https://www.mailvelope.com/)
     - [Thunderbird](https://www.thunderbird.net/)
 - Try email services that support encryption
     - [Proton Mail](https://proton.me/)
         - NOTE: Proton Mail does not automatically encrypt mail to someone who does not use the same service. To encrypt to someone who does not use Proton Mail, you must [set a password the recipient can use to open the message](https://proton.me/support/password-protected-emails) or you may need to [exchange encryption public keys](https://proton.me/support/how-to-use-pgp).

<details><summary>Learn why we recommend this</summary>

Email encryption uses complex mathematics to scramble the content of your files or messages so they can only be read by someone who has the "key" to unscramble it. Technical experts trust encryption because at this point in history, no computer is powerful enough to do the math to unscramble it.

Using encryption with email is important. Your email passes through a number of other devices on its way to the person you are writing to. The wifi hub or hotspot you are connected to is among them. Messages also pass through wires and wireless connections (like wifi and Bluetooth).

Your email is also saved on devices called servers, and may be saved on more than one of your recipient's devices, like their mobile and their computer. An attacker could theoretically find your messages in any one of these places. Encryption stops attackers from being able to read what they find, making your messages look to attackers as if they are paragraphs of nonsense letters and numbers.

Email was not built with encryption included. It was developed in the 1970s, when it was used by a more limited number of people. Today, most email providers (like Gmail, Outlook, Yandex, etc.) protect your messages with encryption, using for example HTTPS, as they go from your device, through your local wifi router, and to your email providers' servers (this is sometimes called to-server encryption). However, your messages remain unencrypted when they are stored on your device and on your email provider's servers as well as on the recipient's device and email provider's servers. This means someone with access to the servers or devices can read your messages.

Because of this, we recommend email clients or services that are end-to-end encrypted (e2ee). For example those which use or enable PGP encryption (like Thunderbird and Mailvelope or Proton Mail). This means that your messages are protected with encryption when they are saved on your email provider's servers, as well as on your and recipients devices.

However, email encryption has a few drawbacks. Most email encryption does not hide who is sending mail to whom, or when. An attacker could use that information to demonstrate that people are talking to each other. Additionally, managing encryption requires extra effort and mindfulness.
</details>

## Avoid retaining sensitive information you no longer need

 - Set up disappearing messages on messaging applications where possible.
 - Delete messages when possible.


<details><summary>Learn why we recommend this</summary>

Consider which messages someone might find if they were able to get access to your account on your email server or device. What would those messages reveal about your work, your views, or your contacts? Weigh that risk against the benefit of having access to your email on multiple devices. Backing up your email to an encrypted device is one way to retain and save your emails safely.
</details>

# Video and audio chat more safely

## Use end-to-end encrypted video, voice and text services

If use end-to-end encryption is not possible, make sure to use recommended services, which are hosted on trusted servers by trusted providers.

## Control who is connected to video calls and chats

 - Know who you are sending a call or chat invite to.
     - Confirm the phone numbers or email addresses you are sending to.
     - Do not share invitations on public social media.
 - Before you begin a call, familiarize yourself with the administrator or moderator tools that let you mute, block, or eject unwanted participants. Set it up so only moderators have the ability to mute and block on video calls.
 - When a call or chat begins, do not assume you know who is connected only by reading assigned names. It is not difficult for someone to enter the name of someone you know as their user name, and pretend to be them.
     - Check the identities of everyone on the call by asking them to speak or to switch on their camera.
     - Confirm in another channel (like secure chat or email) that the person in a call is who their screen name says they are.
     - Take screenshots or record unwanted participants to collect evidence for later analysis and legal actions.
     - Eject anyone who you do not wish to have in the call or conversation.
 - If ejecting or blocking someone does not work, start an entirely new conversation. Double-check the list of phone numbers or email addresses you are sending to, to make sure they are correct for the people you want in the conversation. Contact each person through a different channel to confirm (for example, using a phone call if you think their email address is wrong).

<details><summary>Learn why we recommend this</summary>

Online services do not prevent people from signing in to calls or chats using someone else's name. To be sure everyone is who they say they are, verify their identity before you discuss sensitive details.

During the COVID pandemic, many people learned the hard way that people who wanted to harass them could find their way into their organizing video calls. When moderators have the ability to mute or remove participants from calls, it ensures video calls can proceed without interruption.
</details>

## Change video calls to mic and camera off by default

- Assume that when you connect, your camera and microphone may be turned on by default. Check this setting immediately, and be careful what you show and say until you know these are off.
- Consider covering your camera with a sticker or adhesive bandage, and only remove it when you use the camera.
- Consider disabling your microphone and camera in the device settings, if you cannot turn them off it in the service before connecting.

## Set rules about capturing and participating in video calls

 - Agree on ground rules before a meeting starts. These could address:
     - whether participants will use real names or a nick names
     - whether you will keep your cameras on or off
     - whether participants will keep their microphones on or off when not speaking
     - how participants will indicate that they would like to speak
     - who will lead the meeting
     - who will take notes, and where, whether, and how will those notes be written and distributed
     - whether it is acceptable to take screenshots of or record a video call, etc.

<details><summary>Learn why we recommend this</summary>

If your participants are concerned for their safety, a video record of their participation on the call could put them at risk of their location and their affiliation with your group being identified. Set ground rules in advance about cameras and microphones if this is a risk. Remember, ground rules alone will not stop someone from recording the call.

Rules about keeping microphones off when you are not speaking, and how you will take turns, can also ensure a smoother, less stressful meeting.
</details>

## Use a headset

<details><summary>Learn why we recommend this</summary>

Using headphones or earbuds ensures that someone near you who is listening to your conversation cannot overhear what others on the call are saying (though of course they would be able to hear you).
</details>


## Check what can be seen and heard

 - Be mindful of what is visible in the background of your video, both who and what is in the frame.
 - You may not want to give away too much information on your house, family pictures, notes on the walls or boards, the natural environment or the city outside your window, etc.
 - Choose a location with a blank wall behind you if possible, or clear away clutter.
 - Test what is in the background _before_ a call by, for example, opening your camera app or meet.jit.si and starting an empty meeting with your camera switched on so you can see what is in the picture.
 - Be mindful who and what can be heard in the background.
 - Close the door and windows, or alert those sharing your space about your meeting.

<details><summary>Learn why we recommend this</summary>

Human rights defenders have been found by attackers based on what can be seen and heard in the background when they are on calls, both audio and video. Keep track of the following to stay safe.
</details>

# Use secure chat

 - See our [recommendations for chat tools](../tools#more-secure-text-voice-and-video-chat-applications) in the Related Tools section.
 - Also see our recommendations about [Anonymity while browsing the Web](../../internet-connection/anonymity-and-circumvention/#anonymity-while-browsing-the-web).

<details><summary>Learn why we recommend this</summary>

Chat services may access and collect information on your location, activity, content, and whom you are speaking to. Picking the right one is important to protecting your safety.
</details>

## Delete chat history

- Find the option in an app to delete all conversations, or specific ones.
    - If you are deleting a chat app, delete messages first before you remove the app, for extra assurance the messages will be removed.
    - If there is no option to automatically make messages disappear, learn how to remove messages manually. Ask your contacts to do the same.
- You may also be able to set messages to "expire," or be deleted from your phone and the recipient's phone after a certain amount of time.
    - Consider how long you want the messages to be visible before they expire. You may have the option to make them expire in minutes, hours, or days.
    - Disappearing messages do not guarantee that your messages will never be found! It is possible that someone could take a screenshot of the messages or that someone could take a photo of the screen using another camera.

<details><summary>Learn why we recommend this</summary>

Chat apps keep a record of everything you and your contacts have said or shared, by default. Use the disappearing messages option to limit the amount of information an app stores on your phone.
</details>

## Take identifying information out of your photos and other files

- Use [Obscuracam](../../files/tools#obscuracam), [Scrambled Exif](../../files/tools#scrambled-exif), [MetaX](../../files/tools#metax), or [Exifcleaner](../../files/tools#exifcleaner).
- See [this article on removing information from photos](https://freedom.press/training/redacting-photos-on-the-go/) for additional techniques.

<details><summary>Learn why we recommend this</summary>

It may seem like simply using a face blur feature or covering sensitive details will protect the people or places in your images. However, there are some ways of saving edited photos which do not stop someone who has the file from seeing what you were trying to hide.

All files have a small amount of information saved about where and how they were created. This information is called metadata. You can usually get a look at some of a file's metadata on a computer by right-clicking on the file and selecting "Properties" or "Get info."

Some metadata may include your location or the device the file was made with: information that someone looking at the file could use to identify you. [See this article for more information on metadata](https://freedom.press/training/everything-you-wanted-know-about-media-metadata-were-afraid-ask/ ), and how you can clean it from your files.
</details>
