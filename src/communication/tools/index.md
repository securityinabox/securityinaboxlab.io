---
weight: 999
post_date: 9 October 2024
topic: communication
title: Related Tools
subtitle: Communication
---

Use the following tools to protect your privacy and security while sending and receiving email, chatting via text, voice or video, joining and organizing video conferences; sharing large files; or collaborating on sensitive documents. You can also learn more about how to [protect the privacy of your online communication](../private-communication), [secure your email communications](../secure-email), [recover from possible account compromise](../account-compromise) and [protect yourself and your data when using social media](../social-media/).

# More secure email providers

## [Proton Mail](https://proton.me/)

[![](../../../media/en/logos/proton-logo.png)](https://proton.me/)
(Android, iOS apps and web based for Linux, macOS, Windows)

An email service that supports end-to-end encryption both between its users and with external mail accounts. Apps are free and open-source.

1GB email storage for free - more for a fee.

[Download](https://proton.me/) | See [their guide](https://proton.me/support/mail)

## [Riseup Mail](https://riseup.net/email)

[![](../../../media/en/logos/riseup-logo.png)](https://riseup.net/email)
(Web based or using a third-party email client such as [Thunderbird](#thunderbird))

An invite-only email service that supports encryption and has a long history of protecting activists.

1 GB of storage space for e-mail.

See [their guide](https://riseup.net/en/email)

## [Autistici/Inventati](https://www.autistici.org/services/mail)

[![](../../../media/en/logos/autistici-logo.png)](https://www.autistici.org/services/mail)
(web based or using a third-party email client like [Thunderbird](#thunderbird))

An email service with a long history of protecting activists.

See [their guide](https://www.autistici.org/docs/mail/)

## [Disroot](https://disroot.org/en)

[![](../../../media/en/logos/disroot-logo.png)](https://disroot.org/en)
(web based or using a third-party email client like [Thunderbird](#thunderbird))

An email service run by a platform built on the principles of privacy, openness, transparency and freedom.

1GB email storage for free - more for a fee.

See [their guide](https://disroot.org/en/services/email)

## [Mailfence](https://mailfence.com/en/)

[![](../../../media/en/logos/Mailfence-logo.png)](https://mailfence.com/en/)
(Android and iOS apps; web based or using a third-party email client like [Thunderbird](#thunderbird) for Linux, macOS, Windows)

An email service that supports end-to-end encryption both between its users and with external mail accounts.

500 MB email storage for free - more for a fee.

See [their documentation](https://kb.mailfence.com/)

## [Posteo](https://posteo.de/en)

[![](../../../media/en/logos/posteo-logo.png)](https://posteo.de/en)
(Web based or using a third-party email client such as [Thunderbird](#thunderbird))

An email service with a focus on security, privacy and sustainability.

Paid service starting at 1 euro per month.

See [their documentation](https://kb.mailfence.com/)


# Disposable mailboxes

## [Guerrilla Mail](https://www.guerrillamail.com/)

[![](../../../media/en/logos/guerrillamail-logo.png)](https://www.guerrillamail.com/)

A free disposable email service that randomly generates disposable email addresses with several domain names you can choose from. Mailboxes can only be used to receive email, not to send messages.

## [anonbox](https://anonbox.net/index.en.html)

[![](../../../media/en/logos/anonbox-logo.png)](https://anonbox.net/index.en.html)

A free disposable email service managed by the German [Chaos Computer Club](https://www.ccc.de/en/) that randomly generates one-time email addresses. Mailboxes can only be used to receive email, not to send messages.

# Email applications that support end-to-end encryption

## [Thunderbird](https://www.thunderbird.net/)

[![](../../../media/en/logos/thunderbird-logo.png)](https://www.thunderbird.net/)
(Linux, macOS, Windows, Android in testing stage)

A free and open-source email application that supports end-to-end encryption and can download messages to your computer.

[Download for computers](https://www.thunderbird.net/thunderbird/all/) | [Download the testing version for Android](https://play.google.com/store/apps/details?id=net.thunderbird.android.beta) | See [their support pages](https://support.mozilla.org/en-US/products/thunderbird)

## [K-9 Mail](https://k9mail.app)

[![](../../../media/en/logos/k9-logo.png)](https://k9mail.app)
(Android)

A free and open-source email app that supports end-to-end encryption and can download messages to your Android device.

Download from [Google Play](https://play.google.com/store/apps/details?id=com.fsck.k9) or [F-Droid](https://f-droid.org/packages/com.fsck.k9/) | See [their user forum](https://forum.k9mail.app/)

## [OpenKeychain](https://www.openkeychain.org)

[![](../../../media/en/logos/openkeychain-logo.png)](https://www.openkeychain.org)
(Android)

A free and open-source encryption key management app that works with K-9 Mail.

Download from [Google Play](https://play.google.com/store/apps/details?id=org.sufficientlysecure.keychain) or [F-Droid](https://f-droid.org/app/org.sufficientlysecure.keychain) | See [their FAQ](https://www.openkeychain.org/faq/)

## [Mailvelope](https://mailvelope.com)

[![](../../../media/en/logos/mailvelope-logo.png)](https://mailvelope.com) (Chrome/Chromium, Firefox, Edge)

A free and open-source add-on/extension that you can use in Chrome/Chromium, Firefox and Edge to encrypt emails with PGP using the web interface of several webmail providers.

[Download](https://mailvelope.com/products) | See [their guide](https://mailvelope.com/help)

## [FlowCrypt](https://flowcrypt.com)

[![](../../../media/en/logos/flowcrypt-logo.png)](https://flowcrypt.com) (Android app for any email provider, iOS app for Gmail, browser extension/add-on for Gmail available for Firefox, Chrome/Chromium, Brave, Edge and Opera)

A free and open-source email encryption mobile app and browser extension based on OpenPGP. It is especially focused on Gmail but its Android app works for any email provider.

[Download](https://flowcrypt.com/download) | See [their documentation](https://flowcrypt.com/docs/)

# More secure text, voice and video chat applications

## [Signal](https://www.signal.org)

[![](../../../media/en/logos/signal-logo.png)](https://www.signal.org)
(Android, iOS, Linux, macOS, Windows)

A free and open-source application that provides secure text, voice and video chat services but that requires a working phone number to register

[Download](https://signal.org/download/) | See [their guide](https://support.signal.org/)

## [Jitsi](https://jitsi.org/)

[![](../../../media/en/logos/jitsi-logo.png)](https://jitsi.org/)
(Android, iOS, Linux, macOS, Windows)

A free and open-source app — with both free and paid versions — that provides secure voice and video chat services. Browser-based access is available for Linux, macOS and Windows users. (See our [Guide to Secure Group Chat & Conferencing Tools](https://www.frontlinedefenders.org/en/resource-publication/guide-secure-group-chat-and-conferencing-tools) and use only on trusted servers.) Individuals and organizations with the appropriate technical expertise can host Jitsi services themselves.

Download from [Apple's iOS App Store](https://apps.apple.com/us/app/jitsi-meet/id1165103905), [Google Play](https://play.google.com/store/apps/details?id=org.jitsi.meet) or [F-Droid](https://f-droid.org/en/packages/org.jitsi.meet/) | See [our guide](https://www.frontlinedefenders.org/sites/default/files/jitsi_guide_en.pdf) and [their FAQ](https://jitsi.org/user-faq/)

## [Element](https://element.io/)

[![](../../../media/en/logos/element-logo.png)](https://element.io/)
(Android, iOS, Linux, macOS, Windows)

A free and open-source application that provides end-to-end encrypted text chat and file sharing for individuals and groups. Element also provides secure voice and video messaging. Individuals and organizations with the appropriate technical expertise can host Element services themselves. Element is based on [Matrix](https://matrix.org/) communication standard.

[Download](https://element.io/get-started) | See [their guide](https://element.io/help)

## [Delta Chat](https://delta.chat/en/)

[![](../../../media/en/logos/deltachat-logo.png)](https://delta.chat/en/)
(Android, iOS, Linux, macOS, Windows)

A free and open-source application that provides secure text-chat services by relying on email providers to transfer data. Does not require a phone number to register (unlike WhatsApp, Signal and Telegram)

[Download](https://delta.chat/en/download) | See [their guide](https://delta.chat/en/help)

## [Mattermost](https://mattermost.com)

[![](../../../media/en/logos/mattermost-logo.png)](https://mattermost.com)
(Android, iOS, Linux, macOS, Windows)

A text, voice and video chat application for small-group collaboration. Individuals and organizations with the appropriate technical expertise can host Mattermost services themselves.

[Download](https://mattermost.com/download/) | See [their guide](https://mattermost.com/mattermost-overview/)

# More private social media platforms

## [Mastodon](https://joinmastodon.org)

[![](../../../media/en/logos/mastodon-logo.png)](https://joinmastodon.org)
(Android, iOS, Linux, macOS, Windows)

A public, decentralized social-media service that is not owned by corporations

[Download](https://joinmastodon.org/apps) | See [their guide](https://docs.joinmastodon.org/)

# Share large files more securely

## [share.riseup.net](https://share.riseup.net/)

[![](../../../media/en/logos/share.riseup.net-logo.png)](https://share.riseup.net/)
(Web based)

Protect the files you send with encryption. Files must be 50 MB or smaller, and the recipient must download them within 12 hours.

## [upload.disroot.org](https://upload.disroot.org/)

[![](../../../media/en/logos/upload.disroot.org-logo.png)](https://upload.disroot.org/)
(Web based)

Protect the files you send with encryption. Files can be up to 2 GB, and the recipient must download them within 30 days.

## [send.tresorit.com](https://send.tresorit.com/)

[![](../../../media/en/logos/send.tresorit.com-logo.png)](https://send.tresorit.com/)
(Web based)

Protect the files you send with encryption. Files can be up to 5 GB, and the recipient must download them within 7 days.

## [OnionShare](https://onionshare.org)

[![](../../../media/en/logos/onionshare-logo.png)](https://onionshare.org)
(Android, iOS, Linux, macOS, Windows)

Protect the files you send with end-to-end encryption, and send those files anonymously using [the Tor network](https://www.torproject.org/). OnionShare requires both you and the recipient to install software, but it also allows you to chat anonymously (text only) with the recipient or host a temporary, anonymous website. OnionShare is free and open-source software.

[Download](https://onionshare.org/#download) | See [their guide](https://docs.onionshare.org/2.6.2/en/)

# Collaborate on documents more securely

## [cryptpad.fr](https://cryptpad.fr/)

[![](../../../media/en/logos/cryptpad.fr-logo.png)](https://cryptpad.fr/)
(Web based)

Securely edit and share encrypted documents, spreadsheets, presentations, and surveys with this free and open-source software. Cryptpad.fr can also be self hosted.

See [their guide](https://docs.cryptpad.fr/en/)
