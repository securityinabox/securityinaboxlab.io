---
title: How does Security in a Box work?
topic: about
---

# How does Security in a Box work?

Security in a Box is a free and open-source resource hosted on [Gitlab.com](https://gitlab.com/securityinabox/securityinabox.gitlab.io).

Security in a Box is constantly updated thanks to the inputs of Front Line Defenders' Digital Protection Team as well as external contributions. All updates and changes are coordinated by Front Line Defenders' Digital Protection Editor.

All content included in Security in a Box is accurately selected based on the following criteria.

## How we develop the protection strategies we recommend

Security in a Box is a resource that includes many digital protection strategies. Browsing this website, you will find recommendations on how to create and manage strong passwords and on how to protect your accounts, communications, devices, internet connection, and sensitive information.

All these strategies are focused on the protection of digital assets, but they are developed in a holistic way: even if what we want to help you protect is digital, we know that protection always ranges across different spheres, including the legal and psycho-social context as well as a need to look at the physical level. For more information on what a holistic approach to protection implies, we recommend reading about the [holistic approach](https://holistic-security.tacticaltech.org/chapters/prepare/chapter-1-1-what-is-holistic-security.html) in the [Holistic Security Manual](https://holistic-security.tacticaltech.org).

Security in a Box primarily aims to help a global community of human rights defenders whose work puts them at risk. In general, we consider this resource aimed at helping not only human rights defenders but all those who are at risk of digital attacks, for example activists, journalists, and other members of civil society, as well as women and LGBTQIA+ persons who are at risk of gender-based online violence.

A central part of our approach is enabling our target users to assess the risks that they face and define their own needs. We aim to do no harm to human rights defenders, their families and communities, as well as to other readers of SiaB. We also recognise that listening with empathy to human rights defenders and other users at risk of digital violence and understanding the situations they are in is key to providing effective protection support. So the strategies described in SiaB are developed with an approach that aims at empowering our readers and are based on our users' actual needs and goals, in keeping with Front Line Defenders' [Vision, Mission & Core Values](https://www.frontlinedefenders.org/en/vision-mission-core-values).

## How we choose the tools and services we recommend

Software is complex, and it is not all created equal when it comes to its security- and privacy-protecting properties. Different tools and services can be considered more or less effective depending on the jurisdiction you are in, its laws, and the adversaries you may face.

We take a number of factors into account when selecting the tools and services that we recommend in Security in a Box. Each factor is important. Because different areas have different legal requirements for technology and face different threats, it is difficult to rank the importance of each factor globally.

Below, we list what we consider the most important questions we ask when we consider which tools and services we recommend. You can also make use of these criteria when you need to assess the relative safety of tools that are not listed in Security in a Box.


### Can the people who develop this tool or operate this service be trusted?

- What is the history of the development and ownership of the tool or service? Has it been created by activists or by a company? Does this group have a public face, and what's its background?
- What are the mission and business model of the entity that develops or operates this tool or service?
- Have there been any security challenges, for example data breaches or requests for information by state authorities? How have the service provider/tool developers reacted to those challenges? Have they openly addressed problems, or have they tried to cover them up?

### Does the tool encrypt data? With what kind of encryption technology?

- Does the tool encrypt the connection between the user and the people they are communicating with (end-to-end encryption), making it impossible for service providers to access their information?
- If end-to-end encryption is not available, we prioritize tools that encrypt the data between the user's device and the service infrastructure (to-server encryption). In such cases we always remind our readers that if data is not encrypted in the servers, they have to trust the people managing this service, as they have potential free access to the information they have stored there.
- Do the default settings protect users' privacy and security?
- If no end-to-end encryption is available, does the technology allow users to host the service in their own infrastructure, so they can control who can access their data?


### Is the code available to inspect?

- In other words, is it open-source?
- Has the tool or service been audited by security experts who are independent of the software development project or of the service provider company?
- When was the last audit? Are regular audits planned?
- Has the audit included all parts of the tool or service, or just some of its parts? If it is a service, has the server infrastructure been audited, or just the user interface?
- What do independent experts say about the tool or service?

### Is the technology mature?

- How long has this technology been in operation? Has it passed the test of time?
- Does it have a large community of developers who are still actively working on its development?
- How many active users does it have?

### Where are the servers located?

- This can be a difficult question to answer, with more and more services in the cloud, but trustworthy developers and service providers tend to publish this information in their websites or to make it available through trusted intermediaries.
- Also drawing conclusions from this piece of information can be tricky for us, as the servers might be located in a country that would protect HRDs and other civil society members in some states but not in others.
- In general we ask ourselves whether the servers are located in a country that would comply with a request by authorities from countries where there is no rule of law, and whether that country enforces human rights and consumer protection. In a nutshell, the question is: do authorities in non-democratic countries have the legal right to seize data or access information or shut down these services because of where the servers are located?

### What personal information does it require from users? What does the owner/operator have access to?

- Does the tool or service ask users to provide their phone number, email, nickname, or other personally identifiable information?
- Do they require to install a dedicated app/program that might potentially track users?
- What does their privacy policy declare? Are they protecting users' privacy and complying with privacy laws like the EU General Data Protection Regulation?
- What is stored on the servers? Do the company's terms and conditions give the owner the right to access users' data? For what purposes?
- What will this app/program have access to on a device: address book, location, microphone, camera, etc.? Can these permissions be deactivated, or will the app/program stop working in such case?

### What's the price? Is it affordable?

- When considering a service or tool, we consider its cost. Is it affordable? What is its developers' or provider's business model?
- In addition to up-front payment, we consider hosting costs, potential subscription fees, the cost of learning how to use and manage the tool or service, the need for possible IT support and/or additional equipment, etc.
- If the tool or service is provided for free, we ask ourselves why. Who is paying for the project if the tool/service is not paid? Is it free because it is based on volunteer work or because it is funded by governments or companies, or is the company actually making money by selling its users' data to third parties?

### Is it available on multiple operating systems and devices?

- Can the tool or service be used both on computers and mobile devices? Is it only available on proprietary operating systems like macOS and iOS or also on Linux? If it is not available on all operating systems, is there a reliable alternative for other devices where this tool/service cannot be used?

### Is it user-friendly? Will it work for people at risk?

- Is it confusing or frustrating to use this tool safely, or have the developers/provider made an effort to make it user-friendly?
- Do human rights defenders and other people at risk keep using this tool or service, or do they tend to abandon it?
- Is the user interface intuitive? Or, alternatively, has documentation been developed to clearly explain how to use the tool or service?

### Is it translated into several languages?

- Is the tool or service localized? Into how many languages?
- Is the localization continuously updated?
- What is the quality of the localization?
- Is multi-lingual documentation also available to help users understand how to safely use it?
