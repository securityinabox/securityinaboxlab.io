---
title: About Security-in-a-Box
topic: about
---

Security in a Box (SiaB) is a project of [Front Line Defenders](http://www.frontlinedefenders.org).

Created in 2005 (as [NGO in a Box - Security edition](https://web.archive.org/web/20060209174038/http://security.ngoinabox.org/)) in collaboration with [Tactical Technology Collective](http://www.tacticaltech.org) and then [renamed to its current title in 2009](https://web.archive.org/web/20090326230528/http://security.ngoinabox.org), it was significantly overhauled by Front Line Defenders in 2021 and is undergoing a continuous update process.

Security in a Box primarily aims to help a global community of human rights defenders whose work puts them at risk. It has been recognized worldwide as a foundational resource for helping people at risk protect their digital security and privacy.

Security in a Box is a free and open-source tool hosted on [Gitlab.com](https://gitlab.com/securityinabox/securityinabox.gitlab.io).

If you would like to give us feedback or contribute to Security in a Box, please follow [the instructions in the Readme file of the SiaB project on Gitlab](https://gitlab.com/securityinabox/securityinabox.gitlab.io/-/blob/main/readme.md).

## Access Security in a Box anonymously

To access Security in a Box anonymously using the Tor Browser, you can visit the [*onion service*](https://community.torproject.org/onion-services/overview/) below:

[http://lxjacvxrozjlxd7pqced7dyefnbityrwqjosuuaqponlg3v7esifrzad.onion/en/](http://lxjacvxrozjlxd7pqced7dyefnbityrwqjosuuaqponlg3v7esifrzad.onion/en/)

## About Front Line Defenders

Front Line Defenders was founded with the specific aim of protecting human rights defenders at risk, people who work, non-violently, for any or all of the rights enshrined in the Universal Declaration of Human Rights (UDHR). Front Line Defenders aims to address some of the needs identified by defenders themselves, including protection, networking, training and access to international bodies that can take action on their behalf.

## Funders

The development of Security in a Box has been supported by [Hivos](https://hivos.org/), [Internews](http://www.internews.eu/), [Sida](http://www.sida.se/English/), [Oak Foundation](http://www.oakfnd.org/), [Sigrid Rausing Fund](https://www.sigrid-rausing-trust.org/), [AJWS](http://ajws.org/), [Open Society Foundations](http://www.opensocietyfoundations.org/), [Ford Foundation](http://www.fordfoundation.org/), and [EIDHR](http://ec.europa.eu/europeaid/index_en.htm).

This website has been produced with the assistance of the European Union. Nevertheless, the contents of this website are the responsibility of Front Line Defenders and can in no way be taken to reflect the views of the European Union.

## License

This work is licensed under a [Creative Commons Attribution-Share Alike 4.0 Unported License](https://creativecommons.org/licenses/by-sa/4.0/). We strongly encourage the re-use of the material in Security in a Box.
