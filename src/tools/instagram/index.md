---
title: Protect yourself and your data when using Instagram
weight: 103
collection: none
post_date: 21 January 2025
logo: ../../../media/en/logos/social_networking-logo.png
website: https://www.instagram.com
---

Read this guide to learn how to secure your Instagram account.

Also see our guides on [Facebook](../facebook) and [WhatsApp](../whatsapp) to increase your privacy and security when using other platforms belonging to Meta.

Consider if relying on commercial, corporate products is secure enough to protect your information. Commercial corporations might not share your values and remove or filter content that they believe violates their policies. It may also be difficult for them to understand your local context, particularly if they do not operate in your language.

## Know how to move your data out of Instagram

- Test how to download your data from your Instagram Account. Start the download process, then take a thorough look at what data it provides. Assess what has been downloaded, in what format and how you can read and use it.
    - [Learn how to download your Instagram data](https://help.instagram.com/181231772500920).
    - Allow some time for this process. If you have had an account for a long time, there will be a lot of data to download, and the service may take a day or so to bundle it for your download.

<details><summary>Learn why we recommend this</summary>

Before you store data on a large commercial platform, it is always a good idea to understand what it will take to move your data out in case you change your mind and want to migrate to a different service. It is also important to consider that in some cases it may take a long time for your data to be completely deleted from the servers of a large platform.
</details>

# Inform yourself about Instagram's policies

## Learn how Instagram uses your information

- You can find a clarification of Instagram's terms of service on [Terms of Service; Didn't Read](https://tosdr.org/en/service/219).
- [Learn about Instagram's privacy policy](https://privacycenter.instagram.com/policy).
- Also review other Instagram's policies, in particular their [terms of use](https://help.instagram.com/581066165581870/) and [community standards](https://transparency.meta.com/policies/community-standards/).

<details><summary>Learn why we recommend this</summary>

It is often unclear what online service providers will do with your information when you share it. Is your data combined with other information to guess things about you? Is it sold to other companies that may share that information even if you did not want it to be shared? Read their terms of use and privacy policy to find out.

You can also install [the add-on/extension of Terms of Service; Didn't Read](https://tosdr.org/en/downloads) in your browser to see an overview of the terms of service for each commercial online service provider you use.
</details>

## Learn what Instagram will turn over to governments or law enforcement

- See [Meta's policy on law enforcement requests](https://about.meta.com/actions/safety/audiences/law/guidelines).
- See [Meta's transparency report on government requests for user data](https://transparency.meta.com/reports/government-data-requests/) to learn about the nature and extent of these requests and the policies and processes Meta has in place to handle them.
- See [Meta's transparency report on content restrictions based on local law](https://transparency.meta.com/reports/government-data-requests/) to learn what kind of requests they received and how they responded.
- Also see [Meta's transparency reports page](https://transparency.meta.com/reports/).

<details><summary>Learn why we recommend this</summary>

Social media sites may give your information, including information you were trying to keep private, to governments or law enforcement agencies if requested to do so. Look through the links in this section to learn more about the conditions under which they have provided or will provide such information.
</details>

# Create a new account

You can [create an Instagram account](https://help.instagram.com/155940534568753) for free.

## Decide whether you will use a real or fake name, and maintain separate accounts

- Be aware that even if you provide a fake name to Instagram, you may still be identifiable by the network you connect from and the IP address it assigns to your device unless you [use a VPN or Tor to hide this information](../../communication/multiple-identities/#hide-your-ip-address).
- Consider using separate accounts or separate identities/pseudonyms for different campaigns and activities. You will likely want to keep your personal and work accounts separate, at the very least.
    - Read more on this strategy in our guide on [multiple identity management](../../communication/multiple-identities).
- Remember that the key to using a social networking platform safely is being able to trust the people you connect with. You and the others in your network will want to know that the people behind the accounts are who they say they are, and have ways to validate this. That does not necessarily mean you have to use your real name, but it may be important to use consistent fake names.

<details><summary>Learn why we recommend this</summary>

Some people maintain social media accounts with fake names, or one account with their actual name and one with a fake name, to ensure they can organize and connect with others with less risk to their free speech, safety, or liberty.
</details>


## Set up with a fresh email address

- Set up a new email address on a privacy-friendly server while using [the Tor Browser or a trusted VPN](../../communication/multiple-identities/#hide-your-ip-address).
- You can find a list of suggested privacy-friendly mail services in [the communication tools section](../../communication/tools/#more-secure-email-providers).

<details><summary>Learn why we recommend this</summary>

Email addresses are one of the easiest ways to search for you: you need to provide one each time you set up a new account. If you need to hide your identity, it is best to start over with a new social media account which you do not connect to your old accounts or to existing email addresses.
</details>

## Don’t associate your phone number with your account

- If possible, when setting up a new account, do not enter your phone number. Instagram will allow you to set up a new account with only an email address, and that can be harder to associate with you.
   - [Remove your phone number from the Meta Accounts Center](https://www.facebook.com/help/124895950923762#remove-a-mobile-phone-number) (make sure your email address is valid so Instagram can use it to contact you, for example if you lose access to your account).

<details><summary>Learn why we recommend this</summary>

Your phone number can be easily used to look you up and identify you. Consider whether providing your regular phone number would increase your risk. Ask yourself if your local law enforcement can make a request to online service providers specifying your phone number to find out about activities associated with you, or whether someone seeking to harass or locate you might make use of your number.
</details>

## Skip “find friends”

- "Skip" or dismiss Instagram's requests to "allow access" to contacts on your device, import your contacts, connect with your other social media accounts, or find more connections when you first set up an account. You can select your contacts more carefully afterwards.
    - [Turn off contact syncing and delete your synced contacts](https://help.instagram.com/236691729788553).
    - [Stop Instagram from recommending you profiles you may want to follow, and from recommending others to follow your profile](https://help.instagram.com/530450580417848).
- Consider only connecting to people you know and whom you trust not to misuse the information you post.
- If you need to connect to an online community of like-minded individuals whom you have never met, consider carefully what information you will share with these people.
- Do not share your employer or educational background, as social media may use this information to share your profile with others unexpectedly.

<details><summary>Learn why we recommend this</summary>

Social media often ensure they will gain in popularity by using the contact lists in your devices and email accounts to find and recommend more people you might want to connect to. This can have dangerous effects when you want to keep your contacts hidden from others. Consider whether law enforcement in your area might use these contact lists to build a case against you and your colleagues if they confiscated your device or accessed your account. Or consider what might happen if social media revealed information about others you associate with to the public. If these are concerns for you, limit social media apps and sites permissions to use your contacts.
</details>

## Designate someone to manage your account if you are unable to do it yourself

- Note that for legal reasons, Instagram will not give a loved one or colleague access to your account. Rather, if you give them permission, they will make it possible to close and ["memorialize" your account](https://help.instagram.com/231764660354188).
- Verified immediate family members can request the removal of a deceased user's account. [Read Instagram's instructions on how to prove you're entitled to request such removal](https://help.instagram.com/264154560391256#removing-the-account).
- If you expect someone else may need to access your account quickly in the event of an emergency, arrange to share your login information with them using [secure communication](../../communication/private-communication).
- If someone you know has been arrested or detained and you need to suspend their account so as to stop others from accessing their network of contacts or compromising information, you can reach out to [Access Now Digital Security Helpline](https://www.accessnow.org/help/) or [Front Line Defenders](https://www.frontlinedefenders.org/emergency-contact) to request assistance in working with Instagram to secure access to their accounts. Also read [the Digital First Aid Kit troubleshooter Someone I Know Has Been Arrested](https://digitalfirstaid.org/arrested/) for further recommendations and see [the Digital First Aid Kit support page](https://digitalfirstaid.org/support/) to look for help desks that may support you for specific needs.

<details><summary>Learn why we recommend this</summary>

Giving access to important accounts to trusted contacts in case of emergencies is something everyone should think about, regardless of their risk level. Internet service providers and social media sites have developed processes to handle situations where someone passes away or is seriously ill or jailed and others need to manage their account. Designating someone to care for your account can ensure others are notified of your situation, and prevent malicious people from defacing or hacking your account, or from finding out who your contacts are.
</details>

# Protect your account

## Check your recovery email and phone number

- [Check your recovery email and phone number](https://help.instagram.com/583107688369069).
- Change this information immediately if you lose access to your email address or phone number.
- [Read more on how to keep your Instagram account secure](https://help.instagram.com/369001149843369/).

<details><summary>Learn why we recommend this</summary>

Online platforms ask you for an email address and/or a phone number to help recover your account in case of authentication issues. The email address is also used to inform the user of any security-related event. It is important to check this information to be sure that an attacker has not changed it to gain control of your account later. Make sure to also secure your recovery email and phone number, as an adversary may hack into one of them to change the password of the account where you set them as recovery methods.
</details>

## Use strong passwords

Use strong passwords to protect your accounts. Anyone who gets into your Instagram account will gain access to a lot of information about you and anyone you are connected to.

- See our guide on [how to create and maintain strong passwords](../../password/passwords) for more information.

## Set up two-factor authentication (2FA)

- Use a security key, authenticator app or security codes for two-factor authentication.
- Do not use SMS or a phone call if possible, as your mobile phone company has full access to these communications and these methods are easier to hack into. Note that we don't recommend associating your usual phone number with your account, especially if your official name is not already associated with your account.
- [Learn how two-factor authentication works on Instagram](https://help.instagram.com/566810106808145).

<details><summary>Learn why we recommend this</summary>

[See our guide on two-factor authentication](../../passwords/2fa) for more on why and how to set up two-factor authentication, sometimes known as 2FA or MFA.
</details>

## Download and save backup codes to get back into your account

- [Get recovery codes to sign into Instagram if you can't use your usual two-factor authentication method](https://help.instagram.com/1006568999411025).
    - Store these codes in your password manager.
		- Alternatively, print these codes out before you are in a situation where you might need them. Keep them somewhere secure and hidden, like a locked safe.

<details><summary>Learn why we recommend this</summary>

Having verification codes safely stored, written down or printed out gives you another way to get back into your account if you lose access to your two-factor authentication method.
</details>

## Consider enrolling in Advanced Protection

- If Instagram identifies your account as vulnerable, you may be invited to enroll in the Advanced Protection program. [Learn how Advanced Protection works and consider enrolling](https://help.instagram.com/945221763907783).

<details><summary>Learn why we recommend this</summary>

Instagram's Advanced Protection program adds a layer of security to particularly vulnerable accounts.

The Advanced Protection program is only available for people selected by Instagram. The criteria are currently not known to us. If you are eligible for Advanced Protection, you will be prompted to learn about the program and enroll. We recommend you consider these prompts as soon as you see them, as you may be locked out of your account if you don't enroll in the given time period.
</details>

## Be very careful with emails or messages claiming to be from Instagram or Meta

- [Learn how to recognize phishing emails and messages](https://help.instagram.com/670309656726033).
- [Check your account settings to make sure that an email you've received has actually been sent by Instagram](https://www.instagram.com/emails/emails_sent/).

<details><summary>Learn why we recommend this</summary>

Phishing messages might try to convince you they are coming from one of your service providers, to trick you into giving someone else access to your account. If you get a security email or text from an online service provider, don't click on any of its links. Also, do not provide your password. Instead, log into your account and check your settings to confirm whether the message was legitimate.
</details>

# Use Instagram more securely

- Instagram will require you to use its app for many activities, for example to publish stories. But if you mostly use Instagram to browse other users' profiles or to publish posts and chat with people, you can consider using a browser instead of installing the app in your mobile device.
    - If you really need to use the Instagram app, consider using a separate device from the one you normally use.
    - In alternative, if you have and Android device, you can [isolate the app in a separate profile](../../communication/multiple-identities/#sandbox-your-applications-on-android).
- If you want to use Instagram without connecting it to your real identity, make sure to create your account through a VPN or through the [Tor Browser](../../internet-connection/tools/#tor-browser) and to [never connect with your actual IP](../../communication/multiple-identities/#hide-your-ip-address). Also make sure to create your account with [an email address that you created using Tor or a VPN](../../internet-connection/anonymity/#exchange-anonymous-emails).

# Check suspicious access

## Check active sessions and authorized devices, review account activity and security events

- Look at the following instructions listing which devices have recently logged into your account (including browsers or apps). Does every login look familiar?
    - [Check where you're logged in from in your Activity log](https://www.instagram.com/session/login_activity/).
    - [Learn how to log out of Instagram on other devices you're currently logged in](https://help.instagram.com/2761108904184084/).
    - *Note that if you are using a VPN or Tor Browser, which can conceal your location, your own device may appear as connecting from unexpected locations*.
    - [Perform regular security checkups](https://about.instagram.com/safety/account-security#security-check-up).
- If you see suspicious activity on your account, immediately change your password to [a new, long, random passphrase](../../passwords/passwords) you do not use for any other accounts. Save this in your [password manager](../../passwords/password-managers).

<details><summary>Learn why we recommend this</summary>

Your adversaries may find ways to log into your account from their devices. If they do so, it is possible you will be able to see this from the Password and security section of your Accounts center, where you can review security issues by running checks across apps, devices and emails you have received from Instagram or other Meta services.
</details>

## Get notified about logins

- Get an alert when someone tries logging in from a device or location that's unusual for you.
    - [Learn how to set the alert](https://help.instagram.com/154776793259282).
    - Get the notification sent to the email address associated with your Instagram account, and make sure you choose an email that you check regularly.
    - When you receive such a notification, review the sign-in details, including device type, time and location. If this activity doesn't look familiar, click Sign out.

<details><summary>Learn why we recommend this</summary>

If you suspect your account may be at risk, use this feature to be notified right away of unusual access.
</details>

# If you think your account has been hacked

- [Take the actions suggested by Instagram](https://help.instagram.com/149494825257596).
- If you can't solve this issue through the standard procedure, check out [the Digital First Aid Kit troubleshooter I lost access to my account](https://digitalfirstaid.org/topics/account-access-issues).

# Decide what information to share

- [Consider making your account private](https://help.instagram.com/517073653436611).
    - [Learn how to make your account private](https://help.instagram.com/448523408565555).
- [Learn what tools are available to protect your privacy on Instagram](https://help.instagram.com/811572406418223/).

## Consider what information you should avoid sharing

Information that should never be shared with others on social media, even via direct messages (DM), may be, for example:

- Passwords
- Personally identifying information, including:
    - your birthday,
    - your phone number (does it appear in screenshots of communications?),
    - government or other ID numbers,
    - medical records,
    - education and employment history (these can be used by untrustworthy people who want to gain your confidence.)
- Information that may lead to understand where you live, for example a picture of your house.

Information that you might not want to post on social media, depending on your assessment of the threats you are facing, is, for example, details about family members, information on your sexual orientation or activities and your email address (at least consider having more and less sensitive email accounts).

- Even if you trust the people in your networks, remember it is easy for someone to copy your information and spread it more widely than you want it to be.
- Agree with your network on what you do and do not want shared, for safety reasons.
- Think about what you may be revealing about your friends that they may not want other people to know; be sensitive about this, and ask them to be sensitive regarding what they reveal on you.

<details><summary>Learn why we recommend this</summary>

The more information about yourself you reveal online, the easier it becomes for others to identify you and monitor your activities. For example, if you share (or "like") a post that opposes some position taken by your government, agents of that government might very well take an interest and target you for additional surveillance or direct persecution. This can have consequences for anybody, across different regions. The family of an activist who has left their home country, for example, may be targeted by the authorities in their homeland because of things that activist has made publicly accessible online.
</details>

## Check if what you publish on Instagram is automatically cross-posted

- Be careful when connecting your social network accounts for automatic cross-posting. You may be anonymous on one site, but exposed when using another.
- [Consider turning off automatic sharing to Facebook](https://help.instagram.com/797182937605779/#sharing-your-posts-automatically-to-facebook).
- [Unlink your Instagram account from other social networks](https://help.instagram.com/536741816349927).

<details><summary>Learn why we recommend this</summary>

Most online service providers allow you to integrate information with other services. For example, you can post an update on your Instagram account and have it automatically posted on your X account as well. When other sites and apps have access, they can also be used by hackers to get into your online accounts, and posting on many platforms at the same time, can make it easier to surveil what you do.  
</details>

## Don’t share your location

- [Learn how location works across Facebook and Instagram](https://privacycenter.instagram.com/guide/location/).
- [Turn location off on your iPhone or iPad](https://help.instagram.com/171821142968851).
- To learn how to turn off location on an Android device, find the section ["Stop an app from using your device's location" in the Android help page on how to manage location permissions for apps](https://support.google.com/accounts/answer/6179507).

<details><summary>Learn why we recommend this</summary>

If you are worried about someone finding your current or past location and about location tracking, stop your accounts from storing your location information. In addition we recommend turning off location services on your devices, which also makes your battery charge last longer.
</details>


## Share photos and videos more safely

- Consider what is visible in photos you post. Never post images that include:
    - your vehicle license plates,
    - IDs, credit cards or financial information,
    - photographs of keys ([it is possible to duplicate a key from a photo](https://www.cnet.com/news/duplicating-keys-from-a-photograph/).)
- Think hard before you post pictures that include or make it possible to identify:
    - your friends, colleagues and loved ones (ask permission before posting),
    - your home, your office or other locations where you often spend time,
    - if you are hiding your location, other identifiable locations in the background (buildings, trees, natural landscape features, etc.).
- If you're only posting private pictures, considering [making your account private](https://help.instagram.com/448523408565555).
- [Remove metadata before you post photos, videos and other files](../../files/destroy-identifying-information/#remove-metadata-from-pictures-videos-and-other-files).

<details><summary>Learn why we recommend this</summary>

What you share could put yourself or others at risk. Get in the habit of seeking consent before posting about others, where possible. You may want to work with your colleagues to set guidelines for what you will and won't share publicly, under what conditions.

Photos and videos can reveal a lot of information unintentionally, particularly what is in the background. Many cameras also embed hidden data (metadata or EXIF tags) about the location, date and time the photo was taken, the camera that took the photo, etc. Social media may publish this information when you upload photos or videos.
</details>

## Change who can see when you "like" things

- [Your likes are always visible, even if your account is set to private](https://help.instagram.com/281388201973414).

<details><summary>Learn why we recommend this</summary>

"Likes" and other ways you appreciate posts are sometimes shared by social media in ways that are not clear. In some countries, "likes" or other apparently harmless interaction with other social media accounts have been used in legal cases. You may want to use caution when leaving reactions to others' content, depending on your understanding of the legal situation in your country.
</details>

## Don't share your birthday

- On Instagram, you can register with a birthday that is not your own, and you can change your birthday also later. However, do keep track of what you enter, in case you need it to regain access to your account.
- Instagram does not share your birthday with others. It requests your birthday for age verification reasons.
    - [Learn how to change your birthday on Instagram](https://help.instagram.com/1063758210482081).

<details><summary>Learn why we recommend this</summary>

If you include your actual birthday in your account information, it can be used to identify you.
</details>

# Decide who can see what

## Share to select people

- [Remove an unwanted follower](https://help.instagram.com/413012278753813) (they will not be notified). Also consider [blocking them](https://help.instagram.com/426700567389543).
- Consider [making your account private](https://help.instagram.com/448523408565555).
- [Remove Instagram public photos and videos from search engine results like Google](https://help.instagram.com/147542625391305).
- Be aware that if you share one of your private posts to another social networking platform, [anyone with that link will be able to see what you shared](https://help.instagram.com/442837725762581).

## Manage who can reply to what you post

- [Turn off comments](https://help.instagram.com/1766818986917552).
- [Remove comments on your posts](https://help.instagram.com/289098941190483).
- [Delete multiple comments at once](https://help.instagram.com/289376615404536) (only available on the Instagram app).

<details><summary>Learn why we recommend this</summary>

In some cases, replies to posts have been used to build false claims of human rights defenders associating with people they did not actually associate with. Replies can also be used to harass you. Disabling comments can help lower your stress levels.
</details>

## Think about who you connect with

- [Unfollow people](https://help.instagram.com/286340048138725).
- [Deny requests to follow you](https://help.instagram.com/207917546007234).

<details><summary>Learn why we recommend this</summary>

When you establish contacts online, the community you create will reveal something about you to others. People may assume that you support or agree with what those people are saying or doing, which could make you vulnerable if you are seen to align yourself with particular political groups, for example. In some countries, connections on social media to individuals or groups have been used in court to make a case against someone, even when the two people were only loosely connected.
</details>

## Control mentions and tags

- [Control mentions and tags](https://help.instagram.com/345505572238391).

<details><summary>Learn why we recommend this</summary>

As with comments and other connections, tags could be used by others to make false accusations about your activities in a case against you.
</details>

## Limit who can contact you

- [Choose who can send you direct messages and add you to group conversations](https://help.instagram.com/296884305019942/).
- [Block an account](https://help.instagram.com/454180787965921).

<details><summary>Learn why we recommend this</summary>

Limiting who can contact you can lessen the likelihood that you will be found when you are trying to be private, or targeted by people trying to falsely gain your trust or the trust of your network. Limiting these options can also be useful if you are being harassed in non-public messages.
</details>

## Manage advertising

- [De-select all ad topics](https://www.facebook.com/help/instagram/245100253430454).
- [Tell Instagram not to use data from partners to show you ads](https://www.facebook.com/help/instagram/2885653514995517).
- Also check [the section on managing advertising in the guide on Facebook](../facebook/#manage-advertising) (as Instagram and Facebook both belong to the same company).

<details><summary>Learn why we recommend this</summary>

There is a possibility governments or police forces might buy advertising data from social media companies to target you and your network with disinformation, or try to find you.
</details>

# Leave no trace

## Precautions when using a public or shared device

- Avoid logging into your Instagram account from shared devices (like an internet cafe or other people's devices).
- Never save your passwords and delete your browsing history when you use a web browser on a public machine. Change the passwords of any accounts you accessed from shared devices as soon as you can, using your own device.

## Clear your search history

- [Clear your search history](https://help.instagram.com/354860134605952).

<details><summary>Learn why we recommend this</summary>

Some social media platforms keep track of things you have searched for within their sites and apps. If your account is compromised or your device is seized, your adversary could use this information against you, so it may be a good idea to clear this history out regularly, as well as clearing your browser history.
</details>

# Handle abuse

This section explains how to ask Instagram for assistance in situations of abuse, harassment, attacks and impersonation. If you are a human rights defender, journalist or activist at risk and Instagram is not assisting you effectively, you can reach out to [Access Now Digital Security Helpline](https://www.accessnow.org/help/) or [Front Line Defenders](https://www.frontlinedefenders.org/emergency-contact) to request assistance in working with Instagram. Also see [the Digital First Aid Kit support page](https://digitalfirstaid.org/support/) to look for help desks that may support you for specific needs.

## Report abuse

- [Report abuse and spam](https://help.instagram.com/165828726894770).
- [Report harassment](https://help.instagram.com/547601325292351).
- [Learn more on how to combat bullying and harassment on Instagram](https://help.instagram.com/464473649316860).

<details><summary>Learn why we recommend this</summary>

Social media have unfortunately become a favourite method of harassment and disinformation worldwide. If you see malicious impersonation, hashtags being flooded, disinformation being spread, or if you or your colleagues are being targeted and harassed, you may find help by following Instagram's reporting procedures. Review the processes for reporting in the support pages linked in this section.
</details>

## Report harassment that reveals information about you

- [Read Instagram recommendations on how to handle threats to share private images or personal info](https://help.instagram.com/616583228400289).

<details><summary>Learn why we recommend this</summary>

Some abusers may try to target you by revealing information about where you live or work, your family or friends, or other personal details including images and videos. In many cases you have a right to have this content taken down, even if it is true. This section provides information on how to get that content removed.
</details>

## Identify and report coordinated inauthentic activity (botnets and spam)

- [The best way to report abusive content or spam on Instagram is by using the Report link near the content itself. ](https://help.instagram.com/2922067214679225).
- [You can also remove potential spam accounts from your list of Instagram followers](https://help.instagram.com/340413218365867).

<details><summary>Learn why we recommend this</summary>

Some harassment and disinformation is posted through automated means, rather than by individuals. If you suspect that you are seeing this "coordinated inauthentic activity," you can report it to the platform that is hosting this content and they may ban those automated systems. While automation can be hard to prove, there are some cases in which reporting coordinated inauthentic activity might be more successful than reporting harassment, if you suspect the online platform will not understand the context of the harassment.
</details>

## Report impersonation

- [Learn how to report impersonation on Instagram](https://help.instagram.com/446663175382270).

## Hide stressful content

- [Hide a post marking it as "not interested"](https://help.instagram.com/1105548539497125).
- [Turn comments off for Instagram posts](https://help.instagram.com/1766818986917552).
- [Limit sensitive content that you see on Instagram](https://help.instagram.com/251027992727268).
- [Learn more about the sensitive content control on Instagram](https://help.instagram.com/1055538028699165).
- [Temporarily limit people from interacting with you on Instagram](https://help.instagram.com/4106887762741654).
- [Restrict someone on Instagram so you don't see their comments unless you want to](https://help.instagram.com/2638385956221960/). They also won't be able to see when you’re online or if you’ve read their messages.

<details><summary>Learn why we recommend this</summary>

Any of us may find some content more distressing than other people do, whether it be information on the death of a friend, public arguments which devalue us because of who we are or frightening events in the news. If you need a break from this stress, this section lists some tools which can help hide content you do not wish to see, for as long as you wish.
</details>

## Learn how to recover your account if it is disabled

- [Learn why an account may have been disabled and what you can do to recover it](https://help.instagram.com/804537404168716).

<details><summary>Learn why we recommend this</summary>

For one reason or another, Instagram may disable your account. Human rights defenders have sometimes had their accounts shut down, for example because they were documenting human rights abuses with violent scenes that violated the platform's policies, because they had been reported by a government, by the police or by people who disagreed with them, or even because Instagram did not understand their context well enough to make sense of what they were posting. If this happens to you, you can appeal the decision and ask to have your account restored.
</details>

## Take a break from your account

- [Temporarily deactivate your account](https://help.instagram.com/728869160569983).

<details><summary>Learn why we recommend this</summary>

If you want to stop people from posting to your account for a while — for example because you need to take a break, but also because you suspect you may be detained or jailed — you can consider temporarily deactivating your Instagram account. This can be useful also if you are facing harassment or defamation.
</details>
