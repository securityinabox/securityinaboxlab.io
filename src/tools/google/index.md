---
title: Protect your data and communications when using Google services
weight: 102
collection: none
post_date: 5 December 2024
logo: ../../../media/en/logos/google-logo.png
website: https://about.google/
---

Read this guide to learn how to secure your Google account, including your usage of Gmail, Google Drive, Google Photos and Google Maps.

Also see [our guide on YouTube](../youtube) to increase your privacy and security when sharing and viewing videos on Google's video platform and [our guide on Android](../../phones-and-computers/android/) to use Google's mobile operating system more securely.

Consider if relying on commercial, corporate products is secure enough to protect your information. Commercial corporations might not share your values and remove or filter content that they believe violates their policies. It may also be difficult for them to understand your local context, particularly if they do not operate in your language.

# Remember that Gmail and Google Drive are just some of the services included in your Google account

When you create a mail account on Gmail or you start using Google Drive to share your documents, you automatically create a Google account that gives you access to all of Google's services, including Google Workspace and [YouTube](../youtube). Note that if you log in to your Google account to use a Google service with your browser or [Android device](../../phones-and-computers/android/), you might share data about you and your internet usage, including non-Google services. It's a good idea to log out of your Google account once you've finished using Google services.

In alternative, you can use a dedicated browser only to use services with your Google account and you can [use an email client](#download-your-mail) to read your mailbox on Gmail.

You can also [use Android devices without a Google account](https://securityinabox.org/en/phones-and-computers/android/#advanced-use-android-without-a-google-account).

Also consider using more than one Google account based on your needs and activities. Read more on this strategy in our guide on [multiple identity management](../../communication/multiple-identities).

# Know how to move your data out of Google

- Test how to download your data from your Google Account. Start the download process, then take a thorough look at what data it provides. Assess what has been downloaded, in what format and how you can read and use it.
    - [Learn how to download your Google data](https://support.google.com/accounts/answer/3024190).
    - Select data that you believe may be relevant from [this long list](https://takeout.google.com/).
    - Allow some time for this process. If you have had an account for a long time, there will be a lot of data stored in Google's servers, and the service may take a day or so to bundle it for your download.

<details><summary>Learn why we recommend this</summary>

Before you store data on a large commercial platform, it is always a good idea to understand what it will take to move your data out in case you change your mind and want to migrate to a different service. It is also important to consider that in some cases it may take a long time for your data to be completely deleted from the servers of a large platform.
</details>

# Inform yourself about Google's policies

# Learn how Google uses your information

- You can find a clarification of Google's terms of service on [Terms of Service; Didn't Read](https://tosdr.org/en/service/217).
- Also review [Google's policies](https://policies.google.com/), in particular their [privacy policy](https://policies.google.com/privacy) and their [terms of service](https://policies.google.com/terms).

<details><summary>Learn why we recommend this</summary>

It is often unclear what online service providers will do with your information when you share it. Is your data combined with other information to guess things about you? Is it sold to other companies that may share that information even if you did not want it to be shared? Read their terms of service and privacy policy to find out.

You can also install [the add-on/extension of Terms of Service; Didn't Read](https://tosdr.org/en/downloads) in your browser to see an overview of the terms of service for each commercial online service provider you use.
</details>

## Learn what Google will turn over to governments or law enforcement

- Search for "Google" and the name of your country or jurisdiction on [Lumen](https://lumendatabase.org/).
- See [Google's policy on law enforcement requests](https://policies.google.com/terms/information-requests).

<details><summary>Learn why we recommend this</summary>

Social media sites may give your information, including information you were trying to keep private, to governments or law enforcement agencies if requested to do so. Look through the links in this section to learn more about the conditions under which they have provided or will provide such information.
</details>

# Create a new account

You can [create a Google account](https://support.google.com/accounts/answer/27441) for free either for you personally or for your organisation, your business or your child.

For additional features, Google offers [paid plans](https://support.google.com/a/answer/6043576) (free for [frontline workers](https://support.google.com/a/answer/10393656) and [educational institutions](https://support.google.com/a/answer/134628)).

## Decide whether you will use a real or fake name, and maintain separate accounts

- Be aware that even if you provide a fake name to Google, you may still be identifiable by the network you connect from and the IP address it assigns to your device unless you [use a VPN or Tor to hide this information](../../communication/multiple-identities/#hide-your-ip-address).
- Consider using separate accounts or separate identities/pseudonyms for different campaigns and activities. You will likely want to keep your personal and work accounts separate, at the very least.
    - Read more on this strategy in our guide on [multiple identity management](../../communication/multiple-identities).

## Set up your Google account

You can create a new Google account without providing another email address.

- Go to the [Google Account sign in page](https://accounts.google.com/signin).
- Click Create account and choose "For my personal use".
- Enter your First and Last name (they don't need to be the real ones) and click Next.
- Enter your birthday date and your gender (they don't need to be the real ones) and click Next.
- Check Create a Gmail address and click Next.
- Choose one of the suggested email addresses or create a different one, then click Next.
- Enter a strong and unique password twice, click Next and you're done.

<details><summary>Learn why we recommend this</summary>

If you need to hide your identity, it is best to start over with a new account which you do not connect to your old accounts or to existing email addresses that are connected to your name. For this purpose, we recommend creating a Gmail account without connecting it to an existing email address. If you need to consistently hide your identity also from your email provider, it is best [to also avoid connecting it to your phone or to your IP](../../internet-connection/anonymity/#exchange-anonymous-emails).
</details>

## Don’t associate your phone number with your account

- If possible, when setting up a new account, do not enter your own phone number.
    - [Learn how to create a new phone number to create an account that is not connected to your official identity](../../communication/multiple-identities/#get-a-different-phone-number).

<details><summary>Learn why we recommend this</summary>

Your phone number can be easily used to look you up and identify you. Consider whether providing your regular phone number would increase your risk. Ask yourself if your local law enforcement can make a request to online service providers specifying your phone number to find out about activities associated with you, or whether someone seeking to harass or locate you might make use of your number.
</details>

## Designate someone to manage your account if you are unable to do it yourself

- You can choose a trusted contact who will receive an email if your account is unused for a certain amount of time (at least 3 months). That contact can then download data you have specified from your account.
    - [Select a trusted contact](https://support.google.com/accounts/answer/3036546).
- If you expect someone else may need to access your account quickly in the event of an emergency, arrange to share your login information with them using [secure communication](../../communication/private-communication).
- If someone you know has been arrested or detained and you need to immediately remove compromising information from their Gmail mailbox or other Google services, read [the Digital First Aid Kit troubleshooter Someone I Know Has Been Arrested](https://digitalfirstaid.org/arrested/).
    - If you need immediate assistance in working with Google to secure access to their accounts, you can reach out to [Access Now Digital Security Helpline](https://www.accessnow.org/help/) or [Front Line Defenders](https://www.frontlinedefenders.org/emergency-contact). Also see [the Digital First Aid Kit support page](https://digitalfirstaid.org/support/) to look for specific services you may need.

<details><summary>Learn why we recommend this</summary>

Giving access to important accounts to trusted contacts in case of emergencies is something everyone should think about, regardless of their risk level. Internet service providers and social media sites have developed processes to handle situations where someone passes away or is seriously ill or jailed and others need to manage their account. Designating someone to care for your account can ensure others are notified of your situation, and prevent malicious people from defacing or hacking your account, or from finding out who your contacts are.
</details>

# Protect your account

## Check your recovery email and phone number

- [Check your recovery email](https://myaccount.google.com/recovery/email).
- [Check your recovery phone number](https://myaccount.google.com/signinoptions/rescuephone).
- Change this information immediately if you lose access to your recovery email address or recovery phone number.

<details><summary>Learn why we recommend this</summary>

Online platforms ask you for an email address and/or a phone number to help recover your account in case of authentication issues. The email address is also used to inform the user of any security-related event. It is important to check this information to be sure that an attacker has not changed it to gain control of your account later. Make sure to also secure your recovery email and phone number, as an adversary may hack into one of them to change the password of the account where you set them as recovery methods.
</details>

## Use strong passwords

Use strong passwords to protect your accounts. Anyone who gets into your Google account will gain access to a lot of information about you and anyone you are connected to.

- See our guide on [how to create and maintain strong passwords](../../password/passwords) for more information.


## Set up 2-step verification

- Use a security key, an authenticator app or security codes for multi-factor authentication.
- Do not use SMS or a phone call if possible, as your mobile phone company has full access to these communications and these methods are easier to hack into. Note that we don't recommend associating your usual phone number with your account, especially if your official name is not already associated with your account.
    - [See your options for 2-step verification](https://support.google.com/accounts/topic/7189145).
    - [Turn on 2-step verification](https://support.google.com/accounts/answer/185839).

<details><summary>Learn why we recommend this</summary>

[See our guide on two-factor authentication](../../passwords/2fa) for more on why and how to set up 2-step verification, sometimes known as 2FA or MFA.
</details>

## Download and save backup codes to get back into your account

- [Get backup codes to sign into your Google Account if you can't use your usual 2-step verification method](https://support.google.com/accounts/answer/1187538).
    - Store these codes in your password manager.
    - Alternatively, print these codes out before you are in a situation where you might need them. Keep them somewhere secure and hidden, like a locked safe.
- [If you have an Android device, try using a Google security code](https://support.google.com/accounts/answer/6046815).

<details><summary>Learn why we recommend this</summary>

Having verification codes safely stored, written down or printed out gives you another way to get back into your account if you lose access to your 2-step verification method.
</details>

## Consider enrolling in Google's Advanced Protection Program

- [Learn how to get started](https://landing.google.com/advancedprotection/#enroll-in-minutes).
- [Learn how to turn on Advanced Protection on your mobile device](https://support.google.com/accounts/answer/7519408).
- [Watch a video explaining what the Advanced Protection Program is about](https://youtu.be/Twe0qlqyTfo).
- [Read the Advanced Protection Program FAQ](https://landing.google.com/advancedprotection/faq/).

<details><summary>Learn why we recommend this</summary>

Google's Advanced Protection Program is the strongest level of Google Account security and provides extra safeguards against common attacks like phishing, malware and fraudulent access to data. Google developed this program for people at high risk of digital attacks, like human rights defenders or journalists.

To enroll, you will need a FIDO-compliant security key like a [Google Titan Key](https://cloud.google.com/security/products/titan-security-key?hl=en), a [Yubikey](https://www.yubico.com/products/), a [Nitrokey](https://www.nitrokey.com/products/nitrokeys) or a [Thetis Key](https://thetis.io/). In alternative you can use a passkey, but first read [our recommendations on how to use a passkey more securely in our guide on passwords](../../passwords/passwords/#consider-how-and-when-to-use-passkeys).

Note that once you have enrolled, you will experience some restrictions in your normal usage of Google services. For example you will need your security key or passkey to sign in on any new device and you won't be able to use your Google Account with some apps and services that require access to sensitive data, like your emails or Google Drive.
</details>

## Remember to do regular security and privacy checkups

- Regularly review [your Google Account security page](https://myaccount.google.com/security) to make sure everything is up-to-date.
- Go to the [security checkup page](https://myaccount.google.com/security-checkup) to make sure your account has not been exposed to security incidents.
- Go to the [privacy check-up page](https://myaccount.google.com/privacycheckup) to review what information Google stores about you and your activities.

## Know what to do if your device is lost or stolen

- [Find, lock or erase a stolen Android device](https://support.google.com/accounts/answer/6160491).
- [Sign out from the lost or stolen device](https://support.google.com/accounts/answer/3067630#remove).
- [Read the Digital First Aid Kit troubleshooter I lost my device](https://digitalfirstaid.org/topics/lost-device/) to learn what other important steps you can take in case your device is lost or stolen.

## Be very careful with emails or messages claiming to be from Google

- [Follow these instructions to view security events in your account](https://support.google.com/accounts/answer/6063333).

<details><summary>Learn why we recommend this</summary>

Phishing messages might try to convince you they are coming from one of your service providers, to trick you into giving someone else access to your account. If you get a security email or text from an online service provider, don't click on any of its links. Also, do not provide your password. Instead, log in to your account and go to the security page of your account to confirm whether the message was legitimate.
</details>

# Use your Google services more securely

## Gmail

While Gmail offers an intuitive web interface, a secure and reliable service and a quite large storage space for your mailbox, it is a commercial service and it is not primarily focused on protecting your privacy.

If you would like to consider more privacy-oriented mail providers, see [our list in the communication tools section](../../communication/tools/#more-secure-email-providers) and [the criteria we recommend to choose an email provider](../../communication/secure-email/#choose-an-email-provider).

We recommend you read [the guide on how to secure your email communications](../../communication/secure-email) to start using Gmail more securely.

Also read the following sections to learn how and why to [download your Gmail messages](#download-your-mail) to your device and how to [secure your mail through end-to-end encryption](#encrypt-your-mail).

### Download your mail

- [Learn how to add Gmail to an email client](https://support.google.com/mail/answer/7126229).
- [Learn how to add your Gmail account to Thunderbird](https://support.mozilla.org/en-US/kb/automatic-account-configuration).
    - [Consider regularly deleting mail stored on the server using POP3](../../communication/secure-email/#consider-regularly-deleting-mail-stored-on-the-server).
- [Learn how to configure Thunderbird to work seamlessly with Gmail](https://support.mozilla.org/en-US/kb/thunderbird-and-gmail).

<details><summary>Learn why we recommend this</summary>

While you can read Gmail in a browser or through the native mobile app, we recommend you use an email client like [Thunderbird](../../communication/tools/#thunderbird) or [K-9 Mail](../../communication/tools/#k-9-mail) to download your email, delete it from the server and organize it as you prefer.
</details>

### Protect your email messages with end-to-end encryption

*Note that if you are using Google Workspace Enterprise Plus, Education Plus or Education Standard edition, your system administrator may have enabled server-side encryption, which means that the body of your emails, including inline images and attachments, will be end-to-end encrypted through S/MIME with all members of your organization and can also be encrypted if you are writing to someone who uses S/MIME encryption and has exchanged digital signatures with you.*

- *[Learn more on Gmail client-side encryption if you are using Google Workspace Enterprise Plus, Education Plus or Education Standard edition](https://support.google.com/mail/answer/13317990).*

If you are using a standard Gmail account, you can protect your messages with end-to-end encryption by using a [tool that enables OpenPGP](../../communication/tools/#email-applications-that-support-end-to-end-encryption).

- Download your email through a client that supports end-to-end encryption like [Thunderbird](../../communication/tools/#thunderbird) or [K-9 Mail](../../communication/tools/#k-9-mail) with [OpenKeychain](../../communication/tools/#openkeychain).
    - [Learn more about OpenPGP in Thunderbird](https://support.mozilla.org/en-US/kb/openpgp-thunderbird-howto-and-faq) (Windows, Linux, macOS, Android).
    - Read [the guide on how to encrypt and decrypt email with K-9 Mail and OpenKeychain](https://docs.k9mail.app/en/6.400/security/pgp/) (Android).
- If you prefer reading your email in your browser, install either [Mailvelope](../../communication/tools/#mailvelope) or [FlowCrypt](https://flowcrypt.com/) - two browser add-ons/extensions that let you encrypt and decrypt your email directly in the Gmail web application.
    - [Get started with Mailvelope](https://mailvelope.com/en/help) (Chrome/Chromium, Firefox, Edge).
		- [Learn how to set up and use FlowCrypt](https://flowcrypt.com/docs) (Firefox, Chrome/Chromium, Brave, Opera. Also available as Android and iOS app).

### Check forwarding settings

Google notifies you both in the interface and by email when auto-forwarding is enabled, but you should still check to ensure your mail isn't forwarded automatically to someone else without you knowing. To see this setting, go to the [Forwarding and POP/IMAP section of your account settings](https://mail.google.com/mail/#settings/fwdandpop).

- Make sure that "Disable forwarding" is selected.
- Delete any email address you do not own or trust.
- Also check if POP and IMAP are enabled; turn them off if you are only using your browser and/or Gmail mobile app to access Gmail. If you are [using a mail client to download your email](#download-your-mail), keep POP or IMAP enabled, depending on the protocol you are using to download your email to your devices.

<details><summary>Learn why we recommend this</summary>

Auto-forwarding is an easy way for an attacker to access your email after compromising your account, by having it redirect all of your email to them. Follow the instructions in this section to make sure that this option is not enabled, [especially if you think your account was compromised](#if-you-think-your-account-has-been-hacked).
</details>


## Google Drive

### Share documents securely

- Share a document or a folder [with specific persons](https://support.google.com/drive/answer/2494822?hl=en&co=GENIE.Platform%3DDesktop#zippy=%2Cshare-with-specific-people), [with a group](https://support.google.com/drive/answer/2494822?hl=en&co=GENIE.Platform%3DDesktop#zippy=%2Cshare-with-a-group-of-specific-people), [with anyone who has the document link](https://support.google.com/drive/answer/2494822?hl=en&co=GENIE.Platform%3DDesktop#zippy=%2Callow-general-access-to-the-file) or [publicly](https://support.google.com/drive/answer/2494822?hl=en&co=GENIE.Platform%3DDesktop#zippy=%2Cshare-a-file-publicly).
    - You can also decide to let people [edit a document, add comments or just view it](https://support.google.com/drive/answer/2494822?hl=en&co=GENIE.Platform%3DDesktop#zippy=%2Cchoose-if-people-can-view-comment-or-edit).
    - You can [share the document with an expiration date](https://support.google.com/drive/answer/2494822?hl=en&co=GENIE.Platform%3DDesktop#zippy=%2Cadd-an-expiration-date).
- [Stop sharing a file](https://support.google.com/drive/answer/2494893/#7014631).
- [Limit how your files are shared](https://support.google.com/drive/answer/2494893/#restrict_sharing).
- [Transfer ownership of a file](https://support.google.com/drive/answer/2494892).

## Google Maps

### Don’t share your location

- [Learn how to understand and manage your location when you search on Google](https://support.google.com/websearch/answer/179386).
    - If you have set certain places as "home" or "work" in your "labeled places" in Google Maps, [clear that information](https://support.google.com/websearch/answer/179386#zippy=%2Cyour-home-or-work-address-from-your-google-account).
		- If you're using an Android device, follow the instructions [on how to stop an app from using your phone's location](https://support.google.com/accounts/answer/6179507).

<details><summary>Learn why we recommend this</summary>

If you are worried about someone finding your current or past location and about location tracking, stop your accounts from storing your location information. In addition we recommend turning off location services on your devices, which also makes your battery charge last longer.
</details>

## Google Photos

### Share photos and videos more safely

- Consider what is visible in photos you post. Never post images that include:
    - your vehicle license plates,
    - IDs, credit cards or financial information,
    - photographs of keys ([it is possible to duplicate a key from a photo](https://www.cnet.com/news/duplicating-keys-from-a-photograph/).)
- Think hard before you post pictures that include or make it possible to identify:
    - your friends, colleagues and loved ones (ask permission before posting),
    - your home, your office or other locations where you often spend time,
    - if you are hiding your location, other identifiable locations in the background (buildings, trees, natural landscape features, etc.)
- Google Photos performs some face recognition on photos. See [Google's support page on labeling faces and learn in particular how to remove the "me" face label and how to stop letting contacts get suggestions based on your "me" face label](https://support.google.com/photos/answer/7378566).
- [Stop sharing with others in Google Photos](https://support.google.com/photos/answer/6280921).
- See if there are accounts Google Photos is sharing with that you don't recognize and [stop sharing by "removing a partner"](https://support.google.com/photos/answer/7378858).
- [Turn off suggestions to share with your contacts](https://support.google.com/photos/answer/7378859).
- In the [Google support page on how to choose albums to display on your photo frames, follow the instructions on how to pick albums to display on your device](https://support.google.com/photos/answer/9458709) to see which devices you may be sharing to, and remove any you do not recognize.
- [Remove metadata before you post photos, videos and other files](../../files/destroy-identifying-information/#remove-metadata-from-pictures-videos-and-other-files).

<details><summary>Learn why we recommend this</summary>

What you share could put yourself or others at risk. Get in the habit of seeking consent before posting about others, where possible. You may want to work with your colleagues to set guidelines for what you will and won't share publicly, under what conditions.

Photos and videos can reveal a lot of information unintentionally, particularly what is in the background. Many cameras also embed hidden data (metadata or EXIF tags) about the location, date and time the photo was taken, the camera that took the photo, etc. Social media may publish this information when you upload photos or videos.
</details>


# Check suspicious access

## Check active sessions and authorized devices, review account activity and security events

- Look at the following instructions listing which devices have recently logged in to your account (including browsers or apps). Does every login look familiar?
    - [Check the recent security activity section in your account](https://myaccount.google.com/notifications) and click on each activity to take a closer look.
    - If you see activities you don't recognize, click Secure your account.
    - *Note that if you are using a VPN or Tor Browser, which can conceal your location, your own device may appear as connecting from unexpected locations*.
- If you see suspicious activity on your account, immediately change your password to [a new, long, random passphrase](../../passwords/passwords) you do not use for any other accounts. Save this in your [password manager](../../passwords/password-managers).
- Read more on this topic in [Google's support page on checking devices with account access](https://support.google.com/accounts/answer/3067630).

<details><summary>Learn why we recommend this</summary>

Your adversaries may find ways to watch your accounts by logging in from their devices. If they do so, it is possible you will be able to see this from the security page where Google lists which devices have been used to log in to your accounts.
</details>

## Get notified about logins

- [Google automatically sends notifications of logins](https://support.google.com/accounts/answer/7305876) from new, unknown devices to your Android device or your recovery email.
    - Make sure you choose a recovery email that you check regularly.
    - When you receive such a notification, review the sign-in details, including device type, time and location. If this activity doesn't look familiar, click No, it's not me.

## If you think your account has been hacked

- [Use the Security Checkup tool to see if anything looks unusual](https://myaccount.google.com/security-checkup).
- If your account has been compromised, [follow the steps listed in the Google support page on how to secure a hacked or compromised account](https://support.google.com/accounts/answer/6294825).
- If you can't solve this issue through the standard procedure, check out [the Digital First Aid Kit troubleshooter I lost access to my account](https://digitalfirstaid.org/topics/account-access-issues).

## Review other sites and apps that can access your account

- Avoid using your accounts to log in to other sites (like news sites for example). While it may be convenient to use your Google account as a login option, you may be also making it easier for attackers to exploit this functionality and you may leave more evidence of what you have viewed online. Use a different password for every site, and save it in your [password manager](../../passwords/password-managers).
- Be careful when connecting your accounts. You may be anonymous on one site, but exposed when using another.
- Use the following links to learn about sites and apps connected to your Google account:
    - [Check if your account is connected to third-party apps or services](https://myaccount.google.com/permissions).
    - [Check if you have created passwords for third-party apps](https://myaccount.google.com/apppasswords). If you have, consider that app passwords are less secure than using up-to-date apps and services that comply with modern security standards and check if you can use another method to sign in.
    - Read more on [Sharing some access to your Google Account with third-party apps](https://support.google.com/accounts/answer/14012355).
    - Read more on [Google Account Linking with third-party apps and services](https://support.google.com/accounts/answer/13864929?hl=en&ref_topic=14135058).

<details><summary>Learn why we recommend this</summary>

Most online service providers allow you to integrate information with other services. For example, you can post an update on your X account and have it automatically posted on your Facebook account as well. When other sites and apps have access, they can also be used by hackers to get into your online accounts.
</details>

# Decide what information to share in emails and on your profile

Information that should never be shared publicly or through unencrypted messages may be, for example:

- Passwords
- Personally identifying information, including:
    - your birthday,
    - your phone number,
    - government or other ID numbers,
    - medical records,
    - education and employment history (these can be used by untrustworthy people who want to gain your confidence.)
- Information that may lead to understand where you live, for example a picture of your house.

Information that you might not want to share publicly or in unencrypted messages, depending on your assessment of the threats you are facing, is, for example, details about family members and information on your sexual orientation or activities.

- Even if you trust the people in your networks, remember it is easy for someone to copy your information and spread it more widely than you want it to be.
- Agree with your network on what you do and do not want shared, for safety reasons.
- Think about what you may be revealing about your friends that they may not want other people to know; be sensitive about this, and ask them to be sensitive regarding what they reveal on you.

<details><summary>Learn why we recommend this</summary>

The more information about yourself you reveal online, the easier it becomes for others to identify you and monitor your activities. This can have consequences for anybody, across different regions. The family of an activist who has left their home country, for example, may be targeted by the authorities in their homeland because of things that activist has made publicly accessible online.
</details>

## Don't share your birthday

- When registering your Google account, you can provide a birthday that is not your own. Once you've added your birthday to your account, you can't delete it. However, you can edit its settings and choose who can see it.
    - Read more on how to [change your Google account personal info, like your name and birthday, in Google's support page on how to change your account image, name and other info](https://support.google.com/accounts/answer/27442#name&zippy=%2Cbirthday%2Cname).

<details><summary>Learn why we recommend this</summary>

If you include your actual birthday in your account information, it can be used to identify you.
</details>


# Decide who can find or contact you

## Limit who can contact you

- [Block an email address](https://support.google.com/mail/answer/8151).

<details><summary>Learn why we recommend this</summary>

Blocking email addresses can be useful if you are being harassed in non-public messages.
</details>

## Manage advertising

- [Turn off ad personalization](https://adssettings.google.com/).

<details><summary>Learn why we recommend this</summary>

There is a possibility governments or police forces might buy advertising data from social media companies to target you and your network with disinformation, or try to find you.
</details>

# Leave no trace

## Precautions when using a public or shared device

- Avoid accessing your Gmail inbox or logging in to your Google account from shared devices (like an internet cafe or other people's devices).
- Never save your passwords and delete your browsing history when you use a web browser on a public machine. Change the passwords of any accounts you accessed from shared devices as soon as you can, using your own device.

## Delete search history

- [Clear past Web & App Activity](https://myactivity.google.com/activitycontrols/webandapp).
- [Turn off Web & App Activity tracking](https://myactivity.google.com/activitycontrols).
- [Learn how to delete your activity or stop saving it when you use Google sites, apps and services like Maps](https://support.google.com/accounts/answer/465).

<details><summary>Learn why we recommend this</summary>

Some online services keep track of things you have searched for within their sites and apps. If your account is compromised or your device is seized, your adversary could use this information against you, so it may be a good idea to clear this history out regularly, or avoid saving it altogether, as well as clearing your browser history.
</details>

# Handle abuse

## Report abuse

- [Report abuse in Google Groups](https://support.google.com/groups/answer/81275).
- [Report a violation of Google's Terms of Service in Google Workspace, Sites or Drive](https://support.google.com/docs/answer/2463296).

<details><summary>Learn why we recommend this</summary>

Social media have unfortunately become a favorite method of harassment and disinformation worldwide. If you see malicious disinformation or hate speech, or if you or your colleagues are being targeted and harassed, you may find help by following Google's reporting procedures. Review the processes for reporting in the support pages linked in this section.
</details>

## Report harassment that reveals information about you

- [Remove sensitive information from Google searches](https://support.google.com/websearch/troubleshooter/3111061).
- [Select the specific Google services you would like to request assistance with (including YouTube, Blogger and Google Search) and proceed to reporting content that you would like removed from these services](https://support.google.com/legal/troubleshooter/1114905).

<details><summary>Learn why we recommend this</summary>

Some abusers may try to target you by revealing information about where you live or work, your family or friends, or other personal details including images and videos. In many cases you have a right to have this content taken down, even if it is true. This section provides information on how to get that content removed.
</details>

## Identify and report coordinated inauthentic activity (botnets and spam)

- Look for a "(Send) Feedback" option in most Google products, [for example Search](https://support.google.com/websearch/answer/6223687).
- [In Gmail or Google Messages, report spam](https://support.google.com/mail/answer/1366858).

<details><summary>Learn why we recommend this</summary>

Some harassment and disinformation is posted through automated means, rather than by individuals. If you suspect that you are seeing this "coordinated inauthentic activity," you can report it to the platform that is hosting this content and they may ban those automated systems. While automation can be hard to prove, there are some cases in which reporting coordinated inauthentic activity might be more successful than reporting harassment, if you suspect the online platform will not understand the context of the harassment.
</details>

## Report impersonation

- Google will not assist in removing impersonation per se, but it will [assist in removing fake pornography targeting someone](https://support.google.com/websearch/answer/9116649).

<details><summary>Learn why we recommend this</summary>

Impersonation in the form of parody is usually accepted as free speech by most online platforms, and will not be removed. However, impersonation for the purposes of defamation of character may not be, and [you can report it](#report-abuse).
</details>

## Hide stressful content

- On Google Search, SafeSearch can [block adult content](https://support.google.com/websearch/answer/510).
- On YouTube, Restricted Mode can [block adult and violent content](https://support.google.com/youtube/answer/174084).

<details><summary>Learn why we recommend this</summary>

Any of us may find some content more distressing than other people do, whether it be information on the death of a friend, public arguments which devalue us because of who we are or frightening events in the news. If you need a break from this stress, this section lists some tools which can help hide content you do not wish to see, for as long as you wish.
</details>

## Learn how to recover your account if it is disabled or suspended

- [Learn how to recover your account after it has been disabled](https://support.google.com/accounts/answer/40695).

<details><summary>Learn why we recommend this</summary>

[For one reason or another, Google may disable your account](https://support.google.com/accounts/answer/40695#why). Human rights defenders have sometimes had their accounts shut down, for example because they were documenting human rights abuses with violent scenes that violated the platform's policies, because they had been reported by a government, by the police or by people who disagreed with them, or even because Google did not understand their context well enough to make sense of what they were posting. If this happens to you, you can appeal the decision and ask to have your account restored. Review the instructions linked in this section for information on how to do this.
</details>

## Take a break from your account

- [Set an automatic responder on Gmail to inform people who write to you that you are "away"](https://support.google.com/mail/answer/25922).

<details><summary>Learn why we recommend this</summary>

If you want to stop reading and responding to emails for a while — for example because you need to take a break, but also because you suspect you may be detained or jailed — you can set an automatic responder. This can be useful also if you are facing harassment or defamation.
</details>
