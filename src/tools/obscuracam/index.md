---
weight: 999
title: ObscuraCam for Android
draft: true
post_date: 10 February 2015
status: unmaintained
logo: ../../../media/en/logos/obscuracam-logo.png
website: https://guardianproject.info/apps/obscuracam/
download_links: 
  - 
    label: Download link
    url: https://play.google.com/store/apps/details?id=org.witness.sscphase1
version: 2.0-RC2b
license: Free and Open Source Software
system_requirements: 
  - Android 2.3.3 or higher
---

**ObscuraCam** is a free camera application for Android devices, created by the [Guardian Project](https://guardianproject.info/), that has the ability to recognize and hide faces. It allows you to blur or delete the faces of those you photograph in order to protect their identities. 

# Required reading

- [Use mobile phones as securely as possible](../../mobile-phones)
- [Use smartphones as securely as possible](../../smartphones)

# What you will get from this guide

- The ability to **hide faces in photos taken with your Android device**.
- An easy way to **share or save these "obscured" photos**.

# 1. Introduction to ObscuraCam

- **ObscuraCam**'s automatic face-recognition does not always work, but you can easily select and obscure faces manually.
- On some versions of the Android operating system, the option to *delete the original media file* does not work. If you rely on this option, please check to make sure that you do not have **ObscuraCam** photos (with visible faces!) somewhere on your device.
- If you use **ObscuraCam** to send photos to yourself or to someone else, the tool **does not** provide additional protection (such as end-to-end encryption) for the photo in transit.
- A **tag** is an area in the photo which has been obscured by one of several methods **ObscuraCam** can use.



## 1.0 Other tools like ObscuraCam

Alternative software that lets you pixelate, blur or redact faces from images:

* Windows: [Gimp](http://www.gimp.org/), [Pinta](http://pinta-project.com/)

* OS X: [Gimp](http://www.gimp.org/), [Pinta](http://pinta-project.com/)

* Linux: [Gimp](http://www.gimp.org/), [Pinta](http://pinta-project.com/)


# 2. Install and configure ObscuraCam

## 2.1 Install ObscuraCam

**Step 1.** On your Android device, **download** and **install** the app from the [Google Play](https://play.google.com/store/apps/details?id=org.thoughtcrime.securesms) store by tapping ![](../../../media/en/obscuracam/obscuracam_001.png).

![](../../../media/en/obscuracam/obscuracam_002.png)

*Figure 1: K-9 on the Google Play Store*

**Step 2:**. Before the installation process begins, you will be prompted to review the access that the application will have on your phone. Review this carefully. Once your are happy with the permissions that will be granted, press ![](../../../media/en/obscuracam/obscuracam_003.png) and the installation will complete.  If you do not agree with the permissions that will be granted, press the back button and the installation will be cancelled.

![](../../../media/en/obscuracam/obscuracam_004.png)

*Figure 2: Permissions required*
**Step 3.** **Tap** *Open* to run the app for the first time

**Step 4:** Review the *Terms of use* and if you are happy with them press ![](../../../media/en/obscuracam/obscuracam_005.png) to be brought to the main screen of **ObscuraCam**. Otherwise press ![](../../../media/en/obscuracam/obscuracam_006.png) to stop using app.


# 3. Using ObscuraCam to obscure photos

You can use **ObscuraCam** to hide some or all of the faces that appear in your photos. It works on pictures that you take with the **ObscuraCam** app itself, but you can also blur faces in other photos, as long as you have a way to copy or move the image files onto your Android device.

## 3.1 Taking And Obscuring New Photos

To take a photo on your Android device without showing your subjects' faces, perform the following steps: 

**Step 1:** From the main screen of **ObscuraCam** *tap* ![](../../../media/en/obscuracam/obscuracam_007.png).

![](../../../media/en/obscuracam/obscuracam_008.png)

*Figure 3: ObscuraCam home screen*

**Step 2:** Your phone's camera app will open, take a photo as you normally would.

**Step 3:** A screen with a preview of the photo you took will be displayed. **Tap** ![](../../../media/en/obscuracam/obscuracam_009.png) to proceed with obscuring the photo, otherwise **tap** ![](../../../media/en/obscuracam/obscuracam_010.png) to discard the current photo and take another.

**Step 4:**ObscuraCam** will attempt to identify faces automatically. 

![](../../../media/en/obscuracam/obscuracam_011.png)

*Figure 4: ObscuraCam Detecting Faces* 


**Step 5:** If **ObscuraCam** can detect faces in the photo it will add an image "*tag*" (a rectangle used to select content that should be hidden) over the face and pixelate it. If **ObscuraCam** did not detect any faces or you need to obscure additional areas on the photo, tap on the area to add additional tags.

**Note:** To learn how to modify or add additional blurring or effects see  section [4. Modifying the "blur" mode]( #modifying-the-blur-mode) below.

![](../../../media/en/obscuracam/obscuracam_012.png)

*Figure 5: ObscuraCam - blurred face* 

**Step 6:** To see a preview of the photo you can tap ![](../../../media/en/obscuracam/obscuracam_013.png), to return to the edit screen tap anywhere on the preview image.

**Step 7:** To share the obscured photo via an existing application on your phone such as [K-9 Mail](../../k9/android) or [TextSecure](../../textsecure/android) tap ![](../../../media/en/obscuracam/obscuracam_014.png).

**Step 8:** To save the blurred photo to your phone tap ![](../../../media/en/obscuracam/obscuracam_015.png).  You will be asked whether you want to delete the original, unblurred, image from your phone, if you no longer need the original photo tap ![](../../../media/en/obscuracam/obscuracam_016.png) otherwise tap  ![](../../../media/en/obscuracam/obscuracam_017.png) to save a copy of both the obscured and unobscured photo.

![](../../../media/en/obscuracam/obscuracam_018.png)

*Figure 6: Confirm deletion of original image* 


## 3.2 Obscuring Existing Photos

As well as obscuring photos as you take them, **ObscuraCam** can also obscure any old photos on your phone or that you copy to your phone.

**Step 1:** From the main screen of **ObscuraCam** *tap* ![](../../../media/en/obscuracam/obscuracam_019.png).

**Step 2:** Browse to the file on your phone that you want to obscure.

**Step 3:** **ObscuraCam** will attempt to detect any faces in the image (See Figure 6) and pixelate them.

**Step 4:** If **ObscuraCam** did not detect any faces or you want to obscure additional faces or items in the photo tap the relevant area to add additional pixelation. 


**Note:** To learn how to modify or add additional blurring or effects see  section [Modifying the "blur" mode]( #modifying-the-blur-mode) below.

![](../../../media/en/obscuracam/obscuracam_012.png)

*Figure 7: ObscuraCam - blurred face* 

**Step 5:** To see a preview of the photo you can tap ![](../../../media/en/obscuracam/obscuracam_013.png), to return to the edit screen tap anywhere on the preview image.

**Step 6:** To share the obscured photo via an existing application on your phone such as [K-9 Mail](../../k9/android) or [TextSecure](../../textsecure/android) tap ![](../../../media/en/obscuracam/obscuracam_014.png).

**Step 7:** To save the blurred photo to your phone tap ![](../../../media/en/obscuracam/obscuracam_015.png).  You will be asked whether you want to delete the original, unblurred, image from your phone, if you no longer need the original photo tap ![](../../../media/en/obscuracam/obscuracam_016.png) otherwise tap  ![](../../../media/en/obscuracam/obscuracam_017.png) to save a copy of both the obscured and unobscured photo.


# 4. Modifying the blur mode





## 4.1 Resize or move the tag

While editing a photo in **ObscuraCam**, if you need to resize the tag, touch one of the four corners of the white outline box so that it turns green and drag it to make it larger or smaller.

If you need to move the tag so that it is in a better position on the photo, touch the centre of the pixelated area so that the surrounding white box turns green, then drag the box to the location you want.

![](../../../media/en/obscuracam/obscuracam_020.png)

*Figure 8: Pixelated tag ready for resizing or moving* 

## 4.2 Change the obscure type

ObscuraCam gives you a few options on how to obscure a photo.  From the edit screen tap on an existing obscured area  to bring up the obscuring options:

![](../../../media/en/obscuracam/obscuracam_021.png)

*Figure 9: Obscuring options* 


**Pixelate** will obscure any part of the photo inside the tag by pixelating it.

![](../../../media/en/obscuracam/obscuracam_022.png)

*Figure 10: Pixelated area* 

**Redact** will obscure any part of the photo inside the tag by placing a black box over it.

![](../../../media/en/obscuracam/obscuracam_023.png)

*Figure 11: Redacted area*

**Mask** will obscure any part of the photo inside the tag by placing a funny mask over the person.

![](../../../media/en/obscuracam/obscuracam_024.png)

*Figure 12: Mask* 

**Crowd Pixel** will obscure any part of the photo *outside* the tag by pixelating the it.

![](../../../media/en/obscuracam/obscuracam_025.png)

*Figure 13: Crowd Pixel* 

**Clear Tag** will remove the selected tag from the photo.


# 5. Obscuring Videos

**Obscuracam** can also obscure faces or objects in videos, however unlike with photos it can only obscure existing photos that you took with your phone or copied to your phone.

## 5.1 Automatic Obscuring of Videos

**Step 1:** From the main screen (*See figure 3*) of **Obscuracam** tap ![](../../../media/en/obscuracam/obscuracam_026.png).

**Step 2:** Select the video you with to obscure from your phones storage.

**Step 3** Tap ![](../../../media/en/obscuracam/obscuracam_027.png) when asked *Would you like to detect faces in this video*.

**Step 3:** The video will begin to play and **ObscuraCam** will attempt to detect faces.

**Note:** The process for detecting faces takes approximately *4 seconds* for every *1 second* of the video.

![](../../../media/en/obscuracam/obscuracam_028.png)

*Figure 14: Detecting faces* 

**Step 4:** Once the face detection has completed, you can tap ![](../../../media/en/obscuracam/obscuracam_029.png) to review the video. This will show you the pixelated tags over the faces as well as a green trail showing the path the tag takes through the video as the face moves.

![](../../../media/en/obscuracam/obscuracam_030.png)

*Figure 15: Detected faces*

**Step 5:** After reviewing the video and you are happy that all faces are obscured, tap ![](../../../media/en/obscuracam/obscuracam_031.png) at the top of the screen to save the video to your phones storage.

![](../../../media/en/obscuracam/obscuracam_032.png)

*Figure 16: Saving your video*

**Note:** If **ObscuraCam** did not properly obscure faces or failed to detect some, you can see how to manually obscure them in section [Manual Obscuring of Videos](#manual-obscuring-of-videos).

## 5.2 Manual Obscuring of Videos

If **ObscuraCam** was not able to detect faces properly during the automatic process or you would like more process over the obscuring process you can manually obscure video.

**Step 1:** From the main screen (*See figure 3*) of **Obscuracam** tap ![](../../../media/en/obscuracam/obscuracam_026.png).

**Step 2:** Select the video you with to obscure from your phones storage.

**Step 3:** Tap ![](../../../media/en/obscuracam/obscuracam_033.png) when asked *Would you like to detect faces in this video*.

**Step 4:** Tap ![](../../../media/en/obscuracam/obscuracam_029.png) to start playing the video.

**Step 5:**  While the video plays, touch and hold your finger on a face in the video to add a tag over it,  drag the tag around the screen with your finger following the movement of the face. Lift your finger off the screen to stop obscuring the video.
 
**Note:** as you follow the face on the screen you will see a green trail being left behind, this is the path the pixelated tag will take once the video is saved.  

**Step 6:** If there are additional faces in the video that you wish to obscure, touch and hold ![](../../../media/en/obscuracam/obscuracam_034.png) and move the slider back to the time point where the next face appears on video and repeat *step 5* above.

**Step 7:** Once you have finished manually obscuring faces, you can tap ![](../../../media/en/obscuracam/obscuracam_029.png) to review the video. This will show you the pixelated tags over the faces as well as a green trail  showing the path the tag takes through the video as the face moves.

**Step 8:** After reviewing the video and you are happy that all faces are obscured, tap ![](../../../media/en/obscuracam/obscuracam_031.png) at the top of the screen to save the video to your phones storage.

![](../../../media/en/obscuracam/obscuracam_035.png)

*Figure 17: Obscured section of video*

## 5.3 Changing Output Video Quality

**ObscuraCam** chooses a default video quality when saving that make the video small enough to easily share via email or other methods but does reduce resolution and quality of the video.  

To keep the obscured video in a higher resolution:

**Step 1:** After either [automatically](#automatic-obscuring-of-videos) or [manually](#manual-obscuring-of-videos) detecting faces and before saving the video, tap ![](../../../media/en/obscuracam/obscuracam_036.png) to bring up the menu and select ![](../../../media/en/obscuracam/obscuracam_037.png).

**Step 2:** Review and change the settings you want, where possibly **ObscuraCam** will give you the range by which each setting can be changed.

![](../../../media/en/obscuracam/obscuracam_038.png)

*Figure 18: Video output/saving preferences*

**Step 3:** Once you have made the desired output changes, tap the back button on your phone and save the video.
