---
title: Protect yourself and your data when using WhatsApp
weight: 105
collection: none
post_date: 2021
logo: ../../../media/en/logos/social_networking-logo.png
website: https://www.whatsapp.com
---

# Setup

## Know how hard it is to move what you have posted off social media

- Test how effective it is to use the "download my data" functions of each platform you use. Start the download process, then take a thorough look at what data it provides. You may find that what is downloaded is not in a format you find easy to use.
- Allow some time for this process. If you have had an account for a long time, there will be a lot of data to download, and the service may take a day or so to bundle it for your download.
- [Request your account information](https://faq.whatsapp.com/general/account-and-profile/how-to-request-your-account-information/)
- Export your chat history:
    - [Android](https://faq.whatsapp.com/android/chats/how-to-save-your-chat-history)
    - [Back up to Google Drive](https://faq.whatsapp.com/android/chats/how-to-back-up-to-google-drive)
    - [Back up to iCloud on iPhone](https://faq.whatsapp.com/iphone/chats/how-to-back-up-to-icloud)

<details><summary>Learn why we recommend this</summary>

Avoid relying on a social networking site as a primary host for your content, contacts, or other information.

Consider what you would lose access to if your government blocked a site or app. It is easy for governments to block access to social media within their boundaries if they object to what people are sharing. Social media services may also decide to remove objectionable content themselves, rather than face censorship in a particular country.

Social media might also remove content that they believe violates their policies about, for example, violent images or harassment. It is often difficult for them to understand the local context of what users have posted, particularly if it is not in English.
</details>

## Decide whether you will use a real or fake name, and maintain separate accounts

- Be aware that even if you provide a fake name to a social media site, you may still be identifiable by the network you connect from and the IP address it assigns to your device unless you [use a VPN or Tor to hide this information](../../communication/private-communication/).
    - [Use a VPN](../../communication/private-communication/) when setting up an account for the first time to make it harder for someone to associate your profile with your IP address.
- Consider using separate accounts or separate identities/pseudonyms for different campaigns and activities. You will likely want to keep your personal and work accounts separate, at the very least.
- Remember that the key to using a social network safely is being able to trust people in that network. You and the others in your network will want to know that the people behind the accounts are who they say they are, and have ways to validate this. That does not necessarily mean you have to use your real name, but it may be important to use consistent fake names.

<details><summary>Learn why we recommend this</summary>

Some people maintain social media accounts with fake names, or one account with their actual name and one with a fake name, to ensure they can organize and connect with others with less risk to their free speech, safety, or liberty.
</details>

## Set up with a fresh email address

- Set up a new e-mail address while using the [Tor Browser](../torbrowser/) or a [trusted VPN](../../communication/private-communication/).
- Suggested privacy-friendly mail services:
    - [Proton Mail](https://proton.me/)
    - If you have a friend who can invite you, [Riseup Mail](https://riseup.net/en/email/settings/mail-accounts)
    - [Autistici/Inventati](https://www.autistici.org/services/mail)

<details><summary>Learn why we recommend this</summary>

Email addresses provide one of the easiest ways to search for you: you need to provide one each time you set up a new account. If you really need to hide your identity, it is best to start over with a new social media account which you do not connect to your old accounts or to existing email addresses.
</details>

## Don’t associate your phone number with your account

- It isn't possible to set up WhatsApp without using a phone number you have access to.
- [If you have a dual SIM card phone, you can choose to register with your second SIM even if you do not use that number regularly](https://faq.whatsapp.com/general/verification/using-one-whatsapp-account-on-multiple-phones-or-with-multiple-phone-numbers)

<details><summary>Learn why we recommend this</summary>

Your phone number can be easily used to look you up and identify you. Consider whether your local law enforcement might make a legal request to social media companies to find the activity associated with your account, or whether someone seeking to harass or find you might make use of your number.
</details>

## Skip “find friends”

- WhatsApp requires access to your mobile's contact list in order to function. Be aware that WhatsApp will show that you are using their services to anyone who has your number in their contact list; there is nothing stopping anyone who knows your phone number from viewing your WhatsApp account. [See more information here.](https://faq.whatsapp.com/general/contacts/about-contact-upload/)
- [Change your privacy settings so that a limited group of people (or no one) can see your profile photo, status updates, and "about" information](https://faq.whatsapp.com/general/security-and-privacy/how-to-change-your-privacy-settings/)
- Consider only connecting to people you know, whom you trust not to misuse the information you post.
- If you need to connect to an online community of like-minded individuals whom you have never met, consider carefully what information you will share with these people.
- Do not share your employer or educational background, as social media may use this information to share your profile with others unexpectedly.

<details><summary>Learn why we recommend this</summary>

Social media often ensure they will gain in popularity by by using the contact lists in your devices and email accounts to find and recommend more people you might want to connect to. This can have dangerous effects when you want to keep your contacts hidden from others. Consider whether law enforcement in your area might use these contact lists to build a case against you and your colleagues if they confiscated your device or accessed your account. Or consider what might happen if social media revealed information about others you associate with to the public. If these are concerns for you, limit social media apps and sites' permissions to use your contacts.
</details>

## Designate someone to manage your account if you are unable to do it yourself

- WhatsApp cannot help recover data from or give access to an account because it is encrypted. If you would like to have a selected colleague or friend access or take over your account in the event you are incapacitated, you may need to share login information using your encrypted password manager.

<details><summary>Learn why we recommend this</summary>

This is something everyone should think about, regardless of their risk level. Social media sites have developed processes to handle situations where someone passes away or is seriously ill or jailed and others need to manage their account. Designating someone to care for your account can ensure others are notified of your situation, and prevent malicious people from defacing or hacking your account.
</details>

# Account protection

## Check recovery email and phone

- View your current number in Settings > your profile.
- Because WhatsApp accounts are tied to phone numbers, if someone previously had your phone number, their account information (like name and profile picture) may still be in WhatsApp's system. This does not mean they have access to your account. [Learn more here](https://faq.whatsapp.com/general/account-and-profile/seeing-your-phone-number-already-in-whatsapp)
- Change this information immediately if you lose access to your email address or phone number.

<details><summary>Learn why we recommend this</summary>

Your accounts use an email address and/or a phone number to help recover your account in case of authentication issue. The email address is also used to inform the user of any security related event. It is important to check this information to be sure that an attacker did not change them to gain control of your account later.
</details>

## Use strong passwords

- Use strong passwords to protect your accounts. Anyone who gets into your social media account gains access to a lot of information about you and anyone you are connected to. See our guide on [how to create and maintain secure passwords](../../passwords) for more information.

## Set up multifactor authentication (2FA)

- [Use a security key, authenticator app, or security codes for multifactor authentication.](https://faq.whatsapp.com/general/verification/about-two-step-verification)
- Do not use SMS or a phone call if possible, so you do not have to associate your phone number with your account. This is particularly important if your name is not already associated with your account.

<details><summary>Learn why we recommend this</summary>

[See our guide to passwords and other login protections](../../passwords/passwords-and-2fa/) for more on why and how to set up multifactor authentication, sometimes known as 2FA or MFA.
</details>

## Get a verification code to get back into your account

- [Get verification codes](https://faq.whatsapp.com/general/im-traveling-and-i-cant-get-my-whatsapp-code)
- Store those codes in your password manager.
- Alternately, print these codes out before you are in a situation where you might need them. Keep them somewhere safe and hidden, like your wallet or a locked safe.

<details><summary>Learn why we recommend this</summary>

Having verification codes written down or printed out gives you another way to get back into your account if you lose access. If you are traveling, this can be especially useful when you need to get into your accounts and may not have access to wifi or cellular data to use other multifactor authentication.
</details>

## If your device is lost or stolen

- [Take these actions](https://faq.whatsapp.com/general/account-and-profile/lost-and-stolen-phones)

# Look for suspicious access

## Check active sessions and authorized devices, review account activity and security events

- Look at the following pages listing which devices have recently logged in to your account (including using browsers or apps). Does every login look familiar?
    - [View devices where your account is logged in, and log out of any you do not recognize](https://faq.whatsapp.com/web/download-and-installation/how-to-log-in-or-out)
    - Look for instructions on how to log out devices that are not yours.
- Note that if you are using a VPN or Tor Browser, which can conceal your location, you may see your own device, connected in unexpected locations.
- Look for instructions on how to log out devices that are not yours.
- If you see suspicious activity on your account, immediately change your password to a new, long, random passphrase you do not use for any other accounts. Save this in your password manager.

<details><summary>Learn why we recommend this</summary>

Governments, police, domestic abusers, and other adversaries may find ways to watch your accounts by logging in from their devices. If they do so, it is possible you will be able to see it from these pages where social media services show which devices have been used to log in to your accounts.
</details>

## Get notified about logins

- Set notifications to be sent to the email address you associated with this account.
- Avoid using your phone number for notifications (see "avoid associating your phone number with accounts," above).
- WhatsApp automatically sends notifications when another device tries to log in to your account. [See these instructions if you have received such a notification you were not expecting.](https://faq.whatsapp.com/general/received-verification-code-without-requesting-it)

<details><summary>Learn why we recommend this</summary>

If you suspect your account may be watched, or your adversaries may break into it, use this feature of social media accounts to be notified right away when it happens.
</details>

## If you think your account has been hacked

- [Take steps to protect against your account being stolen or recover it once it has been](https://faq.whatsapp.com/general/account-and-profile/stolen-accounts)

## Download data for further analysis (advanced)

- [Request to download your data](https://faq.whatsapp.com/general/account-and-profile/how-to-request-your-account-information)
- [Suggestions of what to look for](https://guides.securitywithoutborders.org/guide-to-account-check-up/facebook/login.html)

<details><summary>Learn why we recommend this</summary>

If you suspect someone has intruded on your device, you might want to download all records of activity on your account, so you or your technical support person can look for unusual activity.
</details>

# Decide what to post

The more information about yourself you reveal online, the easier it becomes for the authorities and others to identify you and monitor your activities. For example, if you share (or "like") a page that opposes some position taken by your government, agents of that government might very well take an interest and target you for additional surveillance or direct persecution. This can have consequences even for those not living under authoritarian regimes: the families of some activists who have left their home countries have been targeted by the authorities in their homelands because of things those activists have posted on social media.

Information that should never be sent on social media, even via direct message (DM)

- Passwords
- Personally identifying information, including
    - your birthday
    - your phone number (does it appear in screenshots of communications?)
    - government or other ID numbers
    - medical records
    - education and employment history (these can be used by untrustworthy people who want to gain your confidence)

Information that you might not want to post on social media, depending on your assessment of the threats in your region:

- Your email address (at least consider having more- and less-sensitive accounts)
- Details about family members
- Your sexual orientation or activity

- Even if you trust the people in your networks, remember it is easy for someone to copy your information and spread it more widely than you want it to be.
- Agree with your network on what you do and do not want shared, for safety reasons.
- Think about what you may be revealing about your friends that they may not want other people to know; be sensitive about this, and ask them to be sensitive about what they reveal about you.

## Don’t share location

- [Turn off "Live Location" in the privacy settings described here](https://faq.whatsapp.com/general/security-and-privacy/how-to-change-your-privacy-settings/)

<details><summary>Learn why we recommend this</summary>

If you are worried about someone finding you and doing you physical harm, stop your accounts from storing your location information. Turning off location services on your device also makes your mobile device's battery charge last longer.
</details>

## Share photos and videos more safely

- Consider what is visible in photos you post. Never post images that include
    - your vehicle license plates
    - IDs, credit cards, or financial information
    - Photographs of keys ([it is possible to duplicate a key from a photo](https://www.cnet.com/news/duplicating-keys-from-a-photograph/))
- Think hard before you post pictures that include or make it possible to identify
    - your friends, colleagues, and loved ones (ask permission before posting)
    - your home, your office, or other locations where you often spend time
    - if you are hiding your location, other identifiable locations in the background (buildings, trees, natural landscape features, etc)
- [Stop WhatsApp from automatically saving images you receive to your device on Android](https://faq.whatsapp.com/android/how-to-stop-saving-whatsapp-media-to-your-phones-gallery)
- [You may want to turn off your profile photo in your privacy settings](https://faq.whatsapp.com/general/security-and-privacy/how-to-change-your-privacy-settings/)
- [Remove EXIF data before you post photos](https://www.howtogeek.com/203592/what-is-exif-data-and-how-to-remove-it/)

<details><summary>Learn why we recommend this</summary>

What you share could put yourself or others at risk. Get in the habit of seeking consent before posting about others, where possible. You may want to work with your colleagues to set guidelines for what you will and won't share publicly, under what conditions.

Photos and videos can reveal a lot of information unintentionally, particularly what is in the background. Many cameras also embed hidden data (metadata or EXIF tags) about the location, date, and time the photo was taken, the camera that took the photo, etc. Social media may publish this information when you upload photos or video.
</details>

# Decide who can see

## Share to select people

- Be aware that WhatsApp will show that you are using their services to anyone who has your number in their contact list; there is nothing stopping anyone who knows your phone number from viewing your WhatsApp account. [See more information here](https://faq.whatsapp.com/general/contacts/about-contact-upload/). For this and other reasons, we discourage people from using WhatsApp.
- If you do use WhatsApp, [change your privacy settings so that a limited group of people (or no one) can see your profile photo, status updates, and "about" information, or add you to groups](https://faq.whatsapp.com/general/security-and-privacy/how-to-change-your-privacy-settings/)

## Manage who can reply to what you post

- [Block contacts you do not want to hear from](https://faq.whatsapp.com/android/security-and-privacy/how-to-block-and-unblock-a-contact)

<details><summary>Learn why we recommend this</summary>

In some cases, replies to posts have been used to build false claims of human rights defenders associating with people they did not actually associate with. Replies can also be used to harass you. Controlling who can reply can help lower your stress levels.
</details>

## Think about group membership and who you connect with

- WhatsApp allows anyone to add you to a group by default. [Change this setting to "My contacts" (or "My contacts except...") under the Privacy > Groups setting described here](https://faq.whatsapp.com/general/security-and-privacy/how-to-change-your-privacy-settings/).
- [Exit a group and block its administrator](https://faq.whatsapp.com/general/how-to-block-a-group-admin)'

<details><summary>Learn why we recommend this</summary>

When you join or start a community or group online it is revealing something about you to others. People may assume that you support or agree with what the group is saying or doing, which could make you vulnerable if you are seen to align yourself with particular political groups, for example. In some countries, connections on social media to individuals or groups have been used in court to make a case against someone, even when the two people were only loosely connected.

If you set up a group and people choose to join it, consider: what are they announcing to the world by doing so? For example, if it is a LGBTQI support group, will that affiliation bring dangers for members in your region? Consider the impact of visibility in your current moment. There may be times when it is valuable for your movement to be visible, and even at that moment people who want your support might need a way to connect with your group without being identified. Think strategically about the platforms where you create your groups, what you name them (would a coded name help, as it did the Mattachine Society or the Daughters of Bilitis gay and lesbian organizations in the 1950s?), and whether they are public or private.

If you join a large group with members that you don't know, be aware that adversaries might also join groups or make connections to identify you or your colleagues, get a better view of what you are doing, or even build false trust. If you suspect this is likely to happen, it is important to choose connections and post selectively when you make an account connected to your work.
</details>

## Limit who can contact you

- [Android: block or unblock an account](https://faq.whatsapp.com/android/security-and-privacy/how-to-block-and-unblock-a-contact)
- [iPhone: block or unblock an account](https://faq.whatsapp.com/iphone/security-and-privacy/how-to-block-and-unblock-contacts)
- [Exit a group and block its administrator](https://faq.whatsapp.com/general/how-to-block-a-group-admin)

<details><summary>Learn why we recommend this</summary>

Limiting who can contact you can lessen the likelihood that you will be found when you are trying to be private, or targeted by people trying to falsely gain your trust or the trust of your network. This can also be useful if you are being harassed in non-public messages.
</details>

## Manage advertising

- [Be aware that if you use WhatsApp to interact with businesses using WhatsApp Shops or Facebook Shops, those shops may store data about you to advertise to you](https://faq.whatsapp.com/general/security-and-privacy/how-is-your-data-used-with-facebook-shops)

<details><summary>Learn why we recommend this</summary>

There is a possibility governments or police forces might buy advertising data from social media companies to target you and your network with disinformation, or try to find you.
</details>

## Learn what social media will turn over to governments or law enforcement

- Search for "WhatsApp" and the name of your country or jurisdiction on [Lumen](https://lumendatabase.org/)
- [Learn how Whatsapp responds to law enforcement requests](https://faq.whatsapp.com/general/security-and-privacy/information-for-law-enforcement-authorities)

<details><summary>Learn why we recommend this</summary>

Social media sites may give your information, including information you were trying to keep private, to governments or law enforcement if requested. Look through the following links to learn more about the conditions under which they will do so.
</details>

# Leave no trace

## Precautions when using a public or shared device

- Avoid accessing your social network account from shared devices (like an internet cafe or other people's devices).
- Delete your password and browsing history when you use a web browser on a public machine. Change the passwords of any accounts you accessed from shared devices as soon as you can, using your own device.

# Handle abuse

## Report abuse

- [Look for "Advanced safety and security features" > "Report issues to WhatsApp" here](https://faq.whatsapp.com/general/security-and-privacy/staying-safe-on-whatsapp)
- [Read more about reporting hoaxes](https://faq.whatsapp.com/general/security-and-privacy/hoax-messages/)

<details><summary>Learn why we recommend this</summary>

Social media have unfortunately become a favorite method of harassment and disinformation worldwide. If you see malicious impersonation, hashtags being flooded, disinformation being spread, or you or your colleagues are being targeted and harassed, you are not alone and there may be help. Review the processes for reporting using the following links.
</details>

## Report harassment that reveals information about you

- [Look for "Advanced safety and security features" > "Report issues to WhatsApp" here](https://faq.whatsapp.com/general/security-and-privacy/staying-safe-on-whatsapp)

<details><summary>Learn why we recommend this</summary>

Some abusers may try to target you by revealing information about where you live or work, your family or friends, or other personal details including images. In many cases you have a right to have this taken down, even if that information is true. Review the following links for information on how to get that information removed.
</details>

## Identify and report coordinated inauthentic activity (botnets and spam)

- [Look for "Advanced safety and security features" > "Report issues to WhatsApp" here](https://faq.whatsapp.com/general/security-and-privacy/staying-safe-on-whatsapp)
- [Read more about WhatsApp spam](https://faq.whatsapp.com/general/security-and-privacy/unauthorized-use-of-automated-or-bulk-messaging-on-whatsapp/)

<details><summary>Learn why we recommend this</summary>

Some harassment and disinformation is posted through automated means, rather than by individual people. If you suspect that you are seeing this "coordinated inauthentic activity," you can report it to the social media sites and they may ban those automated systems. While automation can be hard to prove, there are some cases in which reporting coordinated inauthentic activity might be more successful than reporting harassment, if you suspect the social media site will not understand the context of the harassment.
</details>

## Report impersonation

- [Look for "Advanced safety and security features" > "Report issues to WhatsApp" here](https://faq.whatsapp.com/general/security-and-privacy/staying-safe-on-whatsapp)

<details><summary>Learn why we recommend this</summary>

Impersonation in the form of parody is usually accepted as free speech by most social media platforms, and will not be removed. However, impersonation for the purposes of defamation of character may not be, and you can report it.
</details>

## Hide stressful content

- [Block contacts you do not want to hear from](https://faq.whatsapp.com/android/security-and-privacy/how-to-block-and-unblock-a-contact)

<details><summary>Learn why we recommend this</summary>

Any of us may find some content more distressing than other people do, whether it be information on the death of a friend, public arguments which devalue us because of who we are, or frightening events in the news. If you need a break from this stress, here are some tools which can help hide content you do not wish to see, for as long as you wish.
</details>

## Learn how to recover your account if it is disabled or suspended

- [If you see a message that says you are banned](https://faq.whatsapp.com/general/account-and-profile/about-account-bans)
- [Contact support](https://www.whatsapp.com/contact/?subject=messenger)

<details><summary>Learn why we recommend this</summary>

For one reason or another, social media sites will sometimes disable an account. Human rights defenders have sometimes had their accounts shut down because they are documenting human rights abuses with violent scenes that violate the social media platforms's policies; because they have been reported by government, police, or other people who disagree with them; or even because the social media platform does not understand their context well enough to make sense of what they are posting. If this happens to you, you can appeal the decision and ask to have your account restored. Review the links below for information on how.
</details>

## Take a break from your account

- WhatsApp does not support suspension or auto-response, but consider backing up your messages and deleting the app from your phone if you will still have access to the same phone number. Or, if you just want to let people know you will be away, put a note in your status message.

<details><summary>Learn why we recommend this</summary>

If you want to stop people from posting to your account because you will not be able to access it for a while-- you suspect you may be detained or jailed, or just because you need to take a break!--you may be able to temporarily deactivate your account on some social media. This can be useful if you face harassment or defamation. On other accounts, like email, you may not be able to stop incoming messages, but can set your account to automatically respond that you are away.
</details>

# Learn how social media use your information

- For a useful add-on which clarifies the Terms of Service of many popular sites, see [Terms of Service; Didn't Read](http://tosdr.org).
- [See WhatsApp's tems of service](https://www.whatsapp.com/legal/privacy-policy)

<details><summary>Learn why we recommend this</summary>

It is often unclear what social media will do with your information when you share it. Are they combined with other data to guess things about you? Are they sold to other companies that may share that information even if you did not want it to be shared? Read the End User Licence Agreement and Privacy Policy or Data Use Policy for social media sites to find out.
</details>

# Further reading

- Consumer Reports has a guide on [how to use WhatsApp privacy settings](https://www.consumerreports.org/privacy/how-to-use-whatsapp-privacy-settings-a5254545737/)
