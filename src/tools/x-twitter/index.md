---
title: Protect yourself and your data when using Twitter
weight: 104
collection: none
post_date: 2021
logo: ../../../media/en/logos/social_networking-logo.png
website: https://twitter.com
---

# Setup

## Know how hard it is to move what you have posted off social media

- Test how effective it is to use the "download my data" functions of each platform you use. Start the download process, then take a thorough look at what data it provides. You may find that what is downloaded is not in a format you find easy to use.
- Allow some time for this process. If you have had an account for a long time, there will be a lot of data to download, and the service may take a day or so to bundle it for your download.
- [Request your account information](https://help.twitter.com/en/managing-your-account/how-to-download-your-twitter-archive)

<details><summary>Learn why we recommend this</summary>

Avoid relying on a social networking site as a primary host for your content, contacts, or other information.

Consider what you would lose access to if your government blocked a site or app. It is easy for governments to block access to social media within their boundaries if they object to what people are sharing. Social media services may also decide to remove objectionable content themselves, rather than face censorship in a particular country.

Social media might also remove content that they believe violates their policies about, for example, violent images or harassment. It is often difficult for them to understand the local context of what users have posted, particularly if it is not in English.
</details>

## Decide whether you will use a real or fake name, and maintain separate accounts

- Be aware that even if you provide a fake name to a social media site, you may still be identifiable by the network you connect from and the IP address it assigns to your device unless you [use a VPN or Tor to hide this information](../../communication/private-communication/).
    - [Use a VPN](../../communication/private-communication/) when setting up an account for the first time to make it harder for someone to associate your profile with your IP address.
- Consider using separate accounts or separate identities/pseudonyms for different campaigns and activities. You will likely want to keep your personal and work accounts separate, at the very least.
- Remember that the key to using a social network safely is being able to trust people in that network. You and the others in your network will want to know that the people behind the accounts are who they say they are, and have ways to validate this. That does not necessarily mean you have to use your real name, but it may be important to use consistent fake names.

<details><summary>Learn why we recommend this</summary>
Some people maintain social media accounts with fake names, or one account with their actual name and one with a fake name, to ensure they can organize and connect with others with less risk to their free speech, safety, or liberty.
</details>

## Set up with a fresh email address

- Set up a new e-mail address while using the [Tor Browser](../torbrowser/) or a [trusted VPN](../../communication/private-communication/).
- Suggested privacy-friendly mail services:
    - [Proton Mail](https://proton.me/)
    - If you have a friend who can invite you, [Riseup Mail](https://riseup.net/en/email/settings/mail-accounts)
    - [Autistici](https://www.autistici.org/services/mail)

<details><summary>Learn why we recommend this</summary>

Email addresses provide one of the easiest ways to search for you: you need to provide one each time you set up a new account. If you really need to hide your identity, it is best to start over with a new social media account which you do not connect to your old accounts or to existing email addresses.
</details>


## Don’t associate your phone number with your account

- If possible, when setting up a new account, do not enter your phone number. Twitter will allow you to set up a new account with only an email address, and that can be harder to associate with you.
- Set your account up with a fresh email address instead of a phone number.
- If you have already set your account up with your phone number, [remove it or limit who can find you using your email address or phone number](https://help.twitter.com/en/safety-and-security/email-and-phone-discoverability-settings)

<details><summary>Learn why we recommend this</summary>

Your phone number can be easily used to look you up and identify you. Consider whether your local law enforcement might make a legal request to social media companies to find the activity associated with your account, or whether someone seeking to harass or find you might make use of your number.
</details>

## Skip “find friends”

- "Skip" or dismiss Twitter's requests to "allow access" to contacts on your device, import your contacts, connect with your other social media accounts, or find more connections when you first set up an account. You can select your contacts more carefully afterwards.
- [See instructions here for removing contacts and turning off contacts syncing](https://help.twitter.com/en/using-twitter/upload-your-contacts-to-search-for-friends)
- [Limit who can find you using your email address or phone number](https://help.twitter.com/en/safety-and-security/email-and-phone-discoverability-settings)
- Consider only connecting to people you know, whom you trust not to misuse the information you post.
- If you need to connect to an online community of like-minded individuals whom you have never met, consider carefully what information you will share with these people.
- Do not share your employer or educational background, as social media may use this information to share your profile with others unexpectedly.

<details><summary>Learn why we recommend this</summary>

Social media often ensure they will gain in popularity by by using the contact lists in your devices and email accounts to find and recommend more people you might want to connect to. This can have dangerous effects when you want to keep your contacts hidden from others. Consider whether law enforcement in your area might use these contact lists to build a case against you and your colleagues if they confiscated your device or accessed your account. Or consider what might happen if social media revealed information about others you associate with to the public. If these are concerns for you, limit social media apps and sites' permissions to use your contacts.
</details>


## Designate someone to manage your account if you are unable to do it yourself

- Note that for legal reasons, Twitter will not give a loved one or colleague access to your account. Rather, if you give them permission, they will make it possible to close and "memorialize" your account. If you expect someone else will need to access your account in the event of an emergency, arrange to share your login information with them using encrypted communication or the sharing function of your password manager.
- [Review the requirements for removing a deceased or incapacitated user's account](https://help.twitter.com/en/rules-and-policies/contact-twitter-about-a-deceased-family-members-account)
- If a colleague has been arrested or detained, contact [Access Now's Helpline](https://www.accessnow.org/help/) or [Front Line Defenders](https://www.frontlinedefenders.org/emergency-contact) for assistance in working with Twitter to secure access to their accounts.

<details><summary>Learn why we recommend this</summary>

This is something everyone should think about, regardless of their risk level. Social media sites have developed processes to handle situations where someone passes away or is seriously ill or jailed and others need to manage their account. Designating someone to care for your account can ensure others are notified of your situation, and prevent malicious people from defacing or hacking your account.
</details>

# Account protection

## Check recovery email and phone

Check:
- [Recovery email](https://help.twitter.com/managing-your-account/how-to-update-your-email-address)
- [Recovery phone](https://help.twitter.com/managing-your-account/how-to-update-your-account-phone-number)
- Change this information immediately if you lose access to your email address or phone number.

<details><summary>Learn why we recommend this</summary>

Your accounts use an email address and/or a phone number to help recover your account in case of authentication issue. The email address is also used to inform the user of any security related event. It is important to check this information to be sure that an attacker did not change them to gain control of your account later.
</details>

## Use strong passwords

- Use strong passwords to protect your accounts. Anyone who gets into your social media account gains access to a lot of information about you and anyone you are connected to. See our guide on [how to create and maintain secure passwords](../../passwords) for more information.


## Set up multifactor authentication (2FA)

- Use a security key, authenticator app, or security codes for multifactor authentication.
- Do not use SMS or a phone call if possible, so you do not have to associate your phone number with your account. This is particularly important if your name is not already associated with your account.
- [Set up authentication](https://help.twitter.com/en/managing-your-account/two-factor-authentication)
- Select "Require personal information to reset my password" in your [Account](https://twitter.com/settings/account) settings.

<details><summary>Learn why we recommend this</summary>

[See our guide to passwords and other login protections](../../passwords/passwords-and-2fa/) for more on why and how to set up multifactor authentication, sometimes known as 2FA or MFA.
</details>

## Get a verification code to get back into your account

- [Use the login code generator in the Twitter app on your mobile](https://help.twitter.com/managing-your-account/issues-with-login-authentication)
- Store those codes in your password manager.
- Alternately, print these codes out before you are in a situation where you might need them. Keep them somewhere safe and hidden, like your wallet or a locked safe.

<details><summary>Learn why we recommend this</summary>

Having verification codes written down or printed out gives you another way to get back into your account if you lose access. If you are traveling, this can be especially useful when you need to get into your accounts and may not have access to wifi or cellular data to use other multifactor authentication.
</details>

## If your device is lost or stolen

- [See "I don’t have my phone. How can I log out of the Twitter app?"](https://help.twitter.com/managing-your-account/phone-number-faqs)


##  Compare emails you may have received about security to those the app or service says it sent you

- Twitter does not provide a list of alerts it may have sent you, but suggests evaluating whether an email is real using [these guidelines](https://help.twitter.com/en/safety-and-security/fake-twitter-emails)

<details><summary>Learn why we recommend this</summary>

Phishing messages might try to convince you they are coming from your social media, to trick you into giving someone else access to your account. If you get a security email or text from a social media site, don't click on any of its links. Also, do not provide your password. Instead, log in to your account and check the following links to confirm whether the message was legitimate.
</details>

# Look for suspicious access

## Check active sessions and authorized devices, review account activity and security events

- Look at the following pages listing [which devices have recently logged in to your account (including using browsers or apps).](https://twitter.com/settings/apps_and_sessions)
- [Does every login look familiar?](https://twitter.com/settings/your_twitter_data/login_history)
    - Look for instructions on how to log out devices that are not yours.
    - Note that if you are using a VPN or Tor Browser, which can conceal your location, you may see your own device, connected in unexpected locations.
- If you see suspicious activity on your account, immediately change your password to a new, long, random passphrase you do not use for any other accounts. Save this in your password manager.

<details><summary>Learn why we recommend this</summary>

Governments, police, domestic abusers, and other adversaries may find ways to watch your accounts by logging in from their devices. If they do so, it is possible you will be able to see it from these pages where social media services show which devices have been used to log in to your accounts.
</details>

## Get notified about logins

- Set notifications to be sent to the email address you associated with this account.
- Avoid using your phone number for notifications (see "avoid associating your phone number with accounts," above).
- [Twitter automatically sends notifications of logins](https://help.twitter.com/en/safety-and-security/account-security-tips) from new, unknown devices to your recovery email and/or app

<details><summary>Learn why we recommend this</summary>

If you suspect your account may be watched, or your adversaries may break into it, use this feature of social media accounts to be notified right away when it happens.
</details>

## If you think your account has been hacked

- [Learn to recognize a hacked account](https://help.twitter.com/safety-and-security/twitter-account-compromised) and [take steps to remedy it](https://help.twitter.com/safety-and-security/twitter-account-compromised)


## Review other sites and apps that can access your account

- Avoid using your accounts to log in to other sites (like news sites, etc.) It is convenient, but that means it is convenient for attackers as well as for you, and may also leave more evidence of what you have viewed online. Use a different password for every site, and save it in your password manager.
- Be careful when connecting your social network accounts. You may be anonymous on one site, but exposed when using another.
- [Follow these instructions for apps you don't recognize. Change app passwords if you need to.](https://help.twitter.com/en/managing-your-account/connect-or-revoke-access-to-third-party-apps)

<details><summary>Learn why we recommend this</summary>

Most social networks allow you to integrate information with other social networks. For example, you can post an update on your Twitter account and have it automatically posted on your Facebook account as well. When other sites and apps have access, they can also be used by hackers to get into your social media accounts.
</details>


## Download data for further analysis (advanced)

- [Suggestions of what to look for](https://guides.securitywithoutborders.org/guide-to-account-check-up/facebook/login.html)
- [Access your data here](https://help.twitter.com/en/managing-your-account/accessing-your-twitter-data)

<details><summary>Learn why we recommend this</summary>

If you suspect someone has intruded on your device, you might want to download all records of activity on your account, so you or your technical support person can look for unusual activity.
</details>

# Decide what to post

The more information about yourself you reveal online, the easier it becomes for the authorities and others to identify you and monitor your activities. For example, if you share (or "like") a page that opposes some position taken by your government, agents of that government might very well take an interest and target you for additional surveillance or direct persecution. This can have consequences even for those not living under authoritarian regimes: the families of some activists who have left their home countries have been targeted by the authorities in their homelands because of things those activists have posted on social media.

Information that should never be sent on social media, even via direct message (DM)

- Passwords
- Personally identifying information, including
    - your birthday
    - your phone number (does it appear in screenshots of communications?)
    - government or other ID numbers
    - medical records
    - education and employment history (these can be used by untrustworthy people who want to gain your confidence)

Information that you might not want to post on social media, depending on your assessment of the threats in your region:

- Your email address (at least consider having more- and less-sensitive accounts)
- Details about family members
- Your sexual orientation or activity
- Even if you trust the people in your networks, remember it is easy for someone to copy your information and spread it more widely than you want it to be.
- Agree with your network on what you do and do not want shared, for safety reasons.
- Think about what you may be revealing about your friends that they may not want other people to know; be sensitive about this, and ask them to be sensitive about what they reveal about you.

## Don’t share location

- [Make sure the setting described here is turned off](https://help.twitter.com/en/using-twitter/tweet-location)

<details><summary>Learn why we recommend this</summary>

If you are worried about someone finding you and doing you physical harm, stop your accounts from storing your location information. Turning off location services on your device also makes your mobile device's battery charge last longer.
</details>

## Share photos and videos more safely

- Consider what is visible in photos you post. Never post images that include
    - your vehicle license plates
    - IDs, credit cards, or financial information
    - Photographs of keys ([it is possible to duplicate a key from a photo](https://www.cnet.com/news/duplicating-keys-from-a-photograph/))
- Think hard before you post pictures that include or make it possible to identify
    - your friends, colleagues, and loved ones (ask permission before posting)
    - your home, your office, or other locations where you often spend time
    - if you are hiding your location, other identifiable locations in the background (buildings, trees, natural landscape features, etc)
- [Remove EXIF data before you post photos](https://www.howtogeek.com/203592/what-is-exif-data-and-how-to-remove-it/)

<details><summary>Learn why we recommend this</summary>

What you share could put yourself or others at risk. Get in the habit of seeking consent before posting about others, where possible. You may want to work with your colleagues to set guidelines for what you will and won't share publicly, under what conditions.

Photos and videos can reveal a lot of information unintentionally, particularly what is in the background. Many cameras also embed hidden data (metadata or EXIF tags) about the location, date, and time the photo was taken, the camera that took the photo, etc. Social media may publish this information when you upload photos or video.
</details>

## Change who can see when you "like" things

 - Your likes are always visible unless your account is set to private. It may be best to not use the like button.

 <details><summary>Learn why we recommend this</summary>

"Likes" and other ways you appreciate posts are sometimes shared by social media in ways that are not clear. In some countries, "likes" or other apparently harmless interaction with other social media accounts have been used in legal cases. You may want to use caution when leaving icon or comment reactions to others' content, depending on your understanding of the legal situation in your country.
</details>


## Don't share your birthday

- It is always possible to give a birthday that is not your own. However, do keep track of what you enter, in case you need it to regain access to your account.
- [Remove your birthday here if you have previously added it](https://help.twitter.com/en/managing-your-account/how-to-customize-your-profile)

<details><summary>Learn why we recommend this</summary>

If you include your actual birthday in your account information, it can be used to identify you.
</details>


# Decide who can see

## Share to select people

- [Learn how to protect your account's tweets](https://help.twitter.com/en/safety-and-security/how-to-make-twitter-private-and-public)
- Note that if you previously had a public account, your past tweets can still be found on archive websites. You may want to start a new account if for plausible deniability.
- [Learn more here about public and protected accounts.](https://help.twitter.com/en/safety-and-security/public-and-protected-tweets)


## Manage who can reply to what you post

- When you post, [follow instructions here](https://help.twitter.com/en/using-twitter/twitter-conversations) to set "Choose who can reply to your tweet" to "Only people you mention" or "People you follow," depending on your preference

<details><summary>Learn why we recommend this</summary>

In some cases, replies to posts have been used to build false claims of human rights defenders associating with people they did not actually associate with. Replies can also be used to harass you. Controlling who can reply can help lower your stress levels.
</details>


## Think about group membership and who you connect with

- [Unfollow people](https://help.twitter.com/using-twitter/how-to-unfollow-on-twitter)
- [Deny requests to follow you](https://help.twitter.com/using-twitter/follow-requests)
- [Manage lists that you make or that someone has put you on](https://help.twitter.com/using-twitter/twitter-lists)

<details><summary>Learn why we recommend this</summary>

When you join or start a community or group online it is revealing something about you to others. People may assume that you support or agree with what the group is saying or doing, which could make you vulnerable if you are seen to align yourself with particular political groups, for example. In some countries, connections on social media to individuals or groups have been used in court to make a case against someone, even when the two people were only loosely connected.

If you set up a group and people choose to join it, consider: what are they announcing to the world by doing so? For example, if it is a LGBTQI support group, will that affiliation bring dangers for members in your region? Consider the impact of visibility in your current moment. There may be times when it is valuable for your movement to be visible, and even at that moment people who want your support might need a way to connect with your group without being identified. Think strategically about the platforms where you create your groups, what you name them (would a coded name help, as it did the Mattachine Society or the Daughters of Bilitis gay and lesbian organizations in the 1950s?), and whether they are public or private.

If you join a large group with members that you don't know, be aware that adversaries might also join groups or make connections to identify you or your colleagues, get a better view of what you are doing, or even build false trust. If you suspect this is likely to happen, it is important to choose connections and post selectively when you make an account connected to your work.
</details>


## Review tags/disallow tags

- Look for "How do I remove my name or username from a photo I’m tagged in? and "I don't want to be tagged in photos" [settings here](https://help.twitter.com/en/using-twitter/tweeting-gifs-and-pictures#tag-settings)

<details><summary>Learn why we recommend this</summary>

As with comments and other connections, tags could be used by others to make false accusations about your activities in a case against you.
</details>

## Limit who can contact you

- [Block accounts so they cannot contact you](https://help.twitter.com/en/using-twitter/blocking-and-unblocking-accounts)

<details><summary>Learn why we recommend this</summary>

Limiting who can contact you can lessen the likelihood that you will be found when you are trying to be private, or targeted by people trying to falsely gain your trust or the trust of your network. This can also be useful if you are being harassed in non-public messages.
</details>

## Manage advertising

- [Turn off "Personalization and Data"](https://twitter.com/settings/account/personalization)

<details><summary>Learn why we recommend this</summary>

There is a possibility governments or police forces might buy advertising data from social media companies to target you and your network with disinformation, or try to find you.
</details>

## Learn what social media will turn over to governments or law enforcement

- Search for "Twitter" and the name of your country or jurisdiction on [Lumen](https://lumendatabase.org/)
- See these Twitter links:
    - [Twitter's transparency reports](https://transparency.twitter.com/)
    - [How Twitter responds to law enforcement requests](https://help.twitter.com/en/rules-and-policies/twitter-law-enforcement-support#)
    - [Overviews of state-backed attempts to manipulate Twitter](https://transparency.twitter.com/reports/information-operations.html)
    - [Overviews of](https://transparency.twitter.com/en/reports/removal-requests.html#2020-jan-jun) [government requests to remove content from Twitter](https://transparency.twitter.com/en/reports/removal-requests.html#2020-jan-jun)
    - [Overviews of non-governmental requests for information about Twitter accounts (including lawsuits, etc)](https://transparency.twitter.com/reports/information-requests.html#2020-jan-jun)

<details><summary>Learn why we recommend this</summary>

Social media sites may give your information, including information you were trying to keep private, to governments or law enforcement if requested. Look through the following links to learn more about the conditions under which they will do so.
</details>

# Leave no trace

## Precautions when using a public or shared device

- Avoid accessing your social network account from shared devices (like an internet cafe or other people's devices).
- Delete your password and browsing history when you use a web browser on a public machine. Change the passwords of any accounts you accessed from shared devices as soon as you can, using your own device.


# Handle abuse

## Report abuse

- [Learn more about how](https://help.twitter.com/en/safety-and-security/report-abusive-behavior)
- [Fill out this form](https://help.twitter.com/en/forms/safety-and-sensitive-content/abuse)

<details><summary>Learn why we recommend this</summary>

Social media have unfortunately become a favorite method of harassment and disinformation worldwide. If you see malicious impersonation, hashtags being flooded, disinformation being spread, or you or your colleagues are being targeted and harassed, you are not alone and there may be help. Review the processes for reporting using the following links.
</details>

## Report harassment that reveals information about you

- [Report harassment](https://help.twitter.com/en/forms/safety-and-sensitive-content/private-information)

<details><summary>Learn why we recommend this</summary>

Some abusers may try to target you by revealing information about where you live or work, your family or friends, or other personal details including images. In many cases you have a right to have this taken down, even if that information is true. Review the following links for information on how to get that information removed.
</details>

## Identify and report coordinated inauthentic activity (botnets and spam)

- Learn what kinds of automated activity are and are not acceptable, and [how to report activities that violate the rules](https://help.twitter.com/rules-and-policies/platform-manipulation)

<details><summary>Learn why we recommend this</summary>

Some harassment and disinformation is posted through automated means, rather than by individual people. If you suspect that you are seeing this "coordinated inauthentic activity," you can report it to the social media sites and they may ban those automated systems. While automation can be hard to prove, there are some cases in which reporting coordinated inauthentic activity might be more successful than reporting harassment, if you suspect the social media site will not understand the context of the harassment.
</details>

## Report impersonation

- [Report impersonation](https://help.twitter.com/en/safety-and-security/report-twitter-impersonation)

<details><summary>Learn why we recommend this</summary>

Impersonation in the form of parody is usually accepted as free speech by most social media platforms, and will not be removed. However, impersonation for the purposes of defamation of character may not be, and you can report it.
</details>

## Hide stressful content

- [Steps you can take to hide content that is distressing to you](https://help.twitter.com/en/safety-and-security/control-your-twitter-experience)
- [You can also mute accounts.](https://help.twitter.com/en/using-twitter/twitter-mute) The person you mute will not see that you have muted them.
- [Block a number of notifications](https://help.twitter.com/en/managing-your-account/understanding-the-notifications-timeline)
- [Hide topics from your Twitter feed, by keyword](https://help.twitter.com/en/using-twitter/advanced-twitter-mute-options)

<details><summary>Learn why we recommend this</summary>

Any of us may find some content more distressing than other people do, whether it be information on the death of a friend, public arguments which devalue us because of who we are, or frightening events in the news. If you need a break from this stress, here are some tools which can help hide content you do not wish to see, for as long as you wish.
</details>

## Learn how to recover your account if it is disabled or suspended

- [Learn more about suspension](https://help.twitter.com/en/managing-your-account/suspended-twitter-accounts)
- [Request your account be re-activated](https://help.twitter.com/forms/general?subtopic=suspended)

<details><summary>Learn why we recommend this</summary>

For one reason or another, social media sites will sometimes disable an account. Human rights defenders have sometimes had their accounts shut down because they are documenting human rights abuses with violent scenes that violate the social media platforms's policies; because they have been reported by government, police, or other people who disagree with them; or even because the social media platform does not understand their context well enough to make sense of what they are posting. If this happens to you, you can appeal the decision and ask to have your account restored. Review the links below for information on how.
</details>

## Take a break from your account

- Twitter does not support temporarily suspending your account. However, if you plan to return to your account within 30 days, and will still have access to the same email and phone you registered with, you could consider fully deactivating it. This process is more difficult, and there is a risk you will lose your account name and someone else could take it over after 30 days. [See more information here.](https://help.twitter.com/managing-your-account/how-to-deactivate-twitter-account)

<details><summary>Learn why we recommend this</summary>

If you want to stop people from posting to your account because you will not be able to access it for a while--you suspect you may be detained or jailed, or just because you need to take a break!--you may be able to temporarily deactivate your account on some social media. This can be useful if you face harassment or defamation. On other accounts, like email, you may not be able to stop incoming messages, but can set your account to automatically respond that you are away.
</details>


# Learn how social media use your information

- For a useful add-on which clarifies the Terms of Service of many popular sites, see [Terms of Service; Didn't Read](http://tosdr.org).
- [See the legal basis of Twitter's data processing](https://help.twitter.com/en/rules-and-policies/data-processing-legal-bases)

<details><summary>Learn why we recommend this</summary>

It is often unclear what social media will do with your information when you share it. Are they combined with other data to guess things about you? Are they sold to other companies that may share that information even if you did not want it to be shared? Read the End User Licence Agreement and Privacy Policy or Data Use Policy for social media sites to find out.
</details>
