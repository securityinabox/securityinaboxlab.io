---
weight: 999
title: Recuva
draft: true
post_date: 7 January 2013
status: unmaintained
---

The Recuva guide is no longer maintained. You might want to try other elements of the [Recover from information loss](../../backukp) guide.
