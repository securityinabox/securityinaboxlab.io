---
title: Protect your data when using YouTube
weight: 102
collection: none
post_date: 5 December 2024
logo: ../../../media/en/logos/youtube-logo.png
website: https://www.google.com/intl/en-US/gmail/about/
---

Read this guide to increase your privacy and security when sharing and viewing videos or when streaming content on YouTube.

To use YouTube as a video creator and to access many features offered to YouTube viewers, you will need to log in with a Google account. Learn more on how to secure this account in [our guide on Google](../google).

Consider if relying on commercial, corporate products is secure enough to protect your information. Commercial corporations might not share your values and remove or filter content that they believe violates their policies. It may also be difficult for them to understand your local context, particularly if they do not operate in your language.

# Remember that YouTube is owned by Google

Note that when you log in to use YouTube you are sharing information with Google. And if you log in to your Google account to use a Google service with your browser or Android device, you might share data about you and your internet usage, including non-Google services. It's a good idea to log out of your Google account once you've finished using YouTube (or other Google services).

In any case, we recommend creating a dedicated Google account for YouTube.

# Learn how YouTube uses your information

- You can find a clarification of YouTube's terms of service on [Terms of Service; Didn't Read](https://tosdr.org/en/service/274).
- Also review [Google's policies](https://policies.google.com/), in particular their [privacy policy](https://policies.google.com/privacy) and their [terms of service](https://policies.google.com/terms).

<details><summary>Learn why we recommend this</summary>

It is often unclear what online service providers will do with your information when you share it. Is your data combined with other information to guess things about you? Is it sold to other companies that may share that information even if you did not want it to be shared? Read their terms of service and privacy policy to find out.

You can also install [the addon/extension of Terms of Service; Didn't Read](https://tosdr.org/en/downloads) in your browser to see an overview of the terms of service for each commercial online service provider you use.
</details>

# Set up your account

YouTube belongs to Google, so you need a Google account to use many of its features. [Learn how to set up a new Google account in our tool guide on how to use Google more securely](../google/#create-your-account).

## Decide whether you will use a real or fake name, and maintain separate accounts

- Be aware that even if you provide a fake name to Google, you may still be identifiable by the network you connect from and the IP address it assigns to your device unless you [use a VPN or Tor to hide this information](../../communication/multiple-identities/#hide-your-ip-address).
- Consider using separate accounts or separate identities/pseudonyms for different activities. You will likely want to keep your personal and work accounts separate, at the very least.
    - Read more on this strategy in our guide on [multiple identity management](../../communication/multiple-identities).

# Protect your account

- [Learn how to secure the Google Account linked to your YouTube channel](https://support.google.com/youtube/answer/9701986).
- Read more on [how to protect the Google account you use for YouTube in our guide on Google](../google/#protect-your-account).

# Familiarize yourself with the essential online safety practices for YouTube creators

- [Explore best practices for maintaining online safety on YouTube](https://support.google.com/youtube/answer/2802848).
- [Visit the Creator safety center and make a plan to stay safe](https://www.youtube.com/creators/safety/).
- [Protect your identity and privacy on YouTube](https://support.google.com/youtube/answer/2801895).

# Check suspicious access

- [Learn how to check suspicious access on your Google account in our tool guide on Google](../tools/google/#check-suspicious-access).

# Decide what information to share

- [Watch the video on how YouTube creators balance their public and private life](https://www.youtube.com/watch?v=26_R8DF-feE), in particular [the section where they explain how they protect their personal information](https://www.youtube.com/watch?v=26_R8DF-feE&t=142s).

Information that should never be shared publicly, either in videos or in captions, comments and chats, is for example:

- Passwords
- Personally identifying information, including:
    - your birthday
    - your phone number
    - government or other ID numbers
    - medical records
    - education and employment history (these can be used by untrustworthy people who want to gain your confidence.)
- Information that may lead to understand where you live, for example a video shot in your house or in front of it.

Information that you might not want to share publicly, depending on your assessment of the threats you are facing, is, for example, details about family members and information on your sexual orientation or activities.

- Even if you trust the people in your networks, remember how easy it is for someone to spread information on you more widely than you want it to be.
- Agree with your network on what you do and do not want shared, for safety reasons.
- Think about what you may be revealing about your friends that they may not want other people to know; be sensitive about this, and ask them to be sensitive regarding what they reveal about you.

<details><summary>Learn why we recommend this</summary>

The more information about yourself you reveal online, the easier it becomes for others to identify you and monitor your activities. This can have consequences for anybody, across different regions. The family of an activists who has left their home country, for example, may be targeted by the authorities in their homelands because of things that activist has made publicly accessible online.
</details>

# Decide who can see what

- [Update the privacy settings of your videos to control where they can appear and who can watch them](https://support.google.com/youtube/answer/157177).
- [Change the privacy settings of your playlists to make them public, private or unlisted](https://support.google.com/youtube/answer/3127309).

# Limit who can comment in your channel

- [Hide users from your channel](https://support.google.com/youtube/answer/9482361).

<details><summary>Learn why we recommend this</summary>

Hiding someone means their comments won't show on your channel, including on your comments page in YouTube Studio. It also prevents them from creating clips from your videos or live streams.
</details>

# Manage advertising

- [Manage what types of ads you see on YouTube videos](https://support.google.com/youtube/answer/7403255).
- [Turn off ad personalization](https://adssettings.google.com/).

<details><summary>Learn why we recommend this</summary>

There is a possibility governments or police forces might buy advertising data from social media companies to target you and your network with disinformation, or try to find you.
</details>

# Leave no trace

## Precautions when using a public or shared device

- Avoid logging in to your Google account from shared devices (like an internet cafe or other people's devices).
- Never save your passwords and delete your browsing history when you use a web browser on a public machine. Change the passwords of any accounts you accessed from shared devices as soon as you can, using your own device.

## Delete your watch history

- [Turn off or delete your watch history](https://support.google.com/youtube/answer/95725).

<details><summary>Learn why we recommend this</summary>

Some online services keep track of things you have searched for within their sites and apps. If your account is compromised or your device is seized, your adversary could use this information against you, so it is a good idea to clear this history out regularly, or avoid saving it altogether, as well as clearing your browser history.
</details>

## Learn what Google will turn over to governments or law enforcement

- See [Google's policy on law enforcement requests](https://policies.google.com/terms/information-requests).
- See [Google's Transparency report](https://transparencyreport.google.com) to learn how Google has responded to requests by governments and corporations.

<details><summary>Learn why we recommend this</summary>

Social media sites may give your information, including information you were trying to keep private, to governments or law enforcement agencies if requested to do so. Look through the links in this section to learn more about the conditions under which they have provided or will provide such information.
</details>

# Handle abuse

## Report abuse

- [Report inappropriate videos, channels and other content on YouTube](https://support.google.com/youtube/answer/2802027).
- [Learn more about YouTube's reporting tools and policies](https://support.google.com/youtube/answer/9563682).

<details><summary>Learn why we recommend this</summary>

Social media have unfortunately become a favorite method of harassment and disinformation worldwide. If you see malicious disinformation or hate speech, or if you or your colleagues are being targeted and harassed, you may find help by following YouTube's reporting procedures. Review the processes for reporting in the support pages linked in this section.
</details>

## Learn what you can do to prevent hate speech or trolling

- [Learn how to moderate your live chat when streaming videos](https://support.google.com/youtube/answer/9826490).
- [Watch a video on how to encourage the conversation in YouTube comments and how to address negative interactions](https://www.youtube.com/watch?v=T8iFv4oo8Vw).
- [Learn how to moderate comments in channels or single videos](https://support.google.com/youtube/answer/9483359).

<details><summary>Learn why we recommend this</summary>

Human rights defenders, journalists, women and LGBTQI individuals are disproportionate targets of hate speech and trolling campaigns, often launched against them by coordinated groups of people. You could see this kind of attacks in particular in comments on YouTube, in the live chat while you're streaming on your channel or also in videos published on other channels and in comments to videos produced by someone else.

Attackers and comments can often be blocked and reported, but you can also prevent hateful comments and trolling through the settings of your channel, videos and streaming. You can for example hide certain users, or create a blocked words list to automatically hide comments that contain certain words and phrases. This section includes links to learn about all the functionalities you can use to moderate your live chat and comments to your videos.
</details>

## Create a team of moderators

If you decide to add moderators to your channel or to a single video, here's a few safety recommendations:

- Don’t share your password, even with people you trust. Add their own user account and use channel permissions to decide what they can or cannot do in your channel.
- Make sure also your moderators use [2-Step verification](../google/#set-up-2-step-verification) and [strong passwords](../../password/passwords).
- When someone leaves your team, remove moderation access immediately.

- [Add or remove a comment moderator](https://support.google.com/youtube/answer/15535966?hl=en&ref_topic=9257890).
- [Learn how to add or remove access to your YouTube channel with channel permissions](https://support.google.com/youtube/answer/9481328).
 - [Learn about moderation in live chat](https://support.google.com/youtube/answer/9826490?hl=en&ref_topic=9257792&sjid=8292629385782889628-EU#zippy=%2Cassign-moderators).

<details><summary>Learn why we recommend this</summary>

Moderating a channel or a live chat may need a certain effort. If you are targeted by hate speech it's best to get help from a team of moderators so you don't have to manage everything on your own. Among other things, this can help you avoid burnout by taking a break while others moderate your channel.
</details>

## Report harassment that reveals information about you

- [Report a privacy violation](https://support.google.com/youtube/answer/7671399).

<details><summary>Learn why we recommend this</summary>

Some abusers may try to target you by revealing information about where you live or work, your family or friends, or other personal details including images and videos. In many cases you have a right to have this content taken down, even if it is true. This section provides information on how to get that content removed.
</details>

## Identify and report coordinated inauthentic activity (botnets and spam)

- [Manage spam in comments](https://support.google.com/youtube/answer/9482362).
- [Learn about YouTube's policies on spam, deceptive practices and scams](https://support.google.com/youtube/answer/2801973).

<details><summary>Learn why we recommend this</summary>

Some harassment and disinformation is posted through automated means, rather than by individuals. If you suspect that you are seeing this "coordinated inauthentic activity," you can report it to the platform that is hosting this content and they may ban those automated systems. While automation can be hard to prove, there are some cases in which reporting coordinated inauthentic activity might be more successful than reporting harassment, if you suspect the online platform will not understand the context of the harassment.
</details>

## Report impersonation

- [Learn about YouTube's impersonation policy](https://support.google.com/youtube/answer/2801947).

<details><summary>Learn why we recommend this</summary>

Impersonation in the form of parody is usually accepted as free speech by most online platforms, and will not be removed. However, impersonation for the purposes of defamation of character may not be, and [you can report it](#report-abuse).
</details>

## Hide stressful content

- [Turn restricted mode on to screen out adult or violent content that you or others using your devices may prefer not to view](https://support.google.com/youtube/answer/174084).

<details><summary>Learn why we recommend this</summary>

Any of us may find some content more distressing than other people do, whether it be information on the death of a friend, public arguments which devalue us because of who we are or frightening events in the news. If you need a break from this stress, this section lists some tools which can help hide content you do not wish to see, for as long as you wish.
</details>

## Learn how to recover your disabled account or channel and how to restore a removed video

- [Appeal a Community Guidelines termination](https://support.google.com/youtube/answer/2802168#appeal_CG).
- [Appeal a Community Guidelines strike or video removal](https://support.google.com/youtube/answer/185111).

<details><summary>Learn why we recommend this</summary>

[For one reason or another, YouTube may disable (or "terminate") your account or channel or it can remove a video](https://support.google.com/youtube/answer/2802168). Human rights defenders have sometimes had their accounts or channels shut down, for example because they were documenting human rights abuses with violent scenes that violated the platform's policies, because they had been reported by a government, by the police or by people who disagreed with them, or even because Google did not understand their context well enough to make sense of what they were posting. If this happens to you, you can appeal the decision and ask to have your account restored. Review the instructions linked in this section for information on how to do this.
</details>

## Take a break from your account

- [Make your channel temporarily hidden](https://support.google.com/youtube/answer/2976814?hl=en#zippy=%2Cchannel-visibility). Your videos, playlists and channel details will no longer be visible to viewers. This can be an option if you want to take a break, for example because your channel is being targeted by a harassment campaign.

<details><summary>Learn why we recommend this</summary>

If you want to stop people from commenting on your channel because you will not be able to access it for a while — for example because you need to take a break, but also because you suspect you may be detained or jailed — you can temporarily hide it. This can be useful also if you face harassment or defamation.
</details>

## If you think your account has been hacked

- Read the [Google support page on what to do if you think your YouTube channel has been hacked into](https://support.google.com/youtube/answer/76187).
- [If you can't access your Google account, read our tips in the Google tool guide](../google#if-you-think-your-account-has-been-hacked).
- If the standard procedures don't work, read the [Digital First Aid Kit troubleshooter on what to do if you have lost access to your account](https://digitalfirstaid.org/topics/account-access-issues/#google).
