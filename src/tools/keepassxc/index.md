---
weight: 001
title: KeePassXC for Windows - Secure Password Manager
post_date: 16 November 2020
logo: ../../../media/tool/logo/keepassxc_logo.png
website: https://www.keepassxc.org/
download_links: 
  - 
    label: Download link
    url: https://keepassxc.org/download/#windows
version: 2.3.4
license: Free and Open Source Software
system_requirements: 
  - MS Windows 2000, XP, Vista, 7, 8, 10
  - Apple Mac OS X (10.10+)
  - Recent GNU/Linux (e.g. Ubuntu 16.04+)
---

KeePassXC is a cross-platform, **free and open source (FOSS)** password manager that allows you to store all of your passphrases in one secure, portable database.

_**Note**: In this guide, we use the words "password" and "passphrase" interchangeably. We recommend creating passwords that are longer than a single word, and one way to achieve this is to use uncommon phrases (with or without spaces between the words)._

# Required reading

- [**Create and maintain secure passwords**](../../passwords)

# Things to keep in mind

- Master passwords must be strong and unique. 
- You must be able to remember master passwords. 
- The database file (*.kbdx) must be kept in a secure location. 
- Plan a gradual transition to reduce the risk of losing master and other passwords. 
- Work together as a team. Help each other become familiar with a password manager.  

# What you will get from this guide

- The ability to save all of your passphrases in one encrypted database.
- The ability to copy and paste those passphrases, so you do not have to memorise them.
- The ability to generate completely random passphrases.
- The ability to encrypt notes and files attached to the entries in your password database.

# 1. Introduction to KeePassXC

This is what logging-in to your account using KeePassXC looks like . 

![](../../../media/en/keepassxc/KeePassXC-win-en-v02-001.gif)

The steps to the example above are:

1. Enter your master passphrase into KeePassXC to open your password database.
2. Select the account you want to access.
3. Copy & paste your username and password from KeePassXC to the login screen of the relevant program (e.g. Mozilla Firefox).

**KeePassXC** helps you store and manage your passphrases inside an encrypted database file. This file is encrypted with a *master passphrase*. Instead of remembering passwords, you can locate them in KeePassXC and copy & paste them where needed. KeePassXC will delete copied information from your *clipboard* memory after 10 seconds. 

**KeePassXC** can also generate strong passphrases for your accounts. The random passphrases created by KeePassXC are typically much stronger than the ones we come up with ourselves. 

Even though the KeePassXC database is encrypted, it is very important to back up this database. We recommend keeping a backup on an external drive, where you trust to be safe and secure. We **do not recommend** sending your database by email or storing it online where it might be accessed by others. 

In the following sections, you will learn how to:

- Create a password database and set a *master passphrase*.
- Save your newly created password database.
- Generate a random password for a particular service or account.
- Copy passwords, usernames and other information from **KeePassXC** when needed.
- Change your *master passphrase*.

# 2. How to use KeePassXC 

## 2.0. Install KeePassXC

**For desktop & laptop** 

- [**Linux**](https://keepassxc.org/download/#linux)
- [**Windows**](https://keepassxc.org/download/#windows)
- [**MacOS**](https://keepassxc.org/download/#mac)

**For mobile devices**: There is no official mobile version of KeePassXC. We recommend the below applications which have similar functionality.

- [**KeePass2Android**](https://play.google.com/store/apps/details?id=keepass2android.keepass2android&hl=en): for **Android**
- [**Strongbox**](https://apps.apple.com/us/app/strongbox-password-safe/id897283731): for **iOS** 

## 2.1 Main features

### 2.1.1. Create and save a new KeePassXC database

![](../../../media/en/keepassxc/KeePassXC-win-en-v02-002.gif)

**Step 1**. **Click** ["Create New Database"]

**Step 2**. **Enter** a name for your database 

**Step 3**. **Configure** ["Encryption Settings"]

Leave default settings on "Encryption Settings" screen and click "Continue"

**Step 4**. **Enter** Master Passphrase

Your master passphrase is the key to all other passphrases stored on the database. It is extremely important to choose a strong and unique passphrase that you are able to memorise. If needed, write the master passphrase on a piece of paper for a few days until it is memorised and then securely destroy the paper.

For more advice, see [**Create and maintain secure passwords**](../../passwords)

**Step 5**. Select **file name** and **location**. 

A KeePassXC database is a file that uses the .kbdx database format (for instance, Passwords.kdbx) to store passphrases of important accounts. You should save this file in a location easily accessible for you, but difficult to find for others you don’t want viewing the information. 

In this example, we name our database "passwords.kdbx"; you can choose a name you prefer. We suggest using a name that disguises the file and makes it less obvious to attackers who could try to access your computer and demand you to unlock the database.

It is also possible to change the extension at the end of the filename. For example, you could name the password database "RentalAgreement.pdf" and the operating system will usually give the file a more discreet icon. Keep in mind if you give your password database a name that does not end in ".kdbx", you will not be able to double-click the file to open it in KeePassXC. Instead, you will have to first launch KeePassXC, then open your database by clicking the "Open existing database" button. Luckily, KeePassXC remembers the last database you opened so you will not have to do this often. 

![](../../../media/en/keepassxc/KeePassXC-win-en-v02-003.gif)

If you store the databse on a USB flash memory stick, along with a copy of the KeePassXC application, you can access and use your database from other computers. Always confirm additional computers are not infected by malware before inserting the USB flash memory stick.

### 2.1.2. Create and manage password entries

![](../../../media/en/keepassxc/KeePassXC-win-en-v02-004.gif)

**Step 1**. **Click** the "Add a new entry" icon

The Add Entry screen allows you to store information about a particular account in your KeePassXC database. 

**Step 2**. **Add** relevent information about the account.  

Use a descriptive name so you can find the item easily. The main information we suggest you add is **username** and **password**. Additionally, you can add URL addresses, notes (e.g. account recovery steps) and other fields. To add more information, see the left side menu. All the information you add will be encrypted in the database.

**Step 3**. **Click** OK to save a new entry.   

**Step 4**. **Save** the database 

### 2.1.4 Change existing passwords 

Please note, creating or modifying password entries in KeePassXC ***does not change the passwords on your actual accounts***. Your KeePassXC database is not connected to your accounts online. Therefore, it only stores what you write and save in it, nothing more.

![](../../../media/en/keepassxc/KeePassXC-win-en-v02-005.gif)

When changing a password in an online account, follow these instructions to also save the new password in your password database. Here, we're using Riseup Email as an example. The process may be slightly different for each online account. 

**Step 1**. **Open** your account settings' change passwords page. You can usually find a page to change your passwords in the account settings.

**Step 2**. **Copy & Paste** the current password from KeePassXC to the relevant fields in the account settings page.

**Step 3**. **Edit** the KeePassXC Entry with a new and random password

KeePassXC allows you to generate a strong, random passphrase by clicking the dice button to the right of the password in the password entry. You can specify the length of your passphrase (longer passphrases would increase security) and type of characters used to create it. When you finish generating the new password, click the "Apply Password" button and then the "OK" button to finish editing the entry.

**Step 4**. **Copy & Paste** your new password into the account setting page.

**Note**: The "History" section on the bottom of the left side menu has backups of all the previous passwords in case somebody needs the old password for this account.

### 2.1.5 Change your master passphrase 

![](../../../media/en/keepassxc/KeePassXC-win-en-v02-006.gif)

You can change your KeePassXC database master passphrase whenever you need (e.g. when you suspect somebody may have seen you typing it in). Make sure you choose a strong, unique master passphrase because it protects the database that contains the log-in information of multiple accounts.

**Step 1**. **Open** "Database" > "Database Security"

**Step 2**. **Click** Change Password

**Step 3**. **Enter & Confirm** a new master passphrase 

**Note**: Remembering your new master passphrase is crucial. In case you forget it, you can back up the database before changing the master passphrase to prevent losing access to your accounts and services. 

## 2.2 Other important security features

### 2.2.1 Back up your password database

It is crucial to back up your KeePassXC Database as a safeguard to any loss and damage. Make sure you backup the KeePassXC file (.kdbx) whenever you make any change to it.

As described in section 2.1.1.["Create and save a new KeePassXC Database"] above, you can name the document whatever you like and can even add a different file extension, such as .pdf, .doc or .jpg. 

### 2.2.2 Lock & Timeouts

Similar to locking your computer screen, it is good practice to lock the password database when you are not using it to protect your passwords from being accessed when you are away from your computer.

To set the automatic lock, open menu [Tools > Settings > Security > Timeouts and activate option]: Select a period of time that your KeePassXC database will be automatically locked.  

### 2.2.3 Password Health Check 

The KeePassXC Health Check feature will scan all the passwords in your database and indicate weak and reused items.

**Open** Menu Database > Database Report and select "Health Check"

Passwords that are “very weak", “weak” or used multiple times are reported in the Health Check and marked red, orange and yellow. The score is based on the password strength, re-use and expiry date.

Consider improving reported passwords.

**Note**: Some passwords might have legitimate reasons to be short or old. You can exclude them from the health check by right clicking on the entry in the report and selecting the "Exclude from reports" option.For example:

- Office door code, you can right click on the entry from the report and "exclude from reports"

- GPG / Email encryption which usually stays the same for the life of the key (~5 years)

You can read more [**here**](https://keepassxc.org/blog/2020-08-15-keepassxc-password-healthcheck/)

### 2.2.4 Two-factor authentication for KeePassXC

In addition to the master password, you can set up another authentication factor called **Key File** . Once this is done, you must select the Key File each time you open your KeePassXC database.

**Open Menu** [Database > Database Security > Add additional protection...]

For **Key File** (*.key)

You can use an existing key file OR generate a new key file for your database. Once you assign a key file to your database, **click** "OK". The next time you log in to your database, you must browse and locate where your key file is.

**Note** KeePassXC also supports using **YubiKey Challenge-Response** as a two-factor authentication device. 

**Note**: The key file must be stored separate from your passwords database file. In case anyone manages to have a copy of your password database file (filename.kbdx), the person would still need to have your key file as the additional authentication. 

# FAQ

***Q***: On the off chance that I forget my master password, is there anything I can do to access retrieve my saved passphrases?

***A***: No. There is nothing you can do in that situation. To prevent this from happening, you could use some of the methods for remembering passphrases described in the [**Create and maintain strong passwords**](../../passwords) guide.

***Q***: If I uninstall or remove **KeePassXC**, what will happen to my passwords?

***A***: The program will be deleted from your computer, but your database (stored in a .kdbx file) will remain. You can open this file at any time in the future if you install **KeePassXC** again. For the same reason, if you wish to hide KeePassXC , it's important that you delete both the program and the database file.

***Q***: I accidentally deleted the database file!

***A***: Hopefully you made a backup beforehand. Make sure you haven't simply forgotten where you stored the file in the first place. Search your computer for a file with a .kdbx extension.

***Q***: There are several accounts that my colleagues and I share. Can I use KeePassXC?

***A***: Yes, but it's important to note a few details to ensure security. 

Make sure you share the username and password through a secure channel. The best option would be to exchange account details in person or in encrpyted communication. Once this is done, you can keep these account details in their own KeePassXC databases.

It is also possible to create a password database file (.kdbx) with shared accounts, and exchange it over secure channels (e.g. USB memory stick) as well as the master password. Once you work with a shared database file, you should assign a person in charge of managing the database file, including backups, and to update if any changes happen to any shared accounts.

In certain circumstances, you may consider an online password manager. It is important that you understand the risks invovled and take appropriate measures, such as excluding highly-sensitive accounts from the online database. We recommend [**Bitwarden Open Source Password Manager**](https://bitwarden.com).

***Q***: Can I open the KeePassXC database on another computer or on the phone?

***A***: Yes. You will need to copy the password database file to another computer or to a phone, install KeePassXC (computer) or KeePassDX (Android) or StrongBox (iOS). And then you will be ready to open the database. Make sure that you copy the database over secure means, like your own trusted USB disk or connecting the phone directly to the computer. Assess if the other computer or phone is a secure environment to open your password database on.

***Q***: How can I hide the that I am using KeePassXC and that I have a password database?

***A***: There are various strategies which you could implement.

For example you may use a portable version of KeePassXC on Microsoft Window so you could store KeePassXC on an external USB disk and save your password database on this external disk. You will not have either KeePassXC or the database on your computer and it will be easier to hide USB disk then a computer.

Another strategy is to give the password database a misleading name which suggests it is something else like "notes.docx" or "image.gif". You will need to instruct KeePassXC not to remember which database it opened last time. Go to Menu Tools > Settings > General and deselect the option "Remember previously used databases". You will need to remember which file is your password database. And you will need to open it directly from KeePassXC.

In addition you could also rename the KeePassXC programme.

***Q***: The time KeePassXC gives me to copy a password to the login screen is too short. How can i change it?

***A***: By default KeePassXC gives 10 seconds for copying and pasting information before clearing it from the clipboard. You can change this by going to menu Tools > Settings > Security >Timeouts and then select the "Clear clipboard after" option. 
