---
weight: 094
title: KeePassDX - Mobile Password Manager for Android
collection: menu
post_date: 01 February 2021
logo: ../../../media/en/logos/keepassdx-logo.png
website: https://www.keepassdx.com/
download_links: 
  - 
    label: Download link
    url: https://play.google.com/store/apps/details?id=com.kunzisoft.keepass.free
version: 2.9.12
license: Free and Open Source Software
system_requirements: 
  - "Android 4.0 or higher"
---

KeePassDX is a **free and open source (FOSS)** password manager application for Android mobile phone and tablet devices. It allows you to use the same database file (*.kdbx) on your Android device as KeePassXC (:link page) on a computer.

**Note**: In this guide, we use both password and passphrase interchangeabily. We recommend password to be longer, more of a phrase, than a word.

For iPhones and other iOS devices, we recommend [**Strongbox**](https://strongboxsafe.com/), which has similar functionality. 

# Required reading

- [**Create and maintain secure passwords**](../../passwords)
- [**KeePassXC**](../keepassxc)

# Things to keep in mind

- Master passwords must be strong and unique.
- You must be able to remember master passwords.
- The database file (*.kbdx) must be kept in a secure location.
- Plan a gradual transition to reduce the risk of losing master and other passwords.
- Work together as a team. Help each other to get familiar with a password manager.

# What you will get from this guide

- The ability to save all of your passphrases in one encrypted database.
- The ability to copy and paste those passphrases, so you do not have to memorise them.
- The ability to generate completely random passphrase.
- The ability to encrypt notes and files attached to the entries in your password database.

# 1. Introduction to KeePassDX

This is what it looks like logging-in to your account using KeePassDX.

![](../../../media/en/keepassdx/keepassdx-en-01.gif)

Steps of the example above are:

1. Enter your master passphrase into KeePassDX to open your password database.
2. Select the account you want to access.
3. Copy & paste your username and password from KeePassDX to the login screen of the relevant program (e.g. Mozilla Firefox).

**KeePassDX** helps you store and manage your passphrases inside an encrypted database file. This file is encrypted with a *master passphrase*. Instead of remembering passwords, you can locate them in KeePassDX and copy & paste them where needed. By default, KeePassDX will delete copied information from your *clipboard* memory after 5 minutes. 

**KeePassDX** can also generate strong passphrases for your accounts. The random passphrases created by KeePassDX are typically much stronger than the ones we come up ourselves. 

Even though the database is encrypted, it is very important to back up your KeePassDX database securely. We recommend keeping a backup on an external drive, where you trust to be safe and secure. We **do not recommend** sending your database by email or storing it online where it might be accessed by others. 

In the following sections, you will learn how to:

* Create a password database and set a *master passphrase*.
* Save your newly created password database.
* Generate a random password for a particular service or account.
* Copy passwords, usernames and other information from **KeePassDX** when needed.
* Change your *master passphrase*.

# 2. How to use KeePassDX

## 2.0. Install KeePassDX

* Install [**KeePassDX**](https://play.google.com/store/apps/details?id=com.kunzisoft.keepass.free&hl=en_US&gl=US) from Google Play Store.

## 2.1 Main features
### 2.1.1. Create and save a new KeePassDX database

![](../../../media/en/keepassdx/keepassdx-en-02.gif)

**Step 1**. **Click** [CREATE NEW DATABASE]

**Step 2**. Select **file location**

Similar to KeePassXC, KeePassDX supports the .kbdx database format (for instance, Passwords.kdbx) to store passphrases of important accounts. You should save this file in a location easily accessible for you, but difficult to find for others you don’t want viewing the information. In particular, avoid saving your database file on an online storage services, connected to your accounts, as it can be accessible by others. 

**Step 3**. **Enter** a name for your database 

In this example, we name our database "keepass.kdbx"; you can choose a name you prefer. You may use a name that disguises the file and makes it less obvious to attackers who could try to access your phone and demand you to unlock the database.

It is also possible to change the "extension" at the end of the filename. For example, you could name the password database "RentalAgreement.pdf" and the operating system will usually give the file a more "normal looking" icon. Keep in mind if you give your password database a name that does not end in ".kdbx", you will not be able to directly open the file. Instead, you will have to launch KeePassDX first, then open your database by clicking the "Open existing database" button. Luckily, KeePassDX remembers the last database you opened so you will not have to do this often.

**Step 4**. **Assign** a master key 

Your master password is the key to all other passphrases stored on the database. It is extremely importapnt to choose a strong and unique passphrase that you are able to memorise. If needed, write the master passphrase on a piece of paper for a few days until it is memorised and then securely destroy the paper.

For more advice, see [**Create and maintain secure passwords**](../../passwords)

### 2.1.2 Transfer and use an existing database file. 

If you're already using KeePassXC on your laptop or desktop, or StrongBox on iOS devices, you need to transfer your database file between your devices without exposing the database to any risk. Since storing or transferring your database online creates unnecessary risks, we **do not recommend** sending your database by email or storing it online where it might be accessed by others.

We recommend transfering the database file directly between your devices using a USB cable.

### 2.1.3. Create and manage password entries

![](../../../media/en/keepassdx/keepassdx-en-03.gif)

**Step 1**. **Click** the "Add entry" icon

The Add Entry screen allows you to store information about a particular account in your KeePassDX database. 

**Step 2**. **Add** relevent information about the account.  

Use a descriptive name so you can find the item easily. The main information we suggest you add is **username** and **password**. Additionally, you can add URL addresses, notes (e.g. account recovery steps) and other fields. All information you add will be encrypted in the database.

You can manually put in the password for the account. KeePassDX also allows you to generate a strong, random passphrase by clicking the dice button to the right of the password in the password entry. You can specify the length of your passphrase (longer passphrases would increase security) and type of characters, including special charecters and brackets, to create strong passphrases. When you finish generating the new password, click the Apply Password button and then the OK button to finish editing the entry.

**Step 3**. **Click** check to save a new entry.   

### 2.1.4 CHANGE EXISTING PASSWORDS
Please note, creating or modifying password entries in KeePassDX does not change the passwords on your actual accounts. Your KeePassDX database is not connected to your accounts online. Therefore, it only stores what you write and save in it, nothing more.

When changing a password in an online account, follow these instructions to also save the new password in your password database. Here, we're using Riseup Email as an example. The process may be slightly different for each online account.

![](../../../media/en/keepassdx/keepassdx-en-04.gif)

Step 1. Open your account settings' change passwords page. You can usually find a page to change your passwords in the account settings.

Step 2. Copy & Paste the current password from KeePassDX to the relevant fields in the account settings page.

Step 3. Edit the KeePassDX Entry with a new and random password

KeePassDX allows you to generate a strong, random passphrase by clicking the dice button to the right of the password in the password entry. You can specify the length of your passphrase (longer passphrases would increase security) and type of characters used to create it. When you finish generating the new password, click the "Apply Password" button and then the "OK" button to finish editing the entry.

Step 4. Copy & Paste your new password into the account setting page.

Note: The "History" section on the bottom of the left side menu has backups of all the previous passwords in case somebody needs the old password for this account.

### 2.1.5 Change your master passphrase 
![](../../../media/en/keepassdx/keepassdx-en-05.gif)

You can change your KeePassDX database master passphrase whenever you need (e.g. when you suspect somebody may have seen you typing it in). Make sure you choose a strong, unique master passphrase because it protects database that contains the log in information of multiple accounts.

**Step 1**. **Open** Setting > Master key  

**Step 2**. **Click** Change master key

**Step 3**. **Enter** a new master passphrase 

**Step 4**. **Click** OK

**Note**: Remembering your new master passphrase is crucial. In case you forget, you can back up the database before changing the master passphrase to prevent losing access to your accounts and services. 

**Avoid using biometric unlocking**: KeePassDX support biometric unlocking. However, they are generally a less secure way of secureing your database compared to passwords. For example, you can not change your biometric information whenever you like. You are also required to leave biometric information, such as fingerprints, in airports, goverment offices, etc. which creates a potential risk. When you are physically restrained by adversaries, unlocking devices with biometrics is even easier than passwords.

For more advice, see [**Create and maintain secure passwords**](../../passwords)

## 2.2 Other important security features

### 2.2.1 Back up your password database

It is crucial to back up your KeePassDX Database as a safeguard to any loss and damage. Make sure you backup the database file (.kdbx) whenever you make any change to it. 

You can find where you have saved your database file by clicking the downward arrow next to the databsae name on the main screen. 

It is important that you place the backup database file in a secure location, other than the device you using KeePassDX on. The best option is connecting your mobile device to a secure computer with usb cable and backup your database. In this case, you can retrive your database even after you loose your device. It is also important to avoid using email or online storage where it might be accessed by others.

### 2.2.2 Lock & Timeouts

Similarly to locking your computer screen, it is good practice to lock the password database when you are not using it to protect your passwords from being accessed when you are away from your device.

In addition to setting a timeout lock on your screen, you can lock KeePassDX. To set the automatic lock, open Settings > Lock > App timeout and choose a time period to lock after inactivity of a certain period of time. 

# FAQ

***Q***: On the off chance that I forget my master password, is there anything I can do to access retrieve my saved passphrases?

***A***: No. There is nothing you can do in that situation. To prevent this from happening, you could use some of the methods for remembering passphrases described in the [**Create and maintain secure passwords**](../../passwords) guide.

***Q***: If I uninstall or remove **KeePassDX**, what will happen to my passwords?

***A***: The application will be deleted from your phone, but your database (stored in a .kdbx file) will remain. You can open this file at any time in the future if you install **KeePassDX** again. For the same reason, if you wish to remove all traces of using KeePassDX, you need to delete both the application and the database file.

***Q***: I accidentally deleted the database file!

***A***: Hopefully you made a backup beforehand. Make sure you haven't simply forgotten where you stored the file in the first place. Search your phone for a file with a .kdbx extension.

***Q***: There are several accounts that my colleagues and I share. Can I use KeePassDX?

***A***: Yes, but it's important to note a few details to ensure security. 

Make sure you share the username and password through a secure channel. The best option would be to exchange account details in person or in encrpyted communication. Once this is done, you can keep these account details in their own KeePassDX databases.

It is also possible to create a password database file (.kdbx) with shared accounts, and exchange it over secure channels (e.g. USB memory stick) as well as the master password. Once you work with a shared database file, you should assign a person in charge of managing the database file, including backups, and update if any changes happened to any shared accounts.

In certain circumstances, you may consider an online password manager. It is important that you understand the risks invovled and take appropriate measures, such as excluding highly-sensitive accounts from the online database. We recommend [**Bitwarden Open Source Password Manager**](https://bitwarden.com).

***Q***: Can I open KeePassDX database on another phone or on the computer?

***A***: Yes. You will need to copy the password database file to another phone or to a computer, install KeePassDX if this is Android phone, StrongBox on iPhone or KeePassXC on computer. And then you will be ready to open the database. Make sure that you copy the database over secure means, like your own trusted USB disk or connecting the phone directy to the computer. Assess if the other phone or computer is a secure environment to open your password database on.

***Q***: How can I hide the that I am using KeePassDX and that I have a password database?
droid
***A***: There are various strategies which you could implement.

It is possible to give a password database misleading name which would suggest it is something else like "notes.docx" or "image.gif". You will need to instruct KeePassDX not to remember which database it opened last time. Open Settings > App > App setting > History and deselect options "Remember database locations" and "Show recent files". You will need to remember yourself which file is saved where to access your password database. And you will need to open it directly from KeePassDX. 

On Android, You can hide the applications name and icon using a custom launcher, such as [Nova Launcher](https://novalauncher.com/) and [Parallel Space](https://play.google.com/store/apps/details?id=com.lbe.parallel.intl&hl=en&gl=US). 

***Q***: How long does KeePassDX remember my password in clipboard? 

***A***: By default KeePassDX gives 5 minutes for coping and pasting information before clearing it from so called clipboard. We consider that 5 minutes is unnecessarily too long and could lead to unwatned pastings. You can change this by going to menu Tools > Settings > Security. You will find there option "Clear clipboard after" in section "Timeouts".
