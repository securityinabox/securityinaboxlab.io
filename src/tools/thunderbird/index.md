---
weight: 005
title: Thunderbird, Enigmail and OpenPGP for Windows - Secure Email
collection: menu
post_date: 22 October 2019
status: out-of-date
logo: ../../../media/tool/logo/thunderbird-logo_hr.png
website: https://www.thunderbird.net
download_links: 
  - 
    label: Download Thunderbird
    url: https://www.thunderbird.net
  - 
    label: Gpg4win 3.1.10
    url: https://www.gpg4win.org/
  - 
    label: Enigmail
    url: https://www.enigmail.net/download/
version: Thunderbird 68.1 (Enigmail 2.1.2)
license: Free and Open Source Software
system_requirements: 
  - Microsoft Windows
  - MacOS
  - GNU/Linux
---

**Mozilla Thunderbird** is free and open source software that allows you to exchange and store email for multiple accounts with multiple service providers. **Enigmail** and **GnuPG** improve the security and privacy of your email correspondence by adding support for OpenPGP end-to-end encryption to **Thunderbird**. They also allow you to sign your messages digitally and verify the digital signatures of others.

# Required reading

- [Keep your online communication private](../../communication/private-communication/)
- [Protect the sensitive files on your computer](../../files/secure-file-storage)

# What you will get from this guide

- The ability to send and receive encrypted email
- The ability to digitally sign your emails and authenticate signed email from others
- The ability to make sure that you are encrypting your email only for the intended recipient/s
- The ability to manage multiple email accounts using a single tool
- The ability to read and compose messages while disconnected from the Internet

## Note on Usage

This guide is recommended for a specific use case, in which you only use **Thunderbird as a tool for email encryption**, while reading and sending unencrypted email through their webmail interface in a browser or through a different client.

You can still use Thunderbird as your regular mail client, and we recommend it if you prefer to download your email to a secure device instead of leaving it in the servers of your email provider. For this latter case, we will recommend alternative settings in the relevant sections.

# 1. Introduction to Thunderbird

**Thunderbird** is a free and open source, cross-platform email client for sending, receiving and storing email. An *email client* is an application that lets you download and manage your messages — from multiple accounts with multiple providers — without a browser.

**Gnu Privacy Guard (GPG)** is free and open source software capable of encrypting, decrypting and digitally signing messages and files. It also generates and manages the public and private keys needed to do so.

**Enigmail** Enigmail is an add-on for Thunderbird to encrypt, decrypt and sign emails, and to manage PGP keys.



## 1.0 Things you should know before you start

You will need at least one email account to use **Thunderbird**. If you want to create a new account to use with **Thunderbird**, you can find a list of privacy-conscious email services [here](https://prxbx.com/email/).

Like all *email clients*, **Thunderbird** makes a copy of your messages available on your computer. This includes the emails you send as well as those you receive. As a result, it is particularly important that you have full-disk encryption enabled if you intend to use Thunderbird. If your version of Windows supports it, we recommend you enable [BitLocker encryption](http://windows.microsoft.com/en-us/windows-8/bitlocker-drive-encryption).

**Thunderbird** cannot protect your device if you open malicious attachments or click on  malicious links. Do not open unsolicited attachments and exercise caution when clicking on links that were sent to you by email. Learn how to [**Protect your device from Malware and Hackers**](../../phones-and-computers/malware/).

In order to better protect the data in your computer, we highly recommend enabling device encryption before starting this guide. If your version of Windows supports it, we recommend you enable [BitLocker encryption](http://windows.microsoft.com/en-us/windows-8/bitlocker-drive-encryption).


## 1.1 Other tools like Thunderbird

**Thunderbird** is available for **GNU/Linux**, **Microsoft Windows** and **macOS**. Securely managing multiple email accounts is a complex task, and we strongly recommend Thunderbird for this purpose. However, if you prefer to use an alternative we recommend the following free and open source tools:

- [**Claws Mail**](http://www.claws-mail.org/) is available for GNU Linux and Microsoft Windows
- [**Sylpheed**](http://sylpheed.sraoss.jp/en/) is available for macOS, GNU/Linux and Microsoft Windows
- [**K9 Mail**](https://play.google.com/store/apps/details?id=com.fsck.k9&hl=de) and [**OpenKeychain**](https://play.google.com/store/apps/details?id=org.sufficientlysecure.keychain&hl=en) are available for Android
- [**Mailpile**](https://www.mailpile.is/) is available for GNU/Linux (and should be available for Windows and macOS in the future).

The security advantages of Thunderbird are significant, particularly when compared to commercial alternatives like *Microsoft Outlook*.

# 2. Install and configure Gpg4win, Thunderbird and Enigmail

## 2.1 Download and Install Gpg4win

If you want to send and receive encrypted emails, you will first need to download and install Ggp4win, the encryption manager that enables your computer to encrypt and decrypt.

1. Go to [Gpg4win's website](https://www.gpg4win.org/) and click the green **Download Gpg4win** button.

    ![](../../../media/thunderbird-en/windows/gpg4winWebsite.PNG)

2. Select an option for a free download ("$0") or for a donation, and click "Download".

    ![](../../../media/thunderbird-en/windows/gpg4winDownload.png)

3. Click "Save File" in the prompt that pops up.

    ![](../../../media/thunderbird-en/windows/winPGPgpg4winSave.PNG)

4. Verify the file you have downloaded, which will be called `GPG4win-version.exe`, following the below steps:

	- Hover over 'SHA256' in the [download page](https://www.gpg4win.org/download.html) below the download button, to visualize a sequence of numbers and characters.
	
	  ![](../../../media/thunderbird-en/windows/SHA256Verification.PNG)
	
	- In your machine, open the Command Prompt.
	
	- Enter the command below:

		      certutil -hashfile \path\to\gpg4win-version.exe SHA256

        The output should be the same as the number you visualized when hovering over 'SHA256' in the Gpg4win download page. If the sequence of numbers and characters is not the same, something wrong happened with your download. For example, the file could be damaged, or the download might be incomplete. **Please do not move on to the next steps before having completed the verification correctly.**

5. Once you have correctly verified the file, you can proceed with the installation: click the download arrow in the top right corner of your browser and then click the GPG4win.exe file in your recent downloads.

    ![](../../../media/thunderbird-en/windows/winPGPDownloadsPGPArrow.PNG) 

6. Click "Yes" when you see the pop-up asking "Do you want to allow this app to make changes to your device?"

7. The Gpg4win setup wizard will appear:

	- Click "Next" in the first page of the wizard.
	
        ![](../../../media/thunderbird-en/windows/winPGPgpg4winInstall.PNG)
	
	- Pick components you wish to download. The components checked in the screenshot below will allow you to encrypt both emails and files stored on your computer.
   
		- **Uncheck GpgOL**, unless you use Outlook (not recommended), as well as **Browser integration**.

	    - Finally, click "Next".
	
            ![](../../../media/thunderbird-en/windows/winPGPgpg4winChooseComponents.PNG)
	
	- Select a destination folder - using the default is fine. Click "Install".
	
        ![](../../../media/thunderbird-en/windows/winPGPgpg4winInstallLocation.PNG)
	
	- In the "Installation Complete" page, click "Next".
	
        ![](../../../media/thunderbird-en/windows/winPGPgpg4winInstallComplete.PNG)
	
	- Click "Finish" in the final page of the setup wizard.
	
        ![](../../../media/thunderbird-en/windows/winPGPgpg4winSetupComplete.PNG)

## 2.2 Download and Install Thunderbird

Thunderbird is an email client that can be used to download and send email, and that you can use together with an add-on called Enigmail to encrypt your email.

1. Visit [Thunderbird's website](https://www.thunderbird.net/) to download Thunderbird.

2. Click the green "Free Download" button.

    ![](../../../media/thunderbird-en/windows/winPGPThunderbirdDownload.PNG)
   
3. When prompted, select "Save File".

    ![](../../../media/thunderbird-en/windows/winPGPThunderbirdDownloadSave.PNG)
   
4. Click the download arrow in the top right corner of your browser. Click the "Thunderbird Setup version .exe" file in your recent downloads.

    ![](../../../media/thunderbird-en/windows/winPGPThunderbirdRecentDownload.PNG)

5. Click "Yes" when you see the pop-up asking "Do you want to allow this app to make changes to your device?"

6. Complete the Thunderbird Installation Wizard:

    * Click "Next" in the Setup Wizard welcome page.

        ![](../../../media/thunderbird-en/windows/windowsPgpThunderbirdInstallWizard1.png)

    * Choose the "Standard" installation and click "Next".
   
        ![](../../../media/thunderbird-en/windows/winPGPThunderbirdSetupType.PNG)
      
        *Note: Thunderbird will be set as your default mail application. If you do not wish for this to be the case, **uncheck the "Use Thunderbird as my default mail application"** option at the bottom of this screen.*

    * Click "Install" to allow Thunderbird to be installed in the selected directory.
   
        ![](../../../media/thunderbird-en/windows/winPGPThunderbirdInstallLocation.PNG)

    * Click "Finish" to complete the installation. Make sure to launch Thunderbird before continuing to the "Download and Install Enigmail" Section. 
   
        ![](../../../media/thunderbird-en/windows/winPGPThunderbirdCompleteSetup.PNG)
 
*Note: When Thunderbird is launched for the first time, it will ask if you would like to set up an existing email account. We will explain how to set up your email account in Thunderbird in the ["Link Thunderbird to Your Existing Email Account" section below](#setup_email_tb). You can click "Cancel" to continue following this guide, or, if you know what you're doing, set up your existing email account now.*


## 2.3 Download and Install Enigmail

Enigmail is an add-on for Thunderbird to encrypt, decrypt and sign emails, and to manage PGP keys.

1. Open Thunderbird and **click the three-bar menu button** in the top-right corner.

    ![](../../../media/thunderbird-en/windows/winPGPThunderbirdMenuBar.PNG)

2. Click "Add-ons" → "Add-ons".

    ![](../../../media/thunderbird-en/windows/winPGPThunderbirdMenuBarAddOns.PNG)

3. Click "Extensions" in the left-hand menu, type "Enigmail" in the search field in the top-right corner and hit Enter. The first result should read "Enigmail" - click the "Add to Thunderbird" button.

    ![](../../../media/thunderbird-en/windows/winPGPEnigmailSearch.PNG)
   
4. In the Software Installation prompt, click "Install Now". 

    ![](../../../media/thunderbird-en/windows/winPGPEnigmailInstallNow.PNG)

5. When the installation of Enigmail is done, a notification will appear in the top left corner. Restart Thunderbird to let the add-on finalize its installation.

    ![](../../../media/thunderbird-en/windows/winPGPEnigmailSuccessfullyInstalled.PNG)


<a name="setup_email_tb"></a>

## 2.4 Link Thunderbird to Your Existing Email Account

*Optional:* Show the Thunderbird Menu Bar. *This step will be useful for the rest of this guide.*

In Thunderbird, click the three-bar menu button, then Options and  check "Menu Bar".

![](../../../media/thunderbird-en/windows/winPGPCheckMenuBar.PNG)


1. Add your existing email account to Thunderbird by navigating to Thunderbird's menu bar. Click "File", then click "New", then "Existing Mail Account".

    ![](../../../media/thunderbird-en/windows/winPGPExistingMailAccount.PNG)

2. Fill out your complete name, your email address and your password. Then uncheck "Remember password".

    *It’s important to fill out your first and last name if you want the receiver to recognize you. But this is not mandatory: you can enter anything you would like to be addressed with.*

    Please note that if you have enabled 2-factor authentication on your email account, you might have to enter an app-specific password here. This won't be necessary if you are using a Gmail account.

    ![](../../../media/thunderbird-en/windows/winPGPExistingMailInput.PNG)

    *Note: if you prefer Thunderbird to store your password, you should make sure that the password database is encrypted, so that nobody who has access to your computer can get to your email password. To do this, you will need to set a strong master password, following [these instructions](https://support.mozilla.org/en-US/kb/protect-your-thunderbird-passwords-master-password).*

3. Click "Continue". Thunderbird will check that your email configuration is correct and will present you with two options for reading your incoming email: IMAP or POP3.

    Leave "IMAP (remote folders)" selected if you want to access the same messages from multiple devices, leaving your email in the servers.

    If you prefer to use Thunderbird for all your email, and to download your email to a trusted secure device rather than keep it in servers you don't control, select the "POP3 (keep mail on your computer)" option. Please note that by choosing this option, the messages you have downloaded will no longer be accessible from other devices. For more details on the IMAP and POP3 options, check out this [Wikipedia page](https://en.wikipedia.org/wiki/Post_Office_Protocol#Comparison_with_IMAP).

    ![](../../../media/thunderbird-en/windows/winPGPExistingMailConfig.PNG)

    **Important: Make sure that both the Incoming and Outgoing information shown on the screen above display SSL (Secure Sockets Layer) or STARTTLS (Start Transport Layer Security). Either one indicates that your connection to your email provider will have a basic layer of encryption.**

If everything has worked fine, by clicking the "Done" button Thunderbird will connect to your email server and download your mailbox. This operation will happen in the background and might take some time, depending on the size of your inbox folder and speed of your internet connection.

*If you are using a Gmail account, you will be prompted to enter your password and 2-factor authentication code and to accept the connection to an email client.*


## 2.5 Create a Private and Public Key: Your PGP Key Pair

Once your email account is connected to Thunderbird, you can start using PGP by creating a key pair, consisting in a **private key**, which you must **store safely and never share with anyone**, and a **public key**, which you can share publicly or with trusted people.

1. In the Thunderbird menu bar, click "Enigmail", then select "Key Management".

    ![](../../../media/thunderbird-en/windows/winPGPEnigmailKeyManagement.PNG)

2. Enigmail's key manager window will appear. Select "Generate" from the top menu and click "New Key Pair".

    ![](../../../media/thunderbird-en/windows/winPGPGenerateNewKeyPair.PNG)

3. Choose a strong and unique passphrase for your key (if you don't know how to generate a strong password, please read [this guide](../../passwords)). In a nutshell, your passphrase should be at least 20-characters long and include special characters, capital letters and numbers.

    In the "Key expires in" box, you can leave the default 5 years option, or select a shorter time span. Don't choose a longer time span, and **leave the "Key does not expire" option unchecked**.

    ![](../../../media/thunderbird-en/windows/winPGPGenerateOpenPGPKeyPage.PNG)

4. Click on the "Advanced" tab next to "Key Expiry", and **make sure the key size is 4096** and the **key type is RSA**.

    ![](../../../media/thunderbird-en/windows/winPGPConfirmKeySize4096.PNG)

5. Click "Generate Key" and confirm to generate your key pair.

    ![](../../../media/thunderbird-en/windows/winPGPConfirmGenerateKey.PNG)
    

## 2.6 Generate a Revocation Certificate

While Enigmail is generating your key pair, you will also be asked to generate a revocation certificate. Generating a revocation certificate is important and you should click on the "Generate Certificate" button in the prompt, and save the certificate in a secure location (like an encrypted USB stick that you keep for exclusive, personal use).

![](../../../media/thunderbird-en/windows/winPGPGenerateRevocationCertificateAsk.PNG)

This file is needed only to revoke your key pair in case you lose your private key or there are doubts about it being compromised. The revocation certificate cannot be used to decrypt your PGP-encrypted communications. 

The revocation certificate will allow you to securely discontinue the usage of your key at any time. It is very important to create a revocation certificate for future use and store it in a secure place that only you or trusted people can access and that is separate from the device where you keep your key pair. Revoking your key will prevent people from encrypting to the unused or compromised key, and signals to the keyservers that the key is no longer valid.


## 2.7 Generate a Revocation Certificate for an Existing Key Pair

If you did not create a revocation certificate while generating your key pair, you should do so now, following the steps below:

1. Go to the Thunderbird menu bar, click "Enigmail", then choose "Key Management".

    ![](../../../media/thunderbird-en/windows/winPGPEnigmailKeyManagement.PNG)

2. Find the key that you would like to generate a revocation certificate for, and right-click it. 

3. Select "Generate & Save Revocation Certificate" from the drop-down menu.

    ![](../../../media/thunderbird-en/windows/winPGPKeyManagementGenerateRevocationCertificate.PNG)

4. Choose a secure location to save your revocation certificate. An encrypted USB stick that you do not lend out and is kept for exclusive, personal use is a good choice. Make sure to keep the storage device where you have saved the certificate in a secure place.

5. Type in the passphrase for your PGP key and click "OK".

    ![](../../../media/thunderbird-en/windows/winPGPEnterPassphrase.PNG)

6. You will be notified that the revocation certificate has been successfully generated. Click "Close".

    ![](../../../media/thunderbird-en/windows/winPGPGenerateRevocationSuccessful.PNG)
    

## 2.8 Back up Your Key Pair Somewhere Safe

Backing up your key pair in a safe location is important to be sure you will not lose it even if your device is lost or your hard disk damaged, or if you want to encrypt your emails with the same key pair on another computer.

1. Open the Key Management window.

    ![](../../../media/thunderbird-en/windows/winPGPEnigmailKeyManagement.PNG)
      
2. Right-click your key in the Key Management window and choose "Export Keys to File" in the drop-down menu.
   
    ![](../../../media/thunderbird-en/windows/winPGPExportKeystoFile.PNG)
   
3. Choose explicitly to "Export Secret Key".

    ![](../../../media/thunderbird-en/windows/winPGPExportSecretKeys.PNG)
   
4. Choose where you would like the keys to be saved. 

    *Note: The private key in your key pair is the most important component of the encryption system, and its security should be top priority. **Only export your key pair to a secure place**, like an encrypted USB stick that you use exclusively, store in a secure place, and do not lend out.*

    ![](../../../media/thunderbird-en/windows/winPGPExportKeytoUSB.PNG)

5. Upon clicking "Save", you will be asked to type in the passphrase for your PGP key and click "OK". You should see a message stating that "The keys were successfully saved".

    ![](../../../media/thunderbird-en/windows/winPGPKeysSuccessfullySaved.PNG)
    

## 2.9 Publish Your Public Key

Your contacts will need to import your public key to encrypt messages to you. To let others find your public key, you may upload your public key to a key server or [attach the key in an email](https://www.enigmail.net/documentation/Key_Management#Share_your_public_key_manually). Public keys uploaded to key servers are searchable by the email address used or PGP Key ID.

**Warning:** If you don't want to disclose publicly that you're using encryption, for example because encryption is illegal in your country, you should avoid publishing your public key on the key servers. In such cases it's a good idea to only send your public key by email or through other means to your trusted contacts you exchange encrypted emails with.

To upload your public key to a keyserver, follow these instructions:

1. Go to Thunderbird's menu bar. Click "Enigmail", then "Key Management".

    ![](../../../media/thunderbird-en/windows/winPGPEnigmailKeyManagement.PNG)

2. Right-click your key and choose "Upload Public Keys to Keyserver" in the drop-down menu.

    ![](../../../media/thunderbird-en/windows/winPGPUploadKeystoKeyserver.PNG)

3. Wait for the key upload to finish.

    ![](../../../media/thunderbird-en/windows/winPGPKeyUploadtoKeyserver.PNG)


<a name="update-thunderbird-settings"></a>
# 3. Improve Thunderbird's security and usability

This section explains how to configure Thunderbird's preferences to help defend your system against attacks that originate in emails. For more information, see [Protect yourself from Malware & Hackers](../../phones-and-computers/malware/).

## 3.1 OpenPGP Security

1. Navigate to Thunderbird's menu bar, select "Tools", then "Account Settings".

    ![](../../../media/thunderbird-en/windows/winPGPThunderbirdAccountSettings.PNG)

2. Click "OpenPgp Security" in the left-hand menu. All "Message Composition Default Options" should be checked, to encrypt messages and drafts and sign messages by default. Make sure "Use specific OpenPGP key ID" is selected, with your public key ID below it. Also make sure that "Prefer Enigmail (OpenPGP)" is checked. Click "OK".

    ![](../../../media/thunderbird-en/windows/winPGPSecuritySettings.PNG)

    *If you are not reading and sending unencrypted email from a webmail interface, and are using Thunderbird for managing all your email, you might want to uncheck the "Encrypt messages by default" option, to make it easier to write unencrypted messages. If you choose to uncheck this box, you should always make sure that the lock icon in your email form is highlighted as in the screenshot below when writing an encrypted email.*

    ![](../../../media/thunderbird-en/windows/winPGPLockIcon.PNG)


## 3.2 Autocrypt

Autocrypt is part of the default configuration of Enigmail.

Autocrypt aims at making encryption easier, but if it isn't properly configured, it may disable encryption automatically for some email addresses.

To set up Autocrypt properly, **it is important to change some settings** by following these instructions:

1. Navigate to Thunderbird's menu bar, select "Tools", then "Account Settings".

    ![](../../../media/thunderbird-en/windows/winPGPThunderbirdAccountSettings.PNG)

2. Click "OpenPgp Security" in the left-hand menu and select the "Autocrypt" tab (to the right of "Message Composition"). Check the following options:

    * Enable Autocrypt
    * Prefer encrypted emails from the people you exchange email with
    
    ![](../../../media/thunderbird-en/windows/winPGPAutocrypt.PNG)
    

## 3.3 Enigmail Preferences

1. In Thunderbird's menu bar, go to Enigmail → Preferences, and verify your **passphrase is remembered** for at least **30** and not more than **60** minutes of inactivity.

    ![](../../../media/thunderbird-en/windows/winPGPEnigmailPreferences1.PNG)
 
2. Click the "Display Expert Settings and Menus" button in the Preferences window, and select the "Sending" tab.

    - in the "General Preferences for Sending" section, check the "Manually configure encryption settings" option.
    - In the "Confirm before sending" section, check "If unencrypted".
    
    ![](../../../media/thunderbird-en/windows/winPGPEnigmailPreferences2.PNG)

    *This step is optional if you are using Thunderbird to send unencrypted email, but please remember to make sure that the lock icon in your email form is highlighted as in the screenshot below when writing an encrypted email.*

    ![](../../../media/thunderbird-en/windows/winPGPLockIcon.PNG)

3. Go to the "Key Selection" tab and uncheck the "By Per-Recipient-Rules" option.

    ![](../../../media/thunderbird-en/windows/winPGPEnigmailPreferences3.PNG)

<a name="subject-encryption"></a>

## 3.4 Subject Encryption

Enigmail can also encrypt the subject of your message (please note that other metadata will still be visible).

If the subject is encrypted, then the visible subject is replaced with a dummy text like "Encrypted Message". It is a good practice to encrypt your subject by default, but in some cases you may need to disable this feature.

In order to disable subject encryption, go to Enigmail → Preferences → click "Display Expert Settings and Menus", then go to the "Advanced" tab and uncheck the "Encrypt subject by default" option.
    
   ![](../../../media/thunderbird-en/windows/winPGPEncryptSubject.PNG)


## 3.5 Disable HTML and remote content

Email messages can contain content such as scripts in the HTML code, images, or stylesheets that can violate your privacy or be used for malicious intents. To protect your privacy and to mitigate the risk of attacks or vulnerabilities, it is always a good idea to disable HTML composition and rendering, and to block remote content.

### 3.5.1 Disable HTML email composition

1. Navigate to Thunderbird's menu bar, select "Tools", then "Account Settings".

    ![](../../../media/thunderbird-en/windows/winPGPThunderbirdAccountSettings.PNG)

2. Select the "Composition & Addressing" tab and uncheck the "Compose messages in HTML format" option.

    ![](../../../media/thunderbird-en/windows/windowsPgpThunderbirdCompositionAddressing.png)
    

### 3.5.2 Disable HTML email rendering

1. Click the three-bar menu button in the top-right corner of the main Thunderbird window and click View → Message Body As → Plain Text.

    ![](../../../media/thunderbird-en/windows/windowsPgpTBViewPlainText.png)

### 3.5.3 Disabling remote content in messages

*Remote content should be disabled by default in Thunderbird, but it's a good idea to check.*

1. Navigate to Thunderbird's menu bar, select "Tools", then "Options".

    ![](../../../media/thunderbird-en/windows/windowsPgpTBOptions.png)

2. Select the "Privacy" tab and make sure that the "Allow remote content in messages" is unchecked.

    ![](../../../media/thunderbird-en/windows/windowsPgpTBremotecontent.png)
    

## 3.6 Add Your PGP ID to Your Email Signature

1. Navigate to Thunderbird's menu bar, select "Tools", then "Account Settings".

    ![](../../../media/thunderbird-en/windows/winPGPThunderbirdAccountSettings.PNG)

2. Find your PGP key under "OpenPGP Security" and copy it. 

    ![](../../../media/thunderbird-en/windows/winPGPSecuritySettingsFindPGPKey.PNG)
   
3. In the left-hand menu, click on your email address, then paste your public key into the box for the Signature text.

    ![](../../../media/thunderbird-en/windows/winPGPSignatureText.PNG)


# 4. Sending and receiving encrypted messages

**GNU Privacy Guard (GnuPG)** is free *cryptographic* software that was developed by the GNU Project. This software is compliant with the OpenPGP standard and was designed to inter-operate with Pretty Good Privacy (PGP), another email encryption program that was initially designed and developed by Phil Zimmermann.

**GnuPG**  relies on a form of *public-key cryptography* that requires each user to generate his or her own pair of *keys*. This *key pair* can be used to encrypt, decrypt and sign digital content such as email messages. It includes a *private key* and a *public key*:

- Your **private key** is extremely sensitive. Anyone who managed to obtain a copy of this key would be able to read encrypted content that was meant only for you. They could also sign messages so they appeared to have *come from* you. Your *private key* is, itself, encrypted to a passphrase that you will choose when generating your *key pair*. You should choose a strong passphrase and take care not to let anyone gain access to your *private key*. You will use your *private key* to decrypt messages sent to you by those who have a copy of your *public key*.

- Your **public key** is meant to be shared with others and cannot be used to read an encrypted message or fake a signed one. Once you have a correspondent’s public key, you can begin sending her encrypted messages. Only she will be able to decrypt and read these messages because only she has access to the *private key* that matches the *public key* you are using to encrypt them. Similarly, in order for someone to send *you* encrypted email, they must obtain a copy of your *public key*. It is important to verify that the *public key* you are using to encrypt email actually does belong to the person with whom you are trying to communicate. If you or your correspondent are tricked into encrypting email with the wrong public key, your conversation will not be secure.

**GnuPG** and **Enigmail** also let you attach **digital signatures** to your messages. If you *sign* a message using your *private key*, any recipient with a copy of your *public key* can verify that it was sent by you and that its content was not tampered with. Similarly, if you have a correspondent's *public key*, you can verify her digital signatures.

## 4.1 Download Your Receiver’s Public Key

1. Go to Thunderbird's menu bar. Click "Enigmail", then "Key Management".

    ![](../../../media/thunderbird-en/windows/winPGPEnigmailKeyManagement.PNG)

2. In the menu bar of the Key Management window, select "Keyserver", then "Search for Keys".

    ![](../../../media/thunderbird-en/windows/winPGPSearchKeyserverKeys.PNG)

3. Search for a key by typing the email address of the receiver or the ID of their PGP key.

    ![](../../../media/thunderbird-en/windows/winPGPSearchKeyBox.PNG)

4. Select the key you need from the import window and click "OK".

    ![](../../../media/thunderbird-en/windows/winPGPImportKey.PNG)

5. A message will appear saying that the key has been imported. Click "OK".

    ![](../../../media/thunderbird-en/windows/winPGPKeyImportedSuccessfully.PNG)

    Once the public key of the receiver is downloaded, you can send them an encrypted email.


## 4.2 Verify the Fingerprint of the Downloaded Public Key

To be sure that the public key you've just downloaded is the right one, and that nobody has tampered with it, it's a good practice to check its fingerprint.

1. To verify your receiver's PGP key, you need to get its fingerprint directly from its owner. In some cases, you might have your contact's business card with their PGP fingerprint included in it, or your contact may have published their PGP fingerprint in their official website. In other cases you will have to ask your receiver for the fingerprint. This communication should happen over a secure channel alternative to email or in person.

2. Go to Thunderbird's menu bar. Click "Enigmail", then "Key Management".

    ![](../../../media/thunderbird-en/windows/winPGPEnigmailKeyManagement.PNG)

3. In the "Key Management" window, search for your receiver's address to identify their key.

    ![](../../../media/thunderbird-en/windows/EnigmailKeyManagementSearch.png)

4. Right-click your receiver's key and select "Key Properties".

    ![](../../../media/thunderbird-en/windows/EnigmailKeyProperties.png)

5. In the "Key Properties" window for the selected key, you will see the fingerprint of the key. Check that it matches with the key you obtained from your contact. If the fingerprints match, you can now be sure that your receiver will be able to decrypt your email and that nobody has tampered with their PGP key.

    ![](../../../media/thunderbird-en/windows/EnigmailKeyFingerprint.png)


## 4.3 Write and Send an Encrypted Email

1. In Thunderbird, click the "Write" button in the top left corner.

    ![](../../../media/thunderbird-en/windows/winPGPWriteEmail.PNG)

2. Enter the email address of the person you want to write to in the "To:" field and make sure that both the **lock and pencil icons are highlighted**. This shows the email is going to be encrypted and signed. If the icons are not highlighted by default, make sure that you have imported your contact's public PGP key and [review Thunderbird's security settings](#update-thunderbird-settings) before continuing.

    ![](../../../media/thunderbird-en/windows/winPGPSendEmailSecurityIcons.PNG)

3. Write the "Subject" and your email message. Click "Send" when complete.

    If subject line encryption is enabled in your [Enigmail Settings](#subject-encryption), your subject line will be encrypted by default. If you have left that option unchecked, or have unchecked it for specific reasons, **you should be very careful not to write private information in the subject line under any circumstances**.
    
    ![](../../../media/thunderbird-en/windows/winPGPEncryptedEmail.PNG)

4. After clicking Send, you must fill out your PGP passphrase for signing the message and click "OK".

    ![](../../../media/thunderbird-en/windows/winPGPEnterPassphrase.PNG)


# 5. Extend Your Expired Key

If your key reaches its expiration date, you can always extend it as long as you still have access to the private key. To do so, you can follow these instructions:

1. Go to Thunderbird's menu bar. Click "Enigmail", then "Key Management".

    ![](../../../media/thunderbird-en/windows/winPGPEnigmailKeyManagement.PNG)

2. In the Key Management window, **right-click** the expired key and select "Change Expiration Date" in the drop-down menu.

    ![](../../../media/thunderbird-en/windows/winPGPExpiredKey.PNG)
   
    *Note: An expired key can be easily identified in Enigmail, as its font format changes to italics.*
   
3. Make sure that the primary and subkey are checked. Set the expiration date to **5 years at most** and click "OK".

    ![](../../../media/thunderbird-en/windows/winPGPExpiredKeyRenew.PNG) 
   
4. Type in the passphrase for your PGP key and click "OK".

    ![](../../../media/thunderbird-en/windows/winPGPExpiredKeyPassphraseEnter.PNG)
   
5. **Right-click** the key you have just extended, and select "Upload Public Keys to Keyserver".

    ![](../../../media/thunderbird-en/windows/winPGPKeyUpload.PNG)
   
    **Warning:** This step is necessary to keep the public keyring details up-to-date in the publicly accessible keyservers. If you have chosen not to publish your PGP key, you should send the extended key to your contacts instead of spreading the updated key to the servers.
   
6. Wait for the key upload to finish.

    ![](../../../media/thunderbird-en/windows/winPGPKeyUploadtoKeyserver.PNG)

    Congratulations! You have everything correctly set up for encrypting your email!


# FAQ

***Q***: What happens if I just install **Enigmail** and not **GnuPG**?

***A***: That's simple, really. **Enigmail** just won't work. After all, it's the **GnuPG** software that provides the encryption engine that **Enigmail** uses.

***Q***: How many email accounts can I set up in **Thunderbird**?

***A***: As many as you like! **Thunderbird** is an email manager and can easily handle 20 or more email accounts!

***Q***: My friend has a **Gmail** account. Should I convince her to install **Thunderbird**, **Enigmail** and **GnuPG**?

***A***: That would be ideal. Just make sure she configures all of her security settings in exactly the same way as you did. Then the two of you will have an extremely effective way of communicating in privacy and safety!

***Q***: I still don't understand the purpose of digitally signing my messages.

***A***: A digital signature proves that you're the real sender of a particular message and that the message hasn't been tampered with on its way to your intended recipient. Think of it as the electronic equivalent of the wax seal on an envelope containing a very important letter.
