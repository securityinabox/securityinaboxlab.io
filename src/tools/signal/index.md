---
weight: 010
title: Signal for Android
collection: menu
post_date: 10 August 2016
logo: ../../../media/tool/logo/signal.png
website: https://www.whispersystems.org
download_links: 
  - 
    label: Download link
    url: https://play.google.com/store/apps/details?id=org.thoughtcrime.securesms&hl=en
version: 3.12.0
license: Free and Open Source Software
system_requirements:
  - Android
  - iOS
---

**Signal** is free and open source software that protects your messages and voice calls using end-to-end encryption. It is developed by [Open Whisper Systems](https://www.whispersystems.org) and is available for Android smartphones and iPhones. A [beta release](https://github.com/WhisperSystems/Signal-Desktop) of [Signal Desktop](https://whispersystems.org/blog/signal-desktop/) is also available as an extension for Google's Chromium and Chrome web browsers, but it requires that you *also* install Signal on an Android smartphone.

# Required reading

- [Use mobile phones as securely as possible](../../mobile-phones)
- [Use smartphones as securely as possible](../../smartphones)

# What you will get from this guide

- The ability to exchange encrypted messages with other Signal users.
- The ability to have encrypted voice calls with other Signal users.
- The ability to encrypt the messages on your phone by setting a passphrase. 

# 1. Introduction to Signal

We recommend Signal for mobile messaging and voice calls because:

* It is free and open source software, so its code is open for independent review
* It protects your messages and voice calls using strong, modern, well documented end-to-end encryption protocols
* It allows you to [verify the identity of your contacts](#identity-verification). 
* It implements *forward secrecy*, so past conversations remain safe even if someone steals the encryption key used to protect a given message
* It is *asynchronous*, so messages that were sent to you while you were offline will be waiting for you when you connect to the service
* It encrypts your message history on the device if you set a passphrase

However, it's important to bear the following in mind:

* Signal only encrypts messages that you exchange, using your device's mobile data or wireless internet connection, with other Signal users. The Android version can also send and receive regular SMS text messages, but it does not encrypt them
* While Signal prevents others from accessing the content of your messages and voice calls, it does not hide the fact that you are sending encrypted messages or making encrypted voice calls
* In order to register Signal, you must be willing and able to receive an SMS text message or a phone call from a number in the United States
* Signal can only be installed via Google's Play Store, which means that you need to have a Google account which will be linked to the installation of the app
* Signal’s reliance on the Google Cloud Messaging platform means that Google can have access to *some* of the metadata produced by Signal (such as the IP address of any device that receives a Signal message)
* In some countries, encryption tools like Signal might attract attention or violate legal constraints

**Note:** If you are interested in using the Signal protocol for encrypted mobile communications independently from Google Play Services, you can install [LibreSignal](https://libraries.io/github/LibreSignal/LibreSignal) (a fork of Signal) instead via [F-Droid](https://f-droid.org/), a free and open source Android app repository. 

## 1.0. Other tools like Signal

* **Android:** [Surespot](https://www.surespot.me/), [ChatSecure](https://guardianproject.info/apps/chatsecure/), [Silent Circle](https://silentcircle.com/products-and-solutions/software/), [Conversations](https://github.com/siacs/Conversations), [OSTel](https://ostel.co/)
* **iOS:** [Surespot](https://www.surespot.me/), [ChatSecure](https://guardianproject.info/apps/chatsecure/), [Silent Circle](https://silentcircle.com/products-and-solutions/software/), [OSTel](https://ostel.co/)

# 2. Install and configure Signal

## 2.1. Install Signal

You can install Signal on your Android device by following the steps below:

**Step 1.** **Download** and **install** Signal from the [Google Play](https://play.google.com/store/apps/details?id=org.thoughtcrime.securesms) store by tapping ![](../../../media/en/signal/textsecure-en-1/001.png).

 ![](../../../media/en/signal/signal-and-en-v02-001.png)

*Figure 1: Signal on the Google Play Store*

**Step 2.** Before the installation process begins, you will be asked to review the access that the app will have on your phone. Review this carefully. Once your are happy with the permissions allowed, press ![](../../../media/en/signal/textsecure-en-1/003.png) and the installation will complete. If you do not agree with the permissions allowed, press the back button and the installation will be cancelled.

 ![](../../../media/en/signal/signal-and-en-v02-002.png)

*Figure 2: Permissions required*

**Step 3.** **Tap** *Open* to run the app for the first time.

 ![](../../../media/en/signal/signal-and-en-v02-003.png)

*Figure 3: Opening Signal*

## 2.2. Initial setup

### 2.2.1. Register Signal

To register Signal, you must be willing and able to receive an SMS text message or a phone call from a number in the United States. 

You can begin the registration process by following the steps below:

**Step 1.** Begin the registration process by entering your phone number, including the country code, and pressing ![](../../../media/en/signal/textsecure-en-1/005.png).

 ![](../../../media/en/signal/signal-and-en-v02-101.png)

*Figure 1: Registration screen*

**Step 2.** Signal will send you an SMS message to confirm registration.

**Note:** If for some reason you do not receive the SMS you will be given the option to receive an automated phone call, where you will hear a 6-digit code. This code, once entered, will complete the registration.
 
![](../../../media/en/signal/signal-and-en-v02-105.png)

*Figure 2: Registration completing*

**Disclaimer:** Burner SIM cards have been used throughout this guide. 

### 2.2.2. Making Signal your default messaging app

In addition to exchanging encrypted messages with other Signal users, the Android version of Signal can also replace your normal SMS text messaging application. Used in this way, it has the ability to send unencrypted text messages to — and display unencrypted text messages from — non-Signal users. These messages are relayed by your mobile service providers rather than being sent over the Internet.

To use Signal as your default SMS text messaging app, look for the notification below: 

![](../../../media/en/signal/signal-and-en-v02-007.png)

*Figure 1: Signal offering to replace your default SMS app*

**Step 1**. **Tap** the message above when it appears.

![](../../../media/en/signal/signal-and-en-v02-008.png)

*Figures 2: Confirming the use of Signal as your default SMS app*

**Step 2**. **Tap** **YES** to confirm.

### 2.2.3. Importing messages

If you configured Signal to be your default SMS app, we recommended that you import your existing messages. That way, all of your messages will be in one place. And, because Signal has the ability to encrypt the messages stored on your device, this will protect your SMS history in the event that someone gets access to your smartphone.

You can do this by following the steps below:

**Step 1.** After registration is complete, Signal will open and you will be asked if you want to import your messages from the default SMS app on your phone. Tap on the message to do this.

![](../../../media/en/signal/signal-and-en-v02-006.png)

*Figure 1: Import messages into Signal*

Once the import is complete you will be able to access your old messages in Signal. You should then delete them from your other application.

### 2.2.4. Using encrypted storage

Signal also allows you to store all your messages in an encrypted container protected by a passphrase, so if someone gains access to your phone, they will not be able to access your messages when Signal is locked. After a set period of time, if you have not looked at any messages, Signal will automatically lock and you will have to enter your passphrase the next time you want to read your messages.

You can store your messages in an encrypted container by following the steps below:

**Step 1.** Tap on ![](../../../media/en/signal/textsecure-en-1/015.png) in the top right of your screen to bring up the menu and select **Settings**.

**Step 2.** Scroll down to the **Privacy** section and swipe right next to *'Enable passphrase'*.

![](../../../media/en/signal/signal-and-en-v02-009.png)

*Figure 1: Passphrase options*

**Step 3.** When prompted enter a *passphrase* that will be used to protect the messages stored on your phone and tap **OK**.

![](../../../media/en/signal/signal-and-en-v02-010.png)

*Figure 2: Setting a passphrase*

**Step 4.** Check the box next to **Inactivity timeout passphrase** to have Signal lock after a period of inactivity.

![](../../../media/en/signal/signal-and-en-v02-011.png)

*Figure 3: Inactivity timeout passphrase option*

**Step 5.** Tap **Inactivity timeout interval** and in the following screen enter after what time you want Signal to lock if unused and tap **OK**.

![](../../../media/en/signal/signal-and-en-v02-012.png)

*Figure 4: Signal timeout settings*

## 2.3. Inviting your contacts to Signal

You can invite your contacts on Signal by following the steps below:

**Step 1.** Open Signal. 

**Step 2.** From the main screen, tap on the *pencil icon* at the bottom right corner to open up your list of contacts. 

![](../../../media/en/signal/signal-and-en-v02-102.png)

*Figure 1: Main Signal screen*

**Step 3.** Select the contact you want to invite on Signal.

![](../../../media/en/signal/signal-and-en-v02-103.png)

*Figure 2: Selecting a contact on Signal*

**Step 4.** Tap on the blue **Invite to Signal** message to invite your contact on Signal.

![](../../../media/en/signal/signal-and-en-v02-015.png)

*Figure 3: Invite friends on Signal*

**Step 5.** Tap on the send icon at the bottom right corner of the screen to send the **"Let's switch to Signal http://sgnl.link/1LoIMUI"** message to your contact. 

![](../../../media/en/signal/signal-and-en-v02-016.png)

*Figure 4: Switching to Signal*

You will receive a notification once your contact has joined Signal:

![](../../../media/en/signal/signal-and-en-v02-017.png)

*Figure 5: Contact joined Signal*

You can further confirm if your contacts are on Signal based on whether there is a padlock next to the telephone icon on the top right corner next to their name:

![](../../../media/en/signal/signal-and-en-v02-018.png)

*Figure 6: Padlock verification of Signal usage*

# 3. Encrypting text messages with Signal

Signal will only exchange encrypted messages with contacts who are also using Signal, and it will only do so over a mobile data or wireless Internet connection. The Android version of Signal allows you to send and receive regular SMS text messages as well, but it does not encrypt them. As a result, your mobile service provider has full access to the content of such messages. 

When you send an encrypted, Signal-to-Signal message, no one but the intended recipient can read what you have written. This includes your mobile service provider, Open Whisper Systems and Google, none of which have access to the *content* of these messages. It also includes images, videos and other attachments. 

However, It’s important to note that while the Signal protocol encrypts the content of our communications, it does *not* encrypt *metadata* – information *about* information - such as who we contact, when and from where. It’s also worth noting that Signal’s reliance on the Google Cloud Messaging platform means that Google can have access to *some* of the metadata produced by Signal, such as the *IP address* of any device that receives a Signal message.


## 3.1. Messaging individuals

You can send encrypted instant messages to your contacts using Signal by following the steps below:

**Step 1.** Open Signal and tap on the *pencil icon* at the bottom right corner of your screen to bring up your contact list.

![](../../../media/en/signal/signal-and-en-v02-102.png)

*Figure 1: Main Signal screen*

**Step 2.** Tap on the contact you wish to message.

![](../../../media/en/signal/signal-and-en-v02-103.png)

*Figure 2: Selecting a contact on Signal*

**Note:** The contact list will display at the top all your contacts who also use **Signal** (under the **Signal Users** heading) and then your full contact list (including Signal users) in the **All Contacts** section.

**Step 3.** Compose your message in the box and tap ![](../../../media/en/signal/textsecure-en-1/024.png) to send it.

![](../../../media/en/signal/signal-and-en-v02-023.png)

*Figure 3: Sending a message over Signal*

The Android version of Signal allows you to choose whether you want to send your contact an encrypted Signal message or an unencrypted *Insecure SMS* text message. To set this preference for a given user, you can **long press** ![](../../../media/en/signal/textsecure-en-1/024.png). Signal will remember your preference, so you will need to *long press* the *send* button again if you want to switch back.  

![](../../../media/en/signal/signal-and-en-v02-024.png)

*Figure 4: Sending options*

**Important:** A closed padlock beneath a message, as shown below, indicates that it was sent encrypted (if the padlock is open, then the message was sent unencrypted). Furthermore, if there is only *one* check mark next to the padlock, then the message was only sent to the server. If there are *two* check marks (as illustrated below) next to the padlock, then the message was delivered to the recipient (though there is no guarantee that it has been read). 

![](../../../media/en/signal/signal-and-en-v02-025.png)

*Figure 5: Exchange of encrypted messages*

## 3.2. Messaging groups

Signal also allows you to send encrypted instant messages to multiple people at once. **However, if at least one of the people you are messaging does *not* use *Signal*, the messages will be sent as an MMS and *not* encrypted.**

You can send messages at once to multiple people by following the steps below:

**Step 1.** Tap on ![](../../../media/en/signal/textsecure-en-1/015.png) in the top right of your screen to bring up the menu and select ![](../../../media/en/signal/textsecure-en-1/034.png).

![](../../../media/en/signal/signal-and-en-v02-026.png)

*Figure 1: Selecting New Group on Signal*

**Step 2.** Enter a name for your chat group and tap ![](../../../media/en/signal/textsecure-en-1/035.png) to add your contacts.

![](../../../media/en/signal/signal-and-en-v02-027.png)

*Figure 2: Creating a chat group on Signal*

**Step 3.** Tap the box to the right of each contact's name to add them to the group and press ![](../../../media/en/signal/textsecure-en-1/036.png).

**Step 4.** Tap ![](../../../media/en/signal/textsecure-en-1/036.png) on top-right to complete the creation of the group and be brought back to the **Signal** main screen.

**Step 5.** Tap on the group you have created and begin messaging the group.

![](../../../media/en/signal/signal-and-en-v02-028.png)

*Figure 3: Sending encrypted messages to multiple contacts at once over Signal*

![](../../../media/en/signal/signal-and-en-v02-029.png)

*Figure 4: Group messaging over Signal*

## 3.3. Sending files

Signal allows you to send images, video and audio files to your contacts. You can do this by following the steps below:

**Step 1.** Start a conversation with the person you want to send a file to.

**Step 2.** Tap on ![](../../../media/en/signal/textsecure-en-1/015.png) in the top right of your screen to bring up the menu and select ![](../../../media/en/signal/textsecure-en-1/040.png).

![](../../../media/en/signal/signal-and-en-v02-030.png)

*Figure 1: Attaching a file on Signal*

**Step 3.** Select the type of file that you want to send.

![](../../../media/en/signal/signal-and-en-v02-031.png)

*Figure 2: File selection through Signal*

**Step 4.** Verify that the file you want to send is in the compose window and press ![](../../../media/en/signal/textsecure-en-1/024.png) to send it.

![](../../../media/en/signal/signal-and-en-v02-032.png)

*Figure 3: Image sent through Signal*

## 3.4. Identity Verification

To confirm that you are exchanging encrypted messages with the right people, you and your contacts should verify your Signal identities. You can do this by reading or scanning one other's cryptographic *fingerprints*, as explained below.

A cryptographic fingerprint is a long string of letters and numbers that uniquely identifies a given encryption key without revealing the key itself. This key (and thus its fingerprint) typically corresponds to a particular installation of the software that created it, so we often say that *a fingerprint uniquely identifies a specific user*. Of course, if an attacker gets a hold of your unlocked mobile phone, he or she can communicate using *your* key, which will have *your* fingerprint. So you should still protect your smartphone by, for example, enabling device encryption and setting a strong passphrase.

Signal encrypts each message you send with a single-use key, none of which are directly associated with your Signal fingerprint. Instead, this fingerprint is used to verify your *long-term identity key*, which allows others to ensure that they are really communicating with you.

If you skip the *identity verification* process described below, an attacker might impersonate both you and the person with whom you are communicating. Such an imposter could record your messages or statements, then re-encrypt them and relay them back and forth between you. This is called a *man-in-the-middle* attack.

### 3.4.1. Reading Fingerprints

To verify identities by comparing fingerprints, both you and your contact should follow the steps below:

**Step 1.** Open an existing conversation with your contact, tap on the menu icon at the top right corner of the screen and select **Conversation settings**.

![](../../../media/en/signal/signal-and-en-v02-033.png)

*Figure 1: Conversation settings on Signal*

**Step 2.** Tap ![](../../../media/en/signal/textsecure-en-1/044.png) from the options under "Conversation settings".

![](../../../media/en/signal/signal-and-en-v02-034.png)

*Figure 2: Identity verification*

**Step 3.** You will be presented with a 66-character fingerprint of your Signal identity and that of your contact. These should be read to each other to verify that you both have the same fingerprints for each other, either in person or via a medium that allows you to confirm visually or audibly to whom you are talking to.

![](../../../media/en/signal/signal-and-en-v02-035.png)

*Figure 3: Signal fingerprints* 

### 3.4.2. Scanning Fingerprints

Alternatively to reading fingerprints, Signal users can verify their contact's identity by scanning their fingerprint. 

**Note:** To verify fingerprints by *scanning* you need to have [Barcode Scanner](https://play.google.com/store/apps/details?id=com.google.zxing.client.android) installed on your phone. If it is not available on your phone at the time of scanning, Signal will download and install the app for you.

To verify identities by scanning fingerprints, the following steps will need to be performed by both parties:

**Step 1.** Open an existing conversation with your contact, tap on the menu icon at the top right corner of the screen and select **Conversation settings**.

![](../../../media/en/signal/signal-and-en-v02-033.png)

*Figure 1: Conversation settings on Signal*

**Step 2.** Tap ![](../../../media/en/signal/textsecure-en-1/044.png) from the options under "Conversation settings".

![](../../../media/en/signal/signal-and-en-v02-034.png)

*Figure 2: Identity verification*

**Step 3.** You will be presented with a 66-character fingerprint of your Signal identity and that of your contact. 

![](../../../media/en/signal/signal-and-en-v02-035.png)

*Figure 3: Signal fingerprints*

**Step 4.** On both of your phones, tap on ![](../../../media/en/signal/textsecure-en-1/046.png).

**Step 5.** On your phone, tap ![](../../../media/en/signal/textsecure-en-1/047.png) and your contact should tap ![](../../../media/en/signal/textsecure-en-1/048.png). Your contact's phone will display a QR code and your phone will open **Barcode Scanner**.

![](../../../media/en/signal/signal-and-en-v02-036.png)

*Figure 4: Scanning options*

**Step 6.** Use **Barcode Scanner** on your phone to scan your contact's *QR code*.

![](../../../media/en/signal/signal-and-en-v02-037.png) ![](../../../media/en/signal/signal-and-en-v02-038.png)

*Figures 5 & 6: Your contact's QR code / Barcode scanner*

**Step 7.** Once the QR code has been successfully scanned Signal will check that the identity is valid.

![](../../../media/en/signal/signal-and-en-v02-039.png)

*Figure 7: Signal identity verified*

**Step 8.** Once you have verified your contact's Signal identity, they should repeat the above steps to verify yours.

## 3.5. Exporting your messages

While Signal allows you to export your messages, the messages in the backup file will **not** be encrypted and you should take additional steps to protect its contents. You can do this by following the steps below:

**Step 1.** Tap on ![](../../../media/en/signal/textsecure-en-1/015.png) in the top right of the main Signal screen to bring up the menu and select ![](../../../media/en/signal/textsecure-en-1/052.png).

![](../../../media/en/signal/signal-and-en-v02-040.png)

*Figure 1: Signal menu options*

**Step 2.** The next screen will open on the *import* options, tap ![](../../../media/en/signal/textsecure-en-1/053.png).

![](../../../media/en/signal/signal-and-en-v02-041.png)

*Figure 2: Import/Export options*

**Step 3.** On the *export* screen tap ![](../../../media/en/signal/textsecure-en-1/054.png).

![](../../../media/en/signal/signal-and-en-v02-042.png)

*Figure 3: Export plain text backup*

**Step 4.** Confirm that you want to export the unencrypted messages to the storage on your phone by tapping ![](../../../media/en/signal/textsecure-en-1/053.png).

![](../../../media/en/signal/signal-and-en-v02-043.png)

*Figure 4: Export plain text backup confirmation*

**Step 5.** Signal will confirm the export has completed by displaying ![](../../../media/en/signal/textsecure-en-1/055.png). You will find a file on your phone's storage that contains your **unencrypted** messages.

# 4. Encrypting voice calls with Signal

## 4.1. Making encrypted voice calls

Now that you have installed and configured Signal and have invited your friends, you can start making **encrypted calls**. You can easily do this by following the steps below:

**Step 1.** Open the Signal app.

![](../../../media/en/signal/signal-and-en-v02-102.png)

*Figure 1: Main Signal screen*

**Step 2.** Tap on the contact you wish to call. 

![](../../../media/en/signal/signal-and-en-v02-103.png)

*Figure 2: Selecting a contact on Signal*

If your contact has Signal, a padlock will show up by the phone symbol in the top right of the screen. 

![](../../../media/en/signal/signal-and-en-v02-104.png)

*Figure 3: Padlock indicating the use of Signal by your contact*

**Step 3.** Signal will start ringing the contact.

![](../../../media/en/signal/signal-and-en-v02-019.png)

*Figure 4: Ringing your contact through Signal*

**Step 4.** Once the call is answered by your contact, Signal will show **two words** in the middle of your screen. By checking with your contact that you both have the same words on your screen, you can be sure that the call is not being tampered with. It is recommended that you say the first word and expect your contact to say the second, or vice versa.

![](../../../media/en/signal/signal-and-en-v02-020.png)

*Figure 5: Verification of contact*

## 4.2. Receiving encrypted voice calls

Receiving a call with Signal is similar to receiving a regular call on your phone. You can do this by following the steps below:

**Step 1.** When a Signal call comes in it will display a screen with the message **SIGNAL CALL** in the middle of the screen.

![](../../../media/en/signal/signal-and-en-v02-021.png)

*Figure 1: Incoming Signal call*

**Step 2.** Swipe ![](../../../media/en/signal/redphone-en/014.png) *right* to answer or *left* to reject the call.

**Step 3.** When you answer the call, Signal will display **two words** in the middle of your screen. By checking with your contact that you both have the same words on your screen, you can be sure that the call is not being tampered with. It is recommended that you say the first word and expect your contact to say the second, or vice versa.

![](../../../media/en/signal/signal-and-en-v02-022.png)

*Figure 2: Receiving an encrypted call with Signal.*

**Option 4.** If you call a Signal user from your regular phone dialler, you will be asked if you would like to make a secure call. Tap **Yes, Secure** to make an encrypted call or tap **No** to make an unencrypted call.
