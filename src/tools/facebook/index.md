---
title: Protect yourself and your data when using Facebook
weight: 101
collection: none
post_date: 29 January 2025
logo: ../../../media/en/logos/social_networking-logo.png
website: https://www.facebook.com
---

Read this guide to learn how to secure your Facebook account.

Also see our guides on [Instagram](../instagram) and [WhatsApp](../whatsapp) to increase your privacy and security when using other platforms belonging to Meta.

Consider if relying on commercial, corporate products is secure enough to protect your information. Commercial corporations might not share your values and remove or filter content that they believe violates their policies. It may also be difficult for them to understand your local context, particularly if they do not operate in your language.


# Know how to move your data out of Facebook

- Test how to download your data from your Facebook Account. Start the download process, then take a thorough look at what data it provides. Assess what has been downloaded, in what format and how you can read and use it.
    - [Learn how to download your Facebook data](https://www.facebook.com/help/212802592074644/).
    - Allow some time for this process. If you have had an account for a long time, there will be a lot of data to download, and the service may take a day or so to bundle it for your download.

<details><summary>Learn why we recommend this</summary>

Before you store data on a large commercial platform, it is always a good idea to understand what it will take to move your data out in case you change your mind and want to migrate to a different service. It is also important to consider that in some cases it may take a long time for your data to be completely deleted from the servers of a large platform.
</details>

# Inform yourself about Facebook's policies

## Learn how Facebook uses your information

- You can find a clarification of Facebook's terms of service on [Terms of Service; Didn't Read](https://tosdr.org/en/service/182).
- [Learn about Facebook's privacy policy](https://www.facebook.com/privacy/explanation/).
- Also review other Facebook's policies, in particular their [terms of service](https://www.facebook.com/legal/terms/preview) and [community standards](https://transparency.meta.com/policies/community-standards/).

<details><summary>Learn why we recommend this</summary>

It is often unclear what online service providers will do with your information when you share it. Is your data combined with other information to guess things about you? Is it sold to other companies that may share that information even if you did not want it to be shared? Read their terms of service and privacy policy to find out.

You can also install [the add-on/extension of Terms of Service; Didn't Read](https://tosdr.org/en/downloads) in your browser to see an overview of the terms of service for each commercial online service provider you use.
</details>

## Learn what Facebook will turn over to governments or law enforcement

- See [Meta's policy on law enforcement requests](https://about.meta.com/actions/safety/audiences/law/guidelines).
- See [Meta's transparency report on government requests for user data](https://transparency.meta.com/reports/government-data-requests/) to learn about the nature and extent of these requests and the policies and processes Meta has in place to handle them.
- See [Meta's transparency report on content restrictions based on local law](https://transparency.meta.com/reports/government-data-requests/) to learn what kind of requests they received and how they responded.
- Also see [Meta's transparency reports page](https://transparency.meta.com/reports/).

<details><summary>Learn why we recommend this</summary>

Social media sites may give your information, including information you were trying to keep private, to governments or law enforcement agencies if requested to do so. Look through the links in this section to learn more about the conditions under which they have provided or will provide such information.
</details>

# Don't use the mobile app

- Do not use the app on your mobile device. Connect using a browser instead.
    - If you really need to use the app, consider using a separate device from the one you normally use.
    - In alternative, if you have and Android device, you can [isolate the app in a separate profile](../../communication/multiple-identities/#sandbox-your-applications-on-android).

<details><summary>Learn why we recommend this</summary>

Having an app that can constantly gather information on you can violate your privacy in many different ways even though you deny it permissions to access your geolocation, microphone, etc. By accessing Facebook through a browser, you can make sure that Facebook is not constantly connected to a mobile device that you take with you everywhere you go.
</details>

# Create a new account

You can [create a Facebook account](https://www.facebook.com/help/188157731232424) for free.

## Decide whether you will use a real or fake name, and maintain separate accounts

- Be aware that even if you provide a fake name to Facebook, you may still be identifiable by the network you connect from and the IP address it assigns to your device unless you [use a VPN or Tor to hide this information](../../communication/multiple-identities/#hide-your-ip-address).
- Consider using separate accounts or separate identities/pseudonyms for different campaigns and activities. You will likely want to keep your personal and work accounts separate, at the very least.
    - Read more on this strategy in our guide on [multiple identity management](../../communication/multiple-identities).
- Remember that the key to using a social networking platform safely is being able to trust the people you connect with. You and the others in your network will want to know that the people behind the accounts are who they say they are, and have ways to validate this. That does not necessarily mean you have to use your real name, but it may be important to use consistent fake names.
- Be aware that Facebook's [name policy](https://www.facebook.com/help/1090831264320592) gives them the right to remove your account if they believe you are using a name that you don't use in everyday life. Facebook also has a poor track record of recognizing "real" names, particularly when they are not in English. If you choose a name that you don't use in everyday life, it's a good idea to use one that Facebook's algorithms will recognize as a plausible name and surname.

<details><summary>Learn why we recommend this</summary>

Some people maintain social media accounts with fake names, or one account with their actual name and one with a fake name, to ensure they can organize and connect with others with less risk to their free speech, safety or liberty.
</details>

## Set up with a fresh email address

- Set up a new email address on a privacy-friendly server. You can find a list of suggested privacy-friendly mail services in [the communication tools section](../../communication/tools/#more-secure-email-providers).
- If you need to protect your identity while setting up new email use [the Tor Browser or a trusted VPN](../../communication/multiple-identities/#hide-your-ip-address).

<details><summary>Learn why we recommend this</summary>

Email addresses are one of the easiest ways to search for you: you need to provide one each time you set up a new account. If you need to hide your identity, it is best to start over with a new social media account which you do not connect to your old accounts or to existing email addresses.
</details>

## Don’t associate your phone number with your account

- If possible, when setting up a new account, do not enter your phone number. Facebook will allow you to set up a new account with only an email address, and that can be harder to associate with you.
    - [Remove your phone number](https://www.facebook.com/help/124895950923762#remove-a-mobile-phone-number).
    - [Adjust who can see your phone number](https://www.facebook.com/help/117118145038822).

<details><summary>Learn why we recommend this</summary>

Your phone number can be easily used to look you up and identify you. Consider whether providing your regular phone number would increase your risk. Ask yourself if your local law enforcement can make a request to online service providers specifying your phone number to find out about activities associated with you, or whether someone seeking to harass or locate you might make use of your number.
</details>

## Skip “find friends”

- "Skip" or dismiss Facebook's requests to "allow access" to contacts on your device, import your contacts, connect with your other social media accounts or find more connections when you first set up an account. You can select your contacts more carefully afterwards.
    - [If you use the Facebook app, turn off automatic contact uploading](https://www.facebook.com/help/355489824655936/).
    - [Delete contacts you have uploaded through the Facebook app](https://www.facebook.com/help/147699975301738/).
- Consider only connecting to people you know and whom you trust not to misuse the information you post.
- If you need to connect to an online community of like-minded individuals whom you have never met, [consider carefully what information you will share with these people](#decide-what-information-to-share).
- Do not share your employer or educational background, as social media may use this information to share your profile with others unexpectedly.

<details><summary>Learn why we recommend this</summary>

Social media often ensure they will gain in popularity by using the contact lists in your devices and email accounts to find and recommend more people you might want to connect to. This can have dangerous effects when you want to keep your contacts hidden from others. Consider whether law enforcement in your area might use these contact lists to build a case against you and your colleagues if they confiscated your device or accessed your account. Or consider what might happen if social media revealed information about others you associate with to the public. If these are concerns for you, limit social media apps and sites permissions to use your contacts.
</details>

## Designate someone to manage your account if you are unable to do it yourself

- Note that for legal reasons, Facebook will not give a loved one or colleague access to your account. Rather, if you give them permission, they will make it possible to close and ["memorialize" your account](https://www.facebook.com/help/275013292838654/).
    - [Consider setting up a legacy contact in case of your death](https://www.facebook.com/help/991335594313139/choose-a-legacy-contact/).
- If you expect someone else may need to access your account quickly in the event of an emergency, arrange to share your login information with them using [secure communication](../../communication/private-communication).
- If someone you know has been arrested or detained and you need to suspend their account so as to stop others from accessing their network of contacts or compromising information, you can reach out to [Access Now Digital Security Helpline](https://www.accessnow.org/help/) or [Front Line Defenders](https://www.frontlinedefenders.org/emergency-contact) to request assistance in working with Facebook to secure access to their accounts. Also read [the Digital First Aid Kit troubleshooter Someone I Know Has Been Arrested](https://digitalfirstaid.org/arrested/) for further recommendations and see [the Digital First Aid Kit support page](https://digitalfirstaid.org/support/) to look for help desks that may support you for specific needs.
- [Learn how to remove the account for a medically incapacitated person](https://www.facebook.com/help/480409628639043).

<details><summary>Learn why we recommend this</summary>

Giving access to important accounts to trusted contacts in case of emergencies is something everyone should think about, regardless of their risk level. Internet service providers and social media sites have developed processes to handle situations where someone passes away or is seriously ill or jailed and others need to manage their account. Designating someone to care for your account can ensure others are notified of your situation, and prevent malicious people from defacing or hacking your account, or from finding out who your contacts are.
</details>

# Protect your account

## Check your recovery email and phone number

- [Check your recovery email](https://www.facebook.com/help/162801153783275/).
- [Check your recovery phone number](https://www.facebook.com/help/124895950923762/).
- Change this information immediately if you lose access to your recovery email address or recovery phone number.

<details><summary>Learn why we recommend this</summary>

Online platforms ask you for an email address and/or a phone number to help recover your account in case of authentication issues. The email address is also used to inform the user of any security-related event. It is important to check this information to be sure that an attacker has not changed it to gain control of your account later. Make sure to also secure your recovery email and phone number, as an adversary may hack into one of them to change the password of the account where you set them as recovery methods.
</details>

## Use strong passwords

Use strong passwords to protect your accounts. Anyone who gets into your Facebook account will gain access to a lot of information about you and anyone you are connected to.

- See our guide on [how to create and maintain strong passwords](../../password/passwords) for more information.

## Set up two-factor authentication (2FA)

- Use a security key, authenticator app or security codes for two-factor authentication.
- Do not use SMS or a phone call if possible, as your mobile phone company has full access to these communications and these methods are easier to hack into. Note that we don't recommend associating your usual phone number with your account, especially if your official name is not already associated with your account.
- [Learn how two-factor authentication works on Facebook](https://www.facebook.com/help/148233965247823/).
- Also go through the ["How to keep your account secure" checkup](https://www.facebook.com/privacy/checkup/), which will check your password and login alerts.

<details><summary>Learn why we recommend this</summary>

[See our guide on two-factor authentication](../../passwords/2fa) for more on why and how to set up two-factor authentication, sometimes known as 2FA or MFA.
</details>

## Download and save recovery codes to get back into your account

- [Get recovery codes to sign into Facebook if you can't use your usual two-factor authentication method](https://www.facebook.com/help/148104135383285).
    - Store these codes in your password manager.
		- Alternatively, print these codes out before you are in a situation where you might need them. Keep them somewhere secure and hidden, like a locked safe.

<details><summary>Learn why we recommend this</summary>

Having verification codes safely stored, written down or printed out gives you another way to get back into your account if you lose access to your two-factor authentication method.
</details>

## Consider turning on Advanced Protection

- [Learn how to turn on Advanced Protection](https://www.facebook.com/help/1052552578831700).

<details><summary>Learn why we recommend this</summary>

Facebook Advanced Protection helps particularly visible people adopt stronger security protections by simplifying security features and by providing additional security protections for accounts and Pages, including monitoring for potential hacking threats.

Advanced Protection is only available for people selected by Facebook. The criteria are currently not known to us. If you are eligible for Advanced Protection, you will see prompts on Facebook to learn about the program and enroll. We recommend you consider these prompts as soon as you see them.
</details>

## Be very careful with emails or messages claiming to be from Facebook

- [Learn more in the Facebook support page on suspicious emails and messages](https://www.facebook.com/help/225602007465207).
- [Check if Facebook emailed you recently](https://www.facebook.com/recent_emails/security).

<details><summary>Learn why we recommend this</summary>

Phishing messages might try to convince you they are coming from one of your service providers, to trick you into giving someone else access to your account. If you get a security email or text from an online service provider, don't click on any of its links. Also, do not provide your password. Instead, log in to your account and go to the security page of your account to confirm whether the message was legitimate.
</details>

## Create an alternative identity

- If you want to use Facebook without connecting it to your real identity, make sure to create your Facebook account through the [Tor Browser](../../internet-connection/tools/#tor-browser) and to [never connect with your actual IP](../../communication/multiple-identities/#hide-your-ip-address). Also make sure to create your account with [an email address that you created using Tor or a VPN](../../internet-connection/anonymity/#exchange-anonymous-emails).
     - If you are using Facebook through the Tor Browser, it's a good idea to use their onion address [https://www.facebookwkhpilnemxj7asaniu7vnjjbiltxjqhye3mhbshg7kx5tfyd.onion/](https://www.facebookwkhpilnemxj7asaniu7vnjjbiltxjqhye3mhbshg7kx5tfyd.onion/).

# Check suspicious access

## Check active sessions and authorized devices, review account activity and security events

- Look at the following instructions listing which devices have recently logged in to your account (including browsers or apps). Does every login look familiar?
    - [Check where you're logged in from in your Activity log](https://www.facebook.com/settings?tab=security).
    - [Check the methods you've set for login alerts to be sure you will actually receive them](https://accountscenter.facebook.com/password_and_security).
    - [Perform regular security checkups](https://www.facebook.com/privacy/review/?review_id=573933453011661).
    - [Read the support page on how to log out of Facebook if a session is opened on another device](https://www.facebook.com/help/211990645501187).
    - *Note that if you are using a VPN or Tor Browser, which can conceal your location, your own device may appear as connecting from unexpected locations*.
- If you see suspicious activity on your account, immediately change your password to [a new, long, random passphrase](../../passwords/passwords) you do not use for any other accounts. Save this in your [password manager](../../passwords/password-managers).

<details><summary>Learn why we recommend this</summary>

Your adversaries may find ways to log in to your account from their devices. If they do so, it is possible you will be able to see this from the Password and security section of your Accounts center, where you can review security issues by running checks across apps, devices and emails you have received from Facebook or Meta.
</details>

## Get notified about logins

- Get an alert when someone tries logging in from a device or location that's unusual for you.
    - [Learn how to set the alert](https://www.facebook.com/help/162968940433354).
    - Get the notification sent to the email address associated with your Facebook account, and make sure you choose an email that you check regularly.
    - When you receive such a notification, review the sign-in details, including device type, time and location. If this activity doesn't look familiar, click This wasn't me.

<details><summary>Learn why we recommend this</summary>

If you suspect your account may be at risk, use this feature to be notified right away of unusual access.
</details>

# If you think your account has been hacked

- [Perform a security checkup to see if anything looks unusual](https://www.facebook.com/privacy/review/?review_id=573933453011661).
- If your account has been compromised, [follow the steps listed in the Facebook page on how to secure a hacked or compromised account](https://www.facebook.com/hacked) or follow [the step-by-step walkthrough to get help with a hacked account](https://www.facebook.com/help/1306725409382822).
- If you can't solve this issue through the standard procedure, check out [the Digital First Aid Kit troubleshooter I lost access to my account](https://digitalfirstaid.org/topics/account-access-issues).

## Review other sites and apps that can access your account

- Avoid using your accounts to log in to other sites (like news sites for example). While it may be convenient to use your Facebook account as a login option, you may be also making it easier for attackers to exploit this functionality and you may leave more evidence of what you have viewed online. Use a different password for every site, and save it in your [password manager](../../passwords/password-managers).
- Be careful when connecting your accounts. You may be anonymous on one site, but exposed when using another.
- Use the following links to learn about sites and apps connected to your Facebook account:
    - [Check if your account is connected to third-party apps or services](https://www.facebook.com/settings?tab=applications).
    - If you find that there are third-party apps or services connected to your Facebook account, [disconnect them](https://www.facebook.com/help/211829542181913) and create new passwords for them.
- If you really need to connect your Facebook account to a third-party app or service, [learn how to manage the privacy settings for these apps and websites](https://www.facebook.com/help/218345114850283/).

<details><summary>Learn why we recommend this</summary>

Most online service providers allow you to integrate information with other services. For example, you can post an update on your X account and have it automatically posted on your Facebook account as well. When other sites and apps have access, they can also be used by hackers to get into your online accounts.
</details>

# Decide what information to share

- [Choose who you share content with](https://www.facebook.com/help/325807937506242#choose-who-you-share-content-with).
- Walk through [Privacy Checkup](https://www.facebook.com/help/443357099140264) to make sure you're sharing your information with who you want.
- [Set up regular Privacy Checkup reminders](https://www.facebook.com/help/443357099140264#set-up-privacy-checkup-reminders).
- Learn [how to manage what others can see about you](https://www.facebook.com/privacy/dialog/how-to-clean-your-profile?helpref=privacycenter).
- Read more on [Facebook's privacy settings and tools](https://www.facebook.com/help/325807937506242#choose-who-you-share-content-with).

## Consider what information you should avoid sharing

Information that should never be shared with others on social media, even via direct messages (DM), may be, for example:

- Passwords
- Personally identifying information, including:
    - your birthday,
    - your phone number (does it appear in screenshots of communications?),
    - government or other ID numbers,
    - medical records,
    - education and employment history (these can be used by untrustworthy people who want to gain your confidence.)
- Information that may lead to understand where you live, for example a picture of your house.

Information that you might not want to post on social media, depending on your assessment of the threats you are facing, is, for example, details about family members, information on your sexual orientation or activities and your email address (at least consider having more and less sensitive email accounts).

- Even if you trust the people in your networks, remember it is easy for someone to copy your information and spread it more widely than you want it to be.
- Agree with your network on what you do and do not want shared, for safety reasons.
- Think about what you may be revealing about your friends that they may not want other people to know; be sensitive about this, and ask them to be sensitive regarding what they reveal on you.

<details><summary>Learn why we recommend this</summary>

The more information about yourself you reveal online, the easier it becomes for others to identify you and monitor your activities. For example, if you share (or "like") a page that opposes some position taken by your government, agents of that government might very well take an interest and target you for additional surveillance or direct persecution. This can have consequences for anybody, across different regions. The family of an activist who has left their home country, for example, may be targeted by the authorities in their homeland because of things that activist has made publicly accessible online.
</details>

## Don’t share your location

- [Learn how location services work on Facebook](https://www.facebook.com/help/337244676357509/).
- We do not recommend you use the Facebook app on your mobile, but if you do make sure to turn off location services.
    - [Learn how to turn off location services on Android](https://www.facebook.com/help/337244676357509#to-turn-location-services-on-or-off-for-your-android-device).
    - [Learn how to turn off location services on iOS](https://www.facebook.com/help/337244676357509#to-turn-location-services-on-or-off-for-your-android-device).
- [Don't add or share your location to your posts](https://www.facebook.com/help/337244676357509#adding-or-sharing-your-location).

<details><summary>Learn why we recommend this</summary>

If you are worried about someone finding your current or past location and about location tracking, stop your accounts from storing your location information. In addition we recommend turning off location services on your devices, which also makes your battery charge last longer.
</details>

## Share photos and videos more safely

- Consider what is visible in photos you post. Never post images that include:
    - your vehicle license plates,
    - IDs, credit cards or financial information,
    - photographs of keys ([it is possible to duplicate a key from a photo](https://www.cnet.com/news/duplicating-keys-from-a-photograph/).)
- Think hard before you post pictures that include or make it possible to identify:
    - your friends, colleagues and loved ones (ask permission before posting),
    - your home, your office or other locations where you often spend time,
    - if you are hiding your location, other identifiable locations in the background (buildings, trees, natural landscape features, etc.).
- Learn how to [delete a photo you uploaded to Facebook](https://www.facebook.com/help/208547132518386/).
- [Control who can see your shared albums on Facebook](https://www.facebook.com/help/410653545719324).
- Learn how to [delete an album together with all the photos it contains](https://www.facebook.com/help/152153388188974).
- Learn how to [remove contributors from a shared album you've created](https://www.facebook.com/help/589394671112683).
- [Remove metadata before you post photos, videos and other files](../../files/destroy-identifying-information/#remove-metadata-from-pictures-videos-and-other-files).

<details><summary>Learn why we recommend this</summary>

What you share could put yourself or others at risk. Get in the habit of seeking consent before posting about others, where possible. You may want to work with your colleagues to set guidelines for what you will and won't share publicly, under what conditions.

Photos and videos can reveal a lot of information unintentionally, particularly what is in the background. Many cameras also embed hidden data (metadata or EXIF tags) about the location, date and time the photo was taken, the camera that took the photo, etc. Social media may publish this information when you upload photos or videos.
</details>

## Change who can see when you "like" things

- [Change "Who can see your social interactions alongside ads"](https://www.facebook.com/adpreferences/ad_settings) to "Only Me".
- Learn [how your interactions with videos on Facebook can be seen by other people](https://www.facebook.com/help/410629549531206).

<details><summary>Learn why we recommend this</summary>

"Likes" and other ways you appreciate posts are sometimes shared by social media in ways that are not clear. In some countries, "likes" or other apparently harmless interaction with other social media accounts have been used in legal cases. You may want to use caution when leaving reactions to others' content, depending on your understanding of the legal situation in your country.
</details>

## Don't share your birthday

- On Facebook, you can register with a birthday that is not your own, and you can change your birthday also later. However, do keep track of what you enter, in case you need it to regain access to your account.
    - [Change your birthday on Facebook and choose who can see it](https://www.facebook.com/help/563229410363824/).

<details><summary>Learn why we recommend this</summary>

If you include your actual birthday in your account information, it can be used to identify you.
</details>

# Decide who can see what

## Share to select people

- Go through the ["Who can see what you share" walkthrough in the Facebook Privacy Checkup](https://www.facebook.com/privacy/checkup/). Set everything under "Profile information" to "Only me." Set limits you think are reasonable on subsequent screens. Consider "Limit past posts" to make it harder for others to see things you posted some time ago.
- [Check out your activity log in your Facebook settings](https://www.facebook.com/settings/).
- [Learn how to Change who can see what apps you use on Facebook](https://www.facebook.com/help/201750739897196).

## Manage who can reply to what you post

- [Choose who can comment on your Facebook Page's posts](https://www.facebook.com/help/369765040737128).
- [Hide a comment from a post on your Facebook Page](https://www.facebook.com/help/297845860255949).
- [Stop people from posting on your Facebook profile](https://www.facebook.com/help/115469971891543).
    - You can also set it so you [review posts before you make them visible on your profile](https://www.facebook.com/help/215642745162773) (this means you have to do more work).
- [Control what visitors can post on your Facebook Page](https://www.facebook.com/business/help/356113237741414).

<details><summary>Learn why we recommend this</summary>

In some cases, replies to posts have been used to build false claims of human rights defenders associating with people they did not actually associate with. Replies can also be used to harass you. Controlling who can reply can help lower your stress levels.
</details>

## Think about group membership and who you connect with

- [Control who can add you as a friend](https://www.facebook.com/help/217125868312360).
- [Choose who can follow you](https://www.facebook.com/help/201148673283205).
- Check [whether groups you are in are public or private](https://www.facebook.com/help/2580077045343981). Public groups can be seen by anyone and can be found through a Facebook search.
- Be aware that third parties can create apps that have access to groups. [Learn what information an app, website or game can access on Facebook](https://www.facebook.com/help/261149227954100).
- [Learn how to leave a group](https://www.facebook.com/help/172944012764072) (note that if the group was public, information about your past membership may still be online).
- [Remove or ban someone from a group you manage](https://www.facebook.com/help/211909018842184).
- [Delete posts or comments you published in a group](https://www.facebook.com/help/887936158224251).
- [Remove a post in a Facebook group you manage](https://www.facebook.com/help/805346819801369).
- [Turn off post comments or comment replies in a Facebook group](https://www.facebook.com/help/987143528046827).

<details><summary>Learn why we recommend this</summary>

When you join or start a community or group online it is revealing something about you to others. People may assume that you support or agree with what the group is saying or doing, which could make you vulnerable if you are seen to align yourself with particular political groups, for example. In some countries, connections on social media to individuals or groups have been used in court to make a case against someone, even when the two people were only loosely connected.

If you set up a group and people choose to join it, consider: what are they announcing to the world by doing so? For example, if it is a LGBTQI support group, will that affiliation bring dangers for members in your region? Consider the impact of visibility in your current moment. There may be times when it is valuable for your movement to be visible, and even at that moment people who want your support might need a way to connect with your group without being identified. Think strategically about the platforms where you create your groups, what you name them (would a coded name help, as it did the Mattachine Society or the Daughters of Bilitis gay and lesbian organizations in the 1950s?), and whether they are public or private.

If you join a large group with members that you don't know, be aware that adversaries might also join groups or make connections to identify you or your colleagues, get a better view of what you are doing, or even build false trust. If you suspect this is likely to happen, it is important to choose connections and post selectively when you make an account connected to your work.
</details>

## Review tags/disallow tags

- [Review tags that people add to your Facebook posts ](https://www.facebook.com/help/247746261926036).
- [Manage how others can tag or mention your Facebook Page](https://www.facebook.com/help/www/1679637135584406). Next to "Who can see posts you’re tagged in on your Page?", click the audience selector, then select "Only me".

<details><summary>Learn why we recommend this</summary>

As with comments and other connections, tags could be used by others to make false accusations about your activities in a case against you.
</details>

## Limit who can contact you

- Go through the ["How people can find you on Facebook" walkthrough in the Privacy Checkup](https://www.facebook.com/privacy/checkup/).
    - Limit "Who can send you friend requests?" to "Friends of friends"
    - Set "Choose who can look you up by your phone number and email address." to something more limited than "Everyone" (Consider that "friends of friends" may still make it possible for malicious people to sneak into your network).
    - In the last step, consider whether you want people to be able to find your Facebook profile using search engines like Google.

<details><summary>Learn why we recommend this</summary>

Limiting who can contact you can lessen the likelihood that you will be found when you are trying to be private, or targeted by people trying to falsely gain your trust or the trust of your network. Limiting these options can also be useful if you are being harassed in non-public messages.
</details>

## Manage advertising

- [Go to the wizard to manage your activity off Meta technologies](https://www.facebook.com/off_facebook_activity).
    - Click "Recent activity", then "Clear previous activity".
    - Click "Manage future activity", then "Disconnect future activity".
- [Go the ad settings](https://www.facebook.com/adpreferences/ad_settings).
    - In "Activity information from ad partners", choose "No, don’t make my ads more relevant by using this information".
    - In "Categories used to reach you", make sure all switches are set to "off" and remove "other categories".
    - Set "Ads from ad partners in other apps" to "Don’t show me ads from ad partners".
    - Set "Ads about Meta" to "Don't use my activity to show me ads about Meta".
    - Set "Social interactions" to "Only me".

<details><summary>Learn why we recommend this</summary>

There is a possibility governments or police forces might buy advertising data from social media companies to target you and your network with disinformation, or try to find you.
</details>

# Leave no trace

## Precautions when using a public or shared device

- Avoid logging in to your Facebook account from shared devices (like an internet cafe or other people's devices).
- Never save your passwords and delete your browsing history when you use a web browser on a public machine. Change the passwords of any accounts you accessed from shared devices as soon as you can, using your own device.

## Delete search history

- [Clear searches from your activity log](https://www.facebook.com/help/382661798450396/).
- [Clear history of your off-Facebook activity](https://www.facebook.com/help/287199741901674).

<details><summary>Learn why we recommend this</summary>

Some social media platforms keep track of things you have searched for within their sites and apps. If your account is compromised or your device is seized, your adversary could use this information against you, so it may be a good idea to clear this history out regularly, as well as clearing your browser history.
</details>

# Handle abuse

This section explains how to ask Facebook for assistance in situations of abuse, harassment, attacks and impersonation. If you are a human rights defender, journalist or activist at risk and Facebook is not assisting you effectively, you can reach out to [Access Now Digital Security Helpline](https://www.accessnow.org/help/) or [Front Line Defenders](https://www.frontlinedefenders.org/emergency-contact) to request assistance in working with Facebook. Also see [the Digital First Aid Kit support page](https://digitalfirstaid.org/support/) to look for help desks that may support you for specific needs.

## Report abuse

- [Learn how to report something inappropriate or abusive on Facebook](https://www.facebook.com/help/212722115425932).
- [Learn how to report inappropriate or abusive content](https://www.facebook.com/help/1380418588640631/how-to-report-things).

<details><summary>Learn why we recommend this</summary>

Social media have unfortunately become a favourite method of harassment and disinformation worldwide. If you see malicious impersonation, hashtags being flooded, disinformation being spread, or if you or your colleagues are being targeted and harassed, you may find help by following Facebook's reporting procedures. Review the processes for reporting in the support pages linked in this section.
</details>

## Report harassment that reveals information about you

- [Learn how to report a privacy violation](https://www.facebook.com/help/1561472897490627).
- [Read Facebook's tips on how to handle bullying, harassment or personal attacks](https://www.facebook.com/help/116326365118751).

<details><summary>Learn why we recommend this</summary>

Some abusers may try to target you by revealing information about where you live or work, your family or friends, or other personal details including images and videos. In many cases you have a right to have this content taken down, even if it is true. This section provides information on how to get that content removed.
</details>

## Identify and report coordinated inauthentic activity (botnets and spam)

- [Learn how to manage spam on Facebook](https://www.facebook.com/help/217854714899185).

<details><summary>Learn why we recommend this</summary>

Some harassment and disinformation is posted through automated means, rather than by individuals. If you suspect that you are seeing this "coordinated inauthentic activity," you can report it to the platform that is hosting this content and they may ban those automated systems. While automation can be hard to prove, there are some cases in which reporting coordinated inauthentic activity might be more successful than reporting harassment, if you suspect the online platform will not understand the context of the harassment.
</details>

## Report impersonation

- [Learn how to report impersonation on Facebook profiles or Pages](https://www.facebook.com/help/174210519303259).

## Hide stressful content

- [Learn how to block or unfriend someone](https://www.facebook.com/help/1000976436606344).
- Also see the [section on managing who can reply to what you post](#manage-who-can-reply-to-what-you-post) to learn how to limit or hide comments on your Facebook Page and how to stop people from posting on your Facebook profile.

<details><summary>Learn why we recommend this</summary>

Any of us may find some content more distressing than other people do, whether it be information on the death of a friend, public arguments which devalue us because of who we are or frightening events in the news. If you need a break from this stress, this section lists some tools which can help hide content you do not wish to see, for as long as you wish.
</details>

## Learn how to recover your account if it is disabled or suspended

- [Learn why your account might be suspended or disabled and how to try and recover it](https://www.facebook.com/help/103873106370583/).

<details><summary>Learn why we recommend this</summary>

For one reason or another, Facebook may suspend, and eventually disable, your account. Human rights defenders have sometimes had their accounts shut down, for example because they were documenting human rights abuses with violent scenes that violated the platform's policies, because they had been reported by a government, by the police or by people who disagreed with them, or even because Facebook did not understand their context well enough to make sense of what they were posting. If this happens to you, you can appeal the decision and ask to have your account restored. Review the instructions linked in this section for information on how to do this.
</details>

## Take a break from your account

- [Temporarily deactivate your account](https://www.facebook.com/help/214376678584711).

<details><summary>Learn why we recommend this</summary>

If you want to stop people from posting to your account for a while — for example because you need to take a break, but also because you suspect you may be detained or jailed — you can consider temporarily deactivating your Facebook account. This can be useful also if you are facing harassment or defamation.
</details>

## Register as a journalist, if you work in the news industry

- [Learn more about journalist registration and its benefits](https://www.facebook.com/business/help/620369758565492?id=1843027572514562).
- [Register as a journalist](https://www.facebook.com/journalist_registration).

<details><summary>Learn why we recommend this</summary>

If you are a journalist or work with a news organization in another role, Facebook can register your account, giving you greater security protection and marking you as a verified source. Unfortunately, this does not apply to freelance or citizen journalists, and may require you to provide additional personal information.
</details>
