---
title: Create and maintain strong passwords
weight: 015
post_date: 28 March 2024
topic: passwords
---

Passwords are important tools for keeping your data and identity secure. Unfortunately, attackers know this, and they have many tricks they can use to figure out your passwords.

But you can defend against those tricks by applying a few important tools and tactics.

# Protect your passwords

- Always use a secure, updated, protected and trusted device to access your accounts and manage your sensitive information, including passwords. Read the guides on [Phones and Computers](../../phones-and-computers/) to learn how to make sure your device is secure.
- Use a password manager. No human brain is powerful enough to develop and remember passwords that are sufficiently long, random and unique to keep all of their devices and accounts secure. A password manager generates and stores these passwords for you, protecting them with encryption. Learn more on what password managers you can use and how to use them in [our guide on password managers](../password-managers).
- Use two-factor authentication (2FA or MFA). Two-factor authentication (2FA), also called multi-factor authentication (MFA) adds a second factor of authentication to your login system. By using 2FA, you can make sure that even if someone manages to guess your password, they cannot log in because they still need a second factor which is harder to get. To learn how to use 2FA in the safest way, read [our guide on two-factor authentication](../2fa).

Read the following sections to learn more about how to protect your passwords and accounts from the most common attacks.

# Create a stronger password

In a nutshell, we suggest to create all the passwords you need with an [offline password manager](../password-managers) and to just have [few longer passphrases that you can remember](#remember-a-few-secure-passwords) to unlock the device where you have installed your password manager and the password manager database.

If you prefer to create all your passwords manually, here's the minimum you should do to make sure your password is strong enough:

- Create a long password - 12 characters is the bare minimum you should aim for. 14 is even better.
- Use letters, numbers and symbols.
- Be aware that the following strategies, on their own, DON'T make your passwords safe. However, you can add them to a strong password to make it more complex.
    - Replacing letters with similar symbols or numbers (for example, "a" with "@" or "e" with "3"). This is an old strategy that is very well-known to people who know how to crack a password.
    - Adding exclamation marks, numbers, or other punctuation at the end - this is also a known strategy that can be guessed by someone who may try to crack your password.
    - Starting Each Word With Upper Case Letters - this is also a known strategy that will be included in attempts at cracking your passwords.

Also remember the following recommendations:

- Do not use a single word that is included in any dictionary.
- Do not use common phrases, such as famous quotations or sentences from song lyrics and poems.
- Do not use words or numbers related to you or people and organisations around you, like:
    - names of people, pets, or organisations,
    - dates of birth, important anniversaries or holidays,
    - telephone numbers or addresses,
    - or anything else a person could find out by researching you and people around you.
- Remember that you don't have to change your passwords frequently. To decide when to change a password, read [When to change your password](#when-to-change-your-password).

# Remember a few secure passwords

Even using a password manager, you will need to memorize a few secure passwords to unlock the device where you have installed your password manager and to unlock the password manager database where you keep your strong and unique passwords.

- Use [the diceware method](https://www.eff.org/dice) to generate strong passwords that you can remember:
    1. Get [a list of numbered words](https://www.eff.org/files/2016/07/18/eff_large_wordlist.txt) and 5 dice.
    2. Roll the 5 dice to get a five-digit number (for example, 6,2,5,1,1).
    3. Use the word in the list with the corresponding number.
    4. Repeat this six times. Use the six words as a "passphrase".
        - Do not re-use this passphrase anywhere else.
    5. Next, [create a mental image](https://xkcd.com/936/) using the words in your passphrase, in order. This will help you [remember the phrase](https://theworld.com/~reinhold/dicewarefaq.html#memory).
    6. Practice entering these passwords regularly, daily at first and then at least once a week. Repetition will help you commit these passwords to memory.
- If you don't have any dice and need to generate a password you can remember you can [generate it with KeePassXC](https://keepassxc.org/docs/KeePassXC_UserGuide#_generating_passphrases).
- Alternatively, you can use the "book method":
    1. Take a random book, possibly not related to your main activity.
    2. Open the book at a random page.
    3. Keeping your eyes closed, put your finger on the open page to choose a random word.
    4. Repeat this process 6 times to obtain a passphrase consisting of 6 random words.
        - Note that this method is less secure, especially if you use a book that's in your office, as it creates less random passphrases. But if you don't want to install software and you use a random book or magazine, it can be a good alternative to generate a long passphrase that you can remember.

<details><summary>Learn why we recommend this</summary>

There will be a few passwords you must memorize, including the password to log in to the device where you have installed your password manager and the master password to your password manager. The diceware method can help you create passphrases that are easy to remember but extremely difficult to guess, even for an attacker who has a lot of time and skills and knows how to use "password cracking" software.
</details>

# If you need to share passwords

- Avoid sharing passwords whenever possible:
    - If you must share a password with a friend, family member or colleague, change it to something temporary and share that password. Change it to something secure again when they are done using it.
    - Consider creating separate accounts for each individual who needs access to a service; many services make this possible. You can limit what actions these accounts are allowed to take, and what they can see.
    - If you need to share access to your device, consider creating a separate account for that device. See the basic security guides for [Android](../../phones-and-computers/android/#create-separate-user-accounts-on-your-devices), [Linux](../../phones-and-computers/linux/#create-separate-user-accounts-on-your-devices), [macOS](../../phones-and-computers/mac/#create-separate-user-accounts-on-your-devices), and [Windows](../../phones-and-computers/windows/#create-separate-user-accounts-on-your-devices) for instructions on how to do this.
        - On iOS devices this option is not available, unless you are using an iPad. See [Shared iPad overview](../../phones-and-computers/mac/#create-separate-user-accounts-on-your-devices) to learn how this works.
- If you really want to share passwords with others, you can set up your KeePassXC so you can use it collaboratively. See [the KeePassXC documentation](https://keepassxc.org/docs/KeePassXC_UserGuide.html#_database_sharing_with_keeshare) to learn how to do this.

<details><summary>Learn why we recommend this</summary>

Every time you share a password, it is almost like you have made an extra copy of your house key and given it away: there is an additional risk of someone else losing that key. In fact, it is riskier than that, because your "house" can then easily be accessed by devices far away without your noticing. Reduce this "attack surface" by avoiding sharing passwords whenever possible.
</details>

# Find out if your passwords have been compromised

- Search [the ';--have i been pwned? website](https://haveibeenpwned.com/) to see if your accounts are reported as compromised.
    - Change any of your account passwords you find there immediately, following the instructions for [using a password manager](../password-managers/#use-a-password-manager-installed-in-your-device).
- Even if none of your accounts show up here, you should still follow the instructions in this guide, as many account breaches are not reported.

<details><summary>Learn why we recommend this</summary>

Attackers look for passwords that have already been breached and are available online. They try passwords for your accounts until they find the correct one to get in. Reusing the same password is thus particularly risky. Have a look at [';--have i been pwned?](https://haveibeenpwned.com/) to see if your passwords are on any of the lists attackers use.
</details>

# Be aware of how attackers can guess your passwords

Here are the most common ways attackers find your passwords:

1. They can guess your password:
    - By using your personal information such as important dates or names, or also famous quotes, songs or authors you like,
    - By using a dictionary,
    - By slightly changing passwords you have used before,
    - By using software to try all possible combinations to unlock your account.
2. They can look for:
    - Where your passwords are written down (like [notes around your desk](https://web.archive.org/web/20180119154754/https://www.businessinsider.com/hawaii-emergency-agency-password-discovered-in-photo-sparks-security-criticism-2018-1)),
    - What you’re typing when you enter your password,
    - Passwords that have already been breached and are available online.
3. They can trick you into:
    - Installing a malware app to record your password,
    - Making you type your password into a fake login page through [phishing](https://ssd.eff.org/module/how-avoid-phishing-attacks),
    - Providing your passwords or other information by pretending to be a support person or someone you know (also known as social engineering).
    - Read more on how to recognize attempts at pressuring you to act quickly or appeal to your emotions in [our guide on malware](../../malware/#be-aware-that-malicious-actors-may-try-to-pressure-you-to-act-quickly-or-appeal-to-your-emotions).
4. They can exploit vulnerabilities:
    - Hacking the website where they want to log in with your password,
    - Stealing your password if it is stored in your browser,
    - Stealing your password from apps you use on your phone.

# Do not give your password when someone emails, calls, or messages you

- Go to the app or site for the service that supposedly sent you the message to verify the request.
    - See our guides on [protecting yourself and your data when using social media](../../communication/social-media/) to find the records of alerts different services may have sent you.
- If it appears to be a person or office you know sending the message, contact them through another channel to verify whether they made the request.
    - For example, if the message was an email, call them.
    - Do not click links in the email or send a response.
- Be aware that when a message is trying to frighten you, make you curious, make you feel you will miss an opportunity, or otherwise make you act quickly and without thinking, it could be a phishing attempt. Pause, remain calm, and find other ways to verify messages like these.

<details><summary>Learn why we recommend this</summary>

Attackers often pretend to be someone they are not, like a bank or technical support representative, to convince us to give them sensitive information. Attackers also often play on our emotions and human nature to make us give them passwords when we shouldn't.

If you receive a call, email or message requesting your password or other sensitive information, or if an email or text includes a link and asks you to click it to provide that information, it is very likely that someone is trying to trick you.

Read more on how to recognize attempts at pressuring you to act quickly or appeal to your emotions in [our guide on malware](../../malware/#be-aware-that-malicious-actors-may-try-to-pressure-you-to-act-quickly-or-appeal-to-your-emotions).

Learn more on phishing in the [Surveillance Self-Defense Guide on how to avoid phishing attacks](https://ssd.eff.org/module/how-avoid-phishing-attacks).
</details>

# When to change your password

Change your password immediately when:

- It appears your account, devices or colleagues and people around you have been victims of a data breach.
- You entered your password on an untrusted, shared or public device (it might have malicious code installed).
- You are concerned that someone watched you type your password.

Minimize damage by warning others who may also have been affected.

Also consider changing your password if you get a credible warning from the services you use that there was an attempt to log in from an unauthorized device or location. In such cases, go through the following steps to figure out whether your password may actually be compromised:

- Look for news reports about data breaches.
- If you received the alert through an email or chat message, double-check on the service provider's own website that they sent the alert. Never click links in emails, SMS messages or chat messages.

See [our guide on social media](../../communication/social-media/) and the basic security guides for [Android](../../phones-and-computers/android/#set-your-screen-to-sleep-and-lock), [iOS](../../phones-and-computers/ios/#set-your-screen-to-sleep-and-lock), [Linux](../../phones-and-computers/linux/#set-your-screen-to-sleep-and-lock), [macOS](../../phones-and-computers/mac/#set-your-screen-to-sleep-and-lock), and [Windows](../../phones-and-computers/windows/#set-your-screen-to-sleep-and-lock) for instructions on how to change the password of your device.

<details><summary>Learn why we recommend this</summary>

Research shows that changing your password repeatedly does not necessarily improve security. When people are required to change passwords often, they tend to make only small changes to the password, instead of coming up with an entirely new password. Read more about this research [in Bruce Schneier's post on why changing passwords frequently is a bad security idea](https://www.schneier.com/blog/archives/2016/08/frequent_passwo.html).

It is more important to change your passwords when there is a data breach. Because we do not always know when data has been leaked, we do recommend changing passwords, especially for online services, every few months to a year, or immediately when there is a reason to believe it may be compromised.
</details>

# Mind where you are and who can see you typing a password

- If you’re in a public space and type your password, be mindful of whether you can be seen or recorded behind your back.
- Check to see if anyone is watching your keyboard or phone while you type your passwords.
- Use a privacy protecting screen to make it harder to see what you are typing.

<details><summary>Learn why we recommend this</summary>

Adversaries can monitor and record you entering a password. If a mobile device is confiscated by the authorities, and they don't have the password to unlock it, they can, for example, study the daily routines of the owner of the device to find a CCTV that may have filmed the owner and the screen of the device while they were typing the password to unlock the device.
</details>

# Avoid fingerprint or face unlock (biometrics)

- If your device is set to unlock using your face or fingerprint, change your settings to use password unlock instead.
-  See the basic security guides for [Android](../../phones-and-computers/android/#set-your-screen-to-sleep-and-lock), [iOS](../../phones-and-computers/ios/#set-your-screen-to-sleep-and-lock), [Linux](../../phones-and-computers/linux/#set-your-screen-to-sleep-and-lock), [macOS](../../phones-and-computers/mac/#set-your-screen-to-sleep-and-lock), and [Windows](../../phones-and-computers/windows//#set-your-screen-to-sleep-and-lock) for instructions on how to do this.

<details><summary>Learn why we recommend this</summary>

Biometrics can make it quicker to access your devices, using personal features like your fingerprint or face. However, they are generally a less secure way of locking your device and account. Unlike a password, you cannot change your fingerprint whenever you like. Many people are required to provide biometric information at airports, government offices, etc. This creates a potential risk that someone will be able to access your accounts without your consent. If your adversaries physically restrain or force you, it can be easier for them to unlock your devices than it would be if you lock your device with a password.
</details>

# Consider how and when to use passkeys

Some online platforms may suggest you use a [passkey](https://www.eff.org/deeplinks/2023/10/what-passkey) instead of your username and password.

Passkeys offer stronger protection against phishing, but rely a lot on biometrics and cloud sharing, which we don't recommend using for anything sensitive, let alone for credentials to log in to your accounts. If you think you may be exposed to advanced phishing attacks, however, you may choose to protect yourself against this threat with passkeys, and use them without biometrics or cloud sharing.

What you will need to activate a passkeys is either a password manager with passkey support or a security key or a high-security chip built into your computer or phone. In most cases people will unlock their passkey by using a cloud-based password manager or their device lock method, which can be based on biometrics like your fingerprint or face.

But this isn't really necessary: in most cases, you can store your passkeys in more secure ways, either in a physical security key or in the high-security chip built into your computer or phone, which we recommend locking with a passphrase rather than through biometrics.

You can also store passkeys with [KeePassXC](https://keepassxc.org/docs/KeePassXC_UserGuide#_passkeys) by enabling the [KeePassXC browser integration](https://keepassxc.org/docs/KeePassXC_UserGuide#_setup_browser_integration), but since this solution relies on your browser, it can be exposed to browser-based attacks and should be considered less secure than the other solutions relying on hardware devices and high-security chips.

# Set safer recovery questions

Many web services ask for "security questions" or "recovery questions" when you create an account. To make it less likely that someone guesses these, you can use the following strategies:

- Provide fake, unrelated answers to these questions.
- You can even use another random, unique code generated by your password manager.
- Make sure to save your responses in your password manager so you don't risk forgetting your recovery questions and getting locked out.

<details><summary>Learn why we recommend this</summary>

Recovery questions are important to helping services verify your identity if they suspect someone else is trying to access your account. You use these answers to change your password in case you lose access to your account. Unfortunately, your answers to questions like "What town were you born in?" or "What is your pet's name?" can be easy to find online. By giving fake answers, you can make it harder for an attacker to hijack your account.
</details>

# Further reading

- Surveillance Self-Defense, ["Creating Strong Passwords"](https://ssd.eff.org/en/module/creating-strong-passwords)

- Tech Radar, ["The dangers of password sharing at work"](https://www.techradar.com/news/the-dangers-of-password-sharing-at-work)

- Security researcher Daniel Miessler compiled a list of the [10,000 most common passwords](https://github.com/danielmiessler/SecLists/blob/master/Passwords/Common-Credentials/10-million-password-list-top-10000.txt). Avoid using anything on this list as a password.

- Wikipedia has informative articles on [passwords](https://en.wikipedia.org/wiki/Password), [guidelines for password strength](https://en.wikipedia.org/wiki/Password_strength), and [how attackers access your accounts](https://en.wikipedia.org/wiki/Password_cracking).
