---
weight: 999
post_date: 28 March 2024
topic: passwords
title: Related Tools
subtitle: Passwords
---

Use the following tools to create and maintain strong passwords and to protect your devices and accounts. In this section, you can also learn more about how to [create and maintain strong passwords](../passwords), [manage your passwords safely](../password-managers) and [use 2-factor authentication](../2fa).

# Security in a Box Tool Guides

[![](../../../media/en/logos/keepassxc-logo.png)](#keepassxc)
[KeePassXC](#keepassxc)

[![](../../../media/en/logos/keepassdx-logo.png)](#keepassdx)
[KeePassDX](#keepassdx)


# Manage your passwords safely

## [KeePassXC](https://keepassxc.org)

[![](../../../media/en/logos/keepassxc-logo.png)](https://keepassxc.org)
(Linux, macOS, Windows)

A free and open-source password manager for computers

[Download](https://keepassxc.org/download/) | See [our guide](../../tools/keepassxc) and [their FAQ](https://keepassxc.org/docs/)

## [KeePassDX](https://www.keepassdx.com)

[![](../../../media/en/logos/keepassdx-logo.png)](https://www.keepassdx.com)
(Android)

A free and open-source password manager for Android phones and tablets

Download from [Google Play](https://play.google.com/store/apps/details?id=com.kunzisoft.keepass.free) or [F-Droid](https://www.f-droid.org/packages/com.kunzisoft.keepass.libre/) | See [our guide](../../tools/keepassdx) and [their documentation](https://github.com/Kunzisoft/KeePassDX/wiki)

## [Strongbox](https://strongboxsafe.com)

[![](../../../media/en/logos/strongbox-logo.png)](https://strongboxsafe.com)
(iOS, macOS)

A free and open-source password manager for iPhones and macOS computers

Download from the [iOS App Store](https://apps.apple.com/us/app/strongbox-keepass-pwsafe/id897283731) or the [macOS App Store](https://apps.apple.com/us/app/strongbox/id1270075435) | See [their documentation](https://strongboxsafe.com/getting-started/)

## [Bitwarden](https://bitwarden.com)

[![](../../../media/en/logos/bitwarden-logo.png)](https://bitwarden.com)
(Android, iOS, Linux, macOS, Windows)

An open-source online password manager with free, paid or self-hosted options

Download from [their website](https://bitwarden.com/download/), [Apple's iOS App Store](https://apps.apple.com/app/bitwarden-free-password-manager/id1137397744) or [Google Play](https://play.google.com/store/apps/details?id=com.x8bit.bitwarden) | See [their documentation](https://bitwarden.com/help/)

# Two-factor authentication (2FA)

## [Aegis](https://getaegis.app/)

[![](../../../media/en/logos/aegis-logo.png)](https://getaegis.app/)
(Android)

Free and open-source app to manage two factor authentication tokens (MFA, 2FA)

Download from [Google Play](https://play.google.com/store/apps/details?id=com.beemdevelopment.aegis) or [F-Droid](https://f-droid.org/en/packages/com.beemdevelopment.aegis) | See [their guide](https://github.com/beemdevelopment/Aegis)

## [Raivo OTP](https://github.com/raivo-otp/ios-application)

[![](../../../media/en/logos/raivo-otp-logo.png)](https://github.com/raivo-otp/ios-application)
(iOS, macOS)

Free and open-source app to manage two factor authentication tokens (MFA, 2FA)

Download from [Apple's iOS App Store](https://apps.apple.com/us/app/raivo-otp/id1459042137) or [Apple's App Store](https://apps.apple.com/us/app/raivo-otp/id1498497896)

### [FreeOTP](https://freeotp.github.io/)

[![](../../../media/en/logos/freeotp-logo.png)](https://freeotp.github.io/)
(Android, iOS)

Free and open-source app to manage two factor authentication tokens (MFA, 2FA)

Download from [Google Play](https://play.google.com/store/apps/details?id=org.fedorahosted.freeotp) or [F-Droid](https://f-droid.org/packages/org.fedorahosted.freeotp/) (Android) or from the [App Store](https://apps.apple.com/us/app/freeotp-authenticator/id872559395) (iOS)
