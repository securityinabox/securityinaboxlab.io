---
title: Manage your passwords safely
weight: 025
published: true
post_date: 28 March 2024
topic: passwords
---

No human brain is powerful enough to develop and remember passwords that are sufficiently long, random and unique to keep all of their devices and accounts secure. A password manager generates and stores these passwords for you, protecting them with encryption.

Password managers store passwords in your computer or on remote servers ("in the cloud"). Even if most cloud-based password managers are end-to-end encrypted, they can be exposed to hacking attempts without you noticing. This is why we strongly recommend using an offline password manager installed in your computer like the ones listed in this guide and in [our section on password managers](../tools/#manage-your-passwords-safely). If you still think that an online password manager is the best choice for you, read our recommendations [at the end of this guide](#if-you-decide-to-use-an-online-password-manager) to minimize potential risks.

Some browsers, email clients or built-in applications like iCloud Keychain will also offer to store your passwords for you. This is generally less secure than using a dedicated password manager for all your passwords. If you still think it would be convenient for you to store some passwords in your browser or mail client, you can follow [our advice on how to do this securely on Firefox, Chrome/Chromium and Thunderbird](#if-you-decide-to-store-some-passwords-in-your-browser-or-email-client). Any solution that uses the same password for unlocking both your account and your password manager (like iCloud Keychain for example) should not be considered secure enough and we strongly recommend using a [dedicated password manager](../tools/#manage-your-passwords-safely) instead.

Read the following paragraphs to learn how to use password managers to generate and safely store strong and unique passwords.

# Use a password manager installed in your device

- Get [KeePassXC](https://keepassxc.org/) (for Linux, macOS or Windows), [KeePassDX](https://keepassdx.com/) (for Android), or [Strongbox](https://strongboxsafe.com/) (for iOS or macOS).
- Let the password manager generate and save a long, random, unique password for each of your accounts.
- Read our guides on [KeePassXC](../../tools/keepassxc/) and [KeePassDX](../../tools/keepassdx) to learn how to generate and store your passwords safely.
- If you need an online password manager, see the section [on online password managers](#if-you-decide-to-use-an-online-password-manager) at the end of this guide.
- We do not recommend using your browser or email client to generate or store your passwords. If, for some reason, you really need to do this, read [our recommendations on how to reduce the risks of storing passwords in your browser or email client](#if-you-decide-to-store-some-passwords-in-your-browser-or-email-client).

<details><summary>Learn why we recommend this</summary>

We recommend [KeePassXC](https://keepassxc.org/), [KeePassDX](https://keepassdx.com/), and [Strongbox](https://strongboxsafe.com/). They are free to use, have been verified as secure by community experts, and continue to be updated. They store passwords in an offline database, which means you have control over where the data is stored and how it is managed.
</details>

# Back up the database of your password manager

Follow these instructions to back up the database of your password manager:

- [How to back up KeePassXC](https://keepassxc.org/docs/KeePassXC_UserGuide.html#_backing_up_a_database_file)
- [How to back up KeePassDX](https://github.com/Kunzisoft/KeePassDX/wiki/Backup)
- [How to back up Strongbox](https://strongboxsafe.com/support/#reamaze#0#/kb/migration-and-import-slash-export/does-strongbox-store-backups-how-can-i-export-them)

<details><summary>Learn why we recommend this</summary>

As with any individual password, losing many of your passwords at once could cause anything from a nuisance to a catastrophic loss of communication with your contacts or loss of your finances. Practice backing up your database regularly.
</details>

# If there are passwords or backup codes you need to keep outside of your password manager

- If you must write passwords down on paper, store them in a secure, locked place like a safe or desk drawer.
    - It's important that your passwords aren't visible to those who pass by, or easy to find and copy.
    - Do not keep paper copies of your passwords in your wallet.
- Destroy any paper copies of passwords or backup codes thoroughly as soon as you no longer need them.
- Alternatively keep these passwords on another device.
- You may also hide your passwords between other notes with no explanation or description.

<details><summary>Learn why we recommend this</summary>

Long passwords can certainly be hard to remember. For passwords you may not be able to save in your password manager (like the ones to unlock your devices), consider writing them down and protecting them with a physical lock.
</details>

# If you decide to use an online password manager

- Avoid storing highly sensitive account information (like financial account or recovery account logins) in the online database.
- Protect access to your online database with 2-factor authentication.
- We recommend [Bitwarden](https://bitwarden.com) as an online password manager.

<details><summary>Learn why we recommend this</summary>

Password managers that automatically synchronize between devices online can be easier to use. They store your database of passwords encrypted on servers. However, online password managers present the additional risk that an attacker could decrypt your database and access your passwords without you knowing.

We recommend [KeePassXC](https://keepassxc.org/), [KeePassDX](https://keepassdx.com/), and [Strongbox](https://strongboxsafe.com/) because they do not store your passwords online. If you do decide to use an online password manager, we recommend taking these steps for additional password protection.
</details>

# If you decide to store some passwords in your browser or email client

If you find it convenient to store some passwords in your browser or email client, it's best to only do so in Firefox and Thunderbird and to minimize risks based on the following advice.

- Avoid storing highly sensitive account information (like financial account or recovery account logins) in your browser.
- Make sure to set a strong and unique primary password following the instructions below:
    - [Firefox](https://support.mozilla.org/en-US/kb/use-primary-password-protect-stored-logins#w_create-a-primary-password)
    - [Thunderbird](https://support.mozilla.org/en-US/kb/protect-your-thunderbird-passwords-primary-password)
- Close your browser whenever you aren't using it and do not sync it on devices you do not trust.
