---
weight: 999
post_date: 25 March 2024
title: Related Tools
subtitle: Files
topic: files
---

Use the tools listed in this section to protect, destroy, or recover important information. In this section, you can also learn more about how to [protect your sensitive information](../secure-file-storage), [destroy sensitive information](../destroy-sensitive-information), [destroy identifying information](../destroy-identifying-information) or [recover from information loss](../backup).

# Security in a Box Tool Guides

[![](../../../media/en/logos/veracrypt-logo.png)](#veracrypt)
[Veracrypt](../../tools/veracrypt)

# Protect and encrypt your files

## [Veracrypt](https://www.veracrypt.fr)

[![](../../../media/en/logos/veracrypt-logo.png)](https://www.veracrypt.fr)
(Linux, macOS, Windows)

Free and open-source application that can encrypt some or all of the files on your computer.

[Download](https://www.veracrypt.fr/en/Downloads.html) | See [our guide](../../tools/veracrypt) and [their documentation](https://www.veracrypt.fr/en/Documentation.html)

## [Cryptomator](https://cryptomator.org)

[![](../../../media/en/logos/cryptomator-logo.png)](https://cryptomator.org)
(Android, iOS, Linux, macOS, Windows)

Free and open-source application that uses independent encryption to protect files you upload to online storage services like Google Drive, OneDrive and Dropbox.

[Download](https://cryptomator.org/downloads/) | See [their documentation](https://docs.cryptomator.org/en/latest/)

## [Tella](https://tella-app.org)

[![](../../../media/en/logos/tella-logo.png)](https://tella-app.org)
(Android and iOS)

A free and open-source app designed for documenting human rights violations that securely hides photos, videos, audio recordings and files on your device.

Download for [Android](https://play.google.com/store/apps/details?id=org.hzontal.tella) and [iOS](https://apps.apple.com/us/app/tella-document-protect/id1598152580) | See [their documentation](https://docs.tella-app.org/)

# Photos and videos

## [ObscuraCam](https://guardianproject.info/apps/obscuracam/)

[![](../../../media/en/logos/obscuracam-logo.png)](https://guardianproject.info/apps/obscuracam/)
(Android)

A free and open-source camera app that removes identifying information from photographs and from the metadata associated with them.

Download from [Google Play](https://play.google.com/store/apps/details?id=org.witness.sscphase1) or [F-Droid](https://f-droid.org/en/packages/org.witness.sscphase1/)

## [ProofMode](https://proofmode.org/)

[![](../../../media/en/logos/proofmode-logo.png)](https://proofmode.org/)
(Android, iOS and Apple Silicon)

A free and open-source camera app designed for documenting human rights violations, Proof Mode gives you the option to gather additional information, sign it cryptographically, and store it securely on your device when you take photographs or record video.

Download for [Android](https://play.google.com/store/apps/details?id=org.witness.proofmode), [iOS and Apple Silicon (Mac M1, M2)](https://apps.apple.com/us/app/proofmode/id1526270484)

## [PutMask](https://putmask.com/)

[![](../../../media/en/logos/putmask.png)](https://putmask.com/) (Android)

A privacy-friendly Android app to blur faces and other sensitive details in videos.

Download from [Google Play](https://play.google.com/store/apps/details?id=com.putmask.facefilter) | See [their tutorials](https://putmask.com/tutorials)


# Remove metadata from pictures, videos and other files

## [ExifCleaner](https://exifcleaner.com)

[![](../../../media/en/logos/exifcleaner-logo.png)](https://exifcleaner.com)
(Linux, macOS, Windows)

A free and open-source application that removes identifying information from the metadata in photos, videos and PDF files.

[Download](https://github.com/szTheory/exifcleaner/releases)

## [Scrambled EXIF](https://gitlab.com/juanitobananas/scrambled-exif)

[![](../../../media/en/logos/scrambledexif-logo.png)](https://gitlab.com/juanitobananas/scrambled-exif)
(Android)

A free and open-source app that removes identifying information from the metadata in photos.

Download from [Google Play](https://play.google.com/store/apps/details?id=com.jarsilio.android.scrambledeggsif) or [F-Droid](https://f-droid.org/en/packages/com.jarsilio.android.scrambledeggsif/)

## [MetaX](https://github.com/Ckitakishi/MetaX)

[![](../../../media/en/logos/metax-logo.png)](https://github.com/Ckitakishi/MetaX)
(iOS, macOS)

A free and open-source app that removes identifying information from the metadata in photos.

Download from [the App Store](https://apps.apple.com/us/app/metax/id1376589355)

# Destroy sensitive files

## [BleachBit](https://www.bleachbit.org/)

[![](../../../media/en/logos/bleachbit-logo.png)](https://www.bleachbit.org/)
(Linux, macOS, Windows)

A free and open-source application that removes traces of your work from various programs (see [cleaners repository](https://github.com/bleachbit/cleanerml)), clears browser trackers, and securely destroys files you have deleted.

[Download](https://www.bleachbit.org/download) | See [their guide](https://docs.bleachbit.org/)

## [CCleaner](https://www.ccleaner.com)

[![](../../../media/en/logos/ccleaner-logo.png)](https://www.ccleaner.com)
(Windows, macOS, Android, iOS)

Removes traces of your work from various programs, clears browser trackers, and securely destroys files you have deleted.

Download for [Windows](https://www.ccleaner.com/ccleaner/download/standard), [macOS](https://www.ccleaner.com/ccleaner-mac/download/standard), [iOS from the App Store](https://play.google.com/store/apps/details?id=com.piriform.ccleaner) or [Android from Google Play](https://play.google.com/store/apps/details?id=com.piriform.ccleaner) | See [their documentation](https://support.piriform.com)

## [Eraser](https://eraser.heidi.ie/)

[![](../../../media/en/logos/eraser-logo.png)](https://eraser.heidi.ie/)
(Windows)

Eraser is a free and open-source secure data removal tool for Windows. It completely removes sensitive data from your hard drive by overwriting it several times with carefully selected patterns.

[Download](https://eraser.heidi.ie/download/) | See [their video guides](https://eraser.heidi.ie/eraser-video/)

# Recover your files

## [Photorec](https://www.cgsecurity.org/wiki/PhotoRec)

[![](../../../media/en/logos/photorec-logo.png)](https://www.cgsecurity.org/wiki/PhotoRec)
(Linux, macOS, Windows)

Recover lost or deleted files on your computer or external storage devices.

[Download](https://www.cgsecurity.org/wiki/TestDisk_Download) | See [their guide](https://www.cgsecurity.org/wiki/PhotoRec_Step_By_Step)

## [Recuva](https://www.ccleaner.com/recuva)

[![](../../../media/en/logos/recuva-logo.png)](https://www.ccleaner.com/recuva)
(Windows)

Recover lost or deleted files on your Windows PC or SD card.

[Download](https://www.ccleaner.com/recuva/download/standard) | See [their documentation](https://support.ccleaner.com/s/articleresults?language=en_US&searched=recuva)
