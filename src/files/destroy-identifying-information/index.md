---
title: Destroy identifying information
weight: 035
published: true
post_date: 20 March 2024
topic: files
---

# Take identifying information out of your photos and other files

Pictures and videos can reveal a lot of information on you and the people you are with. For example, a photo could show the faces of participants in a demonstration, and a selfie could reveal the exact address where you are located at a specific time if you don't pay attention to the street sign you include in it.

But this is not all you can reveal about yourself when you publish a picture, a video or more. By default, photos, videos, PDFs and many other files include metadata, which is information on data. Metadata provides details on when a file was created, as well as where, by whom, with what device and so on. This information can be useful to find a particular file in a folder or to archive it, but it can also give away details to someone who wants to find out more about the person who created or published that file. For example, it could include GPS data and a timestamp, showing where you were when you made a picture and letting someone find your physical location that you were trying to hide.

This guide includes instructions on how to remove identifying information out of your photos, videos, PDFs, and other files.

# Remove identifying details from pictures

If you are taking pictures with your phone and would like to pixelate or obscure faces or other identifying details before you publish the photos online, you can use several tools.

Social networking platforms like Instagram, Facebook or Snapchat offer the possibility of editing images before you publish them, but this implies uploading the images to the platform before you remove the identifying details. This means that the platform will also have the original picture and will be able to choose to share it with others if requested to do so. So it is best to remove any identifying images with tools built into your phone, rather than through online apps.

Be aware that your original images could be automatically backed up. If you do not want this to happen, be sure to deactivate backup for your pictures or to delete the original images from your backup platform.

- For more information on removing identifying information from pictures on Android and iOS, see [Redacting photos on the go: A field guide](https://freedom.press/training/redacting-photos-on-the-go/).


**Android and iOS**

- [Signal](https://signal.org) is a free and open source privacy-friendly messaging app that you can also use to take a picture and blur faces included in it. See [Blur tools for Signal](https://signal.org/blog/blur-tools/) to learn how to do this.

    Signal also makes it possible to take a picture [sending it to yourself](https://support.signal.org/hc/en-us/articles/360043272451-Note-to-Self) and then download it and upload it to an online platform or send it through a different communication tool.

**Android**

- Use the free and open-source [Obscuracam](../tools#obscuracam) to blur faces and remove camera and location metadata.
- Alternatively, you can also use the [built-in Photo app to edit a photo](https://support.google.com/photos/answer/6128850#zippy=%2Cuse-tools). To be sure that your edit cannot be undone with deobfuscating techniques, use the pen tool rather than the marker and run it over the identifying details at least twice.

**iOS**

- Use the Photo app to [Write or draw on a photo](https://support.apple.com/guide/iphone/edit-photos-and-videos-iphb08064d57/ios#iph1c7564dba). Note that a fully opaque shape is more effective than the marker tools, which are not fully opaque. Consider redacting images by adding black rectangles or even using the text tool to add emojis.
- In apps such as Messages, Mail, Notes, and Books, you can draw on photos, screenshots, PDFs and more. See [Draw in apps with Markup on iPhone](https://support.apple.com/guide/iphone/draw-in-apps-iph893c6f8bf/13.0/ios/13.0) to learn how to do this.

**Linux, Windows, macOS**

- If you are taking pictures with a camera and are planning to upload them through a computer, you can blur identifying details with photo editing software like the free and open-source [Gimp](https://www.gimp.org). Learn how to do this in the [Gimp official guide](https://docs.gimp.org/2.10/en/gimp-tool-convolve.html).

<details><summary>Learn why we recommend this</summary>

It may seem like simply using a face blur feature or covering sensitive details will protect the people or places in your images. However, it may still be possible for someone with the file to see what you are covering up if the blur is not done in a specific way. Obscuracam for Android ensures the blur is done securely.

Certain blurring techniques may be vulnerable to deobfuscation methods. Read more on this in [Why You Should Never Use Pixelation To Hide Sensitive Text](https://dheera.net/posts/20140725-why-you-should-never-use-pixelation). You can find some recommendations on what blurring techniques work best in [Redacting photos on the go: A field guide](https://freedom.press/training/redacting-photos-on-the-go/).
</details>


# Remove identifying details from videos

**Android**

- Use [Putmask](https://putmask.com/) to blur faces in videos. See their [face track tutorial](https://putmask.com/face-track-tutorial) to learn how to obscure faces automatically.
- You can blur parts of your video on a computer in YouTube Studio. Learn how in [the Youtube guide on blurring videos](https://support.google.com/youtube/answer/9057652).

**iOS**

- Use [Blur-Video](https://apps.apple.com/us/app/blur-video/id1555770514) to blur faces, logos or objects in the background of your videos.

**Linux, Windows, macOS**

- Use the free and open-source [Shotcut](https://shotcut.org/) video editing software to blur identifying details in your videos. See the Youtube video tutorial [How To Blur Part Of Video in Shotcut](https://www.youtube.com/watch?v=k6eyh1UaRYI) to learn how to do this.


# Disable geolocation in your device

Removing metadata from files other than pictures can be hard on some operating systems, so it is a good idea to prevent the need to remove information on geolocation by turning off the location services in your devices before you create pictures, videos or audio files.

**Android**

- To turn geolocation off in your Android device, see [Manage your Android device’s location settings](https://support.google.com/accounts/answer/3467281).
- Also learn [how to turn off location and wipe history in our guide on how to protect your Android device](../../phones-and-computers/android/#turn-off-location-and-wipe-history).

**iOS**

- To turn geolocation off in your iPhone or iPad, see [Turn Location Services and GPS on or off on your iPhone, iPad, or iPod touch](https://support.apple.com/en-us/102647).
- Also learn [how to turn off location and wipe history in our guide on how to protect your iOS device](../../phones-and-computers/ios/#turn-off-location-and-wipe-history).

**macOS**

- To turn off Location Services on your Mac, see [Turn Location Services off in the official guide on allowing apps to detect the location of your Mac](https://support.apple.com/guide/mac-help/allow-apps-to-detect-the-location-of-your-mac-mh35873/mac#apda5e7d8a845e64).
- Also learn [how to turn off location and wipe history in our guide on how to protect your macOS device](../../phones-and-computers/mac/#turn-off-location-and-wipe-history).

**Windows**

- To stop apps from accessing your location settings, see [the official guide on Windows location service and privacy](https://support.microsoft.com/en-us/windows/windows-location-service-and-privacy-3a8eee0a-5b0b-dc07-eede-2a5ca1c49088).
- Also learn [how to turn off location and wipe history in our guide on how to protect your Windows device](../../phones-and-computers/windows/#turn-off-location-and-wipe-history).

**Linux**

- To turn off location services in Ubuntu, see [the official documentation on controlling location services](https://help.ubuntu.com/stable/ubuntu-help/privacy-location.html).
- To disable geolocation in Gnome-based operating systems, see [the official guide on activating and deactivating geolocation](https://help.gnome.org/users/empathy/stable/geolocation-turn.html.en).
- Also learn [how to turn off location and wipe history in our guide on how to protect your Linux device](../../phones-and-computers/linux/#turn-off-location-and-wipe-history).

It is also worth checking your apps permissions and disabling geolocation whenever possible. To learn how to do this, see our guides on how to protect your devices:

- [Android](../../phones-and-computers/android/#check-your-app-permissions)
- [iOS](../../phones-and-computers/ios/#check-your-app-permissions).
- [macOS](../../phones-and-computers/mac/#check-your-app-permissions).
- [Windows](../../phones-and-computers/windows/#check-your-app-permissions).
- [Linux](../../phones-and-computers/linux/#check-your-app-permissions).

# Remove metadata from pictures, videos and other files

If you need to remove metadata from pictures, videos, audio files, PDF etc., you can use the tools listed in this section. But be aware that in some operating systems it is only possible to delete or edit metadata in videos and pictures.

**Android**

- Use [Scrambled Exif](../tools#scrambled-exif) to remove identifying information from the metadata in your photos.
- Use [ExifTool for Android](https://play.google.com/store/apps/details?id=com.exiftool.free) to view, edit and delete metadata in photos and videos.

**iOS**

- Use [MetaX](../tools#metax) to check, edit and delete the metadata of your photos.
- Use the [Photo & Video Metadata Remover](https://apps.apple.com/us/app/photo-video-metadata-remover/id1079710135) app to remove metadata from your photos and videos. Be aware that this app doesn’t change the original image or video but creates a copy without the metadata.

**Linux, Windows and macOS**

- Use [Exifcleaner](../tools#exifcleaner) to clean metadata from images, videos, PDFs and other files.


<details><summary>Learn why we recommend this</summary>

Images contain more information than just what is visible in the picture. All files contain metadata about where and how they were created. You can usually get a look at some of a file's metadata on a computer by right-clicking the file and selecting **Properties** or **Get info**.

Some metadata may include your location or the model of the device the file was made with: information that someone looking at the file could use to identify you. The apps recommended in this section help you securely erase metadata.

To learn more about metadata and how you can clean it from your files, see [Everything you wanted to know about media metadata, but were afraid to ask](https://freedom.press/training/everything-you-wanted-know-about-media-metadata-were-afraid-ask/).
</details>
