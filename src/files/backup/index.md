---
title: Back up and recover from information loss
weight: 025
post_date: 14 March 2024
topic: files
---

There is a common saying among digital protection professionals: "the question is not _if_ you will lose your data, but _when_ you will lose it." A backup plan can minimize what you lose, so you can still access important information if any of your devices are stolen, seized, or damaged. Take or consider the following steps to plan ahead.

# Recover deleted or lost information

To reflect on what to do if you have lost your data, you can start troubleshooting with the help of the Digital First Aid Kit's guide [I lost my data](https://digitalfirstaid.org/topics/lost-data/).

You can use the tools listed in this section to recover deleted or lost data that was stored in a computer or external device.

Be aware that these tools do not work if your device has written new data over your deleted information. If possible, stop using your device until you have tried to recover the files (or had someone do that for you). Doing additional work on your device could write over the files you lost and make them impossible to retrieve. The longer you use your computer before attempting to restore the file, the less likely it is that the file will be retrievable.

**Linux, macOS, and Windows**

- Try [TestDisk & PhotoRec](https://www.cgsecurity.org/wiki/PhotoRec). See [the official guide](https://www.cgsecurity.org/testdisk.pdf) for instructions on how to use it.

**Windows**

- Try [Recuva](https://www.ccleaner.com/recuva/). See [their guide](https://www.ccleaner.com/knowledge/recuva-tip-search-for-files).


<details><summary>Learn why we recommend this</summary>

When you delete a file, it disappears from view, but it remains on your device. Even after you empty the Recycle Bin or Trash, files you deleted can usually be found on the hard drive. See [How to destroy sensitive information](../destroy-sensitive-information) to learn how this can put your security at risk.

But if you accidentally delete an important file, this might work to your advantage. Some programs, like the ones listed in this section, can restore access to recently deleted files.
</details>

# Create a backup plan

To create a backup plan, take the following steps:

## Organize your information

Before you make your backup plan, try to move all of the folders containing the files you intend to back up into a single location, such as inside the "Documents" or "My Documents" folder.

## Identify where and what your information is

The first step to making a backup plan is to picture where your personal and work information is currently located. Your email, for example, may be stored on your email provider's server, on your own computer, or in both places at once. And, of course, you might have several email accounts.

Then there are important documents on the computers you use in the office or at home: word processing documents, presentations, PDFs and spreadsheets, among other examples. Your computer and mobile devices also store your contact lists, chat histories and personal program settings, all of which can be considered sensitive.

You may also have stored some information on removable media like USB memory sticks, portable hard drives and other storage media.  If you have a website, it may contain a large collection of articles built up over years of work. Finally, don't forget your non-digital information, such as paper notebooks, diaries and letters.

You can learn more on how to map your information in the [Holistic security manual section on understanding and cataloguing information](https://holistic-security.tacticaltech.org/chapters/explore/2-4-understanding-and-cataloguing-our-information.html).

## Define which are primary copies and which are duplicates

When you are backing up files, it is sometimes suggested you use the 3-2-1 rule: have at least 3 copies of any piece of information, in at least 2 places, with at least 1 copy in a different place from the original.

Before you start making backups, decide which of the files you have mapped are "primary copies" and which are duplicates. The primary copy should be the most up-to-date version of a particular file or collection of files; it should be the copy you would actually edit if you needed to update the content. Obviously, this does not apply to files of which you have only one copy, but it is extremely important for certain types of information.

One common disaster scenario occurs when only duplicates of an important document are backed up and the primary copy itself gets lost or destroyed before those duplicates can be updated. For example, say you have been travelling for a week, updating a copy of an important spreadsheet stored on a USB memory stick. You should begin thinking of that copy as your primary copy, because it is more up-to-date than backup copies you may have made at the office.

Write down the physical location of all primary and duplicate copies of the information you have identified. This will help you clarify your needs and begin to define an appropriate backup policy. The following table is a very basic example. Your list may be much longer, and contain more than one data type stored on multiple storage devices.

| Data type               | Primary / Duplicate | Storage device      | Location |
|-------------------------|---------------------|---------------------|----------|
| Research files          | Primary             | Computer hard drive | Office   |
| Human rights violations testimonies | Duplicate | USB memory stick  | With me  |
| Program databases (photos, address book, calendar, etc.) | Primary  | Computer hard drive | Office   |
| Shared documents        | Duplicate           | Office server       | Office   |
| Videos and pictures     | Duplicate           | External hard disk          | Home     |
| Email & email contacts  | Primary             | Email account   | Email server |
| Text messages & phone contacts | Primary      | Mobile phone        | With me  |
| Printed documents (contracts, invoices, etc.) | Primary | Desk drawer | Office |

In the table above, you can see that:

- The only documents that will survive if your office computer's hard drive crashes are duplicates on your USB memory stick and other external storage media as well as shared documents on the server.
- You have no offline copy of your email messages or your address book, so if you forget your email password (or if someone manages to change it maliciously), you will lose access to them.
- You have no copies of any data from your mobile phone.
- You have no duplicate copies, digital or physical, of printed documents such as contracts and invoices.

After following the checklist in this section, you should have rearranged your storage devices, data types and backups in a way that makes your information more resistant to disaster. For example:

| Data Type       | Primary/Duplicate | Storage Device                 | Location |
|-----------------|-------------------|--------------------------------|----------|
| Human rights violations testimonies  | Primary | Computer hard drive | Office   |
| Human rights violations testimonies  | Duplicate | USB memory stick  | Home     |
| Research files  | Primary           | Computer hard drive            | Office   |
| Research files  | Duplicate         | USB memory stick               | With me  |
| Program databases | Primary         | Computer hard drive            | Office   |
| Program databases | Duplicate       | External drive                 | Home     |
| Email & email contacts | Primary    | Email account              | Email server |
| Email & email contacts | Duplicate  | Thunderbird on office computer | Office   |
| Text messages & mobile phone contacts | Primary | Mobile phone       | With me  |
| Text messages & mobile phone contacts | Duplicate | Computer hard drive | Office   |
| Text messages & mobile phone contacts | Duplicate | Backup SD card       | Home |
| Printed documents  | Primary        | Desk drawer                    | Office   |
| Scanned documents  | Duplicate      | External drive                 | Home     |
| Backup of all documents | Duplicate | Office server                  | Office   |

In the new table you will have 3 copies of information: in the computer, in the office server and at home, in 2 places, and at least one copy outside the office. 3-2-1 rule :)

Once you have completed your checklist, it's time to make the backup copies.

# Back up the files in your computer to a local device

Store your backup on portable storage media so that you can take it to a safer location. External hard drives or USB memory sticks are possible choices.

Because the files you decide to back up often contain the most sensitive information, it is important that you protect them using encryption. You can learn how to do this in [our guide on how to protect information](../secure-file-storage).

**Linux**

- Most Linux distributions include a backup tool. Ubuntu has a built-in tool called Déjà Dup which allows you to back up and encrypt your files. [See this guide to Déjà Dup](https://www.techtarget.com/searchdatabackup/tutorial/Tutorial-How-to-use-Linux-Deja-Dup-to-back-up-and-restore-files) to learn how to use it.

**macOS**

- [Back up to an external drive using Time Machine](https://support.apple.com/en-us/HT201250).


**Windows**

- Follow the instructions in the section on how to back up to an external drive in [How to back up your files in Windows](https://www.microsoft.com/en-us/windows/learning-center/back-up-files).


# Back up your phone to a local device

If you are backing up your mobile device to your computer, your next step should be storing that backup to an external storage media device. Set your phone to back up automatically.

To back up the contacts, text messages, settings and other data on your mobile phone, you may be able to connect it to your computer with a USB cable. You may also need to install software from the website of the company that manufactured your phone.

**Android**

- [Move files from your Android device to your PC](https://support.google.com/android/answer/9064445).
- If you find it difficult to back up all different type of information from the Android phone to a computer you can back up to [Google's cloud services using the tools built into your device](https://support.google.com/android/answer/2819582). Be aware that in such case your information will be saved on servers owned by Google.

**iOS**

- [Back up to your macOS computer](https://support.apple.com/en-us/HT211229)
- [Back up to your Windows computer](https://support.apple.com/en-us/108967)
- Make sure to select the Encrypt local backup checkbox and to create a strong password.

# Consider whether or not you should back up to "cloud" services

Consider whether you would prefer to use a service that encrypts your data for you as part of its service (called "end-to-end encryption" or "zero-knowledge" file storage services) or encrypt your files yourself and then back them up to the cloud.

<details><summary>Learn why we recommend this</summary>

When you hear someone call a computer service "the cloud," think "other people's computers." Online file storage services like Google Drive, iCloud or Dropbox store your backups and other data on servers (computers) owned by the service providers, which are often companies. This means that your adversary could have a lot of time to try and access your information without you noticing (unlike devices in your possession, where you would be more likely to notice suspicious activity). So it is likely better to make a local copy of your valuable data and store it somewhere safe.

However, if there is a strong likelihood that your devices or workspace might be destroyed, or your backup may be found and seized, it could make sense to store your encrypted data in trusted file storage services.
</details>

##  Protect your files before you store them on cloud services

- Get [Cryptomator](https://cryptomator.org/) and [use it to protect files](https://docs.cryptomator.org) you want to store on the cloud service.
- Alternatively, use [VeraCrypt](../../tools/veracrypt/) to create an encrypted volume, then copy it to the cloud.

<details><summary>Learn why we recommend this</summary>

If you are worried about someone (like hackers, or the owner of the service) accessing files you have stored online, you can protect them using encryption.
</details>

## Encrypted cloud services

If you decide to keep your files in the cloud, consider using one of the following zero-knowledge, end-to-end encrypted options:

- [Proton Drive](https://proton.me/drive)	(5 GB free; then paid)
- [Mega.io](https://mega.io/) (20 GB free; then paid)
- [Sync](https://www.sync.com/) (5 GB free; then paid)
- [Internxt](https://internxt.com/drive) (10 GB free; then paid)
- [Skiff Drive](https://skiff.com/drive)	(10 GB free; then paid)
- [Filen](https://filen.io/) (10 GB free; then paid)
- [PCloud](https://www.pcloud.com/) (paid plans, specifically pCloud Encryption)
- [SpiderOak](https://spideroak.com/one/) (paid plans)
- [Tresorit](https://tresorit.com/) (paid plans)
- [Nextcloud](https://nextcloud.com/) (NextCloud can be self-hosted if you have your own server, or else you can choose [a hosting provider](https://nextcloud.com/providers/) offering at least 2 GB free.)


## Back up to cloud services using your device's built-in tools

**Android**

- [Back up to your Google account](https://support.google.com/android/answer/2819582).
- Backups are uploaded to Google servers and are encrypted with your Google Account password. For some data, your screen lock PIN, pattern, or password is also used to encrypt your data.

**iOS**

- [Use iCloud](https://support.apple.com/en-us/HT204025).
- [Encrypt your iOS backups](https://support.apple.com/en-us/108756).

**macOS**

- [Use iCloud](https://support.apple.com/en-us/HT204025).
- [Turn on Advanced Data Protection for iCloud](https://support.apple.com/en-us/108756).

**Windows**

- [Use OneDrive](https://support.microsoft.com/office/back-up-your-documents-pictures-and-desktop-folders-with-onedrive-d61a7930-a6fb-4b95-b28a-6552e77c3057).


# Back up your email

- You can use an email client program (like Thunderbird) to regularly view and back up your email on your device. You can find explanations on the difference between downloading email with POP3 (to delete your email from the server) or IMAP (to keep it on the server) in [the official Thunderbird documentation](https://support.mozilla.org/en-US/kb/difference-between-imap-and-pop3). Most email services provide instructions on how to set up an email client to receive your mail with POP3 or IMAP.

# Scan and back up printed documents

- When possible, scan (or photograph) all of your important papers. Back up the scans or photos along with your other electronic documents, as discussed above.

# Set a backup schedule

- To back up all of the data types listed above, you will need a combination of software and processes. Make sure that each data type is stored in at least two separate locations.

# Practice recovery

- Once you have made your backups, test to make sure that you know how to open files and use them again. Remember that, in the end, it is the restore procedure, not the backup procedure, that you really care about!

# Establish procedures for coworkers

- If you are working with a team, write up and share procedures for all staff to reliably and securely back up files. Communicate the risks that losing your data could have to your ability to do your work. It may help to work with your colleagues to fill out a grid like the one above to identify all data you work with as a team.

# Other considerations

When you are making a backup plan, think of it in a larger perspective, asking yourself: "How can I recover from a disaster and keep working?"

Your plan should not just be about your files, but also about:

- Software you use, and the licenses for it,
- How you can replace equipment if it is lost, destroyed, or confiscated,
- Having a place you can go to to continue working in a crisis.

Planning for this may mean setting aside money to recover from a loss. For example, you might consider writing this amount into a grant.
