$( document ).ready(function() {

  // Hide snippet when empty
  function hide_preprocess_wrapper_when_empty() {
    $('.preprocess-wrapper').each(function(index) {
      if (!$(this).text()) {
        $(this).addClass('invisible');
      }
    });
  }

  // Hide toc when empty
  function hide_toc_when_empty (){
    if (!$('#toc li').length>0) {
      $('#toc').addClass('invisible');
    }
  }

  // Display scroll top when scrolling past header
  function toggle_scroll_top_when_scrolling_past_header() {
    if ('#scroll-top' && '#header') {
      if ( ($(document).scrollTop()) > ($('#header').height()) ) {
        $('#scroll-top').fadeIn();
      } else {
        $('#scroll-top').fadeOut();
      }
    }
  }

  // Display TOC modal trigger when scrolling some way down the page unless on mobile
  function toggle_toc_button_when_scrolling() {
    if ('#toc-modal-trigger' && '#header' && $('#toc li').length>0 ) {
      if ( ($(document).scrollTop()) > ($('#header').height()*6) ) {
        $('#toc-modal-trigger').fadeIn();
      } else {
        $('#toc-modal-trigger').fadeOut();
      }
    }
  }

  function init() {
    hide_preprocess_wrapper_when_empty();
    hide_toc_when_empty();
  }

  init();

  $(window).scroll(function () {
    toggle_scroll_top_when_scrolling_past_header();
    toggle_toc_button_when_scrolling();
  });

});
