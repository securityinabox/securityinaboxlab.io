$( document ).ready(function() {

  // Scroll the page to the position
  // @Param: position to scroll to
  function scroll_to_pos(pos){
    $('html, body').stop().animate({
        scrollTop: pos
    }, 1000);
  }

  /* The fixed navigation bar overlaps jump-to anchors in the page.
   * We workaround this by reimplementing jump-to behaviour using JS for
   * URLs with hash anchor (f.e. /en/tools/veracrypt#installing-veracrypt) and
   * jump-to links in HTML (f.e. <a href="#something").
   */
  function scroll_to_anchor_position() {

    // check if a URL with anchor as hash is called directly
    if(window.location.hash) {
        var hash = window.location.hash.substring(0);
        var pos = $(hash).offset().top-($("#top-nav").height()*4);
        scroll_to_pos(pos);
    }

    // listen if jump to anchor is clicked
    $('a[href^="#"]').on('click', function(event) {
        var target = $(this.getAttribute('href'));

        if( target.length ) {
            event.preventDefault();
            var pos = target.offset().top-($("#top-nav").height()*4);
            scroll_to_pos(pos);
        }

    });
  }

  // scroll to top of page
  function scroll_to_top() {
    $('#scroll-top').on('click', function(event) {
      scroll_to_pos(0);
    });
  }

  function init(){
    scroll_to_anchor_position();
    scroll_to_top();
  }

  init();
});
