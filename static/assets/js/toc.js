$( document ).ready(function() {

  /* Build the table of contents for Tools and Tactics on top of the page
   * by scraping all the H1s values and outputting
   * them as a list and appending the list to the #toc element.
   */
  function build_toc() {
    if ( $('body').hasClass('tools') || $('body').hasClass('tactics') ) {
      $('#toc').removeClass('toc-invisible');
      $('.loading-element').addClass('invisible');
      $('#content h1').each(function(index) {
        if (!$(this).hasClass('title')) {
          $('#toc ul').append('<li><a href="#'+$(this).attr('id')+'">'+$(this).text()+'</a></li>');
        }
      });
    }
  }

  // Returns true if a TOC has been build
  function has_toc(){
    if ( $('#toc li').length>0 ) {
      return true;
    } else {
      return false;
    }
  }

  /* Display the TOC if it exists in a (Bootstrap) modal
   * by simply copying the TOC over and display the trigger
   * for the modal
   */
  function add_toc_to_modal() {
    if ( has_toc() && $('#toc-modal .modal-body') ) {
      $('#toc').clone().appendTo('#toc-modal .modal-body');
    }
  }

  /* Hide the modal when clicking on a link
   * inside the modal.
   */
  function hide_modal_when_link_clicked() {
    $('#toc-modal a').click(function(){
      $('#toc-modal').modal('hide');
    });
  }

  function init() {
    build_toc();
    add_toc_to_modal();
    hide_modal_when_link_clicked();
  }

  init();
});
