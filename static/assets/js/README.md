

All JS files are minified into one file all-scripts.js using metalsmith-uglify. See configuration in
metalsmith.json. Pls note that metalsmith-uglify concatenates all JS files it finds in the metalsmith app.
Only those scripts specified in metalsmith.json are actually minified. The order matters!

This file is then loaded for every site using a defer script at the bottom of the body tag in main.jade.
