/**
 * Track source filename and current file in file metadata.
 */
function pluginInit() {
    return function plugin(files, metalsmith, done) {
        Object.keys(files).forEach(function (file) {
            // Original filename, only saved once.
            if (!files[file].src_file) {
                files[file].src_file = file;
            }
            // Current filename, updated each time to track changes.
            files[file].dst_file = file;
        });
        done();
    };
}

module.exports = pluginInit;
