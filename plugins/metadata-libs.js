// Simple plugin to load libraries in the global metadata.

function pluginInit(requireLibs = []) {
    const libs = [];
    for (const lib of requireLibs) {
        libs[lib] = require(lib);
    }
    return function plugin(files, metalsmith) {
        const metadata = metalsmith.metadata();
        metadata.libs = libs;
    }
}

module.exports = pluginInit;
