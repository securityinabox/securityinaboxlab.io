#!/bin/bash
# Script used to generate the initial PO files from existing translations.

set -e

LANGS=(es ar id fa fr pt ru th tr vi zh bo)
MASTER_LANG=en
PO_DIR=locales/po
SRC_DIR=src
OUT_DIR=content

FMT="text"
YFM_KEYS="author,download_links,license,logo,subtopic,system_requirements,"
YFM_KEYS+="teaser,teaser_image,title,website"
FMT_OPTS=(-o markdown -o neverwrap -o yfm_keys="${YFM_KEYS}")
CHARSET=UTF-8

gettextize() {
    local master="$1"
    local output="$2"
    local localised="$3"

    if [ "${output}" -nt "${master}" ]; then
        return
    fi
    CMD=(po4a-gettextize --master-charset "${CHARSET}" --format "${FMT}")
    CMD+=("${FMT_OPTS[@]}" --master "${master}" --po "${output}")
    if [ -n "${localised}" -a -f "${localised}" ]; then
        CMD+=(--localized "${localised}")
    fi
    echo "Gettextizing ${master} -> ${output}"
    "${CMD[@]}"
}

for srcfile in $(find "${SRC_DIR}" -type f -name index.md); do
    basename=$(realpath --relative-to="${SRC_DIR}" "${srcfile}")
    master=${basename%.md}
    #gettextize "${srcfile}" "${PO_DIR}/${master}.pot"
    for lang in "${LANGS[@]}"; do
        localised="${OUT_DIR}/${lang}/${basename}"
        gettextize "${srcfile}" "${PO_DIR}/${master}.${lang}.po" "${localised}"
    done
done
