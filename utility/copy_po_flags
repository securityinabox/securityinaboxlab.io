#!/usr/bin/env python3

# PO fix-up: copy missing flags from POT and re-wrap.

import argparse

import polib


# Copy flags from POT file, as GNU gettext drops any custom flags.
def copy_flags(pot, po):
  potentries = {
      entry.msgid_with_context: entry for entry in pot
  }
  for poentry in po:
    potentry = potentries.get(poentry.msgid_with_context)
    if not potentry:
      continue

    pofuzzy = poentry.fuzzy
    potflags = set(potentry.flags) - {'fuzzy'}
    poflags = set(poentry.flags) - {'fuzzy'}
    missing = potflags - poflags
    if newflags := poflags - potflags:
      print('Unexpected new flags in file %s: %s' % (po.fpath, newflags))

    poentry.flags.extend((
      flag for flag in potentry.flags if flag not in poflags))
  return po


def main():
  parser = argparse.ArgumentParser(
      description='Copy flags from POT file and re-wrap')
  parser.add_argument(
      'pofile', metavar='DEST',
      help='PO file to copy flags to')
  parser.add_argument(
      'potfile', metavar='TEMPLATE',
      help='template to copy flags from')
  parser.add_argument(
      '--wrap', metavar='N', type=int, default=77,
      help='wrap lines after N columns')

  args = parser.parse_args()

  pot = polib.pofile(args.potfile)
  po = polib.pofile(args.pofile, wrapwidth=args.wrap)
  copy_flags(pot, po)
  po.save()


if __name__ == '__main__':
  main()
