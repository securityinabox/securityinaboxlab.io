# Modify metalsmith.json to adjust to env variables.

# Set source and destination directories.
.source = $source_dir |
.destination = $dest_dir |

# Add prefix and site_url to metadata variables.
.metadata.prefix = $prefix |
.metadata.site_url = $site_url |

# Add prefix to redirections.
(
    .plugins[] | select(."metalsmith-redirect") |
    ."metalsmith-redirect".redirections[] |
    select(test("^/"))
) |= $prefix + . |

# Filter available locales.
(
    $linguas | split(" ") | map({(.): null}) | add
) as $linguas_idx |

.metadata.locales |= (
    with_entries(select(.key | in($linguas_idx)))
) |

# Use the metadata.locales object to configure filemetadata plugin.
(
    .metadata.locales | to_entries |
    map({
        pattern: (.key + "/**"),
        metadata: ({ locale: .key } + .value),
    })
) as $localemeta |
(
    .plugins[] | select(."metalsmith-filemetadata") |
    ."metalsmith-filemetadata"
) |= $localemeta + . |

# Use the metadata.locales object to configure i18n plugin.
(
    .plugins[] | select(."metalsmith-i18n") |
    ."metalsmith-i18n".locales
) = (.metadata.locales | keys) |

# Disable plugins according to the type of build (production/development):
#
# * In `development` builds disable uglification/minification and the drafts
#   plugin (so "draft" pages get rendered too), and enable "beautification" of
#   code to aid development and debugging.
# * In `production` builds the opposite is done, to optimise load times and
#   hide unfinished/outdated "draft" pages.
if $env != "production" then
    del(.plugins[] | select(."@metalsmith/drafts")) |
    del(.plugins[] | select(."metalsmith-uglify")) |
    del(.plugins[] | select(."metalsmith-clean-css"))
else
    del(.plugins[] | select(."metalsmith-beautify"))
end |

.
